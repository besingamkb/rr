<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* Name:  Twilio
	*
	* Author: Ben Edmunds
	*		  ben.edmunds@gmail.com
	*         @benedmunds
	*
	* Location:
	*
	* Created:  03.29.2011
	*
	* Description:  Twilio configuration settings.
	*
	*
	*/

	/**
	 * Mode ("sandbox" or "prod")
	 **/
	$config['mode']   = 'sandbox';

	/**
	 * Account SID
	 **/
	$config['account_sid']   = 'ACedfc8a4989b06f6a2e305b83ecf67d36';

	/**
	 * Auth Token
	 **/
	$config['auth_token']    = '49bf015338a5fad40939ebdb6b7a9429';

	/**
	 * API Version
	 **/
	$config['api_version']   = '2010-04-01';

	/**
	 * Twilio Phone Number
	 **/
	// $config['number']        = '(575) 219-6537';
	$config['number']        = '+1 415-599-2671';


/* End of file twilio.php */