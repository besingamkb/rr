<?php
$lang["header"] = array(
				'signup' => "inscription",
				'login' => "connexion",
				'logout' => "déconnexion",
				'dashboard' => "tableau de bord",
				'socialmarket' => "La place de marché sociale des Locations de vacances",
				'offer' => "Vous avez quelque chose à offrir?",
				'create_listind' => "Créer une liste.",						
				'free' => "C'est gratuit",						
				); 

$lang["search"] = array(
				'search' => "chercher",
				'where' => "Où voulez-vous aller",
				'checkin' => "enregistrer",
				'checkout' => "consultez",
				'guest' => "invité",
				);

$lang["travel"] = array(
				'travel' => "Voyage",
				'content' => "Des appartements et des chambres à cabanes dans les arbres et les bateaux: rester dans des espaces uniques dans 192 contires.",				
				); 

$lang["host"] = array(
				'host' => "Hôte",
				'content' => "La location de l'espace non utilisé pourrait payer vos factures ou de financer vos prochaines vacances.",				
				);

$lang["how_it_works"] = array(
				'how_it_works' => "Comment ça marche",
				'content' => "De Verified ID à notre équipe de support client dans le monde entier, nous sommes là pour vous",				
				);

$lang["neighborhood"] = array(
				'neighborhood' => "quartier",
				'allneighborhood' => "tous les guides de quartier",
				'content' => "Ne savez pas où séjourner? Nous avons créé des guides de quartier pour les villes du monde entier.",								
				);

$lang["footer"] = array(
				'subscribe' => "Souscrire",
				'group' => "Groupe",			
				'help' => "Aider",			
				'terms' =>"termes",			
				'contact_us' =>"Contactez-nous",			
				'about_us' =>"Qui Sommes-nous",		
				'how_it_works' => "Comment ça marche",
				);