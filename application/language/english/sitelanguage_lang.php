<?php
$c = get_homecontent();
$lang["header"] = array(
				'signup' => "Sign up",
				'login' => "Log in",
				'logout' => "Logout",
				'dashboard' => "Dashboard",
				'socialmarket' => $c->header_left,
				'offer' => $c->header_right,
				'create_listind' => "Create a listing.",						
				'free' => "It's free",						
				); 

$lang["search"] = array(
				'search' => "search",
				'where' => "Where do you want to go ?",
				'checkin' => "Check-In",
				'checkout' => "Check-out",
				'guest' => "Guest",
				);

$lang["travel"] = array(
				'travel' => $c->col1_heading,
				'content' => $c->col1_description,				
				); 

$lang["host"] = array(
				'host' => $c->col2_heading,
				'content' => $c->col2_description,				
				);

$lang["how_it_works"] = array(
				'how_it_works' => $c->col3_heading,
				'content' => $c->col1_description,				
				);

$lang["neighborhood"] = array(
				'neighborhood' => "neighborhood",
				'allneighborhood' => "all neighborhood guides",
				'content' => "Not sure where to stay? We've created neighborhood guides for cities all around the world.",								
				);

$lang["footer"] = array(
				'subscribe' => "Subscribe",
				'group' => "Groups",			
				'help' => "Help",			
				'terms' =>"Terms",			
				'contact_us' =>"Contact Us",			
				'about_us' =>"About Us",		
				'how_it_works' => "How It Works",
				);