<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front_model extends CI_Model {	

	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return  $this->db->insert_id();
		else
			return FALSE;		
	}

	public function get_result($table_name='', $id_array='',$id_array2=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			
       }
       endif;
		if(!empty($id_array2)):		
			foreach ($id_array2 as $key => $value){
				$this->db->or_where($key, $value);
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_row($table_name='', $id_array=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}

	public function get_pagination_result($table_name='', $limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get($table_name);
			return $query->num_rows();
		}
	}


	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;		
		return $this->db->update($table_name, $data);
	}

	public function delete($table_name='', $id_array=''){		
		return $this->db->delete($table_name, $id_array);
	}

	
	public function get_pagination_where($table_name='', $limit='', $offset='', $condition='')
 	{ 
		if(!empty($condition)):  
			foreach ($condition as $key => $value){
			 	$this->db->where($key, $value);
			}
		endif;
		$this->db->order_by('id','desc');   
		if($limit > 0 && $offset>=0)
		{
		$this->db->limit($limit, $offset);
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
		  return $query->result();
		else
		  return FALSE;
		}
		else
		{
		   $query=$this->db->get($table_name);
		   return $query->num_rows();
		}
 	}	


	public function get_pagination_or_where($table_name='', $limit='', $offset='', $condition='')
 	{ 
		if(!empty($condition)):  
			foreach ($condition as $key => $value){
			 	$this->db->or_where($key, $value);
			}
		endif;
		$this->db->order_by('id','desc');   
		if($limit > 0 && $offset>=0){
		$this->db->limit($limit, $offset);
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
		  return $query->result();
		else
		  return FALSE;
		}
		else{
		   $query=$this->db->get($table_name);
		   return $query->num_rows();
			}
 	}	
		

	function user_login($email, $password, $user_role, $flag='')
	{	
		$this->db->where('user_email', $email);
		if($flag == 1)
		{
			$this->db->where('password', $password);
		}
		else
		{
			$this->db->where('password', sha1($password));
		}	
		$this->db->where('user_role', $user_role);
		$this->db->where('user_status',1);
		$this->db->from('users');			
		$query=$this->db->get();
		$result = $query->row();
		if ($query->num_rows() == 1) 
		{				
			$row=array(
				'id'         => $result->id,
				'first_name' => $result->first_name,						
				'last_name'  => $result->last_name,
				'user_email' => $result->user_email,
				'user_role'  => $result->user_role,
				'user_status'=> $result->user_status,				
				'last_login' => $result->last_login,
				'last_ip'    => $result->last_ip,
				'logged_in'  => TRUE
				);						
		
			if($user_role == 3)
			{
				$this->session->set_userdata('user_info',$row);
			}
			if($user_role == 4)
			{
				$this->session->set_userdata('customer_info',$row);
			}
			$this->update('users', array('last_login'=> date('Y-m-d, h:i:s'), 'last_ip' => $this->input->ip_address() ), array('id' => $result->id ) );
			return "valid";			
		}
		else
		{
			return "invalid";
		}
	}

	

	public function get_property($offset, $location="", $check_in="", $check_out="", $guest="", $neghborhood_id="")
	{
		$limit = 3;

		if($check_in !="" && $check_out !="" )
		{
			$check_in_time = strtotime($check_in);
			$check_out_time = strtotime($check_out);
			$check_in = date('Y-m-d',$check_in_time); 
			$check_out = date('Y-m-d',$check_out_time);
            
			while($check_in_time <= $check_out_time)
			{
               $result = $this->db->get_where('pr_calender',array('start'=>date('Y-m-d',$check_in_time),'event_type !='=>2));
               if($result->result())
               {
               	  foreach ($result->result() as $row)
               	  {
               	     if(@in_array($row->property_id,$final_res))
               	     {
               	     	continue;
               	     }
                        $final_res[] = $row->property_id;	
               	  }
               }
               $check_in_time = $check_in_time+86400;
			}

            $this->db->where_not_in('pr.id',$final_res);
        }
        

		if($neghborhood_id !="" )
		{
			$places = get_property_of_neighbourhood($neghborhood_id);
			if(!empty($places))
			{
				foreach($places as $pr)
				{
					$this->db->or_where('pr.id', $pr->id);
				}
			}
        }



		if($location !="")
		{
			if(strpos($location, ',')){
				$arr = explode(',', $location);
				$i = 1;			
				$this->db->like('pi.city',$arr[0]);			
			}else{
				$this->db->like('pi.city',$location);
			}
		}
		

		if($guest !="" )
		{
			$this->db->where('pi.accommodates', $guest);
        }


		$this->db->order_by('pr.id', 'desc');		
		$this->db->select('pr.*, u.first_name,u.last_name,u.image, pi.unit_street_address,pi.city,pi.country,pi.state,pi.room_type');
		$this->db->from('properties as pr');
		$this->db->where('pr.status',1);
		$this->db->where('pr.is_delete !=', 1);
		$this->db->join('users as u',      'u.id = pr.user_id', 'left');
		$this->db->join('pr_info as pi',  'pr.id = pi.pr_id',   'left');
		$this->db->limit($limit,$offset);		
		$query = $this->db->get();
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}


	public function filter_property($post, $offset){	
		$limit = 3;

         /*Search check in date */
		if($post['search_check_in'] !="" && $post['search_check_out'] !="" )
		{
			$check_in_time = strtotime($post['search_check_in']);
			$check_out_time = strtotime($post['search_check_out']);
			$check_in = date('Y-m-d',$check_in_time); 
			$check_out = date('Y-m-d',$check_out_time);
            
			while($check_in_time <= $check_out_time)
			{
               $result = $this->db->get_where('pr_calender',array('start'=>date('Y-m-d',$check_in_time),'event_type !='=>2));
               if($result->result())
               {
               	  foreach ($result->result() as $row)
               	  {
               	     if(@in_array($row->property_id,$final_res))
               	     {
               	     	continue;
               	     }
                        $final_res[] = $row->property_id;	
               	  }
               }
               $check_in_time = $check_in_time+86400;
			}

            $this->db->where_not_in('pr.id',$final_res);
        }


		if($post['neghborhood_id'] !="" )
		{
			$places = get_property_of_neighbourhood($post['neghborhood_id']);
			if(!empty($places))
			{
				foreach($places as $pr)
				{
					$this->db->or_where('pr.id', $pr->id);
				}
			}
        }


		if($post['location'] !="")
		{
			$location = $post['location'];
			if(strpos($location, ',')){
				$arr = explode(',', $location);
				$this->db->like('pi.city',$arr[0]);		
			}else{
				$this->db->like('pi.city',$location);
			}
		}
		

		if($post['guest_count'] !="" )
		{
			$this->db->where('pi.accommodates', $post['guest_count']);
        }
         /*Search check out date */


		$this->db->order_by('pr.id', 'desc');

		if($post['room_type'] !=""){
			 $this->db->where('pi.room_type',$post['room_type']);			
		}

		if($post['bedroom'] !=""){
			if($post['bedroom'] == "5+")
				$this->db->where('pi.bed >','5');
			else
				$this->db->where('pi.bed',$post['bedroom']);
		}

		if($post['bathrooms'] !="")
			$this->db->where('pi.bath',$post['bathrooms']);

		if($post['keyword'] !="")
			$this->db->like('pr.title',$post['keyword']);

		if($post['pricerange'] !=""){
			$val = explode('-', $post['pricerange']);
			$this->db->where("pr.application_fee BETWEEN ".$val[0]." AND ".$val[1]);
		}

		if(!empty($post['amenities'])){
			foreach ($post['amenities'] as $key => $value) {
				$this->db->or_where('pa.pr_amenity_id',$value);				
			}
		}

		if($post['sortby'] !=""){
			if($post['sortby'] == 'l2h')
				$this->db->order_by('pr.application_fee', 'ASC');
			if($post['sortby'] == 'h2l')
				$this->db->order_by('pr.application_fee', 'desc');
			if($post['sortby'] == 'new')
				$this->db->order_by('pr.id', 'desc');
		}


			$this->db->group_by('pr.id');
			$this->db->select('pr.*, u.first_name,u.last_name,u.image,pi.unit_street_address,pi.city,pi.state,pi.country,pi.room_type');
			$this->db->from('properties as pr');
			$this->db->where('pr.status', 1);
			$this->db->where('pr.is_delete !=', 1);
			$this->db->join('users as u', 'u.id = pr.user_id', 'left');
			$this->db->join('pr_amenities as pa', 'pa.pr_id = pr.id', 'left');
			$this->db->join('pr_info as pi', 'pi.pr_id = pr.id', 'left');
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				$arr  = $query->result();
				if($offset > 0)
				{					
					$result = array_slice($arr, $offset,$limit);
				}
				else
				{
					$result = array_slice($arr, 0,$limit);
				}
				return $result;				
			}
			else
				return FALSE;
	}



	public function get_details($id){
		$this->db->group_by('pr.id');
		$this->db->where('pr.id', $id);
		$this->db->where('pr.status !=', 0);
		$this->db->where('pr.is_delete !=',1);
		$this->db->select('pr.*,pi.unit_street_address,pi.city,pi.state,pi.country,pi.bed_type,pi.room_type,pi.bed,pi.bath,pi.accommodates,pi.house_rules,u.first_name,u.last_name,u.image,cont.phone ');
		$this->db->from('properties as pr');		
		$this->db->join('pr_info as pi', 'pi.pr_id = pr.id', 'left');
		$this->db->join('users as u', 'u.id = pr.user_id', 'left');				
		$this->db->join('pr_contact_info as cont', 'cont.pr_id = pr.id', 'left');				
		$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->row();
			}else
				return FALSE;
    }

	public function group_members_properties($group_id="",$limit='',$offset='',$search=""){ 
	  $this->db->select('group_members_properties.*,users.first_name,users.last_name,users.user_email,properties.featured_image,properties.title,properties.description');
	  $this->db->from('group_members_properties');
	  $this->db->join('users', 'users.id = group_members_properties.member_id');
	  $this->db->join('properties', 'properties.id = group_members_properties.property_id');
	  $this->db->order_by('group_members_properties.id','desc');    
	  $this->db->where('group_members_properties.group_id',$group_id);
	  $this->db->where('properties.status',1);

	  if(!empty($search)){
	    $this->db->like('properties.title',$search,'after');
	  }
	    if($limit > 0 && $offset>=0){
	      $this->db->limit($limit, $offset);
	      $query=$this->db->get();
	      if($query->num_rows()>0)
	        return $query->result();
	      else
	        return FALSE;
	    }else{
	      $query=$this->db->get();
	      return $query->num_rows();
	    }
	  }

  	public function get_pagination_like($table_name='', $limit='',$offset='',$search=""){	
		$this->db->order_by('id','desc');
		if($search!=""){
		$this->db->like('group_name', $search);
		}			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get($table_name);
			return $query->num_rows();
		}
	}

    public function get_customer_members($id="",$limit='',$offset=''){	
		$this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
		$this->db->from('group_members');
		$this->db->join('users', 'users.id = group_members.member_id');
		$this->db->order_by('group_members.id','desc');			
		$this->db->where('group_members.group_id',$id);
		$this->db->where('group_members.member_role','4');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get();
				return $query->num_rows();
			}
	}

	public function get_owner_members($id="",$limit='',$offset=''){	
		$this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
		$this->db->from('group_members');
		$this->db->join('users', 'users.id = group_members.member_id');
		$this->db->order_by('group_members.id','desc');			
		$this->db->where('group_members.group_id',$id);
		$this->db->where('group_members.member_role','3');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get();
				return $query->num_rows();
			}
	}

  public function get_faq_search_result($post="")
  {
  	$this->db->like('question',$post['search_faq']);
  	$query = $this->db->get('faqs');
    return $query->result();
  }

	public function property_details_for_profile($limit, $offset, $uid='')
	{
		$this->db->select('pr.id prop_id, pr.title, pr.featured_image, pr.application_fee, pr.currency_type, p_in.unit_street_address, p_in.property_type_id, p_in.accommodates, p_in.square_feet');
		$this->db->from('properties pr');
		$this->db->join('pr_info as p_in','pr.id = p_in.pr_id','left');
		$this->db->where('pr.user_id',$uid);
		// $this->db->where('review.status',1);
		$this->db->order_by('pr.id','desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
		}
		else{
		   $query=$this->db->get();
		   return $query->num_rows();
		}
	}


	public function collection_property($collection_id='')
	{
		$this->db->select('properties.*,pr_info.*');
		$this->db->from('properties');
		$this->db->join('pr_info','pr_info.pr_id = properties.id','left');
		$this->db->where('properties.status',1);
		$this->db->where('pr_info.pr_collect_cat_id',$collection_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_my_groups($uid='')
	{
		$this->db->select('gm.id as relt_id, gm.member_id, g.*');
		$this->db->from('group_members as gm');
		$this->db->join('groups as g','gm.group_id = g.id');
		$this->db->where('gm.member_id',$uid);
		$this->db->order_by('gm.id','desc');
		$query=$this->db->get();
		if($query->num_rows()>0)
	    	return $query->result();
		else
		    return FALSE;
	}

	public function make_event_available($start="", $end="",$property_id="")
	{
		$this->db->where('property_id',$property_id);
		$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
		$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
		return $query=$this->db->delete('pr_calender');
	}

	public function check_event_existance($start, $end, $listing_id)
	{
		$this->db->where('property_id', $listing_id);
		$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
		$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
		$query=$this->db->get('pr_calender');
		// return $query=$this->db->delete('pr_calender');
		if($query->num_rows()>0)
		{
			$groupids = array();
			$lastgroupid = 0;
			foreach ($query->result() as $v) {
				if ($lastgroupid != $v->groupid) {
					$groupids[] = $v->groupid;
					$lastgroupid = $v->groupid;
				}
			}
			if (!empty($groupids)) {
				$this->db->select('property_id, start, end, rangeopen, rangeclose, groupid, color, textColor, event_type, price, currency_type');
				$this->db->order_by('start', 'asc');
				$this->db->where('property_id', $listing_id);
				$this->db->where('rangeopen', '1');
				$this->db->or_where('rangeclose', '1');
				$query=$this->db->get('pr_calender');
				if($query->num_rows() > 0){
					$temp = array();
					$i=0;
					foreach ($query->result_array() as $key => $value) {
						if (in_array($value['groupid'], $groupids)) {
							$temp[$i] = $value;
							$i++;
							// unset($temp[$key]);
						}
					}
					return $temp;
				}
				else
					return FALSE;
			}
			return $query->result();
		}
		else
			return FALSE;
	}


	////Fetch Event Starts
	public function get_events($table_name='', $id_array=array(), $order=array(), $month ,$year,$property_id=""){
        if(!empty($property_id))
        {
          $this->db->where('property_id',$property_id);
        }			

		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		if (!empty($order)):
			$this->db->order_by($order['by'], $order['order']);
		else:
			$this->db->order_by('id', 'asc');
		endif;

		if (!empty($month) && !empty($year)):
			$this->db->like('start', $year.'-'.$month, 'after');
		else:
			$this->db->like('start', date("Y-m"), 'after');
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	////Fetch Event Ends


}	