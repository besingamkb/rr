<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model {	

	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return  $this->db->insert_id();
		else
			return FALSE;		
	}

	public function get_result($table_name='', $id_array='',$id_array2=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($id_array2)):		
			foreach ($id_array2 as $key => $value){
				$this->db->or_where($key, $value);
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_row($table_name='', $id_array=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}

	public function get_pagination_result($table_name='', $limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get($table_name);
			return $query->num_rows();
		}
	}


	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;		
		return $this->db->update($table_name, $data);
	}

	public function delete($table_name='', $id_array=''){		
		return $this->db->delete($table_name, $id_array);
	}

	
	
	public function get_pagination_where($table_name='', $limit='', $offset='', $condition='')
	 	{ 
			if(!empty($condition)):  
				foreach ($condition as $key => $value){
				 	$this->db->where($key, $value);
				}
			endif;
			$this->db->order_by('id','desc');   
			if($limit > 0 && $offset>=0)
			{
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
			}
			else
			{
			   $query=$this->db->get($table_name);
			   return $query->num_rows();
			}
	 	}	


	/*user Bookings*/
		
	public function user_booking($email='',$user_name=''){
		$this->db->where('guest_name', $user_name);
		$this->db->where('email', $email);
		$query = $this->db->get('bookings');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;	
	}

	/*user Bookings*/

	/*user message*/
	
	public function user_message($email='',$user_name=''){
		$this->db->where('user_name', $user_name);
		$this->db->where('user_email', $email);
		$query = $this->db->get('user_message');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;	
	}

	/*user message*/


	/*faraz work*/

	public function get_pagination_or_where($table_name='', $limit='', $offset='', $condition='')
 	{ 
		if(!empty($condition)):  
			foreach ($condition as $key => $value){
			 	$this->db->or_where($key, $value);
			}
		endif;
		$this->db->order_by('id','desc');   
		if($limit > 0 && $offset>=0){
		$this->db->limit($limit, $offset);
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
		  return $query->result();
		else
		  return FALSE;
		}
		else{
		   $query=$this->db->get($table_name);
		   return $query->num_rows();
			}
 	}	
		

	
  public function customer_favorites_property($limit,$offset){

    $customer = $this->session->userdata('customer_info');
    $id = $customer['id'];
    $this->db->select('favorites_property.*,properties.title,properties.featured_image,users.user_email,users.first_name,users.last_name');
    $this->db->from('favorites_property');
    $this->db->join('users','users.id = favorites_property.user_id','left');
    $this->db->join('properties','properties.id = favorites_property.property_id','left');
    $this->db->where('favorites_property.customer_id',$id);
    $this->db->order_by('id','desc');
        if($limit > 0 && $offset>=0){
            $this->db->limit($limit, $offset);
            $query=$this->db->get();
            if($query->num_rows()>0)
              return $query->result();
            else
              return FALSE;
        }
        else{
           $query=$this->db->get();
           return $query->num_rows();
        }
    }


	public function customer_review($limit,$offset){
    $customer = $this->session->userdata('customer_info');
    $id = $customer['id'];
	$this->db->select('review.*,properties.title,users.user_email');
	$this->db->from('review');
	$this->db->join('users','users.id = review.user_id','left');
	$this->db->join('properties','properties.id = review.property_id','left');
	$this->db->where('review.customer_id',$id);
	$this->db->where('review.status',1);
	$this->db->order_by('id','desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
		}
		else{
		   $query=$this->db->get();
		   return $query->num_rows();
		}
	}

    public function get_customer_members($id="",$limit='',$offset=''){	
		$this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
		$this->db->from('group_members');
		$this->db->join('users', 'users.id = group_members.member_id');
		$this->db->order_by('group_members.id','desc');			
		$this->db->where('group_members.group_id',$id);
		$this->db->where('group_members.member_role','4');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get();
				return $query->num_rows();
			}
	}

	public function get_owner_members($id="",$limit='',$offset=''){	
		$this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
		$this->db->from('group_members');
		$this->db->join('users', 'users.id = group_members.member_id');
		$this->db->order_by('group_members.id','desc');			
		$this->db->where('group_members.group_id',$id);
		$this->db->where('group_members.member_role','3');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get();
				return $query->num_rows();
			}
	}

	public function group_members_properties($group_id="",$limit='',$offset='',$user_id=""){	
	$this->db->select('group_members_properties.*,users.first_name,users.last_name,users.user_email,properties.featured_image,properties.title');
	$this->db->from('group_members_properties');
	$this->db->join('users', 'users.id = group_members_properties.member_id');
	$this->db->join('properties', 'properties.id = group_members_properties.property_id');
	$this->db->order_by('group_members_properties.id','desc');		
	$this->db->where('group_members_properties.group_id',$group_id);
	if(!empty($user_id)){
     	$this->db->where('group_members_properties.member_id',$user_id);
	}
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}


}	