<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

	private $table_name	= 'users';			// user table name
	private	 $profile_table_name = 'user_profiles';	// user profiles table name
		
	function login($username=null, $password, $user_role=null) {	
		if(strpos($username,'@')===FALSE)			
			$this->db->where('username', $username);
		else 
			$this->db->where('user_email', $username);		

		$this->db->where('password', sha1($password));
		$this->db->where('user_role', $user_role);
		$query=$this->db->from($this->table_name);			
		$query=$this->db->get();

		if ($query->num_rows()===1) 
		{				
			$row=array(
						'id'=>$query->row()->id,
						'first_name'=>$query->row()->first_name,						
						'display_name'=>$query->row()->display_name,
						'user_email'=>$query->row()->user_email,
						'user_status'=> $query->row()->user_status,
						'last_ip'=>$query->row()->last_ip,
						'last_login'=>$query->row()->last_login,				
						'logged_in'=>TRUE
						);						

					if($user_role == 1)
					{
						$this->session->set_userdata('superadmin_info',$row);
					}
					elseif($user_role == 3)
					  {
					  	if($query->row()->user_status==0)
					  	{
							$msg['error_msg']='your Account is not activated';
							$msg['status']=FALSE;
							return $msg;
					  	}

					  	if($query->row()->is_delete == 1)
					  	{
							$msg['error_msg']='Your Account has been Deleted by Admin';
							$msg['status']=FALSE;
							return $msg;
					  	}

					  	$subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>$query->row()->id));
                        
                        if(empty($subadmin_restrictions))
                        {
 							$msg['error_msg']='Subadmin restrictions to this account is not added by Admin yet. Please contact the Admin First.';
							$msg['status']=FALSE;
							return $msg;
                        }

					  	if($query->row()->is_subadmin==1)
					  	{
							$this->session->set_userdata('subadmin_info',$row);
							$this->session->set_userdata('user_info',$row);
					  	}
					  	else
					  	{
							$msg['error_msg']='Invalid username and password.';
							$msg['status']=FALSE;
							return $msg;
					  	}

					  }
				// $this->session->set_userdata($session_name,$row);
				
				$this->update_login_info($query->row()->id, $record_ip=TRUE, $record_time=TRUE);

				$msg['error_msg']='Okk.';			
				$msg['status']=TRUE;
				return $msg;			
			}
			else
			{
				$msg['error_msg']='Invalid username and password.';
				$msg['status']=FALSE;
				return $msg;
			}

	}

	function user_login($username, $password,$flag='')
	{	
		$this->db->where('user_email', $username);
		if($flag==1){		
			$this->db->where('password', $password);
		}
		else
		{
			$this->db->where('password', sha1($password));
		}
		$this->db->where('user_role',3);
		$this->db->where('user_status',1);
		$query=$this->db->from('users');			
		$query=$this->db->get();
		if ($query->num_rows()===1) {				
			$row=array(
				'id'=>$query->row()->id,
				'first_name'=>$query->row()->first_name,						
				'last_name'=>$query->row()->last_name,
				'user_email'=>$query->row()->user_email,
				'user_role'=> $query->row()->user_role,
				//'lastlogin'=>$query->row()->last_ip,
				'last_login'=>$query->row()->last_login,				
				'logged_in'=>TRUE
				);						
			
				if($query->row()->user_status == 1)
					$session_name = 'user_info';

				$this->session->set_userdata($session_name,$row);
				
				//$this->update_login_info1($query->row()->id, $record_ip=TRUE, $record_time=TRUE);

				$msg['error_msg']='Okk.';			
				$msg['status']=TRUE;
				return $msg;			
				
			}else{
				$msg['error_msg']='Invalid username and password.';
				$msg['status']=FALSE;
				return $msg;
			}
	}


	function is_email_available($email)	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

		
	function create_user($data, $activated = TRUE) {
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;

		if ($this->db->insert($this->table_name, $data)) {
			$user_id = $this->db->insert_id();
			if ($activated)	$this->create_profile($user_id);
			return array('user_id' => $user_id);
		}
		return NULL;
	}

	function activate_user($user_id, $activation_key, $activate_by_email){
		$this->db->select('1', FALSE);
		$this->db->where('ID', $user_id);
		if ($activate_by_email) {
			$this->db->where('new_email_key', $activation_key);
		} else {
			$this->db->where('new_password_key', $activation_key);
		}
		$this->db->where('activated', 0);
		$query = $this->db->get($this->table_name);

		if ($query->num_rows() == 1) {
			$this->db->set('activated', 1);
			$this->db->set('new_email_key', NULL);
			$this->db->where('ID', $user_id);
			$this->db->update($this->table_name);

			$this->create_profile($user_id);			
			return TRUE;
		}
		return FALSE;
	}

	function update_login_info($user_id, $record_ip, $record_time) {
		
		if ($record_ip)		$this->db->set('last_ip', $this->input->ip_address());
		if ($record_time)	$this->db->set('last_login', date('Y-m-d H:i:s'));

		$this->db->where('id', $user_id);
		$this->db->update($this->table_name);
	}

	function ban_user($user_id, $reason = NULL) {
		$this->db->where('ID', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 1,
			'ban_reason'	=> $reason,
		));
	}

	function unban_user($user_id) {
		$this->db->where('ID', $user_id);
		$this->db->update('users', array(
			'banned'		=> 0,
			'ban_reason'	=> NULL,
		));
	}

	function set_password_key($user_id, $new_pass_key) {
		$this->db->set('new_password_key', $new_pass_key);
		$this->db->set('new_password_requested', date('Y-m-d H:i:s'));
		$this->db->where('ID', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	function can_reset_password($user_id, $new_pass_key, $expire_period = 900) {
		$this->db->select('1', FALSE);
		$this->db->where('ID', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		$this->db->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 1;
	}

	function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900) {
		$this->db->set('password', $new_pass);
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);
		$this->db->where('ID', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		$this->db->where('UNIX_TIMESTAMP(new_password_requested) >=', time() - $expire_period);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	function change_password($user_id, $new_pass) {
		$this->db->set('password', $new_pass);
		$this->db->where('ID', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	function delete_user($user_id) {
		$this->db->where('ID', $user_id);
		$this->db->delete($this->table_name);
		if ($this->db->affected_rows() > 0) {
			$this->delete_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}
	private function create_profile($user_id) {
		$this->db->set('user_id', $user_id);
		return $this->db->insert($this->profile_table_name);
	}
	
	private function delete_profile($user_id) {
		$this->db->where('user_id', $user_id);
		$this->db->delete($this->profile_table_name);
	}

}