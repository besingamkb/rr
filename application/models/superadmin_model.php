<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Superadmin_model extends CI_Model {	

	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return  $this->db->insert_id();
		else
			return FALSE;		
	}

	public function get_result($table_name='', $id_array='',$id_array2=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($id_array2)):		
			foreach ($id_array2 as $key => $value){
				$this->db->or_where($key, $value);
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_row($table_name='', $id_array=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}

	public function get_pagination_result($table_name='', $limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get($table_name);
			return $query->num_rows();
		}
	}


	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;		
		return $this->db->update($table_name, $data);
	}

	public function delete($table_name='', $id_array=''){		
		return $this->db->delete($table_name, $id_array);
	}

	
	public function members($limit='',$offset='',$sort='',$name='',$post="")
	{	
		if($sort =='ban'){
			$this->db->where('user_status',0);
		}
		if($sort =='unban'){
			$this->db->where('user_status',1);
		}
		if($sort =='old'){
			$this->db->order_by('created', 'asc');
		}
		if($sort =='new'){
			$this->db->order_by('created', 'desc');
		}
		else{
			$this->db->order_by('id', 'desc');
		}

		if(!empty($name))
		{
			$this->db->or_where('first_name', $name);
			$this->db->or_where('last_name', $name);
			$this->db->or_where('user_email', $name);
		 	$this->db->or_where('city', $name);
		 	$this->db->or_where('state', $name);
		}

		if(!empty($post['name'])){
			$this->db->like('first_name',$post['name'],"after");
		}

		if(!empty($post['email'])){
			$this->db->where('user_email',$post['email']);
		}
		
		if(!empty($post['status_typo'])){
			if($post['status_typo']=="active")
			{
	    		$this->db->where('user_status',1);
			}
			else
			{
     			$this->db->where('user_status',0);
			}
		}


		$this->db->where('user_role !=',1);
		$this->db->where('is_delete !=',1);
		if($limit > 0 && $offset>=0)
		{
			$this->db->limit($limit, $offset);
			$query=$this->db->get('users');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			$query=$this->db->get('users');
			return $query->num_rows();
		}
	}


	public function properties($limit='',$offset='',$post='')
	{	
	
		if(isset($post['sort']))
		{
			if($post['sort'] =='In-Active_Listings'){
				$this->db->where('status',0);
			}
			if($post['sort'] =='Active_Listings'){
				$this->db->where('status',1);
			}
			if($post['sort'] =='new_requests'){
				$this->db->order_by('created', 'desc');
			}
		}
		else
		{
		$this->db->order_by('id', 'desc');
		}


		if(!empty($post['property_title'])){
			$this->db->like('title',$post['property_title'],'after');
		}

		if(!empty($post['from_date'])){
			$date_1 = date('Y-m-d h:i:s',strtotime($post['from_date']));
			$this->db->where('created >=',$date_1);
		}

		if(!empty($post['to_date'])){
			$date_2 = date('Y-m-d h:i:s',strtotime($post['to_date']));
			$this->db->where('created <=',$date_2);
		}

		if(!empty($post['status_typo'])){
			if($post['status_typo']=="pending")
			{
	    		$this->db->where('status',0);
			}
			else
			{
     			$this->db->where('status',1);
			}
		}


		$this->db->where('is_delete !=',1);
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('properties');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get('properties');
			return $query->num_rows();
		}
	}

	// public function bookings($limit='',$offset='',$sort='',$name=''){	
	// 	if($sort =='max_am'){
	// 		$this->db->order_by('due_amount', 'desc');
	// 	}
	// 	if($sort =='min_am'){
	// 		$this->db->order_by('due_amount', 'asc');
	// 	}
	// 	if($sort =='old'){
	// 		$this->db->order_by('created', 'asc');
	// 	}
	// 	if($sort =='new'){
	// 		$this->db->order_by('created', 'desc');
	// 	}
	// 	else{
	// 		$this->db->order_by('id', 'desc');
	// 	}
	// 	if(!empty($name)){
			
	// 		$this->db->where('guest_name',$name);
	// 	}
				
	// 	if($limit > 0 && $offset>=0){
	// 		$this->db->limit($limit, $offset);
	// 		$query=$this->db->get('bookings');
	// 		if($query->num_rows()>0)
	// 			return $query->result();
	// 		else
	// 			return FALSE;
	// 	}else{
	// 		$query=$this->db->get('bookings');
	// 		return $query->num_rows();
	// 	}
	// }

	public function bookings($limit='',$offset='',$sort='',$name='',$post=""){	
		if($sort =='Cancelled_Bookings'){
			$this->db->where('status', '2');
		}elseif($sort =='Pending_Bookings'){
			$this->db->where('status', '0');
		}elseif($sort == 'completed_Bookings'){
			$this->db->where('status', '1');	
		}else{
			$this->db->order_by('id', 'desc');
		}

		if(!empty($name)){
			
			$this->db->or_where('guest_name',$name);
			$this->db->or_where('booking_number',$name);
			$this->db->or_where('check_in',$name);
			$this->db->or_where('check_out',$name);

		}

		if(!empty($post['guest_name'])){
			$this->db->like('customer_name',$post['guest_name'],"after");
		}

		if(!empty($post['book_number'])){
			$this->db->where('id',$post['book_number']);
		}
		
		if(!empty($post['start_check'])){
			$date_1 = date('m/d/Y',strtotime($post['start_check']));
			$this->db->where('check_in',$date_1);
		}
		
		if(!empty($post['end_check'])){
			$date_2 = date('m/d/Y',strtotime($post['end_check']));
			$this->db->where('check_out',$date_2);
		}
		
		if(!empty($post['status_typo'])){
			if($post['status_typo']=="pending")
			{
	    		$this->db->where('status',0);
			}
			else
			{
     			$this->db->where('status',1);
			}
		}

 		$this->db->where('is_delete_by_superadmin !=',1);

		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('bookings');
            if($sort=="Pending_Check-in"){
				foreach ($query->result() as $row) {
					$check_in_time = strtotime($row->check_in);
					$today_time = strtotime(date('d-m-Y'));
					$result_1="";
					if($check_in_time <= $today_time){
						$result_1 = $this->db->or_where('check_in',$row->check_in);
						$this->db->where('status','0');
				    }
				}

					if($result_1!=""){
						$resu=$this->db->get('bookings');
						return $resu->result();
					}
					else{
						return false;
					}
					
			}

			if($sort=="Pending_Check-out"){
				foreach ($query->result() as $row) {
					$check_out_time = strtotime($row->check_out);
					$today_time = strtotime(date('d-m-Y'));
                    $result="";
					if($check_out_time <= $today_time){
						$result = $this->db->or_where('check_out',$row->check_out);
						$this->db->where('status','0');
					}
			    }
					if($result!=""){
						$resu=$this->db->get('bookings');
						return $resu->result();					
					}
					else{
                     	return false;
                    }
            }

			if($query->num_rows()>0){
				return $query->result();
			}
			else
			return FALSE;
		}
	  else{
            $query=$this->db->get('bookings');
            if($sort=="Pending_Check-in"){
                $result_1='';					
				foreach ($query->result() as $row) {
					$check_in_time = strtotime($row->check_in);
					$today_time = strtotime(date('d-m-Y'));
					if($check_in_time <= $today_time){
						$result_1 = $this->db->or_where('check_in',$row->check_in);
						$this->db->where('status','0');
					}
				}

					if($result_1!=""){
	        			$resu=$this->db->get('bookings');
						return $resu->num_rows();
					}
					else{
						return false;
					}
					
			}

			if($sort=="Pending_Check-out"){
                    $result="";
					foreach ($query->result() as $row){
						$check_out_time = strtotime($row->check_out);
						$today_time = strtotime(date('d-m-Y'));
						if($check_out_time <= $today_time){
							$result = $this->db->or_where('check_out',$row->check_out);
							$this->db->where('status','0');
					    }
					}
					if($result!=""){
	        			$resu=$this->db->get('bookings');
		                return $resu->num_rows();					
					}
					else{
                     	return false;
                    }
            }
			  return $query->num_rows();
		}
	}

	public function trainers($limit='',$offset='')
	{
		$this->db->where('type','u');
		$this->db->order_by('id', 'desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('users');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get('users');
			return $query->num_rows();
		}
	}

	public function property_details($pr_id='')
	{
		$this->db->where('properties.id',$pr_id);
		$this->db->from('properties');
		$this->db->join('pr_info','properties.id = pr_info.pr_id');
		$query=$this->db->get();
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;		
	}


	public function posts($limit='',$offset=''){
		$this->db->select('posts.*,users.display_name as post_author');
		$this->db->from('posts');
		$this->db->join('users','users.ID=posts.post_author');
		$this->db->order_by('posts.ID','desc');		
		$this->db->where('posts.post_type','post');		
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function categories($limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('post_categories');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get('post_categories');
			return $query->num_rows();
		}
	}	

	public function blog($limit='',$offset=''){	
		$this->db->select('p.*,c.category_name');
		$this->db->from('posts as p');
		$this->db->join('post_categories as c', 'c.id = p.category_id');
		$this->db->order_by('p.id','desc');			
		$this->db->where('p.post_type', 1);
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function comments($limit='',$offset=''){	
		$this->db->select('c.*,p.post_title');
		$this->db->from('comments as c');
		$this->db->join('posts as p', 'p.id = c.post_id');
		$this->db->order_by('c.id','desc');					
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function contact_form($limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('contact_us');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get('contact_us');
			return $query->num_rows();
		}
	}

	public function video_posts($limit='',$offset=''){	
		$this->db->select('p.*,c.category_name');
		$this->db->from('posts as p');
		$this->db->join('categories as c', 'c.id = p.category_id','left');
		$this->db->order_by('p.id','desc');			
		$this->db->where('p.post_type', 2);
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function artist($limit='',$offset=''){	
		$this->db->order_by('id', 'desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('artist');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get('artist');
			return $query->num_rows();
		}
	}

	// ----------------

	public function themes($limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('themes');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get('themes');
			return $query->num_rows();
		}
	}

	public function add_theme($object){
		$this->db->insert('themes', $object);
	}


	/*faraz work*/

	public function get_pagination_where($table_name='', $limit='', $offset='', $condition='')
 	{ 
		if(!empty($condition)):  
			foreach ($condition as $key => $value){
			 	$this->db->where($key, $value);
			}
		endif;
		$this->db->order_by('id','desc');   
		if($limit > 0 && $offset>=0){
		$this->db->limit($limit, $offset);
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
		  return $query->result();
		else
		  return FALSE;
		}
		else{
		   $query=$this->db->get($table_name);
		   return $query->num_rows();
           }
 	}	


  public function get_owner_members($id="",$limit='',$offset=''){ 
  $this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
  $this->db->from('group_members');
  $this->db->join('users', 'users.id = group_members.member_id');
  $this->db->order_by('group_members.id','desc');     
  $this->db->where('group_members.group_id',$id);
  $this->db->where('group_members.member_role','3');
    if($limit > 0 && $offset>=0){
      $this->db->limit($limit, $offset);
      $query=$this->db->get();
      if($query->num_rows()>0)
        return $query->result();
      else
        return FALSE;
    }else{
      $query=$this->db->get();
      return $query->num_rows();
    }
  }

   public function get_customer_members($id="",$limit='',$offset=''){  
    $this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
    $this->db->from('group_members');
    $this->db->join('users', 'users.id = group_members.member_id');
    $this->db->order_by('group_members.id','desc');     
    $this->db->where('group_members.group_id',$id);
    $this->db->where('group_members.member_role','4');
      if($limit > 0 && $offset>=0){
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        if($query->num_rows()>0)
          return $query->result();
        else
          return FALSE;
      }else{
        $query=$this->db->get();
        return $query->num_rows();
      }
  }

   public function group_members_properties($group_id="",$limit='',$offset=''){  
	  $this->db->select('group_members_properties.*,users.first_name,users.last_name,users.user_email,properties.featured_image,properties.title');
	  $this->db->from('group_members_properties');
	  $this->db->join('users', 'users.id = group_members_properties.member_id');
	  $this->db->join('properties', 'properties.id = group_members_properties.property_id');
	  $this->db->order_by('group_members_properties.id','desc');    
	  $this->db->where('group_members_properties.group_id',$group_id);
     if($limit > 0 && $offset>=0){
	      $this->db->limit($limit, $offset);
	      $query=$this->db->get();
	      if($query->num_rows()>0)
	        return $query->result();
	      else
	        return FALSE;
     }else{
	      $query=$this->db->get();
	      return $query->num_rows();
      }
  }

	/*faraz work*/


	public function payments($limit="",$offset="",$sort="",$name="",$post=""){
		if($sort =="newest"){
			$this->db->order_by('p.id', 'desc');
		}elseif($sort =="oldest"){
			$this->db->order_by('p.id', 'asc');			
		}elseif($sort == 'refund'){
			$this->db->where('p.is_refund',1);
		}elseif($sort == 'paid_to_owner'){
			$this->db->where('p.paid_to_owner',1);
		}

		if($name!=""){
			$this->db->like('u.first_name',$name);
		}

		if(!empty($post['book_number'])){
			$this->db->where('booking_id',$post['book_number']);
		}

		if(!empty($post['guest_name'])){
			$this->db->like('guest_name',$post['guest_name'],'after');
		}

		if(!empty($post['host_name'])){
			$this->db->like('host_name',$post['host_name'],'after');
		}

		if(!empty($post['property_name'])){
			$this->db->like('property_name',$post['property_name']);
		}
		$this->db->where('p.is_delete !=',1);

		$this->db->select('p.*, u.first_name');
		$this->db->from('payments as p');
		$this->db->join('users as u', 'u.id = p.user_id', 'left');

		if($limit > 0 && $offset>=0){
	      $this->db->limit($limit, $offset);
	      $query=$this->db->get();
	      if($query->num_rows()>0)
	        return $query->result();
	      else
	        return FALSE;
	     }else{
		      $query=$this->db->get();
		      return $query->num_rows();
	      }
	}


	public function referral_payments($limit,$offset,$sort,$name){
		if($sort =="newest"){
			$this->db->order_by('i.id', 'desc');
		}elseif($sort =="oldest"){
			$this->db->order_by('i.id', 'asc');			
		// }elseif($sort == 'property_credit'){
		// 	$this->db->where('i.is_refund',1);
		// }elseif($sort == 'pending_payment'){
		// 	$this->db->where('i.paid_to_owner',1);
		}elseif($sort == 'joined'){
			$this->db->where('i.is_joined',1);
		}

		if($name!=""){
			$this->db->like('u.first_name',$name);
		}

		$this->db->select('i.*, u.first_name');
		$this->db->from('invitation as i');
		$this->db->join('users as u', 'u.id = i.user_id', 'left');

		if($limit > 0 && $offset>=0){
	      $this->db->limit($limit, $offset);
	      $query=$this->db->get();
	      if($query->num_rows()>0)
	        return $query->result();
	      else
	        return FALSE;
	     }else{
		      $query=$this->db->get();
		      return $query->num_rows();
	      }
	}


	public function transaction_history($limit,$offset,$sort,$name){
		if($sort =="newest"){
			$this->db->order_by('ph.id', 'desc');
		}elseif($sort =="oldest"){
			$this->db->order_by('ph.id', 'asc');			
		// }elseif($sort == 'property_credit'){
		// 	$this->db->where('ph.is_refund',1);
		// }elseif($sort == 'pending_payment'){
		// 	$this->db->where('ph.paid_to_owner',1);
		// }elseif($sort == 'joined'){
		// 	$this->db->where('ph.is_joined',1);
		}

		if($name!=""){
			$this->db->like('u.first_name',$name);
		}

		$this->db->select('ph.*, u.first_name');
		$this->db->from('payment_history as ph');
		$this->db->join('users as u', 'u.id = ph.recievebyuser', 'left');
		if($limit > 0 && $offset>=0){
	      $this->db->limit($limit, $offset);
	      $query=$this->db->get();
	      if($query->num_rows()>0)
	        return $query->result();
	      else
	        return FALSE;
	     }else{
		      $query=$this->db->get();
		      return $query->num_rows();
	      }
	}

	public function security_deposit($limit='', $offset='',$sort="",$condition='' )
 	{ 
		$this->db->where('security_deposit !=',0);
		$this->db->where($condition,1);

		if($sort =="newest"){
			$this->db->order_by('id', 'desc');
		}elseif($sort =="oldest"){
			$this->db->order_by('id', 'asc');	
		}		

		$this->db->order_by('id','desc');   
		if($limit > 0 && $offset>=0){
		$this->db->limit($limit, $offset);
		$query=$this->db->get('payments');
		if($query->num_rows()>0)
		  return $query->result();
		else
		  return FALSE;
		}
		else{
		   $query=$this->db->get('payments');
		   return $query->num_rows();
        }
 	}	

	public function make_event_available($start="", $end="",$property_id="")
	{
		$this->db->where('property_id',$property_id);
		@$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
		@$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
		return $query=$this->db->delete('pr_calender');
	}

	public function check_event_existance($start, $end, $listing_id)
	{
		$this->db->where('property_id', $listing_id);
		$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
		$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
		$query=$this->db->get('pr_calender');
		// return $query=$this->db->delete('pr_calender');
		if($query->num_rows()>0)
		{
			$groupids = array();
			$lastgroupid = 0;
			foreach ($query->result() as $v) {
				if ($lastgroupid != $v->groupid) {
					$groupids[] = $v->groupid;
					$lastgroupid = $v->groupid;
				}
			}
			if (!empty($groupids)) {
				$this->db->select('property_id, start, end, rangeopen, rangeclose, groupid, color, textColor, event_type, price, currency_type');
				$this->db->order_by('start', 'asc');
				$this->db->where('property_id', $listing_id);
				$this->db->where('rangeopen', '1');
				$this->db->or_where('rangeclose', '1');
				$query=$this->db->get('pr_calender');
				if($query->num_rows() > 0){
					$temp = array();
					$i=0;
					foreach ($query->result_array() as $key => $value) {
						if (in_array($value['groupid'], $groupids)) {
							$temp[$i] = $value;
							$i++;
							// unset($temp[$key]);
						}
					}
					return $temp;
				}
				else
					return FALSE;
			}
			return $query->result();
		}
		else
			return FALSE;
	}

	public function get_featured_images_result($table_name='', $limit='',$offset='',$post="")
	{	
		if(!empty($post['sort']))
		{
			if($post['sort']=="new_requests")
			{
     			$this->db->where('featured_request',1);
			}
			if($post['sort']=="publish")
			{
     			$this->db->where('is_featured_image',1);
			}
			if($post['sort']=="unpublish")
			{
     			$this->db->where('is_featured_image',0);
			}
		}
		
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get($table_name);
			return $query->num_rows();
		}
	}

}	