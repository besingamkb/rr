<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return  $this->db->insert_id();
		else
			return FALSE;		
	}

	public function get_result($table_name='', $id_array='',$id_array2=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($id_array2)):		
			foreach ($id_array2 as $key => $value){
				$this->db->or_where($key, $value);
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_row($table_name='', $id_array=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}

	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;		
		return $this->db->update($table_name, $data);
	}

	public function delete($table_name='', $id_array=''){		
		return $this->db->delete($table_name, $id_array);
	}

	
	public function search($s){
		$sql = "SELECT *\n"
			    . "FROM (`posts`)\n"
			    . "WHERE \n"
			    . "( `content` LIKE '%".$s."%'\n"
			    . "OR `post_title` LIKE '%".$s."%'\n"
			    . "OR `post_excerpt` LIKE '%".$s."%'\n"
			    . ")\n"
			    . "AND `post_type` = 2\n"
			    . "ORDER BY `id` desc ";

		/*$this->db->order_by('id', 'desc');
	 	$this->db->like('content', $s);
	 	$this->db->or_like('post_title', $s);
	 	$this->db->or_like('post_excerpt', $s);*/
	 	$query = $this->db->query($sql);
	 	if($query->num_rows() > 0){
	 		return $query->result();
	 	}
	 	else{
	 		return FALSE;
	 	}
	}

	public function get_post($id){
		$this->db->where('id', $id);
		$query = $this->db->get('posts');
		if($query->num_rows() > 0){
			return $query->row();
		}
		else{
			return FALSE;
		}
	}

	public function get_comments($id){
		$this->db->where('comment_approved', 1);
		$this->db->where('post_id', $id);
		$query = $this->db->get('comments');
		if($query->num_rows() > 0){
			return $query->result();
		}
		else{
			return FALSE;
		}
	}

	

	

	public function blog($limit, $offset, $category){
		if($category != "ALL"){			
		$this->db->where('category_id', $category);
		}		
		$this->db->where('post_status', 1);
		$this->db->where('post_type', 1);
		$this->db->order_by('id', 'desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get('posts');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{			
			$query=$this->db->get('posts');
			return $query->num_rows();
		}
	}

}/* End of file home_model.php */
