<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {	

	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return  $this->db->insert_id();
		else
			return FALSE;		
	}

	public function get_result($table_name='', $id_array='',$id_array2=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($id_array2)):		
			foreach ($id_array2 as $key => $value){
				$this->db->or_where($key, $value);
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_row($table_name='', $id_array=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}

	public function get_pagination_result($table_name='', $limit='',$offset=''){	
		$this->db->order_by('id','desc');			
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get($table_name);
			return $query->num_rows();
		}
	}


	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;		
		return $this->db->update($table_name, $data);
	}

	public function delete($table_name='', $id_array=''){		
		return $this->db->delete($table_name, $id_array);
	}

	
	/*
	* 
	*
	*/
	/*faraz work*/
	
		public function get_pagination_where($table_name='', $limit='', $offset='', $condition='',$slug="")
	 	{ 
			if(!empty($condition)):  
				foreach ($condition as $key => $value){
				 	$this->db->where($key, $value);
				}
			endif;
			if($slug=="asc"){
			$this->db->order_by('id','asc'); 
			}
			else{
			$this->db->order_by('id','desc'); 
			}

			if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
			}
			else{
			   $query=$this->db->get($table_name);
			   return $query->num_rows();
}
	 	}	


	/*faraz work*/

	public function activation($activation_key){
		//return $activation_key;
		
		$this->db->where('secret_key',$activation_key);
		$query=$this->db->get('users');

	 	// return $query->num_rows();
	 	// die();
		if($query->num_rows()>0){			
			if($query->row()->user_status== 0){							
				if($this->update('users',array('user_status'=>1,'secret_key'=>''),array('id'=>$query->row()->id))){
					$this->session->set_flashdata('msg_success_act_key','Dear '.$query->row()->first_name.', Your account activated.');
					return $query->row()->user_email.' '.$query->row()->password;
				}else{
					$this->session->set_flashdata('msg_error_act_key','Dear '.$query->row()->fname.', Your account activation failed.');
					return 2;
				}
			}else{
				$this->session->set_flashdata('msg_success_act_key','Dear '.$query->row()->fname.', Your account already activated.');
				return 3;
			}

		}else{
			$this->session->set_flashdata('msg_error_act_key',' Invalid account activation url.');
			return false;
		}
	}

	/*user Bookings*/
		
	public function user_booking($email='',$user_name=''){
		$this->db->where('guest_name', $user_name);
		$this->db->where('email', $email);
		$query = $this->db->get('bookings');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;	
	}

	/*user Bookings*/

	/*user message*/
	
	public function user_message($email='',$user_name=''){
		$this->db->where('user_name', $user_name);
		$this->db->where('user_email', $email);
		$query = $this->db->get('user_message');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;	
	}

	/*user message*/


	/*faraz work*/

	public function get_pagination_or_where($table_name='', $limit='', $offset='', $condition='')
 	{ 
		if(!empty($condition)):  
			foreach ($condition as $key => $value){
			 	$this->db->or_where($key, $value);
			}
		endif;
		$this->db->order_by('id','desc');   
		if($limit > 0 && $offset>=0){
		$this->db->limit($limit, $offset);
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
		  return $query->result();
		else
		  return FALSE;
		}
		else{
		   $query=$this->db->get($table_name);
		   return $query->num_rows();
			}
 	}	
		

	
    public function user_favorites_property($limit,$offset,$favorites_category_id){
    $this->db->select('favorites_property.*,properties.title,properties.featured_image,users.first_name,users.last_name,users.user_email');
    $this->db->from('favorites_property');
    $this->db->join('users','users.id = favorites_property.user_id','left');
    $this->db->join('properties','properties.id = favorites_property.property_id','left');
    $this->db->where('favorites_property.favorites_category_id',$favorites_category_id);
    $this->db->order_by('id','desc');
        if($limit > 0 && $offset>=0){
            $this->db->limit($limit, $offset);
            $query=$this->db->get();
            if($query->num_rows()>0)
            {
              return $query->result();
            }
            else
              return FALSE;
        }
        else{
           $query=$this->db->get();
           return $query->num_rows();
        }
    }

	public function user_review($limit,$offset){
    $user = $this->session->userdata('user_info');
    $id = $user['id'];
	$this->db->select('review.*,properties.title,users.first_name,users.last_name,users.user_email');
	$this->db->from('review');
	$this->db->join('users','users.id = review.customer_id','left');
	$this->db->join('properties','properties.id = review.property_id','left');
	$this->db->where('review.user_id',$id);
	$this->db->where('review.status',1);
	$this->db->order_by('id','desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
		}
		else{
		   $query=$this->db->get();
		   return $query->num_rows();
		}
	}

    public function get_customer_members($id="",$limit='',$offset=''){	
		$this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
		$this->db->from('group_members');
		$this->db->join('users', 'users.id = group_members.member_id');
		$this->db->order_by('group_members.id','desc');			
		$this->db->where('group_members.group_id',$id);
		$this->db->where('group_members.member_role','4');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get();
				return $query->num_rows();
			}
	}

	public function get_owner_members($id="",$limit='',$offset=''){	
		$this->db->select('group_members.*,users.first_name,users.last_name,users.user_email,users.image');
		$this->db->from('group_members');
		$this->db->join('users', 'users.id = group_members.member_id');
		$this->db->order_by('group_members.id','desc');			
		$this->db->where('group_members.group_id',$id);
		$this->db->where('group_members.member_role','3');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get();
				return $query->num_rows();
			}
	}

	public function group_members_properties($group_id="",$limit='',$offset='',$user_id=""){	
	$this->db->select('group_members_properties.*,users.first_name,users.last_name,users.user_email,properties.featured_image,properties.title,properties.description');
	$this->db->from('group_members_properties');
	$this->db->join('users', 'users.id = group_members_properties.member_id');
	$this->db->join('properties', 'properties.id = group_members_properties.property_id');
	$this->db->order_by('group_members_properties.id','desc');		
	$this->db->where('group_members_properties.group_id',$group_id);
	if(!empty($user_id)){
     	$this->db->where('group_members_properties.member_id',$user_id);
	}
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function get_invitation($limit='',$offset=''){	
		$user = $this->session->userdata('user_info');		
			$this->db->where('user_id', $user['id']);
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get('invitation');
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get('invitation');
				return $query->num_rows();
			}
	}

	/*Trips work*/
	/*Trips work*/
	public function get_current_trips($limit='',$offset=''){
	$user_info = $this->session->userdata('user_info');
	$id = $user_info['id'];
	$this->db->select('bookings.*,properties.featured_image,properties.title');
	$this->db->from('bookings');
	$this->db->join('properties', 'properties.id = bookings.pr_id');
	$this->db->order_by('bookings.id','desc');		
	$this->db->where('bookings.customer_id',$id);
	$this->db->where('bookings.check_in <=',date('m/d/Y'));
	$this->db->where('bookings.check_out >=',date('m/d/Y'));
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function get_previous_trips($limit='',$offset=''){
	$user_info = $this->session->userdata('user_info');
	$id = $user_info['id'];
	$this->db->select('bookings.*,properties.featured_image,properties.title');
	$this->db->from('bookings');
	$this->db->join('properties', 'properties.id = bookings.pr_id');
	$this->db->order_by('bookings.id','desc');		
	$this->db->where('bookings.customer_id',$id);
	$this->db->where('bookings.check_out <',date('m/d/Y'));
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function get_upcoming_trips($limit='',$offset=''){
	$user_info = $this->session->userdata('user_info');
	$id = $user_info['id'];
	$this->db->select('bookings.*,properties.featured_image,properties.title');
	$this->db->from('bookings');
	$this->db->join('properties', 'properties.id = bookings.pr_id');
	$this->db->order_by('bookings.id','desc');		
	$this->db->where('bookings.customer_id',$id);
	$this->db->where('bookings.check_in >',date('m/d/Y'));
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			$query=$this->db->get();
			return $query->num_rows();
		}
	}

	public function get_trips_export($booking_id = ""){
		$this->db->select('bookings.*,properties.title,properties.description');
		$this->db->from('bookings');
		$this->db->join('properties', 'properties.id = bookings.pr_id');
		$this->db->where('bookings.id',$booking_id);
	    $query=$this->db->get();
	    return $query->row();
	}

	/*Trips work*/
	/*Trips work*/

         /* Review Starts*/

	public function review_about_you($limit,$offset){
    $user = $this->session->userdata('user_info');
    $id = $user['id'];
	$this->db->select('review.*,properties.title,users.first_name,users.last_name,users.user_email');
	$this->db->from('review');
	$this->db->join('users','users.id = review.customer_id','left');
	$this->db->join('properties','properties.id = review.property_id','left');
	$this->db->where('review.user_id',$id);
	$this->db->where('review.status',1);
	$this->db->order_by('id','desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
		}
		else{
		   $query=$this->db->get();
		   return $query->num_rows();
		}
	}

	public function review_by_you($limit,$offset){
    $user = $this->session->userdata('user_info');
    $id = $user['id'];
	$this->db->select('review.*,properties.title,users.first_name,users.last_name');
	$this->db->from('review');
	$this->db->join('users','users.id = review.user_id','left');
	$this->db->join('properties','properties.id = review.property_id','left');
	$this->db->where('review.customer_id',$id);
	$this->db->where('review.status',1);
	$this->db->order_by('id','desc');
		if($limit > 0 && $offset>=0){
			$this->db->limit($limit, $offset);
			$query=$this->db->get();
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
		}
		else{
		   $query=$this->db->get();
		   return $query->num_rows();
		}
	}

   /* Review ends*/


   /*Calender Starts*/

   		public function getWeekDaysRanges($listingId='')
		{
			// $this->db->select('property_id, start, end, rangeopen, rangeclose, groupid, color, textColor, event_type, price, currency_type');
			$this->db->order_by('start', 'asc');
			$this->db->where('property_id', $listingId);
			$this->db->where('event_type', 2);
			// $this->db->where('rangeopen', '1');
			// $this->db->or_where('rangeclose', '1');
			$query=$this->db->get('pr_calender');
			if($query->num_rows() > 0){
				$groupids = array();
				$lastgroupid = 0;
				foreach ($query->result() as $v) {
					if ($lastgroupid != $v->groupid) {
						$groupids[] = $v->groupid;
						$lastgroupid = $v->groupid;
					}
				}
				if (!empty($groupids)) {
					// $this->db->select('property_id, start, end, rangeopen, rangeclose, groupid, color, textColor, event_type, price, currency_type');
					$this->db->order_by('start', 'asc');
					$this->db->where_in('groupid', $groupids);
					// $this->db->where('rangeopen', '1');
					// $this->db->or_where('rangeclose', '1');
					$query=$this->db->get('pr_calender');
					if($query->num_rows() > 0){
						$temp = array();
						$i=0;
						foreach ($query->result_array() as $key => $value) {
							if ((($value['rangeopen'] == 1) && ($value['rangeclose'] == 1)) || (($value['rangeopen'] == 1) && ($value['rangeclose'] == 0)) || (($value['rangeopen'] == 0) && ($value['rangeclose'] == 1))) {
								$temp[$i] = $value;
								$i++;
								// unset($temp[$key]);
							}
						}
						return $temp;
					}
					else
						return FALSE;
				}
				return FALSE;
			}
			else
				return FALSE;
		}

		public function check_event_existance($start, $end, $listing_id)
		{
			$this->db->where('property_id', $listing_id);
			$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
			$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
			$query=$this->db->get('pr_calender');
			// return $query=$this->db->delete('pr_calender');
			if($query->num_rows() > 0){
				$groupids = array();
				$lastgroupid = 0;
				foreach ($query->result() as $v) {
					if ($lastgroupid != $v->groupid) {
						$groupids[] = $v->groupid;
						$lastgroupid = $v->groupid;
					}
				}
				if (!empty($groupids)) {
						$this->db->select('property_id, start, end, rangeopen, rangeclose, groupid, color, textColor, event_type, price, currency_type');
						$this->db->order_by('start', 'asc');
						$this->db->where_in('groupid', $groupids);
						// $this->db->where('rangeopen', '1');
						// $this->db->or_where('rangeclose', '1');
						$query=$this->db->get('pr_calender');
						if($query->num_rows() > 0){
							$temp = array();
							$i=0;
							// print_r($groupids);
							// print_r($query->result_array());
							foreach ($query->result_array() as $key => $value) {
								if ((($value['rangeopen'] == 1) && ($value['rangeclose'] == 1)) || (($value['rangeopen'] == 1) && ($value['rangeclose'] == 0)) || (($value['rangeopen'] == 0) && ($value['rangeclose'] == 1))) {
									$temp[$i] = $value;
									$i++;
									// unset($temp[$key]);
								}
							}
							return $temp;
						}
						else
							return FALSE;
					}
					return FALSE;
				}
				else
					return FALSE;
		}


		public function get_events($table_name='', $id_array=array(), $order=array(), $month ,$year,$property_id=""){
            if(!empty($property_id))
            {
              $this->db->where('property_id',$property_id);
            }			
 
			if(!empty($id_array)):		
				foreach ($id_array as $key => $value){
					$this->db->where($key, $value);
				}
			endif;

			if (!empty($order)):
				$this->db->order_by($order['by'], $order['order']);
			else:
				$this->db->order_by('id', 'asc');
			endif;

			if (!empty($month) && !empty($year)):
				$this->db->like('start', $year.'-'.$month, 'after');
			else:
				$this->db->like('start', date("Y-m"), 'after');
			endif;

			$query=$this->db->get($table_name);
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}

		public function make_event_available($start="", $end="",$property_id="")
		{
			$this->db->where('property_id',$property_id);
			$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
			$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
			return $query=$this->db->delete('pr_calender');
		}

		public function get_booking_dates($start, $end,$property_id)
		{
			$this->db->where('start >= ', date('Y-m-d H:i:s', $start));
			$this->db->where('start <= ', date('Y-m-d H:i:s', $end));
			$this->db->where('event_type', 1);
			$this->db->where('property_id', $property_id);
			$query=$this->db->get('pr_calender');
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}
		
		public function property_details_for_profile($limit, $offset, $uid='')
		{
			$this->db->select('pr.id prop_id, pr.title, pr.featured_image, pr.application_fee, pr.currency_type, p_in.unit_street_address, p_in.property_type_id, p_in.accommodates, p_in.square_feet');
			$this->db->from('properties pr');
			$this->db->join('pr_info as p_in','pr.id = p_in.pr_id','left');
			$this->db->where('pr.user_id',$uid);
			$this->db->where('pr.status',1);
			// $this->db->where('review.status',1);
			$this->db->order_by('pr.id','desc');
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get();
				if($query->num_rows()>0)
				  return $query->result();
				else
				  return FALSE;
			}
			else{
			   $query=$this->db->get();
			   return $query->num_rows();
			}
		}
 


 /*Calender Ends*/

 		public function get_user_notification($limit='', $offset='', $email){	
			$this->db->order_by('id','desc');
			$this->db->where('email', $email);			
			if($limit > 0 && $offset>=0){
				$this->db->limit($limit, $offset);
				$query=$this->db->get('notifications');
				if($query->num_rows()>0)
					return $query->result();
				else
					return FALSE;
			}else{
				$query=$this->db->get('notifications');
				return $query->num_rows();
			}
		}

		public function get_my_groups($uid='')
   		{
   			$this->db->select('gm.id as relt_id, gm.member_id, g.*');
			$this->db->from('group_members as gm');
			$this->db->join('groups as g','gm.group_id = g.id');
			$this->db->where('gm.member_id',$uid);
			$this->db->order_by('gm.id','desc');
			$query=$this->db->get();
			if($query->num_rows()>0)
			  return $query->result();
			else
			  return FALSE;
   		}

	public function transaction_history($limit,$offset){
        $user = $this->session->userdata('user_info');
		$this->db->select('ph.*, u.first_name');
		$this->db->from('payment_history as ph');
		$this->db->join('users as u', 'u.id = ph.recievebyuser', 'left');
        $this->db->where('ph.recievebyuser',$user['id']);
		$this->db->order_by('ph.id','desc');
		if($limit > 0 && $offset>=0){
	      $this->db->limit($limit, $offset);
	      $query=$this->db->get();
	      if($query->num_rows()>0)
	        return $query->result();
	      else
	        return FALSE;
	     }else{
		      $query=$this->db->get();
		      return $query->num_rows();
	      }
	}

}	