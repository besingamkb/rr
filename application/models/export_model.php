<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_model extends CI_Model {	
	
	public function get_row($table_name='', $id_array=''){
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;

		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}



	public function export_orders($from_date='', $to_date='', $type=''){	
		if(strtolower($type)=='storeadmin'){
			$info = $this->session->userdata('storeadmin_info');
			$admin_id = $info['id'];
			$store_arr = get_store_id($admin_id);
		}
		$this->db->order_by('o.id', 'desc');		
		$this->db->select('o.*,oi.order_id as order_item_id ');		
		if(strtolower($type)=='storeadmin'){
		$stores_id=array();
			foreach ($store_arr as $row) {
				$stores_id[]=$row->id;
			}
		if(is_array($stores_id))
			$this->db->where_in('oi.store_id', $stores_id);		
		}
		$this->db->from('orders as o');
		$this->db->where("o.created BETWEEN '$from_date' AND '$to_date'");
		$this->db->join('order_items as oi', 'oi.order_id = o.id');
		$this->db->group_by('o.id');		
		$query=$this->db->get();					
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	public function export_properties($from_date='', $to_date='', $type='',$export_sort="")
	{
		$this->db->order_by('pr.id', 'desc');		
		$this->db->from('properties as pr');	
		$this->db->select('pr.title,pr.description,pr.created,pri.image');
		if($from_date!="" && $to_date!=''){
		  $this->db->where("pr.created BETWEEN '$from_date' AND '$to_date'");
		}
		if($export_sort =='Featured_Listings'){
			$this->db->where('pr.featured_listing_status',1);
		}
		if($export_sort =='In-Active_Listings'){
			$this->db->where('pr.status',0);
		}
		if($export_sort =='Active_Listings'){
			$this->db->where('pr.status',1);
		}
		if($export_sort =='new_requests'){
			$this->db->order_by('pr.created', 'desc');
		}
		$this->db->join('pr_info as pri', 'pri.pr_id = pr.id');
		
		$query=$this->db->get();

		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;

	}
	public function export_bookings($from_date='', $to_date='', $type='',$export_sort='')
	{
		$this->db->order_by('id', 'desc');
		if($from_date!="" && $to_date!=''){
		$this->db->where("created BETWEEN '$from_date' AND '$to_date'");
		}
		if($export_sort =='Cancelled_Bookings'){
			$this->db->where('status', '2');
		}
		if($export_sort =='Pending_Bookings'){
			$this->db->where('status', '0');
		}

       $query=$this->db->get('bookings');
       
        if($export_sort=="Pending_Check-in"){
			foreach ($query->result() as $row) {
				$check_in_time = strtotime($row->check_in);
				$today_time = strtotime(date('Y-m-d'));
				if($check_in_time <= $today_time){
					$responce = $this->db->or_where('check_in',$row->check_in);
					$responce = $this->db->where('status',0);
			    }
			}

				if(@$responce!=""){
					$responce = $resu=$this->db->get('bookings');
					return $responce->result();
				}
				else{
					return false;
				}	
		}

		if($export_sort=="Pending_Check-out"){
			foreach ($query->result() as $row) {
				$check_out_time = strtotime($row->check_out);
				$today_time = strtotime(date('Y-m-d'));
				if($check_out_time <= $today_time){
					$result = $this->db->or_where('check_out',$row->check_out);
					$result = $this->db->where('status','0');
				}
		    }
			 	if(@$result!=""){
					$result=$this->db->get('bookings');
					return $result->result();
				}
				else{
                 	return false;
                }
        }



		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;

	}


	public function export_members($from_date='', $to_date='', $type='',$export_sort='')
	{
		$this->db->order_by('id', 'desc');		
		$this->db->where('user_role',3);

		if($from_date!=''&& $to_date!=''){
		  $this->db->where("created BETWEEN '$from_date' AND '$to_date'");
        }
        if($export_sort=="Active_users"){
		  $this->db->where('user_status',1);
        }	
        if($export_sort=="Ban_users"){
		  $this->db->where('user_status',0);
        }

		
		$query=$this->db->get('users');

		if($query->num_rows()>0){
         	return $query->result();
         }
		else
			return FALSE;

	}


	public function export_payments($from_date='', $to_date='', $type='',$export_sort='')
	{

		if($from_date!=''&& $to_date!=''){
		  $this->db->where("created BETWEEN '$from_date' AND '$to_date'");
        }
        if($export_sort=="newest"){
	    	$this->db->order_by('created', 'desc');		
        }	
        if($export_sort=="oldest"){
    		$this->db->order_by('created', 'asc');		
        }
        if($export_sort=="paid_to_owner"){
    		$this->db->where('paid_to_owner', 1);		
        }
        if($export_sort=="refund"){
    		$this->db->where('is_refund', 1);		
        }

		$query=$this->db->get('payments');

		if($query->num_rows()>0){
         	return $query->result();
         }
		else
			return FALSE;

	}

	public function export_events($from_date, $to_date,$type,$export_sort){
		if($from_date!=''&& $to_date!=''){
		  $this->db->where("created BETWEEN '$from_date' AND '$to_date'");
        }
        if($export_sort=="newest"){
	    	$this->db->order_by('created', 'desc');		
        }	
        if($export_sort=="oldest"){
    		$this->db->order_by('created', 'asc');		
        }
		$this->db->select('admin_events.*');
		$this->db->from('admin_events');
	      $query=$this->db->get();
          return $query->result();
    }

	public function export_security_deposit($from_date,$type,$to_date,$export_sort){
		if($from_date!=''&& $to_date!=''){
		  $this->db->where("created BETWEEN '$from_date' AND '$to_date'");
        }
        if($export_sort=="newest"){
	    	$this->db->order_by('created', 'desc');		
        }	
        if($export_sort=="oldest"){
    		$this->db->order_by('created', 'asc');		
        }
        if($type="deposit")
        {
    		$this->db->where('is_deposit_refund !=',1);		
        }
        if($type="deposit_history")
        {
    		$this->db->where('is_deposit_refund',1);		
        }
	      $query=$this->db->get('payments');
          return $query->result();
    }



}/*end of file*/