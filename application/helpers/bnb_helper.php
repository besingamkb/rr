<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*faraz work*/
function new_reply_for_sender($message_id="")
{
    $CI =& get_instance();
    $CI->db->where('message_id', $message_id);
    $CI->db->where('readByMsgSender', 0);
    $query = $CI->db->get('user_message_reply');
    return $query->num_rows();
}

function new_reply_for_receiver($message_id="")
{
    $CI =& get_instance();
    $CI->db->where('message_id', $message_id);
    $CI->db->where('readByMsgReceiver', 0);
    $query = $CI->db->get('user_message_reply');
    return $query->num_rows();
}

function new_reply_for_admin($message_id="")
{
    $CI =& get_instance();
    $CI->db->where('message_id', $message_id);
    $CI->db->where('readByAdmin', 0);
    $query = $CI->db->get('user_message_reply');
    return $query->num_rows();
}

function new_reply_for_subadmin($message_id="")
{
    $CI =& get_instance();
    $CI->db->where('message_id', $message_id);
    $CI->db->where('readBySubadmin', 0);
    $query = $CI->db->get('user_message_reply');
    return $query->num_rows();
}

function total_reply_count($message_id="")
{
    $CI =& get_instance();
    $user = $CI->session->userdata('user_info');
    $CI->db->where('message_id', $message_id);
    $query = $CI->db->get('user_message_reply');
    return $query->num_rows();
}
/*faraz work*/



function eventJSONarray()
{
    $CI =& get_instance();
    $query = $CI->db->get('admin_events');
    if($query->num_rows() > 0)
    {
        $arr = array();
        $i = 0;
        foreach ($query->result() as $row)
        {
            if($row->isImportant == 1)
            {
                $color = '#D13736';
            }
            else
            {
                $color = '#444444';
            }    
            $arr[$i] = array(
                'title' => $row->title,
                'start' => $row->startDate.' '.date('H:i:s',strtotime($row->startTime)),
                'end'   => $row->endDate.' '.date('H:i:s',strtotime($row->endTime)),
                'color' => $color,
                'allDay'=> false,
                'id'    => $row->id
                );
            $i++;
        } 
        return json_encode($arr);
    }
    else
    {
        return false;
    }    
}


/* get_ review reply of a particular Review */
if (!function_exists('get_review_reply'))
{
  function get_review_reply($review_id)
  {        
    $CI =& get_instance();
    $CI->db->select('r.*, u.first_name, u.image');
    $CI->db->from('review_reply as r','left');
    $CI->db->join('users as u','r.replier_id = u.id' ,'left');
    $CI->db->where('r.review_id',$review_id);
    $CI->db->order_by('id' , 'desc');
    $resukt = $CI->db->get(); 

    return $resukt->result();
  }
}
/* get_ review reply of a particular Review */



  if ( ! function_exists('get_map_bookings')) {  
    function get_map_bookings($pr_id="") {
      $CI =& get_instance();
      $CI->db->where('pr_id',$pr_id);
      $CI->db->where('status',1);
      $query = $CI->db->get('bookings');
      $result = $query->result();
      $array[date('F')] = 0;
      $array[date('F',strtotime('+ 1 month'))] = 0;
      $array[date('F',strtotime('+ 2 month'))] = 0;

        foreach ($result as $row)
        {
          $check_in_time = strtotime($row->check_in);
          $date = getdate($check_in_time);
          if(date('m')==$date['mon'])
          {
             $array[date('F')] ++;
          }
          if(date('m',strtotime('+ 1 month'))==$date['mon'])
          {
             $array[date('F',strtotime('+ 1 month'))] ++;
          }
          if(date('m',strtotime('+ 2 month'))==$date['mon'])
          {
             $array[date('F',strtotime('+ 2 month'))] ++;
          }
        }
       return $array;
    }
  }




  if ( ! function_exists('get_after_discount_price')) {  
    function get_after_discount_price($amount,$discount) {
     $a = $amount*$discount;
     $b = $a/100;
     return $amount-$b; 
    }
  }


  
  /*Get  latitude and longitude */

  function get_lat_long($city="")
  {

      if($city=="")
        return false;

    $location = urlencode($city);
    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$location&sensor=false");
    $json = json_decode($json);
    if($json->status=="OK")
    {
      $latitude_longitude = $json->results[0]->geometry->location;
      $lat_long_array['lat'] =  $latitude_longitude->lat;
      $lat_long_array['lng'] =  $latitude_longitude->lng;
    }
    else
    {
      $lat_long_array['lat'] = 0;
      $lat_long_array['lng'] = 0;
    }
        return $lat_long_array;
  }


  /*Get  latitude and longitude */


  if ( ! function_exists('get_manage_email_info')) {  
    function get_manage_email_info($name="") {
      $CI =& get_instance();
      $CI->db->where('name',$name);
      $query = $CI->db->get('manage_email');
      return $query->row();
    }
  }

if ( ! function_exists('get_no_of_favorites_in_a_category')) {  
  function get_no_of_favorites_in_a_category($cat_id='')
  {
    $CI =& get_instance();
    $CI->db->where('favorites_category_id',$cat_id);
    $query = $CI->db->get('favorites_property');
    if($query->num_rows() > 0)
    
    {
      if($query->num_rows()==1)
      {
         return $query->num_rows()."  Favorite";
      }
      else
      {
         return $query->num_rows()."  Favorites";
      }
    }   
    else{
      return "0 Favorite" ;
    }
  }
}

if ( ! function_exists('get_no_of_favoritesCategory')) {  
  function get_no_of_favoritesCategory($flag="")
  {
    $CI =& get_instance();
    $CI->db->where('user_id',get_my_id());
    $query = $CI->db->get('favorites_category');
    if($query->num_rows() > 0)
    {
      if($flag=="result")
      {
        return $query->result();
      }
      return $query->num_rows();
    }   
    else{
      return 0;
    }
  }
}





if ( ! function_exists('get_commision_fee')) {  
  function get_commision_fee() {
    $CI =& get_instance();
    $CI->db->where('id',1);
    $query = $CI->db->get('commision_fee');   
    return $query->row();
  }
}

if ( ! function_exists('get_taxs')) {  
  function get_taxs() {
    $CI =& get_instance();
    $CI->db->where('id',2);
    $query = $CI->db->get('taxs');   
    return $query->row();
  }
}



if ( ! function_exists('get_all_users')) {  
  function get_all_users() {
    $CI =& get_instance();
    $CI->db->where('user_role !=',1);
    $CI->db->where('user_status',1);
    $query = $CI->db->get('users');   
    return $query->result();
  }
}




/*Check Booking date availability Starts*/
if ( ! function_exists('check_booking_date_availability')) 
{
  function check_booking_date_availability($pr_id="",$check_in_time="",$check_out_time="")
  {
      $check_in = date('Y-m-d',$check_in_time);
      $check_out = date('Y-m-d',$check_out_time);

      $CI =& get_instance();    
      $CI->db->select('*');
      $CI->db->from('pr_calender');
      $CI->db->where('property_id',$pr_id);
      $CI->db->where('start >=', $check_in);
      $CI->db->where('start <=', $check_out);
      $query = $CI->db->get();
      $result = $query->result();
       
       if(!empty($result))
       {
         foreach ($result as $row) 
         {
            if($row->event_type == 0 || $row->event_type == 1)
            {
              return false;
            }
         }
         return true;
       }
       else
       {
         return true;
       }
  } 

}
/*Check Booking date availability Ends*/



/* Check neighbourhoot of property */
if ( ! function_exists('check_neighbourhoot_of_property')) 
{
  function check_neighbourhoot_of_property($pr_id="",$nb_id="")
  {
    $CI =& get_instance();    
    $CI->db->select('id');
    $CI->db->from('pr_nb_relation');
    $CI->db->where('pr_nb_id', $pr_id);
    $CI->db->where('parent_id', 0);
    $parent = $CI->db->get();
    $p = $parent->row();
    if(empty($p))
    {
      return false;
    }
    $parent_id = $p->id;
    $CI->db->select('*');
    $CI->db->from('pr_nb_relation');
    $CI->db->where('parent_id', $parent_id );
    $CI->db->where('pr_nb_id', $nb_id );
    $query = $CI->db->get();
    if($query->num_rows()>0)
    {
      return $query->row(); 
    }
    else
    {
      return false;
    }
  }
}
/* Check neighbourhoot of property */




/*Get featured Images Starts */

if(!function_exists('get_featured_images')){
  function get_featured_images(){
    $CI =& get_instance(); 
    $CI->db->where('is_featured_image',1);
    $query = $CI->db->get('pr_gallery');
    if($query->num_rows > 0)
    {
      $temp_result =  $query->result();


        foreach ($temp_result as $row)
        { 
          if($row->subscription_type == "daily")
          {
            $daily_date = strtotime($row->subscription_date);
            $current_date = strtotime(date('d F Y'));
            if($daily_date == $current_date)
            {
              $act_result[] = $row;
            }
            else
            {
              $CI->db->where('id',$row->id);
              $update_data = array(
                                  'is_featured_image'=>0,
                                  'subscription_date'=>"",
                                  'subscription_type'=>"",
                                  'featured_request' =>0,
                                 );
              $CI->db->update('pr_gallery',$update_data);
            }

          }
          if($row->subscription_type == "weekly")
          {
            $daily_date = strtotime($row->subscription_date);
            $current_date = strtotime(date('d F Y'));
            $difference = $current_date-$daily_date;
            $days = $difference/86400; 
            if($days<=7)
            {
              $act_result[] = $row;
            }
            else
            {
              $CI->db->where('id',$row->id);
              $update_data = array(
                                  'is_featured_image'=>0,
                                  'subscription_date'=>"",
                                  'subscription_type'=>"",
                                  'featured_request' =>0,
                                 );
              $CI->db->update('pr_gallery',$update_data);
            }

          }
          if($row->subscription_type == "monthly")
          {
            $daily_date = strtotime($row->subscription_date);
            $current_date = strtotime(date('d F Y'));
            $difference = $current_date-$daily_date;
            $days = $difference/86400; 
            if($days<=30)
            {
              $act_result[] = $row;
            }
            else
            {
              $CI->db->where('id',$row->id);
              $update_data = array(
                                  'is_featured_image'=>0,
                                  'subscription_date'=>"",
                                  'subscription_type'=>"",
                                  'featured_request' =>0,
                                 );
              $CI->db->update('pr_gallery',$update_data);
            }

          }
        }

       
       return $act_result;

    }
    else{
      return FALSE;
    }
  }
}

/*Get featured Images Ends */


if(!function_exists('get_no_of_booking_alert')){
  function get_no_of_booking_alert(){
    $user_id = get_my_id();
    if($user_id == "")
      return FALSE;
    $CI =& get_instance(); 
    $CI->db->where('owner_id', $user_id);
    $CI->db->where('alert_status !=',1);
    $CI->db->where('is_delete_by_owner !=',1);
    $query = $CI->db->get('bookings');
    if($query->num_rows > 0){
      return $query->num_rows();
    }else{
      return FALSE;
    }
  }
}


if(!function_exists('get_no_of_property_images')){
  function get_no_of_property_images($property_id=""){
    if($property_id == "")
      return FALSE;
    $CI =& get_instance(); 

    $CI->db->where('pr_id', $property_id);
    $query = $CI->db->get('pr_gallery');
    if($query->num_rows > 0){
      return $query->num_rows();
    }else{
      return FALSE;
    }
  }
}






/*setting page helper starts*/

  if ( ! function_exists('get_payment_gateways_info')) {  
    function get_payment_gateways_info($name="") {
      $CI =& get_instance();
      $CI->db->where('name',$name);
      $query = $CI->db->get('payment_gateways');
      return $query->row();
    }
  }

  if ( ! function_exists('get_twillio_info')) {  
    function get_twillio_info() {
      $CI =& get_instance();
      $CI->db->where('id',1);
      $query = $CI->db->get('twillio');
      return $query->row();
    }
  }

  if ( ! function_exists('get_jumio_info')) {  
    function get_jumio_info() {
      $CI =& get_instance();
      $CI->db->where('id',1);
      $query = $CI->db->get('jumio');
      return $query->row();
    }
  }

/* setting page helper Ends */



/*Wasim sir task starts*/

  if ( ! function_exists('get_my_id')) {  
    function get_my_id() {
      $CI =& get_instance();
      $user_info=$CI->session->userdata('user_info');
      return $user_info['id'];
    }
  }


    if(!function_exists('currency_array')){
        function currency_array(){
            $currencies  = array(
                                  "AUD" => "AUD",
                                  "BRL" => "BRL",
                                  "CAD" => "CAD",
                                  "CHF" => "CHF",
                                  "CZK" => "CZK",
                                  "DKK" => "DKK",
                                  "EUR" => "EUR",
                                  "HKD" => "HKD",
                                  "HUF" => "HUF",
                                  "JPY" => "JPY",
                                  "MYR" => "MYR",
                                  "MXN" => "MXN",
                                  "NOK" => "NOK",
                                  "NZD" => "NZD",
                                  "PLN" => "PLN",
                                  "RUB" => "RUB",
                                  "SGD" => "SGD",
                                  "SEK" => "SEK",
                                  "USD" => "USD"
                            );
            return $currencies;
        }
    }

    if(!function_exists('get_recomended_price')){
      function get_recomended_price($for, $nightly=""){
        if (!empty($nightly) || $nightly != 0) {
          if ($for == 'weekly')
            return (7 * $nightly);
          else
            return (30 * $nightly);
        }else
            return FALSE;
      }
    }

/*Wasim sir task Ends*/


/*manage_listing_steps_count Starts*/

if ( ! function_exists('manage_listing_steps_count')) {  
  function manage_listing_steps_count($pr_id='') {
    $CI =& get_instance();
    $CI->db->where('properties.id',$pr_id);
    $CI->db->from('properties');
    $CI->db->join('pr_info', 'properties.id = pr_info.pr_id');
    $query = $CI->db->get();   
    $result = $query->row();
    $i=0;
      if(str_word_count($result->description)<15)
      {
         $i++;
      }
      if(empty($result->featured_image))
      {
        $i++;
      }
      if(empty($result->update_calender))
      {
         $i++;
      }
      if($i>0)
      {
       return $i;
      }
  }
}
/*manage_listing_steps_count Ends*/



if (!function_exists('get_property_type')){
  function get_property_type($property_type_id=""){        
    $CI =& get_instance();
     $CI->db->where('id',$property_type_id);
    $query = $CI->db->get('property_types');
    return $query->row()->property_type;
  }
}



/*Get faqs category  Starts*/
if ( ! function_exists('get_faqs_category')) {  
  function get_faqs_category($id=""){
    $CI =& get_instance();
    $CI->db->where('id',$id);
    $query = $CI->db->get('faqs_category');
    return $query->row();
  }
}

/*Get faqs category  Ends*/

/*Get All faqs categories  Starts*/
if ( ! function_exists('get_all_faqs_categories')) {  
  function get_all_faqs_categories(){
    $CI =& get_instance();
    $CI->db->where('status',1);
    $query = $CI->db->get('faqs_category');
    return $query->result();
  }
}

/*Get All faqs category  Ends*/


/*Get Social sign up/login   Starts*/
if ( ! function_exists('get_oauth_keys')) {  
  function get_oauth_keys($name=""){
    $CI =& get_instance();
    $CI->db->where('name',$name);
    $query = $CI->db->get('social_media_keys');
    return $query->row();
  }
}

/*Get Social sign up/login   Ends*/


if ( ! function_exists('get_property_contact_info')) {  
  function get_property_contact_info($pr_id='') {
    $CI =& get_instance();
    $CI->db->where('pr_id',$pr_id);
    $CI->db->from('pr_contact_info');
    $query = $CI->db->get();   
    return $query->row();
  }
}



/*Get buttons colour Starts*/
if ( ! function_exists('get_buttons_color')) {  
  function get_buttons_color(){
    $CI =& get_instance();
    $query = $CI->db->get('site_details');
    return $query->row();
  }
}

/*Get buttons colour Ends*/

/* currency converter */
function convertCurrency($amount, $to_Currency,  $from_Currency='USD'  )
{
  if($from_Currency==$to_Currency)
  {
    return round($amount,2);
  }

  
  $amount = urlencode($amount);
  $from_Currency = urlencode($from_Currency);
  $to_Currency = urlencode($to_Currency);

  $url = "www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

  $ch = curl_init();
  $timeout = 0;
  curl_setopt ($ch, CURLOPT_URL, $url);
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

  curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
  curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  $rawdata = curl_exec($ch);
  curl_close($ch);
  $data = explode('bld>', $rawdata);
  $data = explode($to_Currency, $data[1]);
  return round($data[0], 2);
}
/* currency converter */


/* get neighbourhood categories wrt id */
if(!function_exists('get_nb_cat'))
{
    function get_nb_cat($id)
    {
        $CI =& get_instance();
        $CI->db->where('neighbour', $id);
        $query = $CI->db->get('neighbour_cat');
        $rows  = $query->result();
        if(!empty($rows))
        {
            $CI->db->select('*');
            $CI->db->from('neb_category');
            foreach($rows as $row)
            {
                $CI->db->or_where('id',$row->category);
            }
            $res = $CI->db->get();
            return $res->result();
        }
        else
        {
            return false;
        }    
    }
}
/* get neighbourhood categories wrt id */





if ( ! function_exists('check_member_in_the_Group')) {  
  function check_member_in_the_Group($group_id="",$member_id=""){
    $CI =& get_instance();
    $CI->db->where('member_id',$member_id);
    $CI->db->where('group_id',$group_id);
    $query = $CI->db->get('group_members');
    return $query->num_rows();
  }
}

if ( ! function_exists('check_property_in_the_Group')) {  
  function check_property_in_the_Group($group_id="",$member_id="",$property_id=""){
    $CI =& get_instance();
    $CI->db->where('member_id',$member_id);
    $CI->db->where('property_id',$property_id);
    $CI->db->where('group_id',$group_id);
    $query = $CI->db->get('group_members_properties');
    return $query->num_rows();
  }
}



/*get number of groups and info */

if ( ! function_exists('get_number_of_groups')) {  
  function get_number_of_groups(){
    $CI =& get_instance();
    $query = $CI->db->get('groups');   
    return $query->num_rows();
  }
}

if ( ! function_exists('get_group_info')) {  
  function get_group_info($group_id=""){
    $CI =& get_instance();
    $CI->db->where('id',$group_id);
    $query = $CI->db->get('groups');   
    return $query->row();
  }
}

/*get number of groups and info */


/* get_property_of_neighbourhood */
if ( ! function_exists('get_property_of_neighbourhood')) 
{
  function get_property_of_neighbourhood($nb_id="", $flag="")
  {

    $CI  =& get_instance();    
    $CI->db->where('pr_nb_id', $nb_id);
    $CI->db->where('parent_id >', 0);
    $rel = $CI->db->get('pr_nb_relation');
   
    $count_rows = 0;
    $final_result = "";


      if($rel->result())
      {
          foreach ($rel->result() as $row)
          {
              $row_1 = get_row('pr_nb_relation',array('id'=>$row->parent_id));
              $row_2 = get_row('properties',array('id'=>$row_1->pr_nb_id,'status'=>1,'is_delete !='=>1));
              if(!empty($row_2))
              {
                  $count_rows++;
                  $final_result[] = $row_2;
              }
          }
      }


           if($flag==1)
           {
              if($count_rows>1)
              {
                 return $count_rows." PLACES";
              }
              else
              {
                 return $count_rows." PLACE";
              }
           }

           else
           {
            return $final_result;
           }

  }
}



/* get_featured_nb_of_city */
if ( ! function_exists('get_featured_nb_of_city')) 
{
  function get_featured_nb_of_city($city_id="", $flag="")
  {
    $CI =& get_instance();    
    $CI->db->where('city', $city_id);
    $CI->db->where('is_featured', 1);
    $query = $CI->db->get('neighbourhoods');
    if($flag == 1)
    {
      if($query->num_rows()>1)
      {
        return $query->num_rows()." neighbourhoods";
      }
      else
      {
        return $query->num_rows()." neighbourhood";
      }
    }
    else
    {
      if($query->num_rows() > 0){ return $query->result(); }
      else{ return false; }
    } 
  }
}
/* get_featured_nb_of_city */


/* get_pr_count_of_city */
/* get_pr_count_of_city */
if ( ! function_exists('get_pr_count_of_city')) 
{
  function get_pr_count_of_city($city="")
  {
    $CI =& get_instance();    
    $CI->db->select('properties.*');
    $CI->db->from('properties');
    $CI->db->Join('pr_info','properties.id = pr_info.pr_id','left');
    $CI->db->where('pr_info.city', $city);
    $CI->db->where('properties.status',1);
    $CI->db->where('properties.is_delete !=',1);
    $query = $CI->db->get();
    $rows = $query->num_rows();
    if(!empty($rows))
    {
      return $rows; 
    }
    else
    {
      return 0;
    }
  }
}
/* get_pr_count_of_city */

/* get_pr_count_of_city */


/* get neighbourhoot of property */
if ( ! function_exists('get_neighbourhoot_of_property')) 
{
  function get_neighbourhoot_of_property($pr_id="", $flag="")
  {
    $CI =& get_instance();    
    $CI->db->select('id');
    $CI->db->from('pr_nb_relation');
    $CI->db->where('pr_nb_id', $pr_id);
    $CI->db->where('parent_id', 0);
    $parent = $CI->db->get();
    $p = $parent->row();
    if(empty($p))
    {
      return false;
    }
    $parent_id = $p->id;

    $CI->db->select('*');
    $CI->db->from('pr_nb_relation');
    $CI->db->where('parent_id', $parent_id );
    $query = $CI->db->get();
    
    if($flag == 1)
    {
      if($query->num_rows()>1)
      {
        return $query->num_rows()." neighbourhoods";
      }
      else
      {
        return $query->num_rows()." neighbourhood";
      }
    }
    else
    {
      if($query->num_rows() > 0){ return $query->result(); }
      else{ return false; }
    } 
  }
}
/* get neighbourhoot of property */



/* get neighbourhoot of city */
if ( ! function_exists('get_neighbourhoot_of_city')) 
{
  function get_neighbourhoot_of_city($city_id="", $flag="")
  {
    $CI =& get_instance();    
    $CI->db->where('city', $city_id);
    $query = $CI->db->get('neighbourhoods');
    if($flag == 1)
    {
      if($query->num_rows()>1)
      {
        return $query->num_rows()." neighbourhoods";
      }
      else
      {
        return $query->num_rows()." neighbourhood";
      }
    }
    else
    {
      if($query->num_rows() > 0){ return $query->result(); }
      else{ return false; }
    } 
  }
}
/* get neighbourhoot of city */



/* get city info  */
if ( ! function_exists('get_city_info')) 
{
  function get_city_info($city_id)
  {
    $CI =& get_instance();    
    $CI->db->where('id', $city_id);
    $query = $CI->db->get('cities');
    if($query->num_rows() == 1)  
      return $query->row();
    else
       return false;
   }
}
/* get city info  */

/* get all cities info  */
if ( ! function_exists('get_cities_info')) 
{
  function get_cities_info()
  {
    $CI =& get_instance();    
    $CI->db->where('is_home',1);
    $query = $CI->db->get('cities');
    if($query->num_rows() > 0)  
      return $query->result();
    else
       return false;
   }
}
/* get all cities info  */




/* get no of members and no of property on groups*/

if ( ! function_exists('get_no_of_customer_from_group')) {  
  function get_no_of_customer_from_group($group_id="",$flag=""){
    $CI =& get_instance();
    $CI->db->where('member_role','4');
    $CI->db->where('group_id',$group_id);
    $query = $CI->db->get('group_members');
    return $query->num_rows();
  }
}

if ( ! function_exists('get_no_of_owner_from_group')) {  
  function get_no_of_owner_from_group($group_id="",$flag=""){
    $CI =& get_instance();
    $CI->db->where('member_role','3');
    $CI->db->where('group_id',$group_id);
    $query = $CI->db->get('group_members');
    return $query->num_rows();
  }
}

if ( ! function_exists('get_customers')) {  
  function get_customers() {
    $CI =& get_instance();
    $CI->db->where('user_role',4);
    $query = $CI->db->get('users');   
    return $query->result();
  }
}

if ( ! function_exists('get_users')) {  
  function get_users() {
    $CI =& get_instance();
    $CI->db->where('user_role',3);
    $query = $CI->db->get('users');   
    return $query->result();
  }
}

if ( ! function_exists('get_members_of_group')) {  
  function get_members_of_group($group_id='',$role="")
  {
    $CI =& get_instance();
    $CI->db->where('group_id',$group_id);
    if($role=="customer"){
      $CI->db->where('member_role','4');
    }
    if($role=="owner"){
      $CI->db->where('member_role','3');
    }
    $query = $CI->db->get('group_members');
    $result = $query->result();
    if(!empty($result)){
      foreach($result as $value){
      $CI->db->or_where('id',$value->member_id);
      }
    $query = $CI->db->get('users');
    return $query->result();
    }
    else
      return FALSE; 
  }
}

if ( ! function_exists('get_no_of_members')) {  
  function get_no_of_members($group_id='')
  {
    $CI =& get_instance();
    $CI->db->where('group_id',$group_id);
    $query = $CI->db->get('group_members');
    if($query->num_rows() > 0){
     return $query->num_rows();
    }   
    else{
      return 0 ;
    }
  }
}

if ( ! function_exists('get_no_of_properties_from_group')) {  
  function get_no_of_properties_from_group($group_id='')
  {
    $CI =& get_instance();
    $CI->db->where('group_id',$group_id);
    $query = $CI->db->get('group_members_properties');
    if($query->num_rows() >= 1){
     return $query->num_rows();
    }   
    else{
      return 0 ;
    }
  }
}
/*For similar listing and near By*/

/*For similar listing and near By*/

if ( ! function_exists('get_similar_listing')) {  
  function get_similar_listing($data="",$current_property_id="")
  {
    $CI =& get_instance();
    $CI->db->select('pr_info.*,properties.*');
    $CI->db->from('properties');
    $CI->db->join('pr_info','pr_info.pr_id=properties.id','left');
    $CI->db->where('properties.status !=',0);
    
    
    // $CI->db->where('pr_info.bed_type',$data['bed_type']);
    $CI->db->where('pr_info.bed',$data['bedrooms']);
    // $CI->db->where('pr_info.bath',$data['bedrooms']);
    $CI->db->where('properties.id !=',$current_property_id);
    $query = $CI->db->get();
    return $query->result();      
  }
}



if ( ! function_exists('get_similar_listing_user_info')) {  
  function get_similar_listing_user_info($pr_id="")
  {
    $CI =& get_instance();
    $CI->db->select('users.image,properties.title');
    $CI->db->from('properties');
    $CI->db->join('users','users.id = properties.user_id');
    $CI->db->where('properties.id',$pr_id);
    $query = $CI->db->get();
    return $query->result_array();      
  }
}

if ( ! function_exists('get_distance_near_by')) {  
  function get_distance_near_by($pr_id='')
  {
    $CI =& get_instance();
    $CI->db->where('id',$pr_id);
    $query = $CI->db->get('properties');   
    $row = $query->row();
    $property_latitude  = $row->latitude;
    $property_longitude = $row->longitude;
    $CI->db->where('id !=',$pr_id);
    $CI->db->where('status',1);
    $query = $CI->db->get('properties');   
     $i=0;
     foreach ($query->result() as $value)
     {
        if($value->latitude!="0")
        {
            $difference_longitude = $value->longitude - $property_longitude;
            $dist = sin(deg2rad($value->latitude)) * sin(deg2rad($property_latitude)) +  cos(deg2rad($value->latitude)) * cos(deg2rad($property_latitude)) * cos(deg2rad($difference_longitude));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $kilometers = $dist * 60 * 1.1515 * 1.609344;
  
            if($kilometers<1000)
            {
               $i++;
               $array[$i]['id'] = $value->id;
               $array[$i]['distance'] = $kilometers;
            }
        }
     }
            return @$array;
  }
}

/*For similar listing and near By*/


/*Helper for room types and bed types will show on properties profile description*/

if (!function_exists('get_room_types')){
  function get_room_types($room_types=""){        
    $CI =& get_instance();
    $CI->db->where('id', $room_types);
    $query = $CI->db->get('room_types');
    return $query->row()->room_type;
  }
}

if (!function_exists('get_types_of_bed')){
  function get_types_of_bed($bed_types=""){        
    $CI =& get_instance();
    $CI->db->where('id', $bed_types);
    $query = $CI->db->get('bed_types');
    return $query->row()->bed_type;
  }
}

/*Helper for room types and bed types will show on properties profile description*/


/* properety rating system */
if ( ! function_exists('property_rating')) 
{
  function property_rating($pr_id)
  {
    $CI =& get_instance();    
    $CI->db->where('property_id',$pr_id);
    $query = $CI->db->get('review');
    $i="0";
    foreach ($query->result() as $row)
    {
        $i+=$row->rating;
    } 
    if($query->num_rows()>0)  
      return $avg  = ($i/$query->num_rows());
    else
       return 0;
   }
}
/* properety rating syatem */





/* get page preview header and footer navigation bar */
if (!function_exists('get_cms_page'))
{
  function get_cms_preview($position)
  {
    $CI =& get_instance();
    if($position == 1)
    {
        $CI->db->where(array('page_link_position' => "1"));
        $CI->db->from('pages');
        $header =  $CI->db->get()->result();
        return $header;
    }
    else
    {
        $CI->db->where(array('page_link_position' => "2"));
        $CI->db->from('pages');
        $footer  = $CI->db->get()->result();
        return $footer;
    }
  }
} 
/* get page  preview header and footer navigation bar */

if ( ! function_exists('get_user_information')) {  
  function get_user_information($email='')
  {
    $CI =& get_instance();
    $CI->db->where('user_email',$email);
    $query = $CI->db->get('users');   
    return $query->row();
  }
}

/* get  property table */
function get_property_table()
{
    $CI =& get_instance();    
    $query = $CI->db->get('properties');
    if($query->num_rows()>0)  
      return $query->result();
    else
       return false;
}
/* get  property table */




/* get amenities of a particular property */
if (!function_exists('get_pr_amenities'))
{
  function get_pr_amenities($pr_id)
  {        
    $CI =& get_instance();
    $CI->db->select('am.name');
    $CI->db->from('pr_amenities as pm');
    $CI->db->join('amenities as am', 'pm.pr_amenity_id = am.id ');
    $CI->db->where('pm.pr_id', $pr_id);
    $row = $CI->db->get(); 
    return $row->result();
  }
}
/* get aminities of a particular property */

/* get_pr_review of a particular property */
if (!function_exists('get_pr_review'))
{
  function get_pr_review($pr_id)
  {        
    $CI =& get_instance();
    $CI->db->select('r.*, u.first_name, u.image');
    $CI->db->from('review as r','left');
    $CI->db->join('users as u','r.customer_id = u.id' ,'left');
    $CI->db->where('r.property_id', $pr_id);
    $CI->db->order_by('id' , 'desc');
    $row = $CI->db->get(); 
    return $row->result();
  }
}
/* get_pr_review of a particular property */

/* get_pr_review count of a particular property */
if (!function_exists('get_pr_review_count'))
{
  function get_pr_review_count($pr_id)
  {        
    $CI =& get_instance();
    $CI->db->where('property_id', $pr_id);
    $row = $CI->db->get('review'); 
    $num = $row->num_rows();
    if($num > 1)
     {
        return $num." reviews";
     } 
     else
     {
        return $num." review";
     }
  }
}
/* get_pr_review count of a particular property */


/* does customer likes particular property or not */
function is_customer_likes_this_pr($customer_id, $pr_id)
{
    $CI =& get_instance();
    $CI->db->where('property_id', $pr_id);
    $CI->db->where('customer_id', $customer_id);
    $row = $CI->db->get('favorites_property'); 
    $num = $row->num_rows();
    if($num > 0)
     {
        return 1;
     } 
     else
     {
        return 0;
     }    
}
/* does customer likes particular property or not */








/* get page header and footer navigation bar */
if (!function_exists('get_cms_page'))
{
  function get_cms_page($position)
  {
    $CI =& get_instance();
    if($position == 1)
    {
        $CI->db->where(array('page_link_position' => "1", 'status' => 1 ));
        $CI->db->from('pages');
        $header =  $CI->db->get()->result();
        return $header;
    }
    else
    {
        $CI->db->where(array('page_link_position' => "2", 'status' => 1 ));
        $CI->db->from('pages');
        $footer  = $CI->db->get()->result();
        return $footer;
    }
  }
} 
/* get page header and footer navigation bar */



/* user properties count */
if (!function_exists('get_user_properties_count'))
{
  function get_user_property_count()
  {
      $CI =& get_instance();
      $user_info=$CI->session->userdata('user_info');
      $id =$user_info['id'];
      $CI->db->where('user_id',$id);
      $CI->db->where('is_delete !=',1);
      $CI->db->where('status',1);
      $query = $CI->db->get('properties');      
      $row   = $query->num_rows();    
      return $row;
  }
} 
/* user properties count */




/* customer message count */
if (!function_exists('get_customer_msg_count'))
{
  function get_customer_msg_count()
  {
      $CI =& get_instance();
      $customer_info=$CI->session->userdata('customer_info');
      $id=$customer_info['id'];
      $email=$customer_info['user_email'];
      
      $CI->db->where('pr_user_id',$id);
      $CI->db->or_where('user_email',$email);
      $query = $CI->db->get('user_message');
      $row   = $query->num_rows();    
      
      return $row;
  }
} 
/* customer message count */

/* customer review count */
if (!function_exists('get_customer_review_count'))
{
  function get_customer_review_count()
  {
      $CI =& get_instance();
      $customer_info=$CI->session->userdata('customer_info');
      $email=$customer_info['user_email'];
      $CI->db->where('user_email',$email);
      $query = $CI->db->get('review');
      $row   = $query->num_rows();    
      return $row;
  }
} 
/* customer review count */

/* user message count */
if (!function_exists('get_note_count'))
{
  function get_note_count()
  {
      $CI =& get_instance();
      $user_info=$CI->session->userdata('user_info');
      $email=$user_info['user_email'];
      $CI->db->where('email',$email);
      $query = $CI->db->get('notifications');
      return $query->num_rows();
  }
}

/* user message count */
if (!function_exists('get_user_msg_count'))
{
  function get_user_msg_count()
  {
      $CI =& get_instance();
      $user_info=$CI->session->userdata('user_info');
      $id=$user_info['id'];
      $email=$user_info['user_email'];
      
      $CI->db->where('pr_user_id',$id);
      $CI->db->or_where('user_email',$email);
      $query = $CI->db->get('user_message');
      $row   = $query->num_rows();    
      
      return $row;
  }
} 
/* user message count */

/* user review count */
if (!function_exists('get_user_review_count'))
{
  function get_user_review_count()
  {
    $CI =& get_instance();
    $user_info=$CI->session->userdata('user_info');
    $customer_info = $CI->session->userdata('customer_info');
      if($user_info!=""){
        $CI->db->where('user_id',$user_info['id']);
        $CI->db->where('status',1);
        $query = $CI->db->get('review');
        $row   = $query->num_rows();    
        return $row;
      }
      if($customer_info!=""){
        $CI->db->where('customer_id',$customer_info['id']);
        $CI->db->where('status',1);
        $query = $CI->db->get('review');
        $row   = $query->num_rows();    
        return $row;
      }
  }
}

if (!function_exists('get_favorites_count'))
{
  function get_favorites_count()
  {
    $CI =& get_instance();
    $user_info=$CI->session->userdata('user_info');
    $customer_info = $CI->session->userdata('customer_info');
      if($user_info!=""){
        $CI->db->where('user_id',$user_info['id']);
        $query = $CI->db->get('favorites_property');
        $row   = $query->num_rows();    
        return $row;
      }
      if($customer_info!=""){
        $CI->db->where('customer_id',$customer_info['id']);
        $query = $CI->db->get('favorites_property');
        $row   = $query->num_rows();    
        return $row;
      }
  }
} 

/* user review count */

/* get number of sign up */
if (!function_exists('get_signup'))
{
  function get_signup()
  {
    $CI =& get_instance();
    $CI->db->where('user_role', 3);
    $CI->db->where('user_status', 0);
    $query = $CI->db->get('users');
    return $query->num_rows();
  }
}
/* get number of sign up */


/* get message information */
if (!function_exists('get_message_info'))
{
    function get_message_info()
    {
        $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('user_message');
        $CI->db->where('status',0);
        $query1 =$CI->db->get();
        $data['row']  = $query1->num_rows();//number of rows

        if($query1->num_rows()>0)
        {
            $CI->db->select('*');
            $CI->db->from('user_message');
            $CI->db->where('status',0);
            $CI->db->order_by('id','desc');     
            $CI->db->limit(4,0);
            $query2=$CI->db->get();
            $data['data'] = $query2->result();//data of rows

            return $data;
        } 
        else
        {   
            return $data;
        }
    }
}
/* end of get message information */


/**
* check user authentication
*/
if ( ! function_exists('superadmin_login')) {  
  function superadmin_login() {
    $CI =& get_instance();
    if ($CI->session->userdata('superadmin_info')) {
      $user_info=$CI->session->userdata('superadmin_info');    
      if($user_info['logged_in']===TRUE):
        return TRUE;      
      else:
        return FALSE;       
      endif;
    } else
      return FALSE;
  }
}

/**
* check user authentication
*/
if ( ! function_exists('subadmin_login')) {  
  function subadmin_login() {
    $CI =& get_instance();
    if ($CI->session->userdata('subadmin_info')) {
      $user_info=$CI->session->userdata('subadmin_info');    
      if($user_info['logged_in']===TRUE):
        return TRUE;      
      else:
        return FALSE;       
      endif;
    } else
      return FALSE;
  }
}


if ( ! function_exists('property_details')) {  
  function property_details($pr_id) {
    $CI =& get_instance();
    $CI->db->select('pr.title, us.id as us_id,pr.id as pro_id,us.user_email,pri.*');
    $CI->db->where('pr.id',$pr_id);
    $CI->db->from('properties as pr');
    $CI->db->join('pr_info as pri','pr.id = pri.pr_id');
    $CI->db->join('users as us','us.id = pr.user_id');
    $query=$CI->db->get();
    if($query->num_rows()>0)
      return $query->row();
    else
      return FALSE; 
  }
}


if ( ! function_exists('get_superadmin_id')) {  
  function get_superadmin_id() {
    $CI =& get_instance();
    if ($CI->session->userdata('superadmin_info')) {
      $user_info=$CI->session->userdata('superadmin_info');    
      if($user_info['logged_in']===TRUE):
        return $user_info['id'];      
      else:
        return FALSE;       
      endif;
    } else
      return FALSE;
  }
}
if ( ! function_exists('get_superadmin_name')) {  
  function get_superadmin_name() {
    $CI =& get_instance();
    if ($CI->session->userdata('superadmin_info')) {
      $user_info=$CI->session->userdata('superadmin_info');    
      if($user_info['logged_in']===TRUE):
        return $user_info['first_name'];      
      else:
        return FALSE;       
      endif;
    } else
      return FALSE;
  }
}

if ( ! function_exists('get_subadmin_id')) {  
  function get_subadmin_id() {
    $CI =& get_instance();
    if ($CI->session->userdata('subadmin_info')) {
      $user_info=$CI->session->userdata('subadmin_info');    
      if($user_info['logged_in']===TRUE):
        return $user_info['id'];      
      else:
        return FALSE;       
      endif;
    } else
      return FALSE;
  }
}
if ( ! function_exists('get_subadmin_name')) {  
  function get_subadmin_name() {
    $CI =& get_instance();
    if ($CI->session->userdata('subadmin_info')) {
      $user_info=$CI->session->userdata('subadmin_info');    
      if($user_info['logged_in']===TRUE):
        return $user_info['first_name'];      
      else:
        return FALSE;       
      endif;
    } else
      return FALSE;
  }
}


/**
* check user authentication
*/
if ( ! function_exists('user_login_in')) {  
  function user_login_in() {
    $CI =& get_instance();
    $user_info=$CI->session->userdata('UserInfo');    
    if($user_info['logged_in']===TRUE):
      return TRUE;      
    else:
      return FALSE;       
    endif;
  }
}

if ( ! function_exists('get_users')) {  
  function get_users() {
    $CI =& get_instance();
    $CI->db->where('user_role',3);
    $query = $CI->db->get('users');   
    return $query->result();
  }
}

if ( ! function_exists('get_property_detail')) {  
  function get_property_detail($pr_id='') {
    $CI =& get_instance();
    $CI->db->where('properties.id',$pr_id);
    $CI->db->from('properties');
    $CI->db->join('pr_info', 'properties.id = pr_info.pr_id');
    $query = $CI->db->get();   
    return $query->row();
  }
}

/**
* Give user Id and user.
*/

if ( ! function_exists('get_all_user')) {  
  function get_all_user() {
    $CI =& get_instance();
    $CI->db->where('user_role',3);
    $query = $CI->db->get('users');   
    return $query->result();
  }
}


if ( ! function_exists('get_users_info')) {  
  function get_users_info($user_id=''){
    $CI =& get_instance();
    $CI->db->where('id',$user_id);

    $query = $CI->db->get('users');   
    return json_encode($query->row());
  }
}


if ( ! function_exists('get_user_info')) {  
  function get_user_info($user_id=''){
    $CI =& get_instance();
    $CI->db->where('id',$user_id);

    $query = $CI->db->get('users');   
    return $query->row();
  }
}


if ( ! function_exists('user_id')) {  
  function user_id() {
    $CI =& get_instance();
    $user_info=$CI->session->userdata('UserInfo');    
    if($user_info['logged_in']===TRUE):
      return $user_info['ID'];      
    else:
      return FALSE;       
    endif;
  }
}

/**
* get usename
*/
if ( ! function_exists('login_username')) { 
  function login_username() {
    $CI =& get_instance();    
    $user_info = $CI->session->userdata('UserInfo');    
    return $user_info['display_name'];  
  }
}

/**
* last login attempt 
*/
if ( ! function_exists('clear_cache')) {
  function last_login_status(){
    $CI =& get_instance();
    $user_sess_name = $CI->session->userdata('user_sess_name'); // get session name   
    $superadmin_info=$CI->session->userdata($user_sess_name); 
    $last_login = strtotime($superadmin_info["last_login"]); $now = now(); 
    $msg='<div style="font-size:10px;float:right;text-align:right;padding-right:10px;">';     
      $msg.="Last login : ".timespan($last_login, $now)." ago <br>";
    $msg.="IP address : ".$superadmin_info["last_ip"]."</div>";
      return $msg;    
  }
}


/**
* clear cache
*/
if ( ! function_exists('clear_cache')) {
  function clear_cache(){
    $CI =& get_instance();
    $CI->output->set_header('Expires: Wed, 11 Jan 1984 05:00:00 GMT' );
    $CI->output->set_header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . 'GMT');
    $CI->output->set_header("Cache-Control: no-cache, no-store, must-revalidate");
    $CI->output->set_header("Pragma: no-cache");      
  }
}

/**
* convert id to encrypt code
*/
if ( ! function_exists('id_encrypt')) { 
  function id_encrypt($id = NULL) {
    $CI =& get_instance();
    return $CI->encrypt->encode($id);
  }
}

/**
* convert decrypt code to id
*/
if ( ! function_exists('id_decrypt')) { 
  function id_decrypt($code = NULL) {
    $CI =& get_instance();
    return $CI->encrypt->decode($code);
  }
}


/* get_theme pagination */
if ( ! function_exists('get_theme_pagination')) {
  function get_theme_pagination(){
    $data = array();
    $data['cur_tag_open'] = '<li class="disabled"><a>';
    $data['cur_tag_close'] = '<</li>';
    $data['full_tag_open'] = '<div class="pagination "><ul>';
    $data['full_tag_close'] = '</ul></div>';
    $data['first_tag_open'] = '<li>';
    $data['first_tag_close'] = '</li>';
    $data['num_tag_open'] = '<li>';
    $data['num_tag_close'] = '</li>';
    $data['last_tag_open'] = '<li>';
    $data['last_tag_close'] = '</li>';
    $data['next_tag_open'] = '<li>';
    $data['next_tag_close'] = '</li>';
    $data['prev_tag_open'] = '<li>';
    $data['prev_tag_close'] = '</li>';
    $data['next_link'] = '&raquo;';
    $data['prev_link'] = '&laquo;';
    $data['cur_tag_open'] = '<li class="active"><a>';
    $data['cur_tag_close'] = '</a></li>';
    return $data;
  }
}



/* theme pagination */
if ( ! function_exists('theme_pagination')) {
  function theme_pagination(){
    $data = array();
    $data['cur_tag_open'] = '<li class="disabled"><a>';
    $data['cur_tag_close'] = '<</li>';
    $data['full_tag_open'] = '<div class="blog_nav"><ul>';
    $data['full_tag_close'] = '</ul></div>';
    $data['first_tag_open'] = '<li>';
    $data['first_tag_close'] = '</li>';
    $data['num_tag_open'] = '<li>';
    $data['num_tag_close'] = '</li>';
    $data['last_tag_open'] = '<li>';
    $data['last_tag_close'] = '</li>';
    $data['next_tag_open'] = '<li>';
    $data['next_tag_close'] = '</li>';
    $data['prev_tag_open'] = '<li>';
    $data['prev_tag_close'] = '</li>';
    $data['next_link'] = '&raquo;';
    $data['prev_link'] = '&laquo;';
    $data['cur_tag_open'] = '<li class="active"><a>';
    $data['cur_tag_close'] = '</a></li>';
    return $data;
  }
}



/**
* alert
*/
/**
* alert
*/
if ( ! function_exists('alert')) {  
  function alert($msg='', $type='success_msg') {
    $CI =& get_instance();?>
    <?php if (empty($msg)): ?>
        <?php if ($CI->session->flashdata('success_msg')): ?>
          <?php echo success_alert($CI->session->flashdata('success_msg')); ?>
        <?php endif ?>
        <?php if ($CI->session->flashdata('error_msg')): ?>
          <?php echo error_alert($CI->session->flashdata('error_msg')); ?>
        <?php endif ?>
        <?php if ($CI->session->flashdata('info_msg')): ?>
          <?php echo info_alert($CI->session->flashdata('info_msg')); ?>
        <?php endif ?>
    <?php else: ?>
        <?php if ($type == 'success_msg'): ?>
          <?php echo success_alert($msg); ?>
        <?php endif ?>
        <?php if ($type == 'error_msg'): ?>
          <?php echo error_alert($msg); ?>
        <?php endif ?>
        <?php if ($type == 'info_msg'): ?>
          <?php echo info_alert($msg); ?>
        <?php endif ?>
    <?php endif; ?>
  <?php }
}

if ( ! function_exists('pass_alert')) {  
  function pass_alert() {
    $CI =& get_instance();?>  
    <?php if ($CI->session->flashdata('success_pass_msg')): ?>
      <?php echo success_alert($CI->session->flashdata('success_pass_msg')); ?>
    <?php endif ?>
    <?php if ($CI->session->flashdata('error_pass_msg')): ?>
      <?php echo error_alert($CI->session->flashdata('error_pass_msg')); ?>
    <?php endif ?>
    

  <?php }
}

if ( ! function_exists('profile_alert')) {  
  function profile_alert() {
    $CI =& get_instance();?>  
    <?php if ($CI->session->flashdata('success_profile_msg')): ?>
      <?php echo success_alert($CI->session->flashdata('success_profile_msg')); ?>
    <?php endif ?>
    <?php if ($CI->session->flashdata('error_profile_msg')): ?>
      <?php echo error_alert($CI->session->flashdata('error_profile_msg')); ?>
    <?php endif ?>
    

  <?php }
}

/*
 * for logo 
*/
  
if ( ! function_exists('get_logo')) {
  function get_logo(){
    $CI =& get_instance();    
    $CI->db->where('id', 1);
    $query = $CI->db->get('site_details');
    return $query->row()->logo;   
  }
}
if ( ! function_exists('get_pr_image')) {
  function get_pr_image($pr_id){
    $CI =& get_instance();    
    $CI->db->where('pr_id',$pr_id);
    $query = $CI->db->get('pr_info');
    return $query->row()->image;   
  }
}




/**
* Success alert
*/
if ( ! function_exists('success_alert')) {  
  function success_alert($msg = '') {?>
<div class="alert alert-success">
  <button data-dismiss="alert" class="close" type="button">×</button>
  <strong>Success!</strong> <?php echo $msg ?>
</div>
  <?php 
  }
}


/**
* Error alert
*/
if ( ! function_exists('error_alert')) {  
  function error_alert($msg = '') {?>
<div class="alert alert-danger">
  <button data-dismiss="alert" class="close" type="button">×</button>
  <strong>Error!</strong> <?php echo $msg ?>
</div>
  <?php 
  }
}

/**
* info alert
*/
if ( ! function_exists('info_alert')) { 
  function info_alert($msg = '') {?>
<div class="alert alert-info">
  <button data-dismiss="alert" class="close" type="button">×</button>
  <strong>Error: </strong> <?php echo $msg ?>
</div>
  <?php 
  }
}

if (!function_exists('pagination')){
  function pagination(){
      $CI =& get_instance(); 
      $data = array();
    $data['cur_tag_open'] = '<li class="disabled"><a>';
    $data['cur_tag_close'] = '<</li>';
    $data['full_tag_open'] = '<div class="pagination pagination-large"><ul>';
    $data['full_tag_close'] = '</ul></div>';
    $data['first_tag_open'] = '<li>';
    $data['first_tag_close'] = '</li>';
    $data['num_tag_open'] = '<li>';
    $data['num_tag_close'] = '</li>';
    $data['last_tag_open'] = '<li>';
    $data['last_tag_close'] = '</li>';
    $data['next_tag_open'] = '<li>';
    $data['next_tag_close'] = '</li>';
    $data['prev_tag_open'] = '<li>';
    $data['prev_tag_close'] = '</li>';
    $data['next_link'] = '&raquo;';
    $data['prev_link'] = '&laquo;';
    $data['cur_tag_open'] = '<li class="active"><a>';
    $data['cur_tag_close'] = '</a></li>';

      echo $CI->acapella_blog->post_pagination($data);            
  }
}

if (!function_exists('remove_special_character')){
  function remove_special_character($string=''){
  return str_replace(array('!','@','#','$','%','^','&','*','(',')','+','{','}','[',']',':','\'', '"', ',' , ';', '<', '>','|','\\','?','/' ), '', $string);           
  }
}

if (!function_exists('comment_count')){
  function comment_count($post_id=""){
    if($post_id == ""){
      return FALSE;
    }    
    $CI =& get_instance();
    $CI->db->where('post_id', $post_id);
    $CI->db->where('comment_approved', 1);
    $query = $CI->db->get('comments');
    return $query->num_rows();
  }
}

if (!function_exists('property_amenities')){
  function property_amenities(){        
    $CI =& get_instance();
    // $CI->db->where('post_id', $post_id);
    // $CI->db->where('comment_approved', 1);
    $query = $CI->db->get('amenities');
    return $query->result();
  }
}
if (!function_exists('get_collection_category')){
  function get_collection_category(){        
    $CI =& get_instance();
    // $CI->db->where('post_id', $post_id);
    // $CI->db->where('comment_approved', 1);
    $query = $CI->db->get('collection_category');
    return $query->result();
  }
}

if (!function_exists('get_bed_types')){
  function get_bed_types(){        
    $CI =& get_instance();
    // $CI->db->where('post_id', $post_id);
    // $CI->db->where('comment_approved', 1);
    $query = $CI->db->get('bed_types');
    return $query->result();
  }
}

if (!function_exists('get_property_types')){
  function get_property_types(){        
    $CI =& get_instance();
    // $CI->db->where('post_id', $post_id);
    // $CI->db->where('comment_approved', 1);
    $query = $CI->db->get('property_types');
    return $query->result();
  }
}
if (!function_exists('get_room_allotment')){
  function get_room_allotment(){        
    $CI =& get_instance();
    // $CI->db->where('post_id', $post_id);
    // $CI->db->where('comment_approved', 1);
    $query = $CI->db->get('room_allotment');
    return $query->result();
  }
}


if (!function_exists('count_blog')){
  function count_blog($category_id=""){
    if($category_id == ""){
      return 0;
    }    
    $CI =& get_instance();
    if ($category_id !="ALL"){    
     $CI->db->where('category_id', $category_id);
    }
    $CI->db->where('post_status', 1);
    $CI->db->where('post_type', 1);
    $query = $CI->db->get('posts');
    return $query->num_rows();
  }

}

/*Create bredcrumb*/
 if (!function_exists('create_breadcrumb')){
  function create_breadcrumb($bredcrumb=''){
     $CI =& get_instance();
    if(!empty($bredcrumb)){
      foreach ($bredcrumb as $label => $url)
        $CI->breadcrumbs->push($label,site_url($url)); //Ex.  site_url('/superadmin/dashboard');

      return $CI->breadcrumbs->show();
    }else
      return FALSE;
  }

}

  /**
  *
  *GIves an array of country code and name
  *
  **/

  if ( ! function_exists('get_country_array'))
  { 
    function get_country_array()
    {
      
      return array(
              "AF"=>"Afghanistan",
              "AX"=>"Aland Islands",
              "AL"=>"Albania",
              "DZ"=>"Algeria",
              "AS"=>"American Samoa",
              "AD"=>"Andorra",
              "AO"=>"Angola",
              "AI"=>"Anguilla",
              "AQ"=>"Antarctica",
              "AG"=>"Antigua and Barbuda",
              "AR"=>"Argentina",
              "AM"=>"Armenia",
              "AW"=>"Aruba",
              "AU"=>"Australia",
              "AT"=>"Austria",
              "AZ"=>"Azerbaijan",
              "BS"=>"Bahamas",
              "BH"=>"Bahrain",
              "BD"=>"Bangladesh",
              "BB"=>"Barbados",
              "BY"=>"Belarus",
              "BE"=>"Belgium",
              "BZ"=>"Belize",
              "BJ"=>"Benin",
              "BM"=>"Bermuda",
              "BT"=>"Bhutan",
              "BO"=>"Bolivia, Plurinational State of",
              "BQ"=>"Bonaire, Sint Eustatius and Saba",
              "BA"=>"Bosnia and Herzegovina",
              "BW"=>"Botswana",
              "BV"=>"Bouvet Island",
              "BR"=>"Brazil",
              "IO"=>"British Indian Ocean Territory",
              "BN"=>"Brunei Darussalam",
              "BG"=>"Bulgaria",
              "BF"=>"Burkina Faso",
              "BI"=>"Burundi",
              "KH"=>"Cambodia",
              "CM"=>"Cameroon",
              "CA"=>"Canada",
              "CV"=>"Cape Verde",
              "KY"=>"Cayman Islands",
              "CF"=>"Central African Republic",
              "TD"=>"Chad",
              "CL"=>"Chile",
              "CN"=>"China",
              "CX"=>"Christmas Island",
              "CC"=>"Cocos (Keeling) Islands",
              "CO"=>"Colombia",
              "KM"=>"Comoros",
              "CG"=>"Congo",
              "CD"=>"Congo, The Democratic Republic of the",
              "CK"=>"Cook Islands",
              "CR"=>"Costa Rica",
              "CI"=>"Cote D'Ivoire",
              "HR"=>"Croatia",
              "CU"=>"Cuba",
              "CW"=>"Curaçao",
              "CY"=>"Cyprus",
              "CZ"=>"Czech Republic",
              "DK"=>"Denmark",
              "DJ"=>"Djibouti",
              "DM"=>"Dominica",
              "DO"=>"Dominican Republic",
              "EC"=>"Ecuador",
              "EG"=>"Egypt",
              "SV"=>"El Salvador",
              "GQ"=>"Equatorial Guinea",
              "ER"=>"Eritrea",
              "EE"=>"Estonia",
              "ET"=>"Ethiopia",
              "FK"=>"Falkland Islands (Malvinas)",
              "FO"=>"Faroe Islands",
              "FJ"=>"Fiji",
              "FI"=>"Finland",
              "FR"=>"France",
              "GF"=>"French Guiana",
              "PF"=>"French Polynesia",
              "TF"=>"French Southern Territories",
              "GA"=>"Gabon",
              "GM"=>"Gambia",
              "GE"=>"Georgia",
              "DE"=>"Germany",
              "GH"=>"Ghana",
              "GI"=>"Gibraltar",
              "GR"=>"Greece",
              "GL"=>"Greenland",
              "GD"=>"Grenada",
              "GP"=>"Guadeloupe",
              "GU"=>"Guam",
              "GT"=>"Guatemala",
              "GG"=>"Guernsey",
              "GN"=>"Guinea",
              "GW"=>"Guinea-Bissau",
              "GY"=>"Guyana",
              "HT"=>"Haiti",
              "HM"=>"Heard Island and McDonald Islands",
              "VA"=>"Holy See (Vatican City State)",
              "HN"=>"Honduras",
              "HK"=>"Hong Kong",
              "HU"=>"Hungary",
              "IS"=>"Iceland",
              "IN"=>"India",
              "ID"=>"Indonesia",
              "IR"=>"Iran, Islamic Republic of",
              "IQ"=>"Iraq",
              "IE"=>"Ireland",
              "IM"=>"Isle of Man",
              "IL"=>"Israel",
              "IT"=>"Italy",
              "JM"=>"Jamaica",
              "JP"=>"Japan",
              "JE"=>"Jersey",
              "JO"=>"Jordan",
              "KZ"=>"Kazakhstan",
              "KE"=>"Kenya",
              "KI"=>"Kiribati",
              "KP"=>"Korea, Democratic People's Republic of",
              "KR"=>"Korea, Republic of",
              "KW"=>"Kuwait",
              "KG"=>"Kyrgyzstan",
              "LA"=>"Lao People's Democratic Republic",
              "LV"=>"Latvia",
              "LB"=>"Lebanon",
              "LS"=>"Lesotho",
              "LR"=>"Liberia",
              "LY"=>"Libya",
              "LI"=>"Liechtenstein",
              "LT"=>"Lithuania",
              "LU"=>"Luxembourg",
              "MO"=>"Macao",
              "MK"=>"Macedonia, The Former Yugoslav Republic of",
              "MG"=>"Madagascar",
              "MW"=>"Malawi",
              "MY"=>"Malaysia",
              "MV"=>"Maldives",
              "ML"=>"Mali",
              "MT"=>"Malta",
              "MH"=>"Marshall Islands",
              "MQ"=>"Martinique",
              "MR"=>"Mauritania",
              "MU"=>"Mauritius",
              "YT"=>"Mayotte",
              "MX"=>"Mexico",
              "FM"=>"Micronesia, Federated States of",
              "MD"=>"Moldova, Republic of",
              "MC"=>"Monaco",
              "MN"=>"Mongolia",
              "ME"=>"Montenegro",
              "MS"=>"Montserrat",
              "MA"=>"Morocco",
              "MZ"=>"Mozambique",
              "MM"=>"Myanmar",
              "NA"=>"Namibia",
              "NR"=>"Nauru",
              "NP"=>"Nepal",
              "NL"=>"Netherlands",
              "NC"=>"New Caledonia",
              "NZ"=>"New Zealand",
              "NI"=>"Nicaragua",
              "NE"=>"Niger",
              "NG"=>"Nigeria",
              "NU"=>"Niue",
              "NF"=>"Norfolk Island",
              "MP"=>"Northern Mariana Islands",
              "NO"=>"Norway",
              "OM"=>"Oman",
              "PK"=>"Pakistan",
              "PW"=>"Palau",
              "PS"=>"Palestinian Territory, Occupied",
              "PA"=>"Panama",
              "PG"=>"Papua New Guinea",
              "PY"=>"Paraguay",
              "PE"=>"Peru",
              "PH"=>"Philippines",
              "PN"=>"Pitcairn",
              "PL"=>"Poland",
              "PT"=>"Portugal",
              "PR"=>"Puerto Rico",
              "QA"=>"Qatar",
              "RE"=>"Reunion",
              "RO"=>"Romania",
              "RU"=>"Russian Federation",
              "RW"=>"Rwanda",
              "BL"=>"Saint Barthelemy",
              "SH"=>"Saint Helena, Ascension and Tristan Da Cunha",
              "KN"=>"Saint Kitts and Nevis",
              "LC"=>"Saint Lucia",
              "MF"=>"Saint Martin (French part)",
              "PM"=>"Saint Pierre and Miquelon",
              "VC"=>"Saint Vincent and the Grenadines",
              "WS"=>"Samoa",
              "SM"=>"San Marino",
              "ST"=>"Sao Tome and Principe",
              "SA"=>"Saudi Arabia",
              "SN"=>"Senegal",
              "RS"=>"Serbia",
              "SC"=>"Seychelles",
              "SL"=>"Sierra Leone",
              "SG"=>"Singapore",
              "SX"=>"Sint Maarten (Dutch part)",
              "SK"=>"Slovakia",
              "SI"=>"Slovenia",
              "SB"=>"Solomon Islands",
              "SO"=>"Somalia",
              "ZA"=>"South Africa",
              "GS"=>"South Georgia and the South Sandwich Islands",
              "SS"=>"South Sudan",
              "ES"=>"Spain",
              "LK"=>"Sri Lanka",
              "SD"=>"Sudan",
              "SR"=>"Suriname",
              "SJ"=>"Svalbard and Jan Mayen",
              "SZ"=>"Swaziland",
              "SE"=>"Sweden",
              "CH"=>"Switzerland",
              "SY"=>"Syrian Arab Republic",
              "TW"=>"Taiwan, Province of China",
              "TJ"=>"Tajikistan",
              "TZ"=>"Tanzania, United Republic of",
              "TH"=>"Thailand",
              "TL"=>"Timor-Leste",
              "TG"=>"Togo",
              "TK"=>"Tokelau",
              "TO"=>"Tonga",
              "TT"=>"Trinidad and Tobago",
              "TN"=>"Tunisia",
              "TR"=>"Turkey",
              "TM"=>"Turkmenistan",
              "TC"=>"Turks and Caicos Islands",
              "TV"=>"Tuvalu",
              "UG"=>"Uganda",
              "UA"=>"Ukraine",
              "AE"=>"United Arab Emirates",
              "GB"=>"United Kingdom",
              "US"=>"United States",
              "UM"=>"United States Minor Outlying Islands",
              "UY"=>"Uruguay",
              "UZ"=>"Uzbekistan",
              "VU"=>"Vanuatu",
              "VE"=>"Venezuela, Bolivarian Republic of",
              "VN"=>"Viet Nam",
              "VG"=>"Virgin Islands, British",
              "VI"=>"Virgin Islands, U.S.",
              "WF"=>"Wallis and Futuna",
              "EH"=>"Western Sahara",
              "YE"=>"Yemen",
              "ZM"=>"Zambia",
              "ZW"=>"Zimbabwe"
            );
    }
  }

  /**
  * Gives Name of Country with respect to the provided country code
  */
  if(! function_exists('get_country_name'))
  { 
    function get_country_name($code='')
    {

      $CI =& get_instance();    

      $country = $CI->get_country_array();    

      return element($code, $country);
    }
  }

   /**
  * Gives Name of Country with respect to the provided country code
  */
  if(! function_exists('get_language_array'))
  { 
    function get_language_array()
    {
      $language = array(
                                0 => 'Bahasa Indonesia', 
                                1 => 'Bahasa Malaysia',
                                2 => 'Bengali',
                                3 => 'Dansk',
                                4 => 'Deutsch',
                                5 => 'English',
                                6 => 'Español',
                                7 => 'Français',
                                8 => 'Hindi',
                                9 => 'Italiano',
                                10 => 'Magyar',
                                11 => 'Nederlands',
                                12 => 'Norsk',
                                13 => 'Polski',
                                14 => 'Português',
                                15 => 'Punjabi',
                                16 => 'Sign Language',
                                17 => 'Suomi',
                                18 => 'Svenska',
                                19 => 'Tagalog',
                                20 => 'Türkçe',
                                21 => 'Čeština',
                                22 => 'Ελληνικά',
                                23 => 'Русский',
                                24 => 'українська',
                                25 => 'עברית',
                                26 => 'العربية',
                                27 => 'ภาษาไทย',
                                28 => '中文',
                                29 => '日本語',
                              );

      return $language; 
    }
  }

  /**
  * Gives Name of Country with respect to the provided country code
  */
  if(! function_exists('get_lang_name'))
  { 
    function get_lang_name($key='')
    {

      $language = get_language_array();    

      return element($key, $language);
    }
  }

  /**
  * Gives Name of Country with respect to the provided country code
  */
  if(! function_exists('get_timezone_array'))
  { 
    function get_timezone_array($key='')
    {
        $timezones = array(
                            'Pacific/Midway'       => "(GMT-11:00) Midway Island",
                            'US/Samoa'             => "(GMT-11:00) Samoa",
                            'US/Hawaii'            => "(GMT-10:00) Hawaii",
                            'US/Alaska'            => "(GMT-09:00) Alaska",
                            'US/Pacific'           => "(GMT-08:00) Pacific Time (US &amp; Canada)",
                            'America/Tijuana'      => "(GMT-08:00) Tijuana",
                            'US/Arizona'           => "(GMT-07:00) Arizona",
                            'US/Mountain'          => "(GMT-07:00) Mountain Time (US &amp; Canada)",
                            'America/Chihuahua'    => "(GMT-07:00) Chihuahua",
                            'America/Mazatlan'     => "(GMT-07:00) Mazatlan",
                            'America/Mexico_City'  => "(GMT-06:00) Mexico City",
                            'America/Monterrey'    => "(GMT-06:00) Monterrey",
                            'Canada/Saskatchewan'  => "(GMT-06:00) Saskatchewan",
                            'US/Central'           => "(GMT-06:00) Central Time (US &amp; Canada)",
                            'US/Eastern'           => "(GMT-05:00) Eastern Time (US &amp; Canada)",
                            'US/East-Indiana'      => "(GMT-05:00) Indiana (East)",
                            'America/Bogota'       => "(GMT-05:00) Bogota",
                            'America/Lima'         => "(GMT-05:00) Lima",
                            'America/Caracas'      => "(GMT-04:30) Caracas",
                            'Canada/Atlantic'      => "(GMT-04:00) Atlantic Time (Canada)",
                            'America/La_Paz'       => "(GMT-04:00) La Paz",
                            'America/Santiago'     => "(GMT-04:00) Santiago",
                            'Canada/Newfoundland'  => "(GMT-03:30) Newfoundland",
                            'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
                            'Greenland'            => "(GMT-03:00) Greenland",
                            'Atlantic/Stanley'     => "(GMT-02:00) Stanley",
                            'Atlantic/Azores'      => "(GMT-01:00) Azores",
                            'Atlantic/Cape_Verde'  => "(GMT-01:00) Cape Verde Is.",
                            'Africa/Casablanca'    => "(GMT) Casablanca",
                            'Europe/Dublin'        => "(GMT) Dublin",
                            'Europe/Lisbon'        => "(GMT) Lisbon",
                            'Europe/London'        => "(GMT) London",
                            'Africa/Monrovia'      => "(GMT) Monrovia",
                            'Europe/Amsterdam'     => "(GMT+01:00) Amsterdam",
                            'Europe/Belgrade'      => "(GMT+01:00) Belgrade",
                            'Europe/Berlin'        => "(GMT+01:00) Berlin",
                            'Europe/Bratislava'    => "(GMT+01:00) Bratislava",
                            'Europe/Brussels'      => "(GMT+01:00) Brussels",
                            'Europe/Budapest'      => "(GMT+01:00) Budapest",
                            'Europe/Copenhagen'    => "(GMT+01:00) Copenhagen",
                            'Europe/Ljubljana'     => "(GMT+01:00) Ljubljana",
                            'Europe/Madrid'        => "(GMT+01:00) Madrid",
                            'Europe/Paris'         => "(GMT+01:00) Paris",
                            'Europe/Prague'        => "(GMT+01:00) Prague",
                            'Europe/Rome'          => "(GMT+01:00) Rome",
                            'Europe/Sarajevo'      => "(GMT+01:00) Sarajevo",
                            'Europe/Skopje'        => "(GMT+01:00) Skopje",
                            'Europe/Stockholm'     => "(GMT+01:00) Stockholm",
                            'Europe/Vienna'        => "(GMT+01:00) Vienna",
                            'Europe/Warsaw'        => "(GMT+01:00) Warsaw",
                            'Europe/Zagreb'        => "(GMT+01:00) Zagreb",
                            'Europe/Athens'        => "(GMT+02:00) Athens",
                            'Europe/Bucharest'     => "(GMT+02:00) Bucharest",
                            'Africa/Cairo'         => "(GMT+02:00) Cairo",
                            'Africa/Harare'        => "(GMT+02:00) Harare",
                            'Europe/Helsinki'      => "(GMT+02:00) Helsinki",
                            'Europe/Istanbul'      => "(GMT+02:00) Istanbul",
                            'Asia/Jerusalem'       => "(GMT+02:00) Jerusalem",
                            'Europe/Kiev'          => "(GMT+02:00) Kyiv",
                            'Europe/Minsk'         => "(GMT+02:00) Minsk",
                            'Europe/Riga'          => "(GMT+02:00) Riga",
                            'Europe/Sofia'         => "(GMT+02:00) Sofia",
                            'Europe/Tallinn'       => "(GMT+02:00) Tallinn",
                            'Europe/Vilnius'       => "(GMT+02:00) Vilnius",
                            'Asia/Baghdad'         => "(GMT+03:00) Baghdad",
                            'Asia/Kuwait'          => "(GMT+03:00) Kuwait",
                            'Africa/Nairobi'       => "(GMT+03:00) Nairobi",
                            'Asia/Riyadh'          => "(GMT+03:00) Riyadh",
                            'Asia/Tehran'          => "(GMT+03:30) Tehran",
                            'Europe/Moscow'        => "(GMT+04:00) Moscow",
                            'Asia/Baku'            => "(GMT+04:00) Baku",
                            'Europe/Volgograd'     => "(GMT+04:00) Volgograd",
                            'Asia/Muscat'          => "(GMT+04:00) Muscat",
                            'Asia/Tbilisi'         => "(GMT+04:00) Tbilisi",
                            'Asia/Yerevan'         => "(GMT+04:00) Yerevan",
                            'Asia/Kabul'           => "(GMT+04:30) Kabul",
                            'Asia/Karachi'         => "(GMT+05:00) Karachi",
                            'Asia/Tashkent'        => "(GMT+05:00) Tashkent",
                            'Asia/Kolkata'         => "(GMT+05:30) Kolkata",
                            'Asia/Kathmandu'       => "(GMT+05:45) Kathmandu",
                            'Asia/Yekaterinburg'   => "(GMT+06:00) Ekaterinburg",
                            'Asia/Almaty'          => "(GMT+06:00) Almaty",
                            'Asia/Dhaka'           => "(GMT+06:00) Dhaka",
                            'Asia/Novosibirsk'     => "(GMT+07:00) Novosibirsk",
                            'Asia/Bangkok'         => "(GMT+07:00) Bangkok",
                            'Asia/Jakarta'         => "(GMT+07:00) Jakarta",
                            'Asia/Krasnoyarsk'     => "(GMT+08:00) Krasnoyarsk",
                            'Asia/Chongqing'       => "(GMT+08:00) Chongqing",
                            'Asia/Hong_Kong'       => "(GMT+08:00) Hong Kong",
                            'Asia/Kuala_Lumpur'    => "(GMT+08:00) Kuala Lumpur",
                            'Australia/Perth'      => "(GMT+08:00) Perth",
                            'Asia/Singapore'       => "(GMT+08:00) Singapore",
                            'Asia/Taipei'          => "(GMT+08:00) Taipei",
                            'Asia/Ulaanbaatar'     => "(GMT+08:00) Ulaan Bataar",
                            'Asia/Urumqi'          => "(GMT+08:00) Urumqi",
                            'Asia/Irkutsk'         => "(GMT+09:00) Irkutsk",
                            'Asia/Seoul'           => "(GMT+09:00) Seoul",
                            'Asia/Tokyo'           => "(GMT+09:00) Tokyo",
                            'Australia/Adelaide'   => "(GMT+09:30) Adelaide",
                            'Australia/Darwin'     => "(GMT+09:30) Darwin",
                            'Asia/Yakutsk'         => "(GMT+10:00) Yakutsk",
                            'Australia/Brisbane'   => "(GMT+10:00) Brisbane",
                            'Australia/Canberra'   => "(GMT+10:00) Canberra",
                            'Pacific/Guam'         => "(GMT+10:00) Guam",
                            'Australia/Hobart'     => "(GMT+10:00) Hobart",
                            'Australia/Melbourne'  => "(GMT+10:00) Melbourne",
                            'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
                            'Australia/Sydney'     => "(GMT+10:00) Sydney",
                            'Asia/Vladivostok'     => "(GMT+11:00) Vladivostok",
                            'Asia/Magadan'         => "(GMT+12:00) Magadan",
                            'Pacific/Auckland'     => "(GMT+12:00) Auckland",
                            'Pacific/Fiji'         => "(GMT+12:00) Fiji",
                        );
        return $timezones;
    }
  }



  /**
  * Gives Name of Country with respect to the provided country code
  */
  if(! function_exists('get_timezone'))
  { 
    function get_timezone($key='')
    {

      $timezone = get_timezone_array();    

      return element($key, $timezone);
    }
  }
  
  if(!function_exists('get_room_type'))
  { 
    function get_room_type()
    {
      $CI =& get_instance();
      $query = $CI->db->get('room_types');
      return $query->result();
    }
  }

  
 if(!function_exists('get_property_title'))
  { 
    function get_property_title($id='')
    {
      $CI =& get_instance();
      $CI->db->where('id',$id);
      $query = $CI->db->get('properties');
      if($query->num_rows() > 0)
        return $query->row()->title;
      else
        return '';
    }
  }
  
  if(!function_exists('get_property_detail'))
  { 
    function get_property_detail($id='')
    {
      $CI =& get_instance();
      $CI->db->where('id',$id);
      $query = $CI->db->get('properties');
      return $query->row();
    }
  }

  if(!function_exists('get_host_email'))
  { 
    function get_host_email($id='')
    {
      $CI =& get_instance();
      $CI->db->where('id',$id);
      $query = $CI->db->get('properties');
      $user_id =$query->row()->user_id;
      $CI->db->where('id',$user_id);
      $query = $CI->db->get('users');
      return $query->row()->user_email;
      
    }
  }

  function bnb_pagination()
  {
      $page = array();
      $page['full_tag_open']  = '<ul class="pagination" style=" font-size:18px; font-weight:bold;">';
      $page['full_tagclose'] = '</ul>';
      $page['cur_tag_open']   = '<li><a  style="background-color:#33AEBD; color:#ffffff;">';
      $page['cur_tag_close']  = '</a></li>';
      $page['first_tag_open'] = '<li>';
      $page['first_tag_close']= '</li>';
      $page['num_tag_open']   = '<li>';
      $page['num_tag_cloe']  = '</li>';
      $page['last_tag_open']  = '<li>';
      $page['last_tag_close'] = '</li>';
      $page['next_tag_open']  = '<li>';
      $page['next_tag_close'] = '</li>';
      $page['prev_tag_open']  = '<li>';
      $page['prev_tag_close'] = '</li>';
      $page['next_link']      = '&raquo;';
      $page['prev_link']      = '&laquo;';
      return $page;
  }

  /* get user information*/
  function get_user_row()
  {
      $CI =& get_instance();
      $user_info=$CI->session->userdata('user_info');
        if($user_info != ""){           
            $id=$user_info['id'];
            $CI->db->select('*');
            $CI->db->from('users');
            $CI->db->where('id',$id);
            $query = $CI->db->get();   
            return $query->row();
        }
        else{
          return FALSE;  
        } 
  }
/* end user iinformation */

/* How many time ago */
function get_time_ago($db_time="")
{
  $before = strlen($db_time);
  $x = str_replace(array(' ','/','-','"/'), "",$db_time);
  $after = strlen($x);
  if($before != $after)
  {
    $db_time = strtotime($db_time);
  }
  $current = time();
  $interval=$current-$db_time;
  if($interval<60)
    {
      return "Just Now";
    }
  else if($interval>=60 && $interval<3600)
    {
      $min=$interval/60;
      return round($min)." Minutes ago";
    }
  else if($interval>=3600 && $interval<86400)
    {
      $hour=$interval/3600;
      return round($hour)." Hours ago";
    }
    else
    {
      return $date=date('d-M-Y',$db_time);
    }
}
/* end of how many time ago */


    if (!function_exists('getMaxprice')){
    function getMaxprice(){      
      $CI =& get_instance();      
      $CI->db->select_max('application_fee');
      $query = $CI->db->get('properties');
      return $query->row();
    }
  }


   if (!function_exists('getEvents')){
    function getEvents($property_id){    
      $CI =& get_instance();      
      $CI->db->where('id', $property_id);
      $query = $CI->db->get('bookings');          
      $dates = "";
      foreach ($query->result() as $row) {        
        $stdate = $row->check_in;
        $enddate = $row->check_out;
        $bookdates = getDatesFromRange($stdate, $enddate);
          foreach ($bookdates as $key => $value) {
            $dates .= "'".date('m-d-Y', strtotime($value))."' : 'booked',";            
          }       
      }
      // echo "'2014-03-21' : 'booked','2014-03-22' : 'booked','2014-03-23' : 'booked',";
      $dates .= "'".date('m-d-Y', strtotime($row->check_out))."':'booked'";
      echo $dates;            
    }
  }


  function getDatesFromRange($start, $end){
    $range = array();
    if (is_string($start) === true) $start = strtotime($start);
    if (is_string($end) === true ) $end = strtotime($end);
    if ($start > $end) return createDateRangeArray($end, $start);
    do {
    $range[] = date('Y-m-d', $start);
    $start = strtotime("+ 1 day", $start);
    }
    while($start < $end);
    return $range;
}


if (!function_exists('get_visitors')){
  function get_visitors(){
    $CI =& get_instance(); 
    $ip = $CI->input->ip_address();

    $CI->load->library('user_agent');
    $browser = $CI->agent->browser();

    $sitedata = array(
      'ipaddress' => $ip,
      'browser' => $browser,
      );

    $CI->db->where('ipaddress',$ip);
    $CI->db->where('browser',$browser);
    $chk = $CI->db->get('site_visit');
    if(!($chk->num_rows() > 0)){
      $CI->db->insert('site_visit', $sitedata);      
    }
  }
}

if (!function_exists('get_totalvisit')){
  function get_totalvisit(){
    $CI =& get_instance();
    $query = $CI->db->get('site_visit');
    return $query->num_rows();
  }
}


if (!function_exists('get_browserpercent')){
  function get_browserpercent($browser=""){
    if($browser == "")
      return FALSE;
    $CI =& get_instance();
    $query0 = $CI->db->get('site_visit');
    $total = $query0->num_rows();

    $CI->db->where('browser',$browser);
    $query = $CI->db->get('site_visit');
    $count = $query->num_rows();
    $percent = ($count/$total)*100;
    return round($percent,2);
  }
}


if (!function_exists('get_allbookings')){
  function get_allbookings(){    
    $CI =& get_instance();        
    $query = $CI->db->get('bookings');
    $count = $query->num_rows();    
    return $count;
  }
}


if (!function_exists('get_recentbookings')){
  function get_recentbookings(){    
    $CI =& get_instance();   
    $today = date('m/d/Y');    
    $old = date( 'm/d/Y', strtotime( $today . ' -5 day' ) );
    // return $old;
    $CI->db->where('created >=',$old);     
    $CI->db->where('created <=',$today);     
    $query = $CI->db->get('bookings');
    $count = $query->num_rows();    
    return $count;
  }
}

/**
* get twitter feed
*/

if ( ! function_exists('get_twitter_feed')) { 
  function get_twitter_feed() {
   require_once APPPATH.'libraries/TwitterAPIExchange.php';  
    $CI =& get_instance();
    
      $twitter = get_oauth_keys('twitter');
     $settings = array(
          'oauth_access_token' => $twitter->twitter_access_token,
          'oauth_access_token_secret' =>$twitter->twitter_access_token_secret,
          'consumer_key' =>$twitter->app_id,
          'consumer_secret' =>$twitter->app_secret_key,
         ); //twitter        

        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $requestMethod = 'GET';

      $getfield="";

      $getfield = '?screen_name='.$twitter->page_id_or_user_name.'&count=20';
      $twitter = new TwitterAPIExchange($settings);
      $response =  $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest(); //get tweet
      $tweet = json_decode($response);   
      return $tweet;
    }    
   
    // return $CI->encrypt->decode($code);
  }

  function countfollow(){
      $facebook = get_oauth_keys('facebook');
      $Facebook_Page_ID_Or_Name="";
      if(!empty($facebook->page_id_or_user_name))
      {
          ########### settings #########    
          $Facebook_Page_ID_Or_Name   = $facebook->page_id_or_user_name; // Facebook Page ID or Name
          ##############################    
      }
    $facebook_like = 'http://graph.facebook.com/'.$Facebook_Page_ID_Or_Name;
    $facebook_data = get_data($facebook_like);    
    // print_r($twitter_data[0]->user->followers_count);
    // echo 'Twitter Followers  : '. $twitter_data->followers_count .'<br />';
    return @$facebook_data->likes;
  
  } 

  function get_data($json_url='',$use_curl=false){
      if($use_curl){
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_URL, $json_url);
          $json_data = curl_exec($ch);
          curl_close($ch);
          return json_decode($json_data);
      }else{
          $json_data = file_get_contents($json_url);
          return json_decode($json_data);
      }
  }



//   if (!function_exists('get_permonthbookings')){
//   function get_permonthbookings($month){    
//     $CI =& get_instance();        
//     $query = $CI->db->get('bookings');
//     $arr = array();
//     foreach ($query->result() as $row) {
//       if(date('m', strtotime($row->created)) == $month){
//         $arr[] = $row;
//       }
//     }
//     $count = count($arr);
//     return $count;
//   }
// }


// if (!function_exists('get_monbookingspending')){
//   function get_monbookingspending($month){    
//     $CI =& get_instance();      
//     $CI->db->where('status',0);  
//     $query = $CI->db->get('bookings');
//     $arr = array();
//     foreach ($query->result() as $row) {
//       if(date('m', strtotime($row->created)) == $month){
//         $arr[] = $row;
//       }
//     }
//     $count = count($arr);
//     return $count;
//   }
// }

//   if (!function_exists('get_monbookingscancel')){
//   function get_monbookingscancel($month){    
//     $CI =& get_instance();      
//     $CI->db->where('status',2);  
//     $query = $CI->db->get('bookings');
//     $arr = array();
//     foreach ($query->result() as $row) {
//       if(date('m', strtotime($row->created)) == $month){
//         $arr[] = $row;
//       }
//     }
//     $count = count($arr);
//     return $count;
//   }
// }


   if (!function_exists('get_permonthbookings')){
  function get_permonthbookings($month){  
  $year = date('Y');  
    $CI =& get_instance();        
    $query = $CI->db->get('bookings');
    $arr = array();
    foreach ($query->result() as $row) {
      if(date('m', strtotime($row->created)) == $month && date('Y', strtotime($row->created)) == $year){
        $arr[] = $row;
      }
    }
    $count = count($arr);
    return $count;
  }
}


if (!function_exists('get_monbookingspending')){
  function get_monbookingspending($month){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('status',0);  
    $query = $CI->db->get('bookings');
    $arr = array();
    foreach ($query->result() as $row) {
      if(date('m', strtotime($row->created)) == $month && date('Y', strtotime($row->created)) == $year){
        $arr[] = $row;
      }
    }
    $count = count($arr);
    return $count;
  }
}

  if (!function_exists('get_monbookingscancel')){
  function get_monbookingscancel($month){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('status',2);  
    $query = $CI->db->get('bookings');
    $arr = array();
    foreach ($query->result() as $row) {
      if(date('m', strtotime($row->created)) == $month && date('Y', strtotime($row->created)) == $year){
        $arr[] = $row;
      }
    }
    $count = count($arr);
    return $count;
  }
}




  if (!function_exists('get_permonthactive')){
  function get_permonthactive($month){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('status',1);  
    $query = $CI->db->get('properties');
    $arr = array();
    foreach ($query->result() as $row) {
      if(date('m', strtotime($row->created)) == $month && date('Y', strtotime($row->created)) == $year){
        $arr[] = $row;
      }
    }
    $count = count($arr);
    return $count;
  }
}


 if (!function_exists('get_featured')){
  function get_featured($month){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('featured_listing_status',1);  
    $query = $CI->db->get('properties');
    $arr = array();
    foreach ($query->result() as $row) {
      if(date('m', strtotime($row->created)) == $month && date('Y', strtotime($row->created)) == $year){
        $arr[] = $row;
      }
    }
    $count = count($arr);
    return $count;
  }
}


 if (!function_exists('get_pendingprop')){
  function get_pendingprop($month){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('status',0);  
    $query = $CI->db->get('properties');
    $arr = array();
    foreach ($query->result() as $row) {
      if(date('m', strtotime($row->created)) == $month && date('Y', strtotime($row->created)) == $year){
        $arr[] = $row;
      }
    }
    $count = count($arr);
    return $count;
  }
}



if(!function_exists('getmember_montharr')){
  function getmember_montharr(){    
    $year = date('Y');
    $CI =& get_instance();          
    $query = $CI->db->get('users');    
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}



if(!function_exists('getactivemember_montharr')){
  function getactivemember_montharr(){    
    $year = date('Y');
    $CI =& get_instance();       
    $CI->db->where('user_status', 1);   
    $query = $CI->db->get('users');    
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}


if(!function_exists('getinactivemember_montharr')){
  function getinactivemember_montharr(){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('user_status', 0);    
    $query = $CI->db->get('users');    
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}

  if(!function_exists('allpayment_montharr')){
  function allpayment_montharr(){    
    $year = date('Y');
    $CI =& get_instance();          
    $query = $CI->db->get('payments');    
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}



if(!function_exists('userpaid_montharr')){
  function userpaid_montharr(){    
    $year = date('Y');
    $CI =& get_instance();       
    $CI->db->where('paid_to_owner', 1);   
    $query = $CI->db->get('payments');    
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}


if(!function_exists('refundpay_montharr')){
  function refundpay_montharr(){    
    $year = date('Y');
    $CI =& get_instance();      
    $CI->db->where('is_refund', 1);    
    $query = $CI->db->get('payments');      
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}


if(!function_exists('thisyear_booking')){
  function thisyear_booking(){    
    $year = date('Y');
    $CI =& get_instance();          
    $query = $CI->db->get('bookings');      
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}


if(!function_exists('thisyear_listings')){
  function thisyear_listings(){    
    $year = date('Y');
    $CI =& get_instance();          
    $query = $CI->db->get('properties');      
    $arr = array();    
    for($i=1; $i <= 12; $i++){ 
    $arr[$i] = array();     
      foreach ($query->result() as $row){                        
        if(date('n', strtotime($row->created)) == $i && date('Y', strtotime($row->created)) == $year){
          $arr[$i][] = $row;
        }      
      }
    }    
    return $arr;
  }
}


if (!function_exists('get_cancelled_booking')){
  function get_cancelled_booking(){        
    $CI =& get_instance();      
    $CI->db->where('status',2);  
    $query = $CI->db->get('bookings');
    if($query->num_rows() > 0){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
}


if (!function_exists('get_pending_booking')){
  function get_pending_booking(){        
    $CI =& get_instance();      
    $CI->db->where('status',0);  
    $query = $CI->db->get('bookings');
    if($query->num_rows() > 0){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
}


if (!function_exists('get_totalprop')){
  function get_totalprop(){        
    $CI =& get_instance();      
    // $CI->db->where('status',0);  
    $query = $CI->db->get('properties');
    if($query->num_rows() > 0){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
}


if (!function_exists('get_totalusers')){
  function get_totalusers(){        
    $CI =& get_instance();      
    // $CI->db->where('status',0);  
    $query = $CI->db->get('users');
    if($query->num_rows() > 0){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
}


if (!function_exists('get_totalneighbour')){
  function get_totalneighbour(){        
    $CI =& get_instance();      
    // $CI->db->where('status',0);  
    $query = $CI->db->get('neighbourhoods');
    if($query->num_rows() > 0){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
}

if(!function_exists('automatic_cancel')){
  function automatic_cancel(){
    $CI =& get_instance(); 
    $CI->db->where('status',0);         
    $query  = $CI->db->get('bookings');
    if($query->num_rows() > 0){
      foreach ($query->result() as $row) {
        $cur_date = date('Y-m-d');
        $created = date('Y-m-d', strtotime($row->created));        
        $datetime1 = new DateTime($created);
        $datetime2 = new DateTime($cur_date);
        $interval = $datetime1->diff($datetime2);
        if($interval->d >= 2){
          $CI->db->update('bookings', array('status'=> 2), array('id'=>$row->id));
        }
      }
    }else{
      return FALSE;
    }
  }
}



if (!function_exists('history_to_bookingid')){
  function history_to_bookingid($payment_id = ""){        
    if($payment_id ==""){
      return FALSE;
    }

    $CI =& get_instance();      
    $CI->db->where('id',$payment_id);  
    $query = $CI->db->get('payments');
    if($query->num_rows() > 0){
      return $query->row()->booking_id;
    }else{
      return 0;
    }
  }
}

if(!function_exists('usermsg_notification')){
  function usermsg_notification(){
    $CI =& get_instance(); 
    $user = $CI->session->userdata('user_info');
    $userid = $user['id'];
    $CI->db->where('pr_user_id',$userid);         
    $CI->db->where('receiver_alert',0);         
    $query  = $CI->db->get('user_message');
    if($query->num_rows() > 0){      
      return $query->num_rows();
    }else{
      return FALSE;
    }
  }
}


if(!function_exists('get_property_images')){
  function get_property_images($property_id=""){
    if($property_id == "")
      return FALSE;
    $CI =& get_instance(); 

    $CI->db->where('pr_id', $property_id);
    $query = $CI->db->get('pr_gallery');
    if($query->num_rows > 0){
      return $query->result();
    }else{
      return FALSE;
    }
  }
}


if(!function_exists('get_homecontent')){
  function get_homecontent(){    
    $CI =& get_instance(); 

    $CI->db->where('id', 1);
    $query = $CI->db->get('site_content');
    if($query->num_rows > 0){
      return $query->row();
    }else{
      return FALSE;
    }
  }
}



function get_nb_tag($id='') 
{
    $CI  =& get_instance();    
    $CI->db->where('neighbour', $id);
    $query = $CI->db->get('neighbour_cat');

    if($query->num_rows > 0)
    {
        $cat = $query->result();
        foreach($cat as $row)
        {
            $CI->db->or_where('id',$row->category);
        }
        $tag = $CI->db->get('neb_category');
        return $tag->result();
    }
    else
    {
        return false;
    }    
}

  function get_row($table_name='', $id_array='')
  {
    $CI  =&get_instance();    
    if(!empty($id_array)):    
      foreach ($id_array as $key => $value){
        $CI->db->where($key, $value);
      }
    endif;

    $query=$CI->db->get($table_name);
    if($query->num_rows()>0)
      return $query->row();
    else
      return FALSE;
  }


