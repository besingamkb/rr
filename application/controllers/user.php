<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once APPPATH.'libraries/facebook/facebook.php';
class User extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();			
		clear_cache();
		$this->load->model('user_model');
		$this->config->load('facebook');
		 $currency = $this->session->userdata('currency');
    	 $data['row']=$this->user_model->get_row('site_details');
	    if(empty($currency))
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	    elseif($data['row']->currency_status==1)
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	}

	public function index($value='')
	{
		redirect('user/dashboard');
	}

	public function check_login()
	{
		if(!$this->session->userdata('user_info'))
		{
			redirect('front');
		}
	}
	
	public function dashboard()
	{
		$this->check_login();
		$user = $this->user_model->get_row('users', array('id' => get_my_id()));
		if (empty($user->password)) {
			$data['msg'] = 'Please update your password, so that you can normally login from login page.<br><a href="'.base_url().'user/change_password" class="alert-link">Click to update your password now.</a>';
			$data['type'] = 'info_msg';
		}
		$data['varifications'] = $user;
		$data['template'] = 'user/dashboard';
		$this->load->view('templates/user_template', $data);
	}

	public function logout()
	{
	 	$this->session->unset_userdata('user_info');
	 	$this->session->unset_userdata('subadmin_info');
	 	redirect('front');
	}

	// public function profile($value='') { 
	// $this->check_login();
	// $test_user =$this->session->userdata('user_info'); 
	
	// if($test_user=='') redirect('user/');
	// $data['member'] =$this->user_model->get_row('users', array('id' => $test_user['id']));
	// $mem_id=$data['member']->id; 
	// $this->form_validation->set_rules('first_name', 'First name', 'required');
	// $this->form_validation->set_rules('last_name', 'Last name', 'required');		
	// $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|callback_check_is_email_unique['.$data['member']->user_email.']');
	// $this->form_validation->set_rules('phone', 'Contact', 'required');
	// $this->form_validation->set_rules('address', 'Address', 'required');
 //    $this->form_validation->set_rules('city', 'City', 'required');
 //    $this->form_validation->set_rules('state', 'State', 'required');
 //    $this->form_validation->set_rules('zip', 'Zip', 'required');
 //    $this->form_validation->set_rules('paypal_email', 'Paypal Email', 'required');
 //    $this->form_validation->set_rules('country', 'Country', 'required');
 //    $this->form_validation->set_error_delimiters('<div style="color:#DE4980;" class="error">', '</div>');
 //    if ($this->form_validation->run() == TRUE){

	// 		$user=array(
				
	// 			'first_name'	=>	strtolower($this->input->post('first_name')),
	// 			'last_name'		=>	strtolower($this->input->post('last_name')),
	// 			'user_email'	=>	 $this->input->post('user_email'),
	// 			'paypal_email'	=>	 $this->input->post('paypal_email'),
	// 			'phone'			=>	$this->input->post('phone'),
	// 			'address'		=>	$this->input->post('address'),	
	// 			'city'			=>	$this->input->post('city'),	
	// 			'state'			=>	$this->input->post('state'),	
	// 			'zip'			=>	$this->input->post('zip'),	
	// 			'country'		=>	$this->input->post('country')
	// 			);
			
	// 		$user_id=$this->user_model->update('users',$user, array('id' => $mem_id));
			
	// 		if ($user_id) {
	// 			$this->session->set_flashdata('success_msg',"Member has been updaed successfully.");
	// 			redirect('user/profile');
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('error_msg',"Error in updating information.");
	// 			redirect('user/profile');
	// 		}

	// 	}
	// 	$data['template'] = 'user/profile';
	// 	$this->load->view('templates/user_template', $data);
	// }

	public function profile($value='') {
		$this->check_login();
		$test_user =$this->session->userdata('user_info'); 
		if($test_user=='') redirect('user/');
		$data['member'] =$this->user_model->get_row('users', array('id' => $test_user['id']));
		if (!$data['member']) {
			$this->session->set_flashdata('error_msg', 'No info found.');
			redirect('user/view_profile');
		}

		if ($this->input->post('gender'))
			$data['gender'] = $this->input->post('gender');
		else
			$data['gender'] = $data['member']->gender;

		if ($this->input->post('country'))
			$data['country'] = $this->input->post('country');
		else
			$data['country'] = $data['member']->country;

		if ($_POST){
			$data['year'] = $this->input->post('year');
			$data['month'] = $this->input->post('month');
			$data['day'] = $this->input->post('day');
			
			if (($this->input->post('day') != '') && ($this->input->post('month') != '') && ($this->input->post('year') != '')){
				$dtime = mktime(0,0,0,$data['day'],$data['month'],$data['year']);
				$dob = date('Y-m-d',$dtime);
			}else
				$dob = $data['member']->dob;
		}
		else{
			$data['year'] = date('Y', strtotime($data['member']->dob));
			$data['month'] = date('j', strtotime($data['member']->dob));
			$data['day'] = date('n', strtotime($data['member']->dob));
			$dob = $data['member']->dob;
		}
		
		$mem_id=$data['member']->id; 
		$this->form_validation->set_rules('first_name', 'First name', 'required');
		$this->form_validation->set_rules('last_name', 'Last name', 'required');		
		$this->form_validation->set_rules('gender', 'Gender', 'required');		
		$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|callback_check_is_email_unique['.$data['member']->user_email.']');
		$this->form_validation->set_rules('day', 'Day', 'required');
		$this->form_validation->set_rules('month', 'Month', 'required');
		$this->form_validation->set_rules('year', 'Year', 'required');
		$this->form_validation->set_rules('phone', 'Contact', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
	    $this->form_validation->set_rules('city', 'City', 'required');
	    $this->form_validation->set_rules('state', 'State', 'required');
	    $this->form_validation->set_rules('zip', 'Zip', 'required');
	    $this->form_validation->set_rules('paypal_email', 'Paypal Email', 'required');
	    $this->form_validation->set_rules('country', 'Country', 'required');
	    $this->form_validation->set_rules('description', 'Description', 'required');
	    $this->form_validation->set_error_delimiters('<div style="color:#DE4980;" class="error">', '</div>');
	    if ($this->form_validation->run() == TRUE){
	  //   	echo "<prev>";
	  //   	print_r($data);
			// die();
			$user=array(
				
				'first_name'	=>	strtolower($this->input->post('first_name')),
				'last_name'		=>	strtolower($this->input->post('last_name')),
				'user_email'	=>	$this->input->post('user_email'),
				'paypal_email'	=>	$this->input->post('paypal_email'),
				'phone'			=>	$this->input->post('phone'),
				'address'		=>	$this->input->post('address'),	
				'city'			=>	$this->input->post('city'),	
				'state'			=>	$this->input->post('state'),	
				'zip'			=>	$this->input->post('zip'),	
				'country'		=>	$this->input->post('country'),
				'gender'		=>	$this->input->post('gender'),
				'description'	=>	$this->input->post('description'),
				'dob'			=>	$dob,
				'school'		=>	$this->input->post('school'),
				'work'			=>	$this->input->post('work'),
				'timezone'		=>	$this->input->post('timezone')
				);
			// print_r($user);
			// die();
			$user_id=$this->user_model->update('users',$user, array('id' => $mem_id));
			
			if ($user_id) {
				$this->session->set_flashdata('success_msg',"Member has been updaed successfully.");
				redirect('user/profile');
			}
			else
			{
				$this->session->set_flashdata('error_msg',"Error in updating information.");
				redirect('user/profile');
			}

		}
		$data['groups'] = $this->user_model->get_my_groups(get_my_id());
		$data['language'] = $this->user_model->get_result('languages', array('user_id' => get_my_id()));
		// print_r($data['groups']);
		// die();
		$data['template'] = 'user/account/profile';
		$this->load->view('templates/user_template', $data);
	}

	public function ajax_add_language()
	{
		if (!empty($_POST)) {
			$langArray = array();
			$userLang = $this->user_model->get_result('languages', array('user_id' => get_my_id()));
			if (!empty($userLang)) {
				foreach ($userLang as $language) {
					$langArray[] = $language->lang_id;
				}
			}
			// echo "<pre>";
			// echo "<br> Count : ".count($_POST['data']);
			$added = 0;
			foreach ($_POST['data'] as $key => $value) {
				
				if (!empty($langArray)) {
					if (in_array($value['value'], $langArray))
						continue;
				}

				$lang = array(
								'user_id' => get_my_id(), 
								'lang_id' => $value['value'], 
								'created' => date('Y-m-d'), 
								'updated' => date('Y-m-d') 
							);
				$this->user_model->insert('languages', $lang);
				$added++;
			}

			// echo "<br> Added : ".$added;
			// die();
			
			if ($added == 0) {
				echo json_encode(array('status' => false, 'msg' => 'All selected languages are already added.'));
			}else
				echo json_encode(array('status' => true, 'msg' => 'Selected language added successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No language found to add.'));
		}
	}

	public function ajax_remove_language()
	{
		if (!empty($_POST)) {
			$id = $_POST['id'];
			$lang = $this->user_model->delete('languages', array('id' => $id));
			if ($lang)
				echo json_encode(array('status' => true));
			else
				echo json_encode(array('status' => false));
		}else{
			echo json_encode(array('status' => false));
		}
	}

	public function ajax_remove_groups()
	{
		if (!empty($_POST)) {
			$id = $_POST['id'];
			$group = $this->user_model->delete('group_members', array('id' => $id));
			if ($group) {
				echo json_encode(array('status' => true));
			}else
				echo json_encode(array('status' => false));
		}else{
			echo json_encode(array('status' => false));
		}
	}

	public function change_password($id=''){ 
		$this->check_login();
		// $test_user = $this->session->userdata('user_info');
		// if($test_user=='') redirect('user/');
		$id = get_my_id();
		$user = $this->user_model->get_row('users', array('id' => $id));
		if (!$user) {
			$this->session->set_flashdata('error_msg', 'User info is missing.');
			redirect('user');
		}
	   	$data['old_pass'] = FALSE;
		if (!empty($user->password)) {
	    	$this->form_validation->set_rules('oldpassword','Old Password','required|callback_compare_oldpwd_enterpwd');
			$data['old_pass'] = TRUE;
		}
		$this->form_validation->set_rules('newpassword','New Password','required');
		$this->form_validation->set_rules('confirmpassword','Confirm Password','required|matches[newpassword]');
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');   
	    if($this->form_validation->run() == TRUE){   
		      $newpwd=$this->input->post('newpassword');
		      $newpwd1=sha1(trim($newpwd));
		      $this->user_model->update('users',array('password' => $newpwd1), array('id'=>$id));
		      $this->session->set_flashdata('success_msg','Password Has Been Changed Successfully');
		      redirect('user/change_password');
	     } 
		$data['template'] = 'user/account/change_password1';
		$this->load->view('templates/user_template', $data);
    }

 	public function  compare_oldpwd_enterpwd($enterpwd){
 		$this->check_login();
		$test_user = $this->session->userdata('user_info');
		$id = $test_user['id'];
		$enterpwd1=sha1($enterpwd);
		$row = $this->user_model->get_row('users', array('id'=> $id));
		if($row->password == $enterpwd1){
			return TRUE;
		} 
		else{
			$this->form_validation->set_message('compare_oldpwd_enterpwd','Please Enter Correct Old Password');
			return FALSE;
		}
 	}

 	public function notifications($offset=0)
	{
		$this->check_login();
		$limit=10;
		$user_info = get_user_info(get_my_id());
		// print_r($user_info);
		// die();
		$data['notifications'] = $this->user_model->get_user_notification($limit, $offset, $user_info->user_email);
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/notifications/';
		$config['total_rows'] = $this->user_model->get_user_notification(0, 0, $user_info->user_email);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$offset++;
		$data['counter'] = $offset;
		$data['template'] = 'user/account/notifications';
		$this->load->view('templates/user_template',$data);
	}

	public function ajax_get_msg()
	{
		if (isset($_POST['id'])) {
			$note = $this->user_model->get_row('notifications', array('id' => $_POST['id']));
			if ($note) {
				echo json_encode(array('status' => true, 'sub' => $note->subject, 'msg' => $note->message ));
			}else
				echo json_encode(array('status' => false, 'msg' => "Can't find message." ));
		}else
			echo json_encode(array('status' => false, 'msg' => "Can't find message." ));
	}

 	public function messages_sent($offset=0)
	{
		$this->check_login();
		$user_info = $this->session->userdata('user_info');
		$email = $user_info['user_email'];
		$id = $user_info['id'];
		$limit=10;
		$data['messages'] = $this->user_model->get_pagination_where('user_message', $limit, $offset, array('sender_id' => $id,'delete_by_sender !='=>1));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/messages/';
		$config['total_rows'] = $this->user_model->get_pagination_where('user_message', 0, 0, array('sender_id' => $id,'delete_by_sender !='=>1));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/message_sent_by_me';
		$this->load->view('templates/user_template',$data);
	}

 	public function messages_received($offset=0)
	{
		$this->check_login();
		$user_info = $this->session->userdata('user_info');
		$email = $user_info['user_email'];
		$id = $user_info['id'];
		$limit=10;
		$data['messages'] = $this->user_model->get_pagination_where('user_message', $limit, $offset, array('pr_user_id' => $id,'delete_by_receiver !='=>1));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/messages/';
		$config['total_rows'] = $this->user_model->get_pagination_where('user_message', 0, 0, array('pr_user_id' => $id,'delete_by_receiver !='=>1));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/message_received_by_me';
		$this->load->view('templates/user_template',$data);
	}

 	public function delete_by_sender($message_id='')
 	{
		$this->check_login();
		if(empty($message_id)) redirect('user/messages');		
		$this->user_model->update('user_message',array('delete_by_sender'=>1),array('id'=>$message_id));
		$this->session->set_flashdata('success_msg',"Message has been deleted successfully.");
		redirect('user/messages_sent');
	}

 	public function delete_by_receiver($message_id='')
 	{
		$this->check_login();
		if(empty($message_id)) redirect('user/messages');		
		$this->user_model->update('user_message',array('delete_by_receiver'=>1),array('id'=>$message_id));
		$this->session->set_flashdata('success_msg',"Message has been deleted successfully.");
		redirect('user/messages_received');
	}

 	public function message_view($role='',$message_id='')
 	{
		$this->check_login();
 		if(empty($message_id)) redirect('user/messages_received');
 		$data['message'] =$this->user_model->get_row('user_message',array('id'=>$message_id));
	    $data['role'] = $role; 

	    ////////// Reply Notification //////////
	    if($role == 'received'){
	    	$colomn = 'readByMsgReceiver';
	    }elseif($role == 'sent'){
	    	$colomn = 'readByMsgSender';
	    }
	    $where = array('message_id' => $message_id);
	    $update = array($colomn => 1);
	    $this->user_model->update('user_message_reply',$update,$where);
	    ////////// Reply Notification //////////


	    /////////////////// fetch reply data  ///////////
	    $offset=0;
		$limit=4;
		$data['reply'] = $this->user_model->get_pagination_where('user_message_reply', $limit, $offset, array( 'message_id' =>$message_id ),$slug="desc");
		$data['total_reply'] = $this->user_model->get_pagination_where('user_message_reply', 0, 0, array( 'message_id' =>$message_id ));
        ////////////////// end of reply  ///////////////

		$data['template'] = 'user/message_view';
		$this->load->view('templates/user_template',$data);	 		
 	}

 	public function ajax_load_more_reply($message_id='',$offset='')
	{
	  $this->check_login();
	  if(empty($message_id) || empty($offset)) redirect('user/messages');
	  $limit=4;
	  $data['reply'] = $this->user_model->get_pagination_where('user_message_reply', $limit, $offset, array( 'message_id' =>$message_id ),$slug='desc');
	  $res = $this->load->view('user/reply_message_template', $data);
	  echo $res;   
	}

	// public function reply($message_id,$messanger_id)
 //    {
	// 	$this->check_login();
 // 		$replier_info   = $this->session->userdata('user_info');
	// 	$reply = array(
	// 					'reply'          => $this->input->post('reply'),
	// 					'created'        => time(),
	// 					'message_id'     => $message_id,
	// 				  	'replier_id'     => $replier_info['id'],
	// 					'messanger_id'   => $messanger_id
	// 				  );
	// 	if($reply['replier_id'] == $reply['messanger_id']){
	// 		$reply['readByMsgSender'] = 1;
	// 		$segment = 'sent';
	// 	}else{
	// 		$reply['readByMsgReceiver'] = 1;
	// 		$segment = 'received';
	// 	}

	// 	$this->user_model->insert('user_message_reply', $reply);
	// 	redirect('user/message_view/'.$segment.'/'.$message_id);
 //    }
 
    public function reply($message_id,$messanger_id)
    {
		$this->check_login();
 		$replier_info = $this->session->userdata('user_info');
		$reply = array(
						'reply'          => $this->input->post('reply'),
						'created'        => time(),
						'message_id'     => $message_id,
					  	'replier_id'     => $replier_info['id'],
						'messanger_id'   => $messanger_id
					  );

		if($reply['replier_id'] == $reply['messanger_id']){
			$reply['readByMsgSender'] = 1;
			$segment = 'sent';
		}else{
			$reply['readByMsgReceiver'] = 1;
			$segment = 'received';
		}

    	$message = $this->user_model->get_row('user_message', array('id' => $message_id));
    	// echo "<pre>";

    	if (!empty($message)) {
    		if ($message->sender_id == $reply['replier_id'])
    			$mailTo = $message->pr_user_id;
    		elseif($message->pr_user_id == $reply['replier_id'])
    			$mailTo = $message->sender_id;
    		$receiver = $this->user_model->get_row('users', array('id' => $mailTo));
    		// print_r($receiver);
    		
    		if (!empty($receiver)) {
    			// echo "<br> POST : ".$this->input->post('reply');
    			// print_r($replier_info);
				$from = array($replier_info['user_email'] => $replier_info['first_name'].' '.$replier_info['last_name']);	// From email in array form	
				// $from = array( $replier_info['first_name'].' '.$replier_info['last_name'] => $replier_info['user_email']);	// From email in array form	
				// $receiver->user_email,
    			$message = $this->input->post('reply');

    			// echo "<br>From : ";
    			// echo "<br>Receiver : ".$receiver;
    			$this->sendReplyMail($from,$receiver,$message);
    		}
    	}

    	// print_r($message);
    	// print_r($replier_info);
    	// print_r($reply);
    	// die();
		$this->user_model->insert('user_message_reply', $reply);
		redirect('user/message_view/'.$segment.'/'.$message_id);
    }

    public function sendReplyMail($from,$receiver,$message){
		
		$this->load->library('smtp_lib/smtp_email');

		$subject = 'Conversation Notification';	// Subject for email

		$to = array(
			 $receiver->user_email,
		);

		$html = "<em><strong>Hello ".$receiver->first_name." ".$receiver->last_name." !</strong></em> <br>
				<p><strong>Subject - ".$subject."</strong></p>
				<p><strong>Reply - ".$message."</strong></p>";
		// print_r($to);
		// print_r($from);
		// echo "<br>HTML : ".$html;
		$this->smtp_email->sendEmail($from, $to, $subject, $html);
	}

	public function bookings($offset=0)
 	{
 		$this->check_login();
 		$user = $this->session->userdata('user_info');
		$id = $user['id'];
		$limit = 10;
		$data['bookings']=$this->user_model->get_pagination_where('bookings',$limit,$offset, array('owner_id' => $id ,'is_delete_by_owner !='=>1));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/bookings/';
		$config['total_rows']=$this->user_model->get_pagination_where('bookings',0,0, array('owner_id' => $id ,'transaction_id !='=>"",'is_delete_by_owner !='=>1));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/bookings';
		$this->load->view('templates/user_template',$data);	 		
 	}


	public function delete_booking_of_other($bookings_id='')
	{
		$this->check_login();
		if(empty($bookings_id)) redirect('user/bookings');		
		$this->user_model->update('bookings',array('is_delete_by_owner'=>1),array('id'=>$bookings_id));
		$this->session->set_flashdata('success_msg',"Booking has been deleted successfully.");
		redirect('user/bookings');
	}


    public function bookings_by_me($offset=0)
 	{
 		$this->check_login();
 		$user = $this->session->userdata('user_info');
		$id = $user['id'];
		$limit=10;
		$data['bookings_by_me']=$this->user_model->get_pagination_where('bookings',$limit,$offset,array('customer_id' => $id ,'transaction_id !='=>"",'is_delete_by_customer !='=>1) );
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/bookings_by_me/';
		$config['total_rows']=$this->user_model->get_pagination_where('bookings',0,0, array('customer_id' => $id ,'transaction_id !='=>"",'is_delete_by_customer !='=>1) );
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/bookings_by_me';
		$this->load->view('templates/user_template',$data);	 		
 	}
	public function delete_booking_by_me($bookings_id='')
	{
		$this->check_login();
		if(empty($bookings_id)) redirect('user/bookings');		
		$this->user_model->update('bookings',array('is_delete_by_customer'=>1),array('id'=>$bookings_id));
		$this->session->set_flashdata('success_msg',"Booking has been deleted successfully.");
		redirect('user/bookings');
	}

	public function check_date_of_checkin($date)
	{
		$this->check_login();
		$error_msg = '';
		if ($this->input->post('check_in') == '') {
			$error_msg.='Please select checkin date. <br>';
			// $this->form_validation->set_message('check_date_of_checkin', 'Please select checkin date.');
		 	// return FALSE;
		}

		if ($this->input->post('check_out') == '') {
			$error_msg.='Please select checkout date. <br>';
			$this->form_validation->set_message('check_date_of_checkin', $error_msg);
		 	return FALSE;
		}

		$check_in = strtotime($this->input->post('check_in'));
		$check_out = strtotime($this->input->post('check_out'));
		$date1 = strtotime($date[0]);
		$today_date = strtotime(date('d-m-Y'));
		

		if($today_date > $check_out){
			$error_msg.='Invaild checkout date. <br>';
			$this->form_validation->set_message('check_date_of_checkin', $error_msg);
		 	return FALSE;
		}

		if (!empty($error_msg)) {
			$this->form_validation->set_message('check_date_of_checkin', $error_msg);
		 	return FALSE;
		}

		if($check_in > $check_out){
			$this->form_validation->set_message('check_date_of_checkin', 'Please select The vaild checkin ,checkout dates.');
		 	   return FALSE;
		}else{
			return TRUE;
		}
	}

	public function Edit_booking($bookings_id='')
	{	
		$this->check_login();
		if(empty($bookings_id)) redirect('user/bookings');		
		$data['booking']  = $this->user_model->get_row('bookings',array('id'=>$bookings_id));
		if($_POST){
			$this->form_validation->set_message('numeric', 'Should be valid numeric quantity.');
			$this->form_validation->set_rules('guest_name', 'Guest Name', 'required');
			$this->form_validation->set_rules('email', 'Guest Email', 'required|valid_email');		
			$this->form_validation->set_rules('host_email', 'Host Email', 'required');
			$this->form_validation->set_rules('phone', 'phone', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');	
			$this->form_validation->set_rules('check_in', 'Chech In', 'required|callback_check_date_of_checkin');
	        $this->form_validation->set_rules('check_out', 'Chech out', 'required|callback_check_date_of_checkin');
	        if(isset($_POST['property_id'])){
	        	$this->form_validation->set_rules('property_id', 'Properties Title', 'required');
			}

			$this->form_validation->set_rules('due_amount', 'Due Amount', 'required|numeric');
			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');
			if($this->form_validation->run()==TRUE){
		
				$data=array(
							'guest_name' 	=>$this->input->post('guest_name'),
							'email'      	=>$this->input->post('email'),				
							'contact'    	=>$this->input->post('phone'),				
							'address'    	=>$this->input->post('address'),
															
							'check_in'   	=>date('Y-m-d',strtotime($this->input->post('check_in'))),									
							'check_out'  	=>date('Y-m-d',strtotime($this->input->post('check_out'))),													
							'due_amount' 	=>$this->input->post('due_amount'),								
							'modified'    	=>date('Y-m-d H:i:s')		
						   );
				if(isset($_POST['property_id'])){
	 				
	 				$data['pr_id'] = $this->input->post('property_id');
	 				

				}
				$this->user_model->update('bookings',$data,array('id' =>$bookings_id));		
				$this->session->set_flashdata('success_msg',"Successfully updated Booking. ");
				redirect('user/bookings');
			}
		}

		$data['template'] = 'user/edit_booking';
		$this->load->view('templates/user_template',$data);	 	
	}
	public function user_property(){
	 	$this->check_login();
	 	$resp ='';
		$user_email     =  $this->input->post('user_email');
		$user_details 	=  $this->user_model->get_row('users', array('user_email' => $user_email)); 

		if(empty($user_details)) {
			echo 'NO Record Found!!!!';
			die();
		}

		$properties = $this->user_model->get_result('properties',array('user_id' =>$user_details->id));


		if(!empty($properties)){
			foreach($properties as $row){
					$resp.='<div class="unique_feature">';
		            $resp.='<input onclick="get_amount('.$row->id.');" type="radio" name="property_id" value="'.$row->id.'" >';
		            $resp.= '&nbsp;&nbsp;<strong>'.$row->title.'</strong>';
		            $resp.='</div>';
		    }
		    
		    echo  $resp;        
		}
		else
		{
			echo 'NO Record Found';
		}
 	
 	}


 	public function ajax_propety_amount($id=0){
 		$this->check_login();
  		$data = array();
		$amount = 0;
		$property = $this->user_model->get_row('pr_info', array('pr_id' => $id));
		if ($property) {
			$amount = (2 * $property->rent)/100;
			$amount +=  $property->rent;
			echo json_encode(array('status' => 1, 'amount' => $amount));
		}
		else
			echo json_encode(array('status' => 0));
  	}

	
         /*Review Total Task starts*/
	public function review_about_you($offset=0){
		$this->check_login();
        $limit=10;
        $data['review']=$this->user_model->review_about_you($limit, $offset);
        $config = bnb_pagination();
        $config['base_url'] = base_url().'user/review_about_you/';
        $config['total_rows'] = $this->user_model->review_about_you(0,0);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;        
        $this->pagination->initialize($config);         
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'user/review';
        $this->load->view('templates/user_template', $data);
    }

    public function review_full_view($customer_id="",$property_id="",$review_id="",$last_page=""){
		$this->check_login();
	    if($customer_id == ""){
	        redirect('user/review_about_you');
	    }

	    $data['last_page'] = $last_page;

		$data['customer_info'] = $this->user_model->get_row('users', array('id'=>$customer_id));
		$data['property_info'] = $this->user_model->get_row('properties', array('id'=>$property_id));
		$data['review_info'] = $this->user_model->get_row('review', array('id'=>$review_id));
		$data['template'] = 'user/review_full_view';
		$this->load->view('templates/user_template', $data);
	}
 

	public function edit_review($user_id="",$property_id="",$review_id="",$last_page="")
	{
		if(empty($user_id) || empty($property_id) || empty($review_id) || empty($last_page))
		{
           redirect('user/review_about_you');
		}
	    $this->form_validation->set_rules('review', 'Review', 'required');
	        if($this->form_validation->run()==TRUE)
	        {
	        	$data = array(
	        		'review' => $this->input->post('review'), 
	        		'rating' => $this->input->post('rating') 
	        		);	
		        $this->user_model->update('review',$data ,array('id'=>$review_id));
		        $this->session->set_flashdata('success_msg','Review Has been updated successfully');
		        redirect('user/review_full_view/'.$user_id.'/'.$property_id.'/'.$review_id.'/'.$last_page);
	        }
		$data['user_info'] = $this->user_model->get_row('users', array('id'=>$user_id));
		$data['property_info'] = $this->user_model->get_row('properties', array('id'=>$property_id));
		$data['review_info'] = $this->user_model->get_row('review', array('id'=>$review_id));
		$data['template'] = 'user/edit_review';
		$this->load->view('templates/user_template', $data);
    }

	public function review_reply($user_id="",$property_id="",$review_id="",$last_page="")
	{

		if(empty($user_id) || empty($property_id) || empty($review_id) || empty($last_page))
		{
           redirect('user/review_about_you');
		}

	    $this->form_validation->set_rules('review', 'Review', 'required');
	        if($this->form_validation->run()==TRUE)
	        {
	        	$data = array(
	        		'review' => $this->input->post('review'),
	        		'review_id'=>$review_id,
	        		'replier_id'=>get_my_id(),
	        		'created'=> time(), 
	        		);	
		        $this->user_model->insert('review_reply',$data);
		        $this->session->set_flashdata('success_msg','Reply To this Review Has been Added successfully');
		        redirect('user/review_full_view/'.$user_id.'/'.$property_id.'/'.$review_id.'/'.$last_page);
	        }
		$data['user_info'] = $this->user_model->get_row('users', array('id'=>$user_id));
		$data['review_info'] = $this->user_model->get_row('review', array('id'=>$review_id));
		$data['template'] = 'user/review_reply';
		$this->load->view('templates/user_template', $data);
    }

    
 	public function review_delete($id="",$last_page=""){
        $this->check_login();
        if($id == ""){
            redirect('user/review_about_you');
        }
        $this->user_model->delete('review', array('id'=>$id));
        $this->session->set_flashdata('success_msg',"Review has been Deleted successfully.");
        redirect('user/'.$last_page);
    }

    public function review_by_you($offset=0){
		$this->check_login();
        $limit=10;
        $data['review']=$this->user_model->review_by_you($limit, $offset);
        $config = bnb_pagination();
        $config['base_url'] = base_url().'user/review_by_you/';
        $config['total_rows'] = $this->user_model->review_by_you(0,0);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;        
        $this->pagination->initialize($config);         
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'user/review_by_you';
        $this->load->view('templates/user_template', $data);
    }

    /*Review Total Task Ends*/


  //   public function change_profile_image()
 	// {
 	// 	$this->check_login();
  //       $user = $this->session->userdata('user_info');
  //       $id = $user['id'];
  //       $row = $this->user_model->get_row('users', array('id'=>$id));
  //       @$old_image = $row->image;
		// if(@$_FILES['userfile']['name'] !="")
		// {
		//    $image = $this->do_core_upload('userfile','./assets/uploads/profile_image/');
		//    $this->user_model->update('users',array('image' => $image),array('id' => $id));
		//    if(!empty($old_image))
		//    {
		//     $path = './assets/uploads/profile_image/';
		//     @unlink($path.$old_image);
		//    }
		//    $this->session->set_flashdata('success_msg',"Image has been Changed successfully.");
		//    redirect('user/change_profile_image');
		// }    
  //       $data['user'] = $this->user_model->get_row('users', array('id'=>$id));
  //       $data['template'] = 'user/change_profile_pic';
  //       $this->load->view('templates/user_template', $data);
 	// }


	public function do_core_upload($filename2='user_file' , $upload_path='./assets/uploads/custom_uploads/', $path_of_thumb='')
	{
		$this->check_login();
		$allowed =  array('gif','png','jpg','jpeg','mp4');
		$filename = $_FILES[$filename2]['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ){
			return FALSE;
		}
		else{
			
			if ($_FILES[$filename2]["error"] > 0){
	 			return FALSE; 
	 		}
			else{
			 $name = uniqid();
			 if(move_uploaded_file($_FILES[$filename2]['tmp_name'],$upload_path.$name.'.'.$ext))
			 return $name.'.'.$ext;
			 else
			 return FALSE;
			}
		} 
	}
	
	public function properties($offset=0)
	{
        $this->check_login();
        $user = $this->session->userdata('user_info');
		$user_id = $user['id'];
		$limit=10;
		$data['properties'] = $this->user_model->get_pagination_where('properties', $limit, $offset, array('user_id'=>$user_id,'is_delete !='=>1));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/properties/';
		$config['total_rows'] = $this->user_model->get_pagination_where('properties', 0, 0, array('user_id'=>$user_id, 'is_delete !='=>1));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/properties';
        $this->load->view('templates/user_template', $data);
	}



	public function FavoritesCategory($offset=0)
	{
	    $this->check_login();
	    $limit=10;
	    $user = $this->session->userdata('user_info');
	    $data['FavoritesCategory']=$this->user_model->get_pagination_where('favorites_category',$limit, $offset,array('user_id'=>$user['id']));
	    $config = bnb_pagination();
	    $config['base_url'] = base_url().'user/FavoritesCategory/';
	    $config['total_rows'] = $this->user_model->get_pagination_where('favorites_category',0,0,array('user_id'=>$user['id']));
	    $config['per_page'] = $limit;
	    $config['num_links'] = 5;        
	    $this->pagination->initialize($config);         
	    $data['pagination'] = $this->pagination->create_links();
	    $data['template'] = 'user/FavoritesCategory';
	    $this->load->view('templates/user_template', $data);
	}


	public function addFavoritesCategory(){ 
		$this->check_login();
		$this->form_validation->set_rules('name','Name','required');
		if(isset($_FILES['image']['name']) && $_FILES['image']['name']=="")
		{
    		$this->form_validation->set_rules('image','Image','required');
		}
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');   

	    if($this->form_validation->run() == TRUE)
	    {   
             $data = array(
             	           'name'=>$_POST['name'],
             	           'created'=>date('Y-m-d h:i:s'),
             	           'user_id'=>get_my_id(),
             	           );
			if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
			{
				  $data['image'] = $this->do_core_upload('image','./assets/uploads/favorites_category/');
	        }
         $this->user_model->insert('favorites_category',$data);
         $this->session->set_flashdata('success_msg',"Favorites Category has been added Successfully.");
         redirect('user/FavoritesCategory');
	    } 
		$data['template'] = 'user/addFavoritesCategory';
		$this->load->view('templates/user_template', $data);
    }

	public function editFavoritesCategory($id=""){ 
		$this->check_login();
		$data['category_info'] = $this->user_model->get_row('favorites_category',array('id'=>$id));
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');   
	    if($this->form_validation->run() == TRUE)
	    {   
             $updatedata = array(
             	           'name'=>$_POST['name'],
             	           'updated'=>date('Y-m-d h:i:s'),
             	           );
			if(isset($_FILES['image']['name']) && $_FILES['image']['name']!="")
			{
				  $updatedata['image'] = $this->do_core_upload('image','./assets/uploads/favorites_category/');
				if (!empty($data['category_info']->image)) 
				{
					$path = './assets/uploads/favorites_category/';
					$delete_file = $data['category_info']->image;
					@unlink($path.$delete_file);
				}
	        }
         $this->user_model->update('favorites_category',$updatedata,array('id'=>$id));
         $this->session->set_flashdata('success_msg',"Favorites Category has been updated Successfully.");
         redirect('user/FavoritesCategory');
	    } 
		$data['template'] = 'user/editFavoritesCategory';
		$this->load->view('templates/user_template', $data);
    }


    public function deleteFavoritesCategory($id=""){
        $this->check_login();
        if($id == ""){
            redirect('user/FavoritesCategory');
        }
		$data['category_info'] = $this->user_model->get_row('favorites_category',array('id'=>$id));
		if (!empty($data['category_info']->image)) 
		{
			$path = './assets/uploads/favorites_category/';
			$delete_file = $data['category_info']->image;
			@unlink($path.$delete_file);
		}
        $this->user_model->delete('favorites_category', array('id'=>$id));
        $this->user_model->delete('favorites_property', array('favorites_category_id'=>$id));
        $this->session->set_flashdata('success_msg',"Favorites Category has been Deleted successfully.");
        redirect('user/FavoritesCategory');
    }

      public function favorites_property($favorites_category_id="",$offset=0){
        $this->check_login();
        $limit=10;
        $data['favorites_property']=$this->user_model->user_favorites_property($limit, $offset,$favorites_category_id);
        $config = bnb_pagination();
        $config['base_url'] = base_url().'user/favorites_property/';
        $config['total_rows'] = $this->user_model->user_favorites_property(0,0,$favorites_category_id);
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        $config['num_links'] = 5;        
        $this->pagination->initialize($config);         
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'user/favorites_property';
        $this->load->view('templates/user_template', $data);
    }

    public function view_favorites_property($customer_id="",$property_id=""){
        $this->check_login();
        if($customer_id == ""){
            redirect('user/favorites_property');
        }

        $data['customer_info'] = $this->user_model->get_row('users', array('id'=>$customer_id));
        $data['property_info'] = $this->user_model->get_row('properties', array('id'=>$property_id));
        $data['template'] = 'user/view_favorites_property';
        $this->load->view('templates/user_template', $data);
    } 

    public function delete_favorites_property($id=""){
        $this->check_login();
        if($id == ""){
            redirect('user/favorites_property');
        }
        $this->user_model->delete('favorites_property', array('id'=>$id));
        $this->session->set_flashdata('success_msg',"Favorites has been Deleted successfully.");
        redirect('user/favorites_property');
    }

//////////////////// my  property  ////////////////////
	public function delete_property($pr_id='')
	{	
		$this->check_login();
		if(empty($pr_id)) redirect('user/properties');

        $this->user_model->delete('pr_calender',array('property_id'=>$pr_id));   
        $this->user_model->delete('listing_price',array('listing_id'=>$pr_id));   
        $this->user_model->delete('property_rating',array('pr_id'=>$pr_id));   
        $this->user_model->delete('property_email_record',array('id'=>$pr_id));   
        $this->user_model->delete('property_notes',array('id'=>$pr_id));   
        $this->user_model->delete('review',array('property_id'=>$pr_id));  
        $pr_nb = $this->user_model->get_row('pr_nb_relation',array('pr_nb_id'=>$pr_id,'parent_id'=>0)); 
        $this->user_model->delete('pr_nb_relation',array('parent_id'=>$pr_nb->id));  
        $pr_gallery = $this->user_model->get_result('pr_gallery',array('pr_id'=>$pr_id)); 
   	    if(empty($pr_gallery))
   	    {
   	    	foreach ($pr_gallery as $value)
   	    	{
               @unlink('.assets/uploads/property/'.$value->image_file);
   	    	}
   	    }

		$this->user_model->update('properties',array('is_delete'=>1,'status'=>0),array('id'=>$pr_id));

		$this->session->set_flashdata('success_msg',"property has been deleted successfully.");
		redirect('user/properties');
	}


	public function get_neighbour_city()
	{  

		$city_name = $_POST['city_name']; 
		$city_row =  $this->user_model->get_row('cities',array('city'=>$city_name));
        if(!empty($city_row))
        {
        	echo $city_row->id;
        }
        else
        {
        	return false;
        }
	}

    

    public function get_neighbourhoot_of_city()
    {
    	$city = $this->input->post('city');
    	$nb   = get_neighbourhoot_of_city($city);
    	$res  = "";
    	if(!empty($nb))
    	{
    		$res .= "<option value=''>Select neighbour</option>";
	    	foreach($nb as $row)
	    	{
	    		$res .= "<option value='".$row->id."'>".$row->title."</option>";
	    	}
	    }
	    else
	    {
    		$res = "<option value=''>No Neighbourhood</option>";
	    }	
    	echo $res;
    }

    public function get_neighbourhoot_of_city_on_edit_page()
    {
    	$city = $this->input->post('city');
    	$property_id = $this->input->post('property_id');
    	$nb   = get_neighbourhoot_of_city($city);
    	$res  = "";
    	if(!empty($nb))
    	{
    		$res .= "<option value=''>Select neighbour</option>";
	    	foreach($nb as $row)
	    	{
               $response = check_neighbourhoot_of_property($property_id,$row->id);
		       if($response!="")
		       {
                    $res .= "<option selected value='".$row->id."'>".$row->title."</option>";
		       }
		       else
		       {
                    $res .= "<option value='".$row->id."'>".$row->title."</option>";
		       }
		    }
	    }
	    else
	    {
    		$res = "<option value=''>No Neighbourhood</option>";
	    }	
    	echo $res;
    }



    public function get_neighbourhoot_of_city01($pr_id = ""){
    	if($pr_id == "")
    		return FALSE;

    	$city = $this->input->post('city');
    	$nb   = get_neighbourhoot_of_city($city);
    	$res  = "";
    	$selected = get_neighbourhoot_of_property($pr_id);
    	print_r($selected); die();
    	if(!empty($nb)){
    		$res .= "<option value=''>Select neighbour</option>";
	    	foreach($nb as $row)
	    	{
	    		$res .= "<option value='".$row->id."'>".$row->title."</option>";
	    	}
	    }
	    else
	    {
    		$res = "<option value=''>No Neighbourhood</option>";
	    }	
    	echo $res;
    }

	public function listing_property()
	{
		if(!$this->session->userdata('user_info'))
		{
			redirect('user');
		}
		$user = $this->session->userdata('user_info');
		$user_id = $user['id'];

		$this->form_validation->set_rules('property_title', 'Property Title', 'trim|required');
		$this->form_validation->set_rules('property_descrip', 'Property Description', 'trim|required');
		$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
		$this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');
		$this->form_validation->set_rules('address', 'Address ', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required|numeric');
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('bedroom', 'Bedroom', 'trim|required|numeric');
		$this->form_validation->set_rules('bathroom', 'Bathroom', 'trim|required|numeric');
		$this->form_validation->set_rules('accommodates', 'Accommodate', 'trim|required');
		$this->form_validation->set_rules('size', 'Property Size', 'trim|required|numeric');
		$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');
		$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
		if($this->form_validation->run() == TRUE){
			$array= array(
							  'property_title'  => $this->input->post('property_title'),
							  'property_descrip'   => $this->input->post('property_descrip'),
							  'property_type'=> $this->input->post('property_type'),
							  'bed_type'     => $this->input->post('bed_type'),
							  'address'      => $this->input->post('address'),
							  'city'         => $this->input->post('city'),
							  'state'        => $this->input->post('state'),
							  'zipcode'      => $this->input->post('zipcode'),
							  'country'      => $this->input->post('country'),
							  'bed'	         => $this->input->post('bedroom'),
							  'bath'         => $this->input->post('bathroom'),
							  'size'         => $this->input->post('size'),
							  'accommodate'  => $this->input->post('accommodates'),
							  'room_type'    => $this->input->post('room_type'),
						  	  'latitude'     => $this->input->post('latitude'),
						  	  'longitude'    => $this->input->post('longitude'),
						  	  'created'      => date('Y-m-d H:i:s'),
						  	  'user_id'      => $user_id
						);
			
			if($this->input->post('neighbourhood'))
			{
				$neighbour_id = $this->input->post('neighbourhood');
				$this->session->set_userdata('neighbour_id',$neighbour_id);
			}
			
			$this->session->set_userdata('pr_info',$array);
			redirect('user/add_property_detail');
		}
		$data['template'] = 'user/listing_property';
        $this->load->view('templates/user_template', $data);	
	}

	public function add_property_detail()
	{
		$test = $this->session->userdata('pr_info');
        if(!$test) redirect('user/listing_property');

        $user = $this->session->userdata('user_info');
		$user_id = $user['id'];

		$this->form_validation->set_rules('rent', 'Property Rent', 'trim|required|numeric');
		$this->form_validation->set_rules('room_allotment', 'Room Allotment', 'trim|required');
		$this->form_validation->set_rules('deposit', 'Deposit', 'trim|required|numeric');
		$this->form_validation->set_rules('appliction_fee', 'Appliction Fee', 'trim|required|numeric');
		//$this->form_validation->set_rules('contact_name', 'User Name ', 'trim|required');
		//$this->form_validation->set_rules('user_email', 'User Email', 'trim|required|valid_email');
		//$this->form_validation->set_rules('website', 'Website URL', 'trim|required');
		//$this->form_validation->set_rules('phone', 'User Contact', 'trim|required');
		$this->form_validation->set_rules('collection_category', 'Collection Category', 'trim|required');
		$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
		if($this->form_validation->run() == TRUE)
		{
			$image = $this->do_core_upload('userfile','./assets/uploads/property/');

			$array= array(
						  'rent'  			=> $this->input->post('rent'),
						  'room_allotment'  => $this->input->post('room_allotment'),
						  'deposit'     	=> $this->input->post('deposit'),
						  'appliction_fee'  => $this->input->post('appliction_fee'),
						  //'contact_name'    => $this->input->post('contact_name'),
						 // 'user_email'      => $this->input->post('user_email'),
						  //'website'         => $this->input->post('website'),
						  'featured_image'  => $image,
						  'pr_collect_cat_id'=> $this->input->post('collection_category'),
						  'phone'           => $this->input->post('phone')
						);
			$arr=$this->session->userdata('pr_info');
			$properties= array( 
				'title'          =>$arr['property_title'],
				'description'    => $arr['property_descrip'],
				'room_allotment' => $array['room_allotment'],
				'deposit_amount' => $array['deposit'],
				'application_fee'=> $array['appliction_fee'],
				'slug'           =>str_replace(' ','-', $arr['property_title']),
				'created'        =>$arr['created'],
				'user_id'        =>$user_id,
				'featured_image' =>$array['featured_image'],
				'latitude'       =>$arr['latitude'],
				'longitude'      =>$arr['longitude']
				);
			$property_id=$this->user_model->insert('properties',$properties);
			
			if($this->session->userdata('neighbour_id'))
			{
				$parent       = array('pr_nb_id'=>$property_id, 'parent_id'=>0 );
				$parent_id    = $this->user_model->insert('pr_nb_relation',$parent);
				$neighbour_id = $this->session->userdata('neighbour_id');
				foreach($neighbour_id as $nb_id)
				{
					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
					$this->user_model->insert('pr_nb_relation',$nb);
				}
				$this->session->unset_userdata('neighbour_id');
			}	


			$properties_info=array( 
									'pr_id'              => $property_id,
									'unit_street_address'=> $arr['address'],
									'property_type_id'   => $arr['property_type'],
									'city'               => $arr['city'],
									'rent'               => $array['rent'],
									'state'=> $arr['state'],
									'country'=> $arr['country'],
									'bed_type'=>$arr['bed_type'],
									'bed'=>$arr['bed'],
									'bath'=>$arr['bath'],
									'accommodates'=>$arr['accommodate'],
									'square_feet'=>$arr['size'],
									'room_type'=>$arr['room_type'],
									'pr_collect_cat_id'=>$array['pr_collect_cat_id'],
									'zip'=>$arr['zipcode'],
								);
								// $properties_contact_info=array( 
								// 	'pr_id'=>$property_id,
								// 	'contact_name'=> $array['contact_name'],
								// 	'phone'=> $array['phone'],
								// 	'contact_email'=> $array['user_email'],
								// 	'website'=> $array['website'],
									
								// );
			$u_info = $this->session->userdata('user_info');
			$user_info = $this->user_model->get_row('users',array('id'=>$u_info['id']));


								$properties_contact_info=array( 
									'pr_id'=>$property_id,
									'contact_name'=> $user_info->first_name." ".$user_info->last_name,
									'phone'=> $user_info->phone,
									'contact_email'=> $user_info->user_email,
									//'website'=> $array['website'],
									
								);
			$this->user_model->insert('pr_info',$properties_info);
			$status = $this->user_model->insert('pr_contact_info',$properties_contact_info);
			if(isset($_POST['amenities'])){
				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
					if(!empty($_POST['amenities'][$i])){
						$pr_amenities=array(
						'pr_id'=>$property_id,
						'pr_amenity_id'=>trim($_POST['amenities'][$i])
						);
						$this->user_model->insert('pr_amenities',$pr_amenities);	
					}
				}
			}
			if($status){
	           //////////commision For Inviter Starts ///////////////
	           //////////commision For Inviter Starts ///////////////
				$current_user_info = $this->session->userdata('user_info');
				$gain = $this->user_model->get_row('invitation',array('joined_user_id'=>$current_user_info['id']));
	            if(!empty($gain) && $gain->property_credit_given==0)
	            {
				    $this->user_model->update('invitation',array('property_credit_given'=>1),array('joined_user_id'=>$current_user_info['id']));
	            }
	           //////////commision For Inviter Ends////////////////
	           //////////commision For Inviter Ends////////////////
				$this->session->unset_userdata('pr_info');
				$this->session->set_flashdata('success_msg',"Property has been added successfully.");
				redirect('user/properties');
			}
		}
        $data['user'] = $this->session->userdata('user_info');
		$data['template'] = 'user/add_pr_detail';
        $this->load->view('templates/user_template', $data);
	}

	public function edit_property($pr_id='')
	{
        $this->check_login();
        $user = $this->session->userdata('user_info');
		$user_id = $user['id'];

		if(empty($pr_id)) redirect('user/properties');
		$data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
		$data['pr_amenities'] = $this->user_model->get_row('pr_amenities',array('pr_id'=>$pr_id));
		$data['pr_contact_info'] = $this->user_model->get_row('pr_contact_info',array('pr_id'=>$pr_id));
		$data['pr_info'] = $this->user_model->get_row('pr_info',array('pr_id'=>$pr_id));
		$this->form_validation->set_rules('property_title', 'Property Title', 'trim|required');
		$this->form_validation->set_rules('property_descrip', 'Property Description', 'trim|required');
		$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
		$this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');
		$this->form_validation->set_rules('address', 'Address ', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required|numeric');
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('bedroom', 'Bedroom', 'trim|required|numeric');
		$this->form_validation->set_rules('bathroom', 'Bathroom', 'trim|required|numeric');
		$this->form_validation->set_rules('accommodates', 'Accommodate', 'trim|required');
		$this->form_validation->set_rules('size', 'Property Size', 'trim|required|numeric');
		$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');
		$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
		if($this->form_validation->run() == TRUE)
		{
			$array= array(
							  'property_title'  => $this->input->post('property_title'),
							  'property_descrip'   => $this->input->post('property_descrip'),
							  'property_type'     => $this->input->post('property_type'),
							  'bed_type'        => $this->input->post('bed_type'),
							  'address'       => $this->input->post('address'),
							  'city'       => $this->input->post('city'),
							  'state'     => $this->input->post('state'),
							  'zipcode'   => $this->input->post('zipcode'),
							  'country'   => $this->input->post('country'),
							  'bed'	      =>$this->input->post('bedroom'),
							  'bath'      =>$this->input->post('bathroom'),
							  'size'      =>$this->input->post('size'),
							  'accommodate'=>$this->input->post('accommodates'),
							  'room_type'  =>$this->input->post('room_type'),
						  	  'latitude'  =>$this->input->post('latitude'),
						  	  'longitude' =>$this->input->post('longitude'),
						  	  'created'   => date('Y-m-d H:i:s')
						);

			if($this->input->post('neighbourhood'))
			{
				$neighbour_id = $this->input->post('neighbourhood');
				$this->session->set_userdata('neighbour_id',$neighbour_id);
			}

			$this->session->set_userdata('pr_info',$array);
			redirect('user/edit_property_detail/'.$pr_id);
		}
		$data['template'] = 'user/edit_property';
        $this->load->view('templates/user_template', $data);
	}

	public function edit_property_detail($pr_id='')
	{
        $this->check_login();
        $user = $this->session->userdata('user_info');
		$user_id = $user['id'];

		if(empty($pr_id)) redirect('user/properties');
		
		$data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
		$data['pr_amenities'] = $this->user_model->get_result('pr_amenities',array('pr_id'=>$pr_id));
		$data['pr_contact_info'] = $this->user_model->get_row('pr_contact_info',array('pr_id'=>$pr_id));
		$data['pr_info'] = $this->user_model->get_row('pr_info',array('pr_id'=>$pr_id));
		
		$this->form_validation->set_rules('rent', 'Property Rent', 'trim|required');
		$this->form_validation->set_rules('room_allotment', 'Room Allotment', 'trim|required');
		$this->form_validation->set_rules('deposit', 'Property Deposit', 'trim|required');
		$this->form_validation->set_rules('appliction_fee', 'Appliction Fee', 'trim|required');
		//$this->form_validation->set_rules('website', 'Website', 'trim|required');
		$this->form_validation->set_rules('collection_category', 'Collection Category', 'trim|required');
		//$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	

		if($this->form_validation->run() == TRUE)
		{
			$image = $data['properties']->featured_image;
			if ($_FILES['userfile']['name']) 
			{
				$image = $this->do_core_upload('userfile','./assets/uploads/property/');
				if (!empty($data['properties']->featured_image)) 
				{
					$path = './assets/uploads/property/';
					$delete_file = $data['properties']->featured_image;
					@unlink($path.$delete_file);
				}
			}

			$array= array(
							  'rent'             => $this->input->post('rent'),
							  'room_allotment'   => $this->input->post('room_allotment'),
							  'deposit'          => $this->input->post('deposit'),
							  'appliction_fee'   => $this->input->post('appliction_fee'),
							  //'contact_name'     => $this->input->post('contact_name'),
							  //'website'          => $this->input->post('website'),
							  'user_id'          =>$this->input->post('user_id'),
							  'pr_collect_cat_id'=> $this->input->post('collection_category'),
							  //'phone'            => $this->input->post('phone')
						);

			
			$arr=$this->session->userdata('pr_info');
			
			$properties= array( 
				'title'=>$arr['property_title'],
				'description'=> $arr['property_descrip'],
				'room_allotment'=> $array['room_allotment'],
				'deposit_amount'=> $array['deposit'],
				'application_fee'=> $array['appliction_fee'],
				'slug'=> str_replace(' ','-', $arr['property_title']),
				'created'=> $arr['created'],
				'user_id'=> $user_id,
				'featured_image' => $image,
				'latitude' =>$arr['latitude'],
				'longitude'=>$arr['longitude']
				);
			

			$this->user_model->update('properties',$properties,array('id' =>$pr_id));

			if($this->session->userdata('neighbour_id'))
			{
				$neighbour_list = get_neighbourhoot_of_property($pr_id);
				
				$this->user_model->delete('pr_nb_relation', array('pr_nb_id' => $pr_id, 'parent_id'=>0));
				if(!empty($neighbour_list))
				{
					foreach($neighbour_list as $list)
					{
						$this->user_model->delete('pr_nb_relation', array('id'=>$list->id) );
					}
				}

				$parent     = array('pr_nb_id'=>$pr_id, 'parent_id'=>0 );
				$parent_id  = $this->user_model->insert('pr_nb_relation',$parent);
				$neighbour_id   = $this->session->userdata('neighbour_id');
				foreach($neighbour_id as $nb_id)
				{
					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
					$this->user_model->insert('pr_nb_relation',$nb);
				}
				$this->session->unset_userdata('neighbour_id');

			}	

			
			$properties_info=array( 
									
									'unit_street_address'=> $arr['address'],
									'property_type_id'=>$arr['property_type'],
									'city'=> $arr['city'],
									'rent'=> $array['rent'],
									'state'=> $arr['state'],
									'country'=> $arr['country'],
									'bed_type'=>$arr['bed_type'],
									'bed'=>$arr['bed'],
									'bath'=>$arr['bath'],
									'accommodates'=>$arr['accommodate'],
									'square_feet'=>$arr['size'],
									'room_type'=>$arr['room_type'],
									'pr_collect_cat_id'=>$array['pr_collect_cat_id'],
									'zip'=>$arr['zipcode'],
								);
			$u_info = $this->session->userdata('user_info');
			$user_info = $this->user_model->get_row('users',array('id'=>$u_info['id']));

			$properties_contact_info=array( 
         									'phone'=> $user_info->phone,
		        							//'website'=> $user_info['website'],
                                             );
			// $properties_contact_info=array( 
									
			// 						'phone'=> $array['phone'],
			// 						'website'=> $array['website'],
									
			// 					);

			$this->user_model->update('pr_info',$properties_info,array('pr_id' =>$pr_id));
			$status = $this->user_model->update('pr_contact_info',$properties_contact_info,array('pr_id' =>$pr_id));
			if(isset($_POST['amenities'])){
				$flag_pf=TRUE;
				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
					if($flag_pf){
						$this->user_model->delete('pr_amenities',array('pr_id'=>$pr_id));
						$flag_pf=FALSE;
					}
					if(!empty($_POST['amenities'][$i])){
						$pr_amenities=array(
						'pr_id'=>$pr_id,
						'pr_amenity_id'=>$_POST['amenities'][$i]
						);
						$this->user_model->insert('pr_amenities',$pr_amenities,array('pr_id' =>$pr_id));	
					}
				}
			}

			if($status){
				$this->session->unset_userdata('pr_info');
				$this->session->set_flashdata('success_msg',"Property has been updated successfully.");
				redirect('user/properties');
			}
		}
		$data['template'] = 'user/edit_pr_detail';
        $this->load->view('templates/user_template', $data);
	}


    /*Groups Starts*/


	public function groups($offset=0){
		$this->check_login();
		$limit=10;
		$data['groups']=$this->user_model->get_pagination_result('groups',$limit, $offset);
		$config= bnb_pagination();	
		$config['base_url'] = base_url().'user/groups/';
		$config['total_rows'] = $this->user_model->get_pagination_result('groups',0,0);
		$config['per_page'] = $limit;
		$config['num_links'] =3;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();
		$data['total_row'] = $config['total_rows'];
		$data['template'] = 'user/groups';
        $this->load->view('templates/user_template', $data);	
	}

	public function join_group($group_id="",$flag=""){
	    $owner_info = $this->session->userdata('user_info');
	    if($flag=="leave"){
     	   $this->user_model->delete('group_members',array('member_id'=>$owner_info['id'],'group_id'=>$group_id));
     	   $this->user_model->delete('group_members_properties',array('member_id'=>$owner_info['id'],'group_id'=>$group_id));
     	   $this->session->set_flashdata('success_msg','You have successfully Leave this Group.');
	       redirect('user/groups');
	    }
	    else{
		    $data = array(
		       	     'member_id'=>$owner_info['id'],
		       	     'member_role'=>$owner_info['user_role'],
		       	     'group_id' => $group_id,
		       	     'created'=>date('Y-m-d h:i:s'),
		       	     );
		   $this->user_model->insert('group_members',$data);
     	   $this->session->set_flashdata('success_msg','You have successfully Join this Group.');
	       redirect('user/groups');
	    }
    }

    /*Groups Ends*/

    /*View Members To the Group Starts*/

    public function view_group_members($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('user/groups');
			}
	    $limit = 2;
	    $data['group_id'] = $group_id;
		$data['template'] = 'user/group_members';
	    $this->load->view('templates/user_template', $data);		
    }

    public function customer_group_members($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('user/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $data['members'] = $this->user_model->get_customer_members($group_id,$limit,$offset);
	    $config = bnb_pagination();
		$config['base_url'] = base_url().'user/customer_group_members/'.$group_id;
		$config['total_rows'] = $this->user_model->get_customer_members($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'user/customer_group_members';
	    $this->load->view('templates/user_template', $data);		
    }

	public function owner_group_members($group_id="",$offset=0){
        $this->check_login();
			if($group_id==""){
				redirect('user/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $data['members'] = $this->user_model->get_owner_members($group_id,$limit,$offset);
	    $config= bnb_pagination();
		$config['base_url'] = base_url().'user/owner_group_members/'.$group_id;
		$config['total_rows'] = $this->user_model->get_owner_members($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'user/owner_group_members';
	    $this->load->view('templates/user_template', $data);		
    }

    /*View Members To the Group Ends*/

    /*View Properties To the Group Starts*/

    public function view_group_properties($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('user/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $data['members_properties'] = $this->user_model->group_members_properties($group_id,$limit,$offset);
	    $config= bnb_pagination();
		$config['base_url'] = base_url().'user/view_group_properties/'.$group_id;
		$config['total_rows'] = $this->user_model->group_members_properties($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'user/view_group_properties';
	    $this->load->view('templates/user_template', $data);		
    }

    public function add_property_to_group($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('user/groups');
			}
		$user_info = $this->session->userdata('user_info');
		$user_id = $user_info['id'];
	    $limit = 10;
	    $data['group_id'] = $group_id;
		$data['properties'] = $this->user_model->get_pagination_where('properties', $limit, $offset, array('user_id'=>$user_id));
	    $config= bnb_pagination();
		$config['base_url'] = base_url().'user/add_property_to_group/'.$group_id;
		$config['total_rows'] = $this->user_model->get_pagination_where('properties',0,0, array('user_id'=>$user_id));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'user/add_property_to_group';
	    $this->load->view('templates/user_template', $data);		
    }
   
   public function one_click_add_property(){
    $group_id = $this->input->post('group_id');
   	$property_id = $this->input->post('property_id');
   	$user_info = $this->session->userdata('user_info');
    $member_id = $user_info['id'];
	$response = $this->user_model->get_row('group_members_properties',array('member_id'=>$member_id,'property_id'=>$property_id,'group_id'=>$group_id));
		if($response==""){
		$data = array(
					'member_id'=>$member_id,
					'group_id'=>$group_id,
					'property_id'=>$property_id,
					'created' =>date('Y-m-d h:i:s'),
					);
		 $this->user_model->insert('group_members_properties',$data);
         echo "add";
		}
		else{
         	$this->user_model->delete('group_members_properties',array('member_id'=>$member_id,'property_id'=>$property_id,'group_id'=>$group_id));
			echo "delete";
		}
   }

    public function owner_added_property($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('user/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $user_info = $this->session->userdata('user_info');
	    $user_id = $user_info['id'];
	    $data['members_properties'] = $this->user_model->group_members_properties($group_id,$limit,$offset,$user_id);
	    $config= bnb_pagination();
		$config['base_url'] = base_url().'user/owner_added_property/'.$group_id;
		$config['total_rows'] = $this->user_model->group_members_properties($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'user/owner_added_property';
	    $this->load->view('templates/user_template', $data);		
    }

    public function view_owner_added_property($property_id="",$group_id=""){
		$this->check_login();
    	if($property_id==""){
    		redirect('user/owner_added_property/'.$group_id);
    	}
      $data['group_id'] = $group_id;
      $data['properties_info'] = get_property_detail($property_id);
      $data['template'] = 'user/view_owner_added_property';
      $this->load->view('templates/user_template', $data);		
    }

    public function delete_owner_added_property($id="",$group_id=""){
		$this->check_login();
		if(empty($id)) redirect('user/owner_added_property/'.$group_id);		
		$this->user_model->delete('group_members_properties',array('id'=>$id));
		$this->session->set_flashdata('success_msg',"property has been Removed successfully From The group.");
		redirect('user/owner_added_property/'.$group_id);
	}

	public function check_membership(){
		$group_id = $this->input->post('group_id');
		$user_info = $this->session->userdata('user_info');
		$response = $this->user_model->get_row('group_members',array('member_id'=>$user_info['id'],'group_id'=>$group_id));
	     if($response=="")
	     {
	     	echo "invalid";
	     }
	     else{
	     	echo "valid";
	     }
	}
    /*View Properties To the Group Ends*/
    public function change_profile_image()
 	{
 		$this->check_login();
        $user = $this->session->userdata('user_info');
        $id = $user['id'];
        $row = $this->user_model->get_row('users', array('id'=>$id));
        @$old_image = $row->image;
		if(@$_FILES['userfile']['name'] !="")
		{
		   $image = $this->do_core_upload('userfile','./assets/uploads/profile_image/');
		   $this->user_model->update('users',array('image' => $image),array('id' => $id));
			$this->user_model->insert('user_pic_gallery',array('image' => $image,'status'=>1,'user_id'=>$id));
		   $this->session->set_flashdata('success_msg',"Image has been Changed successfully.");
		   redirect('user/see_all_images');
		}    
        $data['user'] = $this->user_model->get_row('users', array('id'=>$id));
        $data['template'] = 'user/change_profile_pic';
        $this->load->view('templates/user_template', $data);
 	}
	
   /* Ajax upload Drag and drop images starts*/
   
   public function ajax_drag_drop(){
   	$file_name = $_FILES['pic']['name'];
	if(@$file_name !="")
	{
	   $image = $this->do_core_upload('pic','./assets/uploads/profile_image/');
	   $user_info = $this->session->userdata('user_info');
	   $info_data = array(
	   	                  'user_id'=>$user_info['id'],
	   	                  'image'=>$image,
	   	                  'created'=>date('Y-m-d h:i:s'),
	                      ); 
	   $this->user_model->insert('user_pic_gallery',$info_data);
	   
	   $str = "File Uploaded Succesfullly";
	   echo json_encode(array('status'=>$str));
	   exit;
       
	}
	else{
	   $str = "Something went Wrong ";
	   echo json_encode(array('status'=>$str));
	   exit;
	}    

   	
   } 
	
   /* Ajax upload Drag and drop images Endss*/	

   /*See All Images Starts*/
    public function see_all_images()
    {
 		$this->check_login();
		$user_info = $this->session->userdata('user_info');
		$data['gallery'] = $this->user_model->get_result('user_pic_gallery',array('user_id'=>$user_info['id']));
	    $data['template'] = 'user/see_all_images';
		$this->load->view('templates/user_template', $data);		
    }
 	/*See All Images Ends*/

 	/*ajax Change profile images starts*/

    public function ajax_change_profile_image()
 	{
 		$this->check_login();
 		$image_new =  $this->input->post('image');
        $user = $this->session->userdata('user_info');
        $id = $user['id'];
        $row = $this->user_model->get_row('users', array('id'=>$id));
        $old_image = $row->image;
			if($image_new!="")
			{
			   $this->user_model->update('users',array('image' => $image_new),array('id' => $id));
			   $this->user_model->update('user_pic_gallery',array('status'=>0),array('user_id'=>$id));
			   $this->user_model->update('user_pic_gallery',array('status'=>1),array('image'=>$image_new));
			   echo "image has been changed";
			}    
 	}   

 	/*ajax Change profile images Ends*/

 	/*ajax delete image  starts*/
 	public function ajax_delete_image(){
 		$this->check_login();
	    $unique_id = $_POST['unique_id'];
	    $user_info = $this->session->userdata('user_info');
	    $row = $this->user_model->get_row('user_pic_gallery',array('id'=>$unique_id));
		$this->user_model->delete('user_pic_gallery',array('id'=>$unique_id));
	    $old_image = $row->image;
	    if($row->status==1){
		$this->user_model->update('users',array('image'=>''),array('id'=>$user_info['id']));
	    }
		if(!empty($old_image))
		{
		$path = './assets/uploads/profile_image/';
		@unlink($path.$old_image);
		echo "deleted" ;
		} 		
    }


     public function invite(){
    	$this->check_login();
    	$data['template'] = 'user/invite';
        $this->load->view('templates/user_template', $data);
    }

    public function sendinvite(){
 		$this->check_login();
    	if($_POST){
	    	$this->load->library('smtp_lib/smtp_email');
			$user = $this->session->userdata('user_info');    				
			$emailarr = explode(',', $this->input->post('email'));
			$data['message'] = $this->input->post('message');
			$data['username'] = $user['first_name'];
			$post_data['message'] = $data['message'];
			$post_data['user_id'] = $user['id'];
			$post_data['created'] = date('Y-m-d h:i:s');	

			foreach ($emailarr as $key0 => $value0) {
				$userexist = $this->user_model->get_row('users', array('user_email'=>$value0));
				if($userexist){
					$res = array('status'=>'error','error'=>'User already exist with this email - '.$value0);
					echo json_encode($res);
					die();
				}

				$invitaionexist = $this->user_model->get_row('invitation', array('email'=>$value0));
				if($invitaionexist){
					$res = array('status'=>'error', 'error'=> $value0.' already invited');
					echo json_encode($res);
					die();
				}
			}


			foreach ($emailarr as $key => $value) {							
				$post_data['email'] = $value;
				$invitaionexist = $this->user_model->get_row('invitation', array('email'=>$value));
				if($invitaionexist){							
					continue;
				}
				$this->user_model->insert('invitation',$post_data);
				$subject = $user['first_name'].' invite you on vacalio.com';	// Subject for email
				$from = array('no-reply@vacalio.com' =>'vacalio.com');	// From email in array form
				$to = $value;
				$html = $this->load->view('user/invite_template', $data, TRUE);			
				$this->smtp_email->sendEmail($from, $to, $subject, $html);				
			}

			$res = array('status'=>'success');
			echo json_encode($res);
    	}
    }

    public function manage_invitations($offset=0){
    	$this->check_login();

    	$limit=10;
        $data['invitation']=$this->user_model->get_invitation($limit, $offset);
        $config = bnb_pagination();
        $config['base_url'] = base_url().'user/manage_invitations/';
        $config['total_rows'] = $this->user_model->get_invitation(0,0);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;        
        $this->pagination->initialize($config);         
        $data['pagination'] = $this->pagination->create_links();
    	$data['template'] = 'user/manage_invitations';
        $this->load->view('templates/user_template', $data);
    }

     
/* Trips work starts From here*/

	public function current_trips($offset=0)
	{
		$this->check_login();
		$limit=10;
		$data['current_trips'] = $this->user_model->get_current_trips($limit, $offset);
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/current_trips/';
		$config['total_rows'] = $this->user_model->get_current_trips(0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		$data['template'] = 'user/current_trips';
		$this->load->view('templates/user_template',$data);
	}


	public function previous_trips($offset=0)
	{
		$this->check_login();
		$limit=10;
		$data['previous_trips'] = $this->user_model->get_previous_trips($limit, $offset);
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/previous_trips/';
		$config['total_rows'] = $this->user_model->get_previous_trips(0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		$data['template'] = 'user/previus_trips';
		$this->load->view('templates/user_template',$data);
	}

	public function upcoming_trips($offset=0)
	{
		$this->check_login();
		$limit=10;
		$data['upcoming_trips'] = $this->user_model->get_upcoming_trips($limit, $offset);
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/upcoming_trips/';
		$config['total_rows'] = $this->user_model->get_upcoming_trips(0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['offset'] = $offset;
		
		
		$data['template'] = 'user/upcoming_trips';
		$this->load->view('templates/user_template',$data);
	}


	public function view_trips($bookings_id="",$property_id="",$last_page="")
	{ 
		$this->check_login();
		if(empty($bookings_id)){
	    	redirect('user/current_trips');
		}
		$limit=10;
		$data['bookings_id'] = $bookings_id;
		$data['booking_info'] = $this->user_model->get_row('bookings',array('id'=>$bookings_id));
		$data['property_info'] = $this->user_model->get_row('properties',array('id'=>$property_id));
		$data['last_page'] = $last_page;
		$data['template'] = 'user/trips_full_view';
		$this->load->view('templates/user_template',$data);
	}

	public function single_trips_export($bookings_id="",$trips="",$offset=0){
		$this->check_login();
		$limit=50;
		$data = $this->user_model->get_trips_export($bookings_id);
	    $array = (object) array(
	    	        'trip'           => $trips,
	                'pr_description' => $data->description,
	                'pr_title'       => $data->title,
	                'transaction_id' => $data->transaction_id,
	                'total_amount'   => $data->total_amount,
	                'due_amount'     => $data->due_amount,
	                'payment_status' => json_decode($data->extra)->payment_status,
	                'check_in'       => $data->check_in,
	                'check_out'      => $data->check_out,
	                'created'        => $data->created
	            );

		    $data_array['trips'] = $array;             
		    $file = $this->load->view('user/export_trips', $data_array, TRUE);       
		    $this->load->library('mpdf');        
		    $this->mpdf->WriteHTML($file);
		    $this->mpdf->Output($trips.' view '.date('d-m-Y').'.pdf','D');
   } 


	public function all_current_trips($offset=0){
		$this->check_login();
		$limit=10;
		$data['current_trips'] = $this->user_model->get_current_trips($limit, $offset);
	   	  
	   	// print_r($data['current_trips']);
	   	// die();  
	       if(!empty($data['current_trips'])){
			   foreach (@$data['current_trips'] as $row ) {
			       $value[] = (object) array(
						                'pr_title'       => $row->title,
						                'payment_status' => @json_decode($row->extra)->payment_status,
						                'check_in'       => $row->check_in,
						                'check_out'      => $row->check_out,
						                'transaction_id' => @$row->transaction_id,
						                );
			    }
		         $data_array['current_trips'] = $value;             
	       }
	       else{
		         $data_array['current_trips'] = null;             
	       }
         
		    $data_array['trip'] = 'Current';             
		    $file = $this->load->view('user/all_export_trips', $data_array, TRUE);       
		    $this->load->library('mpdf');        
		    $this->mpdf->WriteHTML($file);
		    $this->mpdf->Output(' All Current view '.date('d-m-Y').'.pdf','D');

	} 

	public function all_previous_trips($offset=0){
		$this->check_login();
		$limit=10;
		$data['current_trips'] = $this->user_model->get_previous_trips($limit, $offset);
	  	  
	       if(!empty($data['current_trips'])){
			   foreach (@$data['current_trips'] as $row ) {
			       $value[] = (object) array(
						                'pr_title'       => $row->title,
						                'payment_status' => @json_decode($row->extra)->payment_status,
						                'check_in'       => $row->check_in,
						                'check_out'      => $row->check_out,
						                'transaction_id' => @$row->transaction_id,
						                   );
			    }
		         $data_array['current_trips'] = $value;             
	       }
	       else{
		         $data_array['current_trips'] = null;             
	       }

		    $data_array['trip'] = 'Previous';             
		    $file = $this->load->view('user/all_export_trips', $data_array, TRUE);       
		    $this->load->library('mpdf');        
		    $this->mpdf->WriteHTML($file);
		    $this->mpdf->Output(' All Previous view '.date('d-m-Y').'.pdf','D');
    } 


	public function all_upcoming_trips($offset=0){
		$this->check_login();
		$limit=10;
		$data['current_trips'] = $this->user_model->get_upcoming_trips($limit, $offset);
	       if(!empty($data['current_trips'])){
			   foreach (@$data['current_trips'] as $row ) {
			       $value[] = (object) array(
						                'pr_title'       => $row->title,
						                'payment_status' => @json_decode($row->extra)->payment_status,
						                'check_in'       => $row->check_in,
						                'check_out'      => $row->check_out,
						                'transaction_id' => @$row->transaction_id,
						                   );
			    }
		         $data_array['current_trips'] = $value;             
	       }
	       else{
		         $data_array['current_trips'] = null;             
	       }
	       
		    $data_array['trip'] = 'Upcoming';             
		    $file = $this->load->view('user/all_export_trips', $data_array, TRUE);       
		    $this->load->library('mpdf');        
		    $this->mpdf->WriteHTML($file);
		    $this->mpdf->Output(' All Upcoming view '.date('d-m-Y').'.pdf','D');

	} 


		/* Trips work Ends From here*/


	public function payments($offset = 0){
		$this->check_login();		
		$userinfo = $this->session->userdata('user_info');
		$userid = $userinfo['id'];
		$limit=10;
		$data['payments'] = $this->user_model->get_pagination_where('payments',$limit, $offset, array('user_id'=>$userid));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/payments/';
		$config['total_rows'] = $this->user_model->get_pagination_where('payments',0, 0, array('user_id'=>$userid));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['offset'] = $offset;		
		$data['template'] = 'user/account/payments';
		$this->load->view('templates/user_template',$data);
	}

	public function payment_detail($payment_id=''){
		$this->check_login();
		if(empty($payment_id)) redirect('user/payments');		
		$data['payment'] = $this->user_model->get_row('payments',array('id'=>$payment_id));
		
		$data['template'] = 'user/account/payment_detail';
        $this->load->view('templates/user_template', $data);
	}

	public function approve_booking($id ="")
	{
 		$this->check_login();
		if($id == "")
			redirect('user/bookings');

         //////////Send Approve Booking Email to guest and host Starts////////////////
         //////////Send Approve Booking Email to guest and host Starts////////////////
	
		$data['booking_info']  = $this->user_model->get_row('bookings',array('id'=>$id));
		$data['host_info']     = get_user_info($data['booking_info']->owner_id);
		$data['guest_info']    = get_user_info($data['booking_info']->customer_id);
	    $data['property_info'] = get_property_detail($data['booking_info']->pr_id);

		/* Email To host Satrts*/
		$this->load->library('smtp_lib/smtp_email');
        $email_template = get_manage_email_info('reservation_notification_to_host_after_accepting_booking');
        $website_info = get_manage_email_info('website_url_and_email');
        $website_url = $website_info->website_url;
        $from_email = $website_info->message_from_which_email;

        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

        $subject = $email_template->subject;

        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
        $html =  str_replace($arr_old,$arr_update,$email_template->content);
        
        $to = array($data['host_info']->user_email);
        $from = array($from_email =>$from_email);  // From email in array form 

		 $this->smtp_email->sendEmail($from, $to, $subject, $html);
       
        $insert_data = array(
				        	  'name'=>$data['host_info']->first_name,
				        	  'email'=>$data['host_info']->user_email,
				        	  'subject'=>$subject,
				        	  'message'=>$html,
				        	  'created'=>date('Y-m-d h:i:s')
				        	  );

        $this->user_model->insert('notifications',$insert_data);

	/*Email To host Ends*/

	/* Email To Guest Satrts*/

        $email_template = get_manage_email_info('reservation_notification_to_guest_after_accepting_booking');
        $website_info = get_manage_email_info('website_url_and_email');
        $website_url = $website_info->website_url;
        $from_email = $website_info->message_from_which_email;

        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

        $subject = $email_template->subject;

        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
        $html =  str_replace($arr_old,$arr_update,$email_template->content);

               
        
        $to = array($data['guest_info']->user_email);
        $from = array($from_email =>$from_email);  // From email in array form 

		 $this->smtp_email->sendEmail($from, $to, $subject, $html);
       
        $insert_data = array(
				        	  'name'=>$data['guest_info']->first_name,
				        	  'email'=>$data['guest_info']->user_email,
				        	  'subject'=>$subject,
				        	  'message'=>$html,
				        	  'created'=>date('Y-m-d h:i:s')
				        	  );
        $this->user_model->insert('notifications',$insert_data);

    /*Email To Guest Ends*/

	     //////////Send Approve Booking Email to guest and host Ends////////////////
	     //////////Send Approve Booking Email to guest and host Ends////////////////
 
		$this->user_model->update('bookings', array('status'=>3), array('id'=>$id));
		$this->session->set_flashdata('success_msg','Booking Approved');
		redirect('user/bookings');
	}


   /* Send email to both guest and host for approve Booking starts*/
	public function send_approove_booking_email($email="",$subject="",$html="")
	{
		$this->check_login();
		$this->load->library('smtp_lib/smtp_email');
		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form
        $subject = $subject;
		$to = array($email);
		$is_fail = $this->smtp_email->sendEmail($from,$to,$subject,$html);
		if($is_fail){
		return FALSE;
		}
		else{
			return TRUE;
		}
	}

	public function update_alert_status($id = "")
	{
 		$this->check_login();
		if($id == "")
		{
			return FALSE;
			exit;
		}
		$row = $this->user_model->get_row('bookings', array('id'=>$id));
		if(empty($row->transaction_id))
		{
			echo "invalid";
			exit;
		}
		else
		{
    		$this->user_model->update('bookings', array('alert_status'=> 1), array('id'=>$id));
    		echo "valid";
			exit;
		}
	}


	public function booking_detail($book_id='',$flag=""){
		$this->check_login();
		if(empty($book_id)) redirect('user/bookings');		
		$data['booking'] = $this->user_model->get_row('bookings',array('id'=>$book_id));
		
		// echo $flag;
		// die();
		if($flag!="guest" && $flag!="host")
		{
			redirect('user');
		}
		if($flag=="guest")
		{
			$data['template'] = 'user/booking_detail_for_customer';
	        $this->load->view('templates/user_template', $data);
		}
		if($flag=="host")
		{
			$data['template'] = 'user/booking_detail_for_owner';
	        $this->load->view('templates/user_template', $data);
		}
		// redirect('user');
	}

	public function booking_detail_email($book_id='',$flag="")
	{

		$this->check_login();

		$row['booking'] = $this->user_model->get_row('bookings',array('id'=>$book_id));

		if (empty($row)) {

			$this->session->set_flashdata('error_msg' , "Invalid booking id");

			redirect('user/bookings');

		}

        $host = get_user_info($row['booking']->owner_id);

        $guest = get_user_info($row['booking']->customer_id);

        if($flag=="main_page")

        {

        $message = $this->load->view('export/export_booking_detail',$row,TRUE);

        }

        elseif($flag=='receipt_page')

        {

        $message = $this->load->view('export/export_booking_receipt',$row,TRUE);

        }

			$status_1 = $this->send_booking_detail_email_message($guest,$message);
			$this->session->set_flashdata('success_msg',"Email  has been sent successfully to guest.");
			redirect('user/bookings');

		$data['template'] = 'user/bookings';

        $this->load->view('templates/user_template',$data);	

	}



	public function send_booking_detail_email_message($guest="",$message="")
	{

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form

        $subject = "This is itinenary reciept from the Bnbclone on your Booking by your host";

        if(!empty($guest))
        {

			$to = array($guest->user_email);

			$html = '';

			$html .= '<html>

						<body>

						  <h3>Dear '.$guest->first_name.'</h3>';

        }

		$html .= '<p>This is itinenary reciept from the Bnbclone on your Booking by your host</p>';

		$html .= '<strong>Booking</strong>';

		$html .=	'<p>';

		$html .=  $message;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';

	

		$is_fail = $this->smtp_email->sendEmail($from,$to,$subject,$html);

		if($is_fail){

		return FALSE;

		

		}

		else{

			return TRUE;

		}

	}

	


  public function address_description($pr_id='')
  {
    $this->check_login();
		if(empty($pr_id)) redirect('user/properties');
		$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
		$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');
		$this->form_validation->set_rules('title', 'Property Title', 'trim|required');
		$this->form_validation->set_rules('description', 'Property Description', 'trim|required');
		$this->form_validation->set_rules('house_rules', 'House Rules', 'trim|required');
		$this->form_validation->set_rules('house_manual', 'House Manual', 'trim|required');
		$this->form_validation->set_rules('amenities', 'Ameneties', 'required');
		$this->form_validation->set_rules('accomodates', 'Accommodate', 'trim|required');
		$this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');
		$this->form_validation->set_rules('bedrooms', 'Bedroom', 'trim|required');
		$this->form_validation->set_rules('bathrooms', 'Bathroom', 'trim|required');
		$this->form_validation->set_rules('address', 'Address ', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('size', 'Property Size', 'trim|required|numeric');
		$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
		if($this->form_validation->run() == TRUE)
		{

			$full_address = $_POST['address'].'+'.$_POST['city'].'+'.$_POST['state'].'+'.$_POST['country'];
		    $lat_long_array = get_lat_long($full_address);


			/*Properties table data starts*/
			$properties_table= array(
							  'title'  => $this->input->post('title'),
							  'description'   => $this->input->post('description'),
						  	  'latitude'        => $lat_long_array['lat'],
						  	  'longitude'       => $lat_long_array['lng'],
						  	  'created'   => date('Y-m-d H:i:s')
                         );
			$this->user_model->update('properties',$properties_table,array('id'=>$pr_id));
			/*Properties table data Ends*/

			/*Manage listing page work Starts*/
                $description = $this->input->post('description');
                if(str_word_count($description)<15)
                {
                	$manage_data = array(
                		                 'publish_permission'=>0,
                		                 'status'=>0,
                		                 );
     			$this->user_model->update('properties',$manage_data,array('id'=>$pr_id));
                }
			/*Manage listing page work Ends*/



			/*Pr_info table data Starts*/
			$pr_info_table = array(
							  'property_type_id'     => $this->input->post('property_type'),
							  'pr_collect_cat_id'    => $this->input->post('category_type'),
							  'bed_type'        => $this->input->post('bed_type'),
				    'unit_street_address'       => $this->input->post('address'),
							  'city'       => $this->input->post('city'),
							  'state'     => $this->input->post('state'),
							  'zip'   => $this->input->post('zipcode'),
							  'country'   => $this->input->post('country'),
							  'bed'	      =>$this->input->post('bedrooms'),
							  'bath'      =>$this->input->post('bathrooms'),
							  'square_feet'      =>$this->input->post('size'),
							  'sizeMode' => $this->input->post('sizeMode'),
							  'accommodates'=>$this->input->post('accomodates'),
							  'room_type'  =>$this->input->post('room_type'),
							  'house_rules'  =>$this->input->post('house_rules'),
							  'house_manual'  =>$this->input->post('house_manual'),
				            ); 
			$this->user_model->update('pr_info',$pr_info_table,array('pr_id'=>$pr_id));
			/*Pr_info table data Ends*/

			/*Pr_ameneties table data Starts*/

		   $response = $this->user_model->get_row('pr_amenities',array('pr_id'=>$pr_id)); 
          
          if(empty($response))
          {
			if(isset($_POST['amenities'])){
				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
					if(!empty($_POST['amenities'][$i])){
						$pr_amenities=array(
						'pr_id'=>$pr_id,
						'pr_amenity_id'=>trim($_POST['amenities'][$i])
						);
						$this->user_model->insert('pr_amenities',$pr_amenities);	
					}
				}
			}
          }
          else
          {
			if(isset($_POST['amenities'])){
				$flag_pf=TRUE;
				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
					if($flag_pf){
						$this->user_model->delete('pr_amenities',array('pr_id'=>$pr_id));
						$flag_pf=FALSE;
					}
					if(!empty($_POST['amenities'][$i])){
						$pr_amenities=array(
						'pr_id'=>$pr_id,
						'pr_amenity_id'=>$_POST['amenities'][$i]
						);
						$this->user_model->insert('pr_amenities',$pr_amenities,array('pr_id' =>$pr_id));	
					}
				}
			}
          }
			/*Pr_ameneties table data Ends*/


			/*Pr_nb_relation table data Starts*/

			if($this->input->post('neighbourhood'))
			{
				$neighbour_id = $this->input->post('neighbourhood');
				$this->session->set_userdata('neighbour_id',$neighbour_id);
			}
			if($this->session->userdata('neighbour_id'))
			{
				$neighbour_list = get_neighbourhoot_of_property($pr_id);
				
				$this->user_model->delete('pr_nb_relation', array('pr_nb_id' => $pr_id, 'parent_id'=>0));
				if(!empty($neighbour_list))
				{
					foreach($neighbour_list as $list)
					{
						$this->user_model->delete('pr_nb_relation', array('id'=>$list->id) );
					}
				}

				$parent     = array('pr_nb_id'=>$pr_id, 'parent_id'=>0 );
				$parent_id  = $this->user_model->insert('pr_nb_relation',$parent);
				$neighbour_id   = $this->session->userdata('neighbour_id');
				foreach($neighbour_id as $nb_id)
				{
					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
					$this->user_model->insert('pr_nb_relation',$nb);
				}
				$this->session->unset_userdata('neighbour_id');

			}	
  			/*Pr_nb_relation table data Ends*/
     		$this->session->set_flashdata('success_msg','Property has been updated succesfullly');
			redirect('user/address_description/'.$pr_id);

    }		
	$data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
	$data['pr_amenities'] = $this->user_model->get_result('pr_amenities',array('pr_id'=>$pr_id));
	$data['pr_info'] = $this->user_model->get_row('pr_info',array('pr_id'=>$pr_id));
    $data['template'] = 'user/properties/address_description';
    $this->load->view('templates/user_template', $data);
 }

   /*Multiple Property image has started*/
  
    public function change_property_image($pr_id="")
 	{
 		$this->check_login();
 		if(empty($pr_id)){redirect('user/properties');}
 		$this->check_login();
        $row = $this->user_model->get_row('properties', array('id'=>$pr_id));
        @$old_image = $row->featured_image;
		if(@$_FILES['property_file']['name'] !="")
		{
		   $image = $this->do_core_upload('property_file','./assets/uploads/property/');
		   $this->user_model->update('properties',array('featured_image' => $image),array('id' => $pr_id));
		   if(!empty($old_image))
		   {
		    // $path = './assets/uploads/property/';
		    // @unlink($path.$old_image);
            $res = $this->user_model->get_row('pr_gallery',array('pr_id' => $pr_id));
	            // if(empty($res))
	            // {
     		     $this->user_model->insert('pr_gallery',array('image_file' => $image,'created'=>date('Y-m-d'),'status'=>1,'pr_id'=>$pr_id));
	            // }
	            // else
	            // {
	            //   $this->user_model->update('pr_gallery',array('image_file' => $image,),array('pr_id' => $pr_id,'status'=>1));
	            // }
		   }
		   else
		   {
     		     $this->user_model->insert('pr_gallery',array('image_file' => $image,'created'=>date('Y-m-d'),'status'=>1,'pr_id'=>$pr_id));
		   }
		   $this->session->set_flashdata('success_msg',"Property Image has been Changed successfully.");
		   redirect('user/see_all_property_images/'.$pr_id);
		}    
        $data['properties'] = $this->user_model->get_row('properties', array('id'=>$pr_id));
        $data['template'] = 'user/properties/change_property_image';
        $this->load->view('templates/user_template', $data);
 	}
	
   /* Ajax upload Drag and drop images starts*/
   
   public function ajax_property_drag_drop(){
	$this->check_login();
   	$pr_id = $_POST['property_id'];
   	$file_name = $_FILES['pic']['name'];
	if(@$file_name !="")
	{
	   $image = $this->do_core_upload('pic','./assets/uploads/property/');
	   $info_data = array(
	   	                  'pr_id'=>$pr_id,
	   	                  'created'=>date('d F Y'),
	   	                  'image_file'=>$image,
	                      ); 
	   $this->user_model->insert('pr_gallery',$info_data);
	   $str = "File Uploaded Succesfullly";
	   echo json_encode(array('status'=>$str));
	   exit;
	}
	else{
	   $str = "Something went Wrong ";
	   echo json_encode(array('status'=>$str));
	   exit;
	}    

   	
   } 
	
   /* Ajax upload Drag and drop images Endss*/	

   /*See All Images Starts*/
    public function see_all_property_images($pr_id="")
    {
 		$this->check_login();
    	$data['property_id'] = $pr_id;
		$data['gallery'] = $this->user_model->get_result('pr_gallery',array('pr_id'=>$pr_id));
		$data['get_first_row'] = $this->user_model->get_row('pr_gallery',array('pr_id'=>$pr_id));
	    $data['template'] = 'user/properties/see_all_property_images';
		$this->load->view('templates/user_template', $data);		
    }
 	/*See All Images Ends*/

 	/*ajax Change profile images starts*/
    public function ajax_change_property_profile_image()
 	{
 		$this->check_login();
 		$image_new =  $this->input->post('image');
 		$property_id =  $this->input->post('property_id');
			if($image_new!="" && $property_id!="")
			{
			   $this->user_model->update('properties',array('featured_image' => $image_new),array('id' => $property_id));
			   $this->user_model->update('pr_gallery',array('status'=>0),array('pr_id'=>$property_id));
			   $this->user_model->update('pr_gallery',array('status'=>1),array('image_file'=>$image_new,'pr_id'=>$property_id));
			   $row = $this->user_model->get_row('pr_gallery',array('image_file'=>$image_new,'pr_id'=>$property_id));
			   echo $row->description;
			}    
 	}   
 	/*ajax Change profile images Ends*/

 	/*ajax delete image  starts*/
 	public function ajax_delete_property_image(){
 		$this->check_login();
	    $unique_id = $_POST['unique_id'];
	    $property_id = $_POST['property_id'];
	    $row = $this->user_model->get_row('pr_gallery',array('id'=>$unique_id));
	    $old_image = $row->image_file;
		$this->user_model->delete('pr_gallery',array('id'=>$unique_id));
	    if($row->status==1){
		$this->user_model->update('properties',array('featured_image'=>'','publish_permission'=>0,'status'=>0),array('id'=>$property_id));
	    }
		if(!empty($old_image))
		{
		$path = './assets/uploads/property/';
		@unlink($path.$old_image);
		echo "deleted" ;
		}
    }

  /* add_pro_img_decsription starts*/
   public function add_pro_img_decsription()
   {
	   	$image_name = $_POST['image_name'];
	   	$image_description = $_POST['image_description'];
	   	if(!empty($image_name))
	   	{
	       $status = $this->user_model->update('pr_gallery',array('description'=>$image_description),array('image_file'=>$image_name));   		
	       if($status)
	       {
	       	echo $status;
	       }
	   	}
	   	else
	   	{
	   		return false;
	   	}
   }
  /* add_pro_img_decsription Ends*/

  /*Multiple images uploads Work Ends*/

  /*Advance Pricing Starts*/

  	public function price_terms($pr_id='', $tabid='basic')
 	{
    	$this->check_login();
		$data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
		$data['can_policies'] = $this->user_model->get_result('cancellation_policies');
		if(empty($pr_id)) redirect('user/properties');
		$this->form_validation->set_rules('currency_type', 'Currency Type', 'trim|required');
		$this->form_validation->set_rules('nightly', 'Nightly', 'trim|required');
		$this->form_validation->set_rules('weekly', 'Weekly', 'trim|required');
		$this->form_validation->set_rules('monthly', 'Monthly', 'trim|required');
		if ($this->input->post('is_sublet')) {
			$data['is_sublet'] = $this->input->post('is_sublet');
			$this->form_validation->set_rules('sublet_start', 'Sublet Start', 'trim|required');
			$this->form_validation->set_rules('sublet_end', 'Sublet End', 'trim|required');
			$this->form_validation->set_rules('sublet_price', 'Sublet Price', 'trim|required');
			// $sublet_start = date('Y-m-d H:i:s', $this->input->post('sublet_start'));
			// $sublet_end = date('Y-m-d H:i:s', $this->input->post('sublet_end'));
			$sublet_start = $this->input->post('sublet_start');
			$sublet_end = $this->input->post('sublet_end');
			$sublet_price = $this->input->post('sublet_price');
			$sublet_start = date('Y-m-d H:i:s', strtotime($sublet_start));
			$sublet_end = date('Y-m-d H:i:s', strtotime($sublet_end));
			// echo "<br>".date('Y-m-d H:i:s', strtotime($sublet_start));
			// echo "<br>".date('Y-m-d H:i:s', strtotime($sublet_end));
			// die();
		}else{
			$data['is_sublet'] = $data['properties']->sublet_status;
			$sublet_start = '0000-00-00 00:00:00';
			$sublet_end = '0000-00-00 00:00:00';
			$sublet_price = '0.00';
		}
		$this->form_validation->set_rules('additional_guest_price', 'Additional Guest Price', 'trim|required');
		$this->form_validation->set_rules('additional_guest', 'Additional Guest', 'trim|required');
		$this->form_validation->set_rules('cleaning_fee', 'Cleaning Fee', 'trim|required');//numeric


		$this->form_validation->set_rules('cancellation_policies', 'Cancellation Policies', 'trim|required');
		$this->form_validation->set_rules('min_stay', 'Minimum Stay', 'trim|required');
		$this->form_validation->set_rules('max_stay', 'Maximum Stay', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');

		if($this->form_validation->run() == TRUE)
		{
	  			$properties_table = array(
										  'currency_type'  			=> $this->input->post('currency_type'),
										  'application_fee'   		=> $this->input->post('nightly'),
									  	  'weekly'  				=> $this->input->post('weekly'),
									  	  'monthly' 				=> $this->input->post('monthly'),
									  	  'sublet_status' 			=> $this->input->post('is_sublet'),
									  	  'sublet_from_date' 		=> $sublet_start,
									  	  'sublet_to_date' 			=> $sublet_end,
									  	  'sublet_price' 			=> $sublet_price,
									  	  'additional_guest_price' 	=> $this->input->post('additional_guest_price'),
									  	  'additional_guest' 		=> $this->input->post('additional_guest'),
									  	  'cleaning_fee' 			=> $this->input->post('cleaning_fee'),
									  	  'cancellation_policies' 	=> $this->input->post('cancellation_policies'),
									  	  'min_stay' 				=> $this->input->post('min_stay'),
									  	  'max_stay' 				=> $this->input->post('max_stay')
			                         );
	  			// print_r($properties_table);
	  			// die();
				$this->user_model->update('properties', $properties_table, array('id'=>$pr_id));

	     		$this->session->set_flashdata('success_msg','Price And Terms Updated Succesfullly');
				redirect('user/price_terms/'.$pr_id);
	    }

	    $wlst_prc = $this->user_model->get_result('listing_price', array('listing_id' => $pr_id, 'type' => 'w'));

	    $wlisting_price = array();
	    $data['isExactWeekly'] = 0;
	    if ($wlst_prc) {
	    	$data['isExactWeekly'] = $wlst_prc[0]->weekly_exact;
	    	foreach ($wlst_prc as $row) {
	    		$wlisting_price[$row->start_date]['price'] = $row->price;
	    		$wlisting_price[$row->start_date]['id'] = $row->id;
	    	}
	    }

	    $mlst_prc = $this->user_model->get_result('listing_price', array('listing_id' => $pr_id, 'type' => 'm'));
	 //    echo "<pre>";
	 //    print_r($mlst_prc);
		// die();
	    $mlisting_price = array();
	    $data['isExactMonthly'] = 0;
	    if ($mlst_prc) {
	    	$data['isExactMonthly'] = $mlst_prc[0]->monthly_exact;
	    	foreach ($mlst_prc as $row) {
	    		$mlisting_price[$row->start_date]['price'] = $row->price;
	    		$mlisting_price[$row->start_date]['id'] = $row->id;
	    	}
	    }
	 
		$swklst_prc = $this->user_model->get_result('listing_price', array('listing_id' => $pr_id, 'type' => 's') ,array('type' => 'wk'));
	    // $wklst_prc = $this->user_model->get_result('listing_price', array('listing_id' => $pr_id, 'type' => 'wk'));
	    //    echo "<pre>".$mlst_prc[0]->monthly_exact;
	 //    print_r($slst_prc);
		// die();
		$pricing = array('weekendprice' => $data['properties']->weekendprice ,'weekdaysprice' => $data['properties']->application_fee);
		$final = $this->advance_pricing_data($pr_id, $wlisting_price, $mlisting_price, $swklst_prc, $pricing);
		$data['weeks'] = $final['weeks'];
		$data['months'] = $final['months'];
		$data['rangArray'] = $final['rangArray'];
		$data['listing_id'] = $pr_id;
		$data['tabid'] = $tabid;
		$data['weekendprice'] = $data['properties']->weekendprice;
		$data['weekdaysprice'] = $data['properties']->application_fee;
		// print_r($data['rangArray']);
		// die();
	    $data['template'] = 'user/properties/price_terms';
	    $this->load->view('templates/user_template', $data);
	}

	public function advance_pricing_data($listingId, $listing_price, $mlisting_price, $swklst_prc, $pricing){
		$system = 0;
		$format = ($system == 1) ? 'Y-m-d' : 'F jS, Y H:i:s' ;
		$seasons = array(
							1 => 'Winter',
							2 => 'Winter',
							3 => 'Spring',
							4 => 'Spring',
							5 => 'Spring',
							6 => 'Summer',
							7 => 'Summer',
							8 => 'Summer',
							9 => 'Autumn',
							10 => 'Autumn',
							11 => 'Autumn',
							12 => 'Winter'
						);
		$today = date($format);
		$weekend = date($format, strtotime('sunday this week'.$today));

		$a = getdate(strtotime($weekend));
		$startime = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);

		$a = getdate($startime);
		$endtime = mktime(0,0,0,$a['mon']+13,$a['mday'],$a['year']);
		$endtime = strtotime('last day of '.date($format, $endtime));

		$start = date($format, $startime);
		$end = date($format, $endtime);

		$start = strtotime($start);
		$end = $endtime;

		$weeks = array();
		$months = array();
		$lastmonth = '';
		$final = array();
		while ($startime < $endtime) {  
		    $weeks[] = date($format, $startime);
		    $startime += strtotime('+1 week', 0);
		}
		$i=0;
		$k=1;
		$lastseason='';
		$sundaysofweek = array();
		$a = getdate(strtotime($weekend));
		$sundaysofweek[] = mktime(0,0,0,$a['mon'],$a['mday'],$a['year']);
		// echo "<pre>";
		// print_r($weeks);
		// die();
		foreach ($weeks as $key => $date) {
			$sundaysofweek[] = strtotime('sunday this week'.$weeks[$key]);
			if (isset($weeks[$key+1])) {
				$b = getdate(strtotime($weeks[$key+1]));
				$wkend = mktime(0,0,0,$b['mon'],$b['mday']-1,$b['year']);
				// if (date('n',strtotime($weeks[$key])) == date('n',strtotime($weeks[$key+1]))){
				// }
				// else{
				// 	$b = getdate(strtotime($weeks[$key]));
				// 	$wkend = strtotime($weeks[$key+1]);
				// }

				$next = date('M j', $wkend);
				// echo "<br> Start : ".date('M j', strtotime($date));
				// echo "<br> End : ".$next;
				$y = date('Y', strtotime($date));
			    $m = date('n', strtotime($date));
			    $season = element($m, $seasons);

			    if (($lastseason != '') && ($lastseason == $season)){
					$i++;
				}
				else{
					$i=0;
				}

				$firstDayOfMonth = strtotime('first day of '.date('F Y', strtotime($date)));
				if (!(($lastmonth != '') && ($lastmonth == $m))){
					if ($m == 12){
			    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
						if(isset($mlisting_price[$firstDayOfMonth])){
				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
				    	}else{
				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = "";
				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = "";
				    	}
					}
					else{
			    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
				    	if(isset($mlisting_price[$firstDayOfMonth])){
				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
				    	}else{
				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = "";
				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = "";
				    	}
					}
				}

			    $final[$season.'-'.$y][$i]['week'] = date('M j', strtotime($date))." - ".$next;
			    $final[$season.'-'.$y][$i]['start'] = strtotime($date);
			    $final[$season.'-'.$y][$i]['end'] = $wkend;
		    	if(isset($listing_price[strtotime($date)])){
		    		$final[$season.'-'.$y][$i]['price'] = $listing_price[strtotime($date)]['price'];
		    		$final[$season.'-'.$y][$i]['id'] = $listing_price[strtotime($date)]['id'];
		    	}else{
		    		$final[$season.'-'.$y][$i]['price'] = "";
		    		$final[$season.'-'.$y][$i]['id'] = "";
		    	}
			}else{
				$y = date('Y', strtotime($date));
			    $m = date('n', strtotime($date));
			    $season = element($m, $seasons);
			    $c = getdate(strtotime($date));
				$wkend = mktime(0,0,0,$c['mon'],$c['mday']+6,$c['year']);
			    $next = date('M j', $wkend);

			    if (($lastseason != '') && ($lastseason == $season)){
					$i++;
				}
				else{
					$i=0;
				}

				if (!(($lastmonth != '') && ($lastmonth == $m))){
			    	if (!(($lastmonth != '') && ($lastmonth == $m))){
						if ($m == 12){
				    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
							if(isset($mlisting_price[$firstDayOfMonth])){
					    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
					    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
					    	}else{
					    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = "";
					    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = "";
					    	}
						}
						else{
				    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
					    	if(isset($mlisting_price[$firstDayOfMonth])){
					    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
					    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
					    	}else{
					    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = "";
					    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = "";
					    	}
						}
					}
				}

			    $final[$season.'-'.$y][$i]['week'] = date('M j', strtotime($date))." - ".$next;
			    $final[$season.'-'.$y][$i]['start'] = strtotime($date);
			    $final[$season.'-'.$y][$i]['end'] = $wkend;
		    	if(isset($listing_price[strtotime($date)])){
		    		$final[$season.'-'.$y][$i]['price'] = $listing_price[strtotime($date)]['price'];
		    		$final[$season.'-'.$y][$i]['id'] = $listing_price[strtotime($date)]['id'];
		    	}else{
		    		$final[$season.'-'.$y][$i]['price'] = "";
		    		$final[$season.'-'.$y][$i]['id'] = "";
		    	}
			}

			$lastseason = $season;
			$lastmonth = $m;
			$k++;
		}
		$seasonarray = array();
		$count=0; 
		foreach ($final as $k => $v) {
			$seasonarray[] = $k;
		}
		$data['weeks'] = $final;
		// echo "<pre>";
		// print_r($final);
		// foreach ($final as $value) {
		// 	foreach ($value as $v) {
		// 		echo "<br> Start : ".date('M j', $v['start']);
		// 		echo "<br> End : ".date('M j', $v['end']);
		// 		echo "<br> Range : ".$v['week'];
		// 	}
		// }
		// $data['weeks'] = $seasonarray;
		$data['months'] = $months;
		$data['rangArray'] = $this->createWeekDaysPricing($listingId, $sundaysofweek, $pricing);
		// print_r($data['rangArray']);
		return $data;
	}

	// public function advance_pricing_data($listingId, $listing_price, $mlisting_price, $swklst_prc, $pricing){
	// 	$system = 0;
	// 	$format = ($system == 1) ? 'Y-m-d' : 'F jS, Y H:i:s' ;
	// 	$seasons = array(
	// 						1 => 'Winter',
	// 						2 => 'Winter',
	// 						3 => 'Spring',
	// 						4 => 'Spring',
	// 						5 => 'Spring',
	// 						6 => 'Summer',
	// 						7 => 'Summer',
	// 						8 => 'Summer',
	// 						9 => 'Autumn',
	// 						10 => 'Autumn',
	// 						11 => 'Autumn',
	// 						12 => 'Winter'
	// 					);
	// 	$today = date($format);
	// 	$weekend = date($format, strtotime('sunday this week'.$today));

	// 	$a = getdate(strtotime($weekend));
	// 	$startime = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);

	// 	$a = getdate($startime);
	// 	$endtime = mktime(0,0,0,$a['mon']+13,$a['mday'],$a['year']);
	// 	$endtime = strtotime('last day of '.date($format, $endtime));

	// 	$start = date($format, $startime);
	// 	$end = date($format, $endtime);

	// 	$start = strtotime($start);
	// 	$end = $endtime;

	// 	$weeks = array();
	// 	$months = array();
	// 	$lastmonth = '';
	// 	$final = array();
	// 	while ($startime < $endtime) {  
	// 	    $weeks[] = date($format, $startime);
	// 	    $startime += strtotime('+1 week', 0);
	// 	}
	// 	$i=0;
	// 	$k=1;
	// 	$lastseason='';
	// 	$sundaysofweek = array();
	// 	$a = getdate(strtotime($weekend));
	// 	$sundaysofweek[] = mktime(0,0,0,$a['mon'],$a['mday'],$a['year']);
	// 	foreach ($weeks as $key => $date) {
	// 		$sundaysofweek[] = strtotime('sunday this week'.$weeks[$key]);
	// 		if (isset($weeks[$key+1])) {
	// 			if (date('n',strtotime($weeks[$key])) == date('n',strtotime($weeks[$key+1]))){
	// 				$wkend = strtotime($weeks[$key+1]);
	// 			}
	// 			else{
	// 				$b = getdate(strtotime($weeks[$key+1]));
	// 				$wkend = mktime(0,0,0,$b['mon'],$b['mday']-1,$b['year']);
	// 			}

	// 			$next = date('M j', $wkend);
					
	// 			$y = date('Y', strtotime($date));
	// 		    $m = date('n', strtotime($date));
	// 		    $season = element($m, $seasons);

	// 		    if (($lastseason != '') && ($lastseason == $season)){
	// 				$i++;
	// 			}
	// 			else{
	// 				$i=0;
	// 			}

	// 			$firstDayOfMonth = strtotime('first day of '.date('F Y', strtotime($date)));
	// 			if (!(($lastmonth != '') && ($lastmonth == $m))){
	// 				if ($m == 12){
	// 		    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
	// 					if(isset($mlisting_price[$firstDayOfMonth])){
	// 			    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
	// 			    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
	// 			    	}else{
	// 			    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = "";
	// 			    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = "";
	// 			    	}
	// 				}
	// 				else{
	// 		    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
	// 			    	if(isset($mlisting_price[$firstDayOfMonth])){
	// 			    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
	// 			    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
	// 			    	}else{
	// 			    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = "";
	// 			    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = "";
	// 			    	}
	// 				}
	// 			}

	// 		    $final[$season.'-'.$y][$i]['week'] = date('M j', strtotime($date))." - ".$next;
	// 		    $final[$season.'-'.$y][$i]['start'] = strtotime($date);
	// 		    $final[$season.'-'.$y][$i]['end'] = $wkend;
	// 	    	if(isset($listing_price[strtotime($date)])){
	// 	    		$final[$season.'-'.$y][$i]['price'] = $listing_price[strtotime($date)]['price'];
	// 	    		$final[$season.'-'.$y][$i]['id'] = $listing_price[strtotime($date)]['id'];
	// 	    	}else{
	// 	    		$final[$season.'-'.$y][$i]['price'] = "";
	// 	    		$final[$season.'-'.$y][$i]['id'] = "";
	// 	    	}
	// 		}else{
	// 			$y = date('Y', strtotime($date));
	// 		    $m = date('n', strtotime($date));
	// 		    $season = element($m, $seasons);
	// 		    $next = date('M j', $endtime);

	// 		    if (($lastseason != '') && ($lastseason == $season)){
	// 				$i++;
	// 			}
	// 			else{
	// 				$i=0;
	// 			}

	// 			if (!(($lastmonth != '') && ($lastmonth == $m))){
	// 		    	if (!(($lastmonth != '') && ($lastmonth == $m))){
	// 					if ($m == 12){
	// 			    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
	// 						if(isset($mlisting_price[$firstDayOfMonth])){
	// 				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
	// 				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
	// 				    	}else{
	// 				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['price'] = "";
	// 				    		$months[$season.'-'.($y+1)][date('F', $firstDayOfMonth)]['id'] = "";
	// 				    	}
	// 					}
	// 					else{
	// 			    		// echo "<br> Created => ".date($format, $firstDayOfMonth);
	// 				    	if(isset($mlisting_price[$firstDayOfMonth])){
	// 				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = $mlisting_price[$firstDayOfMonth]['price'];
	// 				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = $mlisting_price[$firstDayOfMonth]['id'];
	// 				    	}else{
	// 				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['price'] = "";
	// 				    		$months[$season.'-'.($y)][date('F', $firstDayOfMonth)]['id'] = "";
	// 				    	}
	// 					}
	// 				}
	// 			}

	// 		    $final[$season.'-'.$y][$i]['week'] = date('M j', strtotime($date))." - ".$next;
	// 		    $final[$season.'-'.$y][$i]['start'] = strtotime($date);
	// 		    $final[$season.'-'.$y][$i]['end'] = $wkend;
	// 	    	if(isset($listing_price[strtotime($date)])){
	// 	    		$final[$season.'-'.$y][$i]['price'] = $listing_price[strtotime($date)]['price'];
	// 	    		$final[$season.'-'.$y][$i]['id'] = $listing_price[strtotime($date)]['id'];
	// 	    	}else{
	// 	    		$final[$season.'-'.$y][$i]['price'] = "";
	// 	    		$final[$season.'-'.$y][$i]['id'] = "";
	// 	    	}
	// 		}

	// 		$lastseason = $season;
	// 		$lastmonth = $m;
	// 		$k++;
	// 	}
	// 	$seasonarray = array();
	// 	$count=0; 
	// 	foreach ($final as $k => $v) {
	// 		$seasonarray[] = $k;
	// 	}
	// 	$data['weeks'] = $final;
	// 	// $data['weeks'] = $seasonarray;
	// 	$data['months'] = $months;
	// 	$data['rangArray'] = $this->createWeekDaysPricing($listingId, $sundaysofweek, $pricing);
	// 	// print_r($data['rangArray']);
	// 	// die();
	// 	return $data;
	// }

	public function createWeekDaysPricing($listingId, $sundaysofweek, $pricing)
	{
		$availableDays = $this->user_model->getWeekDaysRanges($listingId);

		$new_events = array();
		if ($availableDays) {
			usort($availableDays, array('user', 'sortByDate'));
			foreach ($availableDays as $k1 => $v1) {
				if (($v1['rangeopen'] == 1) && ($v1['rangeclose'] == 0)) {
					if (isset($availableDays[$k1+1])) {
						$v1['end'] = $availableDays[$k1+1]['end'];
					}
					$new_events[date('Y-m-d', strtotime($v1['start']))] = $v1;
				}elseif (($v1['rangeopen'] == 1) && ($v1['rangeclose'] == 1)) {
					$new_events[date('Y-m-d', strtotime($v1['start']))] = $v1;
				}
			}
		}

		$rangArray = array();
		$isEve = FALSE;
		$rangeNum = 0;
		$event = array();
		$wasFriday = FALSE;
		$evend = 0;
		$weekDayPrice = $pricing['weekdaysprice'];
		$weekEndPrice = $pricing['weekendprice'];
		// echo "<pre>";
		// if ($pricing['weekendprice'] != 0) {
		// 	$weekEndPrice = $pricing['weekendprice'];
		// }
		foreach ($sundaysofweek as $k => $v) {
			// echo "<br>".$day."Day :".date("Y-m-d", $v);
			$a = getdate($v);
			for ($i=0; $i < 7; $i++) {
				$weekday = mktime(0,0,0,$a['mon'],$a['mday']+$i,$a['year']);
				if (isset($new_events[date('Y-m-d', $weekday)])) {
					// echo "<br> :".date('l F jS Y', $weekday);
					$event = $new_events[date('Y-m-d', $weekday)];
					// print_r($event);
					$isEve = TRUE;
					$rangArray[$rangeNum]['rangeid'] = $event['id'];
					$rangArray[$rangeNum]['price'] = $event['price'];
					$rangArray[$rangeNum]['start'] = strtotime($event['start']);
					$rangArray[$rangeNum]['end'] = strtotime($event['end']);
					$nd = getdate(strtotime($event['end']));
					$evend = mktime(0,0,0,$nd['mon'],$nd['mday'],$nd['year']);
					$rangArray[$rangeNum]['rangeType'] = 1;
					// echo "<br> Start : ".$rangArray[$rangeNum]['start'];
					// echo "<br> End : ".$rangArray[$rangeNum]['end'];
					// echo "<br> Price : ".$rangArray[$rangeNum]['price'];
					// if ((strtotime('+1 day', $weekday) == strtotime($event->end)) && (strtotime('+1 day', $weekday) == strtotime($event->end))) {
					// }
					// $isEve = FALSE;
					// $event = array();
					$rangeNum++;
					// break;
				}else{
					// echo "<br>Non Event : ".date("l F jS Y", $weekday);
					if ($weekday > $evend) {
						if ((date("l", $weekday) == 'Friday' || date("l", $weekday) == 'Saturday') && ($weekEndPrice != 0)) {
							if (date("l", $weekday) == 'Friday') {
								$rangArray[$rangeNum]['start'] = $weekday;
								$rangArray[$rangeNum]['end'] = strtotime('+1 day', $weekday);
								$rangArray[$rangeNum]['price'] = $weekEndPrice;
								$rangArray[$rangeNum]['rangeType'] = 2;
								// echo "<br> Start : ".date('l F jS Y',$rangArray[$rangeNum]['start']);
								// echo "<br> End : ".date('l F jS Y',$rangArray[$rangeNum]['end']);
								// echo "<br> Price : ".$rangArray[$rangeNum]['price'];
								$rangeNum++;
								break;
								// echo "<br>Start : ".date("l F jS Y", $weekday);
							}elseif (date("l", $weekday) == 'Saturday') {
									$rangArray[$rangeNum]['start'] = $weekday;
									$rangArray[$rangeNum]['end'] = $weekday;
									$rangArray[$rangeNum]['price'] = $weekEndPrice;
									$rangArray[$rangeNum]['rangeType'] = 2;
									// echo "<br> Start : ".date('l F jS Y',$rangArray[$rangeNum]['start']);
									// echo "<br> End : ".date('l F jS Y',$rangArray[$rangeNum]['end']);
									// echo "<br> Price : ".$rangArray[$rangeNum]['price'];
									$rangeNum++;
									break;
								// echo "<br>Start : ".date("l F jS Y", $weekday);
							}
						}else{
							if (!isset($rangArray[$rangeNum]['start'])) {
								$rangArray[$rangeNum]['start'] = $weekday;
								$rangArray[$rangeNum]['price'] = $weekDayPrice;
								$rangArray[$rangeNum]['rangeType'] = 2;
								if (date("l", strtotime('+1 day', $weekday)) == 'Friday') {
									$rangArray[$rangeNum]['end'] = $weekday;
									// echo "<br> Start : ".date('l F jS Y',$rangArray[$rangeNum]['start']);
									// echo "<br> End : ".date('l F jS Y',$rangArray[$rangeNum]['end']);
									// echo "<br> Price : ".$rangArray[$rangeNum]['price'];
									$rangeNum++;
								}
								// echo "<br>Start : ".date("l F jS Y", $weekday);
							}else{
								$rangArray[$rangeNum]['end'] = $weekday;
								if (date("l", $weekday) == 'Thursday') {
									$rangArray[$rangeNum]['end'] = $weekday;
									// echo "<br> Start : ".date('l F jS Y',$rangArray[$rangeNum]['start']);
									// echo "<br> End : ".date('l F jS Y',$rangArray[$rangeNum]['end']);
									// echo "<br> Price : ".$rangArray[$rangeNum]['price'];
									$rangeNum++;
								}
								// echo "<br>Start : ".date("l F jS Y", $weekday);
							}
						}
					}
				}
			}
		}
		// echo "<pre>";
		// foreach ($rangArray as $key => $value) {
		// 	echo "<br>Start :".date('l F jS Y', intval($value['start']));
		// 	echo "<br>End :".date('l F jS Y', intval($value['end']));
		// 	echo "<br>Price :".$value['price'];
		// }
		// die();
		// print_r($rangArray);
		// die();
		return $rangArray;
	}

	public function createranges($listing_id, $start, $end)
	{
		$eve = $this->user_model->check_event_existance($start,$end,$listing_id);
		if (!empty($eve)) {
			usort($eve, array('user', 'sortByDate'));
			// print_r($eve);
			$newstart = strtotime($eve[0]['start']);
			// $a = getdate(strtotime(end($eve)['start']));
			$arrayEnd = end($eve);
			// $a = getdate(strtotime($arrayEnd['start']));
			// $newend = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
			$newend = strtotime($arrayEnd['end']);
			$rangeArray = array();
			// echo "<br> S1 : ".date('l F jS H:i:s', $start);
			// echo "<br> E1 : ".date('l F jS H:i:s', $end);
			// echo "<br> S2 : ".date('l F jS H:i:s', $newstart);
			// echo "<br> E2 : ".date('l F jS H:i:s', $newend);
			// die();
			if (($start > $newstart) && ($end < $newend)) {// G
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] = $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				$a = getdate($end);
				$rangeArray[2]['data'] = $arrayEnd;//end($eve);
				$rangeArray[2]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[2]['end'] = $newend;
				$end = $newend ;
				// echo json_encode(array('rangeArray' => 'G'));
				// echo "<br> G";
				// die();
			}elseif (($start > $newstart) && ($end > $newend)) {// F
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] = $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				// echo json_encode(array('rangeArray' => 'F'));
				// die();
			}elseif (($start < $newstart) && ($end < $newend)) { //E
				$rangeArray[0]['start'] = $start ;
				$rangeArray[0]['end'] = $end;
				$a = getdate($end);
				$rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[1]['end'] = $newend;
				$rangeArray[1]['data'] = $arrayEnd;//end($eve); /***************///
				// echo json_encode(array('rangeArray' => 'E'));
				// die();
			}elseif (($start > $newstart) && ($end == $newend)) { //D
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] =  $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				// echo "<br> D";
				// die();
				// echo json_encode(array('rangeArray' => 'D'));
				// die();
			}elseif (($start == $newstart) && ($end < $newend)) { //C
				$rangeArray[0]['start'] = $start ;
				$rangeArray[0]['end'] = $end;
				$a = getdate($end);
				$rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[1]['end'] = $newend;
				$rangeArray[1]['data'] = $arrayEnd;
				// echo json_encode(array('rangeArray' => 'C'));
				// die();
			}elseif ($start == $newend) {//B
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] = $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				// echo json_encode(array('rangeArray' => 'B'));
				// die();
			}elseif ($end == $newstart) {//A
				$rangeArray[0]['start'] = $start;
				$rangeArray[0]['end'] = $end;
				$a = getdate($end);
				$rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[1]['end'] = $newend;
				$rangeArray[1]['data'] = $arrayEnd;
				// echo json_encode(array('rangeArray' => 'A'));
				// die();
			}else{
				$rangeArray[0]['start'] = $start ;
				$rangeArray[0]['end'] = $end;
				// echo json_encode(array('rangeArray' => 'Default'));
				// die();
			}
			// foreach ($eve as $key => $value) {
			// 	$eve[strtotime($value['start'])] = $value;
			// 	unset($eve[$key]);
			// }
			// print_r($eve);
		}else{
			$rangeArray[0]['start'] = $start ;
			$rangeArray[0]['end'] = $end;
		}
		// foreach ($rangeArray as $key => $value) {
		// 	// echo "<br> RStart : ".date('F jS Y H:i:s', $value['start']);
		// 	// echo "<br> REnd : ".date('F jS Y H:i:s', $value['end']);
		// 	$rangeArray[$key]['start'] = date('F jS Y', $value['start']);
		// 	$rangeArray[$key]['end'] = date('F jS Y', $value['end']);
		// 	// echo "<br> End : ".date('F jS Y', $value['end']);
		// 	// if (isset($value['data']))
		// 	// 	print_r($value['data']);
		// }
		// echo "<pre>";
		// print_r($rangeArray);
		// die();
		// echo json_encode(array('msg' => $rangeArray));
		// die();
		return $rangeArray;
	}

	public function ajaxSetDailyPrice($listing_id='', $currency_type="USD"){
		// $_POST['currency_type'] = 'USD';
		// $_POST['start'] = '2014-05-19 00:00:00';
		// $_POST['end'] = '2014-05-27 23:59:59';
		// $_POST['eve_type'] = 1;
		// $_POST['eve_price'] = 900;
		if (isset($_POST['currency_type']))
			$currency_type = $_POST['currency_type'];

		$eventPrice = 0;

		if (isset($_POST['eve_price'])) {
			$eventPrice = $_POST['eve_price'];
		}

		if (isset($_POST['listing_id'])) {
			$listing_id = $_POST['listing_id'];
		}

		if (empty($listing_id)) {
			echo json_encode(array('status' => false, 'msg' => 'Property not found...'));
			die();
		}
		if (isset($_POST['start']) && isset($_POST['end'])) {
			$start = strtotime($_POST['start']);
			$k = getdate(strtotime($_POST['end']));
			$end = mktime(23,59,59,$k['mon'],$k['mday'],$k['year']);
			
			$rangeArray = $this->createranges($listing_id, $start, $end);

			$groupid = 0;
			$startselection = strtotime($_POST['start']);
			$endselection = strtotime($_POST['end']);
			$eve_type = $_POST['eve_type'];
			$operation = 0;
			
			foreach ($rangeArray as $key => $value) {
				$start = $value['start'];
				$eve_start = $start;
				$end = $value['end'];
				$a = getdate($eve_start);// Start From here
				$eve_end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
				if (isset($_POST['eve_type'])) {
					if (($eve_type == 1) && ($eventPrice == 0)) {
						$operation = 0;
						$eve = $this->user_model->make_event_available($start, $end,$listing_id);
						$day = 1;
						$groupid = 0;
						while (TRUE) {
							if (($eve_start <= $end) && ($eve_end >= $end)) {
								if (isset($value['data'])) {
									$data = $value['data'];
									$data['start'] = date('Y-m-d H:i:s', $eve_start);
									$data['end'] = date('Y-m-d H:i:s', $eve_end);
									$data['groupid'] = $groupid;
									
									if ($day == 1) {
										$data['rangeopen']	= 1;
										$data['rangeclose']	= 1;
										$groupid = $this->user_model->insert('pr_calender', $data);
										$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));	
									}else{
										$data['rangeopen']	= 0;
										$data['rangeclose']	= 1;
										$this->user_model->insert('pr_calender', $data);
									}
								}
								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
								break;
							}else{
								if (isset($value['data'])) {
									$data = $value['data'];
									$data['start'] = date('Y-m-d H:i:s', $eve_start);
									$data['end'] = date('Y-m-d H:i:s', $eve_end);
									$data['groupid'] = $groupid;

									$data['rangeclose'] = 0;
									if ($day == 1) {
										$data['rangeopen'] = 1;


										$groupid = $this->user_model->insert('pr_calender', $data);
										$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));

									}else{
										$data['rangeopen'] = 0;
										$this->user_model->insert('pr_calender', $data);

									}
								}
								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
							}

							$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
							$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
							$day++;
						}
					}else{
						$this->user_model->make_event_available($start,$end,$listing_id);
						$day = 1;
						$groupid = 0;

						while (TRUE) {

								if (isset($value['data'])) {
									$data = $value['data'];
									$data['start'] = date('Y-m-d H:i:s', $eve_start);
									$data['end'] = date('Y-m-d H:i:s', $eve_end);
									$data['groupid'] = $groupid;
									// $data['property_id'] = $listing_id;
								}else{
									if(($eve_type == 1) && ($eventPrice != 0)){
										$operation = 1;
										$data = array(
											'price' 		=> $eventPrice, 
											'currency_type' => $currency_type, 
											'start' 		=> date('Y-m-d H:i:s', $eve_start),
											'end' 			=> date('Y-m-d H:i:s', $eve_end),
											'groupid' 		=> $groupid,
											'color' 		=> '#819f2a',
											'textColor' 	=> '#fff',
											'event_type' 	=> 2,
											'property_id'   =>$listing_id
										);
									}else{
										$operation = 2;
										$data = array(
											'start' 		=> date('Y-m-d H:i:s', $eve_start),
											'end' 			=> date('Y-m-d H:i:s', $eve_end),
											'color' 		=> '#666',
											'textColor' 	=> '#666',
											'groupid' 		=> $groupid,
											'event_type' 	=> $eve_type,
											'property_id'   =>$listing_id
										);
									}
								}
							if (($eve_start <= $end) && ($eve_end >= $end)) {
								if ($day == 1) {
									$data['rangeopen']	= 1;
									$data['rangeclose']	= 1;
									$groupid = $this->user_model->insert('pr_calender', $data);
									$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));										
									// echo "<br>Update";
								}else{
									$data['rangeopen']	= 0;
									$data['rangeclose']	= 1;
									$this->user_model->insert('pr_calender', $data);
									// echo "<br>Insert";
								}
								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
								break;
							}else{
								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
								$data['rangeclose'] = 0;
								if ($day == 1) {
									$data['rangeopen'] = 1;
									$groupid = $this->user_model->insert('pr_calender', $data);
									$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));
									// echo "<br>Update";
								}else{
									$data['rangeopen'] = 0;
									$this->user_model->insert('pr_calender', $data);
									// echo "<br>Insert";
								}
							}

							$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
							$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
							$day++;
						}
					}
					
				}else
					echo json_encode(array('status' => false, 'msg' => "Please specify that selection is available or unavailable."));
			}
			if ($operation == 0) {
				$msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as available,';
			}elseif($operation == 1) {
				$msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as available with price $'.$eventPrice.'.';
			}elseif($operation == 2) {
				$msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as unavailable,';
			}
			echo json_encode(array('status' => true, 'msg' => $msg));
			die();
		}else
			echo json_encode(array('status' => false, 'msg' => "Please select date range"));
		die();
	}

	public function sortByDate($a,$b) {
	    $d1 = strtotime($a['start']);
	    $d2 = strtotime($b['start']);
	    return $d1 == $d2 ? 0 : ($d1 > $d2 ? 1 : -1);
	}

	public function ajaxDeleterange()
	{
		if(!$this->session->userdata('user_info'))
		{
			echo json_encode(array('status' => false, 'msg' => 'Please login to update this setting.'));
			die();
		}

		if (isset($_POST['id'])) {
			$this->user_model->delete('pr_calender', array('groupid' => $_POST['id']));
			// // $data[] = $v['value'];
			// 	echo json_encode(array('status' => true, 'price' => $post['price'], 'start' => $post['start'], 'end' => $post['end']));
			// 	die();
			echo json_encode(array('status' => true, 'msg' => 'Selected range has been deleted successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No range found to delete.'));
		}
	}

	public function ajaxSetSequrityDeposit($listing_id='')
	{
		
		if(!$this->session->userdata('user_info'))
		{
			echo json_encode(array('status' => false, 'msg' => 'Please login to update this setting.'));
			die();
		}

		if(empty($listing_id))
		{
			echo json_encode(array('status' => false, 'msg' => 'Listing info is missing.'));
			die();
		}

		if (isset($_POST['sequritydeposit'])) {
			$this->user_model->update('properties' , array('security_deposit' => $_POST['sequritydeposit']) , array('id' => $listing_id));
			echo json_encode(array('status' => true, 'msg' => 'Sequrity deposit has been set to '.number_format($_POST['sequritydeposit'], 2).' successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No sequrity deposit amount found.'));
		}
	}

	public function ajaxSetWeekendPrice($listing_id='')
	{
		
		if(!$this->session->userdata('user_info'))
		{
			echo json_encode(array('status' => false, 'msg' => 'Please login to update this setting.'));
			die();
		}

		if(empty($listing_id))
		{
			echo json_encode(array('status' => false, 'msg' => 'Listing info is missing.'));
			die();
		}

		if (isset($_POST['weekendprice'])) {
			$this->user_model->update('properties' , array('weekendprice' => $_POST['weekendprice']) , array('id' => $listing_id));
			echo json_encode(array('status' => true, 'msg' => 'Weekend Price has been set to '.number_format($_POST['weekendprice'], 2).' successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No weekend price found.'));
		}
	}

	public function ajaxDeleteWeekend()
	{
		if(!$this->session->userdata('user_info'))
		{
			echo json_encode(array('status' => false, 'msg' => 'Please login to update this setting.'));
			die();
		}

		if (isset($_POST['id'])) {
			$this->user_model->update('properties' , array('weekendprice' => 0.00) , array('id' => $_POST['id']));
			// // $data[] = $v['value'];
			// 	echo json_encode(array('status' => true, 'price' => $post['price'], 'start' => $post['start'], 'end' => $post['end']));
			// 	die();
			echo json_encode(array('status' => true, 'msg' => 'Weekend price has been deleted successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No property found to update.'));
		}
	}

	public function getNumDays($start='', $end='')
	{
		$numDays = abs($start - $end)/60/60/24;
		return $numDays;
	}

	public function ajaxSetWeeklyPrice($listing_id='')
	{
		if(!$this->session->userdata('user_info'))
		{
			echo json_encode(array('status' => false, 'msg' => 'Please login to update this setting.'));
			die();
		}

		if(empty($listing_id))
		{
			echo json_encode(array('status' => false, 'msg' => 'Listing info is missing.'));
			die();
		}


		if (!empty($_POST)) {
			$price = array();
			$start = array();
			$end = array();
			$id = array();
			$isExactWeekly = $_POST['isexact'];
			foreach ($_POST['post'] as $k => $v) {

				if ($v['name'] == 'price[]') {
					$price[] = $v['value'];
				}

				if ($v['name'] == 'start[]') {
					$start[] = $v['value'];
				}
				
				if ($v['name'] == 'end[]') {
					$end[] = $v['value'];
				}

				if ($v['name'] == 'id[]') {
					$id[] = $v['value'];
				}
			}
			$i=0;
			foreach ($price as $key => $prc) {
				$listing_price = array(
								'user_id' 			=>		get_my_id(), 
								'listing_id' 		=>		$listing_id, 
								'start_date' 		=>		$start[$i],
								'end_date' 			=>		$end[$i],
								'source' 			=>		'wk', 
								'type' 				=>		'w',
								'weekly_exact' 		=>		$isExactWeekly,
								'days' 				=>		$this->getNumDays($start[$i], $end[$i]), 
								'price' 			=>		$prc, 
								'update_datetime' 	=>		date('Y-m-d'),
								'ipaddress' 		=>		$this->input->ip_address(), 
							);
				if (empty($id[$i]))
					$this->user_model->insert('listing_price', $listing_price);
				else
					$this->user_model->update('listing_price', $listing_price, array('id' => $id[$i]));

				$i++;
			}
			echo json_encode(array('status' => true, 'msg' => 'Weekly price updated successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No pricing found to update.'));
		}
	}

	public function ajaxSetMonthlyPrice($listing_id='')
	{
		if(!$this->session->userdata('user_info'))
		{
			echo json_encode(array('status' => false, 'msg' => 'Please login to update this setting.'));
			die();
		}

		if(empty($listing_id))
		{
			echo json_encode(array('status' => false, 'msg' => 'Listing info is missing.'));
			die();
		}

		// echo json_encode(array('status' => false, 'msg' => $_POST));
		// die();
		if (!empty($_POST)) {
			$price = array();
			$start = array();
			$end = array();
			$id = array();
			$month = '';
			$year = 0;
			$isExactMonthly = $_POST['isexact'];
			foreach ($_POST['post'] as $k => $v) {
				if ($v['name'] == 'price[]') {
					$price[] = $v['value'];
				}

				if ($v['name'] == 'id[]') {
					$id[] = $v['value'];
				}

				if ($v['name'] == 'month[]') {
					$month = $v['value'];
				}
				
				if ($v['name'] == 'year[]') {
					$year = $v['value'];
					$start[] = strtotime('first day of '.$month.' '.$year);
					$end[] = strtotime('last day of '.$month.' '.$year);
				}

			}
				// echo json_encode(array('status' => true, 'start' => $start, 'end' => $end));
				// die();
			
			$i=0;
			foreach ($price as $key => $prc) {
				$listing_price = array(
								'user_id' 			=>		get_my_id(),
								'listing_id' 		=>		$listing_id,
								'start_date' 		=>		$start[$i],
								'end_date' 			=>		$end[$i],
								'source' 			=>		'm',
								'type' 				=>		'm',
								'monthly_exact' 	=>		$isExactMonthly,
								'days' 				=>		$this->getNumDays($start[$i], $end[$i]), 
								'price' 			=>		$prc, 
								'update_datetime' 	=>		date('Y-m-d'),
								'ipaddress' 		=>		$this->input->ip_address(), 
							);
				if (empty($id[$i]))
					$this->user_model->insert('listing_price', $listing_price);
				else
					$this->user_model->update('listing_price', $listing_price, array('id' => $id[$i]));

				$i++;
			}
			// // $data[] = $v['value'];
			// 	echo json_encode(array('status' => true, 'price' => $post['price'], 'start' => $post['start'], 'end' => $post['end']));
			// 	die();
			echo json_encode(array('status' => true, 'msg' => 'Monthly price updated successfully.'));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'No pricing found to update.'));
		}
	}

  /*Advance Pricing Ends*/



  /*Calender Work Starts*/

  public function calendar($pr_id="")
  {
  	    $this->check_login();
  	    if(empty($pr_id))
  	    {
  	    	redirect('user/properties');
  	    }
  	    $data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
	    $data['template'] = 'user/properties/calender';
		$this->load->view('templates/user_template', $data);		
  }


	public function days_of_month($month, $year)
	{
		return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}

	public function add_zero_if_needed($num=0)
	{
		// $num = 12;
		$num_padded = sprintf("%02s", $num);
		return $num_padded; // returns 04
	}

	public function get_events($month=0, $year=0)
	{
		//CRUD EVENTS http://developer-paradize.blogspot.in/2013/06/jquery-fullcalendar-integration-with.html
		// $_POST['month'] = 3;
		// $_POST['year'] = 2014;
		if ($_POST) {
			$listing_id = $_POST['property_id'];
			$property = $this->user_model->get_row('properties', array('id' => $listing_id));
			if ($property) {
				$weekDayPrice = $property->application_fee;
				$weekEndPrice = $property->weekendprice;
			}
			$month = (int) $_POST['month'];
			$month++;
			$month = $this->add_zero_if_needed($month);
			$year = (int) $_POST['year'];

			$strtotime = strtotime($year.'-'.$month.'-1');
			
			$a = getdate(strtotime('first day of '.date('F',$strtotime).' '.date('Y',$strtotime)));
			
	   		$no_of_days = $this->days_of_month($month, $year);

		}else{
			$weekDayPrice = '';
			$weekEndPrice = '';
			$month = date('m');
			$year = date('Y');
			$a = getdate(strtotime('first day of this month'));
			$date = explode("-", date('Y-m-d'));
	   		$no_of_days = $this->days_of_month($date[1], $date[0]);
			// $start1 = date('F-Y',strtotime('first day of april 2014'));
		}
		$order['by'] = 'start';
		$order['order'] = 'ASC';
		$events = $this->user_model->get_events('pr_calender', array('property_id' => $listing_id), $order, $month ,$year);
		$new_events = array();
		if ($events) {
			foreach ($events as $v1) {
				$new_events[date('Y-m-d', strtotime($v1->start))] = $v1;
			}
		}
		// print_r($events);
		// die();
		$event_data = array();
		$system = 1;
		$format = ($system == 1) ? 'Y-m-d H:i:s' : 'F j, Y h:i:s' ;

		// $first_day = date($format, strtotime('first day of this month'));
		// $last_day = date($format, strtotime('last day of this month'));

		$day = 0;
		
	    $day_count = 1;
			for ($i=0; $i < $no_of_days; $i++) {
				
				$beginOfDay = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
				$endOfDay = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);

				//1. UNAVAILABLE : #666,
				//2.Booked : #a10, 
				//3.AVALABLE : #819f2a
					if (isset($new_events[date('Y-m-d', $beginOfDay)])) {
						$events = $new_events[date('Y-m-d', $beginOfDay)];
						// if ($events->event_type == 1)
						// 	$color = '#a10';
						// else
						// 	$color = '#666';
						$event_data[] = array(
												'id' 				=> $events->id,
												'title' 			=> '$'.$events->price,
												// 'description' 		=> $events->description,
												'start' 			=> $beginOfDay,
												'end' 				=> $beginOfDay,
												// 'backgroundColor'	=> '#',
												'color' 			=> $events->color,
												'textColor' 		=> $events->textColor,
												// 'borderColor' 		=> '#',
												'allDay' 			=> true
												);

						// echo "<br><br>Event Start : ".date('Y-m-d',$beginOfDay);
						// echo "<br><br>Event End : ".date('Y-m-d',$beginOfDay);
					} else {
						if ((date('l', $beginOfDay) == 'Friday') || (date('l', $beginOfDay) == 'Saturday')){
							if ($weekEndPrice > 0)
								$price = $weekEndPrice;
							else
								$price = $weekDayPrice;
						}
						else
							$price = $weekDayPrice;

						$event_data[] = array(
												'title' 			=> $price,
												'description' 		=> 'its available.',
												'start' 			=> $beginOfDay,
												'end' 				=> $beginOfDay,
												// 'backgroundColor'	=> '#',
												'color' 			=> '#819f2a',
												'textColor' 		=> '#fff',
												// 'borderColor' 		=> '#',
												'allDay' 			=> true
												);

						// echo "<br><br>Available Start : ".date('Y-m-d',$beginOfDay);
						// echo "<br><br>Available End : ".date('Y-m-d',$beginOfDay);
					}

				$day++;
			}
		echo json_encode($event_data);
	}

	public function check_booking_dates()
	{
		if (isset($_POST['start']) & isset($_POST['end']) & isset($_POST['property_id']))
		{
			$start = strtotime($_POST['start']);
			$end = strtotime($_POST['end']);
			$bookings = $this->user_model->get_booking_dates($start, $end,$_POST['property_id']);
			if ($bookings)
				echo json_encode(array('status' => true, 'msg' => "Can't modify Booking Dates."));
			else
				echo json_encode(array('status' => false, 'msg' => "ok"));
		}
		else
			echo json_encode(array('status' => true, 'msg' => "Please specify Day/Days."));
	}

	public function add_event($listing_id = ''){

		// $_POST['currency_type'] = 'USD';
		// $_POST['start'] = '2014-05-27 00:00:00';
		// $_POST['end'] = '2014-05-29 23:59:59';
		// $_POST['eve_type'] = 1;
		// $_POST['eve_price'] = 1300;
		// $_POST['property_id'] = 91;
		if (isset($_POST['property_id']))
				$listing_id = $_POST['property_id'];
		$currency_type = $_POST['currency_type'];

		if (empty($listing_id)) {
			echo json_encode(array('status' => false, 'msg' => 'Property not found...'));
			die();
		}

		if (isset($_POST['start']) && isset($_POST['end'])) {
			$start = strtotime($_POST['start']);
			$end = strtotime($_POST['end']);
			// echo "<br> Start : ".date('F jS Y', $start);
			// echo "<br> End : ".date('F jS Y', $end);
			$rangeArray = $this->createranges($listing_id, $start, $end);
			// print_r($rangeArray);
			// die();
			$groupid = 0;
			$startselection = strtotime($_POST['start']);
			$endselection = strtotime($_POST['end']);
			$operation = 0;
			// $currency_type = $_POST['currency_type'];
			// print_r($rangeArray);
			// die();
			foreach ($rangeArray as $key => $value) {
				$start = $value['start'];
				$eve_start = $start;
				$end = $value['end'];
				$a = getdate($eve_start);
				$eve_end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
				$eve_type = 0;
				if (isset($_POST['eve_type'])) {
					if ($_POST['eve_type'] == 1) {
						if ($_POST['eve_price'] == 0) {
							// $eve_type = 1;
							// $title = 'Available';
							// $color = '#819f2a';
							$operation = 0;
							$eve = $this->user_model->make_event_available($start, $end,$listing_id);
							$day = 1;
							$groupid = 0;
							while (TRUE) {
								// print_r($value);
								// die();
								if (($eve_start <= $end) && ($eve_end >= $end)) {
									if (isset($value['data'])) {
										$data = $value['data'];
										$data['start'] = date('Y-m-d H:i:s', $eve_start);
										$data['end'] = date('Y-m-d H:i:s', $eve_end);
										$data['groupid'] = $groupid;
										$data['property_id'] = $listing_id;
										// print_r($data);
										if ($day == 1) {
											$data['rangeopen']	= 1;
											$data['rangeclose']	= 1;
											$groupid = $this->user_model->insert('pr_calender', $data);
											$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));	
										}else{
											$data['rangeopen']	= 0;
											$data['rangeclose']	= 1;
											$this->user_model->insert('pr_calender', $data);
										}
										// print_r($data);
									}
									//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
									break;
								}else{
									if (isset($value['data'])) {
										$data = $value['data'];
										$data['start'] = date('Y-m-d H:i:s', $eve_start);
										$data['end'] = date('Y-m-d H:i:s', $eve_end);
										$data['groupid'] = $groupid;
										$data['property_id'] = $listing_id;
										$data['rangeclose'] = 0;
										if ($day == 1) {
											$data['rangeopen'] = 1;
											// $groupid++;
											// $data['groupid'] = $groupid;
											$groupid = $this->user_model->insert('pr_calender', $data);
											$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));
											// print_r($data);
										}else{
											$data['rangeopen'] = 0;
											$this->user_model->insert('pr_calender', $data);
											// print_r($data);
										}
									}
									//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
								}

								$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
								$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
								$day++;
							}
							// $msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as available,';
							// echo json_encode(array('status' => true, 'msg' => $msg));
							// die();
						}else{
							$operation = 1;
							$eve = $this->user_model->make_event_available($start,$end,$listing_id);
							$day = 1;
							$groupid = 0;
							while (TRUE) {
									if (isset($value['data'])) {
										$data = $value['data'];
										$data['start'] = date('Y-m-d H:i:s', $eve_start);
										$data['end'] = date('Y-m-d H:i:s', $eve_end);
										$data['groupid'] = $groupid;
										$data['property_id'] = $listing_id;
										// print_r($data);
									}else{
										$data = array(
											'price' 		=> $_POST['eve_price'], 
											'currency_type' => $currency_type, 
											'start' 		=> date('Y-m-d H:i:s', $eve_start),
											'end' 			=> date('Y-m-d H:i:s', $eve_end),
											'groupid' 		=> $groupid,
											'color' 		=> '#819f2a',
											'textColor' 	=> '#fff',
											'event_type' 	=> 2,
											'property_id'   =>$listing_id
										);
									}

								if (($eve_start <= $end) && ($eve_end >= $end)) {
									if ($day == 1) {
										$data['rangeopen']	= 1;
										$data['rangeclose']	= 1;
										$groupid = $this->user_model->insert('pr_calender', $data);
										$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));	
									}else{
										$data['rangeopen']	= 0;
										$data['rangeclose']	= 1;
										$this->user_model->insert('pr_calender', $data);
									}
									//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
									// print_r($data);
									break;
								}else{
									//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
									$data['rangeclose'] = 0;
									if ($day == 1) {
										$data['rangeopen'] = 1;
										$groupid = $this->user_model->insert('pr_calender', $data);
										$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));
									}else{
										$data['rangeopen'] = 0;
										$this->user_model->insert('pr_calender', $data);
									}
								}

								$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
								$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
								$day++;
							}
							// $msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as available with price $'.$_POST['eve_price'].'.';
							// echo json_encode(array('status' => true, 'msg' => $msg));
							// die();
						}
					}else{
						$operation = 2;
						$eve = $this->user_model->make_event_available($start,$end,$listing_id);
						$day = 1;
						while (TRUE) {
							if (isset($value['data'])) {
								$data = $value['data'];
								$data['start'] = date('Y-m-d H:i:s', $eve_start);
								$data['end'] = date('Y-m-d H:i:s', $eve_end);
								$data['groupid'] = $groupid;
								$data['property_id'] = $listing_id;
							}else{
								$data = array(
											'start' 		=> date('Y-m-d H:i:s', $eve_start),
											'end' 			=> date('Y-m-d H:i:s', $eve_end),
											'color' 		=> '#666',
											'textColor' 	=> '#666',
											'groupid' 		=> $groupid,
											'event_type' 	=> $eve_type,
											'property_id'   =>$listing_id
											);
							}
							if (($eve_start <= $end) && ($eve_end >= $end)) {
								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
								if ($day == 1) {
									$data['rangeopen']	= 1;
									$data['rangeclose']	= 1;
									$groupid = $this->user_model->insert('pr_calender', $data);
									$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));	
								}else{
									$data['rangeopen']	= 0;
									$data['rangeclose']	= 1;
									$this->user_model->insert('pr_calender', $data);
								}
								break;
							}else{
								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
								$data['rangeclose'] = 0;
								if ($day == 1) {
									$data['rangeopen'] = 1;
									$groupid = $this->user_model->insert('pr_calender', $data);
									$this->user_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));
								}else{
									$data['rangeopen'] = 0;
									$this->user_model->insert('pr_calender', $data);
								}
							}

							$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
							$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
							$day++;
						}
						// $msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as unavailable,';
						// echo json_encode(array('status' => true, 'msg' => $msg));
						// die();
					}
				}else
					echo json_encode(array('status' => false, 'msg' => "Please specify that selection is available or unavailable."));
			}

			if ($operation == 0) {
				$msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as available,';
			}elseif($operation == 1) {
				$msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as available with price $'.$_POST['eve_price'].'.';
			}elseif($operation == 2) {
				$msg = 'Days from <strong>'.date('Y-m-d', $startselection).'</strong> to <strong>'.date('Y-m-d', $endselection).'</strong> are marked as unavailable,';
			}
			echo json_encode(array('status' => true, 'msg' => $msg));
			die();

		}else
			echo json_encode(array('status' => false, 'msg' => "Please select date range"));
		die();
	}

	// public function add_event($start='', $end='')
	// {
	// 	if (isset($_POST['start']) && isset($_POST['end'])) {
	// 		$property_id = $_POST['property_id'];
	// 		$currency_type = $_POST['currency_type'];
	// 		$start = $_POST['start'];
	// 		$end = $_POST['end'];
	// 		$start = $this->clean_input($start);
	// 		$end = $this->clean_input($end);
	// 		if (!empty($start) && !empty($end)) {
	// 			/*
	// 			* We are not doing anything over here. Just sending the values received back to the page.
	// 			* In a web application, we should be processing values and storing them in the database.
	// 			*/
	// 			$a = getdate(strtotime($end));
	// 			$end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
	// 			$a = getdate(strtotime($start));
	// 			$start = mktime(0,0,0,$a['mon'],$a['mday'],$a['year']);
	// 			$eve_start = $start;
	// 			$eve_end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);

	// 			$eve_type = 0;
	// 			$title = 'Not Available';
	// 			if (isset($_POST['eve_type'])) {
	// 				if ($_POST['eve_type'] == 1) {
	// 					if ($_POST['eve_price'] == 0) {
	// 						$eve_type = 1;
	// 						$title = 'Available';
	// 						$color = '#819f2a';
	// 						$eve = $this->user_model->make_event_available($start, $end,$property_id);
	// 						$msg = 'Days from <strong>'.date('Y-m-d', $start).'</strong> to <strong>'.date('Y-m-d', $end).'</strong> are marked as available,';
	// 					}else{
	// 						$eve = $this->user_model->make_event_available($start,$end,$property_id);
	// 						$day = 1;
	// 						while (TRUE) {
	// 							if (($eve_start <= $end) && ($eve_end >= $end)) {
	// 								$data = array(
	// 											'price' 		=> $_POST['eve_price'], 
	// 											'currency_type' => $currency_type, 
	// 											'start' 		=> date('Y-m-d H:i:s', $eve_start),
	// 											'end' 			=> date('Y-m-d H:i:s', $eve_end),
	// 											'color' 		=> '#819f2a',
	// 											'textColor' 	=> '#fff',
	// 											'event_type' 	=> 2,
	// 											'property_id'   =>$property_id,
	// 											); 
	// 								//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
	// 								$this->user_model->insert('pr_calender', $data);
	// 								break;
	// 							}else{
	// 								$data = array(
	// 											'price' 		=> $_POST['eve_price'], 
	// 											'currency_type' => $currency_type, 
	// 											'start' 		=> date('Y-m-d H:i:s', $eve_start),
	// 											'end' 			=> date('Y-m-d H:i:s', $eve_end),
	// 											'color' 		=> '#819f2a',
	// 											'textColor' 	=> '#fff',
	// 											'event_type' 	=> 2,
	// 											'property_id'   =>$property_id,
	// 											); 
	// 											//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
	// 								$this->user_model->insert('pr_calender', $data);
	// 							}

	// 							$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
	// 							$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
	// 							$day++;
	// 						}
	// 						$msg = 'Days from <strong>'.date('Y-m-d', $start).'</strong> to <strong>'.date('Y-m-d', $end).'</strong> are marked as available with price $'.$_POST['eve_price'].'.';
	// 					}
	// 				}else{

	// 					$eve = $this->user_model->make_event_available($start,$end,$property_id);
	// 					$day = 1;
	// 					while (TRUE) {
	// 						if (($eve_start <= $end) && ($eve_end >= $end)) {
	// 							$data = array(
	// 										'start' 		=> date('Y-m-d H:i:s', $eve_start),
	// 										'end' 			=> date('Y-m-d H:i:s', $eve_end),
	// 										'color' 		=> '#666',
	// 										'textColor' 	=> '#666',
	// 										'event_type' 	=> $eve_type,
	// 										'property_id'   =>$property_id,
	// 										); //UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
	// 							// print_r($data);
	// 							$this->user_model->insert('pr_calender', $data);
	// 							break;
	// 						}else{
	// 							$data = array(
	// 										'start' 		=> date('Y-m-d H:i:s', $eve_start),
	// 										'end' 			=> date('Y-m-d H:i:s', $eve_end),
	// 										'color' 		=> '#666',
	// 										'textColor' 	=> '#666',
	// 										'event_type' 	=> $eve_type,
	// 										'property_id'   =>$property_id,
	// 										); //UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
	// 							$this->user_model->insert('pr_calender', $data);
	// 						}

	// 						$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
	// 						$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
	// 						$day++;
	// 					}

	// 					$msg = 'Days from <strong>'.date('Y-m-d', $start).'</strong> to <strong>'.date('Y-m-d', $end).'</strong> are marked as unavailable,';
	// 				}
	// 			}

	// 			echo json_encode(array('status' => true, 'msg' => $msg));
	// 		}else
	// 			echo json_encode(array('status' => false, 'msg' => 'Please select some date.'));
	// 	}else
	// 		echo json_encode(array('status' => false, 'msg' => 'Please select booking date.'));
	// }

	public function add_booking_event($property_id=0, $start='', $end='')
	{
		// $start = strtotime('2014-03-08 09:00:00');
        //  $end = strtotime('2014-03-12 19:00:00');
		$property_id = 20;
		if ($property_id == 0) {
			echo json_encode(array('status' => FALSE, 'msg' => 'No Property is specified for booking.'));
			die();
		}
		$start = '2014-04-15 00:00:00';
		$end = '2014-04-17 23:59:59';
		// $_POST['eve_type'] = 1;
		// print_r($_POST);
		// $_POST['start']
		// echo json_encode(array('status' => true, 'msg' => $_POST));
		// die();
			$start = $this->clean_input($start);
			$end = $this->clean_input($end);
			// echo json_encode(array('status' => true, 'start' => $_POST['start'], 'end' => $_POST['end']));
			// die();
			if (!empty($start) && !empty($end)) {
				/*
				* We are not doing anything over here. Just sending the values received back to the page.
				* In a web application, we should be processing values and storing them in the database.
				*/
				$a = getdate(strtotime($end));
				$end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
				$a = getdate(strtotime($start));
				$start = mktime(0,0,0,$a['mon'],$a['mday'],$a['year']);
				$eve_start = $start;
				$eve_end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);

				$eve_type = 1;
				$title = 'Booked';
				$day = 1;
				while (TRUE) {
					if (($eve_start <= $end) && ($eve_end >= $end)) {
						$data = array(
									'property_id' 	=> $property_id,
									'title' 		=> $title,
									'description'	=> 'Its Booked', 
									'start' 		=> date('Y-m-d H:i:s', $eve_start),
									'end' 			=> date('Y-m-d H:i:s', $eve_end),
									'color' 		=> '#a10',
									'textColor' 	=> '#a10',
									'event_type' 	=> $eve_type,
									); //UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
						// print_r($data);
						$this->user_model->insert('pr_calender', $data);
						break;
					}else{
						$data = array(
									'property_id' 	=> $property_id,
									'title' 		=> $title,
									'description'	=> 'Its Booked', 
									'start' 		=> date('Y-m-d H:i:s', $eve_start),
									'end' 			=> date('Y-m-d H:i:s', $eve_end),
									'color' 		=> '#a10',
									'textColor' 	=> '#a10',
									'event_type' 	=> $eve_type,
									); //UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
						$this->user_model->insert('pr_calender', $data);
					}

					$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
					$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
					$day++;
				}

				$msg = 'ok';
				echo json_encode(array('status' => true, 'msg' => $msg));
			}else
				echo json_encode(array('status' => false, 'msg' => 'No Booking Date/Dates.'));
	}

	## Clean the input from script, html, style, and almost all potenially harmful tags.
	public function clean_input($input) {
		$search = array(
			'@<script[^>]*?>.*?</script>@si',   /* strip out javascript */
			'@<[\/\!]*?[^<>]*?>@si',            /* strip out HTML tags */
			'@<style[^>]*?>.*?</style>@siU',    /* strip style tags properly */
			'@<![\s\S]*?--[ \t\n\r]*>@'         /* strip multi-line comments */
		);

		$output = preg_replace($search, '', $input);
		return $output;
	}

  public function update_calender($pr_id)
  {
     if(empty($pr_id)): return false;
     else:
     $status = $this->user_model->update('properties',array('update_calender'=>time()),array('id'=>$pr_id));
      if($status)
      {
         echo "Last Updated few seconds before";
      }
     endif;

  }

  /*Calender Work Ends*/

  /*Manage listing Work starts*/

  public function manage_listing($pr_id="")
  {
  	$this->check_login();
  	if(empty($pr_id))
  	{
  		redirect('user/properties');
  	}
	$data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
	$data['template'] = 'user/properties/manage_listing';
	$this->load->view('templates/user_template', $data);
  }
  /*Manage listing Work Ends*/

  public function changemsgstatus($msgid = ""){
  	if($msgid =="")
  	return FALSE;
  	$this->user_model->update('user_message', array('status'=>1), array('id'=>$msgid));
  }

  public function change_property_status()
  {
     	$pr_id = $_POST['property_id'];
		$data['property'] = $this->user_model->get_row('properties', array('id' => $pr_id));
		if($data['property']->status == 0){
			$array = array('status' => 1, );
				$gain = $this->user_model->get_row('invitation',array('joined_user_id'=>$data['property']->user_id));
	            if(!empty($gain) && $gain->property_credit_given==0)
	            {
				    $this->user_model->update('invitation',array('property_credit_given'=>1),array('joined_user_id'=>$data['property']->user_id));
	            }
			$this->user_model->update('properties',$array,array('id' => $pr_id));
		    echo "active";
		}
		else{
			$array = array('status' =>0, );
			$this->user_model->update('properties',$array,array('id' => $pr_id));
		    echo "inactive"; 
		}
  }


  /*Wasim sir task Starts*/
	// public function price_terms($pr_id='')
 //    {
 //    	$this->check_login();
	// 	$data['properties'] = $this->user_model->get_row('properties',array('id'=>$pr_id));
	// 	$data['can_policies'] = $this->user_model->get_result('cancellation_policies');
	// 	if(empty($pr_id)) redirect('user/properties');
	// 	$this->form_validation->set_rules('currency_type', 'Currency Type', 'trim|required');
	// 	$this->form_validation->set_rules('nightly', 'Nightly', 'trim|required');
	// 	$this->form_validation->set_rules('weekly', 'Weekly', 'trim|required');
	// 	$this->form_validation->set_rules('monthly', 'Monthly', 'trim|required');
	// 	if ($this->input->post('is_sublet')) {
	// 		$data['is_sublet'] = $this->input->post('is_sublet');
	// 		$this->form_validation->set_rules('sublet_start', 'Sublet Start', 'trim|required');
	// 		$this->form_validation->set_rules('sublet_end', 'Sublet End', 'trim|required');
	// 		$this->form_validation->set_rules('sublet_price', 'Sublet Price', 'trim|required');
	// 		// $sublet_start = date('Y-m-d H:i:s', $this->input->post('sublet_start'));
	// 		// $sublet_end = date('Y-m-d H:i:s', $this->input->post('sublet_end'));
	// 		$sublet_start = $this->input->post('sublet_start');
	// 		$sublet_end = $this->input->post('sublet_end');
	// 		$sublet_price = $this->input->post('sublet_price');
	// 		$sublet_start = date('Y-m-d H:i:s', strtotime($sublet_start));
	// 		$sublet_end = date('Y-m-d H:i:s', strtotime($sublet_end));
	// 		// echo "<br>".date('Y-m-d H:i:s', strtotime($sublet_start));
	// 		// echo "<br>".date('Y-m-d H:i:s', strtotime($sublet_end));
	// 		// die();
	// 	}else{
	// 		$data['is_sublet'] = $data['properties']->sublet_status;
	// 		$sublet_start = '0000-00-00 00:00:00';
	// 		$sublet_end = '0000-00-00 00:00:00';
	// 		$sublet_price = '0.00';
	// 	}
	// 	$this->form_validation->set_rules('additional_guest_price', 'Additional Guest Price', 'trim|required');
	// 	$this->form_validation->set_rules('additional_guest', 'Additional Guest', 'trim|required');
	// 	$this->form_validation->set_rules('cleaning_fee', 'Cleaning Fee', 'trim|required');//numeric
	// 	$this->form_validation->set_rules('cancellation_policies', 'Cancellation Policies', 'trim|required');
	// 	$this->form_validation->set_rules('min_stay', 'Minimum Stay', 'trim|required');
	// 	$this->form_validation->set_rules('max_stay', 'Maximum Stay', 'trim|required');
	// 	$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');

	// 	if($this->form_validation->run() == TRUE)
	// 	{
	//   			$properties_table = array(
	// 									  'currency_type'  			=> $this->input->post('currency_type'),
	// 									  'application_fee'   		=> $this->input->post('nightly'),
	// 								  	  'weekly'  				=> $this->input->post('weekly'),
	// 								  	  'monthly' 				=> $this->input->post('monthly'),
	// 								  	  'sublet_status' 			=> $this->input->post('is_sublet'),
	// 								  	  'sublet_from_date' 		=> $sublet_start,
	// 								  	  'sublet_to_date' 			=> $sublet_end,
	// 								  	  'sublet_price' 			=> $sublet_price,
	// 								  	  'additional_guest_price' 	=> $this->input->post('additional_guest_price'),
	// 								  	  'additional_guest' 		=> $this->input->post('additional_guest'),
	// 								  	  'cleaning_fee' 			=> $this->input->post('cleaning_fee'),
	// 								  	  'cancellation_policies' 	=> $this->input->post('cancellation_policies'),
	// 								  	  'min_stay' 				=> $this->input->post('min_stay'),
	// 								  	  'max_stay' 				=> $this->input->post('max_stay')
	// 		                         );
	//   			// print_r($properties_table);
	//   			// die();
	// 			$this->user_model->update('properties', $properties_table, array('id'=>$pr_id));

	//      		$this->session->set_flashdata('success_msg','Price And Terms Updated Succesfullly');
	// 			redirect('user/price_terms/'.$pr_id);
	//     }
		
	// 	// die();
	//     $data['template'] = 'user/properties/price_terms';
	//     $this->load->view('templates/user_template', $data);
 //   }

	public function view_profile($offset=0)
	{
		$this->check_login();
		$limit = 3;
		$data['properties'] = $this->user_model->property_details_for_profile($limit, $offset, get_my_id());
		$data['total_rows'] = $this->user_model->property_details_for_profile(0, 0, get_my_id());
		$data['count'] = count($data['properties']);
		$data['user'] = $this->user_model->get_row('users', array('id' => get_my_id()));
		// print_r($data['properties']);
		// die();
		$data['groups'] = $this->user_model->get_my_groups(get_my_id());
		$data['language'] = $this->user_model->get_result('languages', array('user_id' => get_my_id()));
		$data['template'] = 'user/account/view_profile';
    	$this->load->view('templates/user_template', $data);
	}


	public function ajax_load_property($offset=0)
	{
  		$limit = 3;
  		$properties = $this->user_model->property_details_for_profile($limit, $offset, get_my_id());
  		// $total_rows = $this->user_model->property_details_for_profile(0, 0, get_my_id());
  		$res="";
		if($properties){

			foreach ($properties as $row):
				if($row->accommodates > 1){ $guest = $row->accommodates.' Guests'; }else{ $guest = $row->accommodates.' Guest'; }
				$res.= '<div class="row property col-md-11">';
		            $res.= '<div class="col-md-4">';
		            $res.= '<img src="<?php echo base_url(); ?>assets/uploads/property/'.$row->featured_image.'" alt="Not Available" class="img-thumbnail">';
		            $res.= '</div>';
		            $res.= '<div class="col-md-7 row">';
			            $res.= '<div class="prop-data col-md-11">'.$row->title.'</div>';
		                $res.= '<div class="prop-data col-md-11">'.$row->unit_street_address.'</div>';
		                $res.= '<div class="prop-data col-md-6">'.get_property_type($row->property_type_id).'</div><div class="prop-data col-md-6">'.$guest.'</div>';
		                $res.= '<div class="prop-data col-md-6">'.$row->square_feet.' sqft.</div><div class="prop-data col-md-6">'.$row->currency_type.' '.number_format($row->application_fee, 2).'</div>';
		            $res.= '</div>';
	            $res.= '</div>';
			endforeach;
			echo json_encode(array('status' => true, 'resp' => $res));
		}else
			echo json_encode(array('status' => false, 'msg' => 'No item found.'));
    }


  /*Wasim sir task Ends*/


  public function reciept($id=""){
		if($id=="")
			redirect('user/bookings');
		$data['booking'] = $this->user_model->get_row('bookings', array('id'=>$id));
		$data['template'] = 'user/reciept';
        $this->load->view('templates/user_template', $data);	
	}

	public function trust_and_verification() {
		$this->check_login();
		$data['varifications'] = $this->user_model->get_row('users', array('id' => get_my_id()));
		// print_r($data['varifications']);
		// die();
		$data['template'] = 'user/account/trust_and_varification';
		$this->load->view('templates/user_template', $data);
	}

	public function facebookConnect(){
		 //get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
        //include the facebook.php from libraries directory
		// Live URL :http://www.bnbclone.com/demo/
		
    	$facebook = new Facebook(
							array(
								'appId'		=>  $this->config->item('appID'), 
								'secret'	=> 	$this->config->item('appSecret')
							)
						);	

		$user = $facebook->getUser(); // Get the facebook user id 

		if($user){
			try{
					$user_profile = $facebook->api('/me','get');

					if ($user_profile['id'] != 0) {
						$this->user_model->update('users', array('facebook_id' => $user_profile['id']), array('id' => get_my_id()));
					}

			        ///////////////////////////////////////////
				    //////////User connect call////////////////
			   		$this->session->set_flashdata('success_msg', 'You are successfully connected with facebook.');
			   		redirect('user/trust_and_varification');
				    ///////////////////////////////////////////
			        ///////////////////////////////////////////
			}catch(FacebookApiException $e){
				print_r($e);
				error_log($e);
				$user = NULL;
			}
		}
	}

	public function facebookDisconnect()
	{
		$this->user_model->update('users', array('facebook_id' => ''), array('id' => get_my_id()));
		$this->session->set_flashdata('success_msg', 'You are successfully disconnected with facebook, you can reconnect again anytime.');
		redirect('user/trust_and_varification');
	}

	public function googleConnect(){
		require_once APPPATH.'libraries/Googleapi/lightopenid.php';
		$openid = new LightOpenID(base_url());			
		$openid->identity = 'https://www.google.com/accounts/o8/id';
		$openid->required = array(
								'namePerson/first',
								'namePerson/last',
								'contact/email',
								);

		$openid->returnUrl = base_url().'user/googleResponse';
        redirect($openid->authUrl());
    }

    public function googleResponse()
    {
    	// $this->load->library('Googleapi/lightopenid');
        require_once APPPATH.'libraries/Googleapi/lightopenid.php';
         ////////////////////////////////////////////
        //////////////google response///////////////
       ////////////////////////////////////////////
        
        $openid = new LightOpenID(base_url());
  
		if ($openid->mode) {
			if ($openid->mode == 'cancel') {
			   redirect('front');
			} 
		    elseif($openid->validate()) {
				$data = $openid->getAttributes();
				$email = $data['contact/email'];
				$first = $data['namePerson/first'];
				$last  = $data['namePerson/last'];

				$a = strpos($openid->identity,'?id=');

				$info['google_id'] = substr($openid->identity,$a+4);
				$info['first_name'] = $first;
				$info['last_name']  = $last;
				$info['user_email'] = $email;
				$this->user_model->update('users', array('google_id' => $info['google_id']), array('id' => get_my_id()));
				$this->session->set_flashdata('success_msg', 'You are successfully connected with Google+.');
			   	redirect('user/trust_and_varification');
            } 
			else {
				$this->session->set_flashdata('error_msg', "Can't get data");
				redirect('user/trust_and_varification');
			}
		} 
		else{
			$this->session->set_flashdata('error_msg', "Can't get data");
			redirect('user/trust_and_varification');
		}
    }

    public function googleDisconnect()
	{
		$this->user_model->update('users', array('google_id' => ''), array('id' => get_my_id()));
		$this->session->set_flashdata('success_msg', 'You are successfully disconnected with Google+, you can reconnect again anytime.');
		redirect('user/trust_and_varification');
	}

	public function activate_listing($pr_id="")
	{
		if(empty($pr_id))
		{
			redirect('user/properties');
		}
	  $this->user_model->update('properties',array('publish_permission'=>1));
      $this->session->set_flashdata('success_msg', 'Your listing is activated successfully.');
	  redirect('user/manage_listing/'.$pr_id);
	}

	public function transaction_history($offset=0)
 	{
 		$this->check_login();
 		$user = $this->session->userdata('user_info');
		$id = $user['id'];
		$limit = 10;
		$data['transaction_history']=$this->user_model->transaction_history($limit,$offset);
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/transaction_history/';
		$config['total_rows']=$this->user_model->transaction_history(0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/account/transaction_history';
		$this->load->view('templates/user_template',$data);	 		
 	}


 	/*Featured Image Starts*/

	public function featured_images($property_id="",$offset=0)
 	{
 		$this->check_login();
        $limit = 10;
		$data['featured_images']=$this->user_model->get_pagination_where('pr_gallery',$limit,$offset,array('pr_id'=>$property_id));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'user/featured_images/'.$property_id;
		$config['total_rows']=$this->user_model->get_pagination_where('pr_gallery',0,0,array('pr_id'=>$property_id));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		$data['property_id'] = $property_id;
		$data['offset'] = $offset;
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'user/properties/featured_images';
		$this->load->view('templates/user_template',$data);	 		
 	}

    public function request_for_make_featured_image($id="",$property_id="")
 	{
      $responce_1 = $this->user_model->get_row('pr_gallery',array('id'=>$id));
      if(empty($id) || $responce_1->pr_id != $property_id)
      {
      	redirect('user/featured_images'.$property_id);
      }
      
      $this->user_model->update('pr_gallery',array('featured_request'=>1),array('id'=>$id));
   
  /*Email For Featured Image Starts*/

      $pr_gallery_row = $this->user_model->get_row('pr_gallery',array('id'=>$id));
      $property_id = $pr_gallery_row->pr_id;
      $pr_info = $this->user_model->get_row('properties',array('id'=>$property_id));


      $superadmin_info = $this->user_model->get_row('users',array('id'=>1));
      $superadmin_email = $superadmin_info->user_email;


		$this->load->library('smtp_lib/smtp_email');

        $email_template = get_manage_email_info('user_request_for_featured_image');
        $website_info = get_manage_email_info('website_url_and_email');
        $website_url = $website_info->website_url;
        $from_email = $website_info->message_from_which_email;


        $subject = $email_template->subject;

        $image_picture = '<img style="width:100px;height:100px;border:1px solid #101010;border-radius:3px;box-shadow:1px 1px 5px" src='.base_url().'assets/uploads/property/'.$pr_gallery_row->image_file.'>';		        
       
        $image_link = '<a href='.base_url().'assets/uploads/property/'.$pr_gallery_row->image_file.'>Click Here to see featured Image</a>';

        $arr_old = array('&lt;%propertyimage%&gt;','&lt;%imagelink%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
        $arr_update = array($image_picture,$image_link,$pr_info->title,$website_url);
        $html =  str_replace($arr_old,$arr_update,$email_template->content);

        $to = array($superadmin_email);
        $from = array($from_email =>$from_email);  // From email in array form 

		$this->smtp_email->sendEmail($from, $to, $subject, $html);
       
        $insert_data = array(
				        	  'name'=>$superadmin_info->first_name,
				        	  'email'=>$superadmin_info->user_email,
				        	  'subject'=>$subject,
				        	  'message'=>$html,
				        	  'created'=>date('Y-m-d h:i:s')
				        	  );

        $this->user_model->insert('notifications',$insert_data);


      /*Email For Featured Images Ends*/

     
      $this->session->set_flashdata('success_msg', 'Your request has been Succesfullly sent to superadmin for featured image.');
	  redirect('user/featured_images/'.$property_id);
  	}

	
 	public function pay_for_featured_subscriptions($property_id="")
 	{
   
		          $image_id = $_POST['image_id'];
		$subscriptions_type =  $_POST['subscriptions_type'];
		$subscriptions_amount =  $_POST['subscriptions_amount'];
       
        $featured_info = array(
    	                              'image_id'=>$image_id,
    	                    'subscriptions_type'=>$subscriptions_type,
    	                           'property_id'=>$property_id,
        	                    );
         $this->session->set_userdata('featured_info',$featured_info);
         
        $pro_info = $this->user_model->get_row('properties',array('id'=>$property_id));
        $data['user_info'] = $user_info = $this->user_model->get_row('users',array('id'=>$pro_info->user_id));
        $data['amount'] = $subscriptions_amount;
        $data['template'] = 'user/properties/confirm_featured_payment';
        $this->load->view('templates/user_template', $data);
  	}


  	public function success_featured_pay()
  	{
     $featured_info = $this->session->userdata('featured_info');
     $update_data = array(
     	                  'is_featured_image'=>1,
                          'subscription_date'=>date('d F Y'),
                          'subscription_type'=>$featured_info['subscriptions_type'],
     	                  );
		$this->user_model->update('pr_gallery',$update_data,array('id'=>$featured_info['image_id']));
		$this->session->set_flashdata('success_msg', 'Your Image has been Succesfullly set to Featured Image.');
		redirect('user/featured_images/'.$featured_info['property_id']);
  	}

 	/*Featured Image ends*/


    /*Checking tools of pricing Starts*/
  
   public function check_pricing_tools()
   {
		$pr_id = $_POST['pr_id'];
		$pr_info = get_property_detail($pr_id);
		$c_in = $_POST['check_in'];
		$c_out = $_POST['check_out'];
		$no_of_guest = $_POST['no_of_guest'];

		$remaining_guest = $no_of_guest - $pr_info->additional_guest;
         

		$check_in_time =  strtotime($c_in);
		$check_out_time = strtotime($c_out);
		$differrence =  $check_out_time-$check_in_time;
		$number_of_days = ($differrence/86400)+1;

        

        $total_amount = 0;
        if($remaining_guest > 0)
        {
        	$additional_guest_amount = $remaining_guest*$pr_info->additional_guest_price*$number_of_days;
        } 

        

       if($number_of_days < 7)
       {

        $check_in = date('Y-m-d',$check_in_time);
        $check_out = date('Y-m-d',$check_out_time);

         /*If Partial result found in pr_calender table  Starts*/
               for($i=1;$i<=$number_of_days;$i++)
               {
                 $res_checkin = $this->user_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   { 
                     $total_amount += $res_checkin->price;
                     echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$res_checkin->price." in ".$pr_info->currency_type." (Daily Advance) </li>";
                   }
                   else
                   {
                     if(!empty($pr_info->weekendprice))
                     {
                        $day  = date('D',$check_in_time);
                        if($day=="Fri" || $day=="Sat")
                        {
                           $per_day = ($pr_info->weekendprice)/2;
                           $total_amount += round($per_day);
                           echo "<li>".date('d-m-Y',$check_in_time)." has Weekend price of ".$pr_info->weekendprice." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekend) </li>";
                        }
                        else
                        {
                           $total_amount +=$pr_info->application_fee;
                           echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
                        }
                     }
                     else
                     {
                          $total_amount +=$pr_info->application_fee;
                          echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
                     }

                   }
                     $check_in_time = strtotime($check_in);
                     $check_in_time = $check_in_time+24*60*60;
                     $check_in = date('Y-m-d',$check_in_time);
               }
          
			if($remaining_guest > 0)
			{
              echo "<li> Price increased by ".$additional_guest_amount." in ".$pr_info->currency_type." for ".$remaining_guest." additional guests  (".$additional_guest_amount/$remaining_guest." ".$pr_info->currency_type." per night per guest beyond ".$pr_info->additional_guest." guests)</li>";
			  $total_amount+= $additional_guest_amount;
			}
           echo "<li> Total Amount ".$total_amount." in ".$pr_info->currency_type."</li>";
            
           exit;
         /*If Partial result found in pr_calender table Ends*/
       }

       if($number_of_days>=7 && $number_of_days<30)
       {
          for ($i=1; $i<=$number_of_days ; $i++)
          { 
               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','wk');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk = $query->row();
               if(!empty($resulk))
               {
                   if($resulk->weekly_exact==1)
                   {
                      $diff = $resulk->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      if($days==7 && $check_out_time>=$resulk->end_date)
                      {
                      	$daily_rate = $resulk->price/7;
                        for ($h=1;$resulk->end_date>=$check_in_time; $h++)
                        { 
                           echo "<li>".date('d-m-Y',$check_in_time)." has Weekly price of ".$resulk->price." ".$pr_info->currency_type." ; ".round($daily_rate)." ".$pr_info->currency_type." Per day in (Weekly exact) </li>";
                           $check_in_time = $check_in_time+86400;
	                       $i++;
                        }
                        $total_amount +=$resulk->price;
                      }
		               else
		               {

		                  $check_in =  date('Y-m-d',$check_in_time);
		                  $res_checkin = $this->user_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
		                   if(!empty($res_checkin))
		                   {
		                     $total_amount += $res_checkin->price;
		                     echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$res_checkin->price." in ".$pr_info->currency_type." (Daily Advance) </li>";
		                   }
		                   else
		                   {
		                        if(!empty($pr_info->weekendprice))
		                        {
		                           $day  = date('D',$check_in_time);
		                           if($day=="Fri" || $day=="Sat")
		                           {
			                           $per_day = ($pr_info->weekendprice)/2;
			                           $total_amount += $per_day;
                                       echo "<li>".date('d-m-Y',$check_in_time)." has Weekend price of ".$pr_info->weekendprice." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekend) </li>";
		                           }
		                           else
		                           {
		                              $total_amount +=$pr_info->application_fee;
		                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
		                           }
		                        }
		                        else
		                        {
		                             $total_amount +=$pr_info->application_fee;
		                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
		                        }
		                   }
		               }                     
                   }
                   else
                   {
                     $per_day = ($resulk->price)/7;
                     $total_amount += round($per_day);
    	             echo "<li>".date('d-m-Y',$check_in_time)." has Weekly price of ".$resulk->price." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekly Any stay) </li>";
                   }
               }
               elseif(!empty($pr_info->weekly))
               {
                  $per_day = ($pr_info->weekly)/7;
                  $total_amount += round($per_day);
	               echo "<li>".date('d-m-Y',$check_in_time)." has Weekly price of ".$pr_info->weekly." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekly Basic) </li>";
               }                     
               else
               {
                  $check_in =  date('Y-m-d',$check_in_time);
                  $res_checkin = $this->user_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   {
                     $total_amount += $res_checkin->price;
                     echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$res_checkin->price." in ".$pr_info->currency_type." (Daily Advance) </li>";
                   }
                   else
                   {
                        if(!empty($pr_info->weekendprice))
                        {
                           $day  = date('D',$check_in_time);
                           if($day=="Fri" || $day=="Sat")
                           {
	                           $per_day = ($pr_info->weekendprice)/2;
	                           $total_amount += $per_day;
                               echo "<li>".date('d-m-Y',$check_in_time)." has Weekend price of ".$pr_info->weekendprice." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekend) </li>";
                           }
                           else
                           {
                              $total_amount +=$pr_info->application_fee;
                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
                           }
                        }
                        else
                        {
                             $total_amount +=$pr_info->application_fee;
                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
                        }
                   }
               }                     
                  $check_in_time = $check_in_time+24*60*60;
          }

			if($remaining_guest > 0)
			{
              echo "<li> Price increased by ".$additional_guest_amount." in ".$pr_info->currency_type." for ".$remaining_guest." additional guests  (".$additional_guest_amount/$remaining_guest." ".$pr_info->currency_type." per night per guest beyond ".$pr_info->additional_guest." guests)</li>";
			  $total_amount+= $additional_guest_amount;
			}

           echo "<li> Total Amount ".$total_amount." in ".$pr_info->currency_type."</li>";
           exit;
       }


       if($number_of_days>=30)
       {
          for ($i=1; $i<=$number_of_days ; $i++)
          { 
               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','m');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk = $query->row();

               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','wk');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk_weakly = $query->row();

               if(!empty($resulk))
               {
                   if($resulk->monthly_exact==1)
                   {
                      $diff = $resulk->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      $month_days = $resulk->days+1;
                      if($days==$month_days && $check_out_time>=$resulk->end_date)
                      {
                      	$daily_rate = $resulk->price/$days;
	                        for ($h=1;$h<=$days; $h++)
	                        { 
	                           echo "<li>".date('d-m-Y',$check_in_time)." has Monthly price of ".$resulk->price." ".$pr_info->currency_type." ; ".round($daily_rate)." ".$pr_info->currency_type." Per day in (Monthly exact) </li>";
	                           $check_in_time = $check_in_time+86400;
		                       $i++;
	                        }
                        $total_amount +=$resulk->price;
                      }
		               else
		               {
		                  $check_in =  date('Y-m-d',$check_in_time);
		                  $res_checkin = $this->user_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
		                   if(!empty($res_checkin))
		                   {
		                     $total_amount += $res_checkin->price;
		                     echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$res_checkin->price." in ".$pr_info->currency_type." (Daily Advance) </li>";
		                   }
		                   else
		                   {
		                        if(!empty($pr_info->weekendprice))
		                        {
		                           $day  = date('D',$check_in_time);
		                           if($day=="Fri" || $day=="Sat")
		                           {
			                           $per_day = ($pr_info->weekendprice)/2;
			                           $total_amount += $per_day;
                                       echo "<li>".date('d-m-Y',$check_in_time)." has Weekend price of ".$pr_info->weekendprice." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekend) </li>";
		                           }
		                           else
		                           {
		                              $total_amount +=$pr_info->application_fee;
		                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
		                           }
		                        }
		                        else
		                        {
		                              $total_amount +=$pr_info->application_fee;
		                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
		                        }
		                    }
		                }                     
                   }
                   else
                   {
					 $diff = $resulk->end_date-$resulk->start_date;
					 $days = ($diff/86400)+1;
                     $per_day = ($resulk->price)/$days;
                     echo "<li>".date('d-m-Y',$check_in_time)." has Monthly price of ".$resulk->price." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Monthly Any Stay) </li>";
                     $total_amount += round($per_day);
                   }
               }


               elseif(!empty($resulk_weakly))
               {
                   if($resulk_weakly->weekly_exact==1)
                   {
                      $diff = $resulk_weakly->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      if($days==7 && $check_out_time>=$resulk_weakly->end_date)
                      {
                      	$daily_rate = $resulk_weakly->price/7;
                        for ($h=1;$h<=7; $h++)
                        { 
                           echo "<li>".date('d-m-Y',$check_in_time)." has Weekly price of ".$resulk_weakly->price." ".$pr_info->currency_type." ; ".round($daily_rate)." ".$pr_info->currency_type." Per day in (Weekly exact) </li>";
                           $check_in_time = $check_in_time+86400;
	                       $i++;
                        }
                        $total_amount +=$resulk_weakly->price;
                      }
		               else
		               {

		                  $check_in =  date('Y-m-d',$check_in_time);
		                  $res_checkin = $this->user_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
		                   if(!empty($res_checkin))
		                   {
		                     $total_amount += $res_checkin->price;
		                     echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$res_checkin->price." in ".$pr_info->currency_type." (Daily Advance) </li>";
		                   }
		                   else
		                   {
		                        if(!empty($pr_info->weekendprice))
		                        {
		                           $day  = date('D',$check_in_time);
		                           if($day=="Fri" || $day=="Sat")
		                           {
			                           $per_day = ($pr_info->weekendprice)/2;
			                           $total_amount += $per_day;
                                       echo "<li>".date('d-m-Y',$check_in_time)." has Weekend price of ".$pr_info->weekendprice." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekend) </li>";
		                           }
		                           else
		                           {
		                              $total_amount +=$pr_info->application_fee;
		                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
		                           }
		                        }
		                        else
		                        {
		                             $total_amount +=$pr_info->application_fee;
		                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
		                        }
		                     }
		                }                     
                   }
                   else
                   {
                     $per_day = ($resulk_weakly->price)/7;
                     $total_amount += round($per_day);
    	             echo "<li>".date('d-m-Y',$check_in_time)." has Weekly price of ".$resulk_weakly->price." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekly Any stay) </li>";
                   }
               }

               elseif(!empty($pr_info->monthly))
               {
                  $per_day = ($pr_info->monthly)/30;
	               echo "<li>".date('d-m-Y',$check_in_time)." has Monthly price of ".$pr_info->monthly." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Monthly Basic) </li>";
                  $total_amount += round($per_day);
               }    

               elseif(!empty($pr_info->weekly))
               {
                  $per_day = ($pr_info->weekly)/7;
                  echo "<li>".date('d-m-Y',$check_in_time)." has Weekly price of ".$pr_info->weekly." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekly Basic) </li>";
                  $total_amount += round($per_day);
               }     

               else
               {
                  $check_in =  date('Y-m-d',$check_in_time);
                  $res_checkin = $this->user_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   {
                     $total_amount += $res_checkin->price;
                     echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$res_checkin->price." in ".$pr_info->currency_type." (Daily Advance) </li>";
                   }
                   else
                   {
                        if(!empty($pr_info->weekendprice))
                        {
                           $day  = date('D',$check_in_time);
                           if($day=="Fri" || $day=="Sat")
                           {
	                           $per_day = ($pr_info->weekendprice)/2;
	                           $total_amount += $per_day;
                               echo "<li>".date('d-m-Y',$check_in_time)." has Weekend price of ".$pr_info->weekendprice." ".$pr_info->currency_type." ; ".round($per_day)." ".$pr_info->currency_type." Per day in (Weekend) </li>";
                           }
                           else
                           {
                              $total_amount +=$pr_info->application_fee;
                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
                           }
                        }
                        else
                        {
                             $total_amount +=$pr_info->application_fee;
                              echo "<li>".date('d-m-Y',$check_in_time)." has daily price of ".$pr_info->application_fee." in ".$pr_info->currency_type." (Daily Basic) </li>";
                        }
                   }
               }                     

                  $check_in_time = $check_in_time+24*60*60;
          }
           
			if($remaining_guest > 0)
			{
              echo "<li> Price increased by ".$additional_guest_amount." in ".$pr_info->currency_type." for ".$remaining_guest." additional guests  (".$additional_guest_amount/$remaining_guest." ".$pr_info->currency_type." per night per guest beyond ".$pr_info->additional_guest." guests)</li>";
			  $total_amount+= $additional_guest_amount;
			}
           echo "<li> Total Amount ".$total_amount." in ".$pr_info->currency_type."</li>";
           exit;
       }
   }

    /*Checking tools of pricing Ends*/

    public function ical($id){
  		$this->db->order_by('start', 'asc');
		$this->db->where('property_id', $id);
		$q = $this->db->get('pr_calender');
		if($q->num_rows() > 0)
			$result = $q->result_array();
		else
			$result = array();

		$r = $this->user_model->get_row('properties', array('id'=>$id));

		$currency_sm = '$';
		$ics_contents  = "BEGIN:VCALENDAR\n";
		$ics_contents .= "VERSION:2.0\n";
		$ics_contents .= "PRODID:PHP\n";
		$ics_contents .= "METHOD:PUBLISH\n";
		$ics_contents .= "X-WR-CALNAME:listing-".$id."\n";

		foreach($result as $schedule_details){
			$iid            = $schedule_details['id'];
			$start_date    = date("Y-m-d", strtotime($schedule_details['start']));
			$end_date      = date("Y-m-d", strtotime($schedule_details['end']));
			$price      	 = $schedule_details['price'];
			if($schedule_details['color'] == '#666')
			  	$status = 'Not Available';
			elseif($schedule_details['color'] == '#a10')
			  	$status = 'Booked';
			else
			  	$status = 'Available';

			$name			 = $r->title;
			$name  		.= "-".($price!='' && $price!='0')?$currency_sm.$price."-".$status:$status;
			
			$description			 = $r->description;
			$location			 = '';
			 
			$estart_date   = str_replace("-", "", $start_date);
			$eend_date     = str_replace("-", "", $end_date);
			 
			$estart_time   = str_replace(":", "", '12:00:00');
			$eend_time     = str_replace(":", "", '23:00:00');
			 
			$name          = str_replace("<br />", "\\n",   $name);
			$name          = str_replace("&amp;", "&",    $name);
			$name          = str_replace("&rarr;", "-->", $name);
			$name          = str_replace("&larr;", "<--", $name);
			$name          = str_replace(",", "\\,",      $name);
			$name          = str_replace(";", "\\;",      $name);

			$location      = str_replace("<br />", "\\n",   $location);
			$location      = str_replace("&amp;", "&",    $location);
			$location      = str_replace("&rarr;", "-->", $location);
			$location      = str_replace("&larr;", "<--", $location);
			$location      = str_replace(",", "\\,",      $location);
			$location      = str_replace(";", "\\;",      $location);

			$description   = str_replace("<br />", "\\n",   $description);
			$description   = str_replace("&amp;", "&",    $description);
			$description   = str_replace("&rarr;", "-->", $description);
			$description   = str_replace("&larr;", "<--", $description);
			$description   = str_replace("<em>", "",      $description);
			$description   = str_replace("</em>", "",     $description);

			# Change TZID if need be
			$ics_contents.="BEGIN:VEVENT\n";
			$ics_contents.= "CLASS:PUBLIC\n";
			$ics_contents.= "DESCRIPTION:test\n";
			$ics_contents.= "DTEND:".$eend_date."T230000Z\n";
			$ics_contents.= "DTSTAMP:" . gmdate('Ymd').'T'. gmdate('His') . "Z\n";
			$ics_contents.= "DTSTART:".$estart_date."T120000Z\n";
			$ics_contents.= "LOCATION:\n";
			$ics_contents.= "PRIORITY:5\n";
			$ics_contents.= "SEQUENCE:0\n";
			$ics_contents.= "SUMMARY;LANGUAGE=en-us:".$name."\n";
			$ics_contents.= "TRANSP:OPAQUE\n";
			$ics_contents.= "UID:" . md5(uniqid(mt_rand(), true)) . "\n";
			$ics_contents.= "X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n";
			$ics_contents.= "X-MICROSOFT-CDO-IMPORTANCE:1\n";
			$ics_contents.= "X-MICROSOFT-DISALLOW-COUNTER:FALSE\n";
			$ics_contents.= "X-MS-OLK-ALLOWEXTERNCHECK:TRUE\n";
			$ics_contents.= "X-MS-OLK-AUTOFILLLOCATION:FALSE\n";
			$ics_contents.= "X-MS-OLK-CONFTYPE:0\n";
			//Here is to set the reminder for the event.
			$ics_contents.= "BEGIN:VALARM\n";
			$ics_contents.= "TRIGGER:-PT1440M\n";
			$ics_contents.= "ACTION:DISPLAY\n";
			$ics_contents.= "DESCRIPTION:Reminder\n";
			$ics_contents.= "END:VALARM\n";
			$ics_contents.= "END:VEVENT\n";
		}

		$ics_contents.= "END:VCALENDAR\n";
		$ics_file   = 'property-'.$id.'.ics';
		header("Content-Type: text/Calendar");
		header("Content-Disposition: inline; filename=".$ics_file);
		echo $ics_contents;
	}

	public function webcam_image(){
		$this->check_login();
        $user = $this->session->userdata('user_info');
        $id = $user['id'];
        $row = $this->user_model->get_row('users', array('id'=>$id));
        if(@$_FILES['webcam']['name'] !="")
		{
		   $image = $this->do_core_upload('webcam','./assets/uploads/profile_image/');
		   $this->user_model->insert('user_pic_gallery',array('image' => $image,'status'=>0,'user_id'=>$id));
		   echo $image;
		}
 	}


    public function sendReviewRating()
    {
        $customer_info = $this->session->userdata('user_info');
        $customer = $customer_info['id'];
        
        $where = array('id' => $this->input->post('space'));
        $booking = $this->user_model->get_row('bookings', $where);
        $pr_id  = $booking->pr_id; 

        $insertdata = array(
                        'review' => $this->input->post('review'),
                        'rating' => $this->input->post('radio_rating'),
                   'property_id' => $pr_id,
                       'user_id' => $customer,
                   'customer_id' => $customer,
                        'status' => 1,
                       'created' => date('Y-m-d h:i:s')
            );

        $this->user_model->insert('review', $insertdata);
        echo 'Review has been done successfully';
    }

    public function sendReviewRating123()
    {
        $customer_info = $this->session->userdata('user_info');
        $customer = $customer_info['id'];
        
        $where = array('id' => $this->input->post('space'));
        $booking = $this->user_model->get_row('bookings', $where);
        $pr_id  = $booking->pr_id; 


        $where = array('id' => $pr_id);
        $property = $this->user_model->get_row('properties', $where);
        $user_id  = $property->user_id; 

        $insertdata = array(
                        'review' => $this->input->post('review'),
                        'rating' => $this->input->post('radio_rating'),
                   'property_id' => $pr_id,
                       'user_id' => $user_id,
                   'customer_id' => $customer,
                        'status' => 1,
                       'created' => date('Y-m-d h:i:s')
            );
        
        $this->user_model->insert('review', $insertdata);
        echo 'Review has been done successfully';
    }


    public function notifications_settings()
    {
        $this->check_login();
		$data['template'] = 'user/account/notifications_settings';
    	$this->load->view('templates/user_template', $data);
    }

    public function submit_notification_setting()
    {    
    	 $update="";
    	 if(isset($_POST['email_upcoming_reservation']))
    	 {
    	 	$update['email_upcoming_reservation'] = 1;
    	 }
    	 else
    	 {
    	 	$update['email_upcoming_reservation'] = 0 ;
    	 }
    	 if(isset($_POST['email_review_received']))
    	 {
    	 	$update['email_review_received'] = 1;
    	 }
    	 else
    	 {
    	 	$update['email_review_received'] = 0;
    	 }
    	 if(isset($_POST['email_reference_received']))
    	 {
    	 	$update['email_reference_received'] = 1;
    	 }
    	 else
    	 {
    	 	$update['email_reference_received'] = 0;
    	 }
    	 if(isset($_POST['email_reference_request_received']))
    	 {
    	 	$update['email_reference_request_received'] = 1;
    	 }
    	 else
    	 {
    	 	$update['email_reference_request_received'] = 0;
    	 }
    	 if(isset($_POST['email_review_reminder']))
    	 {
    	 	$update['email_review_reminder'] = 1;
    	 }
    	 else
    	 {
    	 	$update['email_review_reminder'] = 0;
    	 }
    	 if(isset($_POST['email_calendar_reminder']))
    	 {
    	 	$update['email_calendar_reminder'] = 1;
    	 }
    	 else
    	 {
    	 	$update['email_calendar_reminder'] = 0;
    	 }
    	 if(!empty($update))
    	 {
	    	$user_id =  get_my_id();
	    	$this->user_model->update('users',$update,array('id'=>$user_id));
    	 }
    	
    	echo '<div class="alert alert-success">';

        echo '<button data-dismiss="alert" class="close" type="button">×</button>';

        echo '<strong>Success!</strong> Email Setting Has been updated Successfully.';
    }


    public function update_message_status($message_id="",$alert_type="")
    {
      $this->user_model->update('user_message',array($alert_type=>1),array('id'=>$message_id)) ;     
    }

    public function upload_webcam_image()
    {    
    	$image_url = $_POST['image_temp_url'];
    	if(empty($image_url))
    	{
    		return false;
    	}
    	$image_name = basename($image_url);
		$save_as = "./assets/uploads/profile_image/" . $image_name ;
		
		//Download the image from tempary url.
		$downloaded_image_data = file_get_contents($image_url);
		
		// save the image to disk
		file_put_contents($save_as, $downloaded_image_data);
		$this->user_model->insert('user_pic_gallery',array('image' => $image_name,'status'=>1,'user_id'=>get_my_id()));
		$this->user_model->update('users',array('image' => $image_name),array('id' => get_my_id()));
        echo "ok";
   } 

    public function upload_webcam_video()
    {    
    	$video_url = $_POST['mp_4_'];
    	if(empty($video_url))
    	{
    		return false;
    	}
    	$video_name = basename($video_url);
		$save_as = "./assets/uploads/profile_video/" . $video_name ;
		
		//Download the image from tempary url.
		$downloaded_video_data = file_get_contents($video_url);
		
		// save the image to disk
		if(!empty($downloaded_video_data))
		{
			file_put_contents($save_as, $downloaded_video_data);
			$insert_data = array(
				                 'user_id'=>get_my_id(),
				                 'video'=>$downloaded_video_data,
				                 'created'=>time(),
				                  ); 
			$this->user_model->insert('user_videos',$insert_data);
		 echo "ok";
		}
		else
		{
			return FALSE;
		}
   } 


}