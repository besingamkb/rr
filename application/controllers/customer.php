<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();			
		clear_cache();
		$this->load->model('customer_model');
		 $currency = $this->session->userdata('currency');
	    if(empty($currency))
	    {
	      $this->session->set_userdata('currency','USD');
	    }
	}
	
	public function index($value='')
	{
		$this->check_login();
		redirect('customer/dashboard');
	}
	
	public function check_login()
	{
    	if(!$this->session->userdata('customer_info'))
		{
			redirect('front');
		}
	}

	public function dashboard()
	{
		$this->check_login();
		$data['template'] = 'customer/dashboard';
		$this->load->view('templates/customer_template', $data);
	}

	public function logout()
	{
	 	$this->session->unset_userdata('customer_info');
	 	redirect('front');
	}

	public function profile($value='') { 
	$this->check_login();
	$test_user =$this->session->userdata('customer_info'); 
	
	$data['member'] =$this->customer_model->get_row('users', array('id' => $test_user['id']));
	$mem_id=$data['member']->id; 
	$this->form_validation->set_rules('first_name', 'First name', 'required');
	$this->form_validation->set_rules('last_name', 'Last name', 'required');		
	$this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|callback_check_is_email_unique['.$data['member']->user_email.']');
	$this->form_validation->set_rules('phone', 'Contact', 'required');
	$this->form_validation->set_rules('address', 'Address', 'required');
    $this->form_validation->set_rules('city', 'City', 'required');
    $this->form_validation->set_rules('state', 'State', 'required');
    $this->form_validation->set_rules('zip', 'Zip', 'required');
    $this->form_validation->set_rules('country', 'Country', 'required');
    $this->form_validation->set_error_delimiters('<div style="color:#DE4980;" class="error">', '</div>');
    if ($this->form_validation->run() == TRUE){

			$user=array(
				
				'first_name'	=>	strtolower($this->input->post('first_name')),
				'last_name'		=>	strtolower($this->input->post('last_name')),
				'user_email'	=>	 $this->input->post('user_email'),
				'phone'			=>	$this->input->post('phone'),
				'address'		=>	$this->input->post('address'),	
				'city'			=>	$this->input->post('city'),	
				'state'			=>	$this->input->post('state'),	
				'zip'			=>	$this->input->post('zip'),	
				'country'		=>	$this->input->post('country')
				);
			
			$user_id=$this->customer_model->update('users',$user, array('id' => $mem_id));
			
			if ($user_id) {
				$this->session->set_flashdata('success_msg',"Member has been updaed successfully.");
				redirect('customer/profile');
			}
			else
			{
				$this->session->set_flashdata('error_msg',"Error in updating information.");
				redirect('customer/profile');
			}

		}
		$data['template'] = 'customer/profile';
		$this->load->view('templates/customer_template', $data);
	}

	public function change_password($id=''){ 
		$this->check_login();
		$test_user = $this->session->userdata('customer_info');
		$id = $test_user['id'];  
	    $this->form_validation->set_rules('oldpassword','Old Password','required|callback_compare_oldpwd_enterpwd');
		$this->form_validation->set_rules('newpassword','New Password','required');
		$this->form_validation->set_rules('confirmpassword','Confirm Password','required|matches[newpassword]');
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');   
	    if($this->form_validation->run() == TRUE){   
		      $newpwd=$this->input->post('newpassword');
		      $newpwd1=sha1(trim($newpwd));
		      $this->customer_model->update('users',array('password' => $newpwd1), array('id'=>$id));
		      $this->session->set_flashdata('success_msg','Password Has Been Changed Successfully');
		      redirect('customer/change_password');
	     } 
		$data['template'] = 'customer/change_password1';
		$this->load->view('templates/customer_template', $data);
    }

 	public function  compare_oldpwd_enterpwd($enterpwd){
 		$this->check_login();
		$test_user = $this->session->userdata('customer_info');
		$id = $test_user['id'];
		$enterpwd1=sha1($enterpwd);
		$row = $this->customer_model->get_row('users', array('id'=> $id));
		if($row->password == $enterpwd1){
			return TRUE;
		} 
		else{
			$this->form_validation->set_message('compare_oldpwd_enterpwd','Please Enter Correct Old Password');
			return FALSE;
		}
 	}

 	
 	public function messages($offset=0)
	{
		$this->check_login();
		$customer_info = $this->session->userdata('customer_info');
		$email = $customer_info['user_email'];
		$id = $customer_info['id'];
		$limit=10;
		$data['messages'] = $this->customer_model->get_pagination_or_where('user_message', $limit, $offset, array('user_email' => $email));
		$config= bnb_pagination();
		$config['base_url'] = base_url().'customer/messages/';
		$config['total_rows'] = $this->customer_model->get_pagination_or_where('user_message', 0, 0, array('user_email' => $email));
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'customer/message';
		$this->load->view('templates/customer_template',$data);
	}

 	public function delete_message($message_id='')
 	{
		$this->check_login();
		if(empty($message_id)) redirect('customer/messages');		
		$this->customer_model->delete('user_message',array('id'=>$message_id));
		$this->customer_model->delete('user_message_reply',array('message_id'=>$message_id));
		$this->session->set_flashdata('success_msg',"Message has been deleted successfully.");
		redirect('customer/messages');
	}

 	public function message_view($message_id='')
 	{
		$this->check_login();
 		if(empty($message_id)) redirect('customer/messages');
 		$data['message'] =$this->customer_model->get_row('user_message',array('id'=>$message_id));
	    
	    /////////////////// fetch reply data  ///////////
	    $offset=0;
		$limit=4;
		$data['reply'] = $this->customer_model->get_pagination_where('user_message_reply', $limit, $offset, array( 'message_id' =>$message_id ));
		$data['total_reply'] = $this->customer_model->get_pagination_where('user_message_reply', 0, 0, array( 'message_id' =>$message_id ));
        ////////////////// end of reply  ///////////////

		$data['template'] = 'customer/message_view';
		$this->load->view('templates/customer_template',$data);	 		
 	}

	public function ajax_load_more_reply($message_id='',$offset='')
	{
	  $this->check_login();
	  if(empty($message_id) || empty($offset)) redirect('customer/messages');
	  $limit=4;
	  $data['reply'] = $this->customer_model->get_pagination_where('user_message_reply', $limit, $offset, array( 'message_id' =>$message_id ));
	  $res = $this->load->view('customer/reply_message_template', $data);
	  echo $res;   
	}

    public function reply($message_id='')
    {
		$this->check_login();
 		if(empty($message_id)) redirect('customer/messages');
 		$replier_info   = $this->session->userdata('customer_info');
    	$replier_id     = $replier_info['id'];
		$replier_status = $replier_info['user_role'];
		$replier_email  = $replier_info['user_email'];
		$replier_name   = $replier_info['first_name']." ".$replier_info['last_name'];
        
		$reply = array(
						'reply'          => $this->input->post('reply'),
		                'replier_name'   => $replier_name,
						'replier_email'  => $replier_email,
						'replier_status' => $replier_status,
						'created'        => date('Y-m-d H:i:s'),
						'message_id'     => $message_id
					  );
		$this->customer_model->insert('user_message_reply', $reply);
		redirect('customer/message_view/'.$message_id);
    }


	public function bookings()
 	{
 		$this->check_login();
 		$user = $this->session->userdata('customer_info');
		$id = $user['id'];
		$data['bookings']=$this->customer_model->get_result('bookings', array('customer_id' => $id ) );
		$data['template'] = 'customer/bookings';
		$this->load->view('templates/customer_template',$data);	 		
 	}

	public function delete_booking($bookings_id=''){
		$this->check_login();
		if(empty($bookings_id)) redirect('customer/bookings');		
		$this->customer_model->delete('bookings',array('id'=>$bookings_id));
		$this->session->set_flashdata('success_msg',"Booking has been deleted successfully.");
		redirect('customer/bookings');
	}
	public function check_date_of_checkin($date){
		$this->check_login();
		$error_msg = '';
		if ($this->input->post('check_in') == '') {
			$error_msg.='Please select checkin date. <br>';
		}

		if ($this->input->post('check_out') == '') {
			$error_msg.='Please select checkout date. <br>';
			$this->form_validation->set_message('check_date_of_checkin', $error_msg);
		 	return FALSE;
		}

		$check_in = strtotime($this->input->post('check_in'));
		$check_out = strtotime($this->input->post('check_out'));
		$date1 = strtotime($date[0]);
		$today_date = strtotime(date('d-m-Y'));
		

		if($today_date > $check_out){
			$error_msg.='Invaild checkout date. <br>';
			$this->form_validation->set_message('check_date_of_checkin', $error_msg);
		 	return FALSE;
		}

		if (!empty($error_msg)) {
			$this->form_validation->set_message('check_date_of_checkin', $error_msg);
		 	return FALSE;
		}

		if($check_in > $check_out){
			$this->form_validation->set_message('check_date_of_checkin', 'Please select The vaild checkin ,checkout dates.');
		 	   return FALSE;
		}else{
			return TRUE;
		}
	}

	public function Edit_booking($bookings_id='')
	{	
		$this->check_login();
		if(empty($bookings_id)) redirect('user/bookings');		
		$data['booking']  = $this->customer_model->get_row('bookings',array('id'=>$bookings_id));
		if($_POST){
			$this->form_validation->set_message('numeric', 'Should be valid numeric quantity.');
			$this->form_validation->set_rules('guest_name', 'Guest Name', 'required');
			$this->form_validation->set_rules('email', 'Guest Email', 'required|valid_email');		
			$this->form_validation->set_rules('host_email', 'Host Email', 'required');
			$this->form_validation->set_rules('phone', 'phone', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');	
			$this->form_validation->set_rules('check_in', 'Chech In', 'required|callback_check_date_of_checkin');
	        $this->form_validation->set_rules('check_out', 'Chech out', 'required|callback_check_date_of_checkin');
	        if(isset($_POST['property_id'])){
	        	$this->form_validation->set_rules('property_id', 'Properties Title', 'required');
			}

			$this->form_validation->set_rules('due_amount', 'Due Amount', 'required|numeric');
			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');
			if($this->form_validation->run()==TRUE){
		
				$data=array(
							'guest_name' 	=>$this->input->post('guest_name'),
							'email'      	=>$this->input->post('email'),				
							'contact'    	=>$this->input->post('phone'),				
							'address'    	=>$this->input->post('address'),
							'check_in'   	=>date('Y-m-d',strtotime($this->input->post('check_in'))),									
							'check_out'  	=>date('Y-m-d',strtotime($this->input->post('check_out'))),													
							'due_amount' 	=>$this->input->post('due_amount'),								
							'modified'    	=>date('Y-m-d H:i:s')		
						   );
				if(isset($_POST['property_id'])){
	 				
	 				$data['pr_id'] = $this->input->post('property_id');
	 				

				}
				$this->customer_model->update('bookings',$data,array('id' =>$bookings_id));		
				$this->session->set_flashdata('success_msg',"Successfully updated Booking. ");
				redirect('customer/bookings');
			}
		}
		$data['template'] = 'customer/edit_booking';
		$this->load->view('templates/customer_template',$data);	 	
	}

public function review($offset=0){
		$this->check_login();
        $limit=10;
        $data['review']=$this->customer_model->customer_review($limit, $offset);
        $config = bnb_pagination();
        $config['base_url'] = base_url().'customer/review/';
        $config['total_rows'] = $this->customer_model->customer_review(0,0);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;        
        $this->pagination->initialize($config);         
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'customer/review';
        $this->load->view('templates/customer_template', $data);
    }

    public function review_full_view($user_id="",$property_id="",$review_id=""){
		$this->check_login();
	    if($user_id == ""){
	        redirect('customer/review');
	    }

		$data['customer_info'] = $this->customer_model->get_row('users', array('id'=>$user_id));
		$data['property_info'] = $this->customer_model->get_row('properties', array('id'=>$property_id));
		$data['review_info'] = $this->customer_model->get_row('review', array('id'=>$review_id));
		$data['template'] = 'customer/review_full_view';
		$this->load->view('templates/customer_template', $data);
	}

	public function edit_review($user_id="",$property_id="",$review_id=""){
        $this->form_validation->set_rules('review', 'Review', 'required');
	        if($this->form_validation->run()==TRUE){
		        $review = $this->input->post('review');
		        $this->customer_model->update('review',array('review'=>$review),array('id'=>$review_id));
		        $this->session->set_flashdata('success_msg','Review Has been updated successfully');
		        redirect('customer/review');
	        }
		$data['customer_info'] = $this->customer_model->get_row('users', array('id'=>$user_id));
		$data['property_info'] = $this->customer_model->get_row('properties', array('id'=>$property_id));
		$data['review_info'] = $this->customer_model->get_row('review', array('id'=>$review_id));
		$data['template'] = 'customer/edit_review';
		$this->load->view('templates/customer_template', $data);
	}
 
 
 	public function review_delete($id=""){
        $this->check_login();
        if($id == ""){
            redirect('customer/review');
        }
        $this->customer_model->delete('review', array('id'=>$id));
        $this->session->set_flashdata('success_msg',"Review has been Deleted successfully.");
        redirect('customer/review');
    }

    public function change_profile_image()
 	{
 		$this->check_login();
        $user = $this->session->userdata('customer_info');
        $id = $user['id'];
        $row = $this->customer_model->get_row('users', array('id'=>$id));
        @$old_image = $row->image;
		if(@$_FILES['userfile']['name'] !="")
		{
		   $image = $this->do_core_upload('userfile','./assets/uploads/profile_image/');
		   $this->customer_model->update('users',array('image' => $image),array('id' => $id));
		   if(!empty($old_image))
		   {
		    $path = './assets/uploads/profile_image/';
		    @unlink($path.$old_image);
		   }
		   $this->session->set_flashdata('success_msg',"Image has been Changed successfully.");
		   redirect('customer/change_profile_image');
		}    
        $data['user'] = $this->customer_model->get_row('users', array('id'=>$id));
        $data['template'] = 'customer/change_profile_pic';
        $this->load->view('templates/customer_template', $data);
 	}


	public function do_core_upload($filename2='user_file' , $upload_path='./assets/uploads/custom_uploads/', $path_of_thumb='')
	{
		$this->check_login();
		$allowed =  array('gif','png','jpg','jpeg');
		$filename = $_FILES[$filename2]['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ){
			return FALSE;
		}
		else{
			
			if ($_FILES[$filename2]["error"] > 0){
	 			return FALSE; 
	 		}
			else{
			 $name = uniqid();
			 if(move_uploaded_file($_FILES[$filename2]['tmp_name'],$upload_path.$name.'.'.$ext))
			 return $name.'.'.$ext;
			 else
			 return FALSE;
			}
		} 
	}
	
       public function favorites_property($offset=0){
        $this->check_login();
        $limit=10;
        $data['favorites_property']=$this->customer_model->customer_favorites_property($limit, $offset);
        $config = bnb_pagination();
        $config['base_url'] = base_url().'customer/favorites_property/';
        $config['total_rows'] = $this->customer_model->customer_favorites_property(0,0);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;        
        $this->pagination->initialize($config);         
        $data['pagination'] = $this->pagination->create_links();
        $data['template'] = 'customer/favorites_property';
        $this->load->view('templates/customer_template', $data);
    }

    public function view_favorites_property($user_id="",$property_id=""){
        $this->check_login();
        if($user_id == ""){
            redirect('customer/favorites_property');
        }

        $data['customer_info'] = $this->customer_model->get_row('users', array('id'=>$user_id));
        $data['property_info'] = $this->customer_model->get_row('properties', array('id'=>$property_id));
        $data['template'] = 'customer/view_favorites_property';
        $this->load->view('templates/customer_template', $data);
    } 

    public function delete_favorites_property($id=""){
        $this->check_login();
        if($id == ""){
            redirect('customer/favorites_property');
        }
        $this->customer_model->delete('favorites_property', array('id'=>$id));
        $this->session->set_flashdata('success_msg',"Favorites has been Deleted successfully.");
        redirect('customer/favorites_property');
    }

    /*Groups Task 18-feb Starts*/
    /*Groups Starts*/
	public function groups($offset=0){
		$this->check_login();
		$limit=10;
		$data['groups']=$this->customer_model->get_pagination_result('groups',$limit, $offset);
		$config= bnb_pagination();	
		$config['base_url'] = base_url().'customer/groups/';
		$config['total_rows'] = $this->customer_model->get_pagination_result('groups',0,0);
		$config['per_page'] = $limit;
		$config['num_links'] =3;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();
		$data['total_row'] = $config['total_rows'];
		$data['template'] = 'customer/groups';
        $this->load->view('templates/customer_template', $data);	
	}

	public function join_group($group_id="",$flag=""){
		$this->check_login();
	    $customer_info = $this->session->userdata('customer_info');
	    if($flag=="leave"){
     	   $this->customer_model->delete('group_members',array('member_id'=>$customer_info['id'],'group_id'=>$group_id));
     	   $this->session->set_flashdata('success_msg','You have successfully Leave this Group.');
	       redirect('customer/groups');
	    }
	    else{
		    $data = array(
		       	     'member_id'=>$customer_info['id'],
		       	     'member_role'=>$customer_info['user_role'],
		       	     'group_id' => $group_id,
		       	     'created'=>date('Y-m-d h:i:s'),
		       	     );
		   $this->customer_model->insert('group_members',$data);
     	   $this->session->set_flashdata('success_msg','You have successfully Join this Group.');
	       redirect('customer/groups');
	    }
    }

    /*Groups Endss*/
    /*View Members To the Group Starts*/

    public function view_group_members($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('customer/groups');
			}
	    $data['group_id'] = $group_id;
		$data['template'] = 'customer/group_members';
	    $this->load->view('templates/customer_template', $data);		
    }

    public function customer_group_members($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('customer/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $data['members'] = $this->customer_model->get_customer_members($group_id,$limit,$offset);
	    $config = bnb_pagination();
		$config['base_url'] = base_url().'customer/customer_group_members/'.$group_id;
		$config['total_rows'] = $this->customer_model->get_customer_members($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'customer/customer_group_members';
	    $this->load->view('templates/customer_template', $data);		
    }

	public function owner_group_members($group_id="",$offset=0){
        $this->check_login();
			if($group_id==""){
				redirect('customer/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $data['members'] = $this->customer_model->get_owner_members($group_id,$limit,$offset);
	    $config= bnb_pagination();
		$config['base_url'] = base_url().'customer/owner_group_members/'.$group_id;
		$config['total_rows'] = $this->customer_model->get_owner_members($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'customer/owner_group_members';
	    $this->load->view('templates/customer_template', $data);		
    }

    /*View Members To the Group Ends*/

    /*View properties of the Group Starts*/

    public function view_group_properties($group_id="",$offset=0){
		$this->check_login();
			if($group_id==""){
				redirect('customer/groups');
			}
	    $limit = 10;
	    $data['group_id'] = $group_id;
	    $data['members_properties'] = $this->customer_model->group_members_properties($group_id,$limit,$offset);
	    $config= bnb_pagination();
		$config['base_url'] = base_url().'customer/view_group_properties/'.$group_id;
		$config['total_rows'] = $this->customer_model->group_members_properties($group_id,0,0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();	
		$data['template'] = 'customer/view_group_properties';
	    $this->load->view('templates/customer_template', $data);		
    }

    /*View properties of the Group Ends*/

    /*Groups Task 18feb Ends*/

}