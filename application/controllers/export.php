<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('export_model');
	}

	private function export_download($query='',$file_name=''){
        $this->load->helper('csv');
        array_to_csv($query, $file_name);
    }

	public function export_properties(){
		  if($_POST){
            // print_r($_POST);
            // die();
            $type = $this->input->post('type');
            $export_file_format=$this->input->post('export_file_format');
            $main_date=$this->input->post('date_range1');
            $export_sort = $this->input->post('export_sort');

            if(empty($main_date)){
                $main_date = "";    
                $from_date = "";
                $to_date = "";
            }
            else{   
                $aar =explode( '-', $main_date );
                $from_date = date('Y-m-d',strtotime($aar[0]));
                $to_date = date('Y-m-d',strtotime('+1 day', strtotime($aar[1])));
            }

            
            if(strtolower($export_file_format)=='csv'){
                $array=$this->export_csv($from_date, $to_date, $type,$export_sort);
                $this->export_download($array,'properties'.date('Y-m-d').'.csv');
            }elseif(strtolower($export_file_format)=='excel'){
                $this->export_excel($from_date, $to_date, $type,$export_sort);
            }else{
              $data_arr['properties']=$this->export_pdf($from_date, $to_date, $type,$export_sort);              
                $data_arr['from_date']=$from_date;
                $data_arr['to_date']=$to_date;
                $data_arr['export_pdf'] = $export_sort;
                $file = $this->load->view('export/export_orders_pdf', $data_arr, TRUE);       
                $this->load->library('mpdf');        
                $this->mpdf->WriteHTML($file);
                $this->mpdf->Output('properties'.date('Y-m-d').'.pdf','D');
            }
        }       
  }

  private function export_excel($from_date='', $to_date='', $type='',$export_sort=""){
        $data=$this->export_model->export_properties($from_date, $to_date, $type,$export_sort);      
        // print_r($data); die();
         // $array=array();
         // $array[]=array('Order Id','Date','Status');              
        $orders ="";
        $header = "\n#\t\ttitle\t\t\t\t\tdescription\n";
        $i=1;
        if($data):
        foreach ($data as $row) {
           $title =$row->title; 
           $order_id = $i;
           $description =$row->description;                                   
           $created=date('Y-m-d',strtotime($row->created));
           // $line1 = "Order Id";
           $line2 = "#$order_id\t$title\t\t\t\t\t$description\t\t\t\t$created";

           // $line3 = "\n Date \n";
           // $line4 = "\n$created\n";

           // $line5 = "\nOrder Status\n";
           // $line6 = "\n$order_status\n";

           $orders .= "$line2\n";
           $i++;
        }

        // print_r($orders);
        $file_name = "properties".date('m/d/Y');
        $this->load->helper('excel_download_helper');
          order_download($file_name,$header,$orders);
        endif;      
       // return $array;
    }

   private function export_csv($from_date='', $to_date='', $type='',$export_sort=""){
        $data=$this->export_model->export_properties($from_date, $to_date, $type,$export_sort);      
         $array=array();
         $array[]=array('Title','Date','Description');              
        if($data):
        foreach ($data as $row) {
            $created =date('Y-m-d',strtotime($row->created));
            $title = $row->title;
            $description =$row->description;                       
            $array[]=array(
                "#".$title,
                $created,
                $description
            );         
        }
        endif;      
       return $array;
    }

    private function export_pdf($from_date='',$to_date='', $type='',$export_sort=""){


         $data=$this->export_model->export_properties($from_date,$to_date, $type,$export_sort);              
        if($data):
        foreach ($data as $row) {
            //$created =$row->created; 
            $created =date('Y-m-d',strtotime($row->created));
            $title = $row->title;
            $description =$row->description;
            $image=$row->image;    
            $array[]= (object) array(
                'title' => "#".$title,
                'created' => $created,
                'description'=> $description,
                'image'=>$image
                
            );
          
        }      
            return  $array;
        else:
            return  FALSE;
        endif;       
    } 
    public function export_bookings(){
          if($_POST){
            $type = $this->input->post('type');
            $export_file_format=$this->input->post('export_file_format');
            $export_sort = $this->input->post('export_sort');
            $main_date=$this->input->post('report_range2');


            if(empty($main_date)){
                $main_date = "";    
                $from_date = "";
                $to_date = "";
            }
            else{   
                $aar =explode( '-', $main_date );
                $from_date = date('Y-m-d',strtotime($aar[0]));
                $to_date = date('Y-m-d',strtotime('+1 day', strtotime($aar[1])));
            }

            if(strtolower($export_file_format)=='csv'){
                $array=$this->export_bookings_csv($from_date, $to_date, $type,$export_sort);
                $this->export_download($array,'Bookings'.date('Y-m-d').'.csv');
            }elseif(strtolower($export_file_format)=='excel'){
                $this->export_bookings_excel($from_date, $to_date, $type,$export_sort);
            }else{
                $data_arr['bookings']=$this->export_bookings_pdf($from_date, $to_date, $type,$export_sort);             
                $data_arr['from_date']=$from_date;
                $data_arr['to_date']=$to_date;
                $data_arr['export_sort']=$export_sort;
                $file = $this->load->view('export/export_Bookings_pdf', $data_arr, TRUE);       
                $this->load->library('mpdf');        
                $this->mpdf->WriteHTML($file);
                $this->mpdf->Output('bookings'.date('Y-m-d').'.pdf','D');
            }
        }       
    }
    private function export_bookings_excel($from_date='', $to_date='', $type='',$export_sort=""){
        $data=$this->export_model->export_bookings($from_date, $to_date, $type,$export_sort);      
        // print_r($data); die();
         // $array=array();
         // $array[]=array('Order Id','Date','Status');              
        $orders ="";
        $header = "\n#\t\tNumber\t\temail\t\tAddress\t\tContact\t\tcreated\n";
        $i=1;
        if($data):
        foreach ($data as $row) {
           $name =$row->booking_number; 
           $address =$row->address; 
           $email =$row->email; 
           $contact =$row->contact; 
           $order_id = $i;
                                       
           $created=date('Y-m-d',strtotime($row->created));
           // $line1 = "Order Id";
           $line2 = "#$order_id\t\t$name\t\t$email\t\t$address\t\t$contact\t\t$created";

           // $line3 = "\n Date \n";
           // $line4 = "\n$created\n";

           // $line5 = "\nOrder Status\n";
           // $line6 = "\n$order_status\n";

           $orders .= "$line2\n";
           $i++;
        }

        // print_r($orders);
        $file_name = "bookings".date('m/d/Y');
        $this->load->helper('excel_download_helper');
            order_download($file_name,$header,$orders);
        endif;      
       // return $array;
    }

     private function export_bookings_csv($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_bookings($from_date, $to_date, $type,$export_sort);      
         $array=array();
         $array[]=array('Booking number','Address','Email','Contact','Created');              
        if($data):
        foreach ($data as $row) {
             $name =$row->booking_number; 
               $address =$row->address; 
               $email =$row->email; 
               $contact =$row->contact; 
       
                                       
           $created=date('Y-m-d',strtotime($row->created));                     
            $array[]=array(
                "#".$name,
                $address,
                $email,
                $contact,
                $created
            );         
        }
        endif;      
       return $array;
    }

    private function export_bookings_pdf($from_date='', $to_date='', $type='',$export_sort=''){
         $data=$this->export_model->export_bookings($from_date, $to_date, $type,$export_sort);              
        
        if($data):
        foreach ($data as $row) {
            //$created =$row->created; 
            $created =date('Y-m-d',strtotime($row->created));
              $name =$row->booking_number; 
               $address =$row->address; 
               $email =$row->email; 
               $contact =$row->contact;    
             $array[]= (object) array(
                'booking_number' => "#".$name,
                'address' => $address,
                'email'=> $email,
                'contact'=>$contact,
                'created'=>$created

                
            );
          
        }      
            return  $array;
        else:
            return  FALSE;
        endif;       
    }      

	public function export_members(){
          if($_POST){
            // print_r($_POST);
            // die();
            $type = $this->input->post('type');
            $export_file_format=$this->input->post('export_file_format');
            $main_date=$this->input->post('report_range2');
            $export_sort = $this->input->post('export_sort');


            if(empty($main_date)){
                $main_date = "";    
                $from_date = "";
                $to_date = "";
            }
            else{   
                $aar =explode( '-', $main_date );
                $from_date = date('Y-m-d',strtotime($aar[0]));
                $to_date = date('Y-m-d',strtotime('+1 day', strtotime($aar[1])));
            }
            if(strtolower($export_file_format)=='csv'){
                $array=$this->export_members_csv($from_date, $to_date, $type,$export_sort);
                $this->export_download($array,'members'.date('Y-m-d').'.csv');
            }elseif(strtolower($export_file_format)=='excel'){
                $this->export_members_excel($from_date, $to_date, $type,$export_sort);
            }else{
                $data_arr['members']=$this->export_members_pdf($from_date, $to_date, $type,$export_sort);             
                $data_arr['from_date']=$from_date;
                $data_arr['to_date']=$to_date;
                $data_arr['export_sort'] = $export_sort;
                $file = $this->load->view('export/export_members_pdf', $data_arr, TRUE);       
                $this->load->library('mpdf');        
                $this->mpdf->WriteHTML($file);
                $this->mpdf->Output('members'.date('Y-m-d').'.pdf','D');
            }
        }       
    }
    private function export_members_excel($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_members($from_date, $to_date, $type,$export_sort);      
        // print_r($data); die();
         // $array=array();
         // $array[]=array('Order Id','Date','Status');              
        $orders ="";
        $header = "\n#\tName\temail\tAddress\tCity\tState\tCountry\tContact\tStatus\tcreated\t\n";
        $i=1;
        if($data):
        foreach ($data as $row) {
            $name =$row->first_name.' '.$row->last_name; 
            $address =$row->address; 
            $email =$row->user_email; 
            $city =$row->city; 
            $state =$row->state;
             $contact= $row->phone;
            $country =$row->country; 
            if($row->user_status==0){
                $status ='Banned';
            } 
            else
            {
                $status ='Active';  
            }
            $created =date('Y-m-d',strtotime($row->created));
            $order_id=$i; 
           // $line1 = "Order Id";
           $line2 = "#$order_id\t $name\t$email\t$address\t$city\t$state\t$country\t $contact\t$status\t$created";

           // $line3 = "\n Date \n";
           // $line4 = "\n$created\n";

           // $line5 = "\nOrder Status\n";
           // $line6 = "\n$order_status\n";

           $orders .= "$line2\n";
           $i++;
        }

        // print_r($orders);
        $file_name = "members".date('m/d/Y');
        $this->load->helper('excel_download_helper');
            order_download($file_name,$header,$orders);
        endif;      
       // return $array;
    }

     private function export_members_csv($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_members($from_date, $to_date, $type,$export_sort);      
         $array=array();
         $array[]=array('Name','Address','City','State','Country','Email','Contact','Status','Created');              
        if($data):
        foreach ($data as $row) {

            $name =$row->first_name.' '.$row->last_name; 
            $address =$row->address; 
            $email =$row->user_email; 
            $city =$row->city; 
            $state =$row->state;
             $contact= $row->phone;
            $country =$row->country; 
            if($row->user_status==0){
                $status ='Banned';
            } 
            else
            {
                $status ='Active';  
            }
            $created =date('Y-m-d',strtotime($row->created));                     
            
            $array[]=array(
                "#".$name,
                $address,
                $city,
                $state,
                $country,
                $email,
                $contact,
                $status,
                $created
            );         
        }
        endif;      
       return $array;
    }

    private function export_members_pdf($from_date='', $to_date='', $type='',$export_sort=''){
         $data=$this->export_model->export_members($from_date, $to_date, $type,$export_sort);              
        if($data):
        foreach ($data as $row) {
            //$created =$row->created; 
            $created =date('Y-m-d',strtotime($row->created));
            $name =$row->first_name.' '.$row->last_name; 
            $address =$row->address; 
            $email =$row->user_email; 
            $city =$row->city; 
            $state =$row->state;
             $contact= $row->phone;
            $country =$row->country; 
            if($row->user_status==0){
                $status ='Banned';
            } 
            else
            {
                $status ='Active';  
            }
            $created =date('Y-m-d',strtotime($row->created));      
            $array[]= (object) array(
                'name' => "#".$name,
                'address' => $address,
                'email'=> $email,
                'city'=> $city,
                'state'=> $state,
                'country'=> $country,
                'contact'=>$contact,
                'status'=>$status,
                'created'=>$created

                
            );
          
        }      
            return  $array;
        else:
            return  FALSE;
        endif;       
    }     

  /*Export payments Starts*/

  public function export_payments(){
          if($_POST){
            $type = $this->input->post('type');
            $export_file_format=$this->input->post('export_file_format');
            $main_date=$this->input->post('report_range2');
            $export_sort = $this->input->post('export_sort');

            if(empty($main_date)){
                $main_date = "";    
                $from_date = "";
                $to_date = "";
            }

            else{   
                $aar =explode( '-', $main_date );
                $from_date = date('Y-m-d',strtotime($aar[0]));
                $to_date = date('Y-m-d',strtotime('+1 day', strtotime($aar[1])));
            }

            if(strtolower($export_file_format)=='csv'){
                $array = $this->export_payments_csv($from_date, $to_date, $type,$export_sort);
                $this->export_download($array,'payments'.date('Y-m-d').'.csv');
            }
            elseif(strtolower($export_file_format)=='excel'){
                $this->export_payments_excel($from_date, $to_date, $type,$export_sort);
            }
            else{
                $data_arr['payments']=$this->export_payments_pdf($from_date, $to_date, $type,$export_sort);             
                $data_arr['from_date']=$from_date;
                $data_arr['to_date']=$to_date;
                $data_arr['export_sort'] = $export_sort;
                $file = $this->load->view('export/export_payment', $data_arr, TRUE);       
                $this->load->library('mpdf');        
                $this->mpdf->WriteHTML($file);
                $this->mpdf->Output('payments'.date('Y-m-d').'.pdf','D');
            }
        }       
    }


    private function export_payments_excel($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_payments($from_date, $to_date, $type,$export_sort);      
        $orders ="";
        $header = "\n#\tGuest\tHost\tProperty\tBooking\tPayment Status\tPayment Type\tCreated\n";
        $i=1;
        if($data):
        foreach ($data as $row) {
            $guest    = get_user_info($row->user_id);
            $host     = get_user_info($row->owner_id);
            $property = get_property_detail($row->property_id);

            $guest_name      = $guest->first_name.' '.$guest->last_name; 
            $host_name       = $host->first_name.' '.$host->last_name;
            $property_title  = $property->title; 
            $booking_date    = date('Y-m-d',strtotime($row->created)); 
            $payment_type    = $row->payment_type; 
            $payments_status = json_decode($row->transaction_details)->payment_status;
            $created         = date('Y-m-d',strtotime($row->created));
            $order_id=$i; 
           // $line1 = "Order Id";
           $line2 = "#$order_id\t $guest_name\t$host_name\t$property_title\t$booking_date\t$payment_type\t$payments_status\t$created";

           // $line3 = "\n Date \n";
           // $line4 = "\n$created\n";

           // $line5 = "\nOrder Status\n";
           // $line6 = "\n$order_status\n";

           $orders .= "$line2\n";
           $i++;
        }

        // print_r($orders);
        $file_name = "Payments".date('m/d/Y');
        $this->load->helper('excel_download_helper');
            order_download($file_name,$header,$orders);
        endif;      
       // return $array;
    }

     private function export_payments_csv($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_payments($from_date, $to_date, $type,$export_sort);      
        if($data):
        foreach ($data as $row) {
            $guest    = get_user_info($row->user_id);
            $host     = get_user_info($row->owner_id);
            $property = get_property_detail($row->property_id);

            $guest_name      = $guest->first_name.' '.$guest->last_name; 
            $host_name       = $host->first_name.' '.$host->last_name;
            $property_title  = $property->title; 
            $booking_date    = $row->created; 
            $payment_type    = $row->payment_type; 
            $payments_status = json_decode($row->transaction_details)->payment_status;
            $created         = date('Y-m-d',strtotime($row->created));
            $array[]= array(
                'guest_name'     => $guest_name,
                'host_name'      => $host_name,
                'property_title' => $property_title,
                'booking_date'   => $booking_date,
                'payment_type'   => $payment_type,
                'created'        =>$created,
                'payments_status'=>$payments_status,
               );
          
        }      
           return $array;
       else:
            return  FALSE;
        endif;       

    }

    private function export_payments_pdf($from_date='', $to_date='', $type='',$export_sort=''){
         $data=$this->export_model->export_payments($from_date, $to_date, $type,$export_sort);              
        if($data):
        foreach ($data as $row) {

            //$created =$row->created; 
            $guest = get_user_info($row->user_id);
            $host  = get_user_info($row->owner_id);
            $property = get_property_detail($row->property_id);

            $guest_name      = $guest->first_name.' '.$guest->last_name; 
            $host_name       = $host->first_name.' '.$host->last_name;
            $property_title  = $property->title; 
            $booking_date    = $row->created; 
            $payment_type    = $row->payment_type; 
            $payments_status = json_decode($row->transaction_details)->payment_status;
            $created         = date('Y-m-d',strtotime($row->created));
            $array[]= (object) array(
                'guest_name'     => $guest_name,
                'host_name'      => $host_name,
                'property_title' => $property_title,
                'booking_date'   => $booking_date,
                'payment_type'   => $payment_type,
                'created'        => $created,
                'payments_status'=> $payments_status,
               );
          
        }      
           return $array;
       else:
            return  FALSE;
        endif;       
    }      


    /*Export payments Ends*/


    /*Export Events Starts*/
  public function export_events(){
          if($_POST){
            $type = $this->input->post('type');
            $export_file_format=$this->input->post('export_file_format');
            $main_date=$this->input->post('report_range2');
            $export_sort = $this->input->post('export_sort');

            if(empty($main_date)){
                $main_date = "";    
                $from_date = "";
                $to_date = "";
            }

            else{   
                $aar =explode( '-', $main_date );
                $from_date = date('Y-m-d',strtotime($aar[0]));
                $to_date = date('Y-m-d',strtotime('+1 day', strtotime($aar[1])));
            }

            if(strtolower($export_file_format)=='csv'){
                $array = $this->export_events_csv($from_date, $to_date, $type,$export_sort);
                $this->export_download($array,'Export_Events'.date('Y-m-d').'.csv');
            }
            elseif(strtolower($export_file_format)=='excel'){
                $this->export_events_excel($from_date, $to_date, $type,$export_sort);
            }
            else{
                $data_arr['events']=$this->export_events_pdf($from_date, $to_date, $type,$export_sort);             
                $file = $this->load->view('export/export_events', $data_arr, TRUE);       
                $this->load->library('mpdf');        
                $this->mpdf->WriteHTML($file);
                $this->mpdf->Output('export_events'.date('Y-m-d').'.pdf','D');
            }
        }       
    }

    private function export_events_excel($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_events($from_date, $to_date, $type,$export_sort);      
        $orders ="";
        $header = "\n#\tEvent\tStart Date\tEnd Date\tStart Time\tEnd Time\tCreated\n";
        $i=1;
        if($data):
        foreach ($data as $row) {

                       $event = $row->title;
                       $startDate =  date('d-m-Y',strtotime($row->startDate));
                       $endDate =   date('d-m-Y',strtotime($row->endDate));
                       $startTime = $row->startTime;
                       $endTime = $row->endTime;
                       $created =  date('d-m-Y',strtotime($row->created));
            $order_id=$i; 
           $line2 = "#$order_id\t $event\t$startDate\t$endDate\t$startTime\t$endTime\t$created";
           $orders .= "$line2\n";
           $i++;
        }

        $file_name = "Export_Events".date('m/d/Y');
        $this->load->helper('excel_download_helper');
            order_download($file_name,$header,$orders);
        else:
        $file_name = "Export_Events".date('m/d/Y');
        $this->load->helper('excel_download_helper');
        $orders .= "\tNo Records Found\n";
            order_download($file_name,$header,$orders);
        endif;      
    }

    private function export_events_csv($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_events($from_date, $to_date, $type,$export_sort);      
        if($data):
        foreach ($data as $row) {
                     
                       $event = $row->title;
                       $startDate =  date('d-m-Y',strtotime($row->startDate));
                       $endDate =   date('d-m-Y',strtotime($row->endDate));
                       $startTime = $row->startTime;
                       $endTime = $row->endTime;
                       $created =  date('d-m-Y',strtotime($row->created));

            $array[]= array(
                            'guest_name'     => $event,
                            'host '          => $startDate,
                            'credit_earned'  => $endDate,
                            'credit_paid'    => $startTime,
                            'credit_to_pay'  => $endTime,
                            'credit_to_pay'  => $created,
                          );
        }      
           return $array;
       else:
        $array[] = array('Status'=>'No Records Found');
            return  $array;
        endif;       

    }

    private function export_events_pdf($from_date='', $to_date='', $type='',$export_sort=''){
         $data=$this->export_model->export_events($from_date, $to_date, $type,$export_sort);              
         return $data;
    }    

    public function export_booking_detail($booking_id="")
    {
        $data['booking'] = $this->export_model->get_row('bookings',array('id'=>$booking_id));
        $page = $this->load->view('export/export_booking_detail',$data,TRUE);
        $this->load->library('mpdf');        
        $this->mpdf->WriteHTML($page);
        $this->mpdf->Output('Booking_detail '.date('Y-m-d').'.pdf','D');
    }  

    public function export_booking_receipt($booking_id="")
    {
        $data['booking'] = $this->export_model->get_row('bookings',array('id'=>$booking_id));
        $page = $this->load->view('export/export_booking_receipt',$data,TRUE);
      // echo $page;die();
        $this->load->library('mpdf');        
        $this->mpdf->WriteHTML($page);
        $this->mpdf->Output('Booking_receipt '.date('Y-m-d').'.pdf','D');
    }  

    /*Export Events Ends*/



    /*Export Events Starts*/
  public function export_security_deposit(){
          if($_POST){
            $type = $this->input->post('type');
            $export_file_format=$this->input->post('export_file_format');
            $main_date=$this->input->post('report_range2');
            $export_sort = $this->input->post('export_sort');

            if(empty($main_date)){
                $main_date = "";    
                $from_date = "";
                $to_date = "";
            }

            else{   
                $aar =explode( '-', $main_date );
                $from_date = date('Y-m-d',strtotime($aar[0]));
                $to_date = date('Y-m-d',strtotime('+1 day', strtotime($aar[1])));
            }

            if(strtolower($export_file_format)=='csv'){
                $array = $this->export_security_deposit_csv($from_date, $to_date, $type,$export_sort);
                $this->export_download($array,'Export_Events'.date('Y-m-d').'.csv');
            }
            elseif(strtolower($export_file_format)=='excel'){
                $this->export_security_deposit_excel($from_date, $to_date, $type,$export_sort);
            }
            else{
                $data_arr['security_deposit']=$this->export_security_deposit_pdf($from_date, $to_date, $type,$export_sort);             
                $file = $this->load->view('export/export_security_deposit', $data_arr, TRUE);       
                $this->load->library('mpdf');        
                $this->mpdf->WriteHTML($file);
                $this->mpdf->Output('Export_security_deposit'.date('Y-m-d').'.pdf','D');
            }
        }       
    }

    private function export_security_deposit_excel($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_security_deposit($from_date, $to_date, $type,$export_sort);      
        $orders ="";
        $header = "\nBooking  ID\tHost \tGuest\tListing\tDeposit\tBooking Date\n";
        $i=1;
        if($data):
        foreach ($data as $row) {

             $booking_id = $row->booking_id;
             $owner =  $row->host_name;
             $guest =  $row->guest_name;
             $pr_detail = property_details($row->property_id);
             $property_name = word_limiter($pr_detail->title,3);
             $deposit = $row->security_deposit;
             $created =  date('d-m-Y',strtotime($row->created));

            $order_id=$i; 
           $line2 = "#$booking_id\t$owner\t$guest\t$property_name\t$deposit\t$created";
           $orders .= "$line2\n";
           $i++;
        }

        $file_name = "Export_Events".date('m/d/Y');
        $this->load->helper('excel_download_helper');
            order_download($file_name,$header,$orders);
        else:
        $file_name = "Export_Events".date('m/d/Y');
        $this->load->helper('excel_download_helper');
        $orders .= "\tNo Records Found\n";
            order_download($file_name,$header,$orders);
        endif;      
    }

    private function export_security_deposit_csv($from_date='', $to_date='', $type='',$export_sort=''){
        $data=$this->export_model->export_security_deposit($from_date, $to_date, $type,$export_sort);      
        if($data):
        foreach ($data as $row) {
                     
             $booking_id = $row->booking_id;
             $owner =  $row->host_name;
             $guest =  $row->guest_name;
             $pr_detail = property_details($row->property_id);
             $property_name = word_limiter($pr_detail->title,3);
             $deposit = $row->security_deposit;
             $created =  date('d-m-Y',strtotime($row->created));

            $array[]= array(
                            'guest_name'     => $booking_id,
                            'host '          => $owner,
                            'credit_earned'  => $guest,
                            'credit_paid'    => $property_name,
                            'credit_to_pay'  => $deposit,
                            'credit_to_pay'  => $created,
                          );
        }      
           return $array;
       else:
        $array[] = array('Status'=>'No Records Found');
            return  $array;
        endif;       

    }

    private function export_security_deposit_pdf($from_date='', $to_date='', $type='',$export_sort=''){
         $data=$this->export_model->export_security_deposit($from_date, $to_date, $type,$export_sort);              
         return $data;
    }    


}/* End of file export.php */
