<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_handler extends CI_Controller {

	public function __construct(){
		parent::__construct();		
		//$this->load->helper('url');
	}	

	public function property(){
		error_reporting(E_ALL | E_STRICT);
		$data=array(
			'script_url' => base_url().'upload_handler/property',
            'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/assets/blueimp/property/',
            'upload_url' => $this->get_full_url().'/assets/blueimp/property/');
		$this->load->library('UploadHandler',$data);		
	}

	public function footprint(){
		error_reporting(E_ALL | E_STRICT);
		$data2=array(
			'script_url' => base_url().'upload_handler/footprint',
            'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/assets/blueimp/footprint/',
            'upload_url' => $this->get_full_url().'/assets/blueimp/footprint/');
		$this->load->library('UploadHandler',$data2);		
	}

	protected function get_server_var($id) {
        return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
    }

     protected function get_full_url() {
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0;
        return
            ($https ? 'https://' : 'http://').
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
            ($https && $_SERVER['SERVER_PORT'] === 443 ||
            $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
            substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }

	public function save_to_db(){		
		if($_POST){
			foreach ($_POST['files'] as  $value) {
				if(!empty($value)){					
				rename('./assets/jquery_file_upload/files/'.$value, './assets/jquery_file_upload/new/'.$value);
				unlink('./assets/jquery_file_upload/files/thumbnail/'.$value);
				//unlink('./assets/jquery_file_upload/files/'.$value);
				}
			}
		}
		redirect('uploader');
	}


	public function fineupload(){
		//error_reporting(E_ALL | E_STRICT);

		$this->load->library('fineuploader');	

		
		// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
		//$uploader->allowedExtensions = array('jpg', 'png', 'jpeg', 'gif');

		// Specify max file size in bytes.
		//$uploader->sizeLimit = 10 * 1024 * 1024;

		// Specify the input name set in the javascript.
		//$uploader->inputName = 'qqfile';

		// If you want to use resume feature for uploader, specify the folder to save parts.
		//$uploader->chunksFolder = '/assets/uploads/property/chunks/';

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		//$result = $uploader->handleUpload($uploadpath);

		// To save the upload with a specified name, set the second parameter.
		// $result = $uploader->handleUpload('uploads/', md5(mt_rand()).'_'.$uploader->getName());

		// To return a name used for uploaded file you can use the following line.
		//$result['uploadName'] = $uploader->getUploadName();

		//header("Content-Type: text/plain");
		//echo json_encode($result);

		$uploadpath = './assets/uploads/property/';

		$allowedExtensions = array('jpg', 'png', 'jpeg', 'gif');
    	// max file size in bytes
    	$sizeLimit = 10 * 1024 * 1024;


		$uploader = new fineuploader($allowedExtensions, $sizeLimit);
		$uploader->allowedExtensions = array('jpg', 'png', 'jpeg', 'gif');
    	$result = $uploader->handleUpload($uploadpath);
    		// to pass data through iframe you will need to encode all html tags
    	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

	}
}
