<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'libraries/facebook/facebook.php';
class Open_id_login_controller extends CI_Controller {
	
	public function __construct(){
		parent::__construct();			
		clear_cache();
		$this->load->model('open_id_login_model');
		$this->load->helper('bnb');
		$this->config->load('facebook');
	}


	public function set_session_by_ajax(){		
	     $user_role = $this->input->post('user_role');
	     $this->session->set_userdata('check_user_role',$user_role);
		echo $this->session->userdata('check_user_role');
	}

	public function google_sign_up(){
        // $this->load->library('Googleapi/lightopenid');
        require_once APPPATH.'libraries/Googleapi/lightopenid.php';
        $user_role = $this->session->userdata('check_user_role');
       
         ////////////////////////////////////////////
        ////google creadentials coding starts////////
       ////////////////////////////////////////////
        
        $openid = new LightOpenID(base_url());
  
		if ($openid->mode) {
			if ($openid->mode == 'cancel') {
			   redirect('front');
			} 
		    elseif($openid->validate()) {
				$data = $openid->getAttributes();
				$email = $data['contact/email'];
				$first = $data['namePerson/first'];
				$last  = $data['namePerson/last'];

				$a = strpos($openid->identity,'?id=');

				 $info['google_id'] = substr($openid->identity,$a+4);
				 $info['first_name'] = $first;
				 $info['last_name']  = $last;
				 $info['user_email'] = $email;
				$this->session->set_userdata('applier_information',$info);
            } 
			else {
			  redirect('front');
			}
		} 
		else{
		   redirect('front');
		}

		 ///////////////////////////////////////////
        ////google credentials coding Ends/////////
       ////////////////////////////////////////////

		if(($user_role)==15){
		   redirect('open_id_login_controller/google_sign_up_for_customer');
		}
		else if(($user_role)==20){
		   redirect('open_id_login_controller/google_sign_up_for_user');
		}
		else if(($user_role)==4){
		   redirect('open_id_login_controller/google_login_for_customer');
		}
		else if(($user_role)==3){
		   redirect('open_id_login_controller/google_login_for_user');
		}
	}

   
   public function google_sign_up_for_customer(){

	    $customer_info = $this->session->userdata('applier_information');
	    $customer_google_id = $customer_info['google_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('google_id'=>$customer_google_id),array('user_email'=>$customer_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have an account...Please login from google + ');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from google + ');
               redirect('front/index/0/1');
             }
        }
	    else{
	    	$value = array(
	    		'google_id'=>$customer_info['google_id'],
				'first_name'=>$customer_info['first_name'],
				'last_name' =>$customer_info['last_name'],
				'user_email'=>$customer_info['user_email'],
				'user_role'=>'4',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
 			);
         	 $customer_id = $this->open_id_login_model->insert('users',$value);
               
            $data  = array(  
            	            'id'       =>$customer_id,
				    		'google_id'=>$customer_info['google_id'],
							'first_name'=>$customer_info['first_name'],
							'last_name' =>$customer_info['last_name'],
							'user_email'=>$customer_info['user_email'],
							'user_role'=>'4',
							'user_status'=>'1',
							'created'    =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
            $this->session->set_userdata('customer_info',$data);
	    	redirect('customer');
	    }

    }


    public function google_sign_up_for_user(){

	    $user_info = $this->session->userdata('applier_information');
	    $user_google_id = $user_info['google_id'];
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('google_id'=>$user_google_id),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have account as a property owner...Please login from google + ');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from google + ');
               redirect('front/index/0/1');
            }
        }
	    else{
	    	$value = array(
	    		'google_id' =>$user_info['google_id'],
				'first_name'=>$user_info['first_name'],
				'last_name' =>$user_info['last_name'],
				'user_email'=>$user_info['user_email'],
				'user_role'=>'3',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
			);
         	 $user_id = $this->open_id_login_model->insert('users',$value);
               
		if(!empty($user_id)){
		  $this->open_id_login_model->update('invitation', array('joined_user_id'=>$user_id, 'is_joined'=>1), array('email'=>$user_email));

         /*Social Invite Starts*/
          if($this->session->userdata('social_invite'))
          {
			  $resr = $this->open_id_login_model->get_row('invitation',array('email'=>$user_email));
	          if(empty($resr))
	          {
	          	$session_data = $this->session->userdata('social_invite');
			    $insert_data = array(
			    	                 'user_id'=>$session_data['inviter_id'],
			    	                 'joined_user_id'=>$user_id, 
			    	                 'is_joined'=>1,
			    	                 'email'=>$user_email,
			    	                 'created'=>date('Y-m-d h:i:s')
			    	                 );
			    $this->open_id_login_model->insert('invitation',$insert_data);
	            $this->session->unset_userdata('social_invite');
	          }
          }
            /*Social Invite Ends*/

		}

            $data  = array(  
            	            'id'       =>$user_id,
				    		'google_id'=>$user_info['google_id'],
							'first_name'=>$user_info['first_name'],
							'last_name' =>$user_info['last_name'],
							'user_email'=>$user_info['user_email'],
							'user_role'=>'3',
							'user_status'=>'1',
						);
            $this->session->set_userdata('user_info',$data);
	    	redirect('user');
	    }

    }

    public function google_login_for_customer(){
	    $customer_info = $this->session->userdata('applier_information');
	    $customer_google_id = $customer_info['google_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('google_id'=>$customer_google_id),array('user_email'=>$customer_email));
            if(($response->user_role)==4){
            $data  = array(  
            	            'id'         =>$response->id,
				    		'google_id'  =>$response->google_id,
							'first_name' =>$response->first_name,
							'last_name'  =>$response->last_name,
							'user_email' =>$response->user_email,
							'user_role'  =>'4',
							'user_status'=>'1',
						);
               $this->session->set_userdata('customer_info',$data);
               redirect('customer');
            }
            else{
            $this->session->set_flashdata('error_msg','you account is registered as property owner...Please login from google + ');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 

    public function google_login_for_user()
    {
	    $user_info = $this->session->userdata('applier_information');
	    $user_google_id = $user_info['google_id'];
        $user_email = $user_info['user_email'];

        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));


        if($response!="")
        {
	        if($response->user_status!=1)
	        {
	            $this->session->set_flashdata('error_msg','Your Account has been blocked');
	        	redirect('front/index/0/2');
	        }
           $this->open_id_login_model->update('users',array('google_id'=>$user_google_id,'last_ip'=>$this->input->ip_address(),'last_login' =>date('Y-m-d h:i:s')),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $data  = array(  
            	            'id'       =>$response->id,
				    		'google_id'=>$response->google_id,
							'first_name'=>$response->first_name,
							'last_name' =>$response->last_name,
							'user_email'=>$response->user_email,
							'user_role'=>'3',
							'user_status'=>'1',
						);
               $user = $this->session->set_userdata('user_info',$data);
               redirect('user');
            }
            else{
             $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from google + ');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 

   
   	public function yahoo_sign_up(){
        // $this->load->library('Googleapi/lightopenid');
        require_once APPPATH.'libraries/Googleapi/lightopenid.php';
        $user_role = $this->session->userdata('check_user_role');
       
         ////////////////////////////////////////////
        ////yahoo creadentials coding starts////////
       ////////////////////////////////////////////
        
        $openid = new LightOpenID(base_url());
  
		if ($openid->mode) {
			if ($openid->mode == 'cancel') {
			   redirect('front');
			} 
		    elseif($openid->validate()) {
				$data = $openid->getAttributes();
				$email = $data['contact/email'];
				$name  = $data['namePerson'];
				 
				 $a = strpos($openid->identity,'/a/');
                $info['yahoo_id'] = substr($openid->identity,$a+3);
                 $b = strpos($name," "); 
               
				$info['first_name'] = substr($name,'0',$b+1);
				$info['last_name']  = substr($name,$b+1);
				$info['user_email'] = $email;
				$this->session->set_userdata('yahoo_applier_information',$info);
            } 
			else {
			  redirect('front');
			}
		} 
		else{
		   redirect('front');
		}

		 ///////////////////////////////////////////
        ////yahoo credentials coding Ends/////////
       ////////////////////////////////////////////

		if(($user_role)==15){
		   redirect('open_id_login_controller/yahoo_sign_up_for_customer');
		}
		else if(($user_role)==20){
		   redirect('open_id_login_controller/yahoo_sign_up_for_user');
		}
		else if(($user_role)==4){
		   redirect('open_id_login_controller/yahoo_login_for_customer');
		}
		else if(($user_role)==3){
		   redirect('open_id_login_controller/yahoo_login_for_user');
		}
	}

   public function yahoo_sign_up_for_customer(){

	    $customer_info = $this->session->userdata('yahoo_applier_information');
	    $customer_yahoo_id = $customer_info['yahoo_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('yahoo_id'=>$customer_yahoo_id),array('user_email'=>$customer_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have an account ...Please login from yahoo');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from yahoo');
               redirect('front/index/0/1');
             }
        }
	    else{
	    	$value = array(
	    		'yahoo_id'=>$customer_info['yahoo_id'],
				'first_name'=>$customer_info['first_name'],
				'last_name' =>$customer_info['last_name'],
				'user_email'=>$customer_info['user_email'],
				'user_role'=>'4',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
 			);
         	 $customer_id = $this->open_id_login_model->insert('users',$value);
               
            $data  = array(  
            	            'id'       =>$customer_id,
				    		'yahoo_id'=>$customer_info['yahoo_id'],
							'first_name'=>$customer_info['first_name'],
							'last_name' =>$customer_info['last_name'],
							'user_email'=>$customer_info['user_email'],
							'user_role'=>'4',
							'user_status'=>'1',
							'created'    =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
            $this->session->set_userdata('customer_info',$data);
	    	redirect('customer');
	    }

    }

    public function yahoo_sign_up_for_user(){

	    $user_info = $this->session->userdata('yahoo_applier_information');
	    $user_yahoo_id = $user_info['yahoo_id'];
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('yahoo_id'=>$user_yahoo_id),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have account as a property owner...Please login from yahoo');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from yahoo');
               redirect('front/index/0/1');
            }
        }
	    else{
	    	$value = array(
	    		'yahoo_id' =>$user_info['yahoo_id'],
				'first_name'=>$user_info['first_name'],
				'last_name' =>$user_info['last_name'],
				'user_email'=>$user_info['user_email'],
				'user_role'=>'3',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
			);
         	 $user_id = $this->open_id_login_model->insert('users',$value);
               
			if(!empty($user_id)){
			  $this->open_id_login_model->update('invitation', array('joined_user_id'=>$user_id, 'is_joined'=>1), array('email'=>$user_email));

         /*Social Invite Starts*/
          if($this->session->userdata('social_invite'))
          {
			  $resr = $this->open_id_login_model->get_row('invitation',array('email'=>$user_email));
	          if(empty($resr))
	          {
	          	$session_data = $this->session->userdata('social_invite');
			    $insert_data = array(
			    	                 'user_id'=>$session_data['inviter_id'],
			    	                 'joined_user_id'=>$user_id, 
			    	                 'is_joined'=>1,
			    	                 'email'=>$user_email,
			    	                 'created'=>date('Y-m-d h:i:s')
			    	                 );
			    $this->open_id_login_model->insert('invitation',$insert_data);
	            $this->session->unset_userdata('social_invite');
	          }
          }
            /*Social Invite Ends*/

			}

            $data  = array(  
            	            'id'       =>$user_id,
				    		'yahoo_id'=>$user_info['yahoo_id'],
							'first_name'=>$user_info['first_name'],
							'last_name' =>$user_info['last_name'],
							'user_email'=>$user_info['user_email'],
							'user_role'=>'3',
							'user_status'=>'1',
						);
            $this->session->set_userdata('user_info',$data);
	    	redirect('user');
	    }

    }

    public function yahoo_login_for_customer(){

	    $customer_info = $this->session->userdata('yahoo_applier_information');
	    $customer_yahoo_id = $customer_info['yahoo_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('yahoo_id'=>$customer_yahoo_id),array('user_email'=>$customer_email));
            if(($response->user_role)==4){
            $data  = array(  
            	            'id'         =>$response->id,
				    		'yahoo_id'  =>$response->yahoo_id,
							'first_name' =>$response->first_name,
							'last_name'  =>$response->last_name,
							'user_email' =>$response->user_email,
							'user_role'  =>'4',
							'user_status'=>'1',
						);
               $this->session->set_userdata('customer_info',$data);
               redirect('customer');
            }
            else{
            $this->session->set_flashdata('error_msg','you account is registered as property owner...Please login from yahoo ');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 

    public function yahoo_login_for_user(){
	    $user_info = $this->session->userdata('yahoo_applier_information');
	    $user_yahoo_id = $user_info['yahoo_id'];
        
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));
        

        if($response!=""){
        if($response->user_status!=1)
        {
            $this->session->set_flashdata('error_msg','Your Account has been blocked');
        	redirect('front/index/0/2');
        }
           $this->open_id_login_model->update('users',array('yahoo_id'=>$user_yahoo_id,'last_ip'=>$this->input->ip_address(),'last_login' =>date('Y-m-d h:i:s')),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $data  = array(  
            	            'id'       =>$response->id,
				    		'yahoo_id'=>$response->yahoo_id,
							'first_name'=>$response->first_name,
							'last_name' =>$response->last_name,
							'user_email'=>$response->user_email,
							'user_role'=>'3',
							'user_status'=>'1',
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
               $user = $this->session->set_userdata('user_info',$data);
               redirect('user');
            }
            else{
             $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from yahoo');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 


    
    public function facebook(){
		 //get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
        //include the facebook.php from libraries directory

    	$facebook = new Facebook(
							array(
							'appId'		=>  $this->config->item('appID'), 
							'secret'	=> $this->config->item('appSecret'),
							));	

		$user = $facebook->getUser(); // Get the facebook user id 

		if($user){
			try{
				$user_profile = $facebook->api('/me','get');

				 $info['facebook_id']  = $user_profile['id'];
				 $info['first_name']   = $user_profile['first_name'];
				 $info['last_name']    = $user_profile['last_name'];
				 $info['user_email']   = $user_profile['email'];

				$this->session->set_userdata('facebook_applier_information',$info);
			    $user_role = $this->session->userdata('check_user_role');

                
				
   				$params = array('next' => base_url().'user/logout');
				$ses_user=array('User'=>$user_profile,
				   'logout' =>$facebook->getLogoutUrl($params)   //generating the logout url for facebook 
				);
        ///////////////////////////////////////////
	    //////////  User role Check starts /////////////

			if(($user_role)==15){
			   redirect('open_id_login_controller/facebook_sign_up_for_customer');
			}
			else if(($user_role)==20){
			   redirect('open_id_login_controller/facebook_sign_up_for_user');
			}
			else if(($user_role)==4){
			   redirect('open_id_login_controller/facebook_login_for_customer');
			}
			else if(($user_role)==3){
			   redirect('open_id_login_controller/facebook_login_for_user');
			}

	    //////////  User role Check Ends  /////////////
        ///////////////////////////////////////////
			}catch(FacebookApiException $e){
				print_r($e);
				error_log($e);
				$user = NULL;
			}		

		}	

	}


	   public function facebook_sign_up_for_customer(){

	    $customer_info = $this->session->userdata('facebook_applier_information');
	    $customer_facebook_id = $customer_info['facebook_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('facebook_id'=>$customer_facebook_id),array('user_email'=>$customer_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have account as a property owner...Please login from facebook');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from facebook');
               redirect('front/index/0/1');
             }
        }
	    else{
	    	$value = array(
	    		'facebook_id'=>$customer_info['facebook_id'],
				'first_name'=>$customer_info['first_name'],
				'last_name' =>$customer_info['last_name'],
				'user_email'=>$customer_info['user_email'],
				'user_role'=>'4',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
 			);
         	 $customer_id = $this->open_id_login_model->insert('users',$value);
            $data  = array(  
            	            'id'       =>$customer_id,
				    		'facebook_id'=>$customer_info['facebook_id'],
							'first_name'=>$customer_info['first_name'],
							'last_name' =>$customer_info['last_name'],
							'user_email'=>$customer_info['user_email'],
							'user_role'=>'4',
							'user_status'=>'1',
							'created'    =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
            $this->session->set_userdata('customer_info',$data);
	    	redirect('customer');
	    }

    }

        public function facebook_sign_up_for_user(){

	    $user_info = $this->session->userdata('facebook_applier_information');
	    $user_facebook_id = $user_info['facebook_id'];
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('facebook_id'=>$user_facebook_id),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have an account...Please login from facebook');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from facebook');
               redirect('front/index/0/1');
            }
        }
	    else{
	    	$value = array(
	    		'facebook_id' =>$user_info['facebook_id'],
				'first_name'=>$user_info['first_name'],
				'last_name' =>$user_info['last_name'],
				'user_email'=>$user_info['user_email'],
				'user_role'=>'3',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
			);
         	 $user_id = $this->open_id_login_model->insert('users',$value);
               
		if(!empty($user_id)){
		  $this->open_id_login_model->update('invitation', array('joined_user_id'=>$user_id, 'is_joined'=>1), array('email'=>$user_email));

         /*Social Invite Starts*/
          if($this->session->userdata('social_invite'))
          {
			  $resr = $this->open_id_login_model->get_row('invitation',array('email'=>$user_email));
	          if(empty($resr))
	          {
	          	$session_data = $this->session->userdata('social_invite');
			    $insert_data = array(
			    	                 'user_id'=>$session_data['inviter_id'],
			    	                 'joined_user_id'=>$user_id, 
			    	                 'is_joined'=>1,
			    	                 'email'=>$user_email,
			    	                 'created'=>date('Y-m-d h:i:s')
			    	                 );
			    $this->open_id_login_model->insert('invitation',$insert_data);
	            $this->session->unset_userdata('social_invite');
	          }
          }
            /*Social Invite Ends*/

		}

            $data  = array(  
            	            'id'       =>$user_id,
				    		'facebook_id'=>$user_info['facebook_id'],
							'first_name'=>$user_info['first_name'],
							'last_name' =>$user_info['last_name'],
							'user_email'=>$user_info['user_email'],
							'user_role'=>'3',
							'user_status'=>'1',
						);
            $this->session->set_userdata('user_info',$data);
	    	redirect('user');
	    }

    }


       public function facebook_login_for_customer(){
	    $customer_info = $this->session->userdata('facebook_applier_information');
	    $customer_facebook_id = $customer_info['facebook_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('facebook_id'=>$customer_facebook_id,'last_login' =>date('Y-m-d h:i:s'),'last_ip'=>$this->input->ip_address()),array('user_email'=>$customer_email));
           
							

            if(($response->user_role)==4){
            $data  = array(  
            	            'id'         =>$response->id,
				    		'facebook_id'  =>$response->facebook_id,
							'first_name' =>$response->first_name,
							'last_name'  =>$response->last_name,
							'user_email' =>$response->user_email,
							'user_role'  =>'4',
							'user_status'=>'1',
							'last_login' =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
						);
               $this->session->set_userdata('customer_info',$data);
               redirect('customer');
            }
            else{
            $this->session->set_flashdata('error_msg','you account is registered as property owner...Please login from facebook');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 

    public function facebook_login_for_user(){
	    
	    $user_info = $this->session->userdata('facebook_applier_information');
	    $user_facebook_id = $user_info['facebook_id'];
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));
       
        
        if($response!=""){
	        if($response->user_status!=1)
	        {
	            $this->session->set_flashdata('error_msg','Your Account has been blocked');
	        	redirect('front/index/0/2');
	        }
        	$update = array('facebook_id'=>$user_facebook_id,'last_login' =>date('Y-m-d h:i:s'),'last_ip'=>$this->input->ip_address());
            $this->open_id_login_model->update('users',$update,array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $data  = array(  
            	            'id'       =>$response->id,
				    		'facebook_id'=>$response->facebook_id,
							'first_name'=>$response->first_name,
							'last_name' =>$response->last_name,
							'user_email'=>$response->user_email,
							'user_role'=>'3',
							'user_status'=>'1',
							'last_login' =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
						);
               $user = $this->session->set_userdata('user_info',$data);
               redirect('user');
            }
            else{
             $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from facebook');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }
    } 

    public function msn(){ 
	require_once APPPATH.'libraries/hotmail/msn_connect.php';
    $user_role = $this->session->userdata('check_user_role');
	$msn_code = $_GET['code'];
	// echo $msn_code;
	// die();
		if($msn_code!=""){

			$msn = new msn_connect();
			$user_info = $msn->get_user_info($msn_code);
			// print_r($user_info);
			// die();
		    $info['first_name'] = $user_info['first_name'];
		    $info['last_name']  = $user_info['last_name'];
		    $info['user_email'] = $user_info['emails']['account'];
		    $info['msn_id']     = $msn_code;
		    $this->session->set_userdata('applier_information',$info);

	    }
	    else{
             $this->session->set_flashdata('error_msg','Something went wrong . Please Try Again!');
             redirect('front/index/0/1');
	    }

	   	if(($user_role)==15){
		   redirect('open_id_login_controller/msn_sign_up_for_customer');
		}
		else if(($user_role)==20){
		   redirect('open_id_login_controller/msn_sign_up_for_user');
		}
		else if(($user_role)==4){
		   redirect('open_id_login_controller/msn_login_for_customer');
		}
		else if(($user_role)==3){
		   redirect('open_id_login_controller/msn_login_for_user');
		}
    }

       public function msn_sign_up_for_customer(){

	    $customer_info = $this->session->userdata('applier_information');
	    $customer_msn_id = $customer_info['msn_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('msn_id'=>$customer_msn_id),array('user_email'=>$customer_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have account as a property owner...Please login from msn');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from msn');
               redirect('front/index/0/1');
             }
        }
	    else{
	    	$value = array(
	    		'msn_id'=>$customer_info['msn_id'],
				'first_name'=>$customer_info['first_name'],
				'last_name' =>$customer_info['last_name'],
				'user_email'=>$customer_info['user_email'],
				'user_role'=>'4',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
 			);
         	 $customer_id = $this->open_id_login_model->insert('users',$value);
               
            $data  = array(  
            	            'id'       =>$customer_id,
				    		'msn_id'=>$customer_info['msn_id'],
							'first_name'=>$customer_info['first_name'],
							'last_name' =>$customer_info['last_name'],
							'user_email'=>$customer_info['user_email'],
							'user_role'=>'4',
							'user_status'=>'1',
							'created'    =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
            $this->session->set_userdata('customer_info',$data);
	    	redirect('customer');
	    }

    }

        public function msn_sign_up_for_user(){

	    $user_info = $this->session->userdata('applier_information');
	    $user_msn_id = $user_info['msn_id'];
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('msn_id'=>$user_msn_id),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have an account...Please login from msn');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from msn');
               redirect('front/index/0/1');
            }
        }
	    else{
	    	$value = array(
	    		'msn_id' =>$user_info['msn_id'],
				'first_name'=>$user_info['first_name'],
				'last_name' =>$user_info['last_name'],
				'user_email'=>$user_info['user_email'],
				'user_role'=>'3',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
			);
         	 $user_id = $this->open_id_login_model->insert('users',$value);
               
			if(!empty($user_id)){
			  $this->open_id_login_model->update('invitation', array('joined_user_id'=>$user_id, 'is_joined'=>1), array('email'=>$user_email));

         /*Social Invite Starts*/
          if($this->session->userdata('social_invite'))
          {
			  $resr = $this->open_id_login_model->get_row('invitation',array('email'=>$user_email));
	          if(empty($resr))
	          {
	          	$session_data = $this->session->userdata('social_invite');
			    $insert_data = array(
			    	                 'user_id'=>$session_data['inviter_id'],
			    	                 'joined_user_id'=>$user_id, 
			    	                 'is_joined'=>1,
			    	                 'email'=>$user_email,
			    	                 'created'=>date('Y-m-d h:i:s')
			    	                 );
			    $this->open_id_login_model->insert('invitation',$insert_data);
	            $this->session->unset_userdata('social_invite');
	          }
          }
            /*Social Invite Ends*/

			}

            $data  = array(  
            	            'id'       =>$user_id,
				    		'msn_id'=>$user_info['msn_id'],
							'first_name'=>$user_info['first_name'],
							'last_name' =>$user_info['last_name'],
							'user_email'=>$user_info['user_email'],
							'user_role'=>'3',
							'user_status'=>'1',
						);
            $this->session->set_userdata('user_info',$data);
	    	redirect('user');
	    }

    }

        public function msn_login_for_customer(){
	    $customer_info = $this->session->userdata('applier_information');
	    $customer_msn_id = $customer_info['msn_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('msn_id'=>$customer_msn_id),array('user_email'=>$customer_email));
            if(($response->user_role)==4){
            $data  = array(  
            	            'id'         =>$response->id,
				    		'msn_id'     =>$response->msn_id,
							'first_name' =>$response->first_name,
							'last_name'  =>$response->last_name,
							'user_email' =>$response->user_email,
							'user_role'  =>'4',
							'user_status'=>'1',
						);
               $this->session->set_userdata('customer_info',$data);
               redirect('customer');
            }
            else{
            $this->session->set_flashdata('error_msg','you account is registered as property owner...Please login from msn');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 

    public function msn_login_for_user(){
	    $user_info = $this->session->userdata('applier_information');
	    $user_msn_id = $user_info['msn_id'];
        
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));
       

        if($response!="")
        {
	        if($response->user_status!=1)
	        {
	            $this->session->set_flashdata('error_msg','Your Account has been blocked');
	        	redirect('front/index/0/2');
	        }
           $this->open_id_login_model->update('users',array('msn_id'=>$user_msn_id,'last_ip'=>$this->input->ip_address(),'last_login' =>date('Y-m-d h:i:s')),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $data  = array(  
            	            'id'         =>$response->id,
				    		'msn_id'     =>$response->msn_id,
							'first_name' =>$response->first_name,
							'last_name'  =>$response->last_name,
							'user_email' =>$response->user_email,
							'user_role'  =>'3',
							'user_status'=>'1',
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
               $user = $this->session->set_userdata('user_info',$data);
               redirect('user');
            }
            else{
             $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from msn');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    }

    public function linkedin(){
	    require_once APPPATH.'libraries/linkedin/config.php';
	    require_once APPPATH.'libraries/linkedin/linkedinoAuth.php';
	    require_once APPPATH.'libraries/linkedin/linkedClass.php';    
		    
	    $linkedClass   =   new linkedClass();
	    $linkedin = new linkedinoAuth($config['linkedin_access'], $config['linkedin_secret']);
	    $request_token = $this->session->userdata('request_token');

	   if (isset($_REQUEST['oauth_verifier'])){

	        $linkedin->request_token    =   unserialize($request_token);
	        $linkedin->oauth_verifier   =   $_REQUEST['oauth_verifier'];
	        $linkedin->getAccessToken($_REQUEST['oauth_verifier']);
	        $oauth_access_token = serialize($linkedin->access_token);
       }
	   else{
	        $linkedin->request_token    =   unserialize($request_token);
	        $linkedin->oauth_verifier   =   $_SESSION['oauth_verifier'];
	        $linkedin->access_token     =   unserialize($oauth_access_token);
	   }
   
   $content1 = $linkedClass->linkedinGetUserInfo($request_token,$_REQUEST['oauth_verifier'], $oauth_access_token,$config['linkedin_access'],$config['linkedin_secret']);

   
    $xml   = simplexml_load_string($content1);
    $array = $this->XML2Array($xml);
    $content = array($xml->getName() => $array);

    /////////////////////////////////////////
    ///////// Linkedin  User info ///////////
    /////////////////////////////////////////
    

    $info['first_name'] = $content['person']['first-name'];
    $info['last_name']  = $content['person']['last-name'];
    $info['user_email'] = $content['person']['email-address'];
    $info['linkedin_id'] = $content['person']['id'];
	$this->session->set_userdata('applier_information',$info);

        $user_role = $this->session->userdata('check_user_role');
		if(($user_role)==15){
		   redirect('open_id_login_controller/linkedin_sign_up_for_customer');
		}
		else if(($user_role)==20){
		   redirect('open_id_login_controller/linkedin_sign_up_for_user');
		}
		else if(($user_role)==4){
		   redirect('open_id_login_controller/linkedin_login_for_customer');
		}
		else if(($user_role)==3){
		   redirect('open_id_login_controller/linkedin_login_for_user');
		}
    /////////////////////////////////////////
    ///////// Linkedin  User info ///////////
    /////////////////////////////////////////
    }

    public function XML2Array(SimpleXMLElement $parent)
    {
        $array = array();
        foreach ($parent as $name => $element) {
            ($node = & $array[$name])
                && (1 === count($node) ? $node = array($node) : 1)
                && $node = & $node[];
            $node = $element->count() ? XML2Array($element) : trim($element);
        }
        return $array;
    }

   public function linkedin_sign_up_for_customer(){
	    $customer_info = $this->session->userdata('applier_information');
	    $customer_linkedin_id = $customer_info['linkedin_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('linkedin_id'=>$customer_linkedin_id),array('user_email'=>$customer_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have account as a property owner...Please login from linkedin');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from linkedin');
               redirect('front/index/0/1');
             }
        }
	    else{
	    	$value = array(
	    		'linkedin_id'=>$customer_info['linkedin_id'],
				'first_name'=>$customer_info['first_name'],
				'last_name' =>$customer_info['last_name'],
				'user_email'=>$customer_info['user_email'],
				'user_role'=>'4',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
 			);
         	 $customer_id = $this->open_id_login_model->insert('users',$value);
               
            $data  = array(  
            	            'id'       =>$customer_id,
				    		'linkedin_id'=>$customer_info['linkedin_id'],
							'first_name'=>$customer_info['first_name'],
							'last_name' =>$customer_info['last_name'],
							'user_email'=>$customer_info['user_email'],
							'user_role'=>'4',
							'user_status'=>'1',
							'created'    =>date('Y-m-d h:i:s'),
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
            $this->session->set_userdata('customer_info',$data);
	    	redirect('customer');
	    }

    }

    public function linkedin_sign_up_for_user(){
	    $user_info = $this->session->userdata('applier_information');
	    $user_linkedin_id = $user_info['linkedin_id'];
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('linkedin_id'=>$user_linkedin_id),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $this->session->set_flashdata('error_msg','you already have account as a property owner...Please login from linkedin');
               redirect('front/index/0/1');
            }
            else{
            $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from linkedin');
               redirect('front/index/0/1');
            }
        }
	    else{
	    	$value = array(
	    		'linkedin_id' =>$user_info['linkedin_id'],
				'first_name'=>$user_info['first_name'],
				'last_name' =>$user_info['last_name'],
				'user_email'=>$user_info['user_email'],
				'user_role'=>'3',
				'user_status'=>'1',
				'created'    =>date('Y-m-d h:i:s'),
				'last_ip'    =>$this->input->ip_address(),
				'last_login' =>date('Y-m-d h:i:s'),
			);
         	 $user_id = $this->open_id_login_model->insert('users',$value);
               

		if(!empty($user_id)){
		  $this->open_id_login_model->update('invitation', array('joined_user_id'=>$user_id, 'is_joined'=>1), array('email'=>$user_email));

         /*Social Invite Starts*/
          if($this->session->userdata('social_invite'))
          {
			  $resr = $this->open_id_login_model->get_row('invitation',array('email'=>$user_email));
	          if(empty($resr))
	          {
	          	$session_data = $this->session->userdata('social_invite');
			    $update_data = array(
			    	                 'joined_user_id'=>$user_id, 
			    	                 'is_joined'=>1,
			    	                 'email'=>$user_email,
			    	                 'created'=>date('Y-m-d h:i:s')
			    	                 );
			    $this->open_id_login_model->update('invitation',$update_data,array('user_id'=>$session_data['inviter_id']));
	            $this->session->unset_userdata('social_invite');
	          }
          }
            /*Social Invite Ends*/

		}

            $data  = array(  
            	            'id'       =>$user_id,
				    		'linkedin_id'=>$user_info['linkedin_id'],
							'first_name'=>$user_info['first_name'],
							'last_name' =>$user_info['last_name'],
							'user_email'=>$user_info['user_email'],
							'user_role'=>'3',
							'user_status'=>'1',
						);
            $this->session->set_userdata('user_info',$data);
	    	redirect('user');
	    }

    }

    public function linkedin_login_for_customer(){
	    $customer_info = $this->session->userdata('applier_information');
	    $customer_linkedin_id = $customer_info['linkedin_id'];
       
        $customer_email = $customer_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$customer_email));

        if($response!=""){
           $this->open_id_login_model->update('users',array('linkedin_id'=>$customer_linkedin_id),array('user_email'=>$customer_email));
            if(($response->user_role)==4){
            $data  = array(  
            	            'id'         =>$response->id,
				    		'linkedin_id'  =>$response->linkedin_id,
							'first_name' =>$response->first_name,
							'last_name'  =>$response->last_name,
							'user_email' =>$response->user_email,
							'user_role'  =>'4',
							'user_status'=>'1',
						);
               $this->session->set_userdata('customer_info',$data);
               redirect('customer');
            }
            else{
            $this->session->set_flashdata('error_msg','you account is registered as property owner...Please login from linkedin');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }

    } 

    public function linkedin_login_for_user(){
	    $user_info = $this->session->userdata('applier_information');
	    $user_linkedin_id = $user_info['linkedin_id'];
        
        $user_email = $user_info['user_email'];
        $response = $this->open_id_login_model->get_row('users',array('user_email'=>$user_email));
        
        if($response!=""){
           $this->open_id_login_model->update('users',array('linkedin_id'=>$user_linkedin_id),array('user_email'=>$user_email));
            if(($response->user_role)==3){
            $data  = array(  
            	            'id'       =>$response->id,
				    		'linkedin_id'=>$response->linkedin_id,
							'first_name'=>$response->first_name,
							'last_name' =>$response->last_name,
							'user_email'=>$response->user_email,
							'user_role'=>'3',
							'user_status'=>'1',
							'last_ip'    =>$this->input->ip_address(),
							'last_login' =>date('Y-m-d h:i:s'),
						);
               $user = $this->session->set_userdata('user_info',$data);
               redirect('user');
            }
            else{
             $this->session->set_flashdata('error_msg','you already have account as a customer...Please login from linkedin');
               redirect('front/index/0/1');
            }
        }
        else{
            $this->session->set_flashdata('error_msg','You do not have an account.. Please Sign up first');
        	redirect('front/index/0/2');
        }
    } 

}