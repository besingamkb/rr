<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class subadmin extends CI_Controller {

	

	public function __construct()
	{

		parent::__construct();			

		clear_cache();

		$this->load->helper('download');

		$this->load->model('subadmin_model');

         $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1));

		 $currency = $this->session->userdata('currency');

    	 $data['row']=$this->subadmin_model->get_row('site_details');

	    if(empty($currency))

	    {

		    $currency = $data['row']->currency;

	        $this->session->set_userdata('currency',$currency);

	    }

	    elseif($data['row']->currency_status==1)

	    {

		    $currency = $data['row']->currency;

	        $this->session->set_userdata('currency',$currency);

	    }

	}



	public function index()
	{	
		$this->check_login();
		$this->dashboard();		
	}

	private function check_login()
	{
		if(subadmin_login()===FALSE)
			redirect('subadmin/login');
	}

	public function check_down($file="")
	{

		$data = base_url()."/assets/uploads/email_attachment/"; // Read the file's contents

		$name = $file;

		$status=force_download($name, $data);

		if($status){

			redirect('subadmin/');

		} 

	}



		public function login()
		{

			if(subadmin_login()===TRUE)
				redirect('subadmin');

			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

			$this->form_validation->set_rules('password', 'Password', 'required');

			$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');					

		

		 if ($this->form_validation->run() == TRUE)
		 {
			$this->load->model('login_model');				

			$resp=$this->login_model->login($this->input->post('email'),$this->input->post('password'),3);

			if($resp['status'])

				redirect('subadmin/dashboard');

			else{

				$this->session->set_flashdata('error_msg', $resp['error_msg']);

				redirect('subadmin/login');

				}
		}

        $this->load->view('subadmin/login');		

	}



	public function logout(){	 	

	 	$this->session->unset_userdata('subadmin_info');

	 	$this->session->unset_userdata('user_info');

	 	$this->session->set_flashdata('success_msg', 'You are successfully logged out.');

	 	redirect('subadmin/login');

	}


	public function dashboard($offset=0)

	{		

		$this->check_login();

		$limit=10;

		$data['events'] = $this->subadmin_model->get_pagination_where('admin_events',200,0,array('status'=>0));



        $data['template'] = 'subadmin/dashboard1';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function do_complete($id,$status)

	{

		$this->check_login();

		$this->subadmin_model->update('admin_events', array('status' => $status), array('id' => $id) );

		echo 'ok';

	}









	public function properties($sort='-' ,$offset=0){

		//$data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard','properties' => '/subadmin/properties'));

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->properties==2)
        {
             redirect('subadmin/dashboard');
        }


		$this->check_login();

		$email ='';

		$name  ='';

		$post = "";

		if($_POST){

			$sort = $this->input->post('sort');

			$name = $this->input->post('name');

			$email= $this->input->post('email');

			$post = $_POST;

		}

		$limit=10;

		$data['properties'] = $this->subadmin_model->properties($limit, $offset,$sort,$name,$email,$post);

		$config= get_theme_pagination();

		$config['base_url'] = base_url().'subadmin/properties/'.$sort;

		$config['total_rows'] = $this->subadmin_model->properties(0,0,$sort,$name,$email,$post);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;

		$config['uri_segment']=4;

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

		$data['template'] = 'subadmin/properties';

        $this->load->view('templates/subadmin_template', $data);



	}



     /*Property Email Starts From here*/

	public function property_email($property_id='')

	{

		$this->check_login();

		if (empty($property_id)) {

			$this->session->set_flashdata('error_msg' , "Email can't be send.");

			redirect('subadmin/properties');

		}

		$data['property'] = $this->subadmin_model->get_row('properties', array('id' => $property_id));

		$data['user'] = $this->subadmin_model->get_row('users', array('id'=>$data['property']->user_id));

		$owneremail = $data['user']->user_email;

		$ownername = $data['user']->first_name;

		$this->form_validation->set_rules('subject', 'Subject', 'required');	

		$this->form_validation->set_rules('message', 'Message', 'required');

		if ($this->form_validation->run() == TRUE){

			$subject =	$this->input->post('subject');

			$message =	$this->input->post('message');

			$array = array(

							'property_id' =>$property_id ,

						    'owner_email' =>$owneremail,

						    'owner_name'  =>$ownername,

				     		    'subject' =>$subject,

				     		    'message' =>$message,

				           'owner_contact' =>$data['user']->phone,

				                 'created' =>date('Y-m-d h:i:s'),

        			  	   );

			if($_FILES['userfile']['name']!=''){

				$attach ='';

				$file=$_FILES['userfile']['name'];

				$attach=$_FILES['userfile']['tmp_name'];

				$config['upload_path'] = './assets/uploads/email_attachment/';

				$config['allowed_types'] = 'gif|jpg|png|pdf|txt';

				$config['max_size']	= '20000';

				$this->load->library('upload', $config);											

				if(! $this->upload->do_upload('userfile'))

				{

					$this->session->set_flashdata('image_error', $this->upload->display_errors());

					redirect('subadmin/property_email/'.$property_id);

				}else{

					$array['attachment'] = $_FILES['userfile']['name'];

				}

			}else{

				$attach = "";

			}			

			

			$status = $this->send_property_email_message($ownername,$owneremail,$subject,$message,$attach);

			$this->subadmin_model->insert('property_email_record',$array);



			if($status){

				$this->session->set_flashdata('success_msg',"Email  has been sent successfully.");

				redirect('subadmin/properties');

			}		

		}	

		$data['template'] = 'subadmin/property_email';

        $this->load->view('templates/subadmin_template',$data);	



	}

	public function send_property_email_message($name='',$email='',$subject='',$message='' ,$attach='')

	{

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$from = array('no-reply@bnbclone.com' =>'BnbClone');	// From email in array form

		$to = array($email);

		$html = '';

		$html .= '<html>

					<body>

					  <h3>Dear'.$name.'</h3>';

		$html .= '<p>This is Notifications for you from the Bnbclone on your Property </p>';

		$html .= '<strong>'.$subject.'</strong>';

		$html .=	'<p>';

		$html .=  $message;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';



		$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html,$attach);

		if($is_fail){

		return FALSE;

		

		}

		else{

			return TRUE;

		}

	}



	public function property_email_records($offset=0)

	{

		$this->check_login(); 

		$limit=10;

		$data['offset'] = $offset;

		$data['history']=$this->subadmin_model->get_pagination_result('property_email_record',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/property_email_records/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('property_email_record',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();	

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard', 'Faqs' => '/subadmin/faqs'));

		$data['template'] = 'subadmin/property_email_records';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function view_property_email($id=''){

		$this->check_login();

		if(empty($id)) redirect('subadmin/property_email_records');		

		$data['property_record'] = $this->subadmin_model->get_row('property_email_record', array('id' => $id));

		$data['template'] = 'subadmin/view_property_email_record';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function delete_property_email($id=''){

		$this->check_login();

		if(empty($id)) redirect('subadmin/property_email_records');		

		$this->subadmin_model->delete('property_email_record',array('id'=>$id));

		$this->session->set_flashdata('success_msg',"Property Email Record has been deleted successfully.");

		redirect('subadmin/property_email_records');

	}



	/*Property  email Work ends From here*/



     /////////////////////////////////////

     /////////////////////////////////////

     /////////////////////////////////////

     /////////////////////////////////////





	/*Notes Work strats From here*/

	public function property_notes($property_id='')

	{

		$this->check_login();

		if(empty($property_id)) redirect('subadmin/properties'); 

		$data['notes']=$this->subadmin_model->get_result('property_notes',array('pr_id' =>$property_id));

		$data['id'] = $property_id;

		$data['template'] = 'subadmin/property_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function add_property_note($property_id='')

	{

		$this->check_login(); 

		if(empty($property_id)) redirect('subadmin/properties');

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){			

			$data=array(

						'title'=>$this->input->post('title'),				

						'description'=>$this->input->post('description'),

						'pr_id'=>$property_id,

						'created' => date('Y-m-d H:i:s')		

				       );	

			$this->subadmin_model->insert('property_notes',$data);		

			$this->session->set_flashdata('success_msg'," Successfully Property Note Added.");

			redirect('subadmin/property_notes/'.$property_id);

		}

		$data['template'] = 'subadmin/add_property_note';

        $this->load->view('templates/subadmin_template', $data);		



	}



	public function view_property_note($property_id='')

	{

		$this->check_login();

		if(empty($property_id)) redirect('subadmin/properties'); 

		$data['notes']=$this->subadmin_model->get_row('property_notes',array('id' =>$property_id));

		if(empty($data['notes'])){

			redirect('subadmin/properties');

		}

		$data['template'] = 'subadmin/view_property_note';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_property_note($id="",$property_id="")

	{

		if($id=="")

		{

			redirect('subadmin/properties');

		}

		$this->subadmin_model->delete('property_notes',array('id'=>$id));

		$this->session->set_flashdata('success_msg'," Property note has been deleted successfully.");

		redirect('subadmin/property_notes/'.$property_id);

	}

	public function check_approve_property($property_id="")
	{ 
		$detail =  get_property_detail($property_id);
        
        $i = 0;
        if(str_word_count($detail->description)<15)
        { 
        	$i++;
           echo "".$i.".Your Property description is less then 15 words.  ";
        }
        
		$response=$this->subadmin_model->get_row('pr_gallery',array('pr_id' =>$property_id));


        if(!empty($response))
        {
        	if(empty($detail->featured_image))
        	{
        		$i++;
                echo "".$i.".You Have Not Selected any image as a property profile image.  ";
        	}
        }
        else
        { 
        	$i++;
           echo "".$i.".You Have Not Uploaded Any Image yet . Please Upload and select any image from them. ";
        }
        
	}

     

     /*Payments work ends from here*/





	public function approve_property($pr_id='')
	{
		$this->check_login();

		if(empty($pr_id)) redirect('subadmin/properties');

		$data['member'] = $this->subadmin_model->get_row('properties', array('id' => $pr_id));
        
		if($data['member']->status == 0){

			$array = array('status' => 1,'publish_permission'=>1);

				$gain = $this->subadmin_model->get_row('invitation',array('joined_user_id'=>$data['member']->user_id));

	            if(!empty($gain) && $gain->property_credit_given==0)

	            {

				    $this->subadmin_model->update('invitation',array('property_credit_given'=>1),array('joined_user_id'=>$data['member']->user_id));

	            }

			$this->subadmin_model->update('properties',$array,array('id' => $pr_id));

			$this->session->set_flashdata('success_msg',"Property status has been updated successfully.");

			redirect('subadmin/properties');

		}

		else{

			$array = array('status' =>0,'publish_permission'=>0);

			$this->subadmin_model->update('properties',$array,array('id' => $pr_id));

			$this->session->set_flashdata('success_msg',"Property status has been updated successfully.");

			redirect('subadmin/properties');

		}



	}



	public function approve_all_property()
	{

		$this->check_login();

		$properties = $this->subadmin_model->get_result('properties');
        
        foreach ($properties as $value) 
        {
	        if(str_word_count($value->description)<15)
	        { 
              continue;
	        }
	        
			$response=$this->subadmin_model->get_row('pr_gallery',array('pr_id' =>$value->id));


	        if(!empty($response))
	        {
	        	if(empty($value->featured_image))
	        	{
                   continue;
	        	}
	        }
	        else
	        { 
               continue;
	        }

			$gain = $this->subadmin_model->get_row('invitation',array('joined_user_id'=>$value->user_id));
            if(!empty($gain) && $gain->property_credit_given==0)
            {
			    $this->subadmin_model->update('invitation',array('property_credit_given'=>1),array('joined_user_id'=>$value->user_id));
            }
		  $this->subadmin_model->update('properties',array('status'=>1,'publish_permission'=>1),array('id'=>$value->id));
         }

		$this->session->set_flashdata('success_msg',"The status of All Those Properties which has atleast required entries has been updated successfully.");

		redirect('subadmin/properties');

	}









	public function member_email($mem_id='')

	{

		$this->check_login();

		if (empty($mem_id)) {

			$this->session->set_flashdata('error_msg' , "Member Info can't be updated.");

			redirect('subadmin/members');

		}

		$data['member'] = $this->subadmin_model->get_row('users', array('id' => $mem_id));

		// print_r($data['member']);

		// die();

		$this->form_validation->set_rules('member_name', 'Member name', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		$this->form_validation->set_rules('subject', 'Subject', 'required');	

		$this->form_validation->set_rules('message', 'Message', 'required');

		if ($this->form_validation->run() == TRUE){

			$name 	 =  $this->input->post('member_name');

			$email	 =	$this->input->post('email');

			$subject =	$this->input->post('subject');

			$message =	$this->input->post('message');

		$status = $this->send_member_email_message($name,$email,$subject,$message);

			if($status){

				$this->session->set_flashdata('success_msg',"Email  has been sent successfully.");

				redirect('subadmin/members');

			}		

		}	

		$data['template'] = 'subadmin/email_member';

        $this->load->view('templates/subadmin_template', $data);	



	}

	public function send_member_email_message($name='',$email='',$subject='',$message='')

	{

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$from = array('no-reply@bnbclone.com' =>'BnbClone');	// From email in array form

		$to = array($email,);

		$html = '';

		$html .= '<html>

					<body>

					  <h3>Dear Member'.$name.'</h3>';

		$html .= '<p>This is Notifications for you form the Bnbclone</p>';

		$html .= '<strong>'.$subject.'</strong>';

		$html .=	'<p>';

		$html .=  $message;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';

	

		$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);

		if($is_fail){

		return FALSE;

		

		}

		else{

			return TRUE;

		}

	}



	public function send_member_adding_mail($name,$email_to,$s_subject,$message){

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$subject = 'Added as member Notification';	// Subject for email

		$from = array('no-reply@bnbclone.com' =>'BnbClone');	// From email in array form	

		$to = array(

			 $email_to,

		);

		$html = "<em><strong>Hello ".$name." !</strong></em> <br>

				<p><strong>Subject - ".$s_subject."</strong></p>

				<p><strong>Message - ".$message."</strong></p>";

		$this->smtp_email->sendEmail($from, $to, $subject, $html);

		

		return TRUE;



	}



	public function check_is_email_unique($new_email, $old_email){

		if ($old_email === $new_email) {

			return TRUE;

		}else{

			$resp = $this->subadmin_model->get_row('users', array('user_email' => $new_email));

			if ($resp) {

				$this->form_validation->set_message('check_is_email_unique', 'Email you are choosing already exist.');

				return FALSE;

			}else{

				return TRUE;

			}

		}

	}





	

	public function members($sort='-',$offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->member==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$search_by  ='';

		$post="";

		if($_POST){

			$sort      = $this->input->post('sort');

			$search_by = $this->input->post('search_by');

			$post = $_POST;

		}

		$limit=10;

		$data['members'] = $this->subadmin_model->members($limit, $offset, $sort, $search_by,$post);

		$config= get_theme_pagination();

		$config['base_url'] = base_url().'subadmin/members/'.$sort;

		$config['total_rows'] = $this->subadmin_model->members(0, 0, $sort, $search_by,$post);

		$config['per_page'] =10;

		$config['num_links'] =4;

		$config['uri_segment']=4;

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

        

        $data['template'] = 'subadmin/members';

        $this->load->view('templates/subadmin_template', $data);

	}





	public function add_member(){		

		$this->check_login();



		$this->form_validation->set_rules('firstname', 'First name', 'required');

		$this->form_validation->set_rules('lastname', 'Last name', 'required');		

		$this->form_validation->set_rules('user_role', 'User Type', 'required');		

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.user_email]');

		$this->form_validation->set_rules('phone', 'phone', 'required');

		$this->form_validation->set_rules('address', 'Address', 'required');

        $this->form_validation->set_rules('city', 'City', 'required');

        $this->form_validation->set_rules('state', 'State', 'required');

        $this->form_validation->set_rules('zip', 'Zip', 'required');

        $this->form_validation->set_rules('country', 'Country', 'required');

        $this->form_validation->set_error_delimiters('<span style="color:#D33F3E"><span>');

		if ($this->form_validation->run() == TRUE){

			$length = 8;

	        $member_pass = trim(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length));

	        $name = $this->input->post('firstname')." ".$this->input->post('lastname');

	        $email = $this->input->post('email');

			

			$image = $this->do_core_upload('userfile','./assets/uploads/profile_image/');



			$user=array(

				'image'         => $image, 

				'user_role' 	=>	3,

				'first_name'	=>	strtolower($this->input->post('firstname')),

				'last_name'		=>	strtolower($this->input->post('lastname')),

				'user_email'	=>	$email,

				'phone'			=>	$this->input->post('phone'),

				'password'		=>	sha1(trim($member_pass)),

				'address'		=>	$this->input->post('address'),	

				'city'			=>	$this->input->post('city'),	

				'state'			=>	$this->input->post('state'),	

				'zip'			=>	$this->input->post('zip'),	

				'country'		=>	$this->input->post('country'),

				'created' 		=>	date('Y-m-d')

				);


            if($_POST['user_role']=='subadmin')
            {
            	$user['is_subadmin'] = 1;
            }


			

			$user_id=$this->subadmin_model->insert('users',$user);

			

			if ($user_id) {

				$s_subject = 'Member Notification';

				$message = 'Hello '.$name.':<br>';

				$message .= 'You have successfully added as member at Bnbclone.com.<br>';

				$message .= 'Your Login credentials are :<br>';

				$message .= 'login Url :N/A<br>';

				$message .= 'Login Eamil:'.$email.'<br>';

				$message .= 'login Password :'.$member_pass.'<br>';

				$this->send_member_adding_mail($name,$email,$s_subject,$message);

				$this->session->set_flashdata('success_msg',"Member has been added successfully.");

				redirect('subadmin/members');

			}else{

				$this->session->set_flashdata('error_msg',"Error in adding information.");

				redirect('subadmin/add_member');

			}



		}

        $data['template'] = 'subadmin/add_member';

        $this->load->view('templates/subadmin_template', $data);		

	}







	public function edit_member($mem_id = '')
	{
		$this->check_login();
		if (empty($mem_id)) 
		{
			$this->session->set_flashdata('error_msg' , "Member Info can't be updated.");
			redirect('subadmin/members');
		}

		$data['member'] = $this->subadmin_model->get_row('users', array('id' => $mem_id));

		$this->form_validation->set_rules('firstname', 'First name', 'required');

		$this->form_validation->set_rules('lastname', 'Last name', 'required');		

		$this->form_validation->set_rules('user_role', 'User Type', 'required');		

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_is_email_unique['.$data['member']->user_email.']');

		$this->form_validation->set_rules('phone', 'phone', 'required');

		$this->form_validation->set_rules('address', 'Address', 'required');

        $this->form_validation->set_rules('city', 'City', 'required');

        $this->form_validation->set_rules('state', 'State', 'required');

        $this->form_validation->set_rules('zip', 'Zip', 'required');

        $this->form_validation->set_rules('country', 'Country', 'required');

        $this->form_validation->set_error_delimiters('<span style="color:#D33F3E"></span>');

	

		if ($this->form_validation->run() == TRUE)
		{

	        $name = $this->input->post('firstname')." ".$this->input->post('lastname');

	        $email = $this->input->post('email');

			$user=array(

				'user_role' 	=>	3,

				'first_name'	=>	strtolower($this->input->post('firstname')),

				'last_name'		=>	strtolower($this->input->post('lastname')),

				'user_email'	=>	$email,

				'phone'			=>	$this->input->post('phone'),

				'address'		=>	$this->input->post('address'),	

				'city'			=>	$this->input->post('city'),	

				'state'			=>	$this->input->post('state'),	

				'zip'			=>	$this->input->post('zip'),	

				'country'		=>	$this->input->post('country'),

				'created' 		=>	date('Y-m-d')

				);


            if($_POST['user_role']=='subadmin')
            {
            	$user['is_subadmin'] = 1; 
            }

            if($_POST['user_role']=='user')
            {
            	$user['is_subadmin'] = 0;
            }

			if(!empty($_FILES['userfile']['name']))

			{

				$image = $this->do_core_upload('userfile','./assets/uploads/profile_image/');

				$user['image']	= $image;	

			}

			

			$user_id=$this->subadmin_model->update('users',$user, array('id' => $mem_id));

			if ($user_id)

			{

				$this->session->set_flashdata('success_msg',"Member has been updaed successfully.");

				redirect('subadmin/members');

			}

			else

			{

				$this->session->set_flashdata('error_msg',"Error in updating information.");

				redirect('subadmin/members');

			}

		}

		$data['template'] = 'subadmin/edit_member';

        $this->load->view('templates/subadmin_template', $data);	

	}





	public function do_core_upload($filename2='user_file' , $upload_path='./assets/uploads/custom_uploads/', $path_of_thumb='')

	{

		$allowed =  array('gif','png','jpg','jpeg');

		$filename = $_FILES[$filename2]['name'];

		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		if(!in_array($ext,$allowed) ){

			return FALSE;

		}

		else{

			

			if ($_FILES[$filename2]["error"] > 0){

	 			return FALSE; 

	 		}

			else{

			 $name = uniqid();

			 if(move_uploaded_file($_FILES[$filename2]['tmp_name'],$upload_path.$name.'.'.$ext))

			 return $name.'.'.$ext;

			 else

			 return FALSE;

			}

		} 

	}

	

public function delete_member($mem_id){

		$this->check_login();

		if(empty($mem_id)) redirect('subadmin/members');		

		$this->subadmin_model->update('users',array('is_delete'=>1,'user_status'=>0),array('id'=>$mem_id));
		
		/*Message delete starts */
		$message_result = $this->subadmin_model->get_result('user_message',array('sender_id'=>$mem_id));
		 if(!empty($message_result))
		 {
			foreach ($message_result as $value)
			 {
	    		$this->subadmin_model->delete('user_message_reply',array('message_id'=>$value->id));
			 }
		 }
		$this->subadmin_model->delete('user_message',array('sender_id'=>$mem_id));
		/*Message delete Ends*/

		/*User image starts*/
		 $image_reult = $this->subadmin_model->get_result('user_pic_gallery',array('user_id'=>$mem_id));
		 if(!empty($image_reult))
		 {
			 foreach ($image_reult as $value)
			 {
			 	@unlink(".assets/uploads/profile_image/".$value->image);
			 }
		 }
		 $this->subadmin_model->delete('user_pic_gallery',array('user_id'=>$mem_id));
		/*User image Ends*/


		/*Groups Of this users starts*/
  		 $this->subadmin_model->get_result('group_members',array('member_id'=>$mem_id));
  		 $this->subadmin_model->get_result('group_members_properties',array('member_id'=>$mem_id));
		/*Groups Of this users ends*/

		/*Properties of this user starts*/
		  $properties = $this->subadmin_model->get_result('properties',array('user_id'=>$mem_id));
		  
           if(!empty($properties))
           {
           	  foreach ($properties as $row)
           	  {
                $this->subadmin_model->delete('pr_calender',array('property_id'=>$row->id));   
                $this->subadmin_model->delete('listing_price',array('listing_id'=>$row->id));   
                $this->subadmin_model->delete('property_rating',array('pr_id'=>$row->id));   
                $this->subadmin_model->delete('property_email_record',array('id'=>$row->id));   
                $this->subadmin_model->delete('property_notes',array('id'=>$row->id));  
                $this->subadmin_model->delete('review',array('property_id'=>$row->id));  
                $pr_nb = $this->subadmin_model->get_row('pr_nb_relation',array('pr_nb_id'=>$row->id,'parent_id'=>0)); 
                $this->subadmin_model->delete('pr_nb_relation',array('parent_id'=>$pr_nb->id));  
                $pr_gallery = $this->subadmin_model->get_result('pr_gallery',array('pr_id'=>$row->id)); 
           	    if(!empty($pr_gallery))
           	    {
           	    	foreach ($pr_gallery as $value)
           	    	{
                       @unlink('.assets/uploads/property/'.$value->image_file);
           	    	}
           	    }
           	  }

           }

		  $this->subadmin_model->update('properties',array('is_delete'=>1,'status'=>0),array('user_id'=>$mem_id));
		/*Properties of this user Ends*/
		
		/*Bookings of this user starts*/
		$book_result = $this->subadmin_model->get_result('bookings',array('owner_id'=>$mem_id));
         if(!empty($book_result))
         {
         	foreach ($book_result as $value)
         	{
               $this->subadmin_model->delete('booking_notes',array('id'=>$value->id));
               $this->subadmin_model->delete('booking_email_record',array('id'=>$value->id));
               
         	}
         }

		$b_result = $this->subadmin_model->get_result('bookings',array('customer_id'=>$mem_id));
         if(!empty($b_result))
         {
         	foreach ($b_result as $value)
         	{
               $this->subadmin_model->delete('booking_notes',array('id'=>$value->id));
         	}
         }
		/*Bookings of this user Ends*/

		/*payments of this user starts*/
		$pay_result = $this->subadmin_model->get_result('payments',array('user_id'=>$mem_id));
         if(!empty($pay_result))
         {
         	foreach ($pay_result as $value)
         	{
               $this->subadmin_model->delete('payment_notes',array('id'=>$value->id));

         	}
         }

		$p_result = $this->subadmin_model->get_result('payments',array('owner_id'=>$mem_id));
         if(!empty($p_result))
         {
         	foreach ($p_result as $value)
         	{
               $this->subadmin_model->delete('payment_notes',array('id'=>$value->id));
         	}
         }
		/*payments of this user Ends*/

		/*Favorites of this user starts*/
		$favorites_category = $this->subadmin_model->get_result('favorites_category',array('user_id'=>$mem_id));
         if(!empty($favorites_category))
         {
         	foreach ($favorites_category as $value)
         	{
               $this->subadmin_model->delete('favorites_property',array('favorites_category_id'=>$value->id));

         	}
         }
	    	$this->subadmin_model->delete('favorites_category',array('user_id'=>$mem_id));
		/*Favorites of this user starts*/
		
		/*languages of this user starts*/
    		$this->subadmin_model->delete('languages',array('user_id'=>$mem_id));
		/*languages of this user Ends*/

		$this->session->set_flashdata('success_msg',"member has been deleted successfully.");
		redirect('subadmin/members');
	}
	public function ban_user($mem_id='')

	{

		$this->check_login();

		if(empty($mem_id)) redirect('subadmin/members');

		$data['member'] = $this->subadmin_model->get_row('users', array('id' => $mem_id));

		if($data['member']->user_status == 0){

			$array = array('user_status' => 1, );

			$this->subadmin_model->update('users',$array,array('id' => $mem_id));

			$this->session->set_flashdata('success_msg',"member status has been updated successfully.");

			redirect('subadmin/members');

		}

		else{

			$array = array('user_status' =>0, );

			$this->subadmin_model->update('users',$array,array('id' => $mem_id));

			$this->session->set_flashdata('success_msg',"member status has been updated successfully.");

			redirect('subadmin/members');

		}



	}





     /*Member Note work starts from here*/

	public function member_notes($member_id='')

	{

		$this->check_login();

		if(empty($member_id)) redirect('subadmin/members'); 

		$data['notes']=$this->subadmin_model->get_result('member_notes',array('member_id' =>$member_id));

		$data['id'] = $member_id;

		$data['template'] = 'subadmin/member_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function add_member_note($member_id='')

	{

		$this->check_login(); 

		if(empty($member_id)) redirect('subadmin/members');

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){			

			$data=array(

						'title'=>$this->input->post('title'),				

						'description'=>$this->input->post('description'),

						'member_id'=>$member_id,

						'created' => date('Y-m-d H:i:s')		

				       );	

			$this->subadmin_model->insert('member_notes',$data);		

			$this->session->set_flashdata('success_msg'," Successfully member Note Added.");

			redirect('subadmin/member_notes/'.$member_id);

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Faqs' => '/subadmin/faqs', 'Add Faq' => '/subadmin/add_faq'));

		$data['template'] = 'subadmin/add_member_note';

        $this->load->view('templates/subadmin_template', $data);		



	}



	public function view_member_note($member_id='')

	{

		$this->check_login();

		if(empty($member_id)) redirect('subadmin/members'); 

		$data['notes']=$this->subadmin_model->get_row('member_notes',array('id' =>$member_id));

		if(empty($data['notes'])){

			redirect('subadmin/members');

		}

		$data['template'] = 'subadmin/view_member_note';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_member_note($id="",$member_id="")

	{

		if($id=="")

		{

			redirect('subadmin/members');

		}

		$this->subadmin_model->delete('member_notes',array('id'=>$id));

		$this->session->set_flashdata('success_msg'," member Note has been deleted successfully.");

		redirect('subadmin/member_notes/'.$member_id);

	}

     /*Member Note work ends from here*/







	public function change_password($mem_id='')

	{

		$this->check_login(); 

		if(empty($mem_id)) redirect('subadmin/members');

		$data['member'] = $this->subadmin_model->get_row('users', array('id' => $mem_id));



		$this->form_validation->set_rules('password', 'password', 'required|min_length[8]');			

		$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|matches[password]');			

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){	

			$password   = $this->input->post('password');

			$name = $data['member']->first_name.' '.$data['member']->last_name;

			$user_email = $data['member']->user_email;



			$data=array('password'=>sha1(trim($password)));

			// $data=array('password'=>$password);



			$status = $this->send_change_password_mail($name,$user_email,$password);

			if($status==FALSE){

				$this->session->set_flashdata('success_msg',"Change Password process Fail.");

				redirect('subadmin/members');

			}

			$this->subadmin_model->update('users',$data,array('id' => $mem_id));		

			$this->session->set_flashdata('success_msg',"Member Password has been Changed Successfully.");

			redirect('subadmin/members');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Faqs' => '/subadmin/faqs', 'Add Faq' => '/subadmin/add_faq'));

		$data['template'] = 'subadmin/change_password';

        $this->load->view('templates/subadmin_template', $data);		

		

	}

	public function send_change_password_mail($name,$email_to,$password){

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$subject = 'Password Change Notification';	// Subject for email

		$from = array('no-reply@bnbclone.com' =>'BnbClone');	// From email in array form	

		$to = array(

			 $email_to,

		);

		$html = '';

		$html .= '<html>

					<body>

					  <h4>Dear Member ' .$name. '</h4>';

		$html .= '<p>This is Notifications for you from the Bnbclone </p>';

		$html .='<p>your password has been changed for some security purpose </p>';

		$html .= '<strong>Now Your New Password Is:</strong>';

		$html .=	'<p>';

		$html .=  $password;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';

		$this->smtp_email->sendEmail($from, $to, $subject, $html);

		

		return TRUE;



	}

	



	public function bookings($sort='-',$offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->booking==2)
        {
             redirect('subadmin/dashboard');
        }


		$this->check_login();

		$name='';

		$post="";

		if($_POST)

		{

			$sort  = $this->input->post('sort');

			$name  = $this->input->post('name');

			$post  = $_POST;

		}

		

		$limit=10;

		$data['bookings']=$this->subadmin_model->bookings($limit,$offset,$sort,$name,$post);



		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/bookings/'.$sort;

		$config['total_rows'] = $this->subadmin_model->bookings(0,0,$sort,$name,$post);

		$config['per_page'] = 10;

		$config['num_links'] =3;

		$config['uri_segment'] =4;



		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];



		$data['template'] = 'subadmin/bookings';

        $this->load->view('templates/subadmin_template', $data);		

        

	}



	public function bookings_edit($id='')

	{

		$this->check_login();

		if(empty($id)) redirect('subadmin/bookings');

		$name    = $this->input->post('booking_name');

		$email   = $this->input->post('email');

		$address = $this->input->post('address');

		$contact = $this->input->post('contact');

		$array = array(

 					'booking_name' =>$this->input->post('booking_name') , 

				 	'email' =>$this->input->post('email') ,

				 	'address' =>$this->input->post('address') ,

				 	'contact' =>$this->input->post('contact') 

					);

		$status = $this->subadmin_model->update('bookings',$array,array('id' =>$id));

		if($status){



			$this->session->set_flashdata('success_msg','Booking has been Updated');

			redirect('subadmin/bookings');

		}





	}

	public function check_date_of_checkin($date){

		$error_msg = '';

		if ($this->input->post('check_in') == '') {

			$error_msg.='Please select checkin date. <br>';

			// $this->form_validation->set_message('check_date_of_checkin', 'Please select checkin date.');

		 	// return FALSE;

		}



		if ($this->input->post('check_out') == '') {

			$error_msg.='Please select checkout date. <br>';

			$this->form_validation->set_message('check_date_of_checkin', $error_msg);

		 	return FALSE;

		}



		$check_in = strtotime($this->input->post('check_in'));

		$check_out = strtotime($this->input->post('check_out'));

		$date1 = strtotime($date[0]);

		$today_date = strtotime(date('d-m-Y'));

		// echo '<br> Today: '.date('d-m-Y');

		// echo '<br> checkin: '.$this->input->post('check_in');

		// echo '<br> checkout: '.$this->input->post('check_out');

		// die();

		if($today_date > $check_in){

			$error_msg.='Invaild checkin date. <br>';

			// $this->form_validation->set_message('check_date_of_checkin', 'Please select The vaild checkin date.');

		 // 	   return FALSE;

		}



		if($today_date > $check_out){

			$error_msg.='Invaild checkout date. <br>';

			$this->form_validation->set_message('check_date_of_checkin', $error_msg);

		 	return FALSE;

		}



		if (!empty($error_msg)) {

			$this->form_validation->set_message('check_date_of_checkin', $error_msg);

		 	return FALSE;

		}



		if($check_in > $check_out){

			$this->form_validation->set_message('check_date_of_checkin', 'Please select The vaild checkin ,checkout dates.');

		 	   return FALSE;

		}else{

			return TRUE;

		}

	}

	



	public function add_booking()
	{

		$this->check_login(); 

		$this->form_validation->set_rules('guest_name', 'Guest Name', 'required');

		$this->form_validation->set_rules('guest_email', 'Guest Email', 'required|valid_email');		

		$this->form_validation->set_rules('host_email', 'Host Email', 'required|valid_email');

		// $this->form_validation->set_rules('phone', 'phone', 'required');

		// $this->form_validation->set_rules('address', 'Address', 'required');

        $this->form_validation->set_rules('check_in', 'Chech In', 'required');

        $this->form_validation->set_rules('check_out', 'Chech out', 'required|callback_check_date_range');

        $this->form_validation->set_rules('property_id', 'Properties Title', 'required');

		$this->form_validation->set_rules('total_amount', 'Total Amount', 'required|numeric');			

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE)

		{
		
		    $ownerinfo = $this->subadmin_model->get_row('users', array('user_email'=>$this->input->post('host_email')));

		    if(!$ownerinfo){

		    	$this->session->set_flashdata('error_msg', 'Invalid Owner Details');

		    	redirect('subadmin/add_booking');

		    }


		    $customer  =  $this->subadmin_model->get_row('users', array('id' => $this->input->post('guest_name')));

            $cus_name  = $customer->first_name." ".$customer->last_name;

            $currency = $this->session->userdata('currency');

 			$data=array(

						'customer_id' 	=>$this->input->post('guest_name'),	

						'customer_name' =>$cus_name,				

						'pr_id'      	=>$this->input->post('property_id'),

						'owner_id'      =>$ownerinfo->id,				

						// 'contact'    	=>$this->input->post('phone'),				

						'check_in'   	=>date('m/d/Y',strtotime($this->input->post('check_in'))),									

						'check_out'  	=>date('m/d/Y',strtotime($this->input->post('check_out'))),													

						'total_amount' 	=>$this->input->post('total_amount'),

						'booking_currency_type' =>$currency,							

						'created'   	=>date('Y-m-d h:i:s'),	

						'link_expired' => 0,

						'booking_by_subadmin' => 1,								
  
					   );

 			if(isset($_POST['security_deposit']) && !empty($_POST['security_deposit']))
 			{
 				$data['security_deposit'] = $_POST['security_deposit'];
 			}

			$id = $this->subadmin_model->insert('bookings',$data);

			$property_id = $this->input->post('property_id');
			$check_in = $this->input->post('check_in');
			$check_out = $this->input->post('check_out');


            $this->addBookingToCal($property_id,$check_in,$check_out);       

			if ($id) 

			{

				$s_subject = 'Booking notification';

				$message = 'Hello '.$customer->first_name.':<br>';

				$message .= 'Your booking is added successfully by subadmin:<br>';
				
				$message .= 'Your Check In date is  : '.date('d-F-Y',strtotime($check_in)).'<br>';

				$message .= 'Your Check Out date is  : '.date('d-F-Y',strtotime($check_out)).'<br>';

				$message .= 'Please click on the following link For Payment <br>';

				$message .= '<a href="'.base_url().'properties/host_payment/'.$id.'"> Click Here...</a><br>';

				$name = $customer->first_name;

				$email = $this->input->post('guest_email');

				$this->send_booking_mail($name,$email,$s_subject,$message);
				$this->add_to_notification($name,$email,$s_subject,$message);

				$this->session->set_flashdata('success_msg',"Successfully Added Booking. ");

				redirect('subadmin/bookings');
			}

			else
			{
				$this->session->set_flashdata('error_msg'," Booking failed. ");

				redirect('subadmin/bookings');
			}

		}

		$data['template'] = 'subadmin/add_booking';

        $this->load->view('templates/subadmin_template', $data);		

	}

	public function add_to_notification($name,$email,$s_subject,$message)
	{
		$data=array(
					'name' 		=>		$name,
					'email'     =>		$email,
					'subject'	=>		$s_subject,
					'message'   =>		$message,
					'created'   =>		date('Y-m-d H:i:s'),
					'updated'  	=>		date('Y-m-d H:i:s')
				   );
		$this->subadmin_model->insert('notifications', $data);
	}




	public function get_property_detail($pr_id)
	{
		$this->check_login();
        $currency = $this->session->userdata('currency');
        $row = get_property_detail($pr_id);
        $security_deposit =0;
        $cleaning_fee =0;
        if($row->security_deposit!=0)
        {
          $security_deposit = convertCurrency($row->security_deposit,$currency,$row->currency_type);
        }
        if($row->cleaning_fee!=0)
        {
          $cleaning_fee = convertCurrency($row->cleaning_fee,$currency,$row->currency_type);
        }
        
        $data = array('security_deposit'=>$security_deposit,
        	          'cleaning_fee'=>$cleaning_fee,
        	          );
        echo json_encode($data);
	}  



	public function edit_booking($book_id="")

	{

		$this->check_login(); 

		if(empty($book_id)) redirect('subadmin/bookings');

		$data['booking'] = $this->subadmin_model->get_row('bookings', array('id' => $book_id));

		if($_POST)

		{

			$this->form_validation->set_rules('guest_name', 'Guest Name', 'required');

			$this->form_validation->set_rules('email', 'Guest Email', 'required|valid_email');		

			$this->form_validation->set_rules('host_email', 'Host Email', 'required');

			$this->form_validation->set_rules('phone', 'phone', 'required');

			$this->form_validation->set_rules('address', 'Address', 'required');

        	$this->form_validation->set_rules('check_in', 'Chech In', 'required');

        	$this->form_validation->set_rules('check_out', 'Chech Out', 'required|callback_check_date_range');

	        if($_POST['property_id']!=''){

	        	$this->form_validation->set_rules('property_id', 'Properties Title', 'required');

			}

			$this->form_validation->set_rules('due_amount', 'Due Amount', 'required');			

			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

			if ($this->form_validation->run() == TRUE){

		  		$digits='';

		  		$amountOfDigits = 7;

	   		 	$numbers = range(0,9);

	   			shuffle($numbers);

				for($i = 0;$i < $amountOfDigits;$i++){

				    $digits .= $numbers[$i];

				}

			    $user_details =  $this->subadmin_model->get_row('users', array('id' => $this->input->post('user')));



		    	$check_in  = $this->input->post('check_in');

		    	$check_out = $this->input->post('check_out');

	 			$data=array(

							'guest_name' 	=>$this->input->post('guest_name'),

							'email'      	=>$this->input->post('email'),				

							'contact'    	=>$this->input->post('phone'),				

							'address'    	=>$this->input->post('address'),									

							'check_in'   	=>date('Y-m-d',strtotime($check_in)),									

							'check_out'  	=>date('Y-m-d',strtotime($check_out)),													

							'due_amount' 	=>$this->input->post('due_amount'),								

							'created'    	=>date('Y-m-d H:i:s')		

						   );

	 	

	 			if($_POST['property_id']!='')

	 			{

	 				$data['pr_id'] = $this->input->post('property_id');

				}

				$this->subadmin_model->update('bookings',$data,array('id' =>$book_id));		

				$this->session->set_flashdata('success_msg',"Successfully updated Booking. ");

				redirect('subadmin/bookings');

			}

		}

		$data['template'] = 'subadmin/edit_booking';

        $this->load->view('templates/subadmin_template', $data);		

	}





	public function check_date_range($out)

	{

        if(!$this->input->post('check_in')){ return true; }

        $in = $this->input->post('check_in');

        $intime  = strtotime($in);

        $outtime = strtotime($out); 

		if($intime > $outtime)

		{

        	$this->form_validation->set_message('check_date_range','Invalid date range.');

        	return false;

		}

		else{ return true; }

	}



	public function send_booking_mail($name,$email_to,$s_subject,$message){

		$this->load->library('smtp_lib/smtp_email');

		$subject = 'Booking Added Notification';	// Subject for email

		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form	

		$to = array(

			 $email_to,

		);

		$html = "<em><strong>Hello ".$name." !</strong></em> <br>

				<p><strong>Subject - ".$s_subject."</strong></p>

				<p><strong>Message - ".$message."</strong></p>";

		$this->smtp_email->sendEmail($from, $to, $subject, $html);

	}




	public function delete_booking($bookings_id){

		$this->check_login();

		if(empty($bookings_id)) redirect('subadmin/bookings');		

		$booking_info = $this->subadmin_model->get_row('bookings',array('id'=>$bookings_id));
	    $check_in = $booking_info->check_in;
	    $check_out = $booking_info->check_out;
	    $pr_id = $booking_info->pr_id;
	    $this->subadmin_model->make_event_available($check_in, $check_out,$pr_id);
		$this->subadmin_model->update('bookings',array('is_delete_by_subadmin'=>1),array('id'=>$bookings_id));
		$this->subadmin_model->update('payments',array('is_delete'=>1),array('booking_id'=>$bookings_id));
		$this->session->set_flashdata('success_msg',"booking and payment of this booking has been deleted successfully.");
		redirect('subadmin/bookings');

	}





	public function host_profile($user_id=''){

		$this->check_login();

		if(empty($user_id)) redirect('subadmin/bookings');		

		$data['user'] = $this->subadmin_model->get_row('users',array('id'=>$user_id));

		

		$data['template'] = 'subadmin/host_profile';

        $this->load->view('templates/subadmin_template', $data);

	}





	public function booking_detail($book_id=''){

		$this->check_login();

		if(empty($book_id)) redirect('subadmin/bookings');		

		$data['booking'] = $this->subadmin_model->get_row('bookings',array('id'=>$book_id));

		

		$data['template'] = 'subadmin/booking_detail';

        $this->load->view('templates/subadmin_template', $data);

	}







	public function user_property(){

	 	$this->check_login();

	 	$resp ='';

		$user_email     =  $this->input->post('user_email');

		$user_details 	=  $this->subadmin_model->get_row('users', array('user_email' => $user_email)); 



		if(empty($user_details)) {

			echo 'NO Record Found!!!!';

			die();

		}



		$properties  	=  $this->subadmin_model->get_result('properties',array('user_id' =>$user_details->id,'status'=>1));





		if(!empty($properties)){

			foreach($properties as $row){

					$resp.='<div class="unique_feature">';

		            $resp.='<input style="float:left" onclick="get_property_detail('.$row->id.');" type="radio" name="property_id" value="'.$row->id.'" required >';

		            $resp.= '&nbsp;&nbsp;<strong style="margin-left:1%">'.$row->title.'</strong>';

		            $resp.='</div>';

		    }

		    

		    echo  $resp;        

		}

		else

		{

			echo 'NO Record Found';

		}

 	

 	}

 	public function user_info()

 	{

 		$this->check_login();

 		$user_email    = $this->input->post('user_email');

 		$user_info = $this->subadmin_model->get_row('users',array('user_email' => $user_email));

 		if(!empty($user_info)){

 		 	echo  json_encode($user_info);

 	    }

 	    else{

 	    	echo "No Record Found";

 	    }

 	}



 	public function user_information()

 	{

 		$this->check_login();

 		$user_id    = $this->input->post('user_id');



 		$user_info = $this->subadmin_model->get_row('users',array('id' => $user_id));

 		if(!empty($user_info)){

 		 echo  json_encode($user_info);

 	    }

 	    else{

 	    	echo "No Record Found";

 	    }

 	}



	public function booking_email($book_id='')

	{

		$this->check_login();

		if (empty($book_id)) {

			$this->session->set_flashdata('error_msg' , "Email can't be send.");

			redirect('subadmin/bookings');

		}

		$data['booking'] = $this->subadmin_model->get_row('bookings', array('id' => $book_id));

		// print_r($data['member']);

		// die();

		$data['ownerinfo'] = $this->subadmin_model->get_row('users', array('id'=>$data['booking']->owner_id));

		$owneremail = $data['ownerinfo']->user_email;

		$this->form_validation->set_rules('booking_name', 'Booker Name', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		$this->form_validation->set_rules('subject', 'Subject', 'required');	

		$this->form_validation->set_rules('message', 'Message', 'required');

		if ($this->form_validation->run() == TRUE){

			$name 	 =  $this->input->post('member_name');

			$email	 =	$this->input->post('email');

			$subject =	$this->input->post('subject');

			$message =	$this->input->post('message');

			$array = array(

					'booking_id' =>$book_id ,

				    'email' 	=>$email,

				    'subject'=>$subject,

				    'message'=>$message,

				    'contact'=>$data['booking']->contact,

				    // 'address'=>$data['booking']->address,

			  	  );

			if($_FILES['userfile']['name']!=''){

				$attach ='';

				$file=$_FILES['userfile']['name'];

				$attach=$_FILES['userfile']['tmp_name'];

				

				$config['upload_path'] = './assets/uploads/email_attachment/';

				$config['allowed_types'] = 'gif|jpg|png|pdf|txt';

				$config['max_size']	= '20000';

				$this->load->library('upload', $config);											

				if(! $this->upload->do_upload('userfile'))

				{

					$this->session->set_flashdata('image_error', $this->upload->display_errors());

					redirect('subadmin/booking_email/'.$book_id);

				}else{

					$array['attachment'] = $_FILES['userfile']['name'];

				}

			}else{

				$attach = "";

			}			

			



			$status = $this->send_booking_email_message($name,$email,$subject,$message,$attach,$file,$owneremail);

			$this->subadmin_model->insert('booking_email_record',$array);

			if($status){

				$this->session->set_flashdata('success_msg',"Email  has been sent successfully.");

				redirect('subadmin/bookings');

			}		

		}	

		$data['template'] = 'subadmin/booking_email';

        $this->load->view('templates/subadmin_template',$data);	



	}

	public function send_booking_email_message($name='',$email='',$subject='',$message='' ,$attach='',$file='',$owneremail="")

	{

		 // echo $attach;

		 // die();

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form

		$to = array($email,$owneremail);

		$html = '';

		$html .= '<html>

					<body>

					  <h3>Dear'.$name.'</h3>';

		$html .= '<p>This is Notifications for you form the Bnbclone on your Booking </p>';

		$html .= '<strong>'.$subject.'</strong>';

		$html .=	'<p>';

		$html .=  $message;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';

	

		$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html,$attach,$file);

		if($is_fail){

		return FALSE;

		

		}

		else{

			return TRUE;

		}

	}

	public function booking_status($bok_id='')

	{

		$this->check_login();

		if(empty($bok_id)) redirect('subadmin/bookings');

		$data['booking'] = $this->subadmin_model->get_row('bookings', array('id' => $bok_id));

		if($data['booking']->status == 0){

			$array = array('status' => 1, );

			$this->subadmin_model->update('bookings',$array,array('id' => $bok_id));

			$this->session->set_flashdata('success_msg',"booking status has been updated successfully.");

			redirect('subadmin/bookings');

		}

		else{

			$array = array('status' =>0, );

			$this->subadmin_model->update('bookings',$array,array('id' => $bok_id));

			$this->session->set_flashdata('success_msg',"booking status has been updated successfully.");

			redirect('subadmin/bookings');

		}



	}

	public function cancel_booking($bok_id='')

	{

		$this->check_login();

		if(empty($bok_id)) redirect('subadmin/bookings');

		$data['booking'] = $this->subadmin_model->get_row('bookings', array('id' => $bok_id));

		if($data['booking']->status == 0){

			$array = array('status' => 2, );

			$this->subadmin_model->update('bookings',$array,array('id' => $bok_id));

			$this->session->set_flashdata('success_msg',"booking status has been updated successfully.");

			redirect('subadmin/bookings');

		}

		else{

			$array = array('status' =>0, );

			$this->subadmin_model->update('bookings',$array,array('id' => $bok_id));

			$this->session->set_flashdata('success_msg',"booking status has been updated successfully.");

			redirect('subadmin/bookings');

		}



	}

	public function booking_email_records($offset=0)

	{

		$this->check_login(); 

		$limit=10;

		$data['history']=$this->subadmin_model->get_pagination_result('booking_email_record',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/booking_email_record/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('booking_email_record',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();	

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard', 'Faqs' => '/subadmin/faqs'));

		$data['template'] = 'subadmin/booking_email_records';

        $this->load->view('templates/subadmin_template', $data);		

	}

	public function delete_booking_record($id=''){

		$this->check_login();

		if(empty($id)) redirect('subadmin/booking_email_records');		

		$this->subadmin_model->delete('booking_email_record',array('id'=>$id));

		$this->session->set_flashdata('success_msg',"Booking Record has been deleted successfully.");

		redirect('subadmin/booking_email_records');

	}

	public function view_booking_record($id=''){

		$this->check_login();

		if(empty($id)) redirect('subadmin/booking_email_records');		

		$data['book_record'] = $this->subadmin_model->get_row('booking_email_record', array('id' => $id));

		

		$data['template'] = 'subadmin/view_booking_email_record';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_booking_note($book_id='')

	{

		$this->check_login(); 

		if(empty($book_id)) redirect('subadmin/bookings');

		$data['booking'] = $this->subadmin_model->get_row('bookings', array('id' => $book_id));

		

		$this->form_validation->set_rules('booking_name', 'Booking Name', 'required');			

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('note', 'Note', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){			

			$data=array(

				'title'=>$this->input->post('title'),				

				'note'=>$this->input->post('note'),

				'booking_id'=>$book_id,

				'created' => date('Y-m-d H:i:s')		

				);	

			$this->subadmin_model->insert('booking_notes',$data);		

			$this->session->set_flashdata('success_msg',"Successfully Booking Note Added.");

			redirect('subadmin/booking_notes/'.$book_id);

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Faqs' => '/subadmin/faqs', 'Add Faq' => '/subadmin/add_faq'));

		$data['template'] = 'subadmin/add_note';

        $this->load->view('templates/subadmin_template', $data);		



	}

	public function booking_notes($book_id='')

	{

		$this->check_login();

		if(empty($book_id)) redirect('subadmin/bookings'); 

		$data['notes']=$this->subadmin_model->get_result('booking_notes',array('booking_id' =>$book_id));

		$data['id']=$book_id;

		$data['template'] = 'subadmin/booking_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}

	public function view_notes($book_note_id='')

	{

		$this->check_login();

		if(empty($book_note_id)) redirect('subadmin/bookings'); 

		$data['notes']=$this->subadmin_model->get_row('booking_notes',array('id' =>$book_note_id));

		if(empty($data['notes'])){

			redirect('subadmin/bookings');

		}

		$data['template'] = 'subadmin/view_note';

        $this->load->view('templates/subadmin_template', $data);		

	}

	public function delete_note($book_note_id=''){

		$this->check_login();

		if(empty($book_note_id)) redirect('subadmin/bookings');		

		$this->subadmin_model->delete('booking_notes',array('id'=>$book_note_id));

		$this->session->set_flashdata('success_msg',"Booking Note has been deleted successfully.");

		redirect('subadmin/booking_notes/'.$book_note_id);

	}

	



	public function send_mail()

	{

		$this->load->library('smtp_lib/smtp_email');

		$email       = $this->input->post('email');

		$subject 	 = $this->input->post('subject');	// Subject for email

		$description = $this->input->post('description');

		

		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form

		$to = array($email,);

		$html = '';

		$html .= '<html>

					<body>

					  <h3>DEAR USER</h3>';

		$html .=	'<p>';

		$html .=  $description;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';

	

		$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);

		if($is_fail){

		echo "ERROR :";

		print_r($is_fail);

		}

		else{

			echo "your Email has been sent successfully.";

		}

	}

	public function groups($offset=0){

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->groups==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['groups']=$this->subadmin_model->get_pagination_result('groups',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/groups/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('groups',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		//$data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Groups' => '/subadmin/groups'));



		$data['template'] = 'subadmin/groups';

        $this->load->view('templates/subadmin_template', $data);	

	}

	

	public function add_group(){

		$this->check_login();

		$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');

		$this->form_validation->set_rules('group_description', 'Group Description', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'group_name' =>$this->input->post('group_name'),

						  'group_description' =>$this->input->post('group_description'),

						  'created'=>date('Y-m-d H:i:s') 

						  );

			if($_FILES['banner']['name']!=''){

				$config['upload_path'] = './assets/uploads/banner/';

				$config['allowed_types'] = 'gif|jpg|png';

				$config['max_size']	= '10000';

				$this->load->library('upload', $config);				

							

				if(! $this->upload->do_upload('banner'))

				{

					$this->session->set_flashdata('image_error', $this->upload->display_errors());

					redirect('subadmin/add_group/');

				}else{

					$array['banner']=$_FILES['banner']['name'];

				}

			}



			

			$this->subadmin_model->insert('groups',$array);

			$this->session->set_flashdata('success_msg',"group has been added successfully.");

			redirect('subadmin/groups');

		}

		$data['template'] = 'subadmin/add_group';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function edit_group($group_id){

		$this->check_login();

		if(empty($group_id)) redirect('subadmin/groups');

		$data['group'] = $this->subadmin_model->get_row('groups',array('id'=>$group_id));

		$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');

		$this->form_validation->set_rules('group_description', 'Group Description', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'group_name' =>$this->input->post('group_name'),

						  'group_description' =>$this->input->post('group_description'),

						  'modified'=>date('Y-m-d H:i:s') 

						  );

			

			if($_FILES['banner']['name']!=''){



				if($data['group']->banner !=''){

					$path = './assets/uploads/banner/';

					$file = $data['group']->banner;

					if(!empty($file)){

						@unlink($path.$file);

					}

				}

				$config['upload_path'] = './assets/uploads/banner/';

				$config['allowed_types'] = 'gif|jpg|png';

				$config['max_size']	= '10000';

				$this->load->library('upload', $config);				

							

				if(! $this->upload->do_upload('banner'))

				{

					$this->session->set_flashdata('image_error', $this->upload->display_errors());

					redirect('subadmin/edit_group/');

				}else{

					$array['banner']=$_FILES['banner']['name'];

				}

			}



			$this->subadmin_model->update('groups',$array,array('id'=>$group_id));

			$this->session->set_flashdata('success_msg',"group has been updated successfully.");

			redirect('subadmin/groups');

		}

		$data['template'] = 'subadmin/edit_group';

        $this->load->view('templates/subadmin_template', $data);

	}



    public function delete_group($group_id){

		$this->check_login();

		if(empty($group_id)) redirect('subadmin/groups');		

		$row = $this->subadmin_model->get_row('groups',array('id'=>$group_id));

		$path = './assets/uploads/banner/';

		$file = $row->banner;

			if(!empty($file))

			{

			  @unlink($path.$file);

	        }

		$this->subadmin_model->delete('groups',array('id'=>$group_id));

		$this->subadmin_model->delete('group_members',array('group_id'=>$group_id));

		$this->subadmin_model->delete('group_members_properties',array('group_id'=>$group_id));

		$this->session->set_flashdata('success_msg',"group has been deleted successfully.");

			redirect('subadmin/groups');

	}

	

	public function help($offset=0){

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->help==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login(); 

		

		$limit=10;

		$data['faqs']=$this->subadmin_model->get_pagination_result('faqs',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/faqs/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('faqs',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();	

		$data['template'] = 'subadmin/faqs';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function add_help(){

		$this->check_login(); 

		$this->form_validation->set_rules('question', 'question', 'required');			

		$this->form_validation->set_rules('title', 'Title', 'required');

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE)

		{			

			$data=array(

				'question'=>$this->input->post('question'),				

				'title'=>$this->input->post('title'),

				'description'=>$this->input->post('description'),

				'faq_cat_id'=>$this->input->post('faq_category'),

				'created' => date('Y-m-d H:i:s')		

				);

			if($_FILES['faq_file']['name'] !="")

			{

				$data['image'] = $this->do_core_upload('faq_file','./assets/uploads/faq/');

				if(!$data['image']){

					$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');

					redirect('subadmin/add_faq');

				}

				$data['video'] = "";				

			}

			if($_POST['video'])

			{

				$data['video'] = $_POST['video'];				

				$data['image'] = "";				

			}

			$this->subadmin_model->insert('faqs',$data);		

			$this->session->set_flashdata('success_msg',"Faqs has been added successfully.");

			redirect('subadmin/help');

		}

		$data['template'] = 'subadmin/add_faqs';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function edit_help($id=''){

		$this->check_login();

		if(empty($id)) redirect('subadmin/help'); 

		$data['faqs'] = $this->subadmin_model->get_row('faqs', array('id'=>$id));

		$this->form_validation->set_rules('question', 'question', 'required');			

		$this->form_validation->set_rules('title', 'Title', 'required');

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE)

		{			

			$array=array(

						'question' => $this->input->post('question'),				

						'title'   => $this->input->post('title'),	

						'description'   => $this->input->post('description'),	

         				'faq_cat_id'=>$this->input->post('faq_category'),

						'modified' => date('Y-m-d H:i:s')		

					   );



			if($_FILES['faq_file']['name'] !="")

			{

				$array['image'] = $this->do_core_upload('faq_file','./assets/uploads/faq/');

				if(!$array['image'])

				{

					$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');

					redirect('subadmin/edit_faq');

				}

				else

				{

					$path = './assets/uploads/faq/';

					@unlink($path.$data['faqs']->image); 

				    $array['video'] = "";

				}

			}

			elseif($_POST['video'])

			{

				$array['video'] = $_POST['video'];

				$path = './assets/uploads/faq/';

				@unlink($path.$data['faqs']->image); 

				$array['image'] = "";

			}



				$this->subadmin_model->update('faqs',$array, array('id'=>$id));		

				$this->session->set_flashdata('success_msg',"Faqs has been edited successfully");

				redirect('subadmin/help');



		}

		$data['template'] = 'subadmin/edit_faqs';

        $this->load->view('templates/subadmin_template', $data);

        

	}



	public function delete_faq($faq_id=""){

		$this->check_login();

		if(empty($faq_id)) redirect('subadmin/faqs');	

		$this->subadmin_model->delete('faqs',array('id'=>$faq_id));

		$this->session->set_flashdata('success_msg',"Faqs has been deleted successfully");

		redirect('subadmin/faqs');

	}



    /*Faqs Category starts*/

	public function help_category($offset=0)

	{

		$this->check_login(); 

		$limit=10;

		$data['faqs_category']=$this->subadmin_model->get_pagination_result('faqs_category',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/faqs_category/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('faqs_category',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['offset'] = $offset;

		$data['template'] = 'subadmin/faqs_category';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function add_help_category(){

		$this->check_login(); 

		

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		

		if ($this->form_validation->run() == TRUE){		

		$response = $this->subadmin_model->get_row('faqs_category',array('title'=>$this->input->post('title')));

		if(!empty($response))

		{

			$this->session->set_flashdata('error_msg',"This Category is already exist.");

			redirect('subadmin/add_help_category');

		}

			$data=array(

						'title'=>$this->input->post('title'),				

						'description'=>$this->input->post('description'),				

						'created' => date('Y-m-d H:i:s'),

						'status'  => $this->input->post('status'),

						);	



			$this->subadmin_model->insert('faqs_category',$data);		

			$this->session->set_flashdata('success_msg',"Help Category has been added succesfully.");

			redirect('subadmin/help_category');

		}

		$data['template'] = 'subadmin/add_faqs_category';

	    $this->load->view('templates/subadmin_template', $data);		

	}



	public function faqs_category_status($id='')

	{

		$this->check_login();

		if(empty($id)) redirect('subadmin/faqs_category');

		$data['faqs_category'] = $this->subadmin_model->get_row('faqs_category', array('id' => $id));

		if($data['faqs_category']->status == 0){

			$array = array('status' =>1);

			$this->subadmin_model->update('faqs_category',$array,array('id' => $id));

			$this->session->set_flashdata('success_msg',"Help Category status has been updated successfully.");

			redirect('subadmin/faqs_category');

		}

		else{

			$array = array('status' =>0);

			$this->subadmin_model->update('faqs_category',$array,array('id' => $id));

			$this->session->set_flashdata('success_msg',"Help Category status has been updated successfully.");

			redirect('subadmin/faqs_category');

		}



	}



	public function edit_help_category($id=""){

		$this->check_login(); 

		$data['faqs_category']= $this->subadmin_model->get_row('faqs_category',array('id'=>$id));

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){		

		$response = $this->subadmin_model->get_row('faqs_category',array('title'=>$this->input->post('title')));

			if(!empty($response))

			{

				if($response->id==$id)

				{

					$data=array(

								'title'=>$this->input->post('title'),				

								'description'=>$this->input->post('description'),				

								'updated' => date('Y-m-d H:i:s'),

								'status'  => $this->input->post('status'),

								);	



					$this->subadmin_model->update('faqs_category',$data,array('id'=>$id));		

					$this->session->set_flashdata('success_msg',"Help Category has been edited succesfully.");

					redirect('subadmin/help_category');

				}

				else

				{

					$this->session->set_flashdata('error_msg',"This Category is already exist.");

					redirect('subadmin/edit_help_category/'.$id);

				}

			}

			else

			{

				$data=array(

							'title'=>$this->input->post('title'),				

							'description'=>$this->input->post('description'),				

							'updated' => date('Y-m-d H:i:s'),

							'status'  => $this->input->post('status'),

							);	



				$this->subadmin_model->update('faqs_category',$data,array('id'=>$id));		

				$this->session->set_flashdata('success_msg',"Help Category has been edited succesfully.");

				redirect('subadmin/help_category');

			}

		}

		$data['template'] = 'subadmin/edit_faqs_category';

	    $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_faqs_category($id="")

	{

		if(empty($id)){redirect('subadmin/faqs_category');}

		$this->subadmin_model->delete('faqs_category',array('id'=>$id));

		$this->session->set_flashdata('success_msg',"Help Category has been deleted succesfully.");

		redirect('subadmin/faqs_category');

	}



	     /*Faqs Category Ends*/





	public function cancellation_policies($offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->policies==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login(); 

		

		$limit=10;

		$data['policies']=$this->subadmin_model->get_pagination_result('cancellation_policies',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/cancellation_policies/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('cancellation_policies',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();	

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard', 'Policies' => '/subadmin/cancellation_policies'));

		$data['template'] = 'subadmin/cancellation_policies';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_policy(){



		$this->check_login();

		$this->form_validation->set_rules('policy_title', 'Policy title', 'trim|required');

		$this->form_validation->set_rules('description', 'Policy Description', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'policy_title' =>$this->input->post('policy_title'),

						  'description' =>$this->input->post('description'),						  

						  'created'=>date('Y-m-d H:i:s') 

						  );

			$this->subadmin_model->insert('cancellation_policies',$array);

			$this->session->set_flashdata('success_msg',"Policy has been added successfully.");

			redirect('subadmin/cancellation_policies');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard', 'Policies' => '/subadmin/cancellation_policies','Add Policy' => '/subadmin/add_policy'));

		$data['template'] = 'subadmin/add_policy';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_policy($policy_id){

		$this->check_login();

		if(empty($policy_id)) redirect('subadmin/cancellation_policies');	

		$this->subadmin_model->delete('cancellation_policies',array('id'=>$policy_id));

		$this->session->set_flashdata('success_msg',"policy has been Deleted successfully.");

		redirect('subadmin/cancellation_policies');

	}

	public function edit_policy($policy_id=''){



		$this->check_login();

		if(empty($policy_id)) redirect('subadmin/cancellation_policies');

		$data['policy'] = $this->subadmin_model->get_row('cancellation_policies',array('id'=>$policy_id));

		$this->form_validation->set_rules('policy_title', 'Policy title', 'trim|required');

		$this->form_validation->set_rules('description', 'Policy Description', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'policy_title' =>$this->input->post('policy_title'),

						  'description' =>$this->input->post('description'),						  

						  'modified'=>date('Y-m-d H:i:s') 

						  );

			$this->subadmin_model->update('cancellation_policies',$array, array('id'=>$policy_id));

			$this->session->set_flashdata('success_msg',"Policy has been updated successfully.");

			redirect('subadmin/cancellation_policies');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard', 'Policies' => '/subadmin/cancellation_policies','Edit Policy' => '/subadmin/edit_policy'));

		$data['template'] = 'subadmin/edit_policy';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function room_types($offset=0){

		$this->check_login();

		$limit=20;

		$data['room_types']=$this->subadmin_model->get_pagination_result('room_types',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/room_types/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('room_types',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Room Types' => '/subadmin/room_types'));



		$data['template'] = 'subadmin/room_types';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_room_type(){



		$this->check_login();

		$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'room_type' =>$this->input->post('room_type'),

						  'created'=>date('Y-m-d H:i:s') 

						  );

			

			

			$this->subadmin_model->insert('room_types',$array);

			$this->session->set_flashdata('success_msg',"Room Type has been added successfully.");

			redirect('subadmin/room_types');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Room Types' => '/subadmin/room_types', 'Add Room Type' => '/subadmin/add_room_type'));



		$data['template'] = 'subadmin/add_room_type';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_room_type($rt_id){

		$this->check_login();

		if(empty($rt_id)) redirect('subadmin/room_types');	

		$this->subadmin_model->delete('room_types',array('id'=>$rt_id));

		$this->session->set_flashdata('success_msg',"Room Type has been Deleted successfully.");

		redirect('subadmin/room_types');

	}

	public function edit_room_type($rt_id){

		$this->check_login();

		if(empty($rt_id)) redirect('subadmin/room_types');

		$data['Type'] = $this->subadmin_model->get_row('room_types',array('id'=>$rt_id));

		$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'room_type' =>$this->input->post('room_type'),

						  'modified'=>date('Y-m-d H:i:s') 

						  );

			

			



			$this->subadmin_model->update('room_types',$array,array('id'=>$rt_id));

			$this->session->set_flashdata('success_msg',"Room Type has been updated successfully.");

			redirect('subadmin/room_types');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Room Types' => '/subadmin/room_types', 'Edit Room Type' => '/subadmin/edit_room_type'));



		$data['template'] = 'subadmin/edit_room_type';

        $this->load->view('templates/subadmin_template', $data);

	}

	public function amenities($offset=0){

		$this->check_login();

		$limit=20;

		$data['amenities']=$this->subadmin_model->get_pagination_result('amenities',$limit, $offset);

		$config= get_theme_pagination();

		$config['base_url'] = base_url().'subadmin/amenities/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('amenities',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Amenities' => '/subadmin/amenities'));



		$data['template'] = 'subadmin/amenities';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_amenity(){

		$this->check_login();

		$this->form_validation->set_rules('name', 'Amenity Name', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'name' =>$this->input->post('name'),

						  'created'=>date('Y-m-d H:i:s') 

						  );

			

			

			$this->subadmin_model->insert('amenities',$array);

			$this->session->set_flashdata('success_msg',"amenity has been added successfully.");

			redirect('subadmin/amenities');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Amenities' => '/subadmin/amenities', 'Add Amenity' => '/subadmin/add_amenity'));



		$data['template'] = 'subadmin/add_amenity';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_amenity($amenity_id){

		$this->check_login();

		if(empty($amenity_id)) redirect('subadmin/amenities');	

		$this->subadmin_model->delete('amenities',array('id'=>$amenity_id));

		$this->session->set_flashdata('success_msg',"amenity has been Deleted successfully.");

		redirect('subadmin/amenities');

	}

	public function edit_amenity($amenity_id){

		$this->check_login();

		if(empty($amenity_id)) redirect('subadmin/amenities'); 

		$data['amenity'] = $this->subadmin_model->get_row('amenities',array('id'=>$amenity_id));

		$this->form_validation->set_rules('name','Amenity Name','trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'name' =>$this->input->post('name'),

						  'modified'=>date('Y-m-d H:i:s') 

						  );

			

			



			$this->subadmin_model->update('amenities',$array,array('id'=>$amenity_id));

			$this->session->set_flashdata('success_msg',"Amenity has been updated successfully.");

			redirect('subadmin/amenities');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Amenities' => '/subadmin/amenities', 'Edit Amenity' => '/subadmin/edit_amenity'));



		$data['template'] = 'subadmin/edit_amenity';

        $this->load->view('templates/subadmin_template', $data);

	}

	public function property_types($offset=0){

		$this->check_login();

		$limit=20;

		$data['property_type']=$this->subadmin_model->get_pagination_result('property_types',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/property_types/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('property_types',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Property Types' => '/subadmin/property_types'));



		$data['template'] = 'subadmin/property_types';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_pr_type(){

		$this->check_login();

		$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'property_type' =>$this->input->post('property_type'),

						  'created'=>date('Y-m-d H:i:s') 

						  );

			

			

			$this->subadmin_model->insert('property_types',$array);

			$this->session->set_flashdata('success_msg',"Property Type has been added successfully.");

			redirect('subadmin/property_types');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Property Types' => '/subadmin/property_types', 'Add Property Types' => '/subadmin/add_pr_type'));



		$data['template'] = 'subadmin/add_pr_type';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_pr_type($pr_type_id){

		$this->check_login();

		if(empty($pr_type_id)) redirect('subadmin/property_types');	

		$this->subadmin_model->delete('property_types',array('id'=>$pr_type_id));

		$this->session->set_flashdata('success_msg',"Property Type has been Deleted successfully.");

		redirect('subadmin/property_types');

	}

	public function edit_pr_type($pr_type_id){

		$this->check_login();

		if(empty($pr_type_id)) redirect('subadmin/property_types'); 

		$data['property_type'] = $this->subadmin_model->get_row('property_types',array('id'=>$pr_type_id));

		$this->form_validation->set_rules('property_type','Property Type','trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'property_type' =>$this->input->post('property_type'),

						  'modified'=>date('Y-m-d H:i:s') 

						  );

			

			



			$this->subadmin_model->update('property_types',$array,array('id'=>$pr_type_id));

			$this->session->set_flashdata('success_msg',"Property Type has been updated successfully.");

			redirect('subadmin/property_types');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Property Types' => '/subadmin/property_types', 'Edit Property Types' => '/subadmin/edit_pr_type'));



		$data['template'] = 'subadmin/edit_pr_type';

        $this->load->view('templates/subadmin_template', $data);

	}

	public function bed_types($offset=0){

		$this->check_login();

		$limit=20;

		$data['bed_types']=$this->subadmin_model->get_pagination_result('bed_types',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/bed_types/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('bed_types',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Bed Types' => '/subadmin/bed_types'));



		$data['template'] = 'subadmin/bed_types';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_bed_type(){

		$this->check_login();

		$this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'bed_type' =>$this->input->post('bed_type'),

						  'created'=>date('Y-m-d H:i:s') 

						  );

			

			

			$this->subadmin_model->insert('bed_types',$array);

			$this->session->set_flashdata('success_msg',"Property Type has been added successfully.");

			redirect('subadmin/bed_types');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Bed Types' => '/subadmin/bed_types', 'Add Bed Types' => '/subadmin/add_bed_type'));



		$data['template'] = 'subadmin/add_bed_type';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_bed_type($bed_ty_id){

		$this->check_login();

		if(empty($bed_ty_id)) redirect('subadmin/bed_types');	

		$this->subadmin_model->delete('bed_types',array('id'=>$bed_ty_id));

		$this->session->set_flashdata('success_msg',"Bed Type has been Deleted successfully.");

		redirect('subadmin/bed_types');

	}

	public function edit_bed_type($bed_ty_id){

		$this->check_login();

		if(empty($bed_ty_id)) redirect('subadmin/bed_types'); 

		$data['bed_type'] = $this->subadmin_model->get_row('bed_types',array('id'=>$bed_ty_id));

		$this->form_validation->set_rules('bed_type','Bed Type','trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'bed_type' =>$this->input->post('bed_type'),

						  'modified'=>date('Y-m-d H:i:s') 

						  );

			

			



			$this->subadmin_model->update('bed_types',$array,array('id'=>$bed_ty_id));

			$this->session->set_flashdata('success_msg',"Bed Type has been updated successfully.");

			redirect('subadmin/bed_types');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Bed Types' => '/subadmin/bed_types', 'Edit Bed Types' => '/subadmin/edit_bed_type'));



		$data['template'] = 'subadmin/edit_bed_type';

        $this->load->view('templates/subadmin_template', $data);

	}

	public function room_allotment($offset=0){

		$this->check_login();

		$limit=20;

		$data['allotments']=$this->subadmin_model->get_pagination_result('room_allotment',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/room_allotment/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('room_allotment',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Room Allotment' => '/subadmin/room_allotment'));



		$data['template'] = 'subadmin/room_allotment';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_allotment(){

		$this->check_login();

		$this->form_validation->set_rules('room_allotment', 'Room Allotment', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'room_allotment' =>$this->input->post('room_allotment'),

						  'created'=>date('Y-m-d H:i:s') 

						  );

			

			

			$this->subadmin_model->insert('room_allotment',$array);

			$this->session->set_flashdata('success_msg',"Room Allotment has been added successfully.");

			redirect('subadmin/room_allotment');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Room Allotment' => '/subadmin/room_allotment', 'Add Room Allotment' => '/subadmin/add_room_allotment'));



		$data['template'] = 'subadmin/add_allotment';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_allotment($room_all_id){

		$this->check_login();

		if(empty($room_all_id)) redirect('subadmin/room_allotment');	

		$this->subadmin_model->delete('room_allotment',array('id'=>$room_all_id));

		$this->session->set_flashdata('success_msg',"Room Allotment has been Deleted successfully.");

		redirect('subadmin/room_allotment');

	}

	public function edit_allotment($room_all_id){

		$this->check_login();

		if(empty($room_all_id)) redirect('subadmin/room_allotment'); 

		$data['room_allotment'] = $this->subadmin_model->get_row('room_allotment',array('id'=>$room_all_id));

		$this->form_validation->set_rules('room_allotment','Room Allotment','trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'room_allotment' =>$this->input->post('room_allotment'),

						  'modified'=>date('Y-m-d H:i:s') 

						  );



			$this->subadmin_model->update('room_allotment',$array,array('id'=>$room_all_id));

			$this->session->set_flashdata('success_msg',"Room Allotment has been updated successfully.");

			redirect('subadmin/room_allotment');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Room Allotment' => '/subadmin/room_allotment', 'Edit Room Allotment' => '/subadmin/edit_allotment'));



		$data['template'] = 'subadmin/edit_allotment';

        $this->load->view('templates/subadmin_template', $data);

	}

	/*Setting new starts*/
	public function Settings()
	{
		$subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->setting==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$id = get_subadmin_id();

		$data['user'] = $this->subadmin_model->get_row('users',array('id'=>$id));

		if(isset($_POST['save_profile_info'])){

			$this->form_validation->set_rules('first_name','First Name','trim|required');

			$this->form_validation->set_rules('last_name','Last Name','trim|required');

			$this->form_validation->set_rules('address','Address','trim|required');

			$this->form_validation->set_rules('city','City','trim|required');

			$this->form_validation->set_rules('phone','Phone','trim|regex_match[/^[0-9]+$/]|xss_clean');

			$this->form_validation->set_rules('user_email','User Email','trim|required|valid_email');

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

				$array= array(

							  'first_name'  => $this->input->post('first_name'),

							  'last_name'   => $this->input->post('last_name'),

							  'address'     => $this->input->post('address'),

							  'city'        => $this->input->post('city'),

							  'phone'       => $this->input->post('phone'),

							  'user_email'  => $this->input->post('user_email'),

							  'updated'     => date('Y-m-d H:i:s') 

							);

				$this->subadmin_model->update('users',$array,array('id'=>$id));

				$this->session->set_flashdata('success_profile_msg',"Users Profile has been updated successfully.");

				redirect('subadmin/settings');

			}

		}

		if(isset($_POST['save_password'])){

			$this->form_validation->set_rules('old_password','Old Password','trim|required');

			$this->form_validation->set_rules('new_password','New Password','trim|required');

			$this->form_validation->set_rules('con_password','Confirm Password','trim|required|matches[new_password]');

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

				$old_pass = $data['user']->password;

				$prev_pass = sha1($this->input->post('old_password'));

				if ($prev_pass != $old_pass) {

	        		$this->session->set_flashdata('error_pass_msg', 'Invalid Old Password');

	        		redirect('subadmin/settings');

	        	}else

	        	{

	        		$array = array('password' => sha1($this->input->post('new_password')),

	        							);

	        	}

				$this->subadmin_model->update('users',$array,array('id'=>$id));

				$this->session->set_flashdata('success_pass_msg',"Users Password has been updated successfully.");

				redirect('subadmin/settings');

			}



		}

		if(isset($_POST['mailchimp'])){

			$this->form_validation->set_rules('apikey','Api Key','required');			

			$this->form_validation->set_rules('listid','List Id','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

				$data = array(

					'apikey' => $this->input->post('apikey'),

					'listid' => $this->input->post('listid'),

					);

				$this->subadmin_model->update('mailchimp',$data,array('id'=>1));

				$this->session->set_flashdata('success_pass_msg',"Apikey updated");

				redirect('subadmin/settings');

			}

		}

		if(isset($_POST['paypal_btn_unique'])){

			$this->form_validation->set_rules('paypal_email','Paypal Email','required|valid_email');			

			$this->form_validation->set_rules('paypal_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'paypal'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('paypal_email'),

						'api_key' => $this->input->post('paypal_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'paypal',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_paypal',"Paypal settings has been added successfully");

					redirect('subadmin/settings/#paypal_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('paypal_email'),

						'api_key' => $this->input->post('paypal_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'paypal'));

					$this->session->set_flashdata('success_paypal',"Paypal settings has been updated successfully");

					redirect('subadmin/settings/#paypal_location');
	            }

			}

		}


		if(isset($_POST['braintree_btn_unique'])){

			$this->form_validation->set_rules('braintree_email','Braintree Email','required|valid_email');			

			$this->form_validation->set_rules('braintree_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'braintree'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('braintree_email'),

						'api_key' => $this->input->post('braintree_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'braintree',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_braintree',"Braintree settings has been added successfully");

					redirect('subadmin/settings/#braintree_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('braintree_email'),

						'api_key' => $this->input->post('braintree_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'braintree'));

					$this->session->set_flashdata('success_braintree',"Braintree settings has been updated successfully");

					redirect('subadmin/settings/#braintree_location');
	            }

			}

		}


		if(isset($_POST['stripe_btn_unique'])){

			$this->form_validation->set_rules('stripe_email','Stripe Email','required|valid_email');			

			$this->form_validation->set_rules('stripe_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'stripe'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('stripe_email'),

						'api_key' => $this->input->post('stripe_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'stripe',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_stripe',"Stripe settings has been added successfully");

					redirect('subadmin/settings/#stripe_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('stripe_email'),

						'api_key' => $this->input->post('stripe_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'stripe'));

					$this->session->set_flashdata('success_stripe',"Stripe settings has been updated successfully");

					redirect('subadmin/settings/#stripe_location');
	            }

			}

		}


		if(isset($_POST['2checkout_btn_unique'])){

			$this->form_validation->set_rules('2checkout_email','2Checkout Email','required|valid_email');			

			$this->form_validation->set_rules('2checkout_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'2checkout'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('2checkout_email'),

						'api_key' => $this->input->post('2checkout_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'2checkout',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_2checkout',"2Checkout settings has been added successfully");

					redirect('subadmin/settings/#2checkout_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('2checkout_email'),

						'api_key' => $this->input->post('2checkout_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'2checkout'));

					$this->session->set_flashdata('success_2checkout',"2Checkout settings has been updated successfully");

					redirect('subadmin/settings/#2checkout_location');
	            }

			}

		}

		if(isset($_POST['payza_api_key'])){

			$this->form_validation->set_rules('payza_email','payza Email','required|valid_email');			

			$this->form_validation->set_rules('payza_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'payza'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('payza_email'),

						'api_key' => $this->input->post('payza_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'payza',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_payza',"Payza settings has been added successfully");

					redirect('subadmin/settings/#payza_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('payza_email'),

						'api_key' => $this->input->post('payza_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'payza'));

					$this->session->set_flashdata('success_payza',"Payza settings has been updated successfully");

					redirect('subadmin/settings/#payza_location');
	            }

			}

		}


		if(isset($_POST['skrill_api_key'])){

			$this->form_validation->set_rules('skrill_email','skrill Email','required|valid_email');			

			$this->form_validation->set_rules('skrill_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'skrill'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('skrill_email'),

						'api_key' => $this->input->post('skrill_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'skrill',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_skrill',"Skrill settings has been added successfully");

					redirect('subadmin/settings/#skrill_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('skrill_email'),

						'api_key' => $this->input->post('skrill_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'skrill'));

					$this->session->set_flashdata('success_skrill',"Skrill settings has been updated successfully");

					redirect('subadmin/settings/#skrill_location');
	            }

			}

		}


		if(isset($_POST['dwolla_api_key'])){

			$this->form_validation->set_rules('dwolla_email','Dwolla Email','required|valid_email');			

			$this->form_validation->set_rules('dwolla_api_key','Api Key','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

            $response = $this->subadmin_model->get_row('payment_gateways',array('name'=>'dwolla'));
	            if(empty($response))
	            {
					$data = array(

						'email' => $this->input->post('dwolla_email'),

						'api_key' => $this->input->post('dwolla_api_key'),

						'created' =>date('d-m-Y'),

						'name'    =>'dwolla',

						);

					$this->subadmin_model->insert('payment_gateways',$data);

					$this->session->set_flashdata('success_dwolla',"Dwolla settings has been added successfully");

					redirect('subadmin/settings/#dwolla_location');
                 
	            }
	            else
	            {
					$data = array(

						'email' => $this->input->post('dwolla_email'),

						'api_key' => $this->input->post('dwolla_api_key'),

						'updated' =>date('d-m-Y'),

						);

					$this->subadmin_model->update('payment_gateways',$data,array('name'=>'dwolla'));

					$this->session->set_flashdata('success_dwolla',"Dwolla settings has been updated successfully");

					redirect('subadmin/settings/#dwolla_location');
	            }

			}

		}


		if(isset($_POST['twillio_btn_unique'])){

			$this->form_validation->set_rules('twillio_authtoken','Auth Token','required');			

			$this->form_validation->set_rules('twillio_account_id','Account Id','required');			

			$this->form_validation->set_rules('twillio_number','Number','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

					$data = array(

						'auth_token' => $this->input->post('twillio_authtoken'),

						'account_id' => $this->input->post('twillio_account_id'),

						'your_number' => $this->input->post('twillio_number'),

						);

					$this->subadmin_model->update('twillio',$data,array('id'=>1));

					$this->session->set_flashdata('success_twillio',"Twillio settings has been updated successfully");

					redirect('subadmin/settings/#twillio_location');

			}

		}

		if(isset($_POST['twillio_btn_unique'])){

			$this->form_validation->set_rules('twillio_authtoken','Auth Token','required');			

			$this->form_validation->set_rules('twillio_account_id','Account Id','required');			

			$this->form_validation->set_rules('twillio_number','Number','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

					$data = array(

						'auth_token' => $this->input->post('twillio_authtoken'),

						'account_id' => $this->input->post('twillio_account_id'),

						'your_number' => $this->input->post('twillio_number'),

						);

					$this->subadmin_model->update('twillio',$data,array('id'=>1));

					$this->session->set_flashdata('success_twillio',"Twillio settings has been updated successfully");

					redirect('subadmin/settings/#twillio_location');

			}

		}

		if(isset($_POST['jumio_btn_unique'])){

			$this->form_validation->set_rules('jumio_authtoken','Auth Token','required');			

			$this->form_validation->set_rules('jumio_key','Jumio Key','required');			

			$this->form_validation->set_rules('jumio_secret','Jumio Secret','required');			

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

			if($this->form_validation->run() == TRUE){

					$data = array(

						'auth_token' => $this->input->post('jumio_authtoken'),

						'api_key' => $this->input->post('jumio_key'),

						'api_secret' => $this->input->post('jumio_secret'),

						);

					$this->subadmin_model->update('jumio',$data,array('id'=>1));

					$this->session->set_flashdata('success_jumio',"Jumio settings has been updated successfully");

					redirect('subadmin/settings/#jumio_location');

			}

		}

		$data['mailchimp'] = $this->subadmin_model->get_row('mailchimp', array('id'=>1));

		$data['template'] = 'subadmin/settings';

        $this->load->view('templates/subadmin_template', $data);

	}


	/*Setting new ends*/

	// public function Settings()

	// {

	// 	$this->check_login();

	// 	$id = get_subadmin_id();

	// 	$data['user'] = $this->subadmin_model->get_row('users',array('id'=>$id));

	// 	if(isset($_POST['save_profile_info'])){

	// 		$this->form_validation->set_rules('first_name','First Name','trim|required');

	// 		$this->form_validation->set_rules('last_name','Last Name','trim|required');

	// 		$this->form_validation->set_rules('address','Address','trim|required');

	// 		$this->form_validation->set_rules('city','City','trim|required');

	// 		$this->form_validation->set_rules('phone','Phone','trim|regex_match[/^[0-9]+$/]|xss_clean');

	// 		$this->form_validation->set_rules('user_email','User Email','trim|required|valid_email');

	// 		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

	// 		if($this->form_validation->run() == TRUE){

	// 			$array= array(

	// 						  'first_name'  => $this->input->post('first_name'),

	// 						  'last_name'   => $this->input->post('last_name'),

	// 						  'address'     => $this->input->post('address'),

	// 						  'city'        => $this->input->post('city'),

	// 						  'phone'       => $this->input->post('phone'),

	// 						  'user_email'  => $this->input->post('user_email'),

	// 						  'updated'     => date('Y-m-d H:i:s') 

	// 						);

	// 			$this->subadmin_model->update('users',$array,array('id'=>$id));

	// 			$this->session->set_flashdata('success_profile_msg',"Users Profile has been updated successfully.");

	// 			redirect('subadmin/settings');

	// 		}

	// 	}

	// 	if(isset($_POST['save_password'])){

	// 		$this->form_validation->set_rules('old_password','Old Password','trim|required');

	// 		$this->form_validation->set_rules('new_password','New Password','trim|required');

	// 		$this->form_validation->set_rules('con_password','Confirm Password','trim|required|matches[new_password]');

	// 		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

	// 		if($this->form_validation->run() == TRUE){

	// 			$old_pass = $data['user']->password;

	// 			$prev_pass = sha1($this->input->post('old_password'));

	// 			if ($prev_pass != $old_pass) {

	//         		$this->session->set_flashdata('error_pass_msg', 'Invalid Old Password');

	//         		redirect('subadmin/settings');

	//         	}else

	//         	{

	//         		$array = array('password' => sha1($this->input->post('new_password')),

	//         							);

	//         	}

				



				

	// 			$this->subadmin_model->update('users',$array,array('id'=>$id));

	// 			$this->session->set_flashdata('success_pass_msg',"Users Password has been updated successfully.");

	// 			redirect('subadmin/settings');

	// 		}



	// 	}





	// 	if(isset($_POST['mailchimp'])){

			

	// 		$this->form_validation->set_rules('apikey','Api Key','required');			

	// 		$this->form_validation->set_rules('listid','List Id','required');			

	// 		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

	// 		if($this->form_validation->run() == TRUE){

	// 			$data = array(

	// 				'apikey' => $this->input->post('apikey'),

	// 				'listid' => $this->input->post('listid'),

	// 				);

	// 			$this->subadmin_model->update('mailchimp',$data,array('id'=>1));

	// 			$this->session->set_flashdata('success_pass_msg',"Apikey updated");

	// 			redirect('subadmin/settings');

	// 		}



	// 	}

	// 	$data['mailchimp'] = $this->subadmin_model->get_row('mailchimp', array('id'=>1));

	// 	// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Settings' => '/subadmin/settings' ));

	// 	$data['template'] = 'subadmin/settings';

 //        $this->load->view('templates/subadmin_template', $data);

	// }



	

	

	public function payments($sort='newest', $offset=0)
	{		
	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->payments==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$name='';

		$post="";

		if($_POST){			

			$name  = $this->input->post('name');

            $post = $_POST;

		}

		

		$limit=10;

		$data['payments']=$this->subadmin_model->payments($limit,$offset,$sort,$name,$post);		

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/payments/'.$sort;

		$config['total_rows'] = $this->subadmin_model->payments(0,0,$sort,$name,$post);

		$config['per_page'] = 10;

		$config['num_links'] =3;

		$config['uri_segment'] =4;



		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		$data['template'] = 'subadmin/payments';

        $this->load->view('templates/subadmin_template', $data);

		

	}



	/*Notes Work strats From here*/

	public function payment_notes($payment_id='')

	{

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/payments'); 

		$data['notes']=$this->subadmin_model->get_result('payment_notes',array('payment_id' =>$payment_id));

		$data['id'] = $payment_id;

		$data['template'] = 'subadmin/payment_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function add_payment_note($payment_id='')

	{

		$this->check_login(); 

		if(empty($payment_id)) redirect('subadmin/payments');

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){			

			$data=array(

						'title'=>$this->input->post('title'),				

						'description'=>$this->input->post('description'),

						'payment_id'=>$payment_id,

						'created' => date('Y-m-d H:i:s')		

				       );	

			$this->subadmin_model->insert('payment_notes',$data);		

			$this->session->set_flashdata('success_msg'," Successfully payment Note Added.");

			redirect('subadmin/payment_notes/'.$payment_id);

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Faqs' => '/subadmin/faqs', 'Add Faq' => '/subadmin/add_faq'));

		$data['template'] = 'subadmin/add_payment_note';

        $this->load->view('templates/subadmin_template', $data);		



	}



	public function view_payment_note($payment_id='')

	{

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/payments'); 

		$data['notes']=$this->subadmin_model->get_row('payment_notes',array('id' =>$payment_id));

		if(empty($data['notes'])){

			redirect('subadmin/payments');

		}

		$data['template'] = 'subadmin/view_payment_note';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_payment_note($id="",$payment_id="")

	{

		if($id=="")

		{

			redirect('subadmin/payments');

		}

		$this->subadmin_model->delete('payment_notes',array('id'=>$id));

		$this->session->set_flashdata('success_msg'," Payment Note has been deleted successfully.");

		redirect('subadmin/payment_notes/'.$payment_id);

	}

     

     /*Payments work ends from here*/



	public function coupons($offset=0)

	{

		$this->check_login();

		$limit=10;

		$data['coupons']=$this->subadmin_model->get_pagination_result('coupons',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/coupons/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('coupons',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Bed Types' => '/subadmin/bed_types'));

		$data['template'] = 'subadmin/coupons';

        $this->load->view('templates/subadmin_template', $data);

	}



	public function generate_coupon_code()

	{

		$this->check_login();

		echo time();



	}



	public function check_coupons_date(){



		$start_date = strtotime($this->input->post('start_date'));



		$end_date = strtotime($this->input->post('end_date'));

		// echo $start_date.','.$end_date;

		// die();

		if ($start_date > $end_date){

			$this->form_validation->set_message('check_coupons_date', 'Start date should not exceeds than End date.');

			return FALSE;

		}else{

			return TRUE;

		}

	}



	public function add_coupon(){

		$this->check_login();

		$this->form_validation->set_rules('name', 'Name', 'required');

		$this->form_validation->set_rules('coupon_code', 'Coupon code', 'required');

		$this->form_validation->set_rules('start_date', 'Start date', 'required|callback_check_coupons_date');

		$this->form_validation->set_rules('end_date', 'End Date', 'required');		

		$this->form_validation->set_rules('type', 'Amount Type', 'required|is_numeric');		

		

		$this->form_validation->set_rules('amount', 'Amount', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$data=array(

				'code'=>$this->input->post('coupon_code'),	

				'name'=>$this->input->post('name'),				

				'start_date'=> date('m/d/Y', strtotime($this->input->post('start_date'))),

				'end_date'=>date('m/d/Y', strtotime($this->input->post('end_date'))),				

				'reduction_type'=>$this->input->post('type'),

				// 'discount_use'=>$this->input->post('discount_use'),

				'reduction_amount'=>$this->input->post('amount'),

				'created'=>date('Y-m-d H:i:s'),

				);

			

			

			$this->subadmin_model->insert('coupons',$data);

			$this->session->set_flashdata('success_msg',"coupon has been added successfully.");

			redirect('subadmin/coupons');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Bed Types' => '/subadmin/bed_types', 'Add Bed Types' => '/subadmin/add_bed_type'));



		$data['template'] = 'subadmin/add_coupon';

        $this->load->view('templates/subadmin_template', $data);	

	}

	// public function check_coupon_code($code, $params){

	// 	$param = explode(',', $params);

	// 	$called = $param[0];

	// 	$old_name = $param[1];

	// 	if ($called == 'add'){

	// 		$resp = $this->subadmin_model->get_row('coupons', array('code' => strtolower($code)));

	// 		if ($resp){

	// 			$this->form_validation->set_message('check_coupon_code', 'Code you are choosing already exist.');

	// 			return FALSE;

	// 		}else

	// 			return TRUE;

	// 	}elseif (strtolower($old_name) == strtolower($code)) {

	// 			return TRUE;

	// 		}else{

	// 			$resp = $this->subadmin_model->get_row('coupons', array('code' => strtolower($code)));

	// 			if ($resp) {

	// 				$this->form_validation->set_message('check_coupon_code', 'Code you are choosing already exist.');

	// 				return FALSE;

	// 			}else{

	// 				return TRUE;

	// 			}

	// 		}

	// 	}

	public function delete_coupon($coup_id=''){

		$this->check_login();

		if(empty($coup_id)) redirect('subadmin/coupons');	

		$this->subadmin_model->delete('coupons',array('id'=>$coup_id));

		$this->session->set_flashdata('success_msg',"Coupon has been Deleted successfully.");

		redirect('subadmin/coupons');

	}

	public function edit_coupon($coup_id=''){

		$this->check_login();

		if(empty($coup_id)) redirect('subadmin/coupons'); 

		$data['coupon'] = $this->subadmin_model->get_row('coupons',array('id'=>$coup_id));

		// print_r($data['coupon']);

		// die();

		$this->form_validation->set_rules('name', 'Name', 'required');

		$this->form_validation->set_rules('coupon_code', 'Coupon code', 'required');

		$this->form_validation->set_rules('start_date', 'Start date', 'required|callback_check_coupons_date');

		$this->form_validation->set_rules('end_date', 'End Date', 'required');		

		$this->form_validation->set_rules('type', 'Amount Type', 'required|is_numeric');		

		

		$this->form_validation->set_rules('amount', 'Amount', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$data=array(

				'code'=>$this->input->post('coupon_code'),	

				'name'=>$this->input->post('name'),				

				'start_date'=> date('m/d/Y', strtotime($this->input->post('start_date'))),

				'end_date'=>date('m/d/Y', strtotime($this->input->post('end_date'))),				

				'reduction_type'=>$this->input->post('type'),

				// 'discount_use'=>$this->input->post('discount_use'),

				'reduction_amount'=>$this->input->post('amount'),

				'created'=>date('Y-m-d H:i:s'),

				);

			

			

			$this->subadmin_model->update('coupons',$data,array('id'=>$coup_id));

			$this->session->set_flashdata('success_msg',"coupon has been Updated successfully.");

			redirect('subadmin/coupons');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Bed Types' => '/subadmin/bed_types', 'Edit Bed Types' => '/subadmin/edit_bed_type'));



		$data['template'] = 'subadmin/edit_coupon';

        $this->load->view('templates/subadmin_template', $data);

	}



	// zakir work



	public function add_coupons_note($coupons_id='')

	{

	  $this->check_login(); 

	  if(empty($coupons_id)) redirect('subadmin/coupons');

	  $data['coupons'] = $this->subadmin_model->get_row('coupons', array('id' => $coupons_id));

	   

	  $this->form_validation->set_rules('title', 'Title', 'required');  

	  $this->form_validation->set_rules('note', 'Note', 'required');

	  $this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');   

	  if ($this->form_validation->run() == TRUE){   

	   $data=array(

	    'title'=>$this->input->post('title'),    

	    'note'=>$this->input->post('note'),

	    'coupons_id'=>$coupons_id,



	    'created' => date('Y-m-d H:i(worry)')  

	    ); 

	   $this->subadmin_model->insert('coupons_notes',$data);  

	   $this->session->set_flashdata('success_msg',"Successfully Coupons Note Added.");

	   redirect('subadmin/coupons');

	  }

	  // $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Faqs' => '/subadmin/faqs', 'Add Faq' => '/subadmin/add_faq'));

	  $data['template'] = 'subadmin/add_coupons_note';

	        $this->load->view('templates/subadmin_template', $data); 



	 }

	 public function coupons_notes($coupons_id='')

	 {

	  $this->check_login();

	  if(empty($coupons_id)) redirect('subadmin/coupons'); 

	  $data['notes']=$this->subadmin_model->get_result('coupons_notes',array('coupons_id' =>$coupons_id));

	  $data['coupons_id']=$coupons_id;

	  $data['template'] = 'subadmin/coupons_notes';

	  $this->load->view('templates/subadmin_template', $data);

	 }

	 



	 public function view_coupons_notes($coupons_note_id='')

	 {

	  $this->check_login();

	  if(empty($coupons_note_id)) redirect('subadmin/coupons'); 

	  $data['notes']=$this->subadmin_model->get_row('coupons_notes',array('id' =>$coupons_note_id));

	  if(empty($data['notes'])){

	   redirect('subadmin/coupons');

	  }

	  $data['template'] = 'subadmin/view_coupons_note';

	  $this->load->view('templates/subadmin_template', $data);  

	 }

	 public function delete_coupons_note($coupons_note_id=''){

	  $this->check_login();

	  if(empty($coupons_note_id)) redirect('subadmin/coupons');  

	  $this->subadmin_model->delete('coupons_notes',array('id'=>$coupons_note_id));

	  $this->session->set_flashdata('success_msg',"Coupon Note has been deleted successfully.");

	  redirect('subadmin/coupons');

	 }





// zakir work















	public function collection($offset=0){

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->collection==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['collection']=$this->subadmin_model->get_pagination_result('collection_category',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/collection/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('collection_category',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		//$data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Groups' => '/subadmin/groups'));



		$data['template'] = 'subadmin/collection';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function add_collection_category(){



		$this->check_login();

		$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');

		$this->form_validation->set_rules('description', 'Category Description', 'trim|required');



		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'category_name' =>$this->input->post('category_name'),

						  'description'   =>$this->input->post('description'),

						  'slug'          =>str_replace(' ','-',$this->input->post('category_name')),

						  'created'       =>date('Y-m-d H:i:s') 

						  );

			if($_FILES['collection']['name']!=''){

				$config['upload_path'] = './assets/uploads/collection_banner/';

				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$config['max_size']	= '10000';

				$this->load->library('upload', $config);				

							

				if(! $this->upload->do_upload('collection'))

				{

					$this->session->set_flashdata('image_error', $this->upload->display_errors());

					redirect('subadmin/add_collection_category/');

				}else{

					$array['banner']=$_FILES['collection']['name'];

				}

			}

			else

			{

					$this->session->set_flashdata('image_error','Please Upload banner');

					redirect('subadmin/add_collection_category/');

			}



			

			$this->subadmin_model->insert('collection_category',$array);

			$this->session->set_flashdata('success_msg',"Collection Category has been added successfully.");

			redirect('subadmin/collection');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Groups' => '/subadmin/groups', 'Add Group' => '/subadmin/add_group'));



		$data['template'] = 'subadmin/add_collect_category';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function edit_collection_category($cate_id=""){



		$this->check_login();

		if(empty($cate_id)) redirect('subadmin/collection'); 

		$data['category'] = $this->subadmin_model->get_row('collection_category',array('id'=>$cate_id));

		$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');

		$this->form_validation->set_rules('description', 'Category Description', 'trim|required');



		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

						  'category_name' =>$this->input->post('category_name'),

						  'description'   =>$this->input->post('description'),

						  'slug'          =>str_replace(' ','-',$this->input->post('category_name')),

						  'updated'       =>date('Y-m-d H:i:s') 

						  );

			if($_FILES['collection']['name']!=''){

					$config['upload_path'] = './assets/uploads/collection_banner/';

					$config['allowed_types'] = 'gif|jpg|png|jpeg';

					$config['max_size']	= '10000';

					$this->load->library('upload', $config);				

								

					if(! $this->upload->do_upload('collection'))

					{

						$this->session->set_flashdata('image_error', $this->upload->display_errors());

						redirect('subadmin/add_collection_category/');

					}

					else{

						

						if($data['category']->banner !=''){

							$path = './assets/uploads/collection_banner/';

							$file = $data['category']->banner;

							if(!empty($file)){

								@unlink($path.$file);

							}

						}

						$array['banner']=$_FILES['collection']['name'];

					}

			}

			



			

			$this->subadmin_model->update('collection_category',$array,array('id' =>$cate_id));

			$this->session->set_flashdata('success_msg',"Collection Category has been updated successfully.");

			redirect('subadmin/collection');

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Groups' => '/subadmin/groups', 'Add Group' => '/subadmin/add_group'));



		$data['template'] = 'subadmin/edit_collect_category';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function delete_collection_category($cate_id=''){

		$this->check_login();

		if(empty($cate_id)) redirect('subadmin/collection');	

		$this->subadmin_model->delete('collection_category',array('id'=>$cate_id));

		$this->session->set_flashdata('success_msg',"collection Category has been Deleted successfully.");

		redirect('subadmin/collection');

	}



	public function pages($offset=0){


	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->cms==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['pages']=$this->subadmin_model->get_pagination_result('pages',$limit,$offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/pages/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('pages',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;	

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();	



        $data['template'] = 'subadmin/pages';

        $this->load->view('templates/subadmin_template', $data);	

	}





	public function add_page()

	{

		$this->check_login(); 

		$this->form_validation->set_rules('page_title', 'page_title', 'required');	

		$this->form_validation->set_rules('slug', 'Slug', 'required|callback_check_unique_slug');	

		$this->form_validation->set_rules('content', 'content', 'required');	

		$this->form_validation->set_rules('seo_author', 'Author Name', 'required');	

		$this->form_validation->set_rules('seo_keyword', 'Keyword', 'required');	

		// $this->form_validation->set_rules('seo_description', 'Description', 'required');	

		$this->form_validation->set_rules('page_link_position', 'Link position of page', 'required');	

		if ($this->form_validation->run() == TRUE){	

		$data=array(

		'page_title'     => $this->input->post('page_title'),	

		'slug'           => strtolower(trim(str_replace(' ', '',remove_special_character($this->input->post('slug'))))),	

		'content'        => $this->input->post('content'),	

		'seo_author'     => $this->input->post('seo_author'),	

		'seo_keyword'    => $this->input->post('seo_keyword'),	

		'seo_description'=> $this->input->post('seo_description'),	

		'page_link_position'=> $this->input->post('page_link_position'),	

		'created'        => date('Y-m-d H:i:s')	

		);	



		$this->subadmin_model->insert('pages',$data);	

		$this->session->set_flashdata('success_msg',"Page has been added successfully.");

		redirect('subadmin/pages');

		}	

		$data['template'] = 'subadmin/add_page';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function edit_page($id='')

	{

		$this->check_login(); 

		$data['pages'] = $this->subadmin_model->get_row('pages', array('id'=>$id));

		$this->form_validation->set_rules('page_title', 'page_title', 'required');	

		if($data['pages']->slug != $this->input->post('slug'))

		{

		$this->form_validation->set_rules('slug', 'Slug', 'required|callback_check_unique_slug');	

		}

		$this->form_validation->set_rules('content', 'content', 'required');	

		$this->form_validation->set_rules('seo_author', 'Author Name', 'required');	

		$this->form_validation->set_rules('seo_keyword', 'Keyword', 'required');	

		// $this->form_validation->set_rules('seo_description', 'Description', 'required');	

		$this->form_validation->set_rules('page_link_position', 'Link position of page', 'required');	

		if ($this->form_validation->run() == TRUE)

		{	

		$data=array(

					'page_title'     => $this->input->post('page_title'),	

					'slug'           => strtolower(trim(str_replace(' ', '',remove_special_character($this->input->post('slug'))))),	

					'content'        => $this->input->post('content'),	

					'seo_author'     => $this->input->post('seo_author'),	

					'seo_keyword'    => $this->input->post('seo_keyword'),	

					'seo_description'=> $this->input->post('seo_description'),	

					'page_link_position'=> $this->input->post('page_link_position'),	

					'created'        => date('Y-m-d H:i:s')	

					);	



		$this->subadmin_model->update('pages',$data, array('id'=>$id));	

		$this->session->set_flashdata('success_msg',"Page has been updated successfully.");

		redirect('subadmin/pages');

		}	

		$data['template'] = 'subadmin/edit_page';

        $this->load->view('templates/subadmin_template', $data);	

	}





	public function delete_page($id="")

	{

		if($id=="") redirect('subadmin/pages');

		$this->subadmin_model->delete('pages',array('id'=>$id));

		$this->session->set_flashdata('success_msg',"Page deleted.");

		redirect('subadmin/pages');

	}





	public function publish_page($page_id='')

	{

		$this->check_login();

		if(empty($page_id)) redirect('subadmin/pages');

		$data['page'] = $this->subadmin_model->get_row('pages', array('id' => $page_id));

		if($data['page']->status == 0){

			$array = array('status' => 1, );

			$this->subadmin_model->update('pages',$array,array('id' => $page_id));

			$this->session->set_flashdata('success_msg',"page has been Published successfully.");

			redirect('subadmin/pages');

		}

		else{

			$array = array('status' =>0, );

			$this->subadmin_model->update('pages',$array,array('id' => $page_id));

			$this->session->set_flashdata('success_msg',"page has been Unpublished successfully.");

			redirect('subadmin/pages');

		}



	}

	



	public function taxs($offset=0)

	{

		$this->check_login();

		$limit=10;

		$data['taxs']=$this->subadmin_model->get_pagination_result('taxs',$limit,$offset);

		if(empty($data['taxs'])){

			redirect('subadmin/add_tax');

		}

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/taxs/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('taxs',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;	

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();	



        $data['template'] = 'subadmin/taxs';

        $this->load->view('templates/subadmin_template', $data);	

	}





	public function add_tax(){

			$this->check_login(); 

			$this->form_validation->set_rules('tax', 'Tax', 'required');	

			$this->form_validation->set_rules('tax_type', 'Tax Type', 'required');

			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

			if ($this->form_validation->run() == TRUE){	

				

			$data=array(

					'tax'=>$this->input->post('tax'),	

					'tax_type'=>$this->input->post('tax_type'),	

					'created' => date('Y-m-d H:i:s')	

			);	

			$this->subadmin_model->insert('taxs',$data);	

			$this->session->set_flashdata('success_msg',"taxs has been added successfully.");

			redirect('subadmin/taxs');

			}	

			$data['template'] = 'subadmin/add_tax';

	        $this->load->view('templates/subadmin_template', $data);	

	}



	public function edit_tax($tax_id=''){

		$this->check_login();

		if(empty($tax_id)) redirect('subadmin/collection'); 

		$data['taxs'] = $this->subadmin_model->get_row('taxs',array('id'=>$tax_id)); 

		$this->form_validation->set_rules('tax', 'Tax', 'required|numeric');	

		$this->form_validation->set_rules('tax_type', 'Tax Type', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

		if ($this->form_validation->run() == TRUE){	

			$data=array(

					'tax'=>$this->input->post('tax'),	

					'tax_type'=>$this->input->post('tax_type'),	

					'updated' => date('Y-m-d H:i:s')	

			);	

		$this->subadmin_model->update('taxs',$data,array('id'=>$tax_id));	

		$this->session->set_flashdata('success_msg',"taxs has been updated successfully.");

		redirect('subadmin/taxs');

		}	

		$data['template'] = 'subadmin/edit_tax';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function delete_tax($tax_id=''){

		$this->check_login();

		if(empty($tax_id)) redirect('subadmin/taxs');	

		$this->subadmin_model->delete('taxs',array('id'=>$tax_id));

		$this->session->set_flashdata('success_msg',"taxs has been Deleted successfully.");

		redirect('subadmin/taxs');

	}



	public function set_invite_commission($offset=0)

	{

		$this->check_login();

		$limit=10;

		$data['commission']=$this->subadmin_model->get_pagination_result('set_invite_commission',$limit,$offset);

		if(empty($data['commission'])){

			redirect('subadmin/add_commission');

		}

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/set_invite_commission/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('set_invite_commission',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;	

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();	



        $data['template'] = 'subadmin/set_invite_commission';

        $this->load->view('templates/subadmin_template', $data);	

	}





	public function add_commission(){

		$this->check_login(); 

		$this->form_validation->set_rules('booking_commission', 'Booking Commission', 'required');	

		$this->form_validation->set_rules('listing_commission', 'Listing Commission', 'required');	

		$this->form_validation->set_rules('com_type', 'Tax Type', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

		if ($this->form_validation->run() == TRUE){	

		$data=array(

				'listing_commission'=>$this->input->post('listing_commission'),	

				'booking_commission'=>$this->input->post('booking_commission'),	

				'type'=>$this->input->post('com_type'),	

				'created' => date('Y-m-d H:i:s')	

		);	

		$this->subadmin_model->insert('set_invite_commission',$data);	

		$this->session->set_flashdata('success_msg',"Commission has been added successfully.");

		redirect('subadmin/set_invite_commission');

		}	

		$data['template'] = 'subadmin/add_commission';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function edit_commission($com_id=''){

		$this->check_login();

		if(empty($com_id)) redirect('subadmin/set_invite_commission'); 

		$data['commission'] = $this->subadmin_model->get_row('set_invite_commission',array('id'=>$com_id)); 

		$this->form_validation->set_rules('booking_commission', 'Booking Commission', 'required');	

		$this->form_validation->set_rules('listing_commission', 'Listing Commission', 'required');	

		$this->form_validation->set_rules('com_type', 'Tax Type', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

		if ($this->form_validation->run() == TRUE){	

			$data=array(

					'listing_commission'=>$this->input->post('listing_commission'),	

					'booking_commission'=>$this->input->post('booking_commission'),

					'type'=>$this->input->post('com_type'),	

					'updated' => date('Y-m-d H:i:s')	

			);	

			$this->subadmin_model->update('set_invite_commission',$data,array('id'=>$com_id));	

			$this->session->set_flashdata('success_msg',"commission has been updated successfully.");

			redirect('subadmin/set_invite_commission');

		}	

		$data['template'] = 'subadmin/edit_commission';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function delete_commission($com_id=''){

		$this->check_login();

		if(empty($com_id)) redirect('subadmin/set_invite_commission');	

		$this->subadmin_model->delete('set_invite_commission',array('id'=>$com_id));

		$this->session->set_flashdata('success_msg',"Commission has been Deleted successfully.");

		redirect('subadmin/set_invite_commission');

	}



	public function traveler_service_fee($offset=0)

	{

		$this->check_login();

		$limit=10;

		$data['traveler']=$this->subadmin_model->get_pagination_result('traveler_service_fee',$limit,$offset);

		if(empty($data['traveler'])){

			redirect('subadmin/add_ts_fee');

		}

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/traveler_service_fee/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('traveler_service_fee',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;	

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();	



        $data['template'] = 'subadmin/traveler_service_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}

	

	public function add_ts_fee(){

		$this->check_login(); 

		$this->form_validation->set_rules('ts_fee', 'Tax', 'required');	

		$this->form_validation->set_rules('type', 'Tax Type', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

		if ($this->form_validation->run() == TRUE){	

		$data=array(

				'ts_fee'=>$this->input->post('ts_fee'),	

				'type'=>$this->input->post('type'),	

				'created' => date('Y-m-d H:i:s')	

		);	

		$this->subadmin_model->insert('traveler_service_fee',$data);	

		$this->session->set_flashdata('success_msg',"Traveler Service Fee has been added successfully.");

		redirect('subadmin/traveler_service_fee');

		}	

		$data['template'] = 'subadmin/add_ts_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}

	public function edit_traveler_fee($ts_fee_id=''){

		$this->check_login();

		if(empty($ts_fee_id)) redirect('subadmin/traveler_service_fee'); 

		$data['traveler'] = $this->subadmin_model->get_row('traveler_service_fee',array('id'=>$ts_fee_id)); 

		$this->form_validation->set_rules('ts_fee', 'Tax', 'required');	

		$this->form_validation->set_rules('type', 'Tax Type', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if ($this->form_validation->run() == TRUE){	

			$data=array(

					'ts_fee'=>$this->input->post('ts_fee'),	

					'type'=>$this->input->post('type'),	

					'updated' => date('Y-m-d H:i:s')	

			);	

			$this->subadmin_model->update('traveler_service_fee',$data,array('id'=>$ts_fee_id));	

			$this->session->set_flashdata('success_msg',"Traveler Service Fee has been updated successfully.");

			redirect('subadmin/traveler_service_fee');

		}	

		$data['template'] = 'subadmin/edit_traveler_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function delete_traveler_fee($ts_fee_id=''){

		$this->check_login();

		if(empty($ts_fee_id)) redirect('subadmin/traveler_service_fee');	

		$this->subadmin_model->delete('traveler_service_fee',array('id'=>$ts_fee_id));

		$this->session->set_flashdata('success_msg',"Traveler Service Fee has been Deleted successfully.");

		redirect('subadmin/traveler_service_fee');

	}



	public function property_listing_fee($offset=0)

	{

		$this->check_login();

		$limit=10;

		$data['listing_fee']=$this->subadmin_model->get_pagination_result('property_listing_fee',$limit,$offset);

		if(empty($data['listing_fee'])){

			redirect('subadmin/add_listing_fee');

		}

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/property_listing_fee/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('property_listing_fee',0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;	

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();	



        $data['template'] = 'subadmin/property_listing_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function add_listing_fee(){

		$this->check_login(); 

		$this->form_validation->set_rules('listing_fee', 'Listing Fee', 'required');	

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('fee_duration', 'Fee Duration', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

		if ($this->form_validation->run() == TRUE){	

		$data=array(

					'listing_fee'=>$this->input->post('listing_fee'),	

					'type'=>$this->input->post('type'),	

					'fee_duration'=>$this->input->post('fee_duration'),	

					'created' => date('Y-m-d H:i:s')	

			       );	

		$this->subadmin_model->insert('property_listing_fee',$data);	

		$this->session->set_flashdata('success_msg',"Property Listing Fee has been added successfully.");

		redirect('subadmin/property_listing_fee');

		}	

		$data['template'] = 'subadmin/add_listing_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function edit_listing_fee($fee_id=''){

		$this->check_login();

		if(empty($fee_id)) redirect('subadmin/property_listing_fee'); 

		$data['listing_fee'] = $this->subadmin_model->get_row('property_listing_fee',array('id'=>$fee_id)); 

		$this->form_validation->set_rules('listing_fee', 'Listing Fee', 'required');	

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('fee_duration', 'Fee Duration', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if ($this->form_validation->run() == TRUE){	

			$data=array(

					'listing_fee'=>$this->input->post('listing_fee'),	

					'type'=>$this->input->post('type'),

					'fee_duration'=>$this->input->post('fee_duration'),	

					'updated' => date('Y-m-d H:i:s')	

			);	

			$this->subadmin_model->update('property_listing_fee',$data,array('id'=>$fee_id));	

			$this->session->set_flashdata('success_msg',"Property Listing Fee has been updated successfully.");

			redirect('subadmin/property_listing_fee');

		}	

		$data['template'] = 'subadmin/edit_listing_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function delete_listing_fee($fee_id=''){

		$this->check_login();

		if(empty($fee_id)) redirect('subadmin/property_listing_fee');	

		$this->subadmin_model->delete('property_listing_fee',array('id'=>$fee_id));

		$this->session->set_flashdata('success_msg',"Property Listing Fee has been Deleted successfully.");

		redirect('subadmin/property_listing_fee');

	}



	public function ajax_propety_amount($id=0){

  		$data = array();

		$amount = 0;

		$property = $this->subadmin_model->get_row('pr_info', array('pr_id' => $id));

		if ($property) {

			$amount = (2 * $property->rent)/100;

			$amount +=  $property->rent;

			echo json_encode(array('status' => 1, 'amount' => $amount));

		}

		else

			echo json_encode(array('status' => 0));

  	}

  	public function notifications($offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->notifications==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();
		$limit=10;
		$data['notifications'] = $this->subadmin_model->get_pagination_result('notifications', $limit, $offset);
		$config= get_theme_pagination();
		$config['base_url'] = base_url().'subadmin/notifications/';
		$config['total_rows'] = $this->subadmin_model->get_pagination_result('notifications', 0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'subadmin/notifications';
		$this->load->view('templates/subadmin_template',$data);
	}

	public function notifications_view($note_id='')
 	{
		$this->check_login();
 		$data['notifications'] =$this->subadmin_model->get_row('notifications',array('id'=>$note_id));
 		if(empty($data['notifications'])){
 			$this->session->set_flashdata('error_msg', 'No notification found.');
 			redirect('subadmin/notifications');
 		}
		$data['template'] = 'subadmin/notifications_view';
		$this->load->view('templates/subadmin_template',$data);	 		
 	}

 	public function delete_notification($note_id='')
 	{
		$this->check_login();
		if(empty($note_id)) redirect('subadmin/notifications');		
		$this->subadmin_model->delete('notification',array('id'=>$note_id));
		$this->session->set_flashdata('success_msg',"Notification has been deleted successfully.");
		redirect('subadmin/notifications');
	}

  	/*faraz work*/



  	public function messages($offset=0)

	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->message==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['messages'] = $this->subadmin_model->get_pagination_result('user_message', $limit, $offset);

		$config= get_theme_pagination();

		$config['base_url'] = base_url().'subadmin/messages/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('user_message', 0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

		$data['template'] = 'subadmin/messages';

		$this->load->view('templates/subadmin_template',$data);

	}



 	public function delete_message($message_id='')

 	{

		$this->check_login();

		if(empty($message_id)) redirect('subadmin/messages');		

		$this->subadmin_model->delete('user_message',array('id'=>$message_id));

		$this->subadmin_model->delete('user_message_reply',array('message_id'=>$message_id));

		$this->session->set_flashdata('success_msg',"Message has been deleted successfully.");

		redirect('subadmin/messages');

	}

  //   public function reply($message_id,$messanger_id)
  //   {
		// $this->check_login();
		// $reply = array(
		// 				'reply'          => $this->input->post('reply'),
		// 				'created'        => time(),
		// 				'message_id'     => $message_id,
		// 				'replier_id'     => get_subadmin_id(),
  // 						'messanger_id'   => $messanger_id,
  // 						'readBySubadmin' => 1
		// 			  );
		// if($reply['replier_id'] == $reply['messanger_id']){
		// 	$reply['readByMsgSender'] = 1;
		// }

		// $this->subadmin_model->insert('user_message_reply', $reply);
		// $this->subadmin_model->update('user_message',array('sender_alert' => 0, 'receiver_alert' => 0),array('id' => $message_id));
		// redirect('subadmin/message_view/'.$message_id);
  //   }

    public function reply($message_id,$messanger_id)
    {
		$this->check_login();
		$reply = array(
						'reply'          => $this->input->post('reply'),
						'created'        => time(),
						'message_id'     => $message_id,
						'replier_id'     => get_subadmin_id(),
  						'messanger_id'   => $messanger_id,
  						'readBySubadmin' => 1
					  );
		if($reply['replier_id'] == $reply['messanger_id']){
			$reply['readByMsgSender'] = 1;
		}

		$message = $this->subadmin_model->get_row('user_message', array('id' => $message_id));
    	// echo "<pre>";

    	if (!empty($message)) {

    		$mailTo = array($message->pr_user_id, $message->sender_id);
    		
    		if (!empty($mailTo)) {

    			$sender = $this->subadmin_model->get_row('users', array('id' => get_subadmin_id()));

    			if (!empty($sender)) {
					$from = array($sender->user_email => $sender->first_name.' '.$sender->last_name);	// From email in array form	
    			}else{
					$from = array('subadmin@vacalio.com' => 'Vacalio Sub-Admin');	// From email in array form	
    			}

				$to = array();
    			foreach ($mailTo as $v1) {
    				$receiver = $this->subadmin_model->get_row('users', array('id' => $v1));
		    		if (!empty($receiver)) {
		    			$to[] = $receiver->user_email;
		    		}
    			}
		    	$message = $this->input->post('reply');

		    	$this->sendReplyMail($from,$to,$message);
    		}
    	}

		$this->subadmin_model->insert('user_message_reply', $reply);
		$this->subadmin_model->update('user_message',array('sender_alert' => 0, 'receiver_alert' => 0),array('id' => $message_id));
		redirect('subadmin/message_view/'.$message_id);
    }

    public function sendReplyMail($from,$to,$message){
		
		$this->load->library('smtp_lib/smtp_email');

		$subject = 'Conversation Notification';	// Subject for email

		$html = "<em><strong>Hello User !</strong></em> <br>
				<p><strong>Subject - ".$subject."</strong></p>
				<p><strong>Reply - ".$message."</strong></p>";
		$this->smtp_email->sendEmail($from, $to, $subject, $html);
	}

 	public function message_view($message_id='', $offset=0)
 	{
		$this->check_login();
 		if(empty($message_id)) redirect('subadmin/messages');
 		$data['message'] =$this->subadmin_model->get_row('user_message',array('id'=>$message_id));

	    ////////// Reply Notification //////////
	    $where = array('message_id' => $message_id);
	    $update = array('readBySubadmin' => 1);
	    $this->subadmin_model->update('user_message_reply',$update,$where);
	    ////////// Reply Notification //////////

	    /////////////////// fetch reply data  ///////////
		$limit=5;
		$data['reply'] = $this->subadmin_model->get_pagination_where('user_message_reply', $limit, $offset, array( 'message_id' =>$message_id ));
 		$data['total_reply'] = $this->subadmin_model->get_pagination_where('user_message_reply', 0, 0, array( 'message_id' =>$message_id ));
        $this->subadmin_model->update('user_message', array('status' => 1), array('id' => $message_id)); 
       ////////////////// end of reply  ///////////////

		$data['template'] = 'subadmin/message_view';
		$this->load->view('templates/subadmin_template',$data);	 		
 	}



	public function ajax_load_more_reply($message_id='', $offset=0)
 	{
		$this->check_login();
 		if(empty($message_id)) redirect('subadmin/messages');
		$limit=5;
		$reply = $this->subadmin_model->get_pagination_where('user_message_reply', $limit, $offset, array( 'message_id' =>$message_id ));
        $res = ""; 
        if(!empty($reply))
        {	
            foreach ($reply as $row)
            {	
            	$user = get_user_info($row->replier_id);

			 	$res .= '<table  style="width:700px; border:5px #F2F2F2 solid; border-top:none; margin-top:20px;" class="table table-condensed table-striped faraz" align="left" >';
				 $res .= '<thead>';
				 $res .= '<tr>';
				 $res .= '<th>';
				 $res .= '<span class="fs1" data-icon="" aria-hidden="true" style="color:#D33F3E; font-size:20px;">';
				 $res .= '<a class="btn btn-small btn-primary hidden-tablet hidden-phone" onclick="return confirm(Are you want to delete?);" href="'.base_url().'subadmin/delete_reply/'.$row->id.'/'.$row->message_id.'" style="margin-left:73%">';	
				 $res .= '<i class="icon-remove"></i></a>';
				 $res .= '</span>';
				 $res .= '</th>';
				 $res .= '</tr>';
				 $res .= '</thead>';
				 $res .= '<tbody>';
				 $res .= '<tr>';
				 $res .= '<td>';
				 $res .= '<div style="color:black; font-size:14px; font-weight:bold ">';
				 $res .= 'Reply:';
				 $res .= '</div>';
				 $res .= '<div style="margin-left:55px">';
				 $res .= $row->reply; 
				 $res .= '</div></td></tr><tr><td><span class="pull-right">'; 
				 $res .= 'Sent by ';
				 $res .= '<span style="color:#D33F3E">'.ucwords($user->first_name.' '.$user->last_name).'</span>)';
				 $res .= 'on '.get_time_ago($row->created);
				 $res .= '</span></td></tr></tbody></table>';
            }
        }
        echo $res;
 	}



    public function delete_reply($id="",$message_id="")

    {

    	if($id=="")

    	{

    		redirect('subadmin/messages');

    	}

    	$this->subadmin_model->delete('user_message_reply',array('id'=>$id));

		$this->session->set_flashdata('success_msg',"Reply has been deleted successfully.");

		redirect('subadmin/message_view/'.$message_id);

    }







 	public function check_unique_slug($slug)

	{

		$slug1  = strtolower(trim(str_replace(' ', '',remove_special_character($slug))));

		$array  = array('slug' => $slug1);

		$result = $this->subadmin_model->get_row('pages',$array);

        if($result=="")

        {

            return true;   

		}

		else

		{

			$this->form_validation->set_message('check_unique_slug','This Slug already exist, please enter other slug');	

			return false;

		}	

	}	



//////////// 10 feb 2014 /////////////



  	public function reviews($offset=0)

	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->review==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['reviews'] = $this->subadmin_model->get_pagination_result('review', $limit, $offset);

		$config= get_theme_pagination();

		$config['base_url'] = base_url().'subadmin/reviews/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('review', 0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

		$data['template'] = 'subadmin/reviews';

		$this->load->view('templates/subadmin_template',$data);

	}



 	public function delete_review($review_id='')

 	{

		$this->check_login();

		if(empty($review_id)) redirect('subadmin/reviews');		

		$this->subadmin_model->delete('review',array('id'=>$review_id));

		$this->session->set_flashdata('success_msg',"Review has been deleted successfully.");

		redirect('subadmin/reviews');

	}



 	public function review_view($review_id='')

 	{

		$this->check_login();

 		if(empty($review_id)) redirect('subadmin/reviews');

 		$data['review'] =$this->subadmin_model->get_row('review',array('id'=>$review_id));

		$data['template'] = 'subadmin/review_view';

		$this->load->view('templates/subadmin_template',$data);	 		

 	}



	public function add_review()

	{

		$reviewer_id = get_subadmin_id();

		$this->check_login(); 

		$this->form_validation->set_rules('pr_id', 'Property Id', 'required');	

		$this->form_validation->set_rules('review', 'Review', 'required');	

		$this->form_validation->set_rules('status', 'Status', 'required');	

		if ($this->form_validation->run() == TRUE)

		{	

		$pr_id = $this->input->post('pr_id');

		$detail = get_property_detail($pr_id);

		$owner_id = $detail->user_id;

		$data=array(

		'review'      => $this->input->post('review'),	

		'property_id' => $pr_id,	

		'user_id'     => $owner_id,

		'customer_id' => $reviewer_id,	

		'status'      => $this->input->post('status'),	

		'created'     => date('Y-m-d H:i:s')	

		);	



		$this->subadmin_model->insert('review',$data);	

		$this->session->set_flashdata('success_msg',"Review has been added successfully.");

		redirect('subadmin/reviews');

		}	

		$data['template'] = 'subadmin/add_review';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function edit_review($review_id='')
	{
		$this->check_login(); 
		if($review_id == '') 
		{
			redirect('subadmin/reviews');
		}
		$data['review']	= $this->subadmin_model->get_row('review', array('id'=>$review_id));

		$this->form_validation->set_rules('review', 'Review', 'required');	
		if ($this->form_validation->run() == TRUE)
		{
			$pr_id = $this->input->post('pr_id');
			$detail = get_property_detail($pr_id);
			@$owner_id = $detail->user_id;

			$data=array(
			'review'      => $this->input->post('review'),	
			'rating'      => $this->input->post('rating'),	
			'status'      => $this->input->post('status')	
			);	

			$this->subadmin_model->update('review' ,$data ,array('id' => $review_id));	
			$this->session->set_flashdata('success_msg',"Review has been updated successfully.");
			redirect('subadmin/reviews');
		}
    	$data['pro'] = get_property_detail($data['review']->property_id);
		$data['template'] = 'subadmin/edit_review';
        $this->load->view('templates/subadmin_template', $data);	
	}



    public function ajax_property_detail($id="")

    {

    	$property = get_property_detail($id);

    	echo $property->featured_image;  

    }

  

   public function group_members($group_id="",$offset=0){

	$this->check_login();

		if($group_id==""){

		redirect('subadmin/groups');

		}

	$limit = 10;

	$data['group_id'] = $group_id;

	$data['template'] = 'subadmin/group_members';

	$this->load->view('templates/subadmin_template', $data);    

    }





    public function customer_group_members($group_id="",$offset=0){

	    $this->check_login();



	    if($group_id==""){

	      redirect('subadmin/groups');

	    }

	    $limit = 10;

	    $data['group_id'] = $group_id;

	    $data['members'] = $this->subadmin_model->get_customer_members($group_id,$limit,$offset);

	    $config= get_theme_pagination();

	    $config['base_url'] = base_url().'subadmin/customer_group_members/'.$group_id;

	    $config['total_rows'] = $this->subadmin_model->get_customer_members($group_id,0,0);

	    $config['per_page'] = $limit;

	    $config['num_links'] = 5;

	    $config['uri_segment'] = 4;   

	    $this->pagination->initialize($config);     

	    $data['pagination'] = $this->pagination->create_links();  

	    $data['template'] = 'subadmin/customer_group_members';

	    $this->load->view('templates/subadmin_template', $data);    

    }



    public function add_customer_to_group($group_id=""){

	    $this->check_login();

	    $this->form_validation->set_rules('user_email', 'Name','required');

	    $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');          

     if($this->form_validation->run()==TRUE){

         $member_id = $this->input->post('user_email');

         $response = $this->subadmin_model->get_row('group_members',array('member_id'=>$member_id,'group_id'=>$group_id));

         if($response==""){

         $member_info = $this->subadmin_model->get_row('users',array('id'=>$member_id));

           $data = array(

                      'member_id'=>$member_info->id,

                      'member_role'=>$member_info->user_role,

                      'group_id'=>$group_id,

                      'created' =>date('Y-m-d h:i:s'),);

           $this->subadmin_model->insert('group_members',$data);

           $this->session->set_flashdata('success_msg','Member has been added successfully');

           redirect('subadmin/customer_group_members/'.$group_id);

         }

         else{

            if($response->member_role==3){

               $this->session->set_flashdata('error_msg','Member Already Exist in the Group as a Property Owner');

               redirect('subadmin/customer_group_members/'.$group_id);

            }

            elseif($response->member_role==4){

               $this->session->set_flashdata('error_msg','Member Already Exist in the Group as a Customer');

               redirect('subadmin/customer_group_members/'.$group_id);

            }

          }

      }

      $data['group_id'] = $group_id;

      $data['template'] = 'subadmin/add_customer_to_group';

      $this->load->view('templates/subadmin_template', $data);    

    }



    public function view_customer_profile($customer_id="",$group_id=""){

	  $this->check_login();

    	if($customer_id==""){

    		redirect('subadmin/customer_group_members');

    	}

      $data['group_id'] = $group_id;

      $data['owner_info'] = $this->subadmin_model->get_row('users',array('id'=>$customer_id));

      $data['template'] = 'subadmin/view_customer_profile';

      $this->load->view('templates/subadmin_template', $data);		

    }



    public function delete_customer_member($id="",$group_id=""){

		$this->check_login();

		if(empty($id)) redirect('subadmin/customer_group_members');		

		$this->subadmin_model->delete('group_members',array('id'=>$id));

		$this->session->set_flashdata('success_msg'," Member and his all Record deleted successfully from this Group.");

	    redirect('subadmin/customer_group_members/'.$group_id);

	}



    public function owner_group_members($group_id="",$offset=0){

	    $this->check_login();



	    if($group_id==""){

	      redirect('subadmin/groups');

	    }

	    $limit = 10;

	    $data['group_id'] = $group_id;

	    $data['members'] = $this->subadmin_model->get_owner_members($group_id,$limit,$offset);

	    $config= get_theme_pagination();

	    $config['base_url'] = base_url().'subadmin/owner_group_members/'.$group_id;

	    $config['total_rows'] = $this->subadmin_model->get_owner_members($group_id,0,0);

	    $config['per_page'] = $limit;

	    $config['num_links'] = 5;

	    $config['uri_segment'] = 4;   

	    $this->pagination->initialize($config);     

	    $data['pagination'] = $this->pagination->create_links();  

		$data['template'] = 'subadmin/owner_group_members';

	    $this->load->view('templates/subadmin_template', $data);    

    }





    public function add_owner_to_group($group_id=""){

      $this->check_login();

      if(empty($group_id))

      {

      	redirect('subadmin/groups');

      }

      $this->form_validation->set_rules('user_email', 'Name','required');

      $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');          

        if($this->form_validation->run()==TRUE){

           $member_id = $this->input->post('user_email');

           $response = $this->subadmin_model->get_row('group_members',array('member_id'=>$member_id,'group_id'=>$group_id));

      if($response==""){

         $member_info = $this->subadmin_model->get_row('users',array('id'=>$member_id));

         $data = array(

                    'member_id'=>$member_info->id,

                    'member_role'=>$member_info->user_role,

                    'group_id'=>$group_id,

                    'created' =>date('Y-m-d h:i:s'),);

         $this->subadmin_model->insert('group_members',$data);

         $this->session->set_flashdata('success_msg','Member has been added successfully');

         redirect('subadmin/owner_group_members/'.$group_id);

      }

          else{

                 $this->session->set_flashdata('error_msg','Member Already Exist in the Group. ');

                 redirect('subadmin/owner_group_members/'.$group_id);

          }

        }

      $data['group_id'] = $group_id;

      $data['template'] = 'subadmin/add_owner_to_group';

      $this->load->view('templates/subadmin_template', $data);    

    }



   public function view_owner_profile($member_id="",$group_id=""){

    $this->check_login();

      if($member_id==""){

        redirect('subadmin/group_members');

      }

      $data['group_id'] = $group_id;

      $data['owner_info'] = $this->subadmin_model->get_row('users',array('id'=>$member_id));

      $data['template'] = 'subadmin/view_owner_profile';

      $this->load->view('templates/subadmin_template', $data);    

  }



	public function delete_owner_member($id="",$group_id=""){

		 $this->check_login();

		 if(empty($id)) redirect('subadmin/customer_group_members');   

		 $this->subadmin_model->delete('group_members',array('id'=>$id));

		 $this->session->set_flashdata('success_msg'," Member and his all Record deleted successfully from this Group.");

		 redirect('subadmin/owner_group_members/'.$group_id);

	}





   public function group_members_properties($group_id="",$offset=0){

       $this->check_login();

		    if($group_id==""){

		      redirect('subadmin/groups');

		    }

	    $limit = 10;

	    $data['group_id'] = $group_id;

	    $data['members_properties'] = $this->subadmin_model->group_members_properties($group_id,$limit,$offset);

	    $config= get_theme_pagination();

	    $config['base_url'] = base_url().'subadmin/group_members_properties/'.$group_id;

	    $config['total_rows'] = $this->subadmin_model->group_members_properties($group_id,0,0);

	    $config['per_page'] = $limit;

	    $config['num_links'] = 5;

	    $config['uri_segment'] = 4;   

	    $this->pagination->initialize($config);     

	    $data['pagination'] = $this->pagination->create_links();  

	    $data['template'] = 'subadmin/group_members_properties';

	    $this->load->view('templates/subadmin_template', $data);    

   }



    public function add_properties_to_group($group_id=""){

    $this->check_login();

    if(empty($group_id))

    {

    	redirect('subadmin/groups');

    }

    $this->form_validation->set_rules('user_name', 'Name','required');

    $this->form_validation->set_rules('property', 'Property','required');

    $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');          

    if($this->form_validation->run()==TRUE){

      $member_id = $this->input->post('user_name');

      $property_id = $this->input->post('property');

      $response = $this->subadmin_model->get_row('group_members_properties',array('member_id'=>$member_id,'property_id'=>$property_id,'group_id'=>$group_id));

      if($response==""){

        $data = array(

                'member_id'=>$member_id,

                'group_id'=>$group_id,

                'property_id'=>$property_id,

                'created' =>date('Y-m-d h:i:s'),);

        $this->subadmin_model->insert('group_members_properties',$data);

        $this->session->set_flashdata('success_msg','Property has been added successfully');

        redirect('subadmin/group_members_properties/'.$group_id);

      }

      else{

        $this->session->set_flashdata('error_msg','Property Already Exist in the Group');

        redirect('subadmin/group_members_properties/'.$group_id);

      }

    }

      $data['group_id'] = $group_id;  

      $data['template'] = 'subadmin/add_properties_to_group';

      $this->load->view('templates/subadmin_template', $data);    

    }



    public function view_member_properties($property_id="",$group_id=""){

		$this->check_login();

    	if($property_id==""){

    		redirect('subadmin/group_members_properties/'.$group_id);

    	}

      $data['group_id'] = $group_id;

      $data['properties_info'] = get_property_detail($property_id);

      $data['template'] = 'subadmin/view_properties_of_group';

      $this->load->view('templates/subadmin_template', $data);		

    }



    public function delete_group_properties($id="",$group_id=""){

		$this->check_login();

		if(empty($id)) redirect('subadmin/groups');		

		$this->subadmin_model->delete('group_members_properties',array('id'=>$id));

		$this->session->set_flashdata('success_msg',"Property has been deleted successfully from the Group.");

		redirect('subadmin/group_members_properties/'.$group_id);

	}



    public function owner_information(){

 		$this->check_login();

 		$user_id    = $this->input->post('user_id');



 		$user_info = $this->subadmin_model->get_row('users',array('id' => $user_id));

 		if(!empty($user_info)){

 		 echo  $user_info->user_email;

 	    }

 	    else{

 	    	echo "No Record Found";

 	    }

 	}



    public function owner_property_info(){

 		$this->check_login();

 		$user_id    = $this->input->post('user_id');



		$properties = $this->subadmin_model->get_result('properties',array('user_id' => $user_id));

		if(!empty($properties)){

			$resp = "";

			foreach($properties as $row){

			$resp.='<div class="unique_feature">';

			$resp.='<input type="radio" name="property" value="'.$row->id.'">';

			$resp.= '&nbsp;&nbsp;<strong>'.$row->title.'</strong>';

			$resp.='</div>';

			}

          echo  $resp;        

	    }

	}	





////////////////  property ////////////////////

    // public function get_neighbourhoot_of_city()

    // {

    // 	$city = $this->input->post('city');

    // 	$nb   = get_neighbourhoot_of_city($city);

    // 	$res  = "";

    // 	if(!empty($nb))

    // 	{

    // 		$res .= "<option value=''>Select neighbour</option>";

	   //  	foreach($nb as $row)

	   //  	{

	   //  		$res .= "<option value='".$row->id."'>".$row->title."</option>";

	   //  	}

	   //  }

	   //  else

	   //  {

    // 		$res = "<option value=''>No Neighbourhood</option>";

	   //  }	

    // 	echo $res;

    // }



	// public function listing_property()
	// {

	// 	$this->check_login();

	// 	$this->form_validation->set_rules('property_title', 'Property Title', 'trim|required');

	// 	$this->form_validation->set_rules('property_descrip', 'Property Description', 'trim|required');

	// 	$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');

	// 	// $this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');

	// 	$this->form_validation->set_rules('address', 'Address ', 'trim|required');

	// 	$this->form_validation->set_rules('city', 'City', 'trim|required');

	// 	$this->form_validation->set_rules('state', 'State', 'trim|required');

	// 	$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');

	// 	$this->form_validation->set_rules('country', 'Country', 'trim|required');

	// 	// $this->form_validation->set_rules('bedroom', 'Bedroom', 'trim|required');

	// 	// $this->form_validation->set_rules('bathroom', 'Bathroom', 'trim|required');

	// 	$this->form_validation->set_rules('accommodates', 'Accommodate', 'trim|required');

	// 	$this->form_validation->set_rules('size', 'Property Size', 'trim|required');

	// 	$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');

	// 	$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	



	// 	if($this->form_validation->run() == TRUE)

	// 	{

	// 		$array= array(

	// 						  'property_title'  => $this->input->post('property_title'),

	// 						  'property_descrip'   => $this->input->post('property_descrip'),

	// 						  'property_type'     => $this->input->post('property_type'),

	// 						  'bed_type'        => $this->input->post('bed_type'),

	// 						  'address'       => $this->input->post('address'),

	// 						  'city'       => $this->input->post('city'),

	// 						  'state'     => $this->input->post('state'),

	// 						  'zipcode'   => $this->input->post('zipcode'),

	// 						  'country'   => $this->input->post('country'),

	// 						  'bed'	      =>$this->input->post('bedroom'),

	// 						  'bath'      =>$this->input->post('bathroom'),

	// 						  'size'      =>$this->input->post('size'),

	// 						  'accommodate'=>$this->input->post('accommodates'),

	// 						  'room_type'  =>$this->input->post('room_type'),

	// 					  	  'latitude'  =>$this->input->post('latitude'),

	// 					  	  'longitude' =>$this->input->post('longitude'),

	// 					  	  'created'   => date('Y-m-d H:i:s')

	// 					);



	// 		if($this->input->post('neighbourhood'))

	// 		{

	// 			$neighbour_id = $this->input->post('neighbourhood');

	// 			$this->session->set_userdata('neighbour_id',$neighbour_id);

	// 		}	



	// 		$this->session->set_userdata('pr_info',$array);

	// 		redirect('subadmin/add_property_detail');

	// 	}

	// 	$data['template'] = 'subadmin/listing_property';

 //        $this->load->view('templates/subadmin_template', $data);	

	// }

	public function get_lat_long($city="")
	{
      if($city=="")
      	return false;

		$location = str_replace(" ","",$city);
		$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$location&sensor=false");
		$json = json_decode($json);
		$latitude_longitude = $json->results[0]->geometry->location;
		$lat_long_array['lat'] =  $latitude_longitude->lat;
		$lat_long_array['lng'] =  $latitude_longitude->lng;
        return $lat_long_array;
	}

	public function add_property()
	{
	    $this->check_login();
		if($_POST)
		{       
            $lat_long_array = get_lat_long($_POST['city']);

			/*Properties Table data starts*/
			$properties_table= array(
								  'title'           => $this->input->post('title'),
								  'description'     => $this->input->post('description'),
								  'application_fee' => $this->input->post('price_amount'),
								  'currency_type'   => $this->input->post('price_type'),
							  	  'latitude'        => $lat_long_array['lat'],
							  	  'longitude'       => $lat_long_array['lng'],
							  	  'created'         => date('Y-m-d H:i:s'),
							  	  'update_calender' => time(),
							  	  'user_id'         => $_POST['user_id'],
							  	  'status'          =>1,
							  	'publish_permission'=>1,
             						);
			if($_FILES['property_profile']['name']!="")
			{
			   $image_name = $this->do_core_upload('property_profile','./assets/uploads/property/');	
			   if($image_name!="")
			   {
			   	$properties_table['featured_image'] = $image_name;
			   }
			   else
			   {
				$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
				redirect('subadmin/add_property');
			   }
			}

			$property_id = $this->subadmin_model->insert('properties',$properties_table);
			/*Properties Table data Ends*/

			/*PR_gallery  Table data Satrts*/
			$p_info = $this->subadmin_model->get_row('properties',array('id'=>$property_id));
			$image_data = array(
								'pr_id'=>$p_info->id,
								'created'=>date('d F Y'),
								'image_file'=>$p_info->featured_image,
								'status'=>1
			                   ); 
			$this->subadmin_model->insert('pr_gallery',$image_data);
			/*PR_gallery  Table data Ends*/
			
			/*Pr_info Table data starts*/
			$pr_info_table = array(
								  'pr_id'        =>$property_id,
								  'property_type_id'=> $this->input->post('property_type'),
								  'room_type'    => $this->input->post('room_type'),
								  'bed'	         => $this->input->post('bedrooms'),
					        'unit_street_address'=> $this->input->post('address'),
								  'city'         => $this->input->post('city'),
								  'state'        => $this->input->post('state'),
								  'zip'      => $this->input->post('zipcode'),
								  'country'      => $this->input->post('country'),
								  'accommodates'  => $this->input->post('accomodates'),
                 			      'square_feet'   =>$_POST['size'],
                 			      'sizeMode'   =>$_POST['size_mode'],
                 				);
			$this->subadmin_model->insert('pr_info',$pr_info_table);
			/*Pr_info Table data Ends*/


			/*Pr_nb_relation Table data Starts*/
			if($this->input->post('neighbourhood'))
			{
				$neighbour_id = $this->input->post('neighbourhood');
				$this->session->set_userdata('neighbour_id',$neighbour_id);
			}

			if($this->session->userdata('neighbour_id'))
			{  

				$parent       = array('pr_nb_id'=>$property_id, 'parent_id'=>0 );
				$parent_id    = $this->subadmin_model->insert('pr_nb_relation',$parent);

			    $neighbour_id = $this->session->userdata('neighbour_id');
				foreach($neighbour_id as $nb_id)
				{

					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
					$status = $this->subadmin_model->insert('pr_nb_relation',$nb);
				}
				$this->session->unset_userdata('neighbour_id');
			}	
			/*Pr_nb_relation Table data Ends*/

				/* Email To User Satrts*/
				$this->load->library('smtp_lib/smtp_email');

		        $email_template = get_manage_email_info('after_adding_property');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $subject = $email_template->subject;
                
                $user_info = get_user_info($_POST['user_id']);
 
                $propertyimage = '<img style="width:100px;height:100px;border:1px solid #101010;border-radius:3px;box-shadow:1px 1px 5px" src='.base_url().'assets/uploads/property/'.$p_info->featured_image.'>';		        
               
                $property_link = '<a href='.base_url().'properties/details/'.$property_id.'>Click Here to view your property </a>';

		        $arr_old = array('&lt;%username%&gt;','&lt;%propertyimage%&gt;','&lt;%propertylink%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($user_info->first_name,$propertyimage,$property_link,$p_info->title,$website_url);

		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		               
		        
		        $to = array($user_info->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				$this->smtp_email->sendEmail($from, $to, $subject, $html);
		       
		        $insert_data = array(
						        	  'name'=>$user_info->first_name,
						        	  'email'=>$user_info->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->subadmin_model->insert('notifications',$insert_data);
		/*Email To User Ends*/




			$this->session->set_flashdata('success_msg',"Property has been added successfully.");
			redirect('subadmin/properties');
		}
		$data['template'] = 'subadmin/listing_property';
        $this->load->view('templates/subadmin_template', $data);	
	}

	  public function address_description($pr_id='')
	  {
	    $this->check_login();
			if(empty($pr_id)) redirect('subadmin/properties');
			// $this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
			// $this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');
			// $this->form_validation->set_rules('title', 'Property Title', 'trim|required');
			// $this->form_validation->set_rules('description', 'Property Description', 'trim|required');
			// $this->form_validation->set_rules('house_rules', 'House Rules', 'trim|required');
			// $this->form_validation->set_rules('house_manual', 'House Manual', 'trim|required');
			// $this->form_validation->set_rules('amenities', 'Ameneties', 'required');
			// $this->form_validation->set_rules('accomodates', 'Accommodate', 'trim|required');
			// $this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');
			// $this->form_validation->set_rules('bedrooms', 'Bedroom', 'trim|required');
			// $this->form_validation->set_rules('bathrooms', 'Bathroom', 'trim|required');
			// $this->form_validation->set_rules('address', 'Address ', 'trim|required');
			// $this->form_validation->set_rules('city', 'City', 'trim|required');
			// $this->form_validation->set_rules('state', 'State', 'trim|required');
			// $this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');
			// $this->form_validation->set_rules('country', 'Country', 'trim|required');
			// $this->form_validation->set_rules('size', 'Property Size', 'trim|required|numeric');
			// $this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
			if($_POST)
			{
               $lat_long_array = get_lat_long($_POST['city']);

				/*Properties table data starts*/
				$properties_table= array(
								     'title'  => $this->input->post('title'),
							    'description' => $this->input->post('description'),
							  	  'latitude'        => $lat_long_array['lat'],
							  	  'longitude'       => $lat_long_array['lng'],
							  	  'created'   => date('Y-m-d H:i:s'),
									'security_deposit'=>$_POST['security_deposit'],
									'application_fee'=>$_POST['price_amount'],
									'currency_type'  =>$_POST['price_type'],
									'cleaning_fee'  =>$_POST['cleaning_fee'],
									'update_calender'=>time(),
	                         );
				$this->subadmin_model->update('properties',$properties_table,array('id'=>$pr_id));
				/*Properties table data Ends*/

				/*Manage listing page work Starts*/
	                $description = $this->input->post('description');
	                if(str_word_count($description)<15)
	                {
	                	$manage_data = array(
	                		                 'publish_permission'=>0,
	                		                 'status'=>0,
	                		                 );
	     			$this->subadmin_model->update('properties',$manage_data,array('id'=>$pr_id));
	                }
				/*Manage listing page work Ends*/



				/*Pr_info table data Starts*/
				$pr_info_table = array(
								  'property_type_id'     => $this->input->post('property_type'),
								  'pr_collect_cat_id'    => $this->input->post('category_type'),
								  'bed_type'        => $this->input->post('bed_type'),
					    'unit_street_address'       => $this->input->post('address'),
								  'city'       => $this->input->post('city'),
								  'state'     => $this->input->post('state'),
								  'zip'   => $this->input->post('zipcode'),
								  'country'   => $this->input->post('country'),
								  'bed'	      =>$this->input->post('bedrooms'),
								  'bath'      =>$this->input->post('bathrooms'),
								  'square_feet'      =>$this->input->post('size'),
								  'sizeMode' => $this->input->post('sizeMode'),
								  'accommodates'=>$this->input->post('accomodates'),
								  'room_type'  =>$this->input->post('room_type'),
								  'house_rules'  =>$this->input->post('house_rules'),
								  'house_manual'  =>$this->input->post('house_manual'),

					            ); 
				$this->subadmin_model->update('pr_info',$pr_info_table,array('pr_id'=>$pr_id));
				/*Pr_info table data Ends*/

				/*Pr_ameneties table data Starts*/

			   $response = $this->subadmin_model->get_row('pr_amenities',array('pr_id'=>$pr_id)); 
	          
	          if(empty($response))
	          {
				if(isset($_POST['amenities'])){
					for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
						if(!empty($_POST['amenities'][$i])){
							$pr_amenities=array(
							'pr_id'=>$pr_id,
							'pr_amenity_id'=>trim($_POST['amenities'][$i])
							);
							$this->subadmin_model->insert('pr_amenities',$pr_amenities);	
						}
					}
				}
	          }
	          else
	          {
				if(isset($_POST['amenities'])){
					$flag_pf=TRUE;
					for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
						if($flag_pf){
							$this->subadmin_model->delete('pr_amenities',array('pr_id'=>$pr_id));
							$flag_pf=FALSE;
						}
						if(!empty($_POST['amenities'][$i])){
							$pr_amenities=array(
							'pr_id'=>$pr_id,
							'pr_amenity_id'=>$_POST['amenities'][$i]
							);
							$this->subadmin_model->insert('pr_amenities',$pr_amenities,array('pr_id' =>$pr_id));	
						}
					}
				}
	          }
				/*Pr_ameneties table data Ends*/


				/*Pr_nb_relation table data Starts*/

				if($this->input->post('neighbourhood'))
				{
					$neighbour_id = $this->input->post('neighbourhood');
					$this->session->set_userdata('neighbour_id',$neighbour_id);
				}
				if($this->session->userdata('neighbour_id'))
				{
					$neighbour_list = get_neighbourhoot_of_property($pr_id);
					
					$this->subadmin_model->delete('pr_nb_relation', array('pr_nb_id' => $pr_id, 'parent_id'=>0));
					if(!empty($neighbour_list))
					{
						foreach($neighbour_list as $list)
						{
							$this->subadmin_model->delete('pr_nb_relation', array('id'=>$list->id) );
						}
					}

					$parent     = array('pr_nb_id'=>$pr_id, 'parent_id'=>0 );
					$parent_id  = $this->subadmin_model->insert('pr_nb_relation',$parent);
					$neighbour_id   = $this->session->userdata('neighbour_id');
					foreach($neighbour_id as $nb_id)
					{
						$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
						$this->subadmin_model->insert('pr_nb_relation',$nb);
					}
					$this->session->unset_userdata('neighbour_id');

				}	
	  			/*Pr_nb_relation table data Ends*/
	     		$this->session->set_flashdata('success_msg','Property has been updated succesfullly');
				redirect('subadmin/address_description/'.$pr_id);

	    }		
		$data['properties'] = $this->subadmin_model->get_row('properties',array('id'=>$pr_id));
		$data['pr_amenities'] = $this->subadmin_model->get_result('pr_amenities',array('pr_id'=>$pr_id));
		$data['pr_info'] = $this->subadmin_model->get_row('pr_info',array('pr_id'=>$pr_id));
	    $data['template'] = 'subadmin/address_and_description';
	    $this->load->view('templates/subadmin_template', $data);
	  }


	public function get_neighbour_city()
	{  

		$city_name = $_POST['city_name']; 
		$city_row =  $this->subadmin_model->get_row('cities',array('city'=>$city_name));
        if(!empty($city_row))
        {
        	echo $city_row->id;
        }
        else
        {
        	return false;
        }
	}

    

    public function get_neighbourhoot_of_city()
    {
    	$city = $this->input->post('city');
    	$nb   = get_neighbourhoot_of_city($city);
    	$res  = "";
    	if(!empty($nb))
    	{
    		$res .= "<option value=''>Select neighbour</option>";
	    	foreach($nb as $row)
	    	{
	    		$res .= "<option value='".$row->id."'>".$row->title."</option>";
	    	}
	    }
	    else
	    {
    		$res = "<option value=''>No Neighbourhood</option>";
	    }	
    	echo $res;
    }



	public function add_property_detail()

	{

		$this->check_login();

		$this->form_validation->set_rules('rent', 'Property Rent', 'trim|required');

		$this->form_validation->set_rules('room_allotment', 'Room Allotment', 'trim|required');

		$this->form_validation->set_rules('deposit', 'Property Deposit', 'trim|required');

		$this->form_validation->set_rules('appliction_fee', 'Appliction Fee', 'trim|required');

		$this->form_validation->set_rules('contact_name', 'Contact Name ', 'trim|required');

		$this->form_validation->set_rules('user_email', 'User Email', 'trim|required');

		$this->form_validation->set_rules('confirm_email', 'Confirm Email', 'trim|required');

		// $this->form_validation->set_rules('website', 'Website', 'trim|required');

		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');

		$this->form_validation->set_rules('collection_category', 'Collection Category', 'trim|required');

		$this->form_validation->set_rules('featured_image', 'Featured Image', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	

		

	    if($this->input->post('featured_listing')=="1"){

	    	$featured_listing = 1;

	    }

	    else{

	    	$featured_listing = 0;

	    }



		if($this->form_validation->run() == TRUE){

			$array= array(

							  'rent'             => $this->input->post('rent'),

							  'room_allotment'   => $this->input->post('room_allotment'),
                			  
                			  'currency_type'    => 'USD',
							  
							  'deposit'          => $this->input->post('deposit'),

							  'appliction_fee'   => $this->input->post('appliction_fee'),

							  'contact_name'     => $this->input->post('contact_name'),

							  'confirm_email'    => $this->input->post('confirm_email'),

							  'website'          => $this->input->post('website'),

							  'user_id'          => $this->input->post('user_id'),

							  'featured_image'   =>$this->input->post('featured_image'),

							  'pr_collect_cat_id'=> $this->input->post('collection_category'),

							  'phone'            => $this->input->post('phone')

						);

			

			

			$arr=$this->session->userdata('pr_info');

			

			$properties= array( 

				'title'          =>$arr['property_title'],

				'description'    => $arr['property_descrip'],

				'room_allotment' => $array['room_allotment'],

				'deposit_amount' => $array['deposit'],

				'application_fee'=> $array['appliction_fee'],

				'slug'     =>str_replace(' ','-', $arr['property_title']),

				'created'  =>$arr['created'],

				'user_id'  =>$array['user_id'],

				'featured_image'=>$array['featured_image'],

				'latitude' =>$arr['latitude'],

				'longitude'=>$arr['longitude'],

				'featured_listing_status'=>$featured_listing,

				);

			



			$property_id=$this->subadmin_model->insert('properties',$properties);



			if($this->session->userdata('neighbour_id'))

			{

				$parent       = array('pr_nb_id'=>$property_id, 'parent_id'=>0 );

				$parent_id    = $this->subadmin_model->insert('pr_nb_relation',$parent);

				$neighbour_id = $this->session->userdata('neighbour_id');

				foreach($neighbour_id as $nb_id)

				{

					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );

					$this->subadmin_model->insert('pr_nb_relation',$nb);

				}

				$this->session->unset_userdata('neighbour_id');

			}	





			$properties_info=array( 

									'pr_id'=>$property_id,

									'unit_street_address'=> $arr['address'],

									'property_type_id'=>$arr['property_type'],

									'city'=> $arr['city'],

									'rent'=> $array['rent'],

                    				'image'=>$array['featured_image'],

									'state'=> $arr['state'],

									'country'=> $arr['country'],

									'bed_type'=>$arr['bed_type'],

									'bed'=>$arr['bed'],

									'bath'=>$arr['bath'],

									'accommodates'=>$arr['accommodate'],

									'square_feet'=>$arr['size'],

									'room_type'=>$arr['room_type'],

									'pr_collect_cat_id'=>$array['pr_collect_cat_id'],

									'zip'=>$arr['zipcode'],

								);

								$properties_contact_info=array( 

									'pr_id'=>$property_id,

									'contact_name'=> $array['contact_name'],

									'phone'=> $array['phone'],

									'contact_email'=> $array['confirm_email'],

									'website'=> $array['website'],

									

								);





			



			$this->subadmin_model->insert('pr_info',$properties_info);

			$status = $this->subadmin_model->insert('pr_contact_info',$properties_contact_info);

			if(isset($_POST['amenities'])){

				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 

					if(!empty($_POST['amenities'][$i])){

						$pr_amenities=array(

						'pr_id'=>$property_id,

						'pr_amenity_id'=>trim($_POST['amenities'][$i])

						);

						$this->subadmin_model->insert('pr_amenities',$pr_amenities);	

					}

				}

			}

			if(isset($_POST['property_image'])){

				for ($i=0; $i <count($_POST['property_image']) ; $i++) { 

					if(!empty($_POST['property_image'][$i])){

						$pr_images=array(

						'pr_id'=>$property_id,

						'user_id'=>$array['user_id'],

						'image_file'=>trim($_POST['property_image'][$i])

						);

						$this->subadmin_model->insert('pr_gallery',$pr_images);	

					}

				}

			}





			if($status){

				$this->session->unset_userdata('pr_info');

				$this->session->set_flashdata('success_msg',"Property has been added successfully.");

				redirect('subadmin/properties');

			}

		}

		$data['template'] = 'subadmin/add_pr_detail';

        $this->load->view('templates/subadmin_template', $data);

	}



	public function delete_property($pr_id='')
	{	
		$this->check_login();

		if(empty($pr_id)) redirect('subadmin/properties');		

        $this->subadmin_model->delete('pr_calender',array('property_id'=>$pr_id));   
        $this->subadmin_model->delete('listing_price',array('listing_id'=>$pr_id));   
        $this->subadmin_model->delete('property_rating',array('pr_id'=>$pr_id));   
        $this->subadmin_model->delete('property_email_record',array('id'=>$pr_id));   
        $this->subadmin_model->delete('property_notes',array('id'=>$pr_id));   
        $this->subadmin_model->delete('review',array('property_id'=>$pr_id));  
        $pr_nb = $this->subadmin_model->get_row('pr_nb_relation',array('pr_nb_id'=>$pr_id,'parent_id'=>0)); 
        $this->subadmin_model->delete('pr_nb_relation',array('parent_id'=>$pr_nb->id));  
        $pr_gallery = $this->subadmin_model->get_result('pr_gallery',array('pr_id'=>$pr_id)); 
   	    if(empty($pr_gallery))
   	    {
   	    	foreach ($pr_gallery as $value)
   	    	{
               @unlink('.assets/uploads/property/'.$value->image_file);
   	    	}
   	    }

		$this->subadmin_model->update('properties',array('is_delete'=>1,'status'=>0),array('id'=>$pr_id));

		$this->session->set_flashdata('success_msg',"property has been deleted successfully.");

		redirect('subadmin/properties');

	}



	public function edit_property($pr_id='')

	{

		$this->check_login();

		if(empty($pr_id)) redirect('subadmin/properties');

		$data['properties'] = $this->subadmin_model->get_row('properties',array('id'=>$pr_id));

		$data['pr_amenities'] = $this->subadmin_model->get_row('pr_amenities',array('pr_id'=>$pr_id));

		$data['pr_contact_info'] = $this->subadmin_model->get_row('pr_contact_info',array('pr_id'=>$pr_id));

		$data['pr_info'] = $this->subadmin_model->get_row('pr_info',array('pr_id'=>$pr_id));

		$this->form_validation->set_rules('property_title', 'Property Title', 'trim|required');

		$this->form_validation->set_rules('property_descrip', 'Property Description', 'trim|required');

		$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');

		$this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');

		$this->form_validation->set_rules('address', 'Address ', 'trim|required');

		$this->form_validation->set_rules('city', 'City', 'trim|required');

		$this->form_validation->set_rules('state', 'State', 'trim|required');

		$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required');

		$this->form_validation->set_rules('country', 'Country', 'trim|required');

		$this->form_validation->set_rules('bedroom', 'Bedroom', 'trim|required');

		$this->form_validation->set_rules('bathroom', 'Bathroom', 'trim|required');

		$this->form_validation->set_rules('accommodates', 'Accommodate', 'trim|required');

		$this->form_validation->set_rules('size', 'Property Size', 'trim|required');

		$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	

		if($this->form_validation->run() == TRUE){

			$array= array(

							  'property_title'  => $this->input->post('property_title'),

							  'property_descrip'   => $this->input->post('property_descrip'),

							  'property_type'     => $this->input->post('property_type'),

							  'bed_type'        => $this->input->post('bed_type'),

							  'address'       => $this->input->post('address'),

							  'city'       => $this->input->post('city'),

							  'state'     => $this->input->post('state'),

							  'zipcode'   => $this->input->post('zipcode'),

							  'country'   => $this->input->post('country'),

							  'bed'	      =>$this->input->post('bedroom'),

							  'bath'      =>$this->input->post('bathroom'),

							  'size'      =>$this->input->post('size'),

							  'accommodate'=>$this->input->post('accommodates'),

							  'room_type'  =>$this->input->post('room_type'),

						  	  'latitude'  =>$this->input->post('latitude'),

						  	  'longitude' =>$this->input->post('longitude'),

						  	  'created'   => date('Y-m-d H:i:s')

						);



			if($this->input->post('neighbourhood'))

			{

				$neighbour_id = $this->input->post('neighbourhood');

				$this->session->set_userdata('neighbour_id',$neighbour_id);

			}

			

			$this->session->set_userdata('pr_info',$array);

			redirect('subadmin/edit_property_detail/'.$pr_id);

		}



		$data['template'] = 'subadmin/edit_property';

        $this->load->view('templates/subadmin_template', $data);

	}



	public function edit_property_detail($pr_id='')

	{

		$this->check_login();

		if(empty($pr_id)) redirect('subadmin/properties');

		$data['properties'] = $this->subadmin_model->get_row('properties',array('id'=>$pr_id));

		$data['pr_amenities'] = $this->subadmin_model->get_result('pr_amenities',array('pr_id'=>$pr_id));

		$data['pr_contact_info'] = $this->subadmin_model->get_row('pr_contact_info',array('pr_id'=>$pr_id));

		$data['pr_info'] = $this->subadmin_model->get_row('pr_info',array('pr_id'=>$pr_id));

		$data['pr_gallery']=$this->subadmin_model->get_result('pr_gallery',array('pr_id'=>$pr_id));

		$this->form_validation->set_rules('rent', 'Property Rent', 'trim|required');

		$this->form_validation->set_rules('room_allotment', 'Room Allotment', 'trim|required');

		$this->form_validation->set_rules('deposit', 'Property Deposit', 'trim|required');

		$this->form_validation->set_rules('appliction_fee', 'Appliction Fee', 'trim|required');

		// $this->form_validation->set_rules('contact_name', 'Contact Name ', 'trim|required');

		// $this->form_validation->set_rules('user_email', 'User Email', 'trim|required');

		// $this->form_validation->set_rules('confirm_email', 'Confirm Email', 'trim|required');

		// $this->form_validation->set_rules('website', 'Website', 'trim|required');

		$this->form_validation->set_rules('collection_category', 'Collection Category', 'trim|required');

		// $this->form_validation->set_rules('phone', 'Phone', 'trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	

		 

	    if($this->input->post('featured_listing')=="1"){

	    	$featured_listing = 1;

	    }

	    else{

	    	$featured_listing = 0;

	    }



		if($this->form_validation->run() == TRUE){

			$array= array(

							  'rent'  => $this->input->post('rent'),

							  'room_allotment'   => $this->input->post('room_allotment'),

							  'deposit'     => $this->input->post('deposit'),

							  'appliction_fee'        => $this->input->post('appliction_fee'),

							  'contact_name'       => $this->input->post('contact_name'),

							 

							  'confirm_email'  => $this->input->post('confirm_email'),

							  'website'  => $this->input->post('website'),

							 

							  'user_id' =>$this->input->post('user_id'),

							  'pr_collect_cat_id'  => $this->input->post('collection_category'),

							  'phone'  => $this->input->post('phone')

						);



			if(isset($_POST['property_image'])){

				if(!empty($data['pr_gallery'])){

					foreach ($data['pr_gallery'] as $row){

						if($row->image_file !=''){

						$path = './assets/uploads/property/';

						$thumbpath = './assets/uploads/property/thumbnail/';

							$file = $row->image_file;

							if(!empty($file)){

								@unlink($path.$file);

								@unlink($thumbpath.$file);

							}

				    	}

					}

				}

			}

			

			$arr=$this->session->userdata('pr_info');

			

			$properties= array( 

				'title'=>$arr['property_title'],

				'description'=> $arr['property_descrip'],

				'room_allotment'=> $array['room_allotment'],

				'deposit_amount'=> $array['deposit'],

				'application_fee'=> $array['appliction_fee'],

				'slug'=> str_replace(' ','-', $arr['property_title']),

				'created'=> $arr['created'],

				'user_id'=> $array['user_id'],

				

				'latitude' =>$arr['latitude'],

				'longitude'=>$arr['longitude'],

				'featured_listing_status'=>$featured_listing,



				);

			if(isset($_POST['featured_image'])){

				$properties['featured_image']= $this->input->post('featured_image');

			}





			if($this->session->userdata('neighbour_id'))

			{

				$neighbour_list = get_neighbourhoot_of_property($pr_id);

				

				$this->subadmin_model->delete('pr_nb_relation', array('pr_nb_id' => $pr_id, 'parent_id'=>0));

				if(!empty($neighbour_list))

				{

					foreach($neighbour_list as $list)

					{

						$this->subadmin_model->delete('pr_nb_relation', array('id'=>$list->id) );

					}

				}



				$parent     = array('pr_nb_id'=>$pr_id, 'parent_id'=>0 );

				$parent_id  = $this->subadmin_model->insert('pr_nb_relation',$parent);

				$neighbour_id   = $this->session->userdata('neighbour_id');

				foreach($neighbour_id as $nb_id)

				{

					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );

					$this->subadmin_model->insert('pr_nb_relation',$nb);

				}

				$this->session->unset_userdata('neighbour_id');

			}	









			$this->subadmin_model->update('properties',$properties,array('id' =>$pr_id));

			$properties_info=array( 

									

									'unit_street_address'=> $arr['address'],

									'property_type_id'=>$arr['property_type'],

									'city'=> $arr['city'],

									'rent'=> $array['rent'],

									'state'=> $arr['state'],

									'image'=>$this->input->post('featured_image'),

									'country'=> $arr['country'],

									'bed_type'=>$arr['bed_type'],

									'bed'=>$arr['bed'],

									'bath'=>$arr['bath'],

									'accommodates'=>$arr['accommodate'],

									'square_feet'=>$arr['size'],

									'room_type'=>$arr['room_type'],

									'pr_collect_cat_id'=>$array['pr_collect_cat_id'],

									'zip'=>$arr['zipcode'],

								);

			$properties_contact_info=array( 

									

									'contact_name'=> $array['contact_name'],

									'phone'=> $array['phone'],

									'contact_email'=> $array['confirm_email'],

									'website'=> $array['website'],

									

								);



			$this->subadmin_model->update('pr_info',$properties_info,array('pr_id' =>$pr_id));

			$status = $this->subadmin_model->update('pr_contact_info',$properties_contact_info,array('pr_id' =>$pr_id));

			if(isset($_POST['amenities'])){

				$flag_pf=TRUE;

				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 

					if($flag_pf){

						$this->subadmin_model->delete('pr_amenities',array('pr_id'=>$pr_id));

						$flag_pf=FALSE;

					}

					if(!empty($_POST['amenities'][$i])){

						$pr_amenities=array(

						'pr_id'=>$pr_id,

						'pr_amenity_id'=>$_POST['amenities'][$i]

						);

						$this->subadmin_model->insert('pr_amenities',$pr_amenities,array('pr_id' =>$pr_id));	

					}

				}

			}

			if(isset($_POST['property_image'])){

				$flag_im=TRUE;

				for ($i=0; $i <count($_POST['property_image']) ; $i++) { 

						if($flag_im){

							$this->subadmin_model->delete('pr_gallery',array('pr_id'=>$pr_id));

							$flag_im=FALSE;

						}

						if(!empty($_POST['property_image'][$i])){

						$pr_images=array(

						'pr_id'=>$pr_id,

						'user_id'=>$array['user_id'],

						'image_file'=>trim($_POST['property_image'][$i])

						);

						$this->subadmin_model->insert('pr_gallery',$pr_images);	

					}

				}

			}

			if($status){

				$this->session->unset_userdata('pr_info');

				$this->session->set_flashdata('success_msg',"Property has been updated successfully.");

				redirect('subadmin/properties');

			}

		}



		$data['template'] = 'subadmin/edit_pr_detail';

        $this->load->view('templates/subadmin_template', $data);

	}







/////////////////////////////////////////////////////////////////////



    	// Nebourhood Post



    	public function neb_post($offset=0){

			$this->check_login();



			$limit=10;

			$data['blog']=$this->admin_model->blog($limit,$offset);

			$config= get_theme_pagination();	

			$config['base_url'] = base_url().'index.php/admin/blog/';

			$config['total_rows'] = $this->admin_model->blog(0, 0);

			$config['per_page'] = $limit;

			// $config['num_links'] = 5;		

			$this->pagination->initialize($config); 		

			$data['pagination'] = $this->pagination->create_links();			



	        $data['template'] = 'subadmin/blog';

	        $this->load->view('templates/admin_template', $data);			

		}





		public function add_neb_post(){

			$this->check_login();



			$this->form_validation->set_rules('post_title', 'post_title', 'required');			

			$this->form_validation->set_rules('excerpt', 'excerpt', 'required');			

			$this->form_validation->set_rules('description', 'description', 'required');			

			$this->form_validation->set_rules('category', 'category', 'required');			

			$this->form_validation->set_rules('post_status', 'post_status', 'required');			

			if ($this->form_validation->run() == TRUE){			

				$data=array(

					'post_title'=>$this->input->post('post_title'),				

					'post_excerpt'=>$this->input->post('excerpt'),				

					'content'=>$this->input->post('description'),				

					'category_id'=>$this->input->post('category'),							

					'post_status'=>$this->input->post('post_status'),							

					'post_type'=>1,							

					'created' => date('Y-m-d H:i:s')		

					);			



				if($_FILES['userfile']['name'] !=""){

					$data['post_image'] = $this->do_core_upload('userfile','./assets/uploads/blog/'); //./assets/uploads/custom_uploads/

					if(!$data['post_image']){

						$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');

						redirect('index.php/admin/blog');

					}				

				}

				$this->admin_model->insert('posts',$data);		

				$this->session->set_flashdata('success_msg',"posts has been added successfully.");

				redirect('index.php/admin/blog');

			}

			$data['category'] = $this->admin_model->get_result('post_categories');

			$data['template'] = 'subadmin/add_blog_post';

	        $this->load->view('templates/admin_template', $data);		

		}



		public function edit_neb_post($id=''){

			$this->check_login();



			$data['blog'] = $this->admin_model->get_row('posts', array('id'=>$id));

			if (empty($data['blog'])) {

				$this->session->set_flashdata('error_msg',"No post found.");

				redirect('index.php/admin/blog');

			}

			$this->form_validation->set_rules('post_title', 'post_title', 'required');			

			$this->form_validation->set_rules('excerpt', 'excerpt', 'required');			

			$this->form_validation->set_rules('description', 'description', 'required');			

			$this->form_validation->set_rules('category', 'category', 'required');			

			$this->form_validation->set_rules('post_status', 'post_status', 'required');			

			if ($this->form_validation->run() == TRUE){			

				$update_data=array(

					'post_title'=>$this->input->post('post_title'),				

					'post_excerpt'=>$this->input->post('excerpt'),				

					'content'=>$this->input->post('description'),				

					'category_id'=>$this->input->post('category'),												

					'post_status'=>$this->input->post('post_status'),												

					);





				if($_FILES['userfile']['name'] !=""){ 



					$post_image = $this->do_core_upload('userfile', './assets/uploads/blog/');

					if(!$post_image){

						$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');

						redirect('index.php/admin/edit_blog_post/'.$id);

					}else{

						if(!empty($data['blog']->post_image)){

							$path = './assets/uploads/blog/';

							$delete_file = $data['blog']->post_image;

							@unlink($path.$delete_file);

							// @unlink($path.'thumbs/'.$delete_file);

							}

						$update_data['post_image'] = $post_image;

						// $this->create_thumb($update_data['post_image']);

					}			



				}



				$this->admin_model->update('posts',$update_data, array('id'=>$id));		

				$this->session->set_flashdata('success_msg',"posts has been updated successfully.");

				redirect('index.php/admin/blog');

			}

			$data['category'] = $this->admin_model->get_result('post_categories');		

			$data['template'] = 'subadmin/edit_blog_post';

	        $this->load->view('templates/admin_template', $data);	

	        

		}



		public function delete_neb_post($id){

			$this->check_login();



			$data = $this->admin_model->get_row('posts', array('id'=> $id));

			if (empty($data)) {

				$this->session->set_flashdata('error_msg',"No post found.");

				redirect('index.php/admin/blog');

			}

			if(!empty($data->post_image)){

				$path = './assets/uploads/blog/';

						$delete_file = $data->post_image;

						@unlink($path.$delete_file);

						// @unlink($path.'thumbs/'.$delete_file);

			}

			$resp = $this->admin_model->delete('posts',array('id'=>$id));

			if ($resp) {

				$this->session->set_flashdata('success_msg',"Post deleted.");

				redirect('index.php/admin/Post');

			}else{

				$this->session->set_flashdata('error_msg',"No Post found.");

				redirect('index.php/admin/blog');

			}

		}

	// Nebourhood Post



	// Neighbourhood Category



		public function neb_category($offset=0)
		{
			$this->check_login();
			$limit=10;
			$data['categories']=$this->subadmin_model->get_pagination_result('neb_category',$limit, $offset);
			$config= get_theme_pagination();	
			$config['base_url'] = base_url().'subadmin/neb_category/';
			$config['total_rows'] = $this->subadmin_model->get_pagination_result('neb_category',0,0);
			$config['per_page'] = $limit;
			$config['num_links'] =3;		
			$this->pagination->initialize($config); 		
			$data['pagination'] = $this->pagination->create_links();
			$data['total_row'] = $config['total_rows'];
			$data['template'] = 'subadmin/neb_category';
	        $this->load->view('templates/subadmin_template', $data);	
		}



		public function add_neb_category()
		{
			$this->check_login();
			$this->form_validation->set_rules('category_name', 'Category Name', 'required');
			$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	
			if($this->form_validation->run() == TRUE)
			{
				$array= array(
							  'category_name' 	=>	$this->input->post('category_name'),
							  'created'       	=>	date('Y-m-d H:i:s'),
							  'updated'       	=>	date('Y-m-d H:i:s')
							 );
				$this->subadmin_model->insert('neb_category',$array);
				$this->session->set_flashdata('success_msg',"Neighbourhood Category has been added successfully.");
				redirect('subadmin/neb_category');
			}
			$data['template'] = 'subadmin/add_neb_category';
	        $this->load->view('templates/subadmin_template', $data);	
		}

		public function delete_neb_category($cate_id='')
		{
			$this->check_login();
			if(empty($cate_id)) redirect('subadmin/neb_category');	
			$this->subadmin_model->delete('neb_category',array('id'=>$cate_id));
			$this->subadmin_model->delete('neighbour_cat',array('category'=>$cate_id));
			$this->session->set_flashdata('success_msg',"Neighbourhood Category has been deleted successfully.");
			redirect('subadmin/neb_category');
		}
	// Neighbourhood Category







	// Neighbourhood City

	public function add_neb_city()

	{		

		$this->check_login();

		$this->form_validation->set_rules('city', 'City', 'required');

		$this->form_validation->set_rules('city_description', 'City Description', 'required');		

		$this->form_validation->set_rules('known_for', 'Known For', 'required');		

		$this->form_validation->set_rules('get_around_with', 'Get Around With', 'required');

		$this->form_validation->set_rules('userfile1', 'City Image', 'callback_check_city_img');

		$this->form_validation->set_rules('userfile2', 'City Image', 'callback_check_city_thumb_img');

        $this->form_validation->set_error_delimiters('<span style="color:#D33F3E"><span>');

		if ($this->form_validation->run() == TRUE)

		{

			$image = $this->do_core_upload('userfile1','./assets/uploads/cities/');

			if (!$image)

			{

				$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");

				redirect('subadmin/cities');

			}



			$thumbnail = $this->do_core_upload('userfile2','./assets/uploads/cities/thumbnails/');

			if (!$image)

			{

				$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");

				redirect('subadmin/cities');

			}



			$city=array(

				'city'				=>	$this->input->post('city'),	

				'city_image'    	=> 	$image, 

				'thumbnail'     	=> 	$thumbnail, 

				'city_description' 	=>	$this->input->post('city_description'),

				'is_home' 			=>	$this->input->post('is_home'),

				'known_for'			=>	strtolower($this->input->post('known_for')),

				'get_around_with'	=>	strtolower($this->input->post('get_around_with')),

				'updated' 			=>	date('Y-m-d'),

				'created' 			=>	date('Y-m-d')

				);

			

			$user_id=$this->subadmin_model->insert('cities',$city);

			$this->session->set_flashdata('success_msg',"City has been added successfully.");

			redirect('subadmin/cities');

		}



        $data['template'] = 'subadmin/add_neb_city';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function edit_neb_city($city_id = "")

	{		

		$this->check_login();

		$data['city_info'] = $this->subadmin_model->get_row('cities',array('id'=>$city_id));

		if(empty($data['city_info'])) redirect('subadmin/cities');

		$image     = $data['city_info']->city_image;

		$thumbnail = $data['city_info']->thumbnail;

		$this->form_validation->set_rules('city', 'City', 'required');

		$this->form_validation->set_rules('city_description', 'City Description', 'required');		

		$this->form_validation->set_rules('known_for', 'Known For', 'required');		

		$this->form_validation->set_rules('get_around_with', 'Get Around With', 'required');

        $this->form_validation->set_error_delimiters('<span style="color:#D33F3E"><span>');

        

		if ($this->form_validation->run() == TRUE)

		{

			if ($_FILES['city_image']['name']!='') 

			{

				$image = $this->do_core_upload('city_image','./assets/uploads/cities/');

				if (!$image) {

					$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");

					redirect('subadmin/cities');

				}

				if (!empty($data['city_info']->city_image)) {

					$path = './assets/uploads/cities/';

					$delete_file = $data['city_info']->city_image;

					@unlink($path.$delete_file);

				}

			}



			if ($_FILES['city_thumbnail']['name']!='') 

			{

				$thumbnail = $this->do_core_upload('city_thumbnail','./assets/uploads/cities/thumbnails/');

				if (!$thumbnail) 

				{

					$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");

					redirect('subadmin/cities');

				}

				if (!empty($data['city_info']->thumbnail)) 

				{

					$path = './assets/uploads/cities/thumbnails/';

					$delete_file = $data['city_info']->thumbnail;

					@unlink($path.$delete_file);

				}

			}



			$city=array(

				'city'				=>	$this->input->post('city'),	

				'thumbnail'         =>  $thumbnail,

				'city_image'        =>  $image,

				'city_description' 	=>	$this->input->post('city_description'),

				'known_for'			=>	strtolower($this->input->post('known_for')),

				'is_home' 			=>	$this->input->post('is_home'),

				'get_around_with'	=>	strtolower($this->input->post('get_around_with')),

				'updated' 			=>	date('Y-m-d'),

				'created' 			=>	date('Y-m-d')

				);

		

			$user_id=$this->subadmin_model->update('cities',$city, array('id' =>$city_id));

			$this->session->set_flashdata('success_msg',"City has been updated successfully.");

			redirect('subadmin/cities');

		}

        $data['template'] = 'subadmin/edit_neb_city';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_neb_city($city_id=''){

		$this->check_login();

		$city_info = $this->subadmin_model->get_row('cities',array('id'=>$city_id));



		if(empty($city_info))

			redirect('subadmin/cities');



		if (!empty($city_info->city_image)) {

			$path = './assets/uploads/cities/';

			$delete_file = $city_info->city_image;

			@unlink($path.$delete_file);

		}

		if (!empty($city_info->thumbnail)) {

			$path = './assets/uploads/cities/thumbnails/';

			$delete_file = $city_info->thumbnail;

			@unlink($path.$delete_file);

		}

		$this->subadmin_model->delete('cities',array('id'=>$city_id));

		$this->session->set_flashdata('success_msg',"City has been deleted successfully.");

		redirect('subadmin/cities');

	}



	public function cities($offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->city==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['cities']=$this->subadmin_model->get_pagination_result('cities',$limit, $offset);

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/cities/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('cities',0,0);

		$config['per_page'] = $limit;

		$config['num_links'] =3;		

		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		//$data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Groups' => '/subadmin/groups'));



		$data['template'] = 'subadmin/cities';

        $this->load->view('templates/subadmin_template', $data);	

	}



	public function check_city_img($a)

	{

       if ($_FILES['userfile1']['tmp_name'] == '') 

       {

           $this->form_validation->set_message('check_city_img', 'City image is required.');

            return FALSE;

       }

       else

       		return TRUE;

    }



    public function check_city_thumb_img($a)

	{

       if ($_FILES['userfile2']['tmp_name'] == '') 

       {

           $this->form_validation->set_message('check_city_thumb_img', 'Thumbnail image is required.');

            return FALSE;

       }

       else

       		return TRUE;

    }

	// Neighbourhood City

	// Neighbourhood
	public function add_neighbour()
	{		
		$this->check_login();
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_error_delimiters('<span style="color:#D33F3E"><span>');
		if ($this->form_validation->run() == TRUE)
		{
		
			$image = $this->do_core_upload('userfile','./assets/uploads/neighbourhoods/');
			if (!$image) 
			{
				$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");
				redirect('subadmin/neighbourhoods');
			}

			$homeImage = $this->do_core_upload('homeImage','./assets/uploads/neighbourhoods/');
			if (!$homeImage) 
			{
				$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");
				redirect('subadmin/neighbourhoods');
			}

			$neighbour=array(
					'city'				=>	$this->input->post('city'),	
					'title'				=>	$this->input->post('title'),	
					'banner'    		=> 	$image, 
					'homeImage'    		=> 	$homeImage, 
					'description' 		=>	$this->input->post('description'),
					'excerpt' 			=>	$this->input->post('excerpt'),
					'is_featured' 		=>	$this->input->post('is_featured'),
					'updated' 			=>	date('Y-m-d'),
					'created' 			=>	date('Y-m-d')
				);
			$neb_id = $this->subadmin_model->insert('neighbourhoods', $neighbour);
			
			if ($neb_id) 
			{
				$categories = $this->input->post('category');
				foreach ($categories as $cat) 
				{
					$arr = array(
								'category' 	=> 	$cat,
								'neighbour' => 	$neb_id,
								'updated' 	=>	date('Y-m-d'),
								'created' 	=>	date('Y-m-d')
							);
					$this->subadmin_model->insert('neighbour_cat',$arr);
				}
			}
			else
			{
				$this->session->set_flashdata('error_msg',"There is an error in adding information...!!!");
				redirect('subadmin/neighbourhoods');
			}

			$this->session->set_flashdata('success_msg',"Neighbourhood has been added successfully.");
			redirect('subadmin/neighbourhoods');
		}
		$data['cities'] = $this->subadmin_model->get_result('cities');
		$data['categories'] = $this->subadmin_model->get_result('neb_category');
        $data['template'] = 'subadmin/add_neighbour';
        $this->load->view('templates/subadmin_template', $data);		
	}

	public function edit_neighbour($nb_id ="")
	{		
		$this->check_login();
		$data['neighbour']   = $this->subadmin_model->get_row('neighbourhoods', array('id'=>$nb_id));
		$image = $data['neighbour']->banner;
		$homeImage = $data['neighbour']->homeImage;
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_error_delimiters('<span style="color:#D33F3E"><span>');
		if ($this->form_validation->run() == TRUE)
		{
			if ($_FILES['userfile']['name']!='') 
			{
				$image = $this->do_core_upload('userfile','./assets/uploads/neighbourhoods/');
				if (!$image) 
				{
					$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");
					redirect('subadmin/neighbourhoods');
				}
				else if(!empty($data['neighbour']->banner)) 
				{
					$path = './assets/uploads/neighbourhoods/';
					$delete_file = $data['neighbour']->banner;
					@unlink($path.$delete_file);
				}
			}

			if ($_FILES['homeImage']['name']!='') 
			{
				$homeImage = $this->do_core_upload('homeImage','./assets/uploads/neighbourhoods/');
				if (!$homeImage) 
				{
					$this->session->set_flashdata('error_msg',"There is an error in uploading your file.");
					redirect('subadmin/neighbourhoods');
				}
				else if(!empty($data['neighbour']->homeImage)) 
				{
					$path = './assets/uploads/neighbourhoods/';
					$delete_file = $data['neighbour']->homeImage;
					@unlink($path.$delete_file);
				}
			}
	
			$neighbour=array(
				'city'				=>	$this->input->post('city'),	
				'title'				=>	$this->input->post('title'),	
				'banner'    		=> 	$image, 
				'homeImage'    		=> 	$homeImage, 
				'description' 		=>	$this->input->post('description'),
				'excerpt' 			=>	$this->input->post('excerpt'),
				'is_featured' 		=>	$this->input->post('is_featured'),
				'updated' 			=>	date('Y-m-d')
			);
			$this->subadmin_model->update('neighbourhoods', $neighbour, array('id'=>$nb_id));
			$categories = $this->input->post('category');
			$this->subadmin_model->delete('neighbour_cat', array('neighbour'=>$nb_id));
			foreach ($categories as $cat) 
			{
				$arr = array(
							'category' 	=> 	$cat,
							'neighbour' => 	$nb_id,
							'updated' 	=>	date('Y-m-d')
						);
				$this->subadmin_model->insert('neighbour_cat', $arr);
			}

			$this->session->set_flashdata('success_msg',"Neighbourhood has been updated successfully.");
			redirect('subadmin/neighbourhoods');
		}
		$data['cities']      = $this->subadmin_model->get_result('cities');
		$data['categories']  = $this->subadmin_model->get_result('neb_category');
		$data['current_cat'] = $this->subadmin_model->get_result('neighbour_cat', array('neighbour'=>$nb_id));
      $data['template']    = 'subadmin/edit_neighbour';
        $this->load->view('templates/subadmin_template', $data);		
	}

	public function delete_neighbour($neb_id='')
	{
		$this->check_login();
		$neb_info = $this->subadmin_model->get_row('neighbourhoods',array('id'=>$neb_id));

		if(empty($neb_info))
			redirect('subadmin/neighbourhoods/');

		if (!empty($neb_info->banner)) 
		{
			$path = './assets/uploads/neighbourhoods/';
			$delete_file = $neb_info->banner;
			@unlink($path.$delete_file);
		}
		if (!empty($neb_info->homeImage)) 
		{
			$path = './assets/uploads/neighbourhoods/';
			$delete_file = $neb_info->homeImage;
			@unlink($path.$delete_file);
		}
		$this->subadmin_model->delete('neighbourhoods',array('id'=>$neb_id));
		$this->session->set_flashdata('success_msg',"neighbourhood has been deleted successfully.");
		redirect('subadmin/neighbourhoods');
	}









	public function neighbourhoods($offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->neighbourhood==2)
        {
             redirect('subadmin/dashboard');
        }

				$this->check_login();

				$limit=10;

				$data['neighbours']=$this->subadmin_model->get_pagination_result('neighbourhoods',$limit, $offset);

				$config= get_theme_pagination();	

				$config['base_url'] = base_url().'subadmin/neighbourhoods/';

				$config['total_rows'] = $this->subadmin_model->get_pagination_result('neighbourhoods',0,0);

				$config['per_page'] = $limit;

				$config['num_links'] =3;		

				$this->pagination->initialize($config); 		

				$data['pagination'] = $this->pagination->create_links();

				$data['total_row'] = $config['total_rows'];

				//$data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Groups' => '/subadmin/groups'));



				$data['template'] = 'subadmin/neighbourhoods';

		        $this->load->view('templates/subadmin_template', $data);	

			}



			public function check_neb_img($a){

		       if ($_FILES['userfile']['tmp_name'] == '') {

		           $this->form_validation->set_message('check_neb_img', 'Banner image is required.');

		            return FALSE;

		       }

		       else

		       		return TRUE;

		    }

	// Neighbourhood

		    // pay to user

		      public function pay_to_users($payments_id){
		    	$subadminid = $this->session->userdata('subadmin_info');
		    	$payment = $this->subadmin_model->get_row('payments', array('id'=>$payments_id));
		    	$totalamnt = $payment->amount-$payment->security_deposit;
		    	$currency  = $payment->booking_currency_type;
		    	
		    	$commision = get_commision_fee();

		    	$subadmin_amount = $commision->commision_fee*($totalamnt/100);
		    	$payable_amount = $totalamnt - $subadmin_amount;
		    	$userdetail = $this->subadmin_model->get_row('users', array('id'=>$payment->owner_id));

		    	$booking_info = $this->subadmin_model->get_row('bookings', array('id'=>$payment->booking_id));
		    	 
		    	if(empty($userdetail->paypal_email))
		    	{
		    		$this->session->set_flashdata('error_msg', "Host does not have paypal email  ");
		    		redirect('subadmin/payments');
		    	}
		    	
		    	if($booking_info->status==0)
		    	{ 
		    		$this->session->set_flashdata('error_msg', "Host has not accepted the booking request yet.");
		    		redirect('subadmin/payments');
		    	}

		    	if($booking_info->status==2)
		    	{ 
		    		$this->session->set_flashdata('error_msg', "This Booking Has been cancelled Now you can refund all the payment to guest.");
		    		redirect('subadmin/payments');
		    	}



				$receiverEmail = $userdetail->paypal_email;
			    $uniqueid = time(); //(Unique)
			    $userid = 1;

				$emailSubject =urlencode('example_email_subject');
				$receiverType = urlencode('EmailAddress');
				$currency = urlencode($currency);	// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

				// Add request-specific fields to the request string.

				$nvpStr="&EMAILSUBJECT=$emailSubject&RECEIVERTYPE=$receiverType&CURRENCYCODE=$currency";
				$receiversArray = array();
			        $j=0;
					$receiverData = array(	'receiverEmail' => $receiverEmail,
											'amount' => $payable_amount,
											'uniqueID' => $uniqueid,
											'note' => "Payment By Admin",
											'userid' => $userid
										);
					$receiversArray[$j] = $receiverData;

				foreach($receiversArray as $i => $receiverData) {
					$receiverEmail = urlencode($receiverData['receiverEmail']);
					$amount = urlencode($receiverData['amount']);
					$uniqueID = urlencode($receiverData['uniqueID']);
					$note = urlencode($receiverData['note']);
					$nvpStr .= "&L_EMAIL$i=$receiverEmail&L_Amt$i=$amount&L_UNIQUEID$i=$uniqueID&L_NOTE$i=$note";
				}

				$httpParsedResponseAr = $this->PPHttpPost('MassPay', $nvpStr);

				if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
					$updatedata = array(
							'paid_to_owner' => 1,
						); 

					$this->subadmin_model->update('payments', $updatedata, array('id'=>$payments_id));

					$this->subadmin_model->update('bookings', array('status'=>1), array('id'=>$booking_info->id));

					$payhistory = array(
						'paybyuser' => $subadminid['id'],
						'paywith' => 'paypal',
						'payment_type'=>1,
						'recievebyuser' => $payment->owner_id,
						'bookedbyuser' => $payment->user_id,
						'transaction_id' => $httpParsedResponseAr['CORRELATIONID'],
						'transaction_details' => json_encode($httpParsedResponseAr),
						'amount' => $payable_amount,
						'property_id' => $payment->property_id,
						'booking_id' => $payment->booking_id,					
						'payment_id' => $payments_id,						
						'amountearnedbysubadmin' => $subadmin_amount,
						'amountearnedbyuser' => $payable_amount,	
						'created' => date('Y-m-d h:i:s')			
						);

					$this->subadmin_model->insert('payment_history', $payhistory);					
					$this->session->set_flashdata('success_msg', "Payment Successfully tranferred to the host account.");
					redirect('subadmin/payments');					
				} else  {
					$this->session->set_flashdata('error_msg', "Payment failed");
					redirect('subadmin/payments');
				}

		}



		function PPHttpPost($methodName_, $nvpStr_) {
				$environment = 'sandbox';	// or 'beta-sandbox' or 'live'
				// global $environment;
				// Set up your API credentials, PayPal end point, and API version.
				$API_UserName = urlencode('admin_api1.vacalio.com');
				$API_Password = urlencode('1392796408');
				$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AvsUuepZFqP.6lxBgAaEMuv4v-Eu');
				$API_Endpoint = "https://api-3t.paypal.com/nvp";
				if("sandbox" === $environment || "beta-sandbox" === $environment) {
					$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
				}

				$version = urlencode('51.0');
				// Set the curl parameters.

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, $API_Endpoint);

				curl_setopt($ch, CURLOPT_VERBOSE, 1);



				// Turn off the server and peer verification (TrustManager Concept).

				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);



				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				curl_setopt($ch, CURLOPT_POST, 1);

				// Set the API operation, version, and API signature in the request.
				$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
				// Set the request as a POST FIELD for curl.
				curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
				// Get response from the server.
				$httpResponse = curl_exec($ch);

				if(!$httpResponse) {

					$this->session->set_flashdata('error_msg', "$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');

					redirect('subadmin/payments');

					// exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');

				}



				// Extract the response details.

				$httpResponseAr = explode("&", $httpResponse);
				$httpParsedResponseAr = array();
				foreach ($httpResponseAr as $i => $value) {
					$tmpAr = explode("=", $value);
					if(sizeof($tmpAr) > 1) {
						$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
					}
				}
				if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
					exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
				}
				return $httpParsedResponseAr;

		}

/* subadmin add/edit/delete event */

	public function validDateRange($start_date)

	{	

		$end_date = $this->input->post('eDate');

		$x = strtotime($start_date);

		$y = strtotime($end_date);

		if($x > $y)

		{

			$this->form_validation->set_message('validDateRange' , 'Start date must be small or equal to End date.');

			return false;

		}

		else

		{

			return true;

		}	

	}



	public function timeline()
	{
	     $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1));
        if($subadmin_restrictions->timeline==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$data['events']   = $this->subadmin_model->get_result('admin_events');

		$data['template'] = 'subadmin/timeline';

		$this->load->view('templates/subadmin_template', $data);

	} 





	public function events($offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->timeline==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$limit=10;

		$data['events'] = $this->subadmin_model->get_pagination_result('admin_events' ,$limit, $offset);

		$config= get_theme_pagination();

		$config['base_url'] = base_url().'subadmin/events/';

		$config['total_rows'] = $this->subadmin_model->get_pagination_result('admin_events', 0, 0);

		$config['per_page'] = $limit;

		$config['num_links'] = 5;

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();

		$data['template'] = 'subadmin/events';

        $this->load->view('templates/subadmin_template', $data);

	}





	public function addEvent()

	{

		$this->check_login();

		$this->form_validation->set_rules('title', 'Event title', 'required');

		$this->form_validation->set_rules('description', 'Event description', 'required');

		$this->form_validation->set_rules('location', 'Event location', 'required');

		$this->form_validation->set_rules('sDate', 'Start date', 'required|callback_validDateRange');

		$this->form_validation->set_rules('eDate', 'End date', 'required');

		$this->form_validation->set_rules('sTime', 'Start time', 'required');

		$this->form_validation->set_rules('eTime', 'End time', 'required');

		$this->form_validation->set_rules('isImportant', 'Is important', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	

		if($this->form_validation->run() == TRUE)

		{

			$array= array(

						  'title'       => $this->input->post('title'),

						  'description' => $this->input->post('description'),

						  'location' => $this->input->post('location'),

						  'startDate'   => date('Y-m-d', strtotime($this->input->post('sDate'))),

						  'endDate'     => date('Y-m-d', strtotime($this->input->post('eDate'))),

						  'startTime'   => $this->input->post('sTime'),

						  'endTime'     => $this->input->post('eTime'),

						  'isImportant' => $this->input->post('isImportant'),

						  'status'      => 0,

					  	  'created'     => date('Y-m-d')

					);

			$this->subadmin_model->insert('admin_events',$array);

			$this->session->set_flashdata('success_msg', 'Event has been added successfully.');

			redirect('subadmin/events');

		}

		$data['template'] = 'subadmin/addEvent';

        $this->load->view('templates/subadmin_template', $data);

	}



	public function editEvent($id='')

	{

		$this->check_login();

		if($id == '') redirect('subadmin/events');

		$data['event'] = $this->subadmin_model->get_row('admin_events', array('id' => $id));

		$this->form_validation->set_rules('title', 'Event title', 'required');

		$this->form_validation->set_rules('description', 'Event description', 'required');

		$this->form_validation->set_rules('location', 'Event location', 'required');

		$this->form_validation->set_rules('sDate', 'Start date', 'required|callback_validDateRange');

		$this->form_validation->set_rules('eDate', 'End date', 'required');

		$this->form_validation->set_rules('sTime', 'Start time', 'required');

		$this->form_validation->set_rules('eTime', 'End time', 'required');

		$this->form_validation->set_rules('isImportant', 'Is important', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">','</div>');	

		if($this->form_validation->run() == TRUE)

		{

			$array= array(

						  'title'       => $this->input->post('title'),

						  'description' => $this->input->post('description'),

						  'location' => $this->input->post('location'),

						  'startDate'   => date('Y-m-d', strtotime($this->input->post('sDate'))),

						  'endDate'     => date('Y-m-d', strtotime($this->input->post('eDate'))),

						  'startTime'   => $this->input->post('sTime'),

						  'endTime'     => $this->input->post('eTime'),

						  'isImportant' => $this->input->post('isImportant'),

						  'status'      => 0,

					  	  'updated'     => date('Y-m-d')

					);

			$this->subadmin_model->update('admin_events',$array ,array('id' => $id) );

			$this->session->set_flashdata('success_msg', 'Event has been updated successfully.');

			redirect('subadmin/events');

		}

		$data['template'] = 'subadmin/editEvent';

        $this->load->view('templates/subadmin_template', $data);

	}



	public function deleteEvent($id='')

	{	

		$this->check_login();

		if(empty($id)) redirect('subadmin/events');		

		$this->subadmin_model->delete('admin_events',array('id'=> $id));

		$this->session->set_flashdata('success_msg',"Event has been deleted successfully.");

		redirect('subadmin/events');

	}

/* subadmin add/edit/delete event */





	public function payment_detail($payment_id=''){

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/payments');		

		$data['payment'] = $this->subadmin_model->get_row('payments',array('id'=>$payment_id));

		

		$data['template'] = 'subadmin/payment_detail';

        $this->load->view('templates/subadmin_template', $data);

	}



	public function referral_payments($sort='newest', $offset=0){		

		$this->check_login();

		$name='';

		if($_POST){			

			$name  = $this->input->post('name');

		}

		

		$limit=10;

		$data['payments']=$this->subadmin_model->referral_payments($limit,$offset,$sort,$name);		

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/referral_payments/'.$sort;

		$config['total_rows'] = $this->subadmin_model->referral_payments(0,0,$sort,$name);

		$config['per_page'] = 10;

		$config['num_links'] =3;

		$config['uri_segment'] =4;



		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		$data['template'] = 'subadmin/referral_payments';

        $this->load->view('templates/subadmin_template', $data);

		

	}



	/*Referral Notes Work strats From here*/

	public function referral_notes($rf_id='')

	{

		$this->check_login();

		if(empty($rf_id)) redirect('subadmin/referral_payments'); 

		$data['notes']=$this->subadmin_model->get_result('referral_notes',array('referrel_id' =>$rf_id));

		$data['id'] = $rf_id;

		$data['template'] = 'subadmin/referral_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function add_referral_note($rf_id='')

	{

		$this->check_login(); 

		if(empty($rf_id)) redirect('subadmin/referral_payments');

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){			

			$data=array(

						'title'=>$this->input->post('title'),				

						'description'=>$this->input->post('description'),

						'referrel_id'=>$rf_id,

						'created' => date('Y-m-d H:i:s')		

				       );	

			$this->subadmin_model->insert('referral_notes',$data);		

			$this->session->set_flashdata('success_msg'," Successfully Referral Note Added.");

			redirect('subadmin/referral_notes/'.$rf_id);

		}

		// $data['breadcrumbs'] = create_breadcrumb(array('Dashboard' => '/subadmin/dashboard',  'Faqs' => '/subadmin/faqs', 'Add Faq' => '/subadmin/add_faq'));

		$data['template'] = 'subadmin/add_referral_note';

        $this->load->view('templates/subadmin_template', $data);		



	}



	public function view_referral_note($rf_id='')

	{

		$this->check_login();

		if(empty($rf_id)) redirect('subadmin/referral_payments'); 

		$data['notes']=$this->subadmin_model->get_row('referral_notes',array('id' =>$rf_id));

		if(empty($data['notes'])){

			redirect('subadmin/referral_payments');

		}

		$data['template'] = 'subadmin/view_referral_note';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_referral_note($id="",$rf_id="")

	{

		if($id=="")

		{

			redirect('subadmin/referral_payments');

		}

		$this->subadmin_model->delete('referral_notes',array('id'=>$id));

		$this->session->set_flashdata('success_msg'," Referral Note has been deleted successfully.");

		redirect('subadmin/referral_notes/'.$rf_id);

	}

     

     /*Referral work ends from here*/



	public function pay_referral_amount($invitation='',$amnt=''){

		$this->check_login();

		if($amnt == ""){

			redirect('subadmin/referral_payments');

		}



		if($invitation == ""){

			redirect('subadmin/referral_payments');

		}



		$invitedata = $this->subadmin_model->get_row('invitation', array('id'=>$invitation));		

		if($invitedata->property_credit_given == 1 && $invitedata->booking_credit_given == 1){

	        $paidon = 'both';

	      }else{

	        if($invitedata->property_credit_given == 1 && $invitedata->booking_credit_given != 1){

	          	$paidon = 'property_credit_given';

	        } 



	        if($invitedata->property_credit_given != 1 && $invitedata->booking_credit_given == 1){

	          	$paidon = 'booking_credit_given';

	        } 

	      }



	      // echo $paidon; die();



    	$subadminid = $this->session->userdata('subadmin_info');

    	$payable_amount = $amnt;

	    $userid = $invitedata->user_id;



	    $userdetail = $this->subadmin_model->get_row('users', array('id'=>$userid));

		    	// print_r(); die();

		    	if(empty($userdetail->paypal_email)){

		    		$this->session->set_flashdata('error_msg', "Payment failed");

		    		redirect('subadmin/payments');

		    	}



		$receiverEmail = $userdetail->paypal_email;				

    	// print_r($payable_amount); die();

		// $receiverEmail = 'customer@vacalio.com';

		// $payable_amount = 20.00;

	    $uniqueid = time(); //(Unique)

		

		$emailSubject =urlencode('example_email_subject');

		$receiverType = urlencode('EmailAddress');

		$currency = urlencode('USD');	// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')



		// Add request-specific fields to the request string.

		$nvpStr="&EMAILSUBJECT=$emailSubject&RECEIVERTYPE=$receiverType&CURRENCYCODE=$currency";



		$receiversArray = array();

		// foreach ($user_info as $user) {

			//Create Token

	        $j=0;

			$receiverData = array(	'receiverEmail' => $receiverEmail,

									'amount' => $payable_amount,

									'uniqueID' => $uniqueid,

									'note' => "Referral Payment By Admin",

									'userid' => $userid

								);

			$receiversArray[$j] = $receiverData;

			// $j++;

		// }

		foreach($receiversArray as $i => $receiverData) {

			$receiverEmail = urlencode($receiverData['receiverEmail']);

			$amount = urlencode($receiverData['amount']);

			$uniqueID = urlencode($receiverData['uniqueID']);

			$note = urlencode($receiverData['note']);

			$nvpStr .= "&L_EMAIL$i=$receiverEmail&L_Amt$i=$amount&L_UNIQUEID$i=$uniqueID&L_NOTE$i=$note";

		}

	

		// Execute the API operation; see the PPHttpPost function above.

		$httpParsedResponseAr = $this->PPHttpPost('MassPay', $nvpStr);



		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {

			 // echo 'Payment Completed Successfully: <br>';

			// print_r($httpParsedResponseAr); die();

			if($paidon == "both"){

				$updatedata = array(

					'property_credit_given' => 2,

					'booking_credit_given' => 2,

				); 	

			}else{

				$updatedata = array(

					$paidon => 2,					

				); 	

			}

			



			$this->subadmin_model->update('invitation', $updatedata, array('id'=>$invitation));



			$payhistory = array(

				'paybyuser' => $subadminid['id'],

				'recievebyuser' => $userid,

				'paywith' => 'paypal',

				'payment_type' => 2,

				'transaction_id' => $httpParsedResponseAr['CORRELATIONID'],

				'transaction_details' => json_encode($httpParsedResponseAr),

				'amount' => $payable_amount,

				'invitation_id' => $invitation,				

				'amountearnedbyuser' => $payable_amount,	

				'created' => date('Y-m-d h:i:s')			

				);



			$this->subadmin_model->insert('payment_history', $payhistory);					

			$this->session->set_flashdata('success_msg', "Payment Successfull");

			redirect('subadmin/referral_payments');					

		} else  {

			$this->session->set_flashdata('error_msg', "Payment failed");

			redirect('subadmin/referral_payments');

			// echo 'Payment failed: : <br>';

			// print_r($httpParsedResponseAr);

		}

		// die();

		//API Call End

	}





	public function transaction_history($sort='newest', $offset=0){		

		$this->check_login();

		$name='';

		if($_POST){			

			$name  = $this->input->post('name');

		}

		

		$limit=10;

		$data['payments']=$this->subadmin_model->transaction_history($limit,$offset,$sort,$name);		

		$config= get_theme_pagination();	

		$config['base_url'] = base_url().'subadmin/transaction_history/'.$sort;

		$config['total_rows'] = $this->subadmin_model->transaction_history(0,0,$sort,$name);

		$config['per_page'] = 10;

		$config['num_links'] =3;

		$config['uri_segment'] =4;



		$this->pagination->initialize($config); 		

		$data['pagination'] = $this->pagination->create_links();

		$data['total_row'] = $config['total_rows'];

		$data['template'] = 'subadmin/transaction_history';

        $this->load->view('templates/subadmin_template', $data);

		

	}





	public function transaction_details($payment_id=''){

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/transaction_history');		

		$data['payment'] = $this->subadmin_model->get_row('payment_history',array('id'=>$payment_id));

		

		$data['template'] = 'subadmin/transaction_detail';

        $this->load->view('templates/subadmin_template', $data);

	}





	public function referral_payment_detail($payment_id=''){

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/referral_payments');		

		$data['payment'] = $this->subadmin_model->get_row('invitation',array('id'=>$payment_id));

		

		$data['template'] = 'subadmin/ref_payment_detail';

        $this->load->view('templates/subadmin_template', $data);

	}

	public function refund_money($payment_id=''){
       if(!empty($payment_id))
       {
          $res = $this->subadmin_model->get_row('payments',array('id'=>$payment_id));
          if(empty($res))
          {
			$this->session->set_flashdata('error_msg', "Invalid Payment Id ");
            redirect('subadmin/payments');
          }
       }
       else
       {
			$this->session->set_flashdata('error_msg', "Invalid Payment Id ");
            redirect('subadmin/payments');
       }

		$this->config->load('paypal_cc_config');
		
		$api_username = 'admin_api1.vacalio.com';
		$api_password = '1392796408';
		$api_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AvsUuepZFqP.6lxBgAaEMuv4v-Eu';
		$api_version = $this->config->item('api_version');
		$api_endpoint = $this->config->item('api_endpoint');
		$api_environment = $this->config->item('api_environment');
		
		$this->load->library('paypal_refund'); // load paypal_refund library
		
		$this->paypal_refund->paypal_cofig($api_username, $api_password, $api_signature, $api_environment, $api_endpoint, $api_version);
		
		$payment_details = $this->subadmin_model->get_row('payments',array('id'=>$payment_id));
		$transaction_id = $payment_details->transaction_id;
		$amount = $payment_details->amount;
		$currency = $payment_details->booking_currency_type;


		
		$data_arr=$this->paypal_refund->refund_paypal_amount($transaction_id, $refundType='Full',$amount="",$memo="",$currency);


		if("SUCCESS" == strtoupper($data_arr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($data_arr["ACK"])) {

          /*Booking Cancelled Entry In table Starts*/
  			    $this->subadmin_model->update('bookings',array('status'=>2),array('id'=>$payment_details->booking_id));
			    $booking_info = $this->subadmin_model->get_row('bookings',array('id'=>$payment_details->booking_id));
			    $check_in = $booking_info->check_in;
			    $check_out = $booking_info->check_out;
			    $pr_id = $booking_info->pr_id;
			    $this->subadmin_model->make_event_available($check_in, $check_out,$pr_id);
          /*Booking Cancelled Entry In table Ends*/

			$updatedata = array('is_refund'=>1,'is_deposit_refund'=>1);
			$this->subadmin_model->update('payments', $updatedata,array('id'=>$payment_id));
			
			$refund_res = array(
								'payment_id'=>$payment_id,
								'transaction_id'=>$data_arr['REFUNDTRANSACTIONID'],
								'transaction_details'=>json_encode($data_arr),
								'created'=>date('Y-m-d h:i:s'),
				              );

			/*Email after cancelled booking to both guest and host starts*/

				$data['booking_info']  = $this->subadmin_model->get_row('bookings',array('id'=>$payment_details->booking_id));
				$data['host_info']     = get_user_info($data['booking_info']->owner_id);
				$data['guest_info']    = get_user_info($data['booking_info']->customer_id);
			    $data['property_info'] = get_property_detail($data['booking_info']->pr_id);
		        

		/* Email To host Satrts*/
				$this->load->library('smtp_lib/smtp_email');

		        $email_template = get_manage_email_info('after_booking_hasbeen_cancelled_send_to_host');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
		        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

		        $subject = $email_template->subject;

		        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		               
		        
		        $to = array($data['host_info']->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				$this->smtp_email->sendEmail($from, $to, $subject, $html);
		       
		        $insert_data = array(
						        	  'name'=>$data['host_info']->first_name,
						        	  'email'=>$data['host_info']->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->subadmin_model->insert('notifications',$insert_data);

		/*Email To host Ends*/
				      
		/* Email To Guest For Refund Satrts*/

		        $email_template = get_manage_email_info('after_we_send_a_refund');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
		        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

		        $subject = $email_template->subject;

		        $arr_old = array('&lt;%amount%&gt;','&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($amount.' '.$currency,$data['host_info']->first_name,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		               
		        $data['guest_info']->user_email;
		        $to = array($data['guest_info']->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				 $this->smtp_email->sendEmail($from, $to, $subject, $html);





		       
		        $insert_data = array(
						        	  'name'=>$data['guest_info']->first_name,
						        	  'email'=>$data['guest_info']->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );
		        $this->subadmin_model->insert('notifications',$insert_data);

		/*Email To Guest For Refund Ends*/


				// $html_for_host = $this->load->view('user/email_template/booking_cancel_email_to_host',$data,TRUE);
				// $subject_for_host = 'This is the Notifications for you from Bnbclone on Cancelled booking';
				// $this->send_cancel_booking_email($data['host_info']->user_email,$subject_for_host,$html_for_host);
		  //       $insert_data = array(
				// 		        	  'name'=>$data['host_info']->first_name,
				// 		        	  'email'=>$data['host_info']->user_email,
				// 		        	  'subject'=>$subject_for_host,
				// 		        	  'message'=>$html_for_host,
				// 		        	  'created'=>date('Y-m-d h:i:s')
				// 		        	  );

		  //       $this->subadmin_model->insert('notifications',$insert_data);

				// $html_for_guest = $this->load->view('user/email_template/booking_cancel_email_to_guest',$data,TRUE);
				// $subject_for_guest = 'This is the Notifications for you from Bnbclone on Cancelled Booking';
				// $this->send_cancel_booking_email($data['guest_info']->user_email,$subject_for_guest,$html_for_guest);
		  //       $insert_data = array(
				// 		        	  'name'=>$data['guest_info']->first_name,
				// 		        	  'email'=>$data['guest_info']->user_email,
				// 		        	  'subject'=>$subject_for_guest,
				// 		        	  'message'=>$html_for_guest,
				// 		        	  'created'=>date('Y-m-d h:i:s')
				// 		        	  );
		      
		  //       $this->subadmin_model->insert('notifications',$insert_data);
				/*Email after cancelled booking to both guest and host ends*/

			$this->subadmin_model->insert('refund_payment', $refund_res);
			$this->session->set_flashdata('success_msg', 'Payment Refunded successfully.');
			redirect('subadmin/payments');
		}
		else
		{	
			$this->session->set_flashdata('error_msg', urldecode($data_arr['L_LONGMESSAGE0']));
			redirect('subadmin/payments');				
		}

	}

	/*end refund payment*/ 


	

   /* Send email to both guest and host for cancel Booking starts*/
	public function send_cancel_booking_email($email="",$subject="",$html="")
	{
		$this->check_login();
		$this->load->library('smtp_lib/smtp_email');
		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form
        $subject = $subject;
		$to = array($email);
		$is_fail = $this->smtp_email->sendEmail($from,$to,$subject,$html);
		if($is_fail){
		return FALSE;
		}
		else{
			return TRUE;
		}
	}

   /* Send email to both guest and host for cancel Booking starts*/

	/*Change logo Starts*/

	public function change_logo()

	{	

		$this->check_login();

		$data['logo'] = $this->subadmin_model->get_row('site_details',array('id'=>1));

		$this->form_validation->set_rules('logo_name','Logo Name','trim|required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	

		if($this->form_validation->run() == TRUE){

		    if($_FILES['logo']['name']!=''){

				$config['upload_path'] = './assets/front_end_theme/img/logo/';

				$config['allowed_types'] = 'png';				

				$config['max_size']	= '100';

				$config['max_width']  = '250';

				$config['max_height']  = '100';

				$this->load->library('upload', $config);				

				if(! $this->upload->do_upload('logo'))					

				{

					$this->session->set_flashdata('image_error', $this->upload->display_errors());

					redirect('subadmin/change_logo/');

				}

				else

				{

					if($data['logo']->logo !=''){

					$path = './assets/front_end_theme/img/logo/';

					$file = $data['logo']->logo;

						if(!empty($file)){

							@unlink($path.$file);

						}

					}

					$data = $this->upload->data();

					$array['logo']=$data['file_name'];

				}

			}



			$this->subadmin_model->update('site_details',$array,array('id'=>1));

			$this->session->set_flashdata('success_msg',"logo has been updated successfully.");

			redirect('subadmin/settings');

		}



		$data['template'] = 'subadmin/change_logo';

        $this->load->view('templates/subadmin_template', $data);

	}





	/*Change logo Ends*/





    /* change Buttons color Starts*/



	public function change_buttons_colors()

	{

		$this->check_login();

		$data['buttons']=$this->subadmin_model->get_row('site_details');



		$this->form_validation->set_rules('delete', 'Delete', 'required');

		$this->form_validation->set_rules('edit', 'Edit', 'required');

		$this->form_validation->set_rules('notes', 'Notes', 'required');

		$this->form_validation->set_rules('email', 'Email', 'required');

		$this->form_validation->set_rules('change_password', 'Change Password', 'required');

		$this->form_validation->set_rules('pay_to_host', 'Pay To Host', 'required');

		$this->form_validation->set_rules('refund', 'Refund', 'required');

		$this->form_validation->set_rules('details', 'Details', 'required');

		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');					

		

		if ($this->form_validation->run() == TRUE){

			$arr = array(

				          'delete_btn'=>$_POST['delete'],

				          'edit_btn'=>$_POST['edit'],

				          'notes_btn'=>$_POST['notes'],

				          'pay_to_host_btn'=>$_POST['pay_to_host'],

				          'details_btn'=>$_POST['details'],

				          'change_password_btn'=>$_POST['change_password'],

				          'refund_btn'=>$_POST['refund'],

				          'email_btn'=>$_POST['email'],

				          );

			$this->subadmin_model->update('site_details',$arr,array('id'=>$data['buttons']->id));

            $this->session->set_flashdata('success_msg','Button Colors has been updated successfully');

            redirect('subadmin/settings');

 		}

        

		$data['template'] = 'subadmin/show_all_buttons';

        $this->load->view('templates/subadmin_template', $data);

	}





    /* change Buttons color Ends*/



	/*Change Site currency Starts*/



	public function change_site_currency()

	{

		$this->check_login();

		$data['buttons']=$this->subadmin_model->get_row('site_details');



		$this->form_validation->set_rules('currency', 'Currency', 'required');

		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');					

		

		if ($this->form_validation->run() == TRUE){

			$arr = array(

				          'currency'=>$_POST['currency'],

				          'currency_status' =>1,

				          );

			$this->subadmin_model->update('site_details',$arr,array('id'=>$data['buttons']->id));

            $this->session->set_flashdata('success_msg','Currency has been set successfully');

            redirect('subadmin/settings');

 		}

        

		$data['template'] = 'subadmin/change_site_currency';

        $this->load->view('templates/subadmin_template', $data);

	}



	/*change_media_buttons Starts*/

	public function change_media_buttons($social_name='')
	{
		$this->check_login();
		$data['change_media_buttons']=$this->subadmin_model->get_row('social_media_keys',array('name'=>$social_name));
		$this->form_validation->set_rules('author','Author','required');
      
        if(empty($_FILES['button_images']['name']))
        {
        	if(empty($data['change_media_buttons']->button_images))
        	{
        		$this->form_validation->set_rules('button_images','Image','required');
        	}
        }

	    $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');					

        if($this->form_validation->run()==TRUE)
        {
            if(isset($_FILES['button_images']['name']) && !empty($_FILES['button_images']['name']))
            {
				$image_name = $this->do_core_upload('button_images','./assets/img/third_party/');
            
                if(!empty($image_name))
                {
                	$path = "./assets/img/third_party/";
                    @unlink($path.$data['change_media_buttons']->button_images);
                }

				 $arr = array(
	                          'button_images'=>$image_name,
					          );

				$this->subadmin_model->update('social_media_keys',$arr,array('name'=>$social_name));


            }
	            $this->session->set_flashdata('success_msg','Social Media Button Image has been Updated successfully');
	           
	            redirect('subadmin/settings');
        }

		$data['template'] = 'subadmin/change_media_buttons';

        $this->load->view('templates/subadmin_template', $data);
    }

	/*change_media_buttons Ends*/

	/*Change APi keys Starts*/
	public function change_secret_keys($social_name='')

	{

		$this->check_login();

		$data['social_media_keys']=$this->subadmin_model->get_row('social_media_keys',array('name'=>$social_name));

		$this->form_validation->set_rules('app_id', 'App Id', 'required');

		$this->form_validation->set_rules('app_secret_key', 'App secret key', 'required');
       
        if($social_name=="twitter")
        {
			$this->form_validation->set_rules('twitter_access_token', 'Access Token', 'required');
			$this->form_validation->set_rules('twitter_access_token_secret', 'Access Token Secret', 'required');
			$this->form_validation->set_rules('page_id_or_user_name', 'Twitter User name', 'required');
	    }

        if($social_name=="facebook")
        {
			$this->form_validation->set_rules('page_id_or_user_name', 'Facebook page ID', 'required');
	    }
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');					

		

		if ($this->form_validation->run() == TRUE){

			$arr = array(

				          'app_id'=>$_POST['app_id'],

				          'app_secret_key'=>$_POST['app_secret_key'],

				          );

			if(isset($_POST['page_id_or_user_name']))
			{
              $arr['page_id_or_user_name'] = $_POST['page_id_or_user_name'];
			}

			if(isset($_POST['twitter_access_token']) && isset($_POST['twitter_access_token_secret']))
			{
              $arr['twitter_access_token'] = $_POST['twitter_access_token'];
              $arr['twitter_access_token_secret'] = $_POST['twitter_access_token_secret'];
			}

			$this->subadmin_model->update('social_media_keys',$arr,array('name'=>$social_name));

            $this->session->set_flashdata('success_msg','Secret keys has been updated successfully');

            redirect('subadmin/settings');

 		}

        

		$data['template'] = 'subadmin/change_secret_keys';

        $this->load->view('templates/subadmin_template', $data);

	}




	/*Change APi keys Ends*/



	public function reciept($id=""){

		if($id=="")

			redirect('subadmin/bookings');



		$data['booking'] = $this->subadmin_model->get_row('bookings', array('id'=>$id));

		$data['template'] = 'subadmin/reciept';

        $this->load->view('templates/subadmin_template', $data);	

	}





	public function booking_detail_email($book_id='',$flag="")

	{

		$this->check_login();

		$row['booking'] = $this->subadmin_model->get_row('bookings',array('id'=>$book_id));

		if (empty($row)) {

			$this->session->set_flashdata('error_msg' , "Invalid booking id");

			redirect('subadmin/bookings');

		}

        $host = get_user_info($row['booking']->owner_id);

        $guest = get_user_info($row['booking']->customer_id);

        if($flag=="main_page")

        {

        $message = $this->load->view('export/export_booking_detail',$row,TRUE);

        }

        elseif($flag=='receipt_page')

        {

        $message = $this->load->view('export/export_booking_receipt',$row,TRUE);

        }

			$status_1 = $this->send_booking_detail_email_message($guest,$message,"");

			$status_2 = $this->send_booking_detail_email_message("",$message,$host);

			if($status_2 && $status_1){

				$this->session->set_flashdata('success_msg',"Email  has been sent successfully.");

				redirect('subadmin/bookings');

			}		

		$data['template'] = 'subadmin/booking_email';

        $this->load->view('templates/subadmin_template',$data);	

	}



	public function send_booking_detail_email_message($guest="",$message="",$host="")

	{

		$this->check_login();

		$this->load->library('smtp_lib/smtp_email');

		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form

        $subject = "This is Notifications for you from the Bnbclone on your Booking Itinerary";

        if(!empty($guest))

        {

			$to = array($guest->user_email);

			$html = '';

			$html .= '<html>

						<body>

						  <h3>Dear '.$guest->first_name.'</h3>';

        }

        if(!empty($host))

        {

			$to = array($host->user_email);

			$html = '';

			$html .= '<html>

						<body>

						  <h3>Dear '.$host->first_name.'</h3>';

        }

		$html .= '<p>This is Notifications for you from the Bnbclone on your Booking Itinerary </p>';

		$html .= '<strong>Booking</strong>';

		$html .=	'<p>';

		$html .=  $message;

		$html .='</p><br/>';					

		$html .=	'Thank you</body></html>';

	

		$is_fail = $this->smtp_email->sendEmail($from,$to,$subject,$html);

		if($is_fail){

		return FALSE;

		

		}

		else{

			return TRUE;

		}

	}


	public function get_due_amount($property_id =""){		
		if($property_id == "")
			return FALSE;
		$property = $this->subadmin_model->get_row('properties', array('id'=>$property_id));
		$chkindate = $this->input->post('checkin');
		$chkoutdate = $this->input->post('checkout');		

		  $startTimeStamp = strtotime($chkindate);
          $endTimeStamp = strtotime($chkoutdate);
          $timeDiff = abs($endTimeStamp - $startTimeStamp);
          $numberDays = $timeDiff/86400;  // 86400 seconds in one day
          // and you might want to convert to integer
          $numberDays = intval($numberDays);

          $amt = 0;
		if($property->room_allotment == 4){			
			$amt = $property->application_fee*$numberDays;			
		}elseif($property->room_allotment == 5){			
			$amt = ($property->application_fee/7)*$numberDays;
		}elseif($property->room_allotment == 6){			
			$amt = ($property->application_fee/30)*$numberDays;
		}
		echo $amt;


	}


/*Secority Deposit work Starts from here*/

	public function security_deposit($sort='newest', $offset=0){		
		$this->check_login();
		$name='';
		$post="";
		$limit=10;
		$data['security_deposit']=$this->subadmin_model->security_deposit($limit,$offset,$sort,'is_deposit_refund !=');		
		$config= get_theme_pagination();	
		$config['base_url'] = base_url().'subadmin/security_deposit/'.$sort;
		$config['total_rows'] = $this->subadmin_model->security_deposit(0,0,$sort,'is_deposit_refund !=');
		$config['per_page'] = 10;
		$config['num_links'] =3;
		$config['uri_segment'] =4;
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();
		$data['total_row'] = $config['total_rows'];
		$data['template'] = 'subadmin/security_deposit';
        $this->load->view('templates/subadmin_template', $data);
	}



     /*Secority Deposit work ends from here*/


     /*Secority Deposit History work Starts from here*/

	public function security_deposit_history($sort='newest', $offset=0){		
		$this->check_login();
		$name='';
		$post="";
		$limit=10;
		$data['security_deposit_history']=$this->subadmin_model->security_deposit($limit,$offset,$sort,'is_deposit_refund');		
		$config= get_theme_pagination();	
		$config['base_url'] = base_url().'subadmin/security_deposit_history/'.$sort;
		$config['total_rows'] = $this->subadmin_model->security_deposit(0,0,$sort,'is_deposit_refund');
		$config['per_page'] = 10;
		$config['num_links'] =3;
		$config['uri_segment'] =4;
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();
		$data['total_row'] = $config['total_rows'];
		$data['template'] = 'subadmin/security_deposit_history';
        $this->load->view('templates/subadmin_template', $data);
	}

/*Notes Work strats From here*/

	public function security_deposit_notes($payment_id='')

	{

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/payments'); 

		$data['notes']=$this->subadmin_model->get_result('security_deposit_notes',array('payment_id' =>$payment_id));

		$data['payment_id'] = $payment_id;

		$data['template'] = 'subadmin/security_deposit_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function add_security_deposit_note($payment_id='')

	{

		$this->check_login(); 

		if(empty($payment_id)) redirect('subadmin/payments');

		$this->form_validation->set_rules('title', 'Title', 'required');			

		$this->form_validation->set_rules('description', 'Description', 'required');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			

		if ($this->form_validation->run() == TRUE){			

			$data=array(

						'title'=>$this->input->post('title'),				

						'description'=>$this->input->post('description'),

						'payment_id'=>$payment_id,

						'created' => date('Y-m-d H:i:s')		

				       );	

			$this->subadmin_model->insert('security_deposit_notes',$data);		

			$this->session->set_flashdata('success_msg'," Successfully payment Note Added.");

			redirect('subadmin/security_deposit_notes/'.$payment_id);

		}


		$data['template'] = 'subadmin/add_security_deposit_note';

        $this->load->view('templates/subadmin_template', $data);		
	}



	public function view_security_deposit_notes($payment_id='')

	{

		$this->check_login();

		if(empty($payment_id)) redirect('subadmin/security_deposit'); 

		$data['notes']=$this->subadmin_model->get_row('security_deposit_notes',array('id' =>$payment_id));

		if(empty($data['notes'])){

			redirect('subadmin/security_deposit');

		}

		$data['template'] = 'subadmin/view_security_deposit_notes';

        $this->load->view('templates/subadmin_template', $data);		

	}



	public function delete_security_deposit_notes($id="",$payment_id="")

	{

		if($id=="")

		{

			redirect('subadmin/security_deposit');

		}

		$this->subadmin_model->delete('security_deposit_notes',array('id'=>$id));

		$this->session->set_flashdata('success_msg'," Security Note has been deleted successfully.");

		redirect('subadmin/security_deposit_notes/'.$payment_id);

	}


     /*Refunds  Security Deposit starts from here*/

	public function refund_security_deposit($payment_id=''){
       if(!empty($payment_id))
       {
          $res = $this->subadmin_model->get_row('payments',array('id'=>$payment_id));
          if(empty($res))
          {
			$this->session->set_flashdata('error_msg', "Invalid Payment Id ");
            redirect('subadmin/security_deposit');
          }
       }
       else
       {
			$this->session->set_flashdata('error_msg', "Invalid Payment Id ");
            redirect('subadmin/security_deposit');
       }

		$this->config->load('paypal_cc_config');
		
		$api_username = 'admin_api1.vacalio.com';
		$api_password = '1392796408';
		$api_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31AvsUuepZFqP.6lxBgAaEMuv4v-Eu';
		$api_version = $this->config->item('api_version');
		$api_endpoint = $this->config->item('api_endpoint');
		$api_environment = $this->config->item('api_environment');
		
		$this->load->library('paypal_refund'); // load paypal_refund library
		
		$this->paypal_refund->paypal_cofig($api_username, $api_password, $api_signature, $api_environment, $api_endpoint, $api_version);
		
		$payment_details = $this->subadmin_model->get_row('payments',array('id'=>$payment_id));
		$transaction_id = $payment_details->transaction_id;
		$amount = $payment_details->security_deposit;
		$currency = $payment_details->booking_currency_type;

		$data_arr=$this->paypal_refund->refund_paypal_amount($transaction_id, $refundType='Partial',$amount,$memo="",$currency);
	
		if("SUCCESS" == strtoupper($data_arr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($data_arr["ACK"])) {


				$data['booking_info']  = $this->subadmin_model->get_row('bookings',array('id'=>$payment_details->booking_id));
				$data['host_info']     = get_user_info($data['booking_info']->owner_id);
				$data['guest_info']    = get_user_info($data['booking_info']->customer_id);
			    $data['property_info'] = get_property_detail($data['booking_info']->pr_id);
		        

				$this->load->library('smtp_lib/smtp_email');


		/* Email To Guest For Refund Security Deposit  Satrts*/
				      
		        $email_template = get_manage_email_info('after_we_send_security_deposit');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
		        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

		        $subject = $email_template->subject;

		        $arr_old = array('&lt;%amount%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($amount.' '.$currency,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		               
		        $data['guest_info']->user_email;
		        $to = array($data['guest_info']->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				$this->smtp_email->sendEmail($from, $to, $subject, $html);





		       
		        $insert_data = array(
						        	  'name'=>$data['guest_info']->first_name,
						        	  'email'=>$data['guest_info']->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );
		        $this->subadmin_model->insert('notifications',$insert_data);

		/*Email To Guest For Refund Security Deposit  Ends*/



			$updatedata = array('is_deposit_refund'=>1);
			$this->subadmin_model->update('payments', $updatedata,array('id'=>$payment_id));
			
			$refund_res = array(
				'payment_id'=>$payment_id,
				'transaction_id'=>$data_arr['REFUNDTRANSACTIONID'],
				'transaction_details'=>json_encode($data_arr),
				'created'=>date('Y-m-d h:i:s'),
				);
			$this->subadmin_model->insert('refund_payment', $refund_res);
			$this->session->set_flashdata('success_msg', 'Payment Refunded successfully.');
			redirect('subadmin/security_deposit');
		}
		else
		{	
			$this->session->set_flashdata('error_msg', urldecode($data_arr['L_LONGMESSAGE0']));
			redirect('subadmin/security_deposit');				
		}

	}


     /*Refunds  Security Deposit Ends from here*/

     
     

     /*Payments work ends from here*/

     /* Featured Images works Starts */

	public function featured_images($offset=0)
	{

	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->featured_images==2)
        {
             redirect('subadmin/dashboard');
        }

		$this->check_login();

		$post="";
		if(isset($_POST) && !empty($_POST))
		{
			$post = $_POST;
		}
		$limit = 10;
		$data['featured_images']=$this->subadmin_model->get_featured_images_result('pr_gallery',$limit,$offset,$post);
		$config = get_theme_pagination();
		$config['base_url'] = base_url().'subadmin/featured_images/';
		$config['total_rows']=$this->subadmin_model->get_featured_images_result('pr_gallery',0,0,$post);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['offset'] = $offset;
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'subadmin/featured_images';
		$this->load->view('templates/subadmin_template',$data);	 		
	}

	public function accept_featured_image_request($id="")
 	{
      if(empty($id))
      {
      	redirect('subadmin/featured_images');
      }

      /*Send Email To Feratured Image User  */
      $pr_gallery_row = $this->subadmin_model->get_row('pr_gallery',array('id'=>$id));
      $property_id = $pr_gallery_row->pr_id;
      $pr_info = $this->subadmin_model->get_row('properties',array('id'=>$property_id));
      $user_id = $pr_info->user_id;
      $user_info = $this->subadmin_model->get_row('users',array('id'=>$user_id));

      /*Email For Featured Image Starts*/

				$this->load->library('smtp_lib/smtp_email');

		        $email_template = get_manage_email_info('accept_featured_image_request');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;


		        $subject = $email_template->subject;

                $image_picture = '<img style="width:100px;height:100px;border:1px solid #101010;border-radius:3px;box-shadow:1px 1px 5px" src='.base_url().'assets/uploads/property/'.$pr_gallery_row->image_file.'>';		        
               
                $image_link = '<a href='.base_url().'assets/uploads/property/'.$pr_gallery_row->image_file.'>Click Here to see featured Image</a>';

		        $arr_old = array('&lt;%username%&gt;','&lt;%propertyimage%&gt;','&lt;%imagelink%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($user_info->first_name,$image_picture,$image_link,$pr_info->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		        $to = array($user_info->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 
 
				$this->smtp_email->sendEmail($from, $to, $subject, $html);
		       
		        $insert_data = array(
						        	  'name'=>$user_info->first_name,
						        	  'email'=>$user_info->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->subadmin_model->insert('notifications',$insert_data);


      /*Email For Featured Images Ends*/


      $this->subadmin_model->update('pr_gallery',array('featured_request'=>2),array('id'=>$id));
      $this->session->set_flashdata('success_msg', 'You have successfully accepted the Request');
	  redirect('subadmin/featured_images');
  	}

 	public function reject_featured_image_request($id="")
 	{
      if(empty($id))
      {
      	redirect('subadmin/featured_images');
      }

      /*Send Email To Feratured Image User  */
      $pr_gallery_row = $this->subadmin_model->get_row('pr_gallery',array('id'=>$id));
      $property_id = $pr_gallery_row->pr_id;
      $pr_info = $this->subadmin_model->get_row('properties',array('id'=>$property_id));
      $user_id = $pr_info->user_id;
      $user_info = $this->subadmin_model->get_row('users',array('id'=>$user_id));
      $user_email = $user_info->user_email;


      /*Email For Featured Image Starts*/

				$this->load->library('smtp_lib/smtp_email');

		        $email_template = get_manage_email_info('reject_featured_image_request');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;


		        $subject = $email_template->subject;

                $image_picture = '<img style="width:100px;height:100px;border:1px solid #101010;border-radius:3px;box-shadow:1px 1px 5px" src='.base_url().'assets/uploads/property/'.$pr_gallery_row->image_file.'>';		        
               
                $image_link = '<a href='.base_url().'assets/uploads/property/'.$pr_gallery_row->image_file.'>Click Here to see featured Image</a>';

		        $arr_old = array('&lt;%username%&gt;','&lt;%propertyimage%&gt;','&lt;%imagelink%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($user_info->first_name,$image_picture,$image_link,$pr_info->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);
		               
		        $to = array($user_info->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 
 
				$this->smtp_email->sendEmail($from, $to, $subject, $html);
		       
		        $insert_data = array(
						        	  'name'=>$user_info->first_name,
						        	  'email'=>$user_info->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->subadmin_model->insert('notifications',$insert_data);


      /*Email For Featured Images Ends*/



      $this->subadmin_model->update('pr_gallery',array('featured_request'=>0),array('id'=>$id));
      $this->session->set_flashdata('success_msg', 'You have successfully Reject the Request');
	  redirect('subadmin/featured_images');
  	}
 	

  	public function publish_featured_image()
  	{
     $update_data = array(
     	                  'is_featured_image'=>1,
                          'subscription_date'=>date('d F Y'),
                          'subscription_type'=>$_POST['subscriptions_type'],
     	                  'featured_request' =>2,
     	                  );
		$this->subadmin_model->update('pr_gallery',$update_data,array('id'=>$_POST['image_id']));
		$this->session->set_flashdata('success_msg', 'This Image has been Succesfullly set to Featured Image.');
		redirect('subadmin/featured_images');
  	}

  	public function unpublish_featured_image($id="")
  	{
     $update_data = array(
     	                  'is_featured_image'=>0,
                          'subscription_date'=>"",
                          'subscription_type'=>"",
                          'featured_request'=>0,
     	                  );
		$this->subadmin_model->update('pr_gallery',$update_data,array('id'=>$id));
		$this->session->set_flashdata('success_msg', 'Featured Image has been unpublished Succesfullly.');
		redirect('subadmin/featured_images');
  	}


     /* Featured Images works Ends */


  public function property_images($pr_id="")
  {

	$this->check_login();
      if(empty($pr_id))
      {
      	redirect('subadmin/properties');
      }
	$data['property_id'] = $pr_id;
	$data['template'] = 'subadmin/property_images';
	$this->load->view('templates/subadmin_template',$data);	 		
  }

   public function ajax_property_drag_drop()
   {
	$this->check_login();
   	$pr_id = $_POST['property_id'];
   	$file_name = $_FILES['pic']['name'];
		if(@$file_name !="")
		{
		   $image = $this->do_core_upload('pic','./assets/uploads/property/');
		   $info_data = array(
		   	                  'pr_id'=>$pr_id,
		   	                  'created'=>date('d F Y'),
		   	                  'image_file'=>$image,
		                      ); 
		   $this->subadmin_model->insert('pr_gallery',$info_data);
		   $str = "File Uploaded Succesfullly";
		   echo json_encode(array('status'=>$str));
		   exit;
		}
		else
		{
		   $str = "Something went Wrong ";
		   echo json_encode(array('status'=>$str));
		   exit;
		}    

   	
   } 
	
   /* Ajax upload Drag and drop images Endss*/	

   /*See All Images Starts*/
    public function see_all_property_images($pr_id="")
    {
 		$this->check_login();
	      if(empty($pr_id))
	      {
	      	redirect('subadmin/properties');
	      }

    	$data['property_id'] = $pr_id;
		$data['gallery'] = $this->subadmin_model->get_result('pr_gallery',array('pr_id'=>$pr_id));
		$data['get_first_row'] = $this->subadmin_model->get_row('pr_gallery',array('pr_id'=>$pr_id));
	    $data['template'] = 'subadmin/see_all_property_images';
		$this->load->view('templates/subadmin_template', $data);		
    }
 	/*See All Images Ends*/

 	/*ajax Change profile images starts*/

    public function ajax_change_property_profile_image()
 	{
 		$this->check_login();
 		$image_new =  $this->input->post('image');
 		$property_id =  $this->input->post('property_id');
			if($image_new!="" && $property_id!="")
			{
			   $this->subadmin_model->update('properties',array('featured_image' => $image_new),array('id' => $property_id));
			   $this->subadmin_model->update('pr_gallery',array('status'=>0),array('pr_id'=>$property_id));
			   $this->subadmin_model->update('pr_gallery',array('status'=>1),array('image_file'=>$image_new,'pr_id'=>$property_id));
			   echo "valid";
			}    
 	}   
 	/*ajax Change profile images Ends*/

 	/*ajax delete image  starts*/
 	public function ajax_delete_property_image(){
 		$this->check_login();
	    $unique_id = $_POST['unique_id'];
	    $property_id = $_POST['property_id'];
	    $row = $this->subadmin_model->get_row('pr_gallery',array('id'=>$unique_id));
	    $old_image = $row->image_file;
		$this->subadmin_model->delete('pr_gallery',array('id'=>$unique_id));
	    if($row->status==1){
		$this->subadmin_model->update('properties',array('featured_image'=>'','publish_permission'=>0,'status'=>0),array('id'=>$property_id));
	    }
		if(!empty($old_image))
		{
		$path = './assets/uploads/property/';
		@unlink($path.$old_image);
		echo "deleted" ;
		}
    }

  /*Multiple images uploads Work Ends*/


   // this function using by booking system

   public function get_effective_booking_rate()
   {
        $currency = $this->session->userdata('currency');
        $pr_id = $_POST['pr_id'];
        $pr_info = get_property_detail($pr_id);
        $c_in = $_POST['check_in'];
        $c_out = $_POST['check_out'];

       $check_in_time =  strtotime($c_in);
       $check_out_time = strtotime($c_out);
       $differrence =  $check_out_time-$check_in_time;
       $number_of_days = ($differrence/86400)+1;
      
          
       if(get_subadmin_id()==false)
       {
         echo "Please Login First For Booking !";
         exit;
       }


       
       if($check_out_time < $check_in_time)
       {
        echo "Invalid date range";
         exit;
       }

       if($pr_info->min_stay!="" && $number_of_days < $pr_info->min_stay)
       {
          echo "This duration is Too short for booking minimum ".$pr_info->min_stay." Days is Required";
          exit;
       }


       if($pr_info->max_stay!="" && $number_of_days > $pr_info->max_stay)
       {
          echo "This duration is Too Long for booking maximum ".$pr_info->max_stay." Days is allowed";
          exit;
       }


       if(!check_booking_date_availability($pr_id,$check_in_time,$check_out_time))
       {
          echo "Property is not available or already booked for this date range. Please Choose different date range";
          exit;
       }

       $security_deposit=0;
       if($pr_info->security_deposit!=0)
       {
          $security_deposit = convertCurrency($pr_info->security_deposit, $currency,$pr_info->currency_type);
       }

       $cleaning_fee=0;
       if($pr_info->cleaning_fee!=0)
       {
          $cleaning_fee = convertCurrency($pr_info->cleaning_fee, $currency,$pr_info->currency_type);
       }

       $extra_amount = $security_deposit+$cleaning_fee;
       
       $total_amount = 0;

       if($number_of_days < 7)
       {

        $check_in = date('Y-m-d',$check_in_time);
        $check_out = date('Y-m-d',$check_out_time);

         /*If Partial result found in pr_calender table  Starts*/
               for($i=1;$i<=$number_of_days;$i++)
               {
                 $res_checkin = $this->subadmin_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   { 
                     $total_amount += $res_checkin->price;
                   }
                   else
                   {
                     if(!empty($pr_info->weekendprice))
                     {
                        $day  = date('D',$check_in_time);
                        if($day=="Fri" || $day=="Sat")
                        {
                           $per_day = ($pr_info->weekendprice)/2;
                           $total_amount += round($per_day);
                        }
                        else
                        {
                           $total_amount +=$pr_info->application_fee;
                        }
                     }
                     else
                     {
                          $total_amount +=$pr_info->application_fee;
                     }

                   }
                     $check_in_time = strtotime($check_in);
                     $check_in_time = $check_in_time+24*60*60;
                     $check_in = date('Y-m-d',$check_in_time);
               }
           $actual_amount = convertCurrency($total_amount, $currency,$pr_info->currency_type);
           echo $actual_amount+$extra_amount;
           exit;
         /*If Partial result found in pr_calender table Ends*/
       }

       if($number_of_days>=7 && $number_of_days<30)
       {
          for ($i=1; $i<=$number_of_days ; $i++)
          { 
               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','wk');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk = $query->row();
               if(!empty($resulk))
               {
                   if($resulk->weekly_exact==1)
                   {
                      $diff = $resulk->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      if($days==7 && $check_out_time>=$resulk->end_date)
                      {
                        $total_amount +=$resulk->price;
                      }
                     else
                     {
                        $check_in =  date('Y-m-d',$check_in_time);
                        $res_checkin = $this->subadmin_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                         if(!empty($res_checkin))
                         {
                           $total_amount += $res_checkin->price;
                         }
                         else
                         {
                              if(!empty($pr_info->weekendprice))
                              {
                                 $day  = date('D',$check_in_time);
                                 if($day=="Fri" || $day=="Sat")
                                 {
                                   $per_day = ($pr_info->weekendprice)/2;
                                   $total_amount += $per_day;
                                 }
                                 else
                                 {
                                    $total_amount +=$pr_info->application_fee;
                                 }
                              }
                              else
                              {
                                   $total_amount +=$pr_info->application_fee;
                              }
                         }
                      }                     
                   }
                   else
                   {
                     $per_day = ($resulk->price)/7;
                     $total_amount += round($per_day);
                   }
               }
               elseif(!empty($pr_info->weekly))
               {
                  $per_day = ($pr_info->weekly)/7;
                  $total_amount += round($per_day);
               }                     
               else
               {
                  $check_in =  date('Y-m-d',$check_in_time);
                  $res_checkin = $this->subadmin_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   {
                     $total_amount += $res_checkin->price;
                   }
                   else
                   {
                        if(!empty($pr_info->weekendprice))
                        {
                           $day  = date('D',$check_in_time);
                           if($day=="Fri" || $day=="Sat")
                           {
                             $per_day = ($pr_info->weekendprice)/2;
                             $total_amount += $per_day;
                           }
                           else
                           {
                              $total_amount +=$pr_info->application_fee;
                           }
                        }
                        else
                        {
                             $total_amount +=$pr_info->application_fee;
                        }
                   }
               }                     
                  $check_in_time = $check_in_time+24*60*60;
          }

           $actual_amount = convertCurrency($total_amount, $currency,$pr_info->currency_type);    
           echo $actual_amount+$extra_amount;
           exit;
       }


       if($number_of_days>=30)
       {
          for ($i=1; $i<=$number_of_days ; $i++)
          { 
               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','m');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk = $query->row();

               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','wk');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk_weakly = $query->row();

               if(!empty($resulk))
               {
                   if($resulk->monthly_exact==1)
                   {
                      $diff = $resulk->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      $month_days = $resulk->days+1;
                      if($days==$month_days && $check_out_time>=$resulk->end_date)
                      {
                        $total_amount +=$resulk->price;
                      }
                     else
                     {
                        $check_in =  date('Y-m-d',$check_in_time);
                        $res_checkin = $this->subadmin_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                         if(!empty($res_checkin))
                         {
                           $total_amount += $res_checkin->price;
                         }
                         else
                         {
                              if(!empty($pr_info->weekendprice))
                              {
                                 $day  = date('D',$check_in_time);
                                 if($day=="Fri" || $day=="Sat")
                                 {
                                   $per_day = ($pr_info->weekendprice)/2;
                                   $total_amount += $per_day;
                                 }
                                 else
                                 {
                                    $total_amount +=$pr_info->application_fee;
                                 }
                              }
                              else
                              {
                                    $total_amount +=$pr_info->application_fee;
                              }
                          }
                      }                     
                   }
                   else
                   {
                     $diff = $resulk->end_date-$resulk->start_date;
                     $days = ($diff/86400)+1;
                     $per_day = ($resulk->price)/$days;
                     $total_amount += round($per_day);
                   }
               }


               elseif(!empty($resulk_weakly))
               {
                   if($resulk_weakly->weekly_exact==1)
                   {
                      $diff = $resulk_weakly->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      if($days==7 && $check_out_time>=$resulk_weakly->end_date)
                      {
                        $total_amount +=$resulk_weakly->price;
                      }
                       else
                       {

                          $check_in =  date('Y-m-d',$check_in_time);
                          $res_checkin = $this->subadmin_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                           if(!empty($res_checkin))
                           {
                             $total_amount += $res_checkin->price;
                           }
                           else
                           {
                                if(!empty($pr_info->weekendprice))
                                {
                                   $day  = date('D',$check_in_time);
                                   if($day=="Fri" || $day=="Sat")
                                   {
                                     $per_day = ($pr_info->weekendprice)/2;
                                     $total_amount += $per_day;
                                   }
                                   else
                                   {
                                      $total_amount +=$pr_info->application_fee;
                                   }
                                }
                                else
                                {
                                     $total_amount +=$pr_info->application_fee;
                                }
                             }
                        }                     
                   }
                   else
                   {
                     $per_day = ($resulk_weakly->price)/7;
                     $total_amount += round($per_day);
                   }
               }

               elseif(!empty($pr_info->monthly))
               {
                  $per_day = ($pr_info->monthly)/30;
                  $total_amount += round($per_day);
               }    

               elseif(!empty($pr_info->weekly))
               {
                  $per_day = ($pr_info->weekly)/7;
                  $total_amount += round($per_day);
               }     

               else
               {
                  $check_in =  date('Y-m-d',$check_in_time);
                  $res_checkin = $this->subadmin_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   {
                     $total_amount += $res_checkin->price;
                   }
                   else
                   {
                        if(!empty($pr_info->weekendprice))
                        {
                           $day  = date('D',$check_in_time);
                           if($day=="Fri" || $day=="Sat")
                           {
                             $per_day = ($pr_info->weekendprice)/2;
                             $total_amount += $per_day;
                           }
                           else
                           {
                              $total_amount +=$pr_info->application_fee;
                           }
                        }
                        else
                        {
                             $total_amount +=$pr_info->application_fee;
                        }
                   }
               }                     

                  $check_in_time = $check_in_time+24*60*60;
          }
           
           $actual_amount = convertCurrency($total_amount, $currency,$pr_info->currency_type);    
           echo $actual_amount+$extra_amount;
           exit;
       }

   }

	public function addBookingToCal($listing_id="", $startBooking='', $endBooking='')
	{
		if ((!empty($startBooking)) && (!empty($endBooking)))
		{
			$start = strtotime($startBooking);
			$k = getdate(strtotime($endBooking));
			$end = mktime(23,59,59,$k['mon'],$k['mday'],$k['year']);

			$rangeArray = $this->createranges($listing_id, $start, $end);

			$groupid = 0;
			$startselection = strtotime($startBooking);
			$endselection = strtotime($endBooking);
			$eve_type = 1;
			$operation = 0;

			foreach ($rangeArray as $key => $value)
			{
				$start = $value['start'];
				$eve_start = $start;
				$end = $value['end'];
				$a = getdate($eve_start);	// Start From here
				$eve_end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
				$this->subadmin_model->make_event_available($start, $end, $listing_id);
				$day = 1;
				$groupid = 0;
				while (TRUE)
				{
					if (isset($value['data']))
					{
					$data = $value['data'];
					$data['start'] = date('Y-m-d H:i:s', $eve_start);
					$data['end'] = date('Y-m-d H:i:s', $eve_end);
					$data['groupid'] = $groupid;
					}
				else
				{
					$data = array(
								'start' => date('Y-m-d H:i:s', $eve_start),
								'end' => date('Y-m-d H:i:s', $eve_end),
								'groupid' => $groupid,
								'color' => '#a10',
								'textColor' => '#a10',
								'event_type' => $eve_type,
								'property_id'   => $listing_id
								);
				}
				if (($eve_start <= $end) && ($eve_end >= $end))
				{
					if ($day == 1)
						{
						$data['rangeopen']	= 1;
						$data['rangeclose']	= 1;
						$groupid = $this->subadmin_model->insert('pr_calender', $data);
						$this->subadmin_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));	
						// echo "<br>Update";
						}else{
						$data['rangeopen']	= 0;
						$data['rangeclose']	= 1;
						$this->subadmin_model->insert('pr_calender', $data);
						// echo "<br>Insert";
						}
						//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
						$groupid++;
						break;
				}
				else
				{
					//UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
					$data['rangeclose'] = 0;
					if ($day == 1) {
					$data['rangeopen'] = 1;
					$groupid = $this->subadmin_model->insert('pr_calender', $data);
					$this->subadmin_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));
					// echo "<br>Update";
					}else{
					$data['rangeopen'] = 0;
					$this->subadmin_model->insert('pr_calender', $data);
					// echo "<br>Insert";
					}
				}

				$eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
				$eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
				$day++;
				}
			}
			return array('status' => true, 'msg' => "Selected date range has been marked as booking");
		}
		else
		{
		    return array('status' => false, 'msg' => "Please select date range");
		}
	}

	public function createranges($listing_id, $start, $end)
	{
		$eve = $this->subadmin_model->check_event_existance($start,$end,$listing_id);
		if (!empty($eve)) {
			usort($eve, array('subadmin', 'sortByDate'));
			// print_r($eve);
			$newstart = strtotime($eve[0]['start']);
			// $a = getdate(strtotime(end($eve)['start']));
			$arrayEnd = end($eve);
			// $a = getdate(strtotime($arrayEnd['start']));
			// $newend = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
			$newend = strtotime($arrayEnd['end']);
			$rangeArray = array();
			// echo "<br> S1 : ".date('l F jS H:i:s', $start);
			// echo "<br> E1 : ".date('l F jS H:i:s', $end);
			// echo "<br> S2 : ".date('l F jS H:i:s', $newstart);
			// echo "<br> E2 : ".date('l F jS H:i:s', $newend);
			// die();
			if (($start > $newstart) && ($end < $newend)) {// G
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] = $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				$a = getdate($end);
				$rangeArray[2]['data'] = $arrayEnd;//end($eve);
				$rangeArray[2]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[2]['end'] = $newend;
				$end = $newend ;
				// echo json_encode(array('rangeArray' => 'G'));
				// echo "<br> G";
				// die();
			}elseif (($start > $newstart) && ($end > $newend)) {// F
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] = $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				// echo json_encode(array('rangeArray' => 'F'));
				// die();
			}elseif (($start < $newstart) && ($end < $newend)) { //E
				$rangeArray[0]['start'] = $start ;
				$rangeArray[0]['end'] = $end;
				$a = getdate($end);
				$rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[1]['end'] = $newend;
				$rangeArray[1]['data'] = $arrayEnd;//end($eve); /***************///
				// echo json_encode(array('rangeArray' => 'E'));
				// die();
			}elseif (($start > $newstart) && ($end == $newend)) { //D
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] =  $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				// echo "<br> D";
				// die();
				// echo json_encode(array('rangeArray' => 'D'));
				// die();
			}elseif (($start == $newstart) && ($end < $newend)) { //C
				$rangeArray[0]['start'] = $start ;
				$rangeArray[0]['end'] = $end;
				$a = getdate($end);
				$rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[1]['end'] = $newend;
				$rangeArray[1]['data'] = $arrayEnd;
				// echo json_encode(array('rangeArray' => 'C'));
				// die();
			}elseif ($start == $newend) {//B
				$rangeArray[0]['start'] = $newstart ;
				$a = getdate($start);
				$rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
				$rangeArray[0]['data'] = $eve[0];
				$rangeArray[1]['start'] = $start;
				$rangeArray[1]['end'] = $end;
				// echo json_encode(array('rangeArray' => 'B'));
				// die();
			}elseif ($end == $newstart) {//A
				$rangeArray[0]['start'] = $start;
				$rangeArray[0]['end'] = $end;
				$a = getdate($end);
				$rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
				$rangeArray[1]['end'] = $newend;
				$rangeArray[1]['data'] = $arrayEnd;
				// echo json_encode(array('rangeArray' => 'A'));
				// die();
			}else{
				$rangeArray[0]['start'] = $start ;
				$rangeArray[0]['end'] = $end;
				// echo json_encode(array('rangeArray' => 'Default'));
				// die();
			}
			// foreach ($eve as $key => $value) {
			// 	$eve[strtotime($value['start'])] = $value;
			// 	unset($eve[$key]);
			// }
			// print_r($eve);
		}else{
			$rangeArray[0]['start'] = $start ;
			$rangeArray[0]['end'] = $end;
		}
		// foreach ($rangeArray as $key => $value) {
		// 	// echo "<br> RStart : ".date('F jS Y H:i:s', $value['start']);
		// 	// echo "<br> REnd : ".date('F jS Y H:i:s', $value['end']);
		// 	$rangeArray[$key]['start'] = date('F jS Y', $value['start']);
		// 	$rangeArray[$key]['end'] = date('F jS Y', $value['end']);
		// 	// echo "<br> End : ".date('F jS Y', $value['end']);
		// 	// if (isset($value['data']))
		// 	// 	print_r($value['data']);
		// }
		// echo "<pre>";
		// print_r($rangeArray);
		// die();
		// echo json_encode(array('msg' => $rangeArray));
		// die();
		return $rangeArray;
	}


	public function commision_fee()
	{
		$this->check_login();

		$data['commision_fee']=$this->subadmin_model->get_row('commision_fee');

        $data['template'] = 'subadmin/commision_fee';

        $this->load->view('templates/subadmin_template', $data);	
    }

	public function edit_commision_fee($commision_fee=''){

		$this->check_login();

		if(empty($commision_fee)) redirect('subadmin/settings'); 

		$data['commision_fee'] = $this->subadmin_model->get_row('commision_fee',array('id'=>$commision_fee)); 

		$this->form_validation->set_rules('commision_fee', 'Commision Fee', 'required|numeric');

		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');		

		if ($this->form_validation->run() == TRUE){	

			$updatedata=array(

					'commision_fee'=>$this->input->post('commision_fee'),	

					'created' => date('Y-m-d H:i:s'),	

			);	

		$this->subadmin_model->update('commision_fee',$updatedata,array('id'=>$commision_fee));	

		$this->session->set_flashdata('success_msg',"commision_fee has been updated successfully.");

		redirect('subadmin/commision_fee');

		}	

		$data['template'] = 'subadmin/edit_commision_fee';

        $this->load->view('templates/subadmin_template', $data);	

	}


	/*Notes Work strats From here*/

	public function featured_notes($featured_id='')
	{
		$this->check_login();
		if(empty($featured_id)) redirect('subadmin/featured_images'); 
		$data['notes']=$this->subadmin_model->get_result('featured_notes',array('featured_id' =>$featured_id));
		$data['id'] = $featured_id;
		$data['template'] = 'subadmin/featured_notes';
        $this->load->view('templates/subadmin_template', $data);		
	}



	public function add_featured_note($featured_id='')
	{
		$this->check_login(); 
		if(empty($featured_id)) redirect('subadmin/featured_images');
		$this->form_validation->set_rules('title', 'Title', 'required');			
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');			
		if ($this->form_validation->run() == TRUE){			
			$data=array(
						'title'=>$this->input->post('title'),				
						'description'=>$this->input->post('description'),
						'featured_id'=>$featured_id,
						'created' => date('Y-m-d H:i:s')		
				       );	
			$this->subadmin_model->insert('featured_notes',$data);		
			$this->session->set_flashdata('success_msg'," Successfully Featured Note Added.");
			redirect('subadmin/featured_notes/'.$featured_id);
		}
		$data['template'] = 'subadmin/add_featured_note';
        $this->load->view('templates/subadmin_template', $data);		
	}



	public function view_featured_note($id='')
	{
		$this->check_login();
		if(empty($id)) redirect('subadmin/featured_notes'); 
		$data['notes']=$this->subadmin_model->get_row('featured_notes',array('id' =>$id));
		if(empty($data['notes'])){
			redirect('subadmin/featured_notes');
		}
		$data['template'] = 'subadmin/view_featured_note';
        $this->load->view('templates/subadmin_template', $data);		
	}



	public function delete_featured_note($id="",$featured_id="")
	{
		if($id=="")
		{
			redirect('subadmin/featured_notes/'.$featured_id);
		}
		$this->subadmin_model->delete('featured_notes',array('id'=>$id));
		$this->session->set_flashdata('success_msg'," Featured Note has been deleted successfully.");
		redirect('subadmin/featured_notes/'.$featured_id);
	}

     

     /*Featured work ends from here*/
  public function manageEmail()
     {
	     $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id()));
        if($subadmin_restrictions->manage_email==2)
        {
             redirect('subadmin/dashboard');
        }


		if(isset($_POST['website_url']))
		{
			$this->form_validation->set_rules('website_url','Website Url','required');			
			$this->form_validation->set_rules('message_email','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'website_url_and_email'));
	            if(empty($response))
	            {
					$data = array(
         						'name' => 'website_url_and_email',
         						'website_url'=>$_POST['website_url'],
         						'message_from_which_email'=>$_POST['message_email'],
		         				);

					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_website_url',"Website settings has been added successfully");
					redirect('subadmin/manageEmail/website_url');
	            }
	            else
	            {
					$data = array(
         						'name' => 'website_url_and_email',
         						'website_url'=>$_POST['website_url'],
         						'message_from_which_email'=>$_POST['message_email'],
		         				);
					$this->subadmin_model->update('manage_email',$data,array('name'=>'website_url_and_email'));
					$this->session->set_flashdata('success_website_url',"Website settings has been updated successfully");
					redirect('subadmin/manageEmail/website_url');
	            }
			}
		}

		if(isset($_POST['after_adding_property']))
		{
			$this->form_validation->set_rules('subject_1','Subject','required');			
			$this->form_validation->set_rules('content_1','Content','required');			
			// $this->form_validation->set_rules('message_email_1','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_adding_property'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_adding_property',
								'subject' => $this->input->post('subject_1'),
								'content' => $this->input->post('content_1'),
								// 'message_from_which_email' => $this->input->post('message_email_1'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_adding_property',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_adding_property');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_1'),
									'content' => $this->input->post('content_1'),
									// 'message_from_which_email' => $this->input->post('message_email_1'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_adding_property'));
					$this->session->set_flashdata('success_after_adding_property',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_adding_property');
	            }
			}
		}

		if(isset($_POST['after_host_receives_book_request']))
		{
			$this->form_validation->set_rules('subject_2','Subject','required');			
			$this->form_validation->set_rules('content_2','Content','required');			
			// $this->form_validation->set_rules('message_email_2','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_host_receives_book_request'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_host_receives_book_request',
								'subject' => $this->input->post('subject_2'),
								'content' => $this->input->post('content_2'),
								// 'message_from_which_email' => $this->input->post('message_email_2'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_host_receives_book_request',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_host_receives_book_request');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_2'),
									'content' => $this->input->post('content_2'),
									// 'message_from_which_email' => $this->input->post('message_email_2'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_host_receives_book_request'));
					$this->session->set_flashdata('success_after_host_receives_book_request',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_host_receives_book_request');
	            }
			}
		}


		if(isset($_POST['reservation_notification_to_host_after_accepting_booking']))
		{
			$this->form_validation->set_rules('subject_3','Subject','required');			
			$this->form_validation->set_rules('content_3','Content','required');			
			// $this->form_validation->set_rules('message_email_3','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'reservation_notification_to_host_after_accepting_booking'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'reservation_notification_to_host_after_accepting_booking',
								'subject' => $this->input->post('subject_3'),
								'content' => $this->input->post('content_3'),
								// 'message_from_which_email' => $this->input->post('message_email_3'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_reservation_notification_to_host_after_accepting_booking',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#reservation_notification_to_host_after_accepting_booking');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_3'),
									'content' => $this->input->post('content_3'),
									// 'message_from_which_email' => $this->input->post('message_email_3'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'reservation_notification_to_host_after_accepting_booking'));
					$this->session->set_flashdata('success_reservation_notification_to_host_after_accepting_booking',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#reservation_notification_to_host_after_accepting_booking');
	            }
			}
		}

		if(isset($_POST['reservation_notification_to_guest_after_accepting_booking']))
		{
			$this->form_validation->set_rules('subject_4','Subject','required');			
			$this->form_validation->set_rules('content_4','Content','required');			
			// $this->form_validation->set_rules('message_email_3','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'reservation_notification_to_guest_after_accepting_booking'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'reservation_notification_to_guest_after_accepting_booking',
								'subject' => $this->input->post('subject_4'),
								'content' => $this->input->post('content_4'),
								// 'message_from_which_email' => $this->input->post('message_email_3'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_reservation_notification_to_guest_after_accepting_booking',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#reservation_notification_to_guest_after_accepting_booking');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_4'),
									'content' => $this->input->post('content_4'),
									// 'message_from_which_email' => $this->input->post('message_email_3'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'reservation_notification_to_guest_after_accepting_booking'));
					$this->session->set_flashdata('success_reservation_notification_to_guest_after_accepting_booking',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#reservation_notification_to_guest_after_accepting_booking');
	            }
			}
		}


		if(isset($_POST['on_the_day_of_guest_checkin']))
		{
			$this->form_validation->set_rules('subject_5','Subject','required');			
			$this->form_validation->set_rules('content_5','Content','required');			
			// $this->form_validation->set_rules('message_email_5','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'on_the_day_of_guest_checkin'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'on_the_day_of_guest_checkin',
								'subject' => $this->input->post('subject_5'),
								'content' => $this->input->post('content_5'),
								// 'message_from_which_email' => $this->input->post('message_email_5'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_on_the_day_of_guest_checkin',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#on_the_day_of_guest_checkin');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_5'),
									'content' => $this->input->post('content_5'),
									// 'message_from_which_email' => $this->input->post('message_email_5'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'on_the_day_of_guest_checkin'));
					$this->session->set_flashdata('success_on_the_day_of_guest_checkin',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#on_the_day_of_guest_checkin');
	            }
			}
		}

		if(isset($_POST['after_registiung_with_us']))
		{
			$this->form_validation->set_rules('subject_6','Subject','required');			
			$this->form_validation->set_rules('content_6','Content','required');			
			// $this->form_validation->set_rules('message_email_6','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_registiung_with_us'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_registiung_with_us',
								'subject' => $this->input->post('subject_6'),
								'content' => $this->input->post('content_6'),
								// 'message_from_which_email' => $this->input->post('message_email_6'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_registiung_with_us',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_registiung_with_us');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_6'),
									'content' => $this->input->post('content_6'),
									// 'message_from_which_email' => $this->input->post('message_email_6'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_registiung_with_us'));
					$this->session->set_flashdata('success_after_registiung_with_us',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_registiung_with_us');
	            }
			}
		}


		if(isset($_POST['_3_days_after_property_profile_incomplete']))
		{
			$this->form_validation->set_rules('subject_7','Subject','required');			
			$this->form_validation->set_rules('content_7','Content','required');			
			// $this->form_validation->set_rules('message_email_7','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'_3_days_after_property_profile_incomplete'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'_3_days_after_property_profile_incomplete',
								'subject' => $this->input->post('subject_7'),
								'content' => $this->input->post('content_7'),
								// 'message_from_which_email' => $this->input->post('message_email_7'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success__3_days_after_property_profile_incomplete',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#_3_days_after_property_profile_incomplete');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_7'),
									'content' => $this->input->post('content_7'),
									// 'message_from_which_email' => $this->input->post('message_email_7'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'_3_days_after_property_profile_incomplete'));
					$this->session->set_flashdata('success__3_days_after_property_profile_incomplete',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#_3_days_after_property_profile_incomplete');
	            }
			}
		}


		if(isset($_POST['your_profile_is_incomplete']))
		{
			$this->form_validation->set_rules('subject_8','Subject','required');			
			$this->form_validation->set_rules('content_8','Content','required');			
			// $this->form_validation->set_rules('message_email_8','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'your_profile_is_incomplete'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'your_profile_is_incomplete',
								'subject' => $this->input->post('subject_8'),
								'content' => $this->input->post('content_8'),
								// 'message_from_which_email' => $this->input->post('message_email_8'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_your_profile_is_incomplete',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#your_profile_is_incomplete');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_8'),
									'content' => $this->input->post('content_8'),
									// 'message_from_which_email' => $this->input->post('message_email_8'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'your_profile_is_incomplete'));
					$this->session->set_flashdata('success_your_profile_is_incomplete',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#your_profile_is_incomplete');
	            }
			}
		}


		if(isset($_POST['after_booking_hasbeen_cancelled_send_to_host']))
		{
			$this->form_validation->set_rules('subject_9','Subject','required');			
			$this->form_validation->set_rules('content_9','Content','required');			
			// $this->form_validation->set_rules('message_email_9','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_booking_hasbeen_cancelled_send_to_host'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_booking_hasbeen_cancelled_send_to_host',
								'subject' => $this->input->post('subject_9'),
								'content' => $this->input->post('content_9'),
								// 'message_from_which_email' => $this->input->post('message_email_9'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_booking_hasbeen_cancelled_send_to_host',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_booking_hasbeen_cancelled_send_to_host');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_9'),
									'content' => $this->input->post('content_9'),
									// 'message_from_which_email' => $this->input->post('message_email_9'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_booking_hasbeen_cancelled_send_to_host'));
					$this->session->set_flashdata('success_after_booking_hasbeen_cancelled_send_to_host',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_booking_hasbeen_cancelled_send_to_host');
	            }
			}
		}


		if(isset($_POST['after_booking_hasbeen_cancelled_send_to_guest']))
		{
			$this->form_validation->set_rules('subject_10','Subject','required');			
			$this->form_validation->set_rules('content_10','Content','required');			
			// $this->form_validation->set_rules('message_email_10','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_booking_hasbeen_cancelled_send_to_guest'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_booking_hasbeen_cancelled_send_to_guest',
								'subject' => $this->input->post('subject_10'),
								'content' => $this->input->post('content_10'),
								// 'message_from_which_email' => $this->input->post('message_email_10'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_booking_hasbeen_cancelled_send_to_guest',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_booking_hasbeen_cancelled_send_to_guest');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_10'),
									'content' => $this->input->post('content_10'),
									// 'message_from_which_email' => $this->input->post('message_email_10'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_booking_hasbeen_cancelled_send_to_guest'));
					$this->session->set_flashdata('success_after_booking_hasbeen_cancelled_send_to_guest',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_booking_hasbeen_cancelled_send_to_guest');
	            }
			}
		}


		if(isset($_POST['after_we_send_a_refund']))
		{
			$this->form_validation->set_rules('subject_11','Subject','required');			
			$this->form_validation->set_rules('content_11','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_we_send_a_refund'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_we_send_a_refund',
								'subject' => $this->input->post('subject_11'),
								'content' => $this->input->post('content_11'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_we_send_a_refund',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_we_send_a_refund');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_11'),
									'content' => $this->input->post('content_11'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_we_send_a_refund'));
					$this->session->set_flashdata('success_after_we_send_a_refund',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_we_send_a_refund');
	            }
			}
		}


		if(isset($_POST['reset_password_1']))
		{
			$this->form_validation->set_rules('subject_13','Subject','required');			
			$this->form_validation->set_rules('content_13','Content','required');			
			// $this->form_validation->set_rules('message_email_13','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'reset_password_1'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'reset_password_1',
								'subject' => $this->input->post('subject_13'),
								'content' => $this->input->post('content_13'),
								// 'message_from_which_email' => $this->input->post('message_email_13'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_reset_password_1',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#reset_password_1');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_13'),
									'content' => $this->input->post('content_13'),
									// 'message_from_which_email' => $this->input->post('message_email_13'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'reset_password_1'));
					$this->session->set_flashdata('success_reset_password_1',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#reset_password_1');
	            }
			}
		}


        if(isset($_POST['after_we_send_security_deposit']))
		{
			$this->form_validation->set_rules('subject_14','Subject','required');			
			$this->form_validation->set_rules('content_14','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_we_send_security_deposit'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_we_send_security_deposit',
								'subject' => $this->input->post('subject_14'),
								'content' => $this->input->post('content_14'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_we_send_security_deposit',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_we_send_security_deposit');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_14'),
									'content' => $this->input->post('content_14'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_we_send_security_deposit'));
					$this->session->set_flashdata('success_after_we_send_security_deposit',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_we_send_security_deposit');
	            }
			}
		}


        if(isset($_POST['accept_featured_image_request']))
		{
			$this->form_validation->set_rules('subject_15','Subject','required');			
			$this->form_validation->set_rules('content_15','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'accept_featured_image_request'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'accept_featured_image_request',
								'subject' => $this->input->post('subject_15'),
								'content' => $this->input->post('content_15'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_accept_featured_image_request',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#accept_featured_image_request');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_15'),
									'content' => $this->input->post('content_15'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'accept_featured_image_request'));
					$this->session->set_flashdata('success_accept_featured_image_request',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#accept_featured_image_request');
	            }
			}
		}


        if(isset($_POST['reject_featured_image_request']))
		{
			$this->form_validation->set_rules('subject_16','Subject','required');			
			$this->form_validation->set_rules('content_16','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'reject_featured_image_request'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'reject_featured_image_request',
								'subject' => $this->input->post('subject_16'),
								'content' => $this->input->post('content_16'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_reject_featured_image_request',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#reject_featured_image_request');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_16'),
									'content' => $this->input->post('content_16'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'reject_featured_image_request'));
					$this->session->set_flashdata('success_reject_featured_image_request',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#reject_featured_image_request');
	            }
			}
		}

        if(isset($_POST['user_request_for_featured_image']))
		{
			$this->form_validation->set_rules('subject_17','Subject','required');			
			$this->form_validation->set_rules('content_17','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'user_request_for_featured_image'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'user_request_for_featured_image',
								'subject' => $this->input->post('subject_17'),
								'content' => $this->input->post('content_17'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_user_request_for_featured_image',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#user_request_for_featured_image');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_17'),
									'content' => $this->input->post('content_17'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'user_request_for_featured_image'));
					$this->session->set_flashdata('success_user_request_for_featured_image',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#user_request_for_featured_image');
	            }
			}
		}


        if(isset($_POST['review_3_days_after_checkout']))
		{
			$this->form_validation->set_rules('subject_18','Subject','required');			
			$this->form_validation->set_rules('content_18','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'review_3_days_after_checkout'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'review_3_days_after_checkout',
								'subject' => $this->input->post('subject_18'),
								'content' => $this->input->post('content_18'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_review_3_days_after_checkout',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#review_3_days_after_checkout');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_18'),
									'content' => $this->input->post('content_18'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'review_3_days_after_checkout'));
					$this->session->set_flashdata('success_review_3_days_after_checkout',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#review_3_days_after_checkout');
	            }
			}
		}


        if(isset($_POST['after_contacting_us']))
		{
			$this->form_validation->set_rules('subject_19','Subject','required');			
			$this->form_validation->set_rules('content_19','Content','required');			
			// $this->form_validation->set_rules('message_email_11','Email','required|valid_email');			
			$this->form_validation->set_error_delimiters('<div style="color:red;margin-left:18%">', '</div>');	
			if($this->form_validation->run() == TRUE){
            $response = $this->subadmin_model->get_row('manage_email',array('name'=>'after_contacting_us'));
	            if(empty($response))
	            {
					$data = array(
								'name'    =>'after_contacting_us',
								'subject' => $this->input->post('subject_19'),
								'content' => $this->input->post('content_19'),
								// 'message_from_which_email' => $this->input->post('message_email_11'),
						        );
					$this->subadmin_model->insert('manage_email',$data);
					$this->session->set_flashdata('success_after_contacting_us',"Email settings has been added successfully");
					redirect('subadmin/manageEmail/#after_contacting_us');
	            }
	            else
	            {
					$data = array(
									'subject' => $this->input->post('subject_19'),
									'content' => $this->input->post('content_19'),
									// 'message_from_which_email' => $this->input->post('message_email_11'),
						         );
					$this->subadmin_model->update('manage_email',$data,array('name'=>'after_contacting_us'));
					$this->session->set_flashdata('success_after_contacting_us',"Email settings has been updated successfully");
					redirect('subadmin/manageEmail/#after_contacting_us');
	            }
			}
		}

		$data['template'] = 'subadmin/manageEmail';
        $this->load->view('templates/subadmin_template', $data);		

     }

     public function update_content(){	

		$this->check_login();

		$data['site_content'] = $this->subadmin_model->get_row('site_content',array('id'=>1));		
		$this->form_validation->set_rules('header_right','Header Right Section','required');
		$this->form_validation->set_rules('header_left','Header Left Section','required');
		$this->form_validation->set_rules('col1_heading','column 1 heading','required');
		$this->form_validation->set_rules('col2_heading','column 2 heading','required');
		$this->form_validation->set_rules('col3_heading','column 3 heading','required');
		$this->form_validation->set_rules('col1_description','column 1 description','required');
		$this->form_validation->set_rules('col2_description','column 2 description','required');
		$this->form_validation->set_rules('col3_description','column 3 description','required');
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	
		if($this->form_validation->run() == TRUE){
			$array = array(
				'col1_heading' => $this->input->post('col1_heading'),
				'col2_heading' => $this->input->post('col2_heading'),
				'col3_heading' => $this->input->post('col3_heading'),
				'col1_description' => $this->input->post('col1_description'),
				'col2_description' => $this->input->post('col2_description'),
				'col3_description' => $this->input->post('col3_description'),
				'header_left' => $this->input->post('header_left'),
				'header_right' => $this->input->post('header_right'),
			);
			


		    if($_FILES['col1_image']['name']!=''){
				$config['upload_path'] = './assets/uploads/content/';
				$config['allowed_types'] = 'gif|jpg|png';								
				$config['max_size']	= '0';
				$config['max_width']  = '300';
				$config['max_height']  = '300';
				$this->load->library('upload', $config);				
				if(! $this->upload->do_upload('col1_image')){
					$this->session->set_flashdata('col1_image_error', $this->upload->display_errors());
					redirect('subadmin/update_content/');
				}else{
					if($data['site_content']->col1_image !=''){
					$path = './assets/uploads/content/';
					$file = $data['site_content']->col1_image;
						if(!empty($file)){
							@unlink($path.$file);
						}
					}
					$data0 = $this->upload->data();
					$array['col1_image']=$data0['file_name'];
				}
			}


			if($_FILES['col2_image']['name']!=''){
				$config['upload_path'] = './assets/uploads/content/';
				$config['allowed_types'] = 'gif|jpg|png';				
				$config['max_size']	= '0';
				$config['max_width']  = '300';
				$config['max_height']  = '300';
				$this->load->library('upload', $config);				
				if(! $this->upload->do_upload('col2_image')){
					$this->session->set_flashdata('col2_image_error', $this->upload->display_errors());
					redirect('subadmin/update_content/');
				}else{
					if($data['site_content']->col2_image !=''){
					$path = './assets/uploads/content/';
					$file = $data['site_content']->col2_image;
						if(!empty($file)){
							@unlink($path.$file);
						}
					}
					$data0 = $this->upload->data();
					$array['col2_image']=$data0['file_name'];
				}
			}


			if($_FILES['col3_image']['name']!=''){
				$config['upload_path'] = './assets/uploads/content/';
				$config['allowed_types'] = 'gif|jpg|png';				
				$config['max_size']	= '0';
				$config['max_width']  = '300';
				$config['max_height']  = '300';
				$this->load->library('upload', $config);				
				if(! $this->upload->do_upload('col3_image')){
					$this->session->set_flashdata('col3_image_error', $this->upload->display_errors());
					redirect('subadmin/update_content/');
				}else{
					if($data['site_content']->col3_image !=''){
					$path = './assets/uploads/content/';
					$file = $data['site_content']->col3_image;
						if(!empty($file)){
							@unlink($path.$file);
						}
					}
					$data0 = $this->upload->data();
					$array['col3_image']=$data0['file_name'];
				}
			}

			$this->subadmin_model->update('site_content',$array,array('id'=>1));
			$this->session->set_flashdata('success_msg',"Content has been updated successfully.");
			redirect('subadmin/update_content');
		}


		$data['template'] = 'subadmin/update_content';
        $this->load->view('templates/subadmin_template', $data);
	}


	public function company_details(){
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('address','address','required');
		$this->form_validation->set_rules('phone','phone','required');
		$this->form_validation->set_error_delimiters('<div style="color:red;">', '</div>');	
		if($this->form_validation->run() == TRUE){
			$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'address' => $this->input->post('address'),
				);

			$this->subadmin_model->update('company_details', $data, array('id'=>1));
			$this->session->set_flashdata('success_msg', 'Updated');
			redirect(current_url());
		}

		$data['info'] = $this->subadmin_model->get_row('company_details', array('id'=>1));
		$data['template'] = 'subadmin/company_details';
        $this->load->view('templates/subadmin_template', $data);

	}




//////////  neighborhhod tagging system  ///////////
	public function suggestedTags($offset=0)
	{
		$this->check_login();
		$limit=10;
		$data['tags'] = $this->subadmin_model->get_pagination_result('suggest_tag' ,$limit, $offset);
		$config= get_theme_pagination();
		$config['base_url'] = base_url().'subadmin/suggestedTags/';
		$config['total_rows'] = $this->subadmin_model->get_pagination_result('suggest_tag', 0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['template'] = 'subadmin/suggestedTag';
        $this->load->view('templates/subadmin_template', $data);
	}

	public function deleteSuggestedTag($id='')
	{
		if($id == ''){ redirect('subadmin/suggestedTags'); }
		$this->check_login();
		$this->subadmin_model->delete('suggest_tag', array('id' => $id) );
		$this->session->set_flashdata('success_msg', 'Tag has been deleted successfully. ');
		redirect('subadmin/suggestedTags');
	}

	public function approveTag($id='')
	{
		if($id == ''){ redirect('subadmin/suggestedTags'); }
		$this->check_login();
		$tag = $this->subadmin_model->get_row('suggest_tag' , array('id' => $id) );
		if(empty($tag)){ redirect('subadmin/suggestedTags'); }
		$this->subadmin_model->update('suggest_tag', array('status' => 1), array('id' => $id) );
		$category = $this->subadmin_model->insert('neb_category', array('category_name'=>$tag->tag, 'created' => date('Y-m-d')));
		$this->subadmin_model->insert('neighbour_cat', array('category'=>$category, 'neighbour'=>$tag->nb_id, 'created'=> date('Y-m-d')), array('id' => $id) );
		$this->session->set_flashdata('success_msg', 'Tag has been approved successfully. ');
		redirect('subadmin/suggestedTags');
	}
//////////  neighborhhod tagging system  ///////////


		public function tool_add_favorites_category()
	{
			if($_POST)
			{
	                $response = $this->subadmin_model->get_row('tool_tip',array('tool_tip_page'=>'tool_add_favorites_category'));
		            if(empty($response))
		            {
						$this->subadmin_model->insert('tool_tip',array('tool_tip_page'=>"tool_add_favorites_category"));
					
						$insert_data = array(
			         						'name'=>$_POST['name'],
			         						'image'=>$_POST['image'],
					         				);
						$json_encode =   json_encode($insert_data);

						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_add_favorites_category'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been added successfully");
						redirect('subadmin/tool_add_favorites_category');
		            }
		            else
		            {
						$update_data = array(
	         						'name'=>$_POST['name'],
	         						'image'=>$_POST['image'],
			         				);
						$json_encode = json_encode($update_data);
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_add_favorites_category'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been Updated successfully");
						redirect('subadmin/tool_add_favorites_category');
		            }

			}
		$data['tool_info'] = $this->subadmin_model->get_row('tool_tip', array('tool_tip_page'=>'tool_add_favorites_category'));
		$data['template'] = 'subadmin/tool_add_favorites_category';
        $this->load->view('templates/admin_template', $data);
	}


	public function tool_user_profile()
	{
			if($_POST)
			{
	                $response = $this->subadmin_model->get_row('tool_tip',array('tool_tip_page'=>'tool_user_profile'));
		            if(empty($response))
		            {
						$this->subadmin_model->insert('tool_tip',array('tool_tip_page'=>"tool_user_profile"));
						$update = array(
	         						'first_name'=>$_POST['first_name'],
	         						'last_name'=>$_POST['last_name'],
	         						'gender'=>$_POST['gender'],
	         						'user_email'=>$_POST['user_email'],
	         						'contact'=>$_POST['contact'],
	         						'address'=>$_POST['address'],
	         						'city'=>$_POST['city'],
	         						'state'=>$_POST['state'],
	         						'country'=>$_POST['country'],
	         						'zip'=>$_POST['zip'],
	         						'paypal_email'=>$_POST['paypal_email'],
	         						'language'=>$_POST['language'],
	         						'groups'=>$_POST['groups'],
			         				);
						$json_encode =   json_encode($update);
					
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_user_profile'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been added successfully");
						redirect('subadmin/tool_user_profile');
		            }
		            else
		            {
						$update = array(
	         						'first_name'=>$_POST['first_name'],
	         						'last_name'=>$_POST['last_name'],
	         						'gender'=>$_POST['gender'],
	         						'user_email'=>$_POST['user_email'],
	         						'contact'=>$_POST['contact'],
	         						'address'=>$_POST['address'],
	         						'city'=>$_POST['city'],
	         						'state'=>$_POST['state'],
	         						'country'=>$_POST['country'],
	         						'zip'=>$_POST['zip'],
	         						'paypal_email'=>$_POST['paypal_email'],
	         						'language'=>$_POST['language'],
	         						'groups'=>$_POST['groups'],
			         				);

						$json_encode =   json_encode($update);
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_user_profile'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been Updated successfully");
						redirect('subadmin/tool_user_profile');
		            }

			}
		$data['tool_info'] = $this->subadmin_model->get_row('tool_tip', array('tool_tip_page'=>'tool_user_profile'));
		$data['template'] = 'subadmin/tool_user_profile';
        $this->load->view('templates/admin_template', $data);
	}


	public function tool_on_add_property()
	{
			if($_POST)
			{
	                $response = $this->subadmin_model->get_row('tool_tip',array('tool_tip_page'=>'tool_on_add_property'));
		            if(empty($response))
		            {
						$this->subadmin_model->insert('tool_tip',array('tool_tip_page'=>"tool_on_add_property"));
						$update = array(
	         						'property_type'=>$_POST['property_type'],
	         						'accomodates'=>$_POST['accomodates'],
	         						'room_type'=>$_POST['room_type'],
	         						'bedrooms'=>$_POST['bedrooms'],
	         						'title'=>$_POST['title'],
	         						'description'=>$_POST['description'],
	         						'price'=>$_POST['price'],
			         				);
						$json_encode =   json_encode($update);
					
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_add_property'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been added successfully");
						redirect('subadmin/tool_on_add_property');
		            }
		            else
		            {
						$update = array(
	         						'property_type'=>$_POST['property_type'],
	         						'accomodates'=>$_POST['accomodates'],
	         						'room_type'=>$_POST['room_type'],
	         						'bedrooms'=>$_POST['bedrooms'],
	         						'title'=>$_POST['title'],
	         						'description'=>$_POST['description'],
	         						'price'=>$_POST['price'],
			         				);

						$json_encode =   json_encode($update);
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_add_property'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been Updated successfully");
						redirect('subadmin/tool_on_add_property');
		            }

			}
		$data['tool_info'] = $this->subadmin_model->get_row('tool_tip', array('tool_tip_page'=>'tool_on_add_property'));
		$data['template'] = 'subadmin/tool_on_add_property';
        $this->load->view('templates/admin_template', $data);
	}


	public function tool_on_update_property()
	{
			if($_POST)
			{
	                $response = $this->subadmin_model->get_row('tool_tip',array('tool_tip_page'=>'tool_on_update_property'));
		            if(empty($response))
		            {
						$this->subadmin_model->insert('tool_tip',array('tool_tip_page'=>"tool_on_update_property"));
						$update = array(
	         						'property_type'=>$_POST['property_type'],
	         						'room_type'=>$_POST['room_type'],
	         						'title'=>$_POST['title'],
	         						'description'=>$_POST['description'],
	         						'accomodates'=>$_POST['accomodates'],
	         						'bedrooms'=>$_POST['bedrooms'],
	         						'bathrooms'=>$_POST['bathrooms'],
	         						'size'=>$_POST['size'],
	         						'bed_type'=>$_POST['bed_type'],
	         						'category'=>$_POST['category'],
	         						'house_rules'=>$_POST['house_rules'],
	         						'house_manual'=>$_POST['house_manual'],
	         						'location_information'=>$_POST['location_information'],
			         				);
						$json_encode =   json_encode($update);
					
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_update_property'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been added successfully");
						redirect('subadmin/tool_on_update_property');
		            }
		            else
		            {
						$update = array(
			         						'property_type'=>$_POST['property_type'],
			         						'room_type'=>$_POST['room_type'],
			         						'title'=>$_POST['title'],
			         						'description'=>$_POST['description'],
			         						'accomodates'=>$_POST['accomodates'],
			         						'bedrooms'=>$_POST['bedrooms'],
			         						'bathrooms'=>$_POST['bathrooms'],
			         						'size'=>$_POST['size'],
			         						'bed_type'=>$_POST['bed_type'],
			         						'category'=>$_POST['category'],
			         						'house_rules'=>$_POST['house_rules'],
			         						'house_manual'=>$_POST['house_manual'],
			         						'location_information'=>$_POST['location_information'],
					         				);

						$json_encode =   json_encode($update);
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_update_property'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been Updated successfully");
						redirect('subadmin/tool_on_update_property');
		            }

			}
		$data['tool_info'] = $this->subadmin_model->get_row('tool_tip', array('tool_tip_page'=>'tool_on_update_property'));
		$data['template'] = 'subadmin/tool_on_update_property';
        $this->load->view('templates/admin_template', $data);
	}


	public function tool_on_basic_pricing()
	{
			if($_POST)
			{
	                $response = $this->subadmin_model->get_row('tool_tip',array('tool_tip_page'=>'tool_on_basic_pricing'));
		            if(empty($response))
		            {
						$this->subadmin_model->insert('tool_tip',array('tool_tip_page'=>"tool_on_basic_pricing"));
						$update = array(
	         						'currency_type'=>$_POST['currency_type'],
	         						'nightly'=>$_POST['nightly'],
	         						'weekly'=>$_POST['weekly'],
	         						'monthly'=>$_POST['monthly'],
	         						'sublet'=>$_POST['sublet'],
	         						'cleaning_fee'=>$_POST['cleaning_fee'],
	         						'policy'=>$_POST['policy'],
	         						'minimum_stay'=>$_POST['minimum_stay'],
	         						'maximum_stay'=>$_POST['maximum_stay'],
			         				);
						$json_encode =   json_encode($update);
					
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_basic_pricing'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been added successfully");
						redirect('subadmin/tool_on_basic_pricing');
		            }
		            else
		            {
						$update = array(
		         						'currency_type'=>$_POST['currency_type'],
		         						'nightly'=>$_POST['nightly'],
		         						'weekly'=>$_POST['weekly'],
		         						'monthly'=>$_POST['monthly'],
		         						'sublet'=>$_POST['sublet'],
		         						'cleaning_fee'=>$_POST['cleaning_fee'],
		         						'policy'=>$_POST['policy'],
		         						'minimum_stay'=>$_POST['minimum_stay'],
		         						'maximum_stay'=>$_POST['maximum_stay'],
				         				);

						$json_encode =   json_encode($update);
						$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_basic_pricing'));
						$this->session->set_flashdata('success_msg',"Tool Tip has been Updated successfully");
						redirect('subadmin/tool_on_basic_pricing');
		            }

			}
		$data['tool_info'] = $this->subadmin_model->get_row('tool_tip', array('tool_tip_page'=>'tool_on_basic_pricing'));
		$data['template'] = 'subadmin/tool_on_basic_pricing';
        $this->load->view('templates/admin_template', $data);
	}



	public function tool_on_advance_pricing()
	{
		if($_POST)
		{
                $response = $this->subadmin_model->get_row('tool_tip',array('tool_tip_page'=>'tool_on_advance_pricing'));
	            if(empty($response))
	            {
					$this->subadmin_model->insert('tool_tip',array('tool_tip_page'=>"tool_on_advance_pricing"));
					$update = array(
         						'security_deposit'=>$_POST['security_deposit'],
         						'weekend_pricing'=>$_POST['weekend_pricing'],
         						'daily_pricing'=>$_POST['daily_pricing'],
         						'weekly_pricing'=>$_POST['weekly_pricing'],
         						'monthly_pricing'=>$_POST['monthly_pricing'],
		         				);
					$json_encode =   json_encode($update);
				
					$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_advance_pricing'));
					$this->session->set_flashdata('success_msg',"Tool Tip has been added successfully");
					redirect('subadmin/tool_on_advance_pricing');
	            }
	            else
	            {
					$update = array(
	         						'security_deposit'=>$_POST['security_deposit'],
	         						'weekend_pricing'=>$_POST['weekend_pricing'],
	         						'daily_pricing'=>$_POST['daily_pricing'],
	         						'weekly_pricing'=>$_POST['weekly_pricing'],
	         						'monthly_pricing'=>$_POST['monthly_pricing'],
			         				);

					$json_encode =   json_encode($update);
					$this->subadmin_model->update('tool_tip',array('page_data'=>$json_encode),array('tool_tip_page'=>'tool_on_advance_pricing'));
					$this->session->set_flashdata('success_msg',"Tool Tip has been Updated successfully");
					redirect('subadmin/tool_on_advance_pricing');
	            }
		}
		$data['tool_info'] = $this->subadmin_model->get_row('tool_tip', array('tool_tip_page'=>'tool_on_advance_pricing'));
		$data['template'] = 'subadmin/tool_on_advance_pricing';
        $this->load->view('templates/admin_template', $data);
	}















}