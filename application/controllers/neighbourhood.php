<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Neighbourhood extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();			
		$this->load->model('front_model');
		$this->load->helper('bnb');
		$currency = $this->session->userdata('currency');
		$data['row']=$this->front_model->get_row('site_details');
	    if(empty($currency))
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	    elseif($data['row']->currency_status==1)
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	}

	public function index()
	{
		$data['cities']         = $this->front_model->get_result('cities');
		$data['neighbours'] = $this->front_model->get_result('neighbourhoods');
		$data['template'] = 'neighbourhood/cities';
		$this->load->view('templates/front_template', $data);
	}

	public function city($city_id="", $neighbour="")
	{
		if($city_id=="") { redirect('neighbourhood/index'); }
		$data['city']        = get_city_info($city_id); 		
		$data['neighbours']  = get_neighbourhoot_of_city($city_id);
		$data['featured_nb'] = get_featured_nb_of_city($city_id);
		if($neighbour != "")
		{
			$data['filter']   = $this->front_model->get_result('neb_category');  
			$data['template'] = 'neighbourhood/neighbours';
			$this->load->view('templates/front_template', $data);
		}
		else
		{
			$data['template'] = 'neighbourhood/city';
			$this->load->view('templates/front_template', $data);
		}	
	}	

	public function neighbour($nb_id="")
	{
		if($nb_id=="") { redirect('neighbourhood/index'); }
		$data['neighbour'] = $this->front_model->get_row('neighbourhoods', array('id'=> $nb_id));		
		$data['template']  = 'neighbourhood/neighbourhood';
		$this->load->view('templates/front_template', $data);
	}



	public function nb_ajax_filter($length="", $city="")
	{	
		if($length == 0)
		{
			$this->db->where('city', $city);
			$nb_res = $this->db->get('neighbourhoods');
			$data['neighbours'] = $nb_res->result();
			$res = $this->load->view('neighbourhood/nbs_filter_template', $data);
			echo $res;
		}	

		$filter   = $this->input->post('filter');
		if(!empty($filter))
		{
			$this->db->select('neighbour');
			$this->db->from('neighbour_cat');
			foreach($filter as $value)
			{
				$this->db->or_where('category', $value);
			}
			$query   = $this->db->get();
			$result1 = $query->result();
			
			$string = json_encode($result1);
			
			foreach($result1 as $nb)
			{
				$val   = $nb->neighbour;
				$check = substr_count($string, '{"neighbour":"'.$val.'"}');
				if($check == $length)
				{
					$result[] = $val;
				}
			}	
 
			if(!empty($result))
			{
				$this->db->select('*');
				$this->db->from('neighbourhoods');
				foreach($result as $row)
				{
					$this->db->or_where('id', $row);
					$this->db->where('city', $city);
				}
				$nb_res = $this->db->get();
				$data['neighbours'] = $nb_res->result();
				
				if(!empty($data['neighbours']))
				{
					$res = $this->load->view('neighbourhood/nbs_filter_template', $data);
					echo $res;
				}
				else
				{	
					echo false;
				}	
			}
			else
			{
				echo false;
			}
		}
		else
		{
			echo false;
		}
	}




	public function submitTag()
	{
		$nb_id = $this->input->post('nb_id');
		$nb = $this->input->post('nb');
		$tag = $this->input->post('tag');

		$data = array(
			'nb_id' => $nb_id,
			'nb' => $nb,
			'tag' => $tag,
			'status' => 0,
			'created' => time()
			);
		$this->front_model->insert('suggest_tag', $data);
		echo 'Your tag has been submitted successfully.';
	}






}