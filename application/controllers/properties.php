<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Properties extends CI_Controller {

	public function __construct(){
		parent::__construct();			
		clear_cache();
		$this->load->model('front_model');
     $currency = $this->session->userdata('currency');
       $data['row']=$this->front_model->get_row('site_details');
      if(empty($currency))
      {
        $currency = $data['row']->currency;
          $this->session->set_userdata('currency',$currency);
      }
      elseif($data['row']->currency_status==1)
      {
        $currency = $data['row']->currency;
          $this->session->set_userdata('currency',$currency);
      }

	}


    public function index($offset=0)
    {

      if(isset($_POST['location']))
      {
        $location = $this->input->post('location');
        $data['loca_tion'] = $this->input->post('location');
      }
      else
      {
        $location = "";
      }

      if(isset($_POST['search_check_in']))
      {
        $check_in = $this->input->post('search_check_in');
        $data['c_in_date'] = $this->input->post('search_check_in');
      }
      else
      {
        $check_in = "";
      }

      if(isset($_POST['search_check_out']))
      {
        $check_out = $this->input->post('search_check_out');
        $data['c_out_date'] = $this->input->post('search_check_out');
      }
      else
      {
        $check_out = "";
      }

      if(isset($_POST['guest']))
      {
        $guest = $this->input->post('guest');
        $data['g_count'] = $this->input->post('guest');
      }
      else
      {
        $guest = "";
      }

      if(isset($_POST['neghborhood_id']))
      {
        $neghborhood_id = $this->input->post('neghborhood_id');
        $data['neghborhood_id'] = $this->input->post('neghborhood_id');;
      }
      else
      {
        $neghborhood_id = "";
      }

      $data['location']  = $location;
      $data['amenities'] = $this->front_model->get_result('amenities');
      $data['property']  = $this->front_model->get_property($offset, $location, $check_in, $check_out, $guest, $neghborhood_id);

      /* For meta tags Starts*/
       $data['meta_title'] = "All Property Title";
       $data['meta_description'] = "All Property Description";
       $data['meta_author'] = "Shane Halton";
      /* For meta tags Ends*/
     

      $data['template'] = 'properties/listings';
      $this->load->view('templates/front_template', $data);
    }


    public function details($id="",$flag=""){
      if($id ==""){
        redirect('properties');
      }
      $data['details'] = $this->front_model->get_details($id);
      if(!$data['details']){
        redirect('properties');
      }    

      /* For meta tags Starts*/
       $data['meta_title'] = $data['details']->title;
       $data['meta_description'] = $data['details']->description;
       $data['meta_author'] = $data['details']->first_name." ".$data['details']->last_name;
      /* For meta tags Ends*/

      $data['flag'] = $flag;
      $data['template'] = 'properties/details';
      $this->load->view('templates/front_template', $data);
    }

    public function ajaxfilter_property($offset = 0){   
      if($this->input->post()){
        $post = $_POST;  
        // echo json_encode($_POST);
        $data['properties'] = $this->front_model->filter_property($post, $offset);
          $res = "";
         if (!empty($data['properties'])):
         $res = $this->load->view('properties/ajax_filter_property',$data,TRUE);     
         $photo_view = $this->load->view('properties/ajax_filter_photo_property',$data,TRUE);     
                 $loc = "";

                 foreach ($data['properties'] as $key) {
                    $loc .= '<span id="'.$key->id.'">'.$key->unit_street_address.' '.$key->city.' '.$key->state.' '.$key->country.'</span>';
                 }

                 $arr = array('list'=>$res,'photo_view'=>$photo_view,'loctn'=>$loc);
                echo json_encode($arr);
             else: 
              return FALSE;
             endif; 
      }else{
        return FALSE;
      }
    }


    public function ajaxloadmore($offset)
    {
      $location = $this->input->post('location');
      $check_in = $this->input->post('check_in');
      $check_out = $this->input->post('check_out');
      $neghborhood_id = $this->input->post('neghborhood_id');
      $guest = $this->input->post('guest');

      $data['properties'] = $this->front_model->get_property($offset, $location, $check_in, $check_out, $guest,$neghborhood_id);
          $res = "";
         if (!empty($data['properties'])):
         $res = $this->load->view('properties/ajax_filter_property',$data,TRUE);     
         $photo_view = $this->load->view('properties/ajax_filter_photo_property',$data,TRUE);     
                  $loc = "";
                 foreach ($data['properties'] as $key) {
                    $loc .= '<span id="'.$key->id.'">'.$key->unit_street_address.' '.$key->city.' '.$key->state.' '.$key->country.'</span>';
                 }

                 // echo $loc;
                 $arr = array('list'=>$res,'photo_view'=>$photo_view,'loctn'=>$loc);
                echo json_encode($arr);               
             else: 
              return FALSE;
             endif;          
    }

    public function send_review()
    {
        $customer = $this->session->userdata('user_info');
        if(empty($customer)) { redirect('properties'); }
        $customer_id = $customer['id'];
        $pr_id   = $this->input->post('pr_id');
        $user_id = $this->input->post('user_id');
        $review = $this->input->post('review');
        $insertdata = array(
                        'review' => $review,
                   'property_id' => $pr_id,
                       'user_id' => $user_id,
                   'customer_id' => $customer_id,
                        'status' => 1,
                       'created' => date('Y-m-d h:i:s')
            );
        $this->front_model->insert('review', $insertdata);
        
        $data['pr_id'] = $this->input->post('pr_id');
        $res = $this->load->view('properties/review_template', $data);
        echo $res;
    }

    public function sendReviewRating()
    {
        $customer_info = $this->session->userdata('user_info');
        $customer = $customer_info['id'];
        
        $insertdata = array(
                        'review' => $this->input->post('review'),
                        'rating' => $this->input->post('radio_rating'),
                   'property_id' => $this->input->post('space'),
                       'user_id' => $this->input->post('owner'),
                   'customer_id' => $customer,
                        'status' => 1,
                       'created' => date('Y-m-d h:i:s')
            );

        $this->front_model->insert('review', $insertdata);
        
        $data['space'] = $this->input->post('space');
        $res = $this->load->view('properties/review_template', $data);
        echo $res;
    }



    public function like_property()
    {
        $customer_id = $this->input->post('customer_id');
        $pr_id = $this->input->post('pr_id');
        $user_id = $this->input->post('user_id');
        $insertdata = array(
                   'property_id' => $pr_id,
                       'user_id' => $user_id,
                   'customer_id' => $customer_id,
                        'status' => 1,
                       'created' => date('Y-m-d h:i:s')
            );
        $this->front_model->insert('favorites_property', $insertdata);
        echo "liked";
    }

    public function unlike_property()
    {
        $customer_id = $this->input->post('customer_id');
        $pr_id = $this->input->post('pr_id');
        $array = array(
                   'property_id' => $pr_id,
                   'customer_id' => $customer_id,
            );
        $this->front_model->delete('favorites_property', $array);
        echo 'unliked';
    }

    public function property_rating()
    {   
        $customer    = $this->session->userdata('user_info');  
        $customer_id = $customer['id'];
        $pr_id       = $this->input->post('pr_id');
        $pr_rate     = $this->input->post('pr_rate');
        $insertdata  = array(
              'pr_id'       => $pr_id,
              'customer_id' => $customer_id,
                   'rating' => $pr_rate,
                   'status' => 1
            );
        $this->front_model->delete('property_rating', array('customer_id' => $customer_id, 'pr_id' => $pr_id ) );
        $this->front_model->insert('property_rating', $insertdata);
        echo "rated successfully";
    }




 /*  Work on groups on 14 feb*/

   public function all_groups(){

      /* For meta tags Starts*/
       $data['meta_title'] = "Go2hk Groups";
       $data['meta_description'] = "Go2hk Groups Description";
       $data['meta_author'] = "Shane Halton";
      /* For meta tags Ends*/


    $offset=0;
    $this->form_validation->set_rules('search_group', 'Search Group', 'required');
      if($this->form_validation->run()==TRUE){
         $limit = 15;
         $search = $this->input->post('search_group');
         $data['groups'] = $this->front_model->get_pagination_like('groups',$limit,$offset,$search); 
         $data['num_of_groups'] = 0;  
         $data['template'] = 'properties/all_groups.php';
         $this->load->view('templates/front_template', $data);
      }
      else{
          $limit = 3;
          $data['groups'] = $this->front_model->get_pagination_like('groups',$limit,$offset);  
          $data['num_of_groups'] = $this->front_model->get_pagination_like('groups',0,0);  
          $data['template'] = 'properties/all_groups.php';
          $this->load->view('templates/front_template', $data);
      }
   }
    

   public function group_properties($group_id="",$offset=0,$search=""){
      if(empty($group_id)){
        redirect('properties/all_groups');
      }
        $data['groups'] = $this->front_model->get_row('groups',array('id'=>$group_id));
      $this->form_validation->set_rules('search_properties', 'Search Properties', 'required');
        if($this->form_validation->run()==TRUE){
          $limit = 15;
          $search = $this->input->post('search_properties');
          $data['group_properties'] = $this->front_model->group_members_properties($group_id,$limit,$offset,$search);  
          $data['num_of_group_properties'] = "";  
          $data['template'] = 'properties/group_properties.php';
          $this->load->view('templates/front_template', $data);
           
        }
        else{
            $limit = 2;
            $data['group_properties'] = $this->front_model->group_members_properties($group_id,$limit,$offset);
            $data['num_of_group_properties'] = $this->front_model->group_members_properties($group_id,0,0);
            $data['template'] = 'properties/group_properties.php';
            $this->load->view('templates/front_template', $data);
        }
   }


    public function ajax_load_more_group_property($offset=0,$group_id=""){
     $limit = 2;
     $data['group_properties'] = $this->front_model->group_members_properties($group_id,$limit,$offset);
     $group_properties = $this->load->view('properties/aj_load_properties', $data, TRUE);
     echo $group_properties;
    }
    public function ajax_load_more_groups($offset=0){
     $limit = 2;
     $data['groups'] = $this->front_model->get_pagination_result('groups',$limit,$offset);
     $groups = $this->load->view('properties/aj_load_groups', $data, TRUE);
     echo $groups;
    }

 /*  Work on groups on 14 feb*/

    public function ajax_send_contact_me_message()
    {
        $customer  = $this->session->userdata('user_info');
        $cus_name  = $customer['first_name']." ".$customer['last_name'];
        $cus_email = $customer['user_email'];
        $cus_id    = $customer['id'];
        $pr_id     = $this->input->post('pr_id');
        $pr_title  = $this->input->post('pr_title');
        $owner_id  = $this->input->post('owner_id');
        $check_in  = $this->input->post('check_in');
        $check_out = $this->input->post('check_out');
        $guest     = $this->input->post('guest');
        $message   = $this->input->post('message');
        $contact   = $this->input->post('contact');
        $data = array(
                   'pr_id'      => $pr_id,
                   'pr_user_id' => $owner_id,
                   'pr_title'   => $pr_title,
                   'check_in'   => $check_in,
                   'check_out'  => $check_out,
                   'guest'      => $guest,
                   'sender_id'  => $cus_id,
                   'user_name'  => $cus_name,
                   'user_email' => $cus_email,
                   'subject'    => 0,
                   'message'    => $message,
                   'status'     => 0,
                   'created'    => date('Y-m-d, h:i:s')
                );
        $this->front_model->insert('user_message', $data);
        $responce = $this->sendmsg($message, $contact);
        echo $responce;
    }

    function sendmsg($message, $to)
    {
        $this->load->library('twilio');

        $from = '+1 415-599-2671';
        $response = $this->twilio->sms($from, $to, $message);
        if($response->IsError)
        {
            return 'Error: ' . $response->ErrorMessage;
        }
        else
        {
            return 'Sent message to ' . $to;
        }    
    }

  /* 18feb  Working on show All members of group  Starts*/

    public function view_group_members($group_id="",$offset=0){
         if($group_id==""){
         redirect('properties/all_groups');
         }
      $limit = 2;
      $data['group_id'] = $group_id;
      $data['template'] = 'properties/group_members';
      $this->load->view('templates/front_template', $data);    
    }

    public function customer_group_members($group_id="",$offset=0){
            if($group_id==""){
                redirect('properties/all_groups');
            }
        $limit = 10;
        $data['group_id'] = $group_id;
        $data['members'] = $this->front_model->get_customer_members($group_id,$limit,$offset);
        $data['template'] = 'properties/customer_group_members';
        $this->load->view('templates/front_template', $data);        
    }

    public function owner_group_members($group_id="",$offset=0){
            if($group_id==""){
                redirect('properties/all_groups');
            }
        $limit = 2;
        $data['groups'] = $this->front_model->get_row('groups',array('id'=>$group_id));
        $data['members'] = $this->front_model->get_owner_members($group_id,$limit,$offset);
        $data['num_of_members'] = $this->front_model->get_owner_members($group_id,0,0);
        $data['template'] = 'properties/owner_group_members';
        $this->load->view('templates/front_template', $data);        
    }



    public function ajax_load_more_group_members($offset=0,$group_id=""){
     $limit = 2;
     $data['members'] = $this->front_model->get_owner_members($group_id,$limit,$offset);
     $members = $this->load->view('properties/ajax_load_more_group_members', $data, TRUE);
     echo $members;
    }

  /* 18feb  Working on show All members of group Ends*/

   // this function using by booking system

   public function get_effective_booking_rate()
   {
        $currency = $this->session->userdata('currency');
        $pr_id = $_POST['pr_id'];
        $pr_info = get_property_detail($pr_id);
        $c_in = $_POST['check_in'];
        $c_out = $_POST['check_out'];
        $no_of_guest = $_POST['no_of_guest'];

       $check_in_time =  strtotime($c_in);
       $check_out_time = strtotime($c_out);
       $differrence =  $check_out_time-$check_in_time;
       $number_of_days = ($differrence/86400)+1;


      $remaining_guest = $no_of_guest - $pr_info->additional_guest;
        if($remaining_guest > 0)
        {
          $additional_guest_amount = $remaining_guest*$pr_info->additional_guest_price*$number_of_days;
        } 

          
       // if(get_my_id()=="")
       // {
       //   echo "Please Login First For Booking !";
       //   exit;
       // }

       // if(get_my_id()===$pr_info->user_id)
       // {
       //   echo "You Can't Book Your own property";
       //   exit;
       // }

       
       // if($check_out_time < $check_in_time)
       // {
       //  echo "Invalid date range";
       //   exit;
       // }

       // if($pr_info->min_stay!="" && $number_of_days < $pr_info->min_stay)
       // {
       //    echo "This duration is Too short for booking minimum ".$pr_info->min_stay." Days is Required";
       //    exit;
       // }


       // if($pr_info->max_stay!="" && $number_of_days > $pr_info->max_stay)
       // {
       //    echo "This duration is Too Long for booking maximum ".$pr_info->max_stay." Days is allowed";
       //    exit;
       // }


       // if(!check_booking_date_availability($pr_id,$check_in_time,$check_out_time))
       // {
       //    echo "Property is not available or already booked for this date range. Please Choose different date range";
       //    exit;
       // }
       
       $total_amount = 0;

       if($number_of_days < 7)
       {

        $check_in = date('Y-m-d',$check_in_time);
        $check_out = date('Y-m-d',$check_out_time);

         /*If Partial result found in pr_calender table  Starts*/
               for($i=1;$i<=$number_of_days;$i++)
               {
                 $res_checkin = $this->front_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   { 
                     $total_amount += $res_checkin->price;
                   }
                   else
                   {
                     if(!empty($pr_info->weekendprice))
                     {
                        $day  = date('D',$check_in_time);
                        if($day=="Fri" || $day=="Sat")
                        {
                           $per_day = ($pr_info->weekendprice)/2;
                           $total_amount += round($per_day);
                        }
                        else
                        {
                           $total_amount +=$pr_info->application_fee;
                        }
                     }
                     else
                     {
                          $total_amount +=$pr_info->application_fee;
                     }

                   }
                     $check_in_time = strtotime($check_in);
                     $check_in_time = $check_in_time+24*60*60;
                     $check_in = date('Y-m-d',$check_in_time);
               }

            if($remaining_guest > 0)
            {
               $total_amount+= $additional_guest_amount;
            }
           $actual_amount = convertCurrency($total_amount, $currency,$pr_info->currency_type);
           echo $actual_amount;
           exit;
         /*If Partial result found in pr_calender table Ends*/
       }

       if($number_of_days>=7 && $number_of_days<30)
       {
          for ($i=1; $i<=$number_of_days ; $i++)
          { 
               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','wk');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk = $query->row();
               if(!empty($resulk))
               {
                   if($resulk->weekly_exact==1)
                   {
                      $diff = $resulk->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      if($days==7 && $check_out_time>=$resulk->end_date)
                      {
                        $total_amount +=$resulk->price;
                      }
                     else
                     {
                        $check_in =  date('Y-m-d',$check_in_time);
                        $res_checkin = $this->front_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                         if(!empty($res_checkin))
                         {
                           $total_amount += $res_checkin->price;
                         }
                         else
                         {
                              if(!empty($pr_info->weekendprice))
                              {
                                 $day  = date('D',$check_in_time);
                                 if($day=="Fri" || $day=="Sat")
                                 {
                                   $per_day = ($pr_info->weekendprice)/2;
                                   $total_amount += $per_day;
                                 }
                                 else
                                 {
                                    $total_amount +=$pr_info->application_fee;
                                 }
                              }
                              else
                              {
                                   $total_amount +=$pr_info->application_fee;
                              }
                         }
                      }                     
                   }
                   else
                   {
                     $per_day = ($resulk->price)/7;
                     $total_amount += round($per_day);
                   }
               }
               elseif(!empty($pr_info->weekly))
               {
                  $per_day = ($pr_info->weekly)/7;
                  $total_amount += round($per_day);
               }                     
               else
               {
                  $check_in =  date('Y-m-d',$check_in_time);
                  $res_checkin = $this->front_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   {
                     $total_amount += $res_checkin->price;
                   }
                   else
                   {
                        if(!empty($pr_info->weekendprice))
                        {
                           $day  = date('D',$check_in_time);
                           if($day=="Fri" || $day=="Sat")
                           {
                             $per_day = ($pr_info->weekendprice)/2;
                             $total_amount += $per_day;
                           }
                           else
                           {
                              $total_amount +=$pr_info->application_fee;
                           }
                        }
                        else
                        {
                             $total_amount +=$pr_info->application_fee;
                        }
                   }
               }                     
                  $check_in_time = $check_in_time+24*60*60;
          }

            if($remaining_guest > 0)
            {
               $total_amount+= $additional_guest_amount;
            }
           $actual_amount = convertCurrency($total_amount, $currency,$pr_info->currency_type);    
           echo $actual_amount;
           exit;
       }


       if($number_of_days>=30)
       {
          for ($i=1; $i<=$number_of_days ; $i++)
          { 
               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','m');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk = $query->row();

               $this->db->select('*');
               $this->db->from('listing_price');
               $this->db->where('listing_id',$pr_id);
               $this->db->where('source','wk');
               $this->db->where('price !=',0);
               $this->db->where('start_date <=', $check_in_time);
               $this->db->where('end_date >=', $check_in_time);
               $query = $this->db->get();
               $resulk_weakly = $query->row();

               if(!empty($resulk))
               {
                   if($resulk->monthly_exact==1)
                   {
                      $diff = $resulk->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      $month_days = $resulk->days+1;
                      if($days==$month_days && $check_out_time>=$resulk->end_date)
                      {
                        $total_amount +=$resulk->price;
                      }
                     else
                     {
                        $check_in =  date('Y-m-d',$check_in_time);
                        $res_checkin = $this->front_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                         if(!empty($res_checkin))
                         {
                           $total_amount += $res_checkin->price;
                         }
                         else
                         {
                              if(!empty($pr_info->weekendprice))
                              {
                                 $day  = date('D',$check_in_time);
                                 if($day=="Fri" || $day=="Sat")
                                 {
                                   $per_day = ($pr_info->weekendprice)/2;
                                   $total_amount += $per_day;
                                 }
                                 else
                                 {
                                    $total_amount +=$pr_info->application_fee;
                                 }
                              }
                              else
                              {
                                    $total_amount +=$pr_info->application_fee;
                              }
                          }
                      }                     
                   }
                   else
                   {
                     $diff = $resulk->end_date-$resulk->start_date;
                     $days = ($diff/86400)+1;
                     $per_day = ($resulk->price)/$days;
                     $total_amount += round($per_day);
                   }
               }


               elseif(!empty($resulk_weakly))
               {
                   if($resulk_weakly->weekly_exact==1)
                   {
                      $diff = $resulk_weakly->end_date-$check_in_time;
                      $days = ($diff/86400)+1;
                      if($days==7 && $check_out_time>=$resulk_weakly->end_date)
                      {
                        $total_amount +=$resulk_weakly->price;
                      }
                       else
                       {

                          $check_in =  date('Y-m-d',$check_in_time);
                          $res_checkin = $this->front_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                           if(!empty($res_checkin))
                           {
                             $total_amount += $res_checkin->price;
                           }
                           else
                           {
                                if(!empty($pr_info->weekendprice))
                                {
                                   $day  = date('D',$check_in_time);
                                   if($day=="Fri" || $day=="Sat")
                                   {
                                     $per_day = ($pr_info->weekendprice)/2;
                                     $total_amount += $per_day;
                                   }
                                   else
                                   {
                                      $total_amount +=$pr_info->application_fee;
                                   }
                                }
                                else
                                {
                                     $total_amount +=$pr_info->application_fee;
                                }
                             }
                        }                     
                   }
                   else
                   {
                     $per_day = ($resulk_weakly->price)/7;
                     $total_amount += round($per_day);
                   }
               }

               elseif(!empty($pr_info->monthly))
               {
                  $per_day = ($pr_info->monthly)/30;
                  $total_amount += round($per_day);
               }    

               elseif(!empty($pr_info->weekly))
               {
                  $per_day = ($pr_info->weekly)/7;
                  $total_amount += round($per_day);
               }     

               else
               {
                  $check_in =  date('Y-m-d',$check_in_time);
                  $res_checkin = $this->front_model->get_row('pr_calender',array('property_id'=>$pr_id,'start'=>$check_in));
                   if(!empty($res_checkin))
                   {
                     $total_amount += $res_checkin->price;
                   }
                   else
                   {
                        if(!empty($pr_info->weekendprice))
                        {
                           $day  = date('D',$check_in_time);
                           if($day=="Fri" || $day=="Sat")
                           {
                             $per_day = ($pr_info->weekendprice)/2;
                             $total_amount += $per_day;
                           }
                           else
                           {
                              $total_amount +=$pr_info->application_fee;
                           }
                        }
                        else
                        {
                             $total_amount +=$pr_info->application_fee;
                        }
                   }
               }                     

                  $check_in_time = $check_in_time+24*60*60;
          }

            if($remaining_guest > 0)
            {
               $total_amount+= $additional_guest_amount;
            }
           
           $actual_amount = convertCurrency($total_amount, $currency,$pr_info->currency_type);    
           echo $actual_amount;
           exit;
       }

   }

    

    public function booking()
    {

      /* For meta tags Starts*/
       $data['meta_title'] = "Booking Title";
       $data['meta_description'] = "Booking Description";
       $data['meta_author'] = "Shane Halton";
      /* For meta tags Ends*/


          $coupon_type = $this->input->post('coupon_type');
        $coupon_amount = $this->input->post('coupon_amount');
             $pr_id    = $this->input->post('pr_id');
             $cin      = $this->input->post('cin');
             $cout     = $this->input->post('cout');
             $subtotal = $this->input->post('subtotal');
             $days     = $this->input->post('days');
             $customer = $this->session->userdata('user_info');
             $c_id     = $customer['id'];
        $data  = array(
                      'pr_id'   => $pr_id,
                      'cin'     => $cin,
                      'cout'    => $cout,
                      'subtotal'=> $subtotal,
                      'c_id'    => $c_id,
                      'days'    => $days,
                      'coupon_type' => $coupon_type,
                      'coupon_amount' => $coupon_amount,
                      );
        
        if($pr_id =="")
        {
           redirect('properties');
        }
        $data['details'] = $this->front_model->get_details($pr_id); //$pr_id);    
        $data['template'] = 'properties/booking';
        $this->load->view('templates/front_template', $data);
    }


   public function confirm_booking()
    {
        if(!empty($_post)){ redirect('properties'); }
        $customer    = $this->session->userdata('user_info');
        $customer_id = $customer['id'];
        $event = array( 
        'pr_id'    => $this->input->post('pr_id'), 
        'owner_id' => $this->input->post('owner_id'), 
        'check_in' => $this->input->post('check_in'),
        'check_out'=> $this->input->post('check_out'), 
     'total_amount' => $this->input->post('total_amount'), 
        'message'  => $this->input->post('message'), 
        'comment'  => $this->input->post('comment'), 
        'city'     => $this->input->post('city'), 
        'inquiry'  => $this->input->post('inquiry'),
        'time_zone'=> $this->input->post('time_zone'), 
        'contact'  => $this->input->post('contact'), 
        'payment_country' => $this->input->post('payment_country'),
        'commision'  => $this->input->post('commision'), 
        'tax' => $this->input->post('tax'),
        ); 

        if(isset($_POST['coupon_type']))
        { 
           $event['coupon_type'] = $_POST['coupon_type'];
           $event['coupon_amount'] = $_POST['coupon_amount'];
        }

        $this->session->set_userdata('event' , $event);
        $data['customer'] = $this->front_model->get_row('users', array('id'=>$customer_id));
        $data['event']    = $event;
        $data['template'] = 'properties/confirm_booking';
        $this->load->view('templates/front_template', $data);
    } 


   
    public function success()
    {    
        if(!$this->session->userdata('event')){ redirect('properties'); }
        if(!$this->session->userdata('user_info')){ redirect('front'); }
        if(empty($_REQUEST)){ redirect('properties'); }
        
        $cus      = $this->session->userdata('user_info');
        $cus_id   = $cus['id'];
        $cus_name = $cus['first_name']." ".$cus['last_name'];
        $customer = $this->front_model->get_row('users', array('id'=>$cus_id));
        $book     = $this->session->userdata('event');             
        $currency = $this->session->userdata('currency');

        $prop_info = $this->front_model->get_row('properties',array('id'=>$book['pr_id']));
        
        $booking  = array(
                    'transaction_id' => $_REQUEST['txn_id'],
                    'extra'          => json_encode($_REQUEST),
                    'pr_id'          => $book['pr_id'],   
                    'owner_id'       => $book['owner_id'],
                    'customer_id'    => $cus_id,
                    'customer_name'  => $cus_name,
                    'check_in'       => $book['check_in'],
                    'check_out'      => $book['check_out'],
                    'comment'        => $book['comment'],
                    'city'           => $book['city'], 
                    'inquiry'        => $book['inquiry'], 
                    'time_zone'      => $book['time_zone'],
                    'contact'        => $book['contact'],
                    'total_amount'   => $book['total_amount'],
                    'booking_currency_type'   =>$currency,
                    'created'        => date('m/d/Y'),
                    'modified'       => date('m/d/Y'),
                    'commision'      => $book['commision'],
                    'tax'            => $book['tax'],

        );

        if(!empty($prop_info->security_deposit))
         {
          $booking['security_deposit'] = convertCurrency($prop_info->security_deposit,$currency, $prop_info->currency_type);
         }
         
        if(!empty($book['coupon_type']))
         {
          $booking['coupon_type'] = $book['coupon_type'];
          $booking['coupon_amount'] = $book['coupon_amount'];
         }


        $booking_id = $this->front_model->insert('bookings',$booking);

        $this->addBookingToCal($book['pr_id'],$book['check_in'],$book['check_out']);

        $host_info = $this->front_model->get_row('users',array('id'=>$book['owner_id']));
        $payments = array(
                  'transaction_id'      => $_REQUEST['txn_id'],
                  'transaction_details' => json_encode($_REQUEST),
                  'booking_id'          => $booking_id,
                  'property_id'         => $book['pr_id'],
                  'user_id'             => $cus_id,
                  'guest_name'          => $cus_name,
                  'host_name'           => $host_info->first_name." ".$host_info->last_name,
                  'property_name'       => $prop_info->title,
                  'payment_type'        => 'PAYPAL',
                  'is_refund'           => 0,
                  'paid_to_owner'       => 0,
                  'owner_id'            => $book['owner_id'],
                  'amount'              => $book['total_amount'],
                  'booking_currency_type'=> $currency,
                  'created'             => date('Y-m-d h:i:s'),
                );

                  if(!empty($prop_info->security_deposit))
                   {
                    $payments['security_deposit'] = convertCurrency($prop_info->security_deposit,$currency, $prop_info->currency_type);
                   }

        $payment_id = $this->front_model->insert('payments',$payments);
        
        $property = $this->front_model->get_row('properties', array('id'=>$book['pr_id']));
        $message  = array(
                'pr_id'      => $property->id,
                'pr_user_id' => $property->user_id,
                'pr_title'   => $property->title,
                'check_in'   => $book['check_in'],
                'check_out'  => $book['check_out'],
                'guest'      => 0,
                'sender_id'  => $cus_id,
                'user_name'  => $customer->first_name." ".$customer->last_name,
                'user_email' => $customer->user_email,
                'subject'    => $booking_id." (booking number)",
                'message'    => $book['message'],
                'status'     => 0,
                'created'    => date('Y-m-d')
            );
         //////////commision For Inviter Starts ///////////////
         //////////commision For Inviter Starts ///////////////
        $current_user_info = $this->session->userdata('user_info');
        $gain = $this->front_model->get_row('invitation',array('joined_user_id'=>$current_user_info['id']));
          if(!empty($gain) && $gain->booking_credit_given==0)
          {
        $this->front_model->update('invitation',array('booking_credit_given'=>1),array('joined_user_id'=>$current_user_info['id']));
          }
         //////////commision For Inviter Ends////////////////
         //////////commision For Inviter Ends////////////////

         //////////Send Booking Request Email Starts////////////////
         //////////Send Booking Request Email Starts////////////////
       
        $this->load->library('smtp_lib/smtp_email');

        $email_template = get_manage_email_info('after_host_receives_book_request');
        $website_info = get_manage_email_info('website_url_and_email');
        $website_url = $website_info->website_url;
        $from_email = $website_info->message_from_which_email;

        $check_in_date = date('d-F-Y',strtotime($book['check_in']));
        $check_out_date = date('d-F-Y',strtotime($book['check_out']));

        $subject = $email_template->subject;

        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
        $arr_update = array($host_info->first_name,$customer->first_name,$check_in_date,$check_out_date,$property->title,$website_url);
        $html =  str_replace($arr_old,$arr_update,$email_template->content);

               
        
        $to = array($host_info->user_email);
        $from = array($from_email =>$from_email);  // From email in array form 
        $status_of_email = $this->smtp_email->sendEmail($from, $to, $subject, $html);

            $insert_data = array(
                        'name'=>$host_info->first_name,
                        'email'=>$host_info->user_email,
                        'subject'=>$subject,
                        'message'=>$html,
                        'created'=>date('Y-m-d h:i:s')
                        );

            $this->front_model->insert('notifications',$insert_data);


         //////////Send Booking Request Email Ends////////////////
         //////////Send Booking Request Email Ends////////////////
 
        $this->front_model->insert('user_message',$message);       
        $this->session->unset_userdata('event');
        $this->session->set_flashdata('success_msg','Property Has Been booked Successfully.');
        redirect('user/bookings_by_me');
    }



    public function cancel()
    {       
        $this->session->unset_userdata('event');
        $this->session->set_flashdata('error_msg', 'Booking has been cancelled successfully.');
        redirect('user/bookings');  
    }

    public function check_coupon_valid()
    {
      $coupon_code = $_POST['coupon_code'];
      $response = $this->front_model->get_row('coupons',array('code'=>$coupon_code));
      if(empty($response))
      {
         echo json_encode(array('stat'=>'Coupon does not exist'));
      } 
      else 
      {
          $startdate = $response->start_date;
          $enddate = $response->end_date;
          $current_date = date('m/d/Y');

          if($startdate<=$current_date && $current_date<=$enddate)
          { 
             if($response->reduction_type=="1")
             {
                 $type ="discount"; 
             }
             else
             {
                 $type = "amount";
             }
             $arr = array('stat'=>'Coupon accepted','r_type'=>$type,'amount'=>$response->reduction_amount);
             echo json_encode($arr);
          }
          else
          {
             echo json_encode(array('stat'=>'Coupon is out of date'));
          }
      }
   
    }

    public function view_front_profile($user_id="",$offset=0)
    {

      /* For meta tags Starts*/
       $data['meta_title'] = "Host Profile Title";
       $data['meta_description'] = "Host Profile Description";
       $data['meta_author'] = "Shane Halton";
      /* For meta tags Ends*/

      if(empty($user_id))
      {
        redirect('properties');
      }

      $current_login_user_id =  get_my_id();
      if(!empty($current_login_user_id))
      {
          if($user_id==$current_login_user_id)
          {
             redirect('user/view_profile');
          }
      }

      $limit = 2;
      $data['properties'] = $this->front_model->property_details_for_profile($limit, $offset, $user_id);
      $data['total_rows'] = $this->front_model->property_details_for_profile(0, 0, $user_id);
      $data['user_id'] = $user_id;
      $data['count'] = count($data['properties']);
      $data['user'] = $this->front_model->get_row('users', array('id' => $user_id));
      $data['groups'] = $this->front_model->get_my_groups($user_id);
      $data['language'] = $this->front_model->get_result('languages', array('user_id' => $user_id));
      $data['template'] = 'properties/view_front_profile';
      $this->load->view('templates/front_template', $data);
    }

    public function ajax_load_property($offset=0,$user_id="")
    {
        $limit = 3;
        $data['properties'] = $this->front_model->property_details_for_profile($limit, $offset,$user_id);
        $res="";
      if(!empty($data['properties'])){
        
        $res = $this->load->view('properties/ajax_load_user_profile_property',$data,TRUE);
        echo $res;
      }else
        return FALSE;
   }

   public function collection_category()
   {
      /* For meta tags Starts*/
       $data['meta_title'] = "Collection Category Title";
       $data['meta_description'] = "Collection Category Description";
       $data['meta_author'] = "Shane Halton";
      /* For meta tags Ends*/

      $data['template'] = 'properties/collection_category';
      $this->load->view('templates/front_template', $data);
   }


   public function collection_property($collection_id="")
   {

      /* For meta tags Starts*/
       $data['meta_title'] = "Collection Property Title";
       $data['meta_description'] = "Collection Property Description";
       $data['meta_author'] = "Shane Halton";
      /* For meta tags Ends*/

    if(empty($collection_id))redirect('properties/collection_category');
    $data['collection_info'] = $this->front_model->get_row('collection_category',array('id'=>$collection_id));
    $data['collection_property_info'] = $this->front_model->collection_property($collection_id);
    $data['template'] = 'properties/collection_property';
    $this->load->view('templates/front_template', $data);
   }

   public function invite($inviter_id="")
   {
    $data = array('time'=>time(),'inviter_id'=>$inviter_id);
    $this->session->set_userdata('social_invite',$data);
    redirect('front');
   }


  public function host_payment($booking_id="")
  {
      if(empty($booking_id))
      {
        redirect('front');
      }
      
      $row = $this->front_model->get_row('bookings',array('id'=>$booking_id));
      if($row->link_expired==1)
      {
        $this->session->set_flashdata('error_msg',"This link has been expired.");
        redirect('front/index/0/1');
      }

      if($row->status==2)
      {
        $this->session->set_flashdata('error_msg',"This Booking Has been Cancelled.");
        redirect('front/index/0/1');
      }
   

      $data['currency'] = $row->booking_currency_type;
      $data['booking_id'] = $row->id;
      $data['user_info'] = $this->front_model->get_row('users',array('id'=>$row->customer_id));
      $data['amount'] = $row->total_amount;
      $data['template'] = 'properties/confirm_host_payments';
      $this->load->view('templates/front_template', $data);
  }



  public function success_host_payments($booking_id="")
  {
        if(empty($booking_id)){ redirect('front'); }
        if(empty($_REQUEST)){ redirect('front'); }

        $currency = $this->session->userdata('currency');

        $book = $this->front_model->get_row('bookings',array('id'=>$booking_id));

        $customer = $this->front_model->get_row('users', array('id'=>$book->customer_id));

        $prop_info = $this->front_model->get_row('properties',array('id'=>$book->pr_id));
        
        $booking  = array(
                        'transaction_id' => $_REQUEST['txn_id'],
                        'extra'          => json_encode($_REQUEST),
                        'modified'       => date('m/d/Y'),
                        'status'         =>1,
                        );

      
        $this->front_model->update('bookings',$booking,array('id'=>$book->id));



        $host_info = $this->front_model->get_row('users',array('id'=>$book->owner_id));
        $payments = array(
                  'transaction_id'      => $_REQUEST['txn_id'],
                  'transaction_details' => json_encode($_REQUEST),
                  'booking_id'          => $book->id,
                  'property_id'         => $book->pr_id,
                  'user_id'             => $customer->id,
                  'guest_name'          => $customer->first_name." ".$customer->last_name,
                  'host_name'           => $host_info->first_name." ".$host_info->last_name,
                  'property_name'       => $prop_info->title,
                  'payment_type'        => 'PAYPAL',
                  'is_refund'           => 0,
                  'paid_to_owner'       => 0,
                  'owner_id'            => $book->owner_id,
                  'amount'              => $book->total_amount,
                  'booking_currency_type'=> $currency,
                  'created'             => date('Y-m-d h:i:s'),
                );

        if(!empty($prop_info->security_deposit))
         {
          $payments['security_deposit'] = convertCurrency($prop_info->security_deposit,$currency, $prop_info->currency_type);
         }

        $payment_id = $this->front_model->insert('payments',$payments);
        
        $property = $this->front_model->get_row('properties', array('id'=>$book->pr_id));
        $message  = array(
                'pr_id'      => $property->id,
                'pr_user_id' => $property->user_id,
                'pr_title'   => $property->title,
                'check_in'   => $book->check_in,
                'check_out'  => $book->check_out,
                'guest'      => 0,
                'sender_id'  => $customer->id,
                'user_name'  => $customer->first_name." ".$customer->last_name,
                'user_email' => $customer->user_email,
                'subject'    => $book->id." (booking number)",
                // 'message'    => $book->message,
                'status'     => 0,
                'created'    => date('Y-m-d')
            );

        $this->front_model->insert('user_message',$message);       

        $this->front_model->update('bookings',array('link_expired'=>1),array('id'=>$book->id));

        $this->session->set_flashdata('success_msg','Property Has Been booked Successfully.');
         
        if(get_my_id())
        {
            redirect('user/bookings_by_me');
        }
        else
        {
          redirect('front/index/0/1');
        }
  }

  public function cancel_host_payments($booking_id)
  {
    
    $this->front_model->update('bookings',array('status'=>2),array('id'=>$booking_id));

    $booking_info = $this->front_model->get_row('bookings',array('id'=>$booking_id));
    $check_in = $booking_info->check_in;
    $check_out = $booking_info->check_out;
    $pr_id = $booking_info->pr_id;
    $this->front_model->make_event_available($check_in, $check_out,$pr_id);

    $this->session->set_flashdata('error_msg','The property with a booking  Id '.$booking_id.'  Has been cancelled successfully.');
    if(get_my_id())
    {
        redirect('user/bookings_by_me');
    }
    else
    {
      redirect('front/index/0/1');
    }

  }

    public function sortByDate($a,$b)
    {
      $d1 = strtotime($a['start']);
      $d2 = strtotime($b['start']);
      return $d1 == $d2 ? 0 : ($d1 > $d2 ? 1 : -1);
    }



  public function addBookingToCal($listing_id="", $startBooking='', $endBooking='')
  {
    if ((!empty($startBooking)) && (!empty($endBooking)))
    {
      $start = strtotime($startBooking);
      $k = getdate(strtotime($endBooking));
      $end = mktime(23,59,59,$k['mon'],$k['mday'],$k['year']);

      $rangeArray = $this->createranges($listing_id, $start, $end);

      $groupid = 0;
      $startselection = strtotime($startBooking);
      $endselection = strtotime($endBooking);
      $eve_type = 1;
      $operation = 0;

      foreach ($rangeArray as $key => $value)
      {
        $start = $value['start'];
        $eve_start = $start;
        $end = $value['end'];
        $a = getdate($eve_start); // Start From here
        $eve_end = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
        $this->front_model->make_event_available($start, $end, $listing_id);
        $day = 1;
        $groupid = 0;
        while (TRUE)
        {
          if (isset($value['data']))
          {
          $data = $value['data'];
          $data['start'] = date('Y-m-d H:i:s', $eve_start);
          $data['end'] = date('Y-m-d H:i:s', $eve_end);
          $data['groupid'] = $groupid;
          }
        else
        {
          $data = array(
                'start' => date('Y-m-d H:i:s', $eve_start),
                'end' => date('Y-m-d H:i:s', $eve_end),
                'groupid' => $groupid,
                'color' => '#a10',
                'textColor' => '#a10',
                'event_type' => $eve_type,
                'property_id'   => $listing_id
                );
        }
        if (($eve_start <= $end) && ($eve_end >= $end))
        {
          if ($day == 1)
            {
            $data['rangeopen']  = 1;
            $data['rangeclose'] = 1;
            $groupid = $this->front_model->insert('pr_calender', $data);
            $this->front_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));  
            // echo "<br>Update";
            }else{
            $data['rangeopen']  = 0;
            $data['rangeclose'] = 1;
            $this->front_model->insert('pr_calender', $data);
            // echo "<br>Insert";
            }
            //UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
            $groupid++;
            break;
        }
        else
        {
          //UNAVAILABLE : #666, Booked : #a10, AVALABLE : #819f2a
          $data['rangeclose'] = 0;
          if ($day == 1) {
          $data['rangeopen'] = 1;
          $groupid = $this->front_model->insert('pr_calender', $data);
          $this->front_model->update('pr_calender', array('groupid' => $groupid), array('id' => $groupid));
          // echo "<br>Update";
          }else{
          $data['rangeopen'] = 0;
          $this->front_model->insert('pr_calender', $data);
          // echo "<br>Insert";
          }
        }

        $eve_start = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
        $eve_end = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);
        $day++;
        }
      }
      return array('status' => true, 'msg' => "Selected date range has been marked as booking");
    }
    else
    {
        return array('status' => false, 'msg' => "Please select date range");
    }
  }


  public function createranges($listing_id, $start, $end)
  {
    $eve = $this->front_model->check_event_existance($start,$end,$listing_id);
    if (!empty($eve)) {
      usort($eve, array('properties', 'sortByDate'));
      // print_r($eve);
      $newstart = strtotime($eve[0]['start']);
      // $a = getdate(strtotime(end($eve)['start']));
      $arrayEnd = end($eve);
      // $a = getdate(strtotime($arrayEnd['start']));
      // $newend = mktime(23,59,59,$a['mon'],$a['mday'],$a['year']);
      $newend = strtotime($arrayEnd['end']);
      $rangeArray = array();
      // echo "<br> S1 : ".date('l F jS H:i:s', $start);
      // echo "<br> E1 : ".date('l F jS H:i:s', $end);
      // echo "<br> S2 : ".date('l F jS H:i:s', $newstart);
      // echo "<br> E2 : ".date('l F jS H:i:s', $newend);
      // die();
      if (($start > $newstart) && ($end < $newend)) {// G
        $rangeArray[0]['start'] = $newstart ;
        $a = getdate($start);
        $rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
        $rangeArray[0]['data'] = $eve[0];
        $rangeArray[1]['start'] = $start;
        $rangeArray[1]['end'] = $end;
        $a = getdate($end);
        $rangeArray[2]['data'] = $arrayEnd;//end($eve);
        $rangeArray[2]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
        $rangeArray[2]['end'] = $newend;
        $end = $newend ;
        // echo json_encode(array('rangeArray' => 'G'));
        // echo "<br> G";
        // die();
      }elseif (($start > $newstart) && ($end > $newend)) {// F
        $rangeArray[0]['start'] = $newstart ;
        $a = getdate($start);
        $rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
        $rangeArray[0]['data'] = $eve[0];
        $rangeArray[1]['start'] = $start;
        $rangeArray[1]['end'] = $end;
        // echo json_encode(array('rangeArray' => 'F'));
        // die();
      }elseif (($start < $newstart) && ($end < $newend)) { //E
        $rangeArray[0]['start'] = $start ;
        $rangeArray[0]['end'] = $end;
        $a = getdate($end);
        $rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
        $rangeArray[1]['end'] = $newend;
        $rangeArray[1]['data'] = $arrayEnd;//end($eve); /***************///
        // echo json_encode(array('rangeArray' => 'E'));
        // die();
      }elseif (($start > $newstart) && ($end == $newend)) { //D
        $rangeArray[0]['start'] = $newstart ;
        $a = getdate($start);
        $rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
        $rangeArray[0]['data'] =  $eve[0];
        $rangeArray[1]['start'] = $start;
        $rangeArray[1]['end'] = $end;
        // echo "<br> D";
        // die();
        // echo json_encode(array('rangeArray' => 'D'));
        // die();
      }elseif (($start == $newstart) && ($end < $newend)) { //C
        $rangeArray[0]['start'] = $start ;
        $rangeArray[0]['end'] = $end;
        $a = getdate($end);
        $rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
        $rangeArray[1]['end'] = $newend;
        $rangeArray[1]['data'] = $arrayEnd;
        // echo json_encode(array('rangeArray' => 'C'));
        // die();
      }elseif ($start == $newend) {//B
        $rangeArray[0]['start'] = $newstart ;
        $a = getdate($start);
        $rangeArray[0]['end'] = mktime(23,59,59,$a['mon'],$a['mday']-1,$a['year']);
        $rangeArray[0]['data'] = $eve[0];
        $rangeArray[1]['start'] = $start;
        $rangeArray[1]['end'] = $end;
        // echo json_encode(array('rangeArray' => 'B'));
        // die();
      }elseif ($end == $newstart) {//A
        $rangeArray[0]['start'] = $start;
        $rangeArray[0]['end'] = $end;
        $a = getdate($end);
        $rangeArray[1]['start'] = mktime(0,0,0,$a['mon'],$a['mday']+1,$a['year']);
        $rangeArray[1]['end'] = $newend;
        $rangeArray[1]['data'] = $arrayEnd;
        // echo json_encode(array('rangeArray' => 'A'));
        // die();
      }else{
        $rangeArray[0]['start'] = $start ;
        $rangeArray[0]['end'] = $end;
        // echo json_encode(array('rangeArray' => 'Default'));
        // die();
      }
      // foreach ($eve as $key => $value) {
      //  $eve[strtotime($value['start'])] = $value;
      //  unset($eve[$key]);
      // }
      // print_r($eve);
    }else{
      $rangeArray[0]['start'] = $start ;
      $rangeArray[0]['end'] = $end;
    }
    // foreach ($rangeArray as $key => $value) {
    //  // echo "<br> RStart : ".date('F jS Y H:i:s', $value['start']);
    //  // echo "<br> REnd : ".date('F jS Y H:i:s', $value['end']);
    //  $rangeArray[$key]['start'] = date('F jS Y', $value['start']);
    //  $rangeArray[$key]['end'] = date('F jS Y', $value['end']);
    //  // echo "<br> End : ".date('F jS Y', $value['end']);
    //  // if (isset($value['data']))
    //  //  print_r($value['data']);
    // }
    // echo "<pre>";
    // print_r($rangeArray);
    // die();
    // echo json_encode(array('msg' => $rangeArray));
    // die();
    return $rangeArray;
  }

 ////// Event Fetching Starts

  public function add_zero_if_needed($num=0)
  {
    // $num = 12;
    $num_padded = sprintf("%02s", $num);
    return $num_padded; // returns 04
  }

  public function days_of_month($month, $year)
  {
    return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
  }
  
  public function get_events($listing_id = '')
  {
    //CRUD EVENTS http://developer-paradize.blogspot.in/2013/06/jquery-fullcalendar-integration-with.html
    // $_POST['month'] = 5;
    // $_POST['year'] = 2014;
    // echo "<br> Start : ".date('l F jS Y', $_POST['start']);
    // echo "<br> End : ".date('l F jS Y', $_POST['end']);
    // echo "<br> Month : ".$_POST['month'];
    // echo "<br> Year : ".$_POST['year'];
    // print_r($_POST);
    // die();
    if (isset($_POST['month']) && isset($_POST['year']) && isset($_POST['property_id'])) {
        $listing_id = $_POST['property_id'];
        $property = $this->front_model->get_row('properties', array('id' => $listing_id));
        if ($property) {
          $weekDayPrice = $property->application_fee;
          $weekEndPrice = $property->weekendprice;
        }
        $month = (int) $_POST['month'];
        $month++;
        $month = $this->add_zero_if_needed($month);
        $year = (int) $_POST['year'];

        $strtotime = strtotime($year.'-'.$month.'-1');
      
        $a = getdate(strtotime('first day of '.date('F',$strtotime).' '.date('Y',$strtotime)));

        $no_of_days = $this->days_of_month($month, $year);

    }else{
      $weekDayPrice = '';
      $weekEndPrice = '';
      $month = date('m');
      $year = date('Y');
      $a = getdate(strtotime('first day of this month'));
      $date = explode("-", date('Y-m-d'));
      $no_of_days = $this->days_of_month($date[1], $date[0]);
    }
    $order['by'] = 'start';
    $order['order'] = 'ASC';
    $events = $this->front_model->get_events('pr_calender', array('property_id' => $listing_id), $order, $month ,$year);
    $new_events = array();
    if ($events) {
      foreach ($events as $v1) {
        $new_events[date('Y-m-d', strtotime($v1->start))] = $v1;
      }
    }

    $event_data = array();
    $system = 1;
    $format = ($system == 1) ? 'Y-m-d H:i:s' : 'F j, Y h:i:s' ;

    $day = 0;
    
      $day_count = 1;
      for ($i=0; $i < $no_of_days; $i++) {
        
        $beginOfDay = mktime(0,0,0,$a['mon'],$a['mday']+$day,$a['year']);
        $endOfDay = mktime(23,59,59,$a['mon'],$a['mday']+$day,$a['year']);

        //1. UNAVAILABLE : #666,
        //2.Booked : #a10, 
        //3.AVALABLE : #819f2a
          if (isset($new_events[date('Y-m-d', $beginOfDay)])) {
            $events = $new_events[date('Y-m-d', $beginOfDay)];
            $event_data[] = array(
                        'id'        => $events->id,
                        'title'       => '$'.$events->price,
                        // 'description'    => $events->description,
                        'start'       => $beginOfDay,
                        'end'         => $beginOfDay,
                        // 'backgroundColor'  => '#',
                        'color'       => $events->color,
                        'textColor'     => $events->textColor,
                        // 'borderColor'    => '#',
                        'allDay'      => true
                        );
          }else {
            if ((date('l', $beginOfDay) == 'Friday') || (date('l', $beginOfDay) == 'Saturday')){
              if ($weekEndPrice > 0)
                $price = '$'.$weekEndPrice;
              else
                $price = '$'.$weekDayPrice;
            }
            else
              $price = '$'.$weekDayPrice;

            $event_data[] = array(
                        'title'       => $price,
                        'description' => 'its available.',
                        'start'       => $beginOfDay,
                        'end'         => $beginOfDay,
                        // 'backgroundColor'  => '#',
                        'color'       => '#819f2a',
                        'textColor'     => '#fff',
                        // 'borderColor'    => '#',
                        'allDay'      => true
                        );
          }
        $day++;
      }
    echo json_encode($event_data);
  }

  ////// Event Fetching End


  public function add_into_favorites_category($pr_id)
  {
    $customer_id = get_my_id();
    if($customer_id=="")
    {
      echo "error";
      exit;
    }

    $prop_info = $this->front_model->get_row('properties',array('id'=>$pr_id));
      
     $category_array = $_POST['favorites_category'];
     foreach ($category_array as $value)
     {
      // $is_exist_data = array(
      //                 'property_id'=>$pr_id,
      //                 'customer_id'=>$customer_id,
      //                 'favorites_category_id'=>$value,
      //                 );

      // $exist = $this->front_model->get_row('favorites_property',$is_exist_data);
      // if(!empty($exist))
      //   {
      //     continue;
      //   }
        $insert = array(
                        'property_id'=>$pr_id,
                        'customer_id'=>$customer_id,
                        'favorites_category_id'=>$value,
                        'user_id'=>$prop_info->user_id,
                        );
        $this->front_model->insert('favorites_property',$insert);
     }
     echo "ok";


  }

  public function get_lat_long($location="")
  {
    if(empty($location))
    {
      return FALSE;
      exit;
    }
    $location = str_replace(" ","",$location);
    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$location&sensor=false");
    $json = json_decode($json);
    if($json->status=="OK")
    {
      $latitude_longitude = $json->results[0]->geometry->location;
      $lat_long_array['lat'] =  $latitude_longitude->lat;
      $lat_long_array['lng'] =  $latitude_longitude->lng;
    }
    else
    {
      return FALSE;
      exit;
    }
        echo json_encode($lat_long_array);
  }

  public function payza()
  {
    $data['template'] = 'properties/payza';
    $this->load->view('templates/front_template', $data);
  }

  public function return_from_payza()
  {
      $post = json_encode($_POST);
      $this->front_model->insert('ipn',array('status'=>$post));
      $this->success_all_payments($payment_type="",$transaction_id="",$unique_id="");
  }




  
  public function back()
  {
         $r = urldecode($_GET);
         var_dump($_GET);
  }

  //  public function cancel_from_payza()
  // {
  //    // print_r($ipm);
  // }


  public function confirm_all_payment()
  {
        $user = $this->session->userdata('user_info');

        if(empty($_POST)){ redirect('properties'); }
        if(empty($user)){ redirect('properties'); }

        $currency = $this->session->userdata('currency');

        $booking = array( 
        'pr_id'    => $this->input->post('pr_id'), 
        'owner_id' => $this->input->post('owner_id'), 
        'check_in' => $this->input->post('check_in'),
        'check_out'=> $this->input->post('check_out'), 
     'total_amount'=> $this->input->post('total_amount'), 
        'comment'  => $this->input->post('comment'), 
        'city'     => $this->input->post('city'), 
        'inquiry'  => $this->input->post('inquiry'),
        'time_zone'=> $this->input->post('time_zone'), 
        'contact'  => $this->input->post('contact'), 
        'payment_country' => $this->input->post('payment_country'),
        'commision'  => $this->input->post('commision'), 
        'tax' => $this->input->post('tax'),
        'unique_id'=>uniqid(),
        'customer_id'    => $user['id'],
        'customer_name'  => $user['first_name'].' '.$user['last_name'],
        'created'        => date('m/d/Y'),
        'modified'       => date('m/d/Y'),
        'booking_currency_type'   =>$currency,
        ); 

        if(isset($_POST['coupon_type']))
        { 
           $booking['coupon_type'] = $_POST['coupon_type'];
           $booking['coupon_amount'] = $_POST['coupon_amount'];
        }

        $prop_info = $this->front_model->get_row('properties',array('id'=>$_POST['pr_id']));
        if(!empty($prop_info->security_deposit))
        {
           $booking['security_deposit'] = convertCurrency($prop_info->security_deposit,$currency, $prop_info->currency_type);
        }


        $this->front_model->insert('bookings' , $booking);

        $property = $this->front_model->get_row('properties', array('id'=>$_POST['pr_id']));
       
        $user_message  = array(
                      'pr_id'      => $property->id,
                      'pr_user_id' => $property->user_id,
                      'pr_title'   => $property->title,
                      'check_in'   => $_POST['check_in'],
                      'check_out'  => $_POST['check_out'],
                      'guest'      => 0,
                      'sender_id'  => $user['id'],
                      'user_name'  => $user['first_name']." ".$user['last_name'],
                      'user_email' => $user['user_email'],
                      'message'    => $_POST['message'],
                      'status'     => 0,
                      'created'    => date('Y-m-d'),
                      'unique_id'  => $booking['unique_id'],
                      );
        $this->front_model->insert('user_message',$user_message);       


        $customer    = $this->session->userdata('user_info');
        $customer_id = $customer['id'];
        
        $data['customer'] = $this->front_model->get_row('users', array('id'=>$customer_id));
        $data['insert']    = $insert;
        $data['template'] = 'properties/confirm_booking';
        $this->load->view('templates/front_template', $data);
    } 



    public function success_all_payments($payment_type="",$transaction_id="",$unique_id="")
    {    
        $cus      = $this->session->userdata('user_info');
        $cus_id   = $cus['id'];
        $cus_name = $cus['first_name']." ".$cus['last_name'];
        $customer = $this->front_model->get_row('users', array('id'=>$cus_id));
        $book     = $this->session->userdata('event');             
        $currency = $this->session->userdata('currency');

        $prop_info = $this->front_model->get_row('properties',array('id'=>$book['pr_id']));
        
        $booking  = array(
                    'transaction_id' => $rqst['txn_id'],
                    'extra'          => json_encode($rqst),
                    'pr_id'          => $book['pr_id'],   
                    'owner_id'       => $book['owner_id'],
                    'check_in'       => $book['check_in'],
                    'check_out'      => $book['check_out'],
                    'comment'        => $book['comment'],
                    'city'           => $book['city'], 
                    'inquiry'        => $book['inquiry'], 
                    'time_zone'      => $book['time_zone'],
                    'contact'        => $book['contact'],
                    'total_amount'   => $book['total_amount'],
                    'booking_currency_type'   =>$currency,
                    'customer_id'    => $cus_id,
                    'customer_name'  => $cus_name,
                    'created'        => date('m/d/Y'),
                    'modified'       => date('m/d/Y'),
                    'commision'      => $book['commision'],
                    'tax'            => $book['tax'],

        );

        if(!empty($prop_info->security_deposit))
         {
          $booking['security_deposit'] = convertCurrency($prop_info->security_deposit,$currency, $prop_info->currency_type);
         }
         
        if(!empty($book['coupon_type']))
         {
          $booking['coupon_type'] = $book['coupon_type'];
          $booking['coupon_amount'] = $book['coupon_amount'];
         }


        $booking_id = $this->front_model->insert('bookings',$booking);

        $this->addBookingToCal($book['pr_id'],$book['check_in'],$book['check_out']);

        $host_info = $this->front_model->get_row('users',array('id'=>$book['owner_id']));
        $payments = array(
                  'transaction_id'      => $rqst['txn_id'],
                  'transaction_details' => json_encode($rqst),
                  'booking_id'          => $booking_id,
                  'property_id'         => $book['pr_id'],
                  'user_id'             => $cus_id,
                  'guest_name'          => $cus_name,
                  'host_name'           => $host_info->first_name." ".$host_info->last_name,
                  'property_name'       => $prop_info->title,
                  'payment_type'        => 'PAYPAL',
                  'is_refund'           => 0,
                  'paid_to_owner'       => 0,
                  'owner_id'            => $book['owner_id'],
                  'amount'              => $book['total_amount'],
                  'booking_currency_type'=> $currency,
                  'created'             => date('Y-m-d h:i:s'),
                );

                  if(!empty($prop_info->security_deposit))
                   {
                    $payments['security_deposit'] = convertCurrency($prop_info->security_deposit,$currency, $prop_info->currency_type);
                   }

        $payment_id = $this->front_model->insert('payments',$payments);
        
        $property = $this->front_model->get_row('properties', array('id'=>$book['pr_id']));
        $message  = array(
                'pr_id'      => $property->id,
                'pr_user_id' => $property->user_id,
                'pr_title'   => $property->title,
                'check_in'   => $book['check_in'],
                'check_out'  => $book['check_out'],
                'guest'      => 0,
                'sender_id'  => $cus_id,
                'user_name'  => $customer->first_name." ".$customer->last_name,
                'user_email' => $customer->user_email,
                'subject'    => $booking_id." (booking number)",
                'message'    => $book['message'],
                'status'     => 0,
                'created'    => date('Y-m-d')
            );
         //////////commision For Inviter Starts ///////////////
         //////////commision For Inviter Starts ///////////////
        $current_user_info = $this->session->userdata('user_info');
        $gain = $this->front_model->get_row('invitation',array('joined_user_id'=>$current_user_info['id']));
          if(!empty($gain) && $gain->booking_credit_given==0)
          {
        $this->front_model->update('invitation',array('booking_credit_given'=>1),array('joined_user_id'=>$current_user_info['id']));
          }
         //////////commision For Inviter Ends////////////////
         //////////commision For Inviter Ends////////////////

         //////////Send Booking Request Email Starts////////////////
         //////////Send Booking Request Email Starts////////////////
       
        $this->load->library('smtp_lib/smtp_email');

        $email_template = get_manage_email_info('after_host_receives_book_request');
        $website_info = get_manage_email_info('website_url_and_email');
        $website_url = $website_info->website_url;
        $from_email = $website_info->message_from_which_email;

        $check_in_date = date('d-F-Y',strtotime($book['check_in']));
        $check_out_date = date('d-F-Y',strtotime($book['check_out']));

        $subject = $email_template->subject;

        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
        $arr_update = array($host_info->first_name,$customer->first_name,$check_in_date,$check_out_date,$property->title,$website_url);
        $html =  str_replace($arr_old,$arr_update,$email_template->content);

               
        
        $to = array($host_info->user_email);
        $from = array($from_email =>$from_email);  // From email in array form 
        $status_of_email = $this->smtp_email->sendEmail($from, $to, $subject, $html);

            $insert_data = array(
                        'name'=>$host_info->first_name,
                        'email'=>$host_info->user_email,
                        'subject'=>$subject,
                        'message'=>$html,
                        'created'=>date('Y-m-d h:i:s')
                        );

            $this->front_model->insert('notifications',$insert_data);


         //////////Send Booking Request Email Ends////////////////
         //////////Send Booking Request Email Ends////////////////
 
        $this->front_model->insert('user_message',$message);       
        // $this->session->unset_userdata('event');
        $this->session->set_flashdata('success_msg','Property Has Been booked Successfully.');
        redirect('user/bookings_by_me');
    }



  




}