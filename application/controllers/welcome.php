<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'libraries/twitteroauth/config.php';
require_once APPPATH.'libraries/twitteroauth/twitteroauth.php';
class Welcome extends CI_Controller {

	/**
	 * Index Page for this controllers.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	// define('CONSUMER_KEY', 'QX0gjEScqT3nGnYH6GQ');
	// define('CONSUMER_SECRET', 'k21b1Js4ZMfNVGJig0mbuqNKMxZyR00ukKk52twWMA');
	// define('OAUTH_CALLBACK', 'http://localhost/twitteroauth-master/callback.php');

	public function __construct(){
		parent::__construct();			
		clear_cache();
		$this->load->library('twitconnect');
		// session_start();
	}

	public function index()
	{
		$resp = $this->twitconnect->check_config();
		if ($resp['status']) {
			$resp = $this->twitconnect->redirectmethod();
			if ($resp['status']) {
				redirect($resp['url']);
			}else{
				echo $resp['msg'];
				die();
			}
		}else{
			echo $resp['msg'];
			die();
		}
	}

	public function twitCallback()
	{
		$content = $this->twitconnect->callback();
		if ($content) {
			print_r($content);
		}else
			echo 'Something goes wrong.....';
	}
	public function index1()
	{
		/**
		 * @file
		 * Check if consumer token is set and if so send user to get a request token.
		 */

		/**
		 * Exit with an error message if the CONSUMER_KEY or CONSUMER_SECRET is not defined.
		 */
		if (CONSUMER_KEY === '' || CONSUMER_SECRET === '' || CONSUMER_KEY === '' || CONSUMER_SECRET === '') {
		  echo 'You need a consumer key and secret to test the sample code. Get one from <a href="https://dev.twitter.com/apps">dev.twitter.com/apps</a>';
		  exit;
		}

		/* Build an image link to start the redirect process. */
		$content = '<a href="'.base_url().'welcome/redirectmethod"><img src="'.base_url().'assets/img/third_party/lighter.png" alt="Sign in with Twitter"/></a>';





		/* Include HTML to display on the page */
		// include('html.inc');
		$data['content'] = $content;
		$this->load->view('front/twitter', $data);
	}

	public function redirectmethod()
	{
		/* Build TwitterOAuth object with client credentials. */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
		 
		/* Get temporary credentials. */
		$request_token = $connection->getRequestToken(OAUTH_CALLBACK);

		/* Save temporary credentials to session. */
		$auth = array(
                   'oauth_token'  		=> $request_token['oauth_token'],
                   'oauth_token_secret' => $request_token['oauth_token_secret'],
               );
		$token = $request_token['oauth_token'];
		$this->session->set_userdata('auth', $auth);
		// $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
		// $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
		 
		/* If last connection failed don't display authorization link. */
		switch ($connection->http_code) {
		  case 200:
		    /* Build authorize URL and redirect user to Twitter. */
		    $url = $connection->getAuthorizeURL($token);
		    redirect($url); 
		    // header('Location: ' . $url); 
		    break;
		  default:
		    /* Show notification if something went wrong. */
		    echo 'Could not connect to Twitter. Refresh the page or try again later.';
		}
	}

	public function callback()
	{
		/* If the oauth_token is old redirect to the connect page. */
		$auth = $this->session->userdata('auth');
		if (isset($_REQUEST['oauth_token']) && $auth['oauth_token'] !== $_REQUEST['oauth_token']) {
		  $auth['oauth_status'] = 'oldtoken';
		  $this->session->set_userdata('auth', $auth);
		  // header('Location: ./clearsessions.php');
		  redirect('welcome/clearsessions');
		}

		/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $auth['oauth_token'], $auth['oauth_token_secret']);

		/* Request access tokens from twitter */
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$auth['access_token'] = $access_token;

		/* Remove no longer needed request tokens */
		$auth['oauth_token'] = '';
		$auth['oauth_token_secret'] = '';

		/* If HTTP response is 200 continue otherwise send to connect page to retry */
		if (200 == $connection->http_code) {
		  /* The user has been verified and the access tokens can be saved for future use */
		  $auth['status'] = 'verified';
		  $this->session->set_userdata('auth', $auth);
		  // header('Location: ./index.php');
		  redirect('welcome/result');
		} else {
		  /* Save HTTP status for error dialog on connnect page.*/
		  // header('Location: ./clearsessions.php');
		  redirect('welcome/clearsessions');
		}
	}

	public function result()
	{
		// $this->load->view('welcome_message');
		/* If access tokens are not available redirect to connect page. */
		$auth = $this->session->userdata('auth');
		if (empty($auth['access_token']) || empty($auth['access_token']['oauth_token']) || empty($auth['access_token']['oauth_token_secret'])) {
		    // header('Location: ./clearsessions.php');
		    redirect('welcome/clearsessions');
		}
		/* Get user access tokens out of the session. */
		$access_token = $auth['access_token'];

		/* Create a TwitterOauth object with consumer/user tokens. */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

		/* If method is set change API call made. Test is called by default. */
		$data['content'] = $connection->get('account/verify_credentials');

		/* Some example calls */
		//$connection->get('users/show', array('screen_name' => 'abraham'));
		//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
		//$connection->post('statuses/destroy', array('id' => 5437877770));
		//$connection->post('friendships/create', array('id' => 9436992));
		//$connection->post('friendships/destroy', array('id' => 9436992));
		/* Include HTML to display on the page */
		// include('html.inc');
		$this->load->view('front/twitter', $data);
	}

	public function clearsessions($value='')
	{
		/**
		 * @file
		 * Clears PHP sessions and redirects to the connect page.
		 */
		 
		/* Load and clear sessions */
		session_start();
		session_destroy();
		 
		/* Redirect to page with the connect to Twitter option. */
		// header('Location: ./connect.php');
		redirect('welcome');
	}

	public function payza(){
		$this->load->view('form');	
	}

	public function response(){
		print_r($_REQUEST); die();
	}

	public function handler(){
		define("IPN_V2_HANDLER", "https://secure.payza.com/ipn2.ashx");
		define("TOKEN_IDENTIFIER", "token=");
	
	// get the token from Payza
	$token = urlencode($_POST['token']);

	//preappend the identifier string "token=" 
	$token = TOKEN_IDENTIFIER.$token;
	
	/**
	 * 
	 * Sends the URL encoded TOKEN string to the Payza's IPN handler
	 * using cURL and retrieves the response.
	 * 
	 * variable $response holds the response string from the Payza's IPN V2.
	 */
	
	$response = '';
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, IPN_V2_HANDLER);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $token);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$response = curl_exec($ch);

	curl_close($ch);
	
		if(strlen($response) > 0){
			if(urldecode($response) == "INVALID TOKEN")
			{
				//the token is not valid
			}
			else
			{
				//urldecode the received response from Payza's IPN V2
				$response = urldecode($response);
				
				//split the response string by the delimeter "&"
				$aps = explode("&", $response);
					
				//create a file to save the response information from Payza's IPN V2	
				$myFile = "./assets/IPNRes.txt";
				$fh = fopen($myFile,'a') or die("can't open the file");
				
				//define an array to put the IPN information
				$info = array();

				// foreach ($aps as $ap){
				// 	//put the IPN information into an associative array $info
				// 	$ele = explode("=", $ap);
				// 	$info[$ele[0]] = $ele[1];				
				// 	//write the information to the file IPNRes.txt					
				// }


			// if($info['ap_merchant'] == 'seller_2_faheem.test@gmail.com'){
				foreach ($aps as $ap)
				{
					//put the IPN information into an associative array $info
					$ele = explode("=", $ap);
					$info[$ele[0]] = $ele[1];
					
					//write the information to the file IPNRes.txt
					fwrite($fh, "$ele[0] \t");
					fwrite($fh, "=\t");
					fwrite($fh, "$ele[1]\r\n");
				}

				fwrite($fh, $_POST['token']."\r\n");
				
				fclose($fh);

			// }
				
				//setting information about the transaction from the IPN information array
				$receivedMerchantEmailAddress = $info['ap_merchant'];
				$transactionStatus = $info['ap_status'];
				$testModeStatus = $info['ap_test'];
				$purchaseType = $info['ap_purchasetype'];
				$totalAmountReceived = $info['ap_totalamount'];
				$feeAmount = $info['ap_feeamount'];
				$netAmount = $info['ap_netamount'];
				$transactionReferenceNumber = $info['ap_referencenumber'];
				$currency = $info['ap_currency'];
				$transactionDate = $info['ap_transactiondate'];
				$transactionType = $info['ap_transactiontype'];
				
				//setting the customer's information from the IPN information array
				$customerFirstName = $info['ap_custfirstname'];
				$customerLastName = $info['ap_custlastname'];
				$customerAddress = $info['ap_custaddress'];
				$customerCity = $info['ap_custcity'];
				$customerState = $info['ap_custstate'];
				$customerCountry = $info['ap_custcountry'];
				$customerZipCode = $info['ap_custzip'];
				$customerEmailAddress = $info['ap_custemailaddress'];
				
				//setting information about the purchased item from the IPN information array
				$myItemName = $info['ap_itemname'];
				$myItemCode = $info['ap_itemcode'];
				$myItemDescription = $info['ap_description'];
				$myItemQuantity = $info['ap_quantity'];
				$myItemAmount = $info['ap_amount'];
				
				//setting extra information about the purchased item from the IPN information array
				$additionalCharges = $info['ap_additionalcharges'];
				$shippingCharges = $info['ap_shippingcharges'];
				$taxAmount = $info['ap_taxamount'];
				$discountAmount = $info['ap_discountamount'];
				
				//setting your customs fields received from the IPN information array
				$myCustomField_1 = $info['apc_1'];
				$myCustomField_2 = $info['apc_2'];
				$myCustomField_3 = $info['apc_3'];
				$myCustomField_4 = $info['apc_4'];
				$myCustomField_5 = $info['apc_5'];
				$myCustomField_6 = $info['apc_6'];
				
			}

		}else{
		//something is wrong, no response is received from Payza
		}
	}

	public function testresponse(){
		$r = 'ap_merchant%3Downer%40mystore.com%26ap_custfirstname%3DJohn%26ap_custlastname%3DSmith%26ap_custaddress%3D5200+De+La+Savane%26ap_custcity%3DMontreal%26ap_custstate%3DQC%26ap_custcountry%3DCAN%26ap_custzip%3DH0H0H0%26ap_custemailaddress%3Djohnsmith%40email.com%26apc_1%3Dred%26apc_2%3D%26apc_3%3D%26apc_4%3D%26apc_5%3D%26apc_6%3D%26ap_test%3D0%26ap_purchasetype%3Ditem-goods%26ap_referencenumber%3D13AD5-2WD40-5UE7B%26ap_amount%3D40.00%26ap_quantity%3D1%26ap_currency%3DUSD%26ap_description%3DLorem+Ipsum%26ap_itemcode%3DSU1%26ap_itemname%3DShoes%26ap_shippingcharges%3D2.40%26ap_additionalcharges%3D0.00%26ap_taxamount%3D0.00%26ap_discountamount%3D0.00%26ap_totalamount%3D42.40%26ap_feeamount%3D1.25%26ap_netamount%3D41.15%26ap_status%3DSuccess';
		$enc = urldecode($r);
		$exp = explode('&', $enc);
		print_r($exp);

	}

	public function test(){
		########### settings #########
		$Twitter_Screen_Name        = 'GreatestQuotes'; //Twitter screen name
		$Facebook_Page_ID_Or_Name   = 'facebookIndia'; // Facebook Page ID or Name
		$Google_Page_Id             = '111122551242754991048'; // Google Page ID
		$Google_API_key             = 'AIzaSyCnabEbIBtxEWZ0oMs_A60KkcfVyTOnWcg'; //Google API key
		##############################

		$twitter_link = 'https://api.twitter.com/1.1/users/show.json?screen_name=rsarver';//.$Twitter_Screen_Name;
		$facebook_like = 'http://graph.facebook.com/'.$Facebook_Page_ID_Or_Name;
		$google_page_circle = 'https://www.googleapis.com/plus/v1/people/'.$Google_Page_Id.'?key='.$Google_API_key.'';

		// $twitter_data       = $this->get_data($twitter_link);
		$facebook_data      = $this->get_data($facebook_like);
		$google_data        = $this->get_data($google_page_circle);
		var_dump($google_data);
		$twitter_data = get_twitter_feed();
		// print_r($twitter_data[0]->user->followers_count);
		// echo 'Twitter Followers  : '. $twitter_data->followers_count .'<br />';
		echo 'Facebook Fans      : '. $facebook_data->likes.'<br />';
		// echo 'Google Page Circle : '. $google_data->plusOneCount;

		// echo '<pre>';
		//print_r($twitter_data);
		//print_r($facebook_data);
		//print_r($google_data);
		// echo '</pre>';
	}

	public function getgoogle(){
		$google_api_key = 'AIzaSyAp1hu1IRbq_XIqRi9N-ZY2sJkFE82rTyM';
		$page_id = '109700075714457488819';
		$data =$this->get_data("https://www.googleapis.com/plus/v1/people/".$page_id."?key=".$google_api_key);   
		// echo "http://www.googleapis.com/plus/v1/people/".$page_id."?key=".$google_api_key;
		// // $data = json_decode($data, true);
		echo "<pre>";
		print_r($data);
		// echo $data['plusOneCount'];
	}

	public function get_data($json_url='',$use_curl=true){
	    if($use_curl)
	    {
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_URL, $json_url);
	        $json_data = curl_exec($ch);
	        curl_close($ch);
	        return json_decode($json_data);
	    }
	    else
	    {
	        $json_data = file_get_contents($json_url);
	        return json_decode($json_data);
	    }
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */