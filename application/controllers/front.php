<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct(){		
		parent::__construct();		
		$this->load->model('front_model');
		$this->load->helper('bnb');
		get_visitors();
		$this->load->helper('language');
		 $currency = $this->session->userdata('currency');
    	 $data['row']=$this->front_model->get_row('site_details');
	    if(empty($currency))
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	    elseif($data['row']->currency_status==1)
	    {
		    $currency = $data['row']->currency;
	        $this->session->set_userdata('currency',$currency);
	    }
	}

	public function index($value='',$flag=""){
		$data['flag'] = $flag;
		$data['site_content']=$this->front_model->get_row('site_content');
		$data['template'] = 'front/front';
		$this->load->view('templates/front_template', $data);
	}


	public function switchLanguage($language = "") {
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        redirect(base_url());
    }


////////////////  set currency ///////////////////
	public function set_currency()
	{
		$currency = $this->input->post('currency');
		$this->session->unset_userdata('currency'); 
		$this->session->set_userdata('currency',$currency);
    	$this->front_model->update('site_details',array('currency_status'=>0),array('id'=>1));
		echo $currency; 
	}
/////////////////////////////////////////////////
	// public function front_template($flag=''){
	// 	$data['flag'] = $flag;
	// 	$data['template'] = 'front/front';
	// 	$this->load->view('templates/front_template', $data);
	// }

//////////////////////////////////////////////////
//////////  user registration ///////////////////
////////////////////////////////////////////////
	public function ajax_check_unique_email()
	{
		$email = $this->input->post('email');
		$res   = $this->front_model->get_row('users', array('user_email' => $email ));
        if(empty($res))
        {
        	echo "true";
        }
        else
        {
        	echo "false";
        }
	}

	
	public function register()
	{
		$user_role     = $this->input->post('user_role');
		$first_name    = $this->input->post('first_name');
		$last_name     = $this->input->post('last_name');
		$username      = $first_name." ".$last_name;
		$email         = $this->input->post('user_email');
		$password      = $this->input->post('password');
		$activation_key= sha1($email);					
		$user_data=array(
			'user_role'  => $user_role,
			'first_name' => $first_name,
			'last_name'  => $last_name,
			'user_email' => trim($email),
			'password'   => sha1(trim($password)),
			'secret_key' => $activation_key,					
			'created'    => date('Y-m-d H:i:s'),
			'last_ip'    => $this->input->ip_address()
			);
		$status   = $this->front_model->insert('users',$user_data);
		$uid = $status;
		
		if(!empty($uid))
		{
			$this->front_model->update('invitation', array('joined_user_id'=>$uid, 'is_joined'=>1), array('email'=>$email));
         /*Social Invite Starts*/
          if($this->session->userdata('social_invite'))
          {
			  $resr = $this->front_model->get_row('invitation',array('email'=>$email));
	          if(empty($resr))
	          {
	          	$session_data = $this->session->userdata('social_invite');
			    $insert_data = array(
			    	                 'user_id'=>$session_data['inviter_id'],
			    	                 'joined_user_id'=>$uid, 
			    	                 'is_joined'=>1,
			    	                 'email'=>$email,
			    	                 'created'=>date('Y-m-d h:i:s')
			    	                 );
			    $this->front_model->insert('invitation',$insert_data);
	            $this->session->unset_userdata('social_invite');
	          }
          }
            /*Social Invite Ends*/
		}
		
		$this->send_registration_email($username, $email, $password, $activation_key);
		if($status===FALSE)
		{ 
			echo "<span style='color:red; font-size:16px;'>ERROR : Registeration failed.</span>"; 
		}
		else
		{
			echo "<span style='color:green; font-size:16px;'>registered successfully, Please check Your email..!</span>";
		}
	}

	public function activation($activation_key='')
	{
		$email_res = $this->front_model->get_row('users', array('secret_key' => $activation_key ));
		if($email_res)
		{
			$this->front_model->update('users', array('user_status'=>1,'secret_key'=>''), array('secret_key' => $activation_key ) );
            $email     = $email_res->user_email;   
            $password  = $email_res->password;   
            $user_role = $email_res->user_role;   
			$login_res = $this->front_model->user_login($email, $password, $user_role,1);
			if($login_res == "valid" && $user_role == 3){ redirect('user/dashboard'); }
			if($login_res == "valid" && $user_role == 4){ redirect('customer/dashboard'); }
		}	
		$this->session->set_flashdata('error_msg', 'invalid link');
		redirect('front');
	}	

	public function send_registration_email($username='', $email='', $password='', $activation_key='')
	{
		$this->load->library('smtp_lib/smtp_email');

        $email_template = get_manage_email_info('after_registiung_with_us');
        $website_info = get_manage_email_info('website_url_and_email');
        $website_url = $website_info->website_url;
        $from_email = $website_info->message_from_which_email;
       
        $link = '<a href="'.base_url().'front/activation/'.$activation_key.'" > '.base_url().'front/activation/'.$activation_key.'</a>';
        $username = $username; 

        $arr_old = array('&lt;%username%&gt;','&lt;%link%&gt;','&lt;%websiteurl%&gt;');
        $arr_update = array($username,$link,$website_url);
     
        $html =  str_replace($arr_old,$arr_update,$email_template->content);
		$subject = $email_template->subject;	// Subject for email
		
		$to = $email;
        $from = array($from_email =>$from_email);  // From email in array form 

		$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);
		if($is_fail){
		echo "ERROR :";
		print_r($is_fail);
		}
	}

	public function template_for_resgistration($username, $email, $password, $activation_key){
		$message = '';
		$message .= '<html>
						<body>
						<h3>Dear '.$username.',</h3>
						<p>Thank you for joining the <a href="'.base_url().'">bnbclone.com</a> community.</p>
						<p>Please click on the following link to activate your account.</p>
						<p> <a href="'.base_url().'front/activation/'.$activation_key.'" > '.base_url().'front/activation/'.$activation_key.'</a></p>
						<p>&nbsp;</p>
						<p>Here\'s your login details for your records (just keep it safe!):</p>
						<p>User email: <strong>'.$email.'</strong></p>						
						<p>Do you have any questions about your account, please contact us at:   
						<a href="'.base_url().'">help@bnbclone.com</a>
						</p>
						<p>&nbsp;</p>
						<p>-</p>
						<p>Regards,</p>
						<p>BnbClone</p>
						<p><a href="'.base_url().'">www.bnbclone.com</a></p>';		
		$message .=	'</body></html>';

		return $message;
	}

////////////////////////////////////////////////
/////////////// user login  ///////////////////
//////////////////////////////////////////////	
    public function user_login()
    {   
	  	$user_role = $this->input->post('user_role');
	  	$email     = $this->input->post('login_email');
	  	$password  = $this->input->post('login_password');
	  	$res = $this->front_model->user_login($email, $password, $user_role);
		echo $res;
	}

	
	public function forget_password()
	{
		if($this->input->post('email'))
			{
				$email=$this->input->post('email'); // user email
				$array = array('user_email' => $email);
				$user_info = $this->front_model->get_row('users',$array);
                if(empty($user_info))
                {
					$this->session->set_flashdata('error_msg','Invalid email, Please enter registered email');
					redirect('front/forget_password');
                }
                ///////// insert secret key in database  //////////
                $secret_key = sha1($email);
                $data = array('secret_key' => $secret_key);
                $id_array = array('user_email' => $email);
                $this->front_model->update('users', $data, $id_array);
                /////////////   send mail to user  ///////////////
				$this->load->library('smtp_lib/smtp_email');
              
              /*Starts*/
		       
				$username = $user_info->first_name.' '.$user_info->last_name;
		        
		        $email_template = get_manage_email_info('reset_password_1');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $subject = $email_template->subject;
                $link = '<a href="'.base_url().'front/forget_password_response/'.$secret_key.'" ><font color="grey"><br><br>Click here to reset your password</font></a>';

		        $arr_old = array('&lt;%username%&gt;','&lt;%websiturl%&gt;','&lt;%link%&gt;');
		        $arr_update = array($username,$website_url,$link);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		               
		        
		        $to = array($user_info->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

              /*Ensd*/


				$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);
						if($is_fail)
						{
							echo "ERROR :";
							print_r($is_fail);
						}
				$this->session->set_flashdata('msg_success','Please check your email to access BnbClone account');
				redirect('front/forget_password');
			}
		$data['template'] = 'front/forget_password';
		$this->load->view('templates/front_template', $data);
	}

	public function forget_password_response($secret_key)
	{
		$array = array('secret_key' => $secret_key);
		$user_info = $this->front_model->get_row('users',$array);
		if(empty($user_info))
		{
			$this->session->set_flashdata('error_msg','Email link has been expired,please enter email.');
			redirect('front/forget_password');
		}	
        if($this->input->post('password'))
        {
        	$pwd_temp = $this->input->post('password');
            $pwd = sha1($pwd_temp);
            $id_array = array('secret_key' => $secret_key);
            $data = array('password' => $pwd, 'secret_key' =>'');
            $this->front_model->update('users', $data, $id_array);
			$this->session->set_flashdata('msg_success','Password has been reset successfully, please login..!');
            redirect('front/index/0/1');
        }
        $data['user'] = $user_info; 
		$data['template'] = 'front/reset_forget_password';
		$this->load->view('templates/front_template', $data);
	}
    
	public function template_for_forget_password($username, $email, $secret_key){
		$message = '';
		$message .= '<html>
		                <p style="text-align:center"><img src="www.bnbclone.com/BnbCloneLogo3.png" /></p>
						<body style="-webkit-box-shadow: 0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow: 0 0 0 3px rgba(0,0,0,0.025) !important;-webkit-border-radius: 6px !important;border-radius: 6px !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;-webkit-border-radius: 6px !important;border-radius: 6px !important;width: 75%;margin: 0 auto;-webkit-box-shadow: 0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow: 0 0 0 3px rgba(0,0,0,0.025) !important;">
						<h3 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #7797b4;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;background:#7797b4;border-radius: 6px 6px 0px 0;">Password Reset Instructions</h3>
						<p style="color: #737373;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;padding: 10px 20px;">Someone requested that the password be reset for the following account:<br><br>Email: <a style="text-decoration:none">'.$email.'</a>.</p>
						<p style="color: #737373;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;padding: 0 20px;">To reset your password, visit the following address: <a href="'.base_url().'front/forget_password_response/'.$secret_key.'" ><font color="grey"><br><br>Click here to reset your password</font></a></p>
						<p>&nbsp;</p>
						<p style="color: #737373;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;padding: 0 20px;">Do you have any questions about your account, please contact us at:   
						<a href="'.base_url().'">http://bnbclone.zendesk.com</a>
						</p>
						<p>&nbsp;</p>
						<p style="text-align:center;color:#99b1c7">Kind Regards,</p>
						<p style="text-align:center;color:#99b1c7;margin-bottom:0px;">BnbClone Team</p>
						<p style="text-align:center;margin:0;"><a style="color:#99b1c7;" href="'.base_url().'">www.bnbclone.com</a></p>';		
		$message .=	'</body></html>';

		return $message;
	}

	/////////////////////////////////////////////////////////////
////////////////////  listing property  ////////////////////
///////////////////////////////////////////////////////////
   public function get_neighbourhoot_of_city()
    {
    	$city = $this->input->post('city');
    	$nb   = get_neighbourhoot_of_city($city);
    	$res  = "";
    	if(!empty($nb))
    	{
    		$res .= "<option value=''>Select neighbour</option>";
	    	foreach($nb as $row)
	    	{
	    		$res .= "<option value='".$row->id."'>".$row->title."</option>";
	    	}
	    }
	    else
	    {
    		$res = "<option value=''>No Neighbourhood</option>";
	    }	
    	echo $res;
    }

	// public function listing_property_first()
	// {
	// 	if(!$this->session->userdata('user_info'))
	// 	{
	// 		redirect('front');
	// 	}
	// 	$user = $this->session->userdata('user_info');
	// 	$user_id = $user['id'];

	// 	$this->form_validation->set_rules('property_title', 'Property Title', 'trim|required');
	// 	$this->form_validation->set_rules('property_descrip', 'Property Description', 'trim|required');
	// 	$this->form_validation->set_rules('property_type', 'Property Type', 'trim|required');
	// 	$this->form_validation->set_rules('bed_type', 'Bed Type', 'trim|required');
	// 	$this->form_validation->set_rules('address', 'Address ', 'trim|required');
	// 	$this->form_validation->set_rules('city', 'City', 'trim|required');
	// 	$this->form_validation->set_rules('state', 'State', 'trim|required');
	// 	$this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('country', 'Country', 'trim|required');
	// 	$this->form_validation->set_rules('bedroom', 'Bedroom', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('bathroom', 'Bathroom', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('accommodates', 'Accommodate', 'trim|required');
	// 	$this->form_validation->set_rules('size', 'Property Size', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('room_type', 'Room Type', 'trim|required');
	// 	$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
	// 	if($this->form_validation->run() == TRUE){
	// 		$array= array(
	// 						  'property_title'  => $this->input->post('property_title'),
	// 						  'property_descrip'   => $this->input->post('property_descrip'),
	// 						  'property_type'=> $this->input->post('property_type'),
	// 						  'bed_type'     => $this->input->post('bed_type'),
	// 						  'address'      => $this->input->post('address'),
	// 						  'city'         => $this->input->post('city'),
	// 						  'state'        => $this->input->post('state'),
	// 						  'zipcode'      => $this->input->post('zipcode'),
	// 						  'country'      => $this->input->post('country'),
	// 						  'bed'	         => $this->input->post('bedroom'),
	// 						  'bath'         => $this->input->post('bathroom'),
	// 						  'size'         => $this->input->post('size'),
	// 						  'accommodate'  => $this->input->post('accommodates'),
	// 						  'room_type'    => $this->input->post('room_type'),
	// 					  	  'latitude'     => $this->input->post('latitude'),
	// 					  	  'longitude'    => $this->input->post('longitude'),
	// 					  	  'created'      => date('Y-m-d H:i:s'),
	// 					  	  'user_id'      => $user_id
	// 					);
			
	// 		if($this->input->post('neighbourhood'))
	// 		{
	// 			$neighbour_id = $this->input->post('neighbourhood');
	// 			$this->session->set_userdata('neighbour_id',$neighbour_id);
	// 		}
			
	// 		$this->session->set_userdata('pr_info',$array);
	// 		redirect('front/listing_property_second');
	// 	}
	// 	$data['template'] = 'front/listing_property_1';
 //        $this->load->view('templates/front_template', $data);	
	// }

	public function get_lat_long($city="")
	{
      if($city=="")
      	return false;

		$location = str_replace(" ","",$city);
		$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$location&sensor=false");
		$json = json_decode($json);
		$latitude_longitude = $json->results[0]->geometry->location;
		$lat_long_array['lat'] =  $latitude_longitude->lat;
		$lat_long_array['lng'] =  $latitude_longitude->lng;
        return $lat_long_array;
	}

	public function listing_property_first()
	{
		if(!$this->session->userdata('user_info'))
		{
			redirect('front');
		}
		$user = $this->session->userdata('user_info');
		$user_id = $user['id'];

		if($_POST)
		{

			$full_address = $_POST['address'].'+'.$_POST['city'].'+'.$_POST['state'].'+'.$_POST['country'];
		    $lat_long_array = get_lat_long($full_address);


			/*Properties Table data starts*/
			$properties_table= array(
								  'title'           => $this->input->post('title'),
								  'description'     => $this->input->post('description'),
								  'application_fee' => $this->input->post('price_amount'),
								  'currency_type'   => $this->input->post('price_type'),
							  	  'latitude'        => $lat_long_array['lat'],
							  	  'longitude'       => $lat_long_array['lng'],
							  	  'created'         => date('Y-m-d H:i:s'),
							  	  'user_id'         => $user_id
             						);

			// if($_POST['sublet_from_date'])
			// {
			// 	$properties_table['sublet_status'] = 1;
			// 	$properties_table['sublet_from_date'] = $_POST['sublet_from_date'];
			// 	$properties_table['sublet_to_date']   = $_POST['sublet_to_date']; 
			// }
			
			$property_id = $this->front_model->insert('properties',$properties_table);
			/*Properties Table data Ends*/
			
			/*Pr_info Table data starts*/
			$pr_info_table = array(
								  'pr_id'        =>$property_id,
								  'property_type_id'=> $this->input->post('property_type'),
								  'room_type'    => $this->input->post('room_type'),
								  'bed'	         => $this->input->post('bedrooms'),
					        'unit_street_address'=> $this->input->post('address'),
								  'city'         => $this->input->post('city'),
								  'state'        => $this->input->post('state'),
								  'zip'      => $this->input->post('zipcode'),
								  'country'      => $this->input->post('country'),
								  'accommodates'  => $this->input->post('accomodates'),
                 			
                 				);
			$this->front_model->insert('pr_info',$pr_info_table);
			/*Pr_info Table data Ends*/


			/*Pr_nb_relation Table data Starts*/
			if($this->input->post('neighbourhood'))
			{
				$neighbour_id = $this->input->post('neighbourhood');
				$this->session->set_userdata('neighbour_id',$neighbour_id);
			}

			if($this->session->userdata('neighbour_id'))
			{
				$parent       = array('pr_nb_id'=>$property_id, 'parent_id'=>0 );
				$parent_id    = $this->front_model->insert('pr_nb_relation',$parent);
			    $neighbour_id = $this->session->userdata('neighbour_id');
				foreach($neighbour_id as $nb_id)
				{

					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
					$status = $this->front_model->insert('pr_nb_relation',$nb);
				}
				$this->session->unset_userdata('neighbour_id');
			}	
			/*Pr_nb_relation Table data Ends*/

			$this->session->set_flashdata('success_msg',"Property has been added successfully.");
			redirect('user/properties');
		}
		$data['template'] = 'front/listing_property_1';
        $this->load->view('templates/front_template', $data);	
	}

	public function listing_property_second()
	{
		$test = $this->session->userdata('pr_info');
        if(!$test) redirect('front/listing_property_first');

        $user = $this->session->userdata('user_info');
		$user_id = $user['id'];

		$this->form_validation->set_rules('rent', 'Property Rent', 'trim|required|numeric');
		$this->form_validation->set_rules('room_allotment', 'Room Allotment', 'trim|required');
		$this->form_validation->set_rules('deposit', 'Deposit', 'trim|required|numeric');
		$this->form_validation->set_rules('appliction_fee', 'Appliction Fee', 'trim|required|numeric');
		// $this->form_validation->set_rules('contact_name', 'User Name ', 'trim|required');
		// $this->form_validation->set_rules('user_email', 'User Email', 'trim|required|valid_email');
		// $this->form_validation->set_rules('website', 'Website URL', 'trim|required');
		// $this->form_validation->set_rules('phone', 'User Contact', 'trim|required');
		$this->form_validation->set_rules('collection_category', 'Collection Category', 'trim|required');
		$this->form_validation->set_error_delimiters('<div style="color:#E05284;">','</div>');	
		if($this->form_validation->run() == TRUE)
		{
			$image = $this->do_core_upload('userfile','./assets/uploads/property/');

			$array= array(
						  'rent'  			=> $this->input->post('rent'),
						  'room_allotment'  => $this->input->post('room_allotment'),
						  'deposit'     	=> $this->input->post('deposit'),
						  'appliction_fee'  => $this->input->post('appliction_fee'),
						  // 'contact_name'    => $this->input->post('contact_name'),
						  // 'user_email'      => $this->input->post('user_email'),
						  // 'website'         => $this->input->post('website'),
						  'featured_image'  => $image,
						 'pr_collect_cat_id'=> $this->input->post('collection_category'),
						  'phone'           => $this->input->post('phone')
						);
			$arr=$this->session->userdata('pr_info');
			$properties= array( 
				'title'          =>$arr['property_title'],
				'description'    => $arr['property_descrip'],
				'room_allotment' => $array['room_allotment'],
				'deposit_amount' => $array['deposit'],
				'application_fee'=> $array['appliction_fee'],
				'slug'           =>str_replace(' ','-', $arr['property_title']),
				'created'        =>$arr['created'],
				'user_id'        =>$user_id,
				'featured_image' =>$array['featured_image'],
				'latitude'       =>$arr['latitude'],
				'longitude'      =>$arr['longitude']
				);
			$property_id=$this->front_model->insert('properties',$properties);
			
			if($this->session->userdata('neighbour_id'))
			{
				$parent       = array('pr_nb_id'=>$property_id, 'parent_id'=>0 );
				$parent_id    = $this->front_model->insert('pr_nb_relation',$parent);
				$neighbour_id = $this->session->userdata('neighbour_id');
				foreach($neighbour_id as $nb_id)
				{
					$nb = array('pr_nb_id'=>$nb_id, 'parent_id'=>$parent_id );
					$this->front_model->insert('pr_nb_relation',$nb);
				}

			}	



			$properties_info=array( 
									'pr_id'              => $property_id,
									'unit_street_address'=> $arr['address'],
									'property_type_id'   => $arr['property_type'],
									'city'               => $arr['city'],
									'rent'               => $array['rent'],
									'state'=> $arr['state'],
									'country'=> $arr['country'],
									'bed_type'=>$arr['bed_type'],
									'bed'=>$arr['bed'],
									'bath'=>$arr['bath'],
									'accommodates'=>$arr['accommodate'],
									'square_feet'=>$arr['size'],
									'room_type'=>$arr['room_type'],
									'pr_collect_cat_id'=>$array['pr_collect_cat_id'],
									'zip'=>$arr['zipcode'],
								);
			 $u_info = $this->session->userdata('user_info');
             $user_info = $this->front_model->get_row('users',array('id'=>$u_info['id']));

								$properties_contact_info=array( 
									'pr_id'=>$property_id,
									'contact_name'=> $user_info->first_name." ".$user_info->last_name,
									'phone'=> $user_info->phone,
  									'contact_email'=> $user_info->user_email,
									// 'website'=> $array['website'],
									
								);
								// $properties_contact_info=array( 
								// 	'pr_id'=>$property_id,
								// 	'contact_name'=> $array['contact_name'],
								// 	'phone'=> $array['phone'],
								// 	'contact_email'=> $array['user_email'],
								// 	'website'=> $array['website'],
									
								// );
			$this->front_model->insert('pr_info',$properties_info);
			$status = $this->front_model->insert('pr_contact_info',$properties_contact_info);
			if(isset($_POST['amenities'])){
				for ($i=0; $i <count($_POST['amenities']) ; $i++) { 
					if(!empty($_POST['amenities'][$i])){
						$pr_amenities=array(
						'pr_id'=>$property_id,
						'pr_amenity_id'=>trim($_POST['amenities'][$i])
						);
						$this->front_model->insert('pr_amenities',$pr_amenities);	
					}
				}
			}
			if($status){
	           //////////commision For Inviter Starts ///////////////
	           //////////commision For Inviter Starts ///////////////
				$current_user_info = $this->session->userdata('user_info');
				$gain = $this->user_model->get_row('invitation',array('joined_user_id'=>$current_user_info['id']));
	            if(!empty($gain) && $gain->property_credit_given==0)
	            {
				    $this->user_model->update('invitation',array('property_credit_given'=>1),array('joined_user_id'=>$current_user_info['id']));
	            }
	           //////////commision For Inviter Ends////////////////
	           //////////commision For Inviter Ends////////////////
				$this->session->unset_userdata('pr_info');
				$this->session->set_flashdata('success_msg',"Property has been added successfully.");
				redirect('user/properties');
			}
		}
        $data['user'] = $this->session->userdata('user_info');
		$data['template'] = 'front/listing_property_2';
        $this->load->view('templates/front_template', $data);
	}

	public function do_core_upload($filename2='user_file' , $upload_path='./assets/uploads/custom_uploads/', $path_of_thumb='')
	{
		$allowed =  array('gif','png','jpg','jpeg');
		$filename = $_FILES[$filename2]['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ){
			return FALSE;
		}
		else{
			
			if ($_FILES[$filename2]["error"] > 0){
	 			return FALSE; 
	 		}
			else{
			 $name = uniqid();
			 if(move_uploaded_file($_FILES[$filename2]['tmp_name'],$upload_path.$name.'.'.$ext))
			 return $name.'.'.$ext;
			 else
			 return FALSE;
			}
		} 
	}

	public function listings($value=''){
		$data['template'] = 'front/listings';
		$this->load->view('templates/front_template', $data);
	}

	public function open_id_login_process($id=""){
		require_once APPPATH.'libraries/Googleapi/lightopenid.php';
         $credentials_of = $id;         
        if($credentials_of=="google"){			
			$openid = new LightOpenID(base_url());			
			$openid->identity = 'https://www.google.com/accounts/o8/id';
			$openid->required = array(
									'namePerson/first',
									'namePerson/last',
									'contact/email',
									);

			$openid->returnUrl = base_url().'open_id_login_controller/google_sign_up';
            redirect($openid->authUrl());
        }

		else if($credentials_of=="yahoo"){
			$openid = new LightOpenID(base_url());
			$openid->identity = 'https://me.yahoo.com';
			$openid->required = array(
									'namePerson',
									'contact/email',
									);

			$openid->returnUrl = ''.base_url().'open_id_login_controller/yahoo_sign_up';
		    redirect($openid->authUrl());
		}

		elseif($credentials_of=="msn"){
			$keys = get_oauth_keys('hotmail');
			$client_id = $keys->app_id;
			$client_secret = $keys->app_secret_key;
			$redirect_uri = base_url().'open_id_login_controller/msn';



			$urls_ = 'https://login.live.com/oauth20_authorize.srf?client_id='.$client_id.'&scope=wl.signin%20wl.basic%20wl.emails%20wl.contacts_emails&response_type=code&redirect_uri='.$redirect_uri;
	        redirect($urls_);
		}

		elseif($credentials_of=="linkedin"){
		    ## echo "Authorization URL: " . $linkedin->generateAuthorizeUrl() . "\n\n";
			require_once APPPATH.'libraries/linkedin/config.php';
			require_once APPPATH.'libraries/linkedin/linkedinoAuth.php';
		    # First step is to initialize with your consumer key and secret. We'll use an out-of-band oauth_callback
		    $linkedin = new linkedinoAuth($config['linkedin_access'], $config['linkedin_secret'], $config['base_url'] . 'open_id_login_controller/linkedin' );

		    # Now we retrieve a request token. It will be set as $linkedin->request_token
		    $linkedin->getRequestToken();
		    $this->session->set_userdata('request_token',serialize($linkedin->request_token));
		  
		    # With a request token in hand, we can generate an authorization URL, which we'll direct the user to
		    ## echo "Authorization URL: " . $linkedin->generateAuthorizeUrl() . "\n\n";
		    redirect($linkedin->generateAuthorizeUrl());
		}
    } 


    public function ajax_subscribe(){
    	if($_POST){
            $config = array(
                    'apikey' => 'f4c7b6fc0670a14c4c1b49d34c7cd996-us3' ,     // Insert your api key
                    'secure' => FALSE   // Optional (defaults to FALSE)
                );
                $list_id = '40e5c36355';                
                $email = $this->input->post('email');
                $this->load->library('MCAPI', $config, 'mail_chimp');
                $dd = $this->mail_chimp->listSubscribe($list_id, $email);             
                echo $dd;
            //     $data = array(
            //         'email' => $this->input->post('pemail'),
            //         'created' => date('Y-m-d h:i:s')
            //     );

            // $exist = $this->check_email_exist($email);
            // if(!$exist){
            //     $this->home_model->insert('newsletter', $data);                
            //     echo "1";
            // }else{
            //     echo "exist";
            // }            
         }
    }

    /*FAQs Task starts From here*/

    public function get_faqs_categories()
    {
    $category_id = $_POST['category_id'];
    $result['category'] = $this->front_model->get_pagination_where('faqs',10,0,array('faq_cat_id'=>$category_id));
    $result['category_id'] = $category_id;
    	   $res ="";
	    if(!empty($result['category']))
	    {
		    $res = $this->load->view('front/ajax_faqs',$result,TRUE);
		    echo $res;
	    }
	    else
	    {
           return false;              
	    }
    }

    public function help()
    {
    	if($_POST)
    	{
        $data['search_result'] = $this->front_model->get_faq_search_result($_POST);
    	$data['flag']="valid";
    	}
    	else
    	{
    	$data['flag']="";
    	$data['search_result']="";
    	}
    	$row = $this->front_model->get_row('faqs_category');
        $data['trigger_id'] = $row->id;
    	$data['faqs'] = $this->front_model->get_row('faqs');
		$data['template'] = 'front/faqs';
		$this->load->view('templates/front_template', $data);
    }

        /*FAQs Task Ends From here*/


	public function get_email($email="",$subject="",$html="")
	{
		$this->load->library('smtp_lib/smtp_email');
		$from = array('no-reply@bnbclone.com' =>'bnbclone.com');	// From email in array form
        $subject = "subject";
		$html = "this is get_email";
		$email = "zakir@laeffect.com";
		$to = $email;
		$is_fail = $this->smtp_email->sendEmail($from,$to,$subject,$html);
		if($is_fail){
		return FALSE;
		}
		else{
			return TRUE;
		}
	}


	public function check()
	{
        

        // echo $email_template->content;
        // echo $pos = strpos($email_template->content,'<%username%>');
        // echo $pos_2 = strpos($email_template->content,'<%link%>');
        // echo substr_replace($email_template->content,'dfdfdfdffdf', $pos_2);


	}

	public function checkcron()
	{
		$this->load->library('smtp_lib/smtp_email');
       
        $html =  'this is checkcron';
		$subject = 'subject';
		
		$to = 'zakir@laeffect.com';
        $from = array('no-reply@bnb.com' =>'no-reply@bnb.com');  // From email in array form 

		$is_fail = $this->smtp_email->sendEmail($from, $to, $subject, $html);
		if($is_fail){
		echo "ERROR :";
		print_r($is_fail);
		}
	}


	public function cronjob_for_all_email()
	{
        $this->email_booking_cancel();
        $this->guest_checkin();
        $this->prop_profile_incomplete_after_3_days();
        $this->user_profile_incomplete_after_3_days();
        $this->review_3_days_after_checkout();
	}
   

	public function email_booking_cancel()
	{
		$result = $this->front_model->get_result('bookings',array('status'=>0));

		if(!empty($result)){
			foreach ($result as $row)
			{

			$cur_date_time = strtotime(date('Y-m-d'));
			$created_time = strtotime($row->created);
			$difference = $cur_date_time-$created_time;
			$days = ($difference/86400)+1;

				if($days >= 2)
				{
				 $this->front_model->update('bookings', array('status'=> 2), array('id'=>$row->id));
                 $this->front_model->make_event_available($row->check_in, $row->check_out,$row->property_id);

		 		/* Email To host Satrts*/
                $this->load->library('smtp_lib/smtp_email');

				$data['booking_info']  = $this->front_model->get_row('bookings',array('id'=>$row->id));
				$data['host_info']     = get_user_info($data['booking_info']->owner_id);
				$data['guest_info']    = get_user_info($data['booking_info']->customer_id);
			    $data['property_info'] = get_property_detail($data['booking_info']->pr_id);

		        $email_template = get_manage_email_info('after_booking_hasbeen_cancelled_send_to_host');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
		        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

		        $subject = $email_template->subject;

		        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		        $data['host_info']->user_email;   
		        
		        $to = array($data['host_info']->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				$s = $this->smtp_email->sendEmail($from, $to, $subject, $html);
		        $insert_data = array(
						        	  'name'=>$data['host_info']->first_name,
						        	  'email'=>$data['host_info']->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->front_model->insert('notifications',$insert_data);

		/*Email To host Ends*/

		/* Email To Guest Satrts*/

		        $email_template = get_manage_email_info('after_booking_hasbeen_cancelled_send_to_guest');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
		        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

		        $subject = $email_template->subject;

		        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);

		               
		        
		        $to = array($data['guest_info']->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				 $this->smtp_email->sendEmail($from, $to, $subject, $html);
		       
		        $insert_data = array(
						        	  'name'=>$data['guest_info']->first_name,
						        	  'email'=>$data['guest_info']->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );
		        $this->front_model->insert('notifications',$insert_data);

		/*Email To Guest Ends*/

 				}
			}
		}
		else{
		return FALSE;
		}

	}


	public function prop_profile_incomplete_after_3_days()
	{
		$result = $this->front_model->get_result('properties',array('status'=>0));
         
		if(!empty($result)){
			foreach ($result as $row)
			{
			$cur_date_time = strtotime(date('Y-m-d'));
			$created_time = strtotime(date($row->created));
			$difference = $cur_date_time-$created_time;
			$days = ($difference/86400)+1;
			$missing_info_count = manage_listing_steps_count($row->id);

				if($days == 3 && !empty($missing_info_count))
				{
                   $missing_info = "<ol>";
                   
                   if(empty($row->update_calender))
                   {
                      $missing_info .= "<li>Calender is not updated yet</li>";
                   }

                   if(empty($row->featured_image))
                   {
                      $missing_info .= "<li>You have not uploaded any photos yet</li>";
                   }

                   if(str_word_count($row->featured_image) < 15)
                   {
                      $missing_info .= "<li>You property description is too short(should be greater then 15 words)</li>";
                   }

                   $missing_info .= "</ol>";


		 		/* Email To host Satrts*/
                $this->load->library('smtp_lib/smtp_email');
		        $email_template = get_manage_email_info('_3_days_after_property_profile_incomplete');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $subject = $email_template->subject;

		        $user_info = get_user_info($row->user_id);

		        $arr_old = array('&lt;%username%&gt;','&lt;%missinginfolist%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($user_info->first_name,$missing_info,$row->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);
		           
		        
		        $to = array($user_info->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				$s = $this->smtp_email->sendEmail($from, $to, $subject, $html);
		        $insert_data = array(
						        	  'name'=>$user_info->first_name,
						        	  'email'=>$user_info->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->front_model->insert('notifications',$insert_data);

     		/*Email To host Ends*/
 				}
			}
		}
		else{
		return FALSE;
		}

	}


	public function review_3_days_after_checkout()
	{
		$result = $this->front_model->get_result('bookings',array('status'=>1));
         
		if(!empty($result)){
			foreach ($result as $row)
			{
			$cur_date_time = strtotime(date('Y-m-d'));
			$created_time = strtotime(date($row->check_out));
			$difference = $cur_date_time-$created_time;
			$days = ($difference/86400)+1;

				if($days == 3)
				{
		 		/* Email To host Satrts*/
                $this->load->library('smtp_lib/smtp_email');
		        $email_template = get_manage_email_info('review_3_days_after_checkout');
		        $website_info = get_manage_email_info('website_url_and_email');
		        $website_url = $website_info->website_url;
		        $from_email = $website_info->message_from_which_email;

		        $subject = $email_template->subject;

		        $user_info = get_user_info($row->customer_id);
		        $pro_info = get_property_detail($row->pr_id);

		        $check_in_date = date('d-F-Y',strtotime($row->check_in));
		        $check_out_date = date('d-F-Y',strtotime($row->check_out));

		        $arr_old = array('&lt;%guestname%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
		        $arr_update = array($user_info->first_name,$check_in_date,$check_out_date,$pro_info->title,$website_url);
		        $html =  str_replace($arr_old,$arr_update,$email_template->content);


		        $to = array($user_info->user_email);
		        $from = array($from_email =>$from_email);  // From email in array form 

				$s = $this->smtp_email->sendEmail($from, $to, $subject, $html);
				$superadmin_info = $this->front_model->get_row('users',array('user_role'=>1));

		        $insert_data = array(
						        	  'name'=>$superadmin_info->first_name,
						        	  'email'=>$superadmin_info->user_email,
						        	  'subject'=>$subject,
						        	  'message'=>$html,
						        	  'created'=>date('Y-m-d h:i:s')
						        	  );

		        $this->front_model->insert('notifications',$insert_data);
     		     /*Email To host Ends*/
 			   }
			}
		}
		else{
		return FALSE;
		}

	}


	public function user_profile_incomplete_after_3_days()
	{
		$result = $this->front_model->get_result('users',array('user_role'=>3));
         
		if(!empty($result)){
			foreach ($result as $row)
			{
				$cur_date_time = strtotime(date('Y-m-d'));
				$created_time = strtotime(date($row->created));
				$difference = $cur_date_time-$created_time;
				$days = ($difference/86400)+1;

				if($days == 3)
				{
					if(empty($row->image) || empty($row->phone) || empty($row->paypal_email) || empty($row->address))
	                {
							$missing_info = "<ol>";

							if(empty($row->image))
							{
								$missing_info .= "<li>You don't have any profile pictures yet.</li>";
							}

							if(empty($row->phone))
							{
								$missing_info .= "<li>You don't have contact info.</li>";
							}

							if(empty($row->paypal_email))
							{
								$missing_info .= "<li>You don't have Paypal Email. </li>";
							}

							if(empty($row->address))
							{
								$missing_info .= "<li>You have not provide your address yet.</li>";
							}

							$missing_info .= "</ol>";

				 		/* Email To User Satrts*/
		                $this->load->library('smtp_lib/smtp_email');
				        $email_template = get_manage_email_info('your_profile_is_incomplete');
				        $website_info = get_manage_email_info('website_url_and_email');
				        $website_url = $website_info->website_url;
				        $from_email = $website_info->message_from_which_email;

				        $subject = $email_template->subject;

				        $user_info = get_user_info($row->id);

				        $arr_old = array('&lt;%username%&gt;','&lt;%missinginfolist%&gt;','&lt;%websiteurl%&gt;');
				        $arr_update = array($user_info->first_name,$missing_info,$website_url);
				        $html =  str_replace($arr_old,$arr_update,$email_template->content);
				           
				        
				        $to = array($user_info->user_email);
				        $from = array($from_email =>$from_email);  // From email in array form 

						$s = $this->smtp_email->sendEmail($from, $to, $subject, $html);
				        $insert_data = array(
								        	  'name'=>$user_info->first_name,
								        	  'email'=>$user_info->user_email,
								        	  'subject'=>$subject,
								        	  'message'=>$html,
								        	  'created'=>date('Y-m-d h:i:s')
								        	  );

				        $this->front_model->insert('notifications',$insert_data);

		     		        /*Email To User Ends*/
     		        }
 				}
			}
		}
		else{
		return FALSE;
		}

	}


	public function guest_checkin()
	{
		$result = $this->front_model->get_result('bookings',array('status'=>0));

		if(!empty($result)){
			foreach ($result as $row)
			{

			$cur_date_time = strtotime(date('Y-m-d'));
			$created_time = strtotime($row->check_in);

            if($cur_date_time == $created_time)
            	{
			 		/* Email To host Satrts*/
	                $this->load->library('smtp_lib/smtp_email');

					$data['booking_info']  = $this->front_model->get_row('bookings',array('id'=>$row->id));
					$data['host_info']     = get_user_info($data['booking_info']->owner_id);
					$data['guest_info']    = get_user_info($data['booking_info']->customer_id);
				    $data['property_info'] = get_property_detail($data['booking_info']->pr_id);

			        $email_template = get_manage_email_info('on_the_day_of_guest_checkin');
			        $website_info = get_manage_email_info('website_url_and_email');
			        $website_url = $website_info->website_url;
			        $from_email = $website_info->message_from_which_email;

			        $check_in_date = date('d-F-Y',strtotime($data['booking_info']->check_in));
			        $check_out_date = date('d-F-Y',strtotime($data['booking_info']->check_out));

			        $subject = $email_template->subject;

			        $arr_old = array('&lt;%hostname%&gt;','&lt;%guestname%&gt;','&lt;%guestcontactinfo%&gt;','&lt;%checkin%&gt;','&lt;%checkout%&gt;','&lt;%propertytitle%&gt;','&lt;%websiteurl%&gt;');
			        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,'Not Available Yet',$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
			        
			            
	                    if(!empty($data['guest_info']->phone))
	                    {
         			        $arr_update = array($data['host_info']->first_name,$data['guest_info']->first_name,$data['guest_info']->phone,$check_in_date,$check_out_date,$data['property_info']->title,$website_url);
	                    }
			      
			        $html =  str_replace($arr_old,$arr_update,$email_template->content);
                   
			        $to = array($data['host_info']->user_email);
			        $from = array($from_email =>$from_email);  // From email in array form 

					$s = $this->smtp_email->sendEmail($from, $to, $subject, $html);
			        $insert_data = array(
							        	  'name'=>$data['host_info']->first_name,
							        	  'email'=>$data['host_info']->user_email,
							        	  'subject'=>$subject,
							        	  'message'=>$html,
							        	  'created'=>date('Y-m-d h:i:s')
							        	  );

			        $this->front_model->insert('notifications',$insert_data);

		     	/*Email To host Ends*/
            	}


 		   }
		}
		else
		{
		  return FALSE;
		}

	}





}	