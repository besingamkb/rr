<?php $this->load->view('customer/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Favorites</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <?php if(!empty($favorites_property)): ?>
                                             <thead>
                                                  <tr>
                                                       <th style="width:10%">Image</th>
                                                       <th style="width:15%">Property</th>
                                                       <th style="width:15%">Property owner name</th>
                                                       <th style="width:10%">Property owner email</th>
                                                       <th style="width:15%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php $i=1; foreach ($favorites_property as $row):?>
                                                  <tr>
                                                       <td><img style="width:60px; height:60px;" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                                                       <td><?php echo word_limiter($row->title,10); ?></td>
                                                       <td><?php echo $row->first_name." ".$row->last_name; ?></td>
                                                       <td><?php echo $row->user_email ?></td>
                                                       <td style="text-align:center;">
                                                            <a href="<?php echo base_url();?>customer/view_favorites_property/<?php echo $row->user_id;?>/<?php echo $row->property_id;?>"   class="btn btn-info" >View</a>
                                                            <a href="<?php echo base_url();?>customer/delete_favorites_property/<?php echo $row->id;?>" class="btn btn-warning btn-small" onclick="return confirm('Do you want to delete?' );" >Delete</a>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($favorites_property): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- <td><a href="<?php echo base_url();?>superadmin/add_property_note/<?php echo $row->id;?>" class="btn btn-warning btn-small" >Add Note</a></td>
                                                   -->