<?php $this->load->view('customer/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review Full View</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-12 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Review Message</strong>
                    </div>
                    <div class="col-md-9" style="max-width:50%;">
                      <?php if(!empty($review_info->review)) echo $review_info->review; ?>    
                    </div>
                </div>
                
                <br>
                
             
                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Created Date</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review_info->created)) echo date("d-m-Y",strtotime($review_info->created)); ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Property Owner Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->first_name)) echo $customer_info->first_name." ".$customer_info->last_name; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Property Owner Email</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->user_email)) echo $customer_info->user_email; ?>    
                    </div>
                </div>
                
                <br>

              <br><br>
             <?php $created_time = strtotime($review_info->created); ?>
             <?php $today_time   = time() ?>
             <?php $difference_time = $today_time-$created_time  ?>
             <?php $two_week_time = strtotime('+14 days')-time() ?>
            
              <div class="row">
                <div class="col-md-3" align="right">
                <?php if($difference_time<$two_week_time): ?>
                   <a class="btn btn-info" href="<?php echo base_url()?>customer/edit_review/<?php echo $customer_info->id ?>/<?php echo $property_info->id ?>/<?php echo $review_info->id ?>">Edit Review</a>    
              <?php endif; ?>
                </div>
                <div class="col-md-9">
                   <a class="btn btn-info" href="<?php echo base_url()?>customer/review"> Back to Review</a>    
                </div>
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->