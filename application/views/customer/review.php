<?php $this->load->view('customer/leftbar'); ?>


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
        <table class="table table-hover">
          <?php if(!empty($review)) : ?>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Property Title</th>
                    <th>Review Message</th>
                    <th>Property Owner Email</th>
                    <th style="text-align:center">Operation</th>
                  </tr>
                </thead>
                <tbody>
              <?php foreach ($review as $row) : ?>
                  <tr>
                    <td><?php echo $row->property_id;     ?></td>
                    <td><?php echo word_limiter($row->title,3);  ?></td>
                    <td><?php echo word_limiter($row->review,2);          ?></td>
                    <td><?php echo $row->user_email;         ?></td>
                    <td style="text-align:center">
                        <a class="btn btn-info" title="Full View" href="<?php echo base_url(); ?>customer/review_full_view/<?php echo $row->user_id; ?>/<?php echo $row->property_id; ?>/<?php echo $row->id; ?>">View</a>
                        <a onclick="return confirm('Are you sure.')" class="btn btn-warning" title="Delete Review" href="<?php echo base_url(); ?>customer/review_delete/<?php echo $row->id; ?>">Delete</a>
                    </td>
                  </tr>
              <?php endforeach; ?> 
            <?php else : ?>
            <tr><td>No Review Found</td></tr>    
            <?php endif; ?>        
          </tbody>
        </table>
        <?php if($pagination){echo $pagination;} ?>
      </div>
    </div>
  </div>
</div><!-- /.container -->