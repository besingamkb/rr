<!-- ///////////////////  Reply Message /////////////////////////////// -->
<?php $customer_info = $this->session->userdata('customer_info');  ?>
<?php if($reply): ?>
<?php foreach ($reply as  $row): ?>
    <div class="col-md-12 row faraz" style="margin-left:1%; margin-top:50px;">
        <div class="col-md-2" align="center" >
        <?php $user  = get_user_information($row->replier_email); ?>
        <?php $image = $user->image; ?>
        <?php if(!empty($image)): ?>
        <img src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $image;  ?>" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php else: ?>
        <img src="<?php echo base_url(); ?>assets/img/msg.png" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php endif; ?>
            <br>
            <?php if($row->replier_status == 1 ) echo "Superadmin"; ?>
            <?php if($row->replier_status == 2 ) echo "Admin"; ?>
            <?php if($row->replier_status == 3 ) echo "Property owner"; ?>
            <?php if($row->replier_status == 4 )
                  {
                    if($customer_info['user_email'] == $row->replier_email) 
                        { echo "Me"; }
                    else 
                        { echo "customer"; }  
                  }   
            ?>
        </div>
        <div class="col-md-10" style="padding:10px; box-shadow:5px 10px 7px #8F8F91; border:1px solid #8F8F91; border-radius:5px; background-color:#EDF2F7; color:black;">
            <?php if(!empty($row->reply)) echo $row->reply; ?>    
            <div style="color:#E585A8; padding:5px;">
                Sent by&nbsp;<?php if(!empty($row->replier_email)) echo $row->replier_email; ?>
                (<?php if(!empty($row->replier_name)) echo $row->replier_name; ?>)
                &nbsp;on&nbsp;<?php echo get_time_ago($row->created); ?> 
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>  
