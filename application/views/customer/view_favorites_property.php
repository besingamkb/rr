<?php $this->load->view('customer/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review Full View</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Image</strong>
                    </div>
                    <div class="col-md-9">
                      <img style="width:100px; height:100px;" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($property_info->featured_image)) echo  $property_info->featured_image; ?>">
                    </div>
                </div>
                
                <br>
               
                
                <?php if(!empty($property_info->description)): ?>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Description</strong>
                    </div>
                    <div class="col-md-9">
                      <?php  echo $property_info->description; ?>    
                    </div>
                </div>
                
                <br>
                <?php endif; ?>
                <?php if(!empty($property_info->created)): ?>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Created</strong>
                    </div>
                    <div class="col-md-9">
                      <?php echo date('d-m-Y',strtotime($property_info->created)); ?>    
                    </div>
                </div>
                
                <br>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Owner Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->first_name)) echo $customer_info->first_name." ".$customer_info->last_name; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>Owner Email</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->user_email)) echo $customer_info->user_email; ?>    
                    </div>
                </div>
                
                <br>


              <br><br>

              <div class="row">
                <div class="col-md-3">
                 <strong>&nbsp;&nbsp;&nbsp;</strong>
                </div>
                <div class="col-md-9">
                   <a class="btn btn-info" href="<?php echo base_url()?>customer/favorites_property"> Back to Review</a>    
                </div>
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->