<?php $this->load->view('customer/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Edit Review</h3>
          </div>
          <br>
          <div class="col-md-12 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                
                <br>
                
                
                <div class="row">
                    <div class="col-md-3" align="right">
                     <strong>Property Owner Email</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->user_email)) echo $customer_info->user_email; ?>    
                    </div>
                </div>
                
                <br>
               <div class="col-md-8" style="margin-left:2%; margin-bottom:2%;padding:0px !important">
                <?php echo form_open(base_url().'customer/edit_review/0/0/'.$review_info->id); ?>
                    <textarea rows="3"style="width:70%;padding:5px" name="review" required="required"><?php echo $review_info->review ?></textarea>
                            <br><br>
                            <button style="margin-left:295px" class="btn btn-info btn-large hidden-phone">
                                Review
                            </button>
                <?php echo form_close(); ?>
              </div>
  
            
         </div>
    </div>
  </div>
</div><!-- /.container -->