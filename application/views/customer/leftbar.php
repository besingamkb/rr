<div class="col-lg-12 btm-nav">
    <div class="container">
        <div class="btn-group btn-pdng col-lg-7">
            <a style="text-decoration:none;" href="<?php echo base_url()?>customer/dashboard"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-home"></span >&nbsp;Dashboard</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>customer/profile">  <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-user"></span >&nbsp;Profile</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>customer/bookings"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-flash"></span>&nbsp;Booking</button></a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>customer/messages"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-comment"></span >&nbsp;Inbox</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>customer/change_password"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit"></span >&nbsp;Account</button> </a>
        </div>
        <div class="pull-right">
            <form class="navbar-form" role="search">
                <div class="input-group">
                    <input type="text" class="srch-box form-control" placeholder="Search" name="srch-term" id="srch-term">
                    <div class="input-group-btn">
                        <button class="btn btn-default src-btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-lg-3 side-bar">
        <ul>
            <li style="text-align:center">
                <?php $customer_info = $this->session->userdata('customer_info'); ?>
                <?php $id = $customer_info['id']; ?>
                <?php $user = get_user_info($id);  ?> 
                <?php if($user->image != "") : ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="height:200px;width:200px"><br>
                <?php else: ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:150px"><br> 
                <?php endif; ?>
                <p></p>
                <p>
                    <?php echo ucfirst($user->first_name)." ".ucfirst($user->last_name); ?>
                </p>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>customer/profile">
                    <span>View Profile</span>
                </a> 
                <span class="f-right"></span>
            </li>
            <li>
                <a style="text-decoration:none;"  href="<?php echo base_url(); ?>customer/messages">
                    <span>Messages</span>
                </a>
                <span class="f-right"><?php echo get_customer_msg_count(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>customer/review"> 
                    <span>Reviews</span>
                </a>    
            <span class="f-right"><?php echo get_user_review_count(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>customer/favorites_property"> 
                    <span>Favorites</span>
                </a>    
             <span class="f-right"><?php echo get_favorites_count(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>customer/groups"> 
                    <span>Groups</span>
                </a>    
                <span class="f-right"><?php echo get_number_of_groups(); ?></span>
            </li>
             <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>customer/change_profile_image">
                    <span>Change Profile Image</span>
                </a>    
            </li>
            <li class="no-border">
                <a style="text-decoration:none;" href="<?php echo base_url()?>customer/change_password">
                    <span>Change Password</span>
                </a>    
            </li>
        </ul>
        <ul>
            <li>
                <p>Upcomming Booking</p>
                <p></p>
                <p>You have no upcomming booking</p>
                <p>Discover or create one</p>
            </li>
        </ul>
    </div>