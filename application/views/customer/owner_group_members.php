<?php $this->load->view('customer/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Property Owner</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                        <a class="btn" href="<?php echo base_url(); ?>customer/view_group_members/<?php echo $group_id ?>"> Back To Members </a>  
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:4%">#</th>
                                                       <th style="width:10%">Image</th>
                                                       <th style="width:15%">Owner name</th>
                                                       <th style="width:15%;" >Member since</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($members)): ?>
                                             <?php $i=1; foreach ($members as $row):?>
                                                  <tr> 
                                                       <td><?php echo $i++; ?></td>
                                                       <td><img style="width:80px; height:80px;border-radius:3px" src="<?php echo base_url()?>assets/uploads/profile_image/<?php  if(!empty($row->image)) echo  $row->image; else echo "default-medium.png" ?>"></td>
                                                       <td><?php echo $row->first_name." ".$row->last_name; ?></td>
                                                       <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                                                  </tr>
                                             <?php  endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($members): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
