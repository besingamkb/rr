<?php $this->load->view('customer/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Group Properties</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                        <a class="btn" href="<?php echo base_url(); ?>customer/groups"> Back to groups</a>  
                                   </div>
                              </div><br><br>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:4%">#</th>
                                                       <th style="width:10%">Image</th>
                                                       <th style="width:14%">Property Title</th>
                                                       <th style="width:14%">Owner name</th>
                                                       <th style="width:10%">Owner Email</th>
                                                       <th style="width:15%; text-align:center;" >Added On</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($members_properties)): ?>
                                             <?php $i=1; foreach ($members_properties as $row):?>
                                                  <tr>
                                                       <td><?php echo $i++ ?></td>
                                                       <td><img style="width:80px; height:80px; border-radius:4px" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                                                       <td><?php echo word_limiter($row->title,10); ?></td>
                                                       <td><?php echo $row->first_name." ".$row->last_name ?></td>
                                                       <td><?php echo $row->user_email ?></td>
                                                       <td style="text-align:center;">
                                                        <?php echo date('d-m-Y',strtotime($row->created)) ?>
                                                       </td>
                                                  </tr>
                                             <?php  endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Properties Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($members_properties): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

