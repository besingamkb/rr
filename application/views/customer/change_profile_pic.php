 <style>
#button123
{
  border-color: black;
  border: 1px solid #999;
  border-radius: 3px;
  padding: 5px 8px;
  cursor: pointer;
  text-shadow: 3px 3px #fff;
  font-weight: 700;
  font-size: 10pt;
}
</style>
 
<?php $this->load->view('customer/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Change Profile Image</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span align="center" style="padding:5px !important; width:400px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br><br>
        <div align="center" class="form-group"> 

         <?php if(!empty($user->image)) : ?>

            <img  src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="border-radius:5px; width:150px; height:150px;"><br>
        <?php else: ?>

            <img  src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:200px"><br>

        <?php endif; ?>

           <br>
          <?php echo form_open_multipart(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
              
                <input type="file" name="userfile" id="button123">
                <br>
                <button type="submit" class="btn" >Changes Image</button>
            
          <?php echo form_close(); ?>
        </div>
        </div>
      </div>
    </div><!-- /.container -->

