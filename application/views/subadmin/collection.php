      <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id())); ?>
 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Collection Categories
          </div>
        <?php if($subadmin_restrictions->collection!=1):  ?>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_collection_category"> Add Collection Category </a>
          </div>
         <?php endif; ?>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:10%">Image</th>
                  <th style="width:10%">Category Name</th>
                  <th style="width:30%">Category Description</th>
                  <th style="width:30%">Created</th>
        <?php if($subadmin_restrictions->collection!=1):  ?>
                  <th style="width:15%">Actions</th>
          <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($collection)): ?>
                    <?php $i=1; foreach ($collection as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><img style="width:100%;" src="<?php echo base_url()?>assets/uploads/collection_banner/<?php echo $row->banner; ?>"></td>
                        <td><?php echo $row->category_name; ?></td>
                        <td><?php echo $row->description; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>

        <?php if($subadmin_restrictions->collection!=1):  ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/delete_collection_category/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>subadmin/edit_collection_category/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                        </td>
        <?php endif; ?>
        <?php endif; ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>