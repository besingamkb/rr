<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Payment Details
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php $pro_info = get_property_detail($payment->property_id); 
              $user = get_user_info($pro_info->user_id);  ?>
            <div class="form-horizontal no-margin well" >
              <div class="control-group">
                <label class="control-label">
                 Guset Name
                </label>
                <div class="controls controls-row span6">
                  <span class="form_error span12">
                   <strong>
                    <?php $customer = get_user_info($payment->user_id); ?>
                    <?php echo $customer->first_name." ".$customer->last_name; ?>
                 </strong>
                  </span>
                </div>
              </div>



              <div class="control-group">
                <label class="control-label" for="your-name">
                Property Title
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $pro_info->title; ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Propety Description
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $pro_info->description; ?></span>
                </div>
              </div>


               <div class="control-group">
                <label class="control-label" for="your-name">
                 Host name
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $user->first_name.' '.$user->last_name; ?></span>
                </div>
              </div> 


              <div class="control-group">
                <label class="control-label" for="your-name">
                 Amount
                </label>
                <div class="controls controls-row span6">                
                  <span class="form_form_error span12"><?php echo $payment->amount; ?></span>
                </div>
              </div> 

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Booking Date
                </label>
                <div class="controls controls-row span6">                
                  <span class="form_form_error span12"><?php echo date('m/d/Y', strtotime($payment->amount)); ?></span>
                </div>
              </div> 

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Payments status
                </label>
                <div class="controls controls-row span6">                
                  <span class="form_form_error span12"><?php if ($payment->is_refund == 1){ echo "Refud"; }else{ echo "Completed"; } ?></span>
                </div>
              </div> 

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Host Payment Status
                </label>
                <div class="controls controls-row span6">                
                  <span class="form_form_error span12"><?php if ($payment->paid_to_owner == 1){ echo "Paid"; }else{ echo "Pending"; } ?></span>
                </div>
              </div> 

                    

              <div class="form-actions no-margin">
                <a href="<?php echo base_url();?>subadmin/payments" class="btn btn-info">
                  Back To payment
                </a>
               
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
    .controls span.span12{
      padding-top: 6px;
    }

  </style>