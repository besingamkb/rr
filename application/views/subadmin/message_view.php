<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin" >
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                    Message Full View
                </div>
            </div>
        </div>
        <?php if(!empty($message)): ?>
        <table align="left" style="border:5px #F2F2F2 solid; border-top:none; width:700px;  margin-top:20px;" class="table table-condensed table-striped">    
            <thead>
                <tr>
                    <th>
                        <span style="font-size:20px; color:#D33F3E;" class="fs1" data-icon="" aria-hidden="true">
                            User
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div  style="color:black; font-size:14px; font-weight:bold">
                            Property title:
                        </div>
                        <div style="margin-left:75px">
                            <?php echo $message->pr_title; ?>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="color:black; font-size:14px; font-weight:bold ">
                            Message:
                        </div>
                        <div style="margin-left:75px">
                            <?php echo $message->message; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pull-right"> 
                            Sent by&nbsp;<?php echo ucwords($message->user_email); ?>
                            (<span style="color:#D33F3E"><?php echo ucwords($message->user_name); ?></span>)
                            &nbsp; on &nbsp;<?php echo get_time_ago($message->created); ?> 
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php endif; ?>
<div class="appendable">
        <?php if(!empty($reply)): ?>
        <?php foreach ($reply as $row): ?>
        <table  style="width:700px; border:5px #F2F2F2 solid; border-top:none; margin-top:20px;" class="table table-condensed table-striped faraz" align='left'>    
            <thead>
                <tr>
                    <th>
                        <span class="fs1" data-icon="" aria-hidden="true" style="color:#D33F3E; font-size:20px;">
                           <a class="btn btn-small btn-primary hidden-tablet hidden-phone" onclick="return confirm('Are you want to delete?');" href="<?php echo  base_url() ?>subadmin/delete_reply/<?php echo $row->id ?>/<?php echo $row->message_id ?>" style="margin-left:73%">
                             <i class="icon-remove"></i>
                          </a>                   
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div style="color:black; font-size:14px; font-weight:bold ">
                            Reply:
                        </div>
                        <div style="margin-left:55px">
                            <?php echo $row->reply; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pull-right"> 
                            <?php $user = get_user_info($row->replier_id);  ?>
                            Sent by <span style="color:#D33F3E"><?php echo ucfirst($user->first_name.' '.$user->last_name); ?></span>
                            on <?php echo get_time_ago($row->created); ?>
                        </span> 
                    </td>
                </tr>
            </tbody>
        </table>
        <?php endforeach; ?>   
        <?php endif; ?>
</div>



<!-- //////////////////////// load more start   /////////////////////// -->
    <table align="left" style="width:702px; margin-top:20px; color:#D33F3E;">
        <?php if ($total_reply >5): ?>
        <tr><td>    
            <h4><a id="loadmore" style="margin-left:20px; cursor:pointer;">Load more.........</a></h4> 
        </td></tr>
        <?php endif; ?>
        <input type="hidden" value="<?php echo $total_reply; ?>" id="total_reply">
    </table>
<!-- /////////////////////   end of load more  //////////////////////// -->



        <?php echo form_open(base_url().'subadmin/reply/'.$message->id.'/'.$message->sender_id); ?>
        <table align="left" style="width:702px; margin-top:20px;">    
            <tr>
                <td>
                    <textarea rows="2" style="width:680px; border:5px #EFEFEF solid; border-radius:15px"  name="reply" required="required"></textarea>
                    <br>
                    <button style="margin-left:610px" class="btn btn-info btn-large hidden-phone">
                        Reply
                    </button>
                </td>
            </tr>
        </table>
        <?php echo form_close(); ?>
        <br>

    </div>
</div>


<script>
$(document).ready(function(){  
   $("#loadmore").click(function(){ 
      
      var offset = $(".faraz").length;
      $.ajax({
               url: "<?php echo base_url(); ?>subadmin/ajax_load_more_reply/<?php echo $message->id; ?>/"+offset,
               success: function(data)
               {
                 $(".appendable").append(data);
               }
      });
      
         var total_rows = $("#total_reply").val();
         if(total_rows <= offset+5)
         {
         $("#loadmore").hide();
         }
   });
});
</script>