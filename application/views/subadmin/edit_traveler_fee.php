 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>  Edit Traveler Service fee
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label">
                 
                  Type
                </label>
                <div class="wysiwyg-container controls controls-row span5">
                  <select name="type">
                      <option value="">Select  Fee Type</option>
                      <option value="1" <?php  if($traveler->type == 1) echo"selected";?> >Amount</option>
                      <option value="2" <?php  if($traveler->type == 2) echo"selected";?> >Percentage</option>
                  </select>
                  <span class="form_error span12"><?php echo form_error('type'); ?></span>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Traveler Service Fee
                </label>
                <div class="controls controls-row span5">
                  <input name="ts_fee" class="span12" type="text" placeholder="Traveler Service Fee" value="<?php echo $traveler->ts_fee; ?>">
                  <span class="form_error span12"><?php echo form_error('ts_fee'); ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>