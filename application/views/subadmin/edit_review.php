 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Edit Review
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

            <div class="control-group">
                <label id="title" class="control-label">&nbsp;
                </label> 
                <div id="image" class="controls controls-row span8">
                <h3><?php echo $pro->title; ?></h3>
                <br>
                <img src='<?php echo base_url(); ?>assets/uploads/property/<?php echo $pro->featured_image; ?>' style='width:200px;  height:80px; border-radius:15px; border:5px solid #ffffff;' alt=' '>
                
              </div>
            </div>





             <div class="control-group">
                <label class="control-label">
                Rating
                </label>
                <div class="controls controls-row span8">

                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->
                          <link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
                          <div style="padding:10px 0px 20px 0px;" id="rating_div">
                              <input class="star required" type="radio" name="rating" value="1" <?php if($review->rating == 1){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="2" <?php if($review->rating == 2){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="3" <?php if($review->rating == 3){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="4" <?php if($review->rating == 4){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="5" <?php if($review->rating == 5){ echo 'checked'; } ?>  >
                          </div>
                          <script> 
                             $(function(){ $('#rating_div :radio.star').rating(); });
                          </script>
                          <script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->


                </div>
              </div>


             <div class="control-group">
                <label class="control-label">
                Review
                </label>
                <div class="wysiwyg-container controls controls-row span8">
                  <textarea name="review" class="input-block-level no-margin" placeholder="Enter review text ..." style="height: 140px"><?php echo $review->review; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('review'); ?></span>
                </div>
              </div>


        

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Status
                </label>
                <div class="controls controls-row span8">
                  <select name="status" class="span4" required="required">
                        <option value="">Select status</option>
                        <option value="1" <?php if($review->status==1) echo "selected"; ?> >Publish</option>
                        <option value="0" <?php if($review->status==0) echo "selected"; ?> >Unpublish</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('status'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
