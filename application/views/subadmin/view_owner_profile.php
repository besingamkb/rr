<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin" >
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                    View Owner Profile
                </div>
            </div>
        </div>
        <?php if(!empty($owner_info)): ?>
        <table align="center" style="border:5px #F2F2F2 solid; border-top:none; width:50%;  margin-top:20px;" class="table table-condensed table-striped">    
            <thead>
                <tr>
                    <th style="width:155px;">
                        <span style="font-size:20px; color:#D33F3E; "  data-icon="" >
                            Owner Profile
                        </span>
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <?php 
                        echo "<h2>".$owner_info->first_name." ".$owner_info->last_name."</h2>";
                        ?>
                    </td>
                </tr>
    
                <tr>
                    <td colspan="2">    
                        <img style="width:100%; height:200px;" src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $owner_info->image; ?>">  
                    </td>
                </tr>
    
                <tr>
                    <td>
                        Email
                    </td>
                    <td>    
                        <?php echo $owner_info->user_email; ?>
                    </td>
                </tr>
              <?php if($owner_info->city!=""):  ?>
                <tr>
                    <td>
                        City
                    </td>
                    <td>    
                        <?php echo $owner_info->city; ?>
                    </td>
                </tr>
            <?php endif; ?>
             <?php if($owner_info->state!=""):  ?>
                <tr>
                    <td>
                        State
                    </td>
                    <td>    
                        <?php echo $owner_info->state; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php if($owner_info->country!=""):  ?>
                <tr>
                    <td>
                        Country
                    </td>
                    <td>    
                        <?php echo $owner_info->country; ?>
                    </td>
                </tr>
      
            <?php endif; ?>

                <tr>
                    <td>
                        <a class="btn btn-success"   href="<?php echo base_url(); ?>subadmin/group_members/<?php echo $group_id ?>" >Back</a> 
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>

