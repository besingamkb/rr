 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>  Invite Friend Commission
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label">
                 Commission Type
                </label>
                <div class="wysiwyg-container controls controls-row span5">
                  <input name="com_type" class="span12" type="text" placeholder="Commission Type" readonly="readonly" value="Amount">

                  <span class="form_error span12"><?php echo form_error('com_type'); ?></span>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Booking Commission
                </label>
                <div class="controls controls-row span5">
                  <input name="booking_commission" class="span12" type="text" placeholder="Booking Commission" value="<?php echo $commission->booking_commission; ?>">
                  <span class="form_error span12"><?php echo form_error('booking_commission'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Listing Commission
                </label>
                <div class="controls controls-row span5">
                  <input name="listing_commission" class="span12" type="text" placeholder="Listing Commission" value="<?php echo $commission->listing_commission; ?>">
                  <span class="form_error span12"><?php echo form_error('listing_commission'); ?></span>
                </div>
              </div>


              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>