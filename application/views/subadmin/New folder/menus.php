 <div class="container-fluid">
  <div class="row-fluid">
      <div class="span2">
       <?php $this->load->view('admin/leftbar'); ?> 
      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Menus</h4>
            <a href="<?php echo base_url() ?>admin/add_menu">Add New</a>
        </div><!-- page-header -->

      <div class="row-fluid">         
          <div class="span12">
             <!-- alert -->
             <?php alert() ?>
            <!-- alert -->
            <table class="table">
              <thead>
                <tr>
                  <!-- <th>S.no</th>-->
                  <th width="25%">Name</th>              
                  <th width="30%">Url</th>                  
                  <th width="15%">order</th>                  
                  <th width="20%">Date</th>        
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>

                <?php if ($menus): ?>

                  <?php foreach ($menus as $row): ?>

                    <tr>
                      <td><?php echo $row->name?></td>
                      <td><a href="<?php echo base_url()."pages/".$row->link ?>"><?php echo base_url()."pages/".$row->link; ?></a></td>
                      <td><?php echo $row->order?></td>
                      <td><?php echo date('m/d/y', strtotime($row->created)); ?></td>                      
                      <td style="text-align:left;">
                         &nbsp;&nbsp;&nbsp;
                         <a href="<?php echo base_url() ?>admin/edit_menu/<?php echo $row->id ?>"><i class="icon-pencil"></i></a>
                         &nbsp;&nbsp;&nbsp;                  
                         <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() ?>admin/delete_menu/<?php echo $row->id ?>"><i class="icon-remove"></i></a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                <?php endif ?>
              </tbody>
            </table>

            <?php if($pagination) echo $pagination; ?>

          </div>    

      </div><!-- row-fluid -->

      </div><!--/span10-->

  </div><!--/row-->    

</div><!--/.fluid-container-->