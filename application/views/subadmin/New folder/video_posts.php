 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
       <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Posts</h4>
            <a href="<?php echo base_url() ?>admin/add_video_post" title="">Add new</a>
        </div><!-- page-header -->
      <div class="row-fluid">          
          <div class="span12">
            <!-- alert -->
             <?php alert() ?>
            <!-- alert -->

            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50%">Title</th>                  
                  <th width="10%">Category</th>
                  <th width="10%">Comments</th>
                  <th width="10%">Date</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($video_posts): ?>
                  <?php foreach ($video_posts as $row): ?>
                    <tr>
                      <td> <a href="<?php echo base_url() ?>admin/edit_video_post/<?php echo $row->id ?>"><?php echo $row->post_title ?></a></td>                      
                      <td><?php echo $row->category_name ?></td>
                      <td><span class="badge badge-info"><?php echo comment_count($row->id); ?></span></td>
                      <td><?php $date=explode(",", timespan(strtotime($row->created), time())); echo $date[0] ?> ago <br> 
                        <?php if($row->post_status== 1): ?>
                          <span class="label label-success">Publish</span>
                        <?php else: ?>
                          <span class="label label-warning">Unpublish</span>
                        <?php endif ?>
                      </td>
                      <td> 
                         <a href="<?php echo base_url() ?>admin/edit_video_post/<?php echo $row->id ?>"><i class="icon-pencil"></i></a>
                         &nbsp;&nbsp;&nbsp;  
                         <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() ?>admin/delete_video_post/<?php echo $row->id ?>"><i class="icon-remove"></i></a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                <?php endif ?>
              </tbody>
            </table>
            <?php if($pagination) echo $pagination; ?>
          </div>    
      </div><!-- row-fluid -->
      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->