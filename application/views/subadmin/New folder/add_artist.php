
<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">

  tinyMCE.init({
    mode : "textareas",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",   
    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'
     }); 
</script>
<!-- /TinyMCE -->
 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
          <div class="page-header">
            <h4>Add Artist</h4>
          </div><!-- page-header -->
          <?php alert() ?>
          <div class="row-fluid">
              <div class="span8">
                <?php echo form_open_multipart(current_url()); ?>
                  <div class="control-group">
                    <label class="control-label" for="">Name</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="name" value="<?php echo set_value('name') ?>" placeholder="Enter Name Here">
                    </div>
                    <?php echo form_error('name') ?>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label" for="">Video Type</label>
                    <div class="controls">
                      <select id="video_type" name="video_type">
                        <option value="">Please select</option>
                        <option value="1">Youtube</option>
                        <option value="2">Vimeo</option>                        
                      </select>
                    </div>
                    <?php echo form_error('video_type') ?>
                  </div>
                  <div id="video_url" class="control-group">
                    <label class="control-label" for="">Video URL</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="video_url" placeholder="Enter Video Url Here eg-http://www.youtube.com/embed/X_P26tfRxfI?feature=player_detailpage">
                    </div>
                    <?php echo form_error('video_url') ?>
                  </div>              
                  <div class="control-group">
                    <label class="control-label" for="">Description</label>                    
                    <div class="controls">
                       <textarea id="textarea" class="span12" rows="10"  name="description" placeholder="Enter Content Here"><?php echo set_value('description') ?></textarea>
                    </div>
                    <?php echo form_error('description') ?>
                  </div>
                  
                   <div class="control-group">
                    <label class="control-label" for="">Artist Image <span style="font-size:10px;"> <!-- (800 x 530) in px --> </span> </label>
                    <div class="controls">
                      <input type="file" name="userfile">
                    </div>                 
                  </div>
                 
                  <div class="control-group">
                    <label class="control-label" for=""></label>                   
                    <div class="controls">
                       <input type="submit" value="Add Post" class="btn btn-primary">
                    </div>                 
                  </div>                
               </div> <!--span8 -->
              <?php echo form_close(); ?>
          

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->