<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">

  tinyMCE.init({
    mode : "textareas",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",   
    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'
     }); 
</script>
<!-- /TinyMCE -->

<div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
          <div class="page-header">
            <h4>Edit Artist</h4>
          </div><!-- page-header -->
          <?php alert() ?>

          <div class="row-fluid">
              <div class="span8">
                <?php echo form_open_multipart(current_url()); ?>
                  <div class="control-group">
                    <label class="control-label" for="">Name</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="name" value="<?php echo $artist->name ?>" placeholder="Enter Name Here">
                    </div>
                    <?php echo form_error('name') ?>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="">Video Type</label>
                    <div class="controls">
                      <select id="video_type" name="video_type">
                        <option value="">Please select</option>
                        <option value="1" <?php if($artist->video_type == 1) echo "selected='selected'"; ?>>Youtube</option>
                        <option value="2" <?php if($artist->video_type == 2) echo "selected='selected'"; ?>>Vimeo</option>                        
                      </select>
                    </div>
                    <?php echo form_error('video_type') ?>
                  </div>
                  <div id="video_url" class="control-group">
                    <label class="control-label" for="">Video URL</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="video_url" value="<?php if(!empty($artist->video_url)) echo $artist->video_url; ?>" placeholder="Enter Video Url Here">
                    </div>
                    <?php echo form_error('video_url') ?>
                  </div>
                   <div class="control-group">
                    <label class="control-label" for="">Description</label>                    
                    <div class="controls">
                       <textarea id="textarea" class="span12" rows="10"  name="description" placeholder="Enter Content Here"><?php echo $artist->description ?></textarea>
                    </div>
                    <?php echo form_error('description') ?>
                  </div>                  
                 <div class="control-group">
                  <label class="control-label" for="">Artist Image <span style="font-size:10px;"> <!-- (640 x 360) in px --> </span> </label>
                  <div class="controls">
                    <input type="file" name="userfile">
                    <?php if (!empty($artist->artist_image)): ?>
                      <img src="<?php echo  base_url() ?>assets/uploads/custom_uploads/<?php echo $artist->artist_image; ?>" width="100px" >
                    <?php endif ?>
                  </div>                 
                </div>
                  <div class="control-group">
                    <label class="control-label" for=""></label>                   
                    <div class="controls">
                       <input type="submit" value="Update" class="btn btn-primary">
                    </div>                 
                  </div>                
               </div> <!--span8 -->
              <?php echo form_close(); ?>
          

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->
