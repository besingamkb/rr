 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Edit Comment</h4>
        </div><!-- page-header -->

          <div class="row-fluid">
          <div class="span8">
              <?php echo form_open(current_url()); ?>
                <div class="control-group">
                  <label class="control-label" for="">Name</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="comment_author" value="<?php if(!empty($comments->comment_author)) echo $comments->comment_author; else echo set_value('comment_author') ?>" placeholder="Enter Name Here">
                  </div>
                  <?php echo form_error('comment_author') ?>
                </div>
                 <div class="control-group">
                  <label class="control-label" for="">E-mail <?php if (!empty($comments->comment_author_email)): ?>
                  <a href="mailto:<?php echo $comments->comment_author_email ?>">(send e-mail)</a><?php endif ?></label>
                  <div class="controls">
                    <input  class="span12" type="text" name="comment_author_email" value="<?php if(!empty($comments->comment_author_email)) echo $comments->comment_author_email; else echo set_value('comment_author_email') ?>" placeholder="Enter Email Here">
                  </div>
                  <?php echo form_error('comment_author_email') ?>
                </div>                
                <div class="control-group">
                  <label class="control-label" for="">Comment</label>
                  <div class="controls">
                     <textarea class="span12" rows="10"  name="comment_content" placeholder="Enter Comment Here"><?php if(!empty($comments->comment_content)) echo $comments->comment_content; else echo set_value('comment_content') ?></textarea>
                  </div>
                  <?php echo form_error('comment_content') ?>
                </div>
                 <div class="control-group">
                  <label class="control-label" for="">Status</label>
                  <div class="controls">
                
                      <input type="radio" name="comment_approved" value="1" <?php if($comments->comment_approved=='1') echo "checked"; ?> > <span class="text-success">Approve</span>

                        <input type="radio" name="comment_approved" value="0" <?php if($comments->comment_approved=='0') echo "checked"; ?> > <span class="text-warning">Pending</span>
                         <input type="radio" name="comment_approved" value="2" <?php if($comments->comment_approved=='2') echo "checked"; ?> > <span class="text-error">Spam</span>
                   
                  </div>

                  <div class="control-group">
                  <br>
                  <div class="controls">
                  
                     <label class="control-label" for="">Submitted on: <strong><?php echo date('M d, Y',strtotime($comments->created)) ?> @  <?php echo date('H:i',strtotime($comments->created)) ?></strong></label>
                     <br>
                  </div>                   
                </div> 

                <div class="control-group">
                  <div class="controls">                  
                    <button type="submit" class="btn btn-large btn-primary">Update</button>
                  </div>
                </div>
              <?php echo form_close(); ?>
              </div>    
            </div><!-- row-fluid -->


      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->