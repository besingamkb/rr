 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Add Blog Post</h4>
        </div><!-- page-header -->
        <?php alert() ?>
         <?php echo form_open_multipart(current_url()); ?>
          <div class="row-fluid">
          <div class="span8">
             
                <div class="control-group">
                  <label class="control-label" for="">Post Title</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="post_title" value="<?php echo set_value('post_title') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('post_title') ?></span>
                </div>
                <div class="control-group">
                  <label class="control-label" for="">Post Excerpt</label>
                  <div class="controls">
                    <textarea name="excerpt" class="span12" ><?php echo set_value('excerpt'); ?></textarea>
                  </div>
                 <span style="color:red"><?php echo form_error('excerpt') ?></span>
                </div>
                <div class="control-group">
                  <label class="control-label" for="">Post Description</label>
                  <div class="controls">
                   <textarea class="span12 mceEditor" rows="10" name="description" ><?php echo set_value('description'); ?></textarea>
                  </div>
                 <span style="color:red"><?php echo form_error('description') ?></span>
                </div>
                 <div class="control-group">
                  <label class="control-label" for="">Post Image <span style="font-size:10px;"> (640 x 360) in px </span> </label>
                  <div class="controls">
                    <input type="file" name="userfile">
                  </div>                 
                </div>

                <div class="control-group">
                  <label class="control-label" for="">Select Category</label>
                  <div class="controls">
                  <select name="category">
                    <option value="">Please select</option>
                    <?php foreach ($category as $key): ?>                      
                    <option value="<?php echo $key->id ?>"><?php echo $key->category_name; ?></option>
                    <?php endforeach ?>
                  </select>
                  </div>
                 <span style="color:red"><?php echo form_error('category') ?></span>
                </div>
                <div class="control-group">
                  <label class="control-label" for="">Status</label>
                  <div class="controls">
                  <select name="post_status">
                    <option value="">Please select</option>
                    <option value="1">Publish</option>
                    <option value="2">Unpublish</option>
                  </select>
                  </div>
                 <span style="color:red"><?php echo form_error('status') ?></span>
                </div>
               
                <div class="control-group">
                  <label class="control-label" for=""></label>
                  <div class="controls">
                    <input  class="btn btn-primary" type="submit" value="submit">
                  </div>
                  
                </div>
            </div> <!-- span8 -->

          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->