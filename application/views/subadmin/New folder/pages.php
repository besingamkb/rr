 <div class="container-fluid">
  <div class="row-fluid">
      <div class="span2">
       <?php $this->load->view('admin/leftbar'); ?> 
      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Pages</h4>
            <a href="<?php echo base_url() ?>admin/add_page">Add New</a>
        </div><!-- page-header -->

      <div class="row-fluid">         
          <div class="span12">
             <!-- alert -->
             <?php alert() ?>
            <!-- alert -->
            <table class="table">
              <thead>
                <tr>
                  <!-- <th>S.no</th>-->
                  <th>Page Title</th>              
                  <th width="10%">Slug</th>                  
                  <th width="10%">Date</th>        
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>

                <?php if ($pages): ?>

                  <?php foreach ($pages as $row): ?>

                    <tr>
                      <td><?php echo $row->page_title?></td>
                      <td><?php echo $row->slug?></td>
                      <td><?php echo date('m/d/y', strtotime($row->created)); ?></td>                      
                      <td style="text-align:left;">

                         <!-- <a href="<?php //echo base_url() ?>admin/get_form_field/<?php //echo $row->id ?>"><i class="icon-eye-open"></i></a> -->

                         &nbsp;&nbsp;&nbsp;

                         <a href="<?php echo base_url() ?>admin/edit_page/<?php echo $row->id ?>"><i class="icon-pencil"></i></a>

                         &nbsp;&nbsp;&nbsp;                  

                         <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() ?>admin/delete_page/<?php echo $row->id ?>"><i class="icon-remove"></i></a>

                      </td>

                    </tr>

                  <?php endforeach ?>

                <?php endif ?>

              </tbody>

            </table>

            <?php if($pagination) echo $pagination; ?>

          </div>    

      </div><!-- row-fluid -->

      </div><!--/span10-->

  </div><!--/row-->    

</div><!--/.fluid-container-->