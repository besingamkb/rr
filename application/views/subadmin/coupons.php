 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Coupons
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_coupon">Add Coupons</a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:20%">Coupon Name</th>
                  <th style="width:30%">Amount</th>
                  <!-- <th style="width:30%">Reduction Type</th> -->

                  <th style="width:30%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($coupons)): ?>
                    <?php $i=1; foreach ($coupons as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo word_limiter($row->name,20); ?></td>
                        <td><?php echo $row->reduction_amount; ?></td>
                        <!-- <td><?php echo $row->reduction_type; ?></td> -->

                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        <td>
                          <a class="btn btn-small" href="<?php echo base_url(); ?>subadmin/coupons_notes/<?php echo $row->id;?>">Notes</a>
                          <a href="<?php echo base_url()?>subadmin/delete_coupon/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-success btn-small hidden-phone" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>subadmin/edit_coupon/<?php echo $row->id;?>" id="" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">edit</a>
                        </td>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <th colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>


                    </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>