<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Home page Content
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Company Name
                </label>
                <div class="controls controls-row span6">
                  <input name="name" class="span12" type="text" placeholder="" value="<?php  if(!empty($info->name)) echo $info->name; else echo set_value('name'); ?>">
                  <span class="form_error span12"><?php echo form_error('name'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Company Full Address
                </label>
                <div class="controls controls-row span6">
                  <input name="address" class="span12" type="text" placeholder="" value="<?php  if(!empty($info->address)) echo $info->address; else echo set_value('address'); ?>">
                  <span class="form_error span12"><?php echo form_error('address'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Company Email
                </label>
                <div class="controls controls-row span6">
                  <input name="email" class="span12" type="text" placeholder="" value="<?php  if(!empty($info->email)) echo $info->email; else echo set_value('email'); ?>">
                  <span class="form_error span12"><?php echo form_error('email'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Company Phone
                </label>
                <div class="controls controls-row span6">
                  <input name="phone" class="span12" type="text" placeholder="" value="<?php  if(!empty($info->phone)) echo $info->phone; else echo set_value('phone'); ?>">
                  <span class="form_error span12"><?php echo form_error('phone'); ?></span>
                </div>
              </div>


              
              

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>