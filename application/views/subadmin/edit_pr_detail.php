
<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>
 
 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Property
            </div>
          </div>
          <div class="widget-body">
           
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
           <br>

           <div class="control-group">
                <label class="control-label">
                Property Description
                </label>
                <div class="controls controls-row span8">
                    <div class="row-fluid">

                      
                      <div class="span3">
                        <input class="span12" value="<?php if(!empty($properties->application_fee)){ echo $properties->application_fee;}?>"  type="text" placeholder="How Much" name="appliction_fee">
                      <span class="form_error span12"><?php echo form_error('appliction_fee'); ?></span>
                      </div> 

                      <div class="span3">
                         <select class="span12"  name="room_allotment">                            
                            <option value="" >Room Allot</option>
                            <option value="1"  <?php if(!empty($properties->room_allotment)){   if($properties->room_allotment =='1') echo 'selected="selected"'; }?>   >per hr.</option>
                            <option value="2"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='2') echo 'selected="selected"'; }?> >per half day</option>
                            <option value="3"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='3') echo 'selected="selected"';} ?> >per day</option>
                            <option value="4" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='4') echo 'selected="selected"';} ?> >per night</option>
                            <option value="5" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='5') echo 'selected="selected"';} ?> >per week</option>
                            <option value="6" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='6') echo 'selected="selected"'; }?> >per month</option>
                            <option value="7" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='7') echo 'selected="selected"';} ?> >per ticket</option>
                            <option value="8"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='8') echo 'selected="selected"'; }?>>per time</option>
                            <option value="9" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='9') echo 'selected="selected"';} ?> >per trip</option>
                            <option value="10"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='10') echo 'selected="selected"';} ?>>per unit</option>
                        </select>
                        <span class="form_error span12"><?php echo form_error('room_allotment'); ?></span>
                      </div>


                      <div class="span3">
                       <input class="span12" type="text" value="<?php if(!empty($pr_info->rent)){  echo $pr_info->rent; } ?>"  placeholder="How Many" name="rent">
                        <span class="form_error span12"><?php echo form_error('rent'); ?></span>
                      </div>
                     

                      <div class="span3"> 
                        <input class="span12"  value="<?php if(!empty($properties->deposit_amount)){ echo $properties->deposit_amount;}?>" type="text" placeholder="security Deposit" name="deposit">
                      <span class="form_error span12"><?php echo form_error('deposit'); ?></span>
                      </div>
                    
                    </div>
                </div>
           </div>
           <br>
           <?php $category=get_collection_category();  ?>
            <div class="control-group">
                <label class="control-label">
                  Collection category
                </label>
                <div class="controls controls-row span8">
                 <select name="collection_category">
                      <option value="">Select Category</option>
                      <?php foreach ($category as $row): ?>
                      <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->pr_collect_cat_id)){ if($pr_info->pr_collect_cat_id == $row->id)echo"selected"; }  ?> > <?php echo $row->category_name; ?> </option>
                      <?php endforeach ?>
                  </select>
                  <span class="form_error "><?php echo form_error('collection_category'); ?></span>
                </div>

            </div> 

          <!--   <br>
            <br> -->

            <div class="control-group">
                <label class="control-label">
                  Featured listing
                </label>
                <div class="controls controls-row span8" style="margin-top:0.55%">
                  <?php if(($properties->featured_listing_status)=="1"):?> 
                  <div class="unique_feature"> <input type="checkbox" name="featured_listing"  value="1" checked></div>
                <?php else: ?>
                  <div class="unique_feature"> <input type="checkbox" name="featured_listing"  value="1"></div>
                 <?php endif; ?>
                </div>

            </div> 

<!--             <br>
            <br> -->
            <div class="control-group">
                <label class="control-label">
                  Amenity
                </label>
                <div class="controls controls-row span8">
                 <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                  <div class="unique_feature"> <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>" <?php if(!empty($pr_amenities)){
                   foreach ($pr_amenities as $chok1){ 
                   if($chok1->pr_amenity_id == $row->id) echo "checked"; 
                    } }?> > <?php echo  $row->name; ?> 
                 </div>
                 <?php } ?>
                </div>

            </div> 
           <!--  <br>
            <br> -->

            

              <?php $usr =  get_user_row($properties->user_id); ?>
              <div class="control-group" style="display:none">
                <label class="control-label" >
                  Users Email
                </label>
                <div class="controls controls-row span8">
                   <input class="span12" id="user" name="user_email" type="text" value="<?php if(!empty($pr_contact_info->contact_email)){  echo $pr_contact_info->contact_email; }else{ echo $usr->user_email; } ?>"  placeholder="User Email">
                 
                  <span class="form_error span12"><?php  echo form_error('user_email'); ?></span>
                </div>
              </div>


            <div class="control-group" style="display:none">
                <label class="control-label">
                  Contact information for this listing
                </label>
                <div class="controls controls-row span8">

                  <div class="row-fluid">
                  <div class="span8">
                   <input  class="input-block-level span12" id="contact_name" value="<?php if(!empty($pr_contact_info->contact_name)){ echo $pr_contact_info->contact_name;}else{ echo $usr->first_name; } ?>" type="text" placeholder="Contact Name" name="contact_name">
                  <span class="form_error span12"><?php echo form_error('contact_name'); ?></span>
                  </div>

                  <div class="span4">
                    <input  class="input-block-level span12"  id="phone" value="<?php if(!empty($pr_contact_info->phone)){ echo $pr_contact_info->phone;}else{ echo $usr->phone; } ?>"  type="text" placeholder="Phone" name="phone">
                  <span class="form_error span12"><?php echo form_error('phone'); ?></span>
                  </div>
                
                </div>
              </div>
            </div>
              <div class="control-group" style="display:none">
                 <label class="control-label">
                    &nbsp;
                </label>
              <div class="controls controls-row span8" style="margin-left:10px;">

               <div class="row-fluid">
                   <input  type="hidden" value="<?php if(!empty($properties->user_id)){ echo $properties->user_id;}?>" placeholder="Contact Email" id="user_id" name="user_id">

                
                    <div class="span4 ">
                        <input class="input-block-level span12" type="text" id="confirm_email" value="<?php if(!empty($pr_contact_info->contact_email)){ echo $pr_contact_info->contact_email;}else{ echo $usr->user_email; } ?>"  placeholder="Confirm Email" name="confirm_email">
                        <span class="form_error span12"><?php echo form_error('confirm_email'); ?></span>
                    </div>
                    <div class="span4">
                        <input class="input-block-level span12" type="text" value="<?php if(!empty($pr_contact_info->website)){ echo $pr_contact_info->website;}?>"  placeholder="Website" name="website">
                        <span class="form_error span12"><?php echo form_error('website'); ?></span>
                    </div>
              </div>
            </div>
          </div>
           
            <div class="control-group">
                <label class="control-label">
                 Property Image
                </label>
                <div class="controls controls-row span8">
                  <div class="span8" id="jquery-wrapped-fine-uploader" style="border:1px solid #999;min-height:150px;"> 
                      DRAG AND DROP
                  </div> 

                </div>
                  <div id="triggerUpload" class="btn btn-primary" style="margin-top: 10px; margin-left:-9.5%">
                      Upload now
                  </div>  

            </div> 


             <div class="control-group">
                <label class="control-label">
                 Property Image
                </label>
                <div class="controls controls-row span8">
                  <ul class="thumbnails">
                    <li class="span4">
                      <a href="#" class="thumbnail">
                        <img data-src="holder.js/360x270" alt="360x270" style="width: 360px; height: 270px;" src="<?php echo base_url() ?>assets/uploads/property/<?php echo $properties->featured_image; ?>">
                      </a>
                    </li>
                  <?php if(!empty($pr_gallery)) foreach ($pr_gallery as $row){?>
                    <li class="span3">
                      <a href="#" class="thumbnail">
                        <img data-src="holder.js/260x120" alt="260x120" style="width: 260px; height: 120px;" src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->image_file; ?>">
                      </a>
                    </li>
                  <?php } ?>


                  </ul>
                
                </div> 
             </div>   

             <div class="control-group">
                <label class="control-label">
                &nbsp;
                </label>
                <div class="controls controls-row span8">
                  <ul class="thumbnails" id="thumbnails">
                   

                  </ul>
               </div>
                </div>

            </div> 

            <div class="form-actions no-margin">
              <button type="submit" onclick="return checkbox_valid();" class="btn btn-info">
                Save
              </button>
            </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>


 
  <?php $users = get_all_user();?>
  <script type="text/javascript">
  $(function(){
      var user = new Array();
      <?php foreach($users as $row){ ?>
              user.push('<?php echo $row->user_email; ?>');
      <?php } ?>
    $( "#user" ).autocomplete({
        lookup: user,
        onSelect: function(suggestion){
           var user_email = document.getElementById('user').value; 
           $.post( "<?php echo base_url()?>subadmin/user_info",{'user_email':user_email })
                  .done(function( data ) {
                 if(data !='No Record Found'){  
                    var user =   $.parseJSON(data);
                    document.getElementById('confirm_email').value = user['user_email'];
                    document.getElementById('contact_name').value = user['first_name']+' '+user['last_name'];
                    document.getElementById('phone').value = user['phone'];
                    document.getElementById('user_id').value = user['id'];
                  }
                  else{
                    alert("No Record Found");
                  }
          });
        }
     });
  });  
  </script>
  <script>
      jQuery(document).ready(function (){
      
        var resp='';
      var uploade ='';    
        
   uploade=jQuery('#jquery-wrapped-fine-uploader').fineUploader({
                request: {
                  endpoint: '<?php echo base_url() ?>upload_handler/fineupload', 
                }, validation: {
                  allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
                },
                autoUpload: false
               
              }).on('complete', function(event, id, fileName, responseJSON) {
                

                if(responseJSON.success){

                  jQuery(this).append('<input type="hidden" name="property_image[]" value="'+fileName+'">');
                   resp ='<li>'+
                   '<input type="radio" name="featured_image" value="'+fileName+'"> <span>Featured Image</span>' +
                          '<a href="#" class="thumbnail">'+
                            '<img data-src="<?php echo base_url() ?>holder.js/160x120" alt="160x120" style="width: 160px; height: 120px;" src="<?php echo base_url();?>assets/uploads/property/'+fileName+'">'+
                          '</a>'+
                         '</li>';

                  $('#thumbnails').append(resp);
                }
              });
          
          $('#triggerUpload').click(function() {
              uploade.fineUploader('uploadStoredFiles');
          });
    


        //  jQuery('#jquery-wrapped-fine-uploader2').fineUploader({
        //   request: {
        //     endpoint: '<?php echo base_url() ?>upload_handler/fineupload', 
        //   }, validation: {
        //     allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
        //     itemLimit: 1
        //   }
        // }).on('complete', function(event, id, fileName, responseJSON) {
        //   if (responseJSON.success) {
        //     jQuery(this).append('<input type="hidden" name="footprint_image" value="'+fileName+'">');
        //   }
        // });

      });
    </script>  
    <style type="text/css">
       .control-group{
        margin-bottom: 0px !important;
       }

       .qq-upload-button{
        background-color: #e05284;
      }
    </style>