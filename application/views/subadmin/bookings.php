<?php //echo get_permonthbookings('06'); ?>

<?php $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id())); ?>

<div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Booking Chart
          </div>
        </div>
        <div class="widget-body">
          <div id="column_chart"></div>
        </div>
      </div>
    </div>
  </div>
<?php if($this->session->flashdata('success_msg')): ?>
    <div class="alert alert-success">
      <?php echo $this->session->flashdata('success_msg');?>
    </div>
  <?php endif ?>
  <?php if($this->session->flashdata('error_msg')): ?>
    <div class="alert alert-error">
      <?php echo $this->session->flashdata('error_msg');?>
    </div>
  <?php endif ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">

          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> <a href="<?php echo base_url() ?>subadmin/bookings"> BOOKINGS </a>                   

          </div> 
        <?php if($subadmin_restrictions->booking!=1):  ?>
          <div class="pull-right">
            <a href="<?php echo base_url();?>subadmin/add_booking" class="btn">Add Booking</a>                      
            <a href="<?php echo base_url();?>subadmin/booking_email_records" class="btn">Booking Email History</a>                      
          </div>
         <?php endif; ?>
        </div>
        <div class="widget-body">
          <div class="row" style="margin-left:0%;">
          <div class="span2">
              <?php $attributes = array('name'=>'myForm'); echo form_open(base_url().'subadmin/bookings',$attributes); ?>
                <select onchange="return form_submit();"  class="span10" id="sort" name="sort"> 
                  <option value="">Select To Sort</option>
                  <option value="Pending_Bookings">Pending Bookings</option>
                  <option value="Pending_Check-in">Pending Check-in</option>
                  <option value="Pending_Check-out">Pending Check-out</option>
                  <option value="Cancelled_Bookings">Cancelled Bookings</option>
                  <option value="completed_Bookings">Completed Bookings</option>
                </select>
              <?php echo form_close(); ?>    
          </div>
          </div>


    
          
          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <div class="row" style="margin-left:0%;">
           <h4>Search Criteria</h4><br>
            <?php  $attributes = array('name'=>'search_form', 'class'=>'form-inline'); echo form_open(current_url(),$attributes);?>
            <div class="criteria div span12 row">
              <div class="span3" style="float:left">
                   <input class="input-medium" name='book_number'  placeholder="Booking Number" type="text">
              </div>

              <div class="span3" style="float:left;">
                   <input class="input-medium" name='guest_name'  placeholder="Guest name" type="text">
              </div>

              <div class="input-append span3" style="float:left;">
                   <input class="input-medium" name='start_check'  placeholder="Check In" type="text" id="start_check">
                    <span class="add-on">
                       <i class="icon-calendar"></i>
                    </span>
              </div>

              <div class="input-append span3" style="float:left;">
                   <input class="input-medium" name='end_check'  placeholder="Check Out" type="text" id="end_check">
                    <span class="add-on">
                       <i class="icon-calendar"></i>
                    </span>
              </div>
              
              <div class="span3" style="float:left; margin-left: 0; margin-top:10px">
                   <select style="width:106%" class="input-large" name="status_typo">
                     <option selected="selected" value="">Type</option>
                     <option value="pending">Pending</option>
                     <option value="completed">Completed</option>
                   </select>
              </div>             
                <div class="span1" style="float:left; margin-top:10px">
                  <button class="btn" id="search" onclick="return form_submit_name();" type="submit">Search</button>
                </div>
                <div class="span3" style="float:left; margin-top:10px">
                  <button class="btn" href="<?php echo base_url() ?>subadmin/bookings" >Show All</button>
                </div>                          
          </div>

            <?php echo form_close(); ?>
          </div>

          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->

  <div style="" class="row-fluid">
      <h4>Export Properties</h4><br>
      <?php echo form_open(base_url().'export/export_bookings'); ?>

        <div class="row-fluid">
          <input type="hidden" name="type" value="subadmin">
            <div class="controls">
                <div class="input-append">
                    <input id="date_range1" class="span2 date_picker" type="text" placeholder="Select Date" name="report_range2">
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select  name="export_sort"> 
                    <option value="">All</option>
                    <option value="Pending_Bookings">Pending Bookings</option>
                    <option value="Pending_Check-in">Pending Check-in</option>
                    <option value="Pending_Check-out">Pending Check-out</option>
                    <option value="Cancelled_Bookings">Cancelled Bookings</option>
                  </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
          <span class="inline radio"><input type="radio" name="export_file_format" value="csv">CSV</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" value="excel">Excel</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</span>
          <br><br>
          <span ><input type="submit" value="Export" class="btn-info btn">
        </div>  
                  <?php echo form_close();?>        
             </div>

          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

            <th style="width:5%;">Booking Number</th>
            <th style="width:8%;">Guest Name</th>
            <th style="width:10%;">Listing</th>
            <th style="width:10%;">Check In </th>
            <th style="width:10%;">Check Out </th>
            <th style="width:8%;">Host Name</th>
            <th style="width:6%">Status</th>
            <th style="width:5%;">Created</th>

            <th style="width:5%;" class="hidden-phone">Total Amount</th>
        <?php if($subadmin_restrictions->booking!=1):  ?>
                  <th style="width:60%;" class="hidden-phone">Actions</th>
         <?php endif; ?>
                </tr>
              </thead>
              <tbody>
               <?php if(!empty($bookings)):?> 

               <?php  foreach ($bookings as $row): ?>
               <?php $pr_detail = property_details($row->pr_id);  ?>                      
                <tr class="gradeA">

                 <?php if(empty($row->transaction_id)): ?>
                  <td><a  href="javascript:void(0)" onclick="alert('Receipt is not available because Host has not done Payment Yet !')"><?php echo $row->id;?></a></td>
                 <?php else: ?>
                  <td><a target="_blank" href="<?php echo base_url()?>subadmin/booking_detail/<?php echo  $row->id;?>"><?php echo $row->id;?></a></td>
                 <?php endif; ?>
                  <?php $customer = get_user_info($row->customer_id);?>
                  <td><?php echo $customer->first_name." ".$customer->last_name; ?></td>
                  <td><a target="_blank" href="<?php echo base_url() ?>properties/details/<?php echo $pr_detail->id; ?>"> <?php echo $pr_detail->unit_street_address.' '.$pr_detail->city; ?></a></td>
                  <td><?php echo $row->check_in;?></td>
                  <td><?php echo $row->check_out;?></td>
                  <td> 
                    <a href="<?php echo base_url()?>subadmin/host_profile/<?php echo  $pr_detail->us_id; ?>">
                    <?php $owner = get_user_info($row->owner_id);?>
                      <?php echo $owner->first_name." ".$owner->last_name; ?>
                    </a></td>
                  <td>
                    <?php if($row->status==0){?>
                      <a href="javascript:void(0)" class="btn btn-warning btn-small hidden-phone" data-original-title="">Pending</a>
                    <?php }else if($row->status==1){?>
                      <a href="javascript:void(0)" class="btn btn-info btn-small hidden-phone" data-original-title="">Complete</a>
                    <?php }else if($row->status==2){?>
                      <a href="javascript:void(0)" class="btn btn-danger btn-small hidden-phone"  data-placement="top" data-original-title="Do you want to active it?" data-original-title="">Cancelled</a>
                    <?php }elseif($row->status==3){?>                    
                      <a href="javascript:void(0)" class="btn btn-success btn-small hidden-phone" data-original-title="">Approved</a>

                    <?php }?>
                  </td>
                  <td><?php echo $row->created;?></td>


 
                  <td class="hidden-phone"><?php echo '$ '.convertCurrency($row->total_amount,'USD',$row->booking_currency_type);?></td>
                   <!-- <td> <a href="<?php //echo base_url();?>subadmin/add_booking_note/<?php echo $row->id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a></td> -->
                 <?php if($subadmin_restrictions->booking!=1):  ?>
                
                  <td class="hidden-phone">
                      <?php $colors = get_buttons_color();  ?>
                 
                 
                    <a href="<?php echo base_url();?>subadmin/booking_notes/<?php echo $row->id; ?>" class="<?php echo $colors->notes_btn ?>">Notes</a>
                    <a href="<?php echo base_url();?>subadmin/booking_email/<?php echo $row->id; ?>"  class="<?php echo $colors->email_btn ?>" data-original-title="">Email</a>
                    <a href="<?php echo base_url();?>properties/details/<?php echo $pr_detail->id; ?>" target="_blank" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">
                      View Listing
                    </a>                     
                    <?php if($row->status!=2){?>
                     <?php if($row->status!=1){?>
                    <a href="<?php echo base_url()?>subadmin/cancel_booking/<?php echo $row->id; ?>" onclick="return confirm('Are you want to cancel it?');" class="btn btn-small btn-primary hidden-tablet hidden-phone">
                     Cancel
                    </a>
                    <?php } }?>

                    <a href="<?php echo base_url()?>subadmin/delete_booking/<?php echo $row->id; ?>" onclick="return confirm('Are you want to delete?');" class="<?php echo $colors->delete_btn ?>">
                     Delete
                   </a>
                  </td>
                  <?php endif; ?>  
                </tr>
             <?php endforeach ?>
           <?php else: ?>
            <tr><td colspan="10">
            No records Found
            </td></tr>
             <?php  endif ?>
            </tbody>
            </table>
              <div id="data-table_info" class="dataTables_info"></div>
                <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">
                   <?php if($pagination) echo $pagination; ?>
                   <!--  <a id="data-table_first" class="first paginate_button paginate_button_disabled" tabindex="0">First</a>
                      <a id="data-table_previous" class="previous paginate_button paginate_button_disabled" tabindex="0">Previous</a>
                       <span>
                          <a class="paginate_active" tabindex="0">1</a>
                          <a class="paginate_button" tabindex="0">2</a>
                      </span>
                      <a id="data-table_next" class="next paginate_button" tabindex="0">Next</a>
                    <a id="data-table_last" class="last paginate_button" tabindex="0">Last</a> -->
                </div>
              </div>
            <div class="clearfix"></div>
           
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
       function form_submit(){
          var sort = document.getElementById('sort'); 
            if(sort != ''){
              $("form[name='myForm']").submit();
            }
            else{
              return false;
            }        
        }
function form_submit_name(){
var name  = $('input:text[name=name]').val();
if(name == '')
{
  alert('Please Enter some value');
  return false;
}
else
{
 return true;
}        
}
    </script>


<script type="text/javascript">



//     function drawChart3() {
//   var data = google.visualization.arrayToDataTable([
//     // ['Year', 'Visits', 'Orders', 'Income', 'Expenses'],
//     ['Month', 'Visits', 'Orders', 'Income', 'Expenses'],
//     ['Jan', 300, 800, 900, 300],
//     ['Feb', 1170, 860, 1220, 564],
//     ['Mar', 260, 1120, 2870, 2340],
//     ['Apr', 1030, 540, 3430, 1200],
//     ['May', 200, 700, 1700, 770],
//     ['Jul', 1170, 2160, 3920, 800],
//     ['Aug', 1170, 2160, 3920, 800],
//     ['Sep', 1170, 2160, 3920, 800],
//     ['Oct', 1170, 2160, 3920, 800],
//     ['Nov', 1170, 2160, 3920, 800],
//     ['Dec', 2170, 1160, 2820, 500] ])

//   var options = {
//     width: 'auto',
//     height: '160',
//     backgroundColor: 'transparent',
//     colors: ['#3eb157', '#3660aa', '#d14836', '#dba26b', '#666666', '#f26645'],
//     tooltip: {
//       textStyle: {
//         color: '#666666',
//         fontSize: 11
//       },
//       showColorCode: true
//     },
//     legend: {
//       textStyle: {
//         color: 'black',
//         fontSize: 12
//       }
//     },
//     chartArea: {
//       left: 60,
//       top: 10,
//       height: '80%'
//     },
//   };

//   var chart = new google.visualization.ColumnChart(document.getElementById('ncolumn_chart'));
//   chart.draw(data, options);
// }
</script>


<script type="text/javascript">
      google.load("visualization", '1', {packages:['corechart']});
          google.setOnLoadCallback(drawChart3);
        function drawChart3() {
          var janbooking = "<?php echo get_permonthbookings('01') ?>";
          var febbooking = "<?php echo get_permonthbookings('02') ?>";
          var marbooking = "<?php echo get_permonthbookings('03') ?>";
          var aprbooking = "<?php echo get_permonthbookings('04') ?>";
          var maybooking = "<?php echo get_permonthbookings('05') ?>";
          var junbooking = "<?php echo get_permonthbookings('06') ?>";
          var julbooking = "<?php echo get_permonthbookings('07') ?>";
          var augbooking = "<?php echo get_permonthbookings('08') ?>";
          var sepbooking = "<?php echo get_permonthbookings('09') ?>";
          var octbooking = "<?php echo get_permonthbookings('10') ?>";
          var novbooking = "<?php echo get_permonthbookings('11') ?>";
          var decbooking = "<?php echo get_permonthbookings('12') ?>";

          var janbookingp = "<?php echo get_monbookingspending('01') ?>";
          var febbookingp = "<?php echo get_monbookingspending('02') ?>";
          var marbookingp = "<?php echo get_monbookingspending('03') ?>";
          var aprbookingp = "<?php echo get_monbookingspending('04') ?>";
          var maybookingp = "<?php echo get_monbookingspending('05') ?>";
          var junbookingp = "<?php echo get_monbookingspending('06') ?>";
          var julbookingp = "<?php echo get_monbookingspending('07') ?>";
          var augbookingp = "<?php echo get_monbookingspending('08') ?>";
          var sepbookingp = "<?php echo get_monbookingspending('09') ?>";
          var octbookingp = "<?php echo get_monbookingspending('10') ?>";
          var novbookingp = "<?php echo get_monbookingspending('11') ?>";
          var decbookingp = "<?php echo get_monbookingspending('12') ?>";

          var janbookingc = "<?php echo get_monbookingscancel('01') ?>";
          var febbookingc = "<?php echo get_monbookingscancel('02') ?>";
          var marbookingc = "<?php echo get_monbookingscancel('03') ?>";
          var aprbookingc = "<?php echo get_monbookingscancel('04') ?>";
          var maybookingc = "<?php echo get_monbookingscancel('05') ?>";
          var junbookingc = "<?php echo get_monbookingscancel('06') ?>";
          var julbookingc = "<?php echo get_monbookingscancel('07') ?>";
          var augbookingc = "<?php echo get_monbookingscancel('08') ?>";
          var sepbookingc = "<?php echo get_monbookingscancel('09') ?>";
          var octbookingc = "<?php echo get_monbookingscancel('10') ?>";
          var novbookingc = "<?php echo get_monbookingscancel('11') ?>";
          var decbookingc = "<?php echo get_monbookingscancel('12') ?>";     


          // alert(parseInt(marbooking));

          var data = google.visualization.arrayToDataTable([
         ['Month', 'Bookings', 'Pendings', 'Cancel Bookings'],
    ['Jan', Number(janbooking), Number(janbookingp), Number(janbookingc)],
    ['Feb', Number(febbooking), Number(febbookingp), Number(febbookingc)],
    ['Mar', Number(marbooking), Number(marbookingp), Number(marbookingc)],
    ['Apr', Number(aprbooking), Number(aprbookingp), Number(aprbookingc)],
    ['May', Number(maybooking), Number(maybookingp), Number(maybookingc)],
    ['jun', Number(junbooking), Number(junbookingp), Number(junbookingc)],
    ['Jul', Number(julbooking), Number(julbookingp), Number(julbookingc)],
    ['Aug', Number(augbooking), Number(augbookingp), Number(augbookingc)],
    ['Sep', Number(sepbooking), Number(sepbookingp), Number(sepbookingc)],
    ['Oct', Number(octbooking), Number(octbookingp), Number(octbookingc)],
    ['Nov', Number(novbooking), Number(novbookingp), Number(novbookingc)],
    ['Dec', Number(decbooking), Number(decbookingp), Number(decbookingc)] ]);

    //  ['Month', 'Bookings', 'Pendings', 'Cancel', 'Expenses'],
    // ['Jan', 300, 800, 900, 300],
    // ['Feb', 1170, 860, 1220, 564],
    // ['Mar', 260, 1120, 2870, 2340],
    // ['Apr', 1030, 540, 3430, 1200],
    // ['May', 200, 700, 1700, 770],
    // ['Jul', 1170, 2160, 3920, 800],
    // ['Aug', 1170, 2160, 3920, 800],
    // ['Sep', 1170, 2160, 3920, 800],
    // ['Oct', 1170, 2160, 3920, 800],
    // ['Nov', 1170, 2160, 3920, 800],
    // ['Dec', 2170, 1160, 2820, 500] ]);

          var options = {
            width: 'auto',
            height: '160',
            backgroundColor: 'transparent',
            colors: ['#3eb157', '#3660aa', '#d14836', '#dba26b', '#666666', '#f26645'],
            tooltip: {
              textStyle: {
                color: '#666666',
                fontSize: 11
              },
              showColorCode: true
            },
            legend: {
              textStyle: {
                color: 'black',
                fontSize: 12
              }
            },
            chartArea: {
              left: 60,
              top: 10,
              height: '80%'
            },
          };

          var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'));
          chart.draw(data, options);
        }


    </script>


<script type="text/javascript">
     var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
     
    var checkin = $('#start_check').datepicker({
    onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1);
    checkout.setValue(newDate);
    }
    checkin.hide();
    $('#end_check')[0].focus();
    }).data('datepicker');
    var checkout = $('#end_check').datepicker({
    onRender: function(date) {
    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    checkout.hide();
    }).data('datepicker');
</script>
<style type="text/css">
  .criteria .span3 input{
    width: 100%;
  }

  .input-append.span3 input{
    width: 85%;
  }
</style>