<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Button Colors
</div>
</div>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_buttons_color(); ?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Delete Button
    </label>
    <div class="controls controls-row span6">
        <select name="delete" class="span4">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->delete_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->delete_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->delete_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->delete_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->delete_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->delete_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
       <span class="form_error span6"><?php echo form_error('delete'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Edit Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="edit" class="span4">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->edit_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->edit_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->edit_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->edit_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->edit_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->edit_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_error span6"><?php echo form_error('edit'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Email Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="email">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->email_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->email_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->email_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->email_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->email_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->email_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_form_error span6"><?php  echo form_error('email'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Notes Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="notes">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->notes_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->notes_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->notes_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->notes_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->notes_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->notes_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_form_error span6"><?php  echo form_error('notes'); ?></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Change password Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="change_password">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->change_password_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->change_password_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->change_password_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->change_password_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->change_password_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->change_password_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_form_error span6"><?php  echo form_error('change_password'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Details Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="details">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->details_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->details_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->details_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->details_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->details_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->details_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_form_error span6"><?php  echo form_error('details'); ?></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Refund Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="refund">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->refund_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->refund_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->refund_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->refund_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->refund_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->refund_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_form_error span6"><?php  echo form_error('refund'); ?></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Pay to host Button
    </label>
    <div class="controls controls-row span6">
        <select class="span4" name="pay_to_host">
            <option value="btn btn-success btn-small hidden-phone" <?php if($row->pay_to_host_btn=="btn btn-success btn-small hidden-phone") echo "selected"; ?> >green</option>
            <option value="btn btn-warning btn-small hidden-phone"  <?php if($row->pay_to_host_btn=="btn btn-warning btn-small hidden-phone") echo "selected"; ?> >orange</option>
            <option value="btn btn-small btn-primary hidden-tablet hidden-phone"  <?php if($row->pay_to_host_btn=="btn btn-small btn-primary hidden-tablet hidden-phone") echo "selected"; ?> >black</option>
            <option value="btn btn-small"  <?php if($row->pay_to_host_btn=="btn btn-small") echo "selected"; ?> >white</option>
            <option value="btn btn-info btn-small hidden-phone"  <?php if($row->pay_to_host_btn=="btn btn-info btn-small hidden-phone") echo "selected"; ?> >blue</option>
            <option value="btn btn-small btn-danger hidden-tablet hidden-phone"  <?php if($row->pay_to_host_btn=="btn btn-small btn-danger hidden-tablet hidden-phone") echo "selected"; ?> >red</option>
        </select>
        <span class="form_form_error span6"><?php  echo form_error('pay_to_host'); ?></span>
    </div>
</div>



<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

