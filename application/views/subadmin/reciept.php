
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Booking Details
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php $pro_info = get_property_detail($booking->pr_id); 
              $user = get_user_info($pro_info->user_id);  ?>
            <div class="form-horizontal no-margin well" style="color:#737373; " >
              <div class="row">
                <div class="">
                  <div class="navbar itinenarybtn">
                    <div class="navbar-inner">
                      <!-- <a class="brand" href="#">Title</a> -->
                      <ul class="nav" style="text-align: center !important;">
                        <center>
                          <li style="padding-left:0%" class=""><i class=" icon-white icon-envelope"></i> &nbsp;&nbsp;<a href="<?php echo base_url() ?>subadmin/booking_detail_email/<?php echo $booking->id ?>/receipt_page"><b>Email Itinenary</b></a></li>
                          <li class=""><i class=" icon-white icon-print"></i>&nbsp; &nbsp;<a href="<?php echo base_url() ?>export/export_booking_receipt/<?php echo $booking->id ?>"><b>Print Itinenary</b></a></li>
                          <li class=""><i class="icon-list-alt"></i>&nbsp;&nbsp;<a href="#"><b>View Reciept</b></a></li>                        
                        </center> 
                      </ul>
                    </div>
                  </div>
                  <!-- <a href="" class="btn btn-large">Email Itinenary</a> -->
                </div>
              </div>
                <div class="row">
                  <div class=" " style="margin-left:1.5%">
                        <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">                       
                        
<!--                          <div class="span3 pull-right" style="text-align:right">
                            <h3>Add Your Logo</h3>
                            <p>21 Park Street, Newyork NY USA<br/>
                            Wed, March 19, 2013</p>
                         </div>
 -->                        
              <?php $customer = get_user_info($booking->customer_id); ?>
              <?php $owner = get_user_info($booking->owner_id); ?>
              <?php 
                $startTimeStamp = strtotime($booking->check_in);
                $endTimeStamp = strtotime($booking->check_out);
                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                // and you might want to convert to integer
                $numberDays = intval($numberDays);
                // $interval = date_diff($startTimeStamp, $endTimeStamp);
               ?>

                          <h3 >
                            Customer Reciept                            
                          </h3>
                          <h4>Booking Number - #<?php echo $booking->id; ?></h4>
                           <table class="table table-bordered">
                            <th style="width:25%">NAME</th>
                            <TH style="width:25%">DESTINATION</TH>
                            <th style="width:25%">DURATION</th>
                            <th style="width:25%">ACCOMMODATION TYPE</th>
                            <tr>
                              <td>
                                Dear, <?php echo ucfirst($customer->first_name." ".$customer->last_name); ?>
                              </td>
                              <td><?php echo $pro_info->city ?></td>
                              <td><?php echo  $numberDays ?> Nights</td>
                              <td><?php echo get_property_type($pro_info->property_type_id) ?></td>
                            </tr>
                           </table>

                           <table class="table table-bordered">
                            <th style="width:25%">ACCOMMODATION ADDRESS</th>
                            <th style="width:25%">ACCOMMODATION HOST</th>                            
                            <th style="width:25%">CHECK IN</th>
                            <th style="width:25%">CHECK OUT</th>                            
                            <tr>
                              <td><?php echo $pro_info->unit_street_address." ".$pro_info->city ?></td>
                              <td><?php echo $owner->first_name.' '.$owner->last_name ?><br>+<?php echo $owner->phone ?></td>
                              <td><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?> <br> Flexible checkin time</td>
                              <td>  <?php echo date('D, F d,Y', strtotime($booking->check_out)) ?> <br> Flexible checkout time</td>                              
                            </tr>
                           </table>
                      </div>

                      <div class="span12" style=" margin-left:0; padding:4%">
                        <h3>Payment Details </h3>
                        <?php 
                            $currency = $this->session->userdata('currency');
                            if (!empty($booking->total_amount)):
                              $accomodation_fee = convertCurrency($booking->total_amount, $currency);
                              $service_fee      = convertCurrency(5, $currency);
                              if(!empty($booking->due_amount)){
                              $due_fee          = convertCurrency($booking->due_amount, $currency);
                              }
                              else{
                              $due_fee="";                         
                              }
 
                           ?>

                        <table class="table table-bordered">
                          <tr>
                            <td>ACCOMODATIONS</td>
                            <td><?php echo $accomodation_fee." ".$currency ?></td>
                          </tr>
                          <tr>
                            <td>SERVICE FEE</td>
                            <td><?php echo $service_fee." ".$currency ?></td>
                          </tr>

                          <tr>
                            <td>Total</td>
                            <td><?php echo $total = $accomodation_fee + $service_fee ?> <?php echo $currency  ?></td>
                          </tr>

                          <tr>
                            <td>PAID AMOUNT</td>
                            <td><?php echo $total-$due_fee ?> <?php echo $currency  ?></td>
                          </tr>

                        </table>
                       <?php endif; ?>

                      <p style="">Bnbclone is authorized to accept Accomodation Fee on behalf of the host as its limited agent. this means that your payment obligation to the host is satisfied by your payment to Bnbclone. Any disagreements by the host regarding that payment must be settled between the host and Bnbclone</p>
                      </div>  
                    </div>
                  </div>
                </div>

              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    margin-left: 1%;
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#737373;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>


