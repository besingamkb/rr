<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
.form-horizontal .control-label
{
  width:175px ;
}
</style>

 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Property
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('id' => 'testr' , 'class' => 'form-horizontal no-margin well' ,'onsubmit'=>'return CheckForm()')); ?>
              
             <div class="control-group">
                <label class="control-label">
                User Name
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                     <?php $users = get_all_users(); ?>
                    <div class="span10">
                      <select  required name="user_id">
                          <option value="">Select User</option>
                          <?php foreach ($users as $row): ?>
                             <option value="<?php echo $row->id; ?>"> <?php echo $row->first_name." ".$row->last_name; ?> </option>
                          <?php endforeach ?>
                      </select>
                      <span><?php echo form_error('user_id'); ?></span>
                    </div>
                    <div class="span4">
                    </div>
                </div>
              </div>
            </div> 


              <div class="control-group">
                <label class="control-label" for="your-name">
                 Property Title
                </label>
                <div class="controls controls-row span8">
                  <input name="title" class="input-block-level" type="text" placeholder="Title" required value="<?php echo set_value('title'); ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                Property Description
                </label>
                <div class="controls controls-row span8">
                 <textarea rows="5" id="description" required class="span12" name="description" ></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>  
                </div>
              </div>

               <div class="control-group">
                <label class="control-label">
                  Details about this property
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <?php $property_type = get_property_types(); ?> 
                    <div class="span4">
                      <select name="property_type" required>
                        <option value="">Property Type</option>
                      <?php foreach ($property_type as $row): ?>
                      <option value="<?php echo $row->id; ?>"> <?php echo $row->property_type; ?> </option>
                      <?php endforeach ?>
                      </select>                      
                      <span class="form_error span12"><?php echo form_error('property_type'); ?></span>
                    </div>

                  <div class="span4">
                     <select name="bedrooms" required>
                       <option value="">Bedrooms</option>
                        <option >1</option>
                        <option >2</option>
                        <option >3</option>
                        <option >4</option>
                        <option >5</option>
                        <option >6</option>
                        <option >7</option>
                        <option >8</option>
                        <option >9</option>
                        <option >10</option>
                     </select>
                     <span class="form_error span12"><?php echo form_error('bedrooms'); ?></span>
                  </div>

                  <div class="span3">
                    <select name="accomodates" required>
                        <option value="">Accomodates</option>
                        <option >1</option>
                        <option >2</option>
                        <option >3</option>
                        <option >4</option>
                        <option >5</option>
                        <option >6</option>
                        <option >7</option>
                        <option >8</option>
                        <option >9</option>
                        <option >10</option>
                        <option >11</option>
                        <option >12</option>
                        <option >13</option>
                        <option >14</option>
                        <option >15</option>
                        <option >16+</option>
                    </select>
                    <span class="form_error span12"><?php echo form_error('accommodates'); ?></span> 
                  </div> 
                </div>
              </div>
            </div>

             <div class="control-group">
                <label class="control-label">
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                     <?php $room_type = get_room_type(); ?>
                    <div class="span4">
                      <select  name="room_type" required>
                        <option value="">Room Type</option>
                        <?php foreach ($room_type as $row): ?>
                        <option value="<?php echo $row->id; ?>"> <?php echo $row->room_type; ?> </option>
                        <?php endforeach ?>
                      </select>
                      <span><?php echo form_error('room_type'); ?></span>
                    </div>
                    <div class="span4">
                    </div>
                </div>
              </div>
            </div> 


             <div class="control-group">
                <label class="control-label">
                Price
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span10">
                      <select  style="float:left" name="price_type">
                          <option value="AUD">AUD</option>
                          <option value="BRL">BRL</option>
                          <option value="CAD">CAD</option>
                          <option value="CHF">CAD</option>
                          <option value="CZK">CZK</option>
                          <option value="DKK">DKK</option>
                          <option value="EUR">EUR</option>
                          <option value="HKD">HKD</option>
                          <option value="HUF">HUF</option>
                          <option value="JPY">JPY</option>
                          <option value="MYR">MYR</option>
                          <option value="MXN">MXN</option>
                          <option value="NOK">NOK</option>
                          <option value="NZD">NZD</option>
                          <option value="PLN">PLN</option>
                          <option value="RUB">RUB</option>
                          <option value="SGD">SGD</option>
                          <option value="SEK">SEK</option>
                          <option value="USD">USD</option>
                      </select>
                          <input type="text" class="span2"  maxlength="20" name="price_amount" size="35" style="float:left;margin-left:10px;" required="required" placeholder="Amount">                                            
                      <span><?php echo form_error('price_amount'); ?></span>
                    </div>
                </div>
              </div>
            </div> 

            
             <div class="control-group">
                <label class="control-label">
                Size
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span12">

                      <input type="text" class="span2"  maxlength="20" name="size" size="35" style="float:left;" required="required" placeholder="Size">                                            
                      <select  name="size_mode" style="float:left;margin-left:10px;" >
                          <option selected="">Sq foot</option>
                          <option>Sqm.</option>
                      </select>
                      <span><?php echo form_error('room_type'); ?></span>
                    </div>
                    <div class="span4">
                    </div>
                </div>
              </div>
            </div> 


             <div class="control-group">
                <label class="control-label">
                Image
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span12">
                      <input type="file" name="property_profile" class="span6"  required="required" >                                            
                    </div>
                    <div class="span4">
                    </div>
                </div>
              </div>
            </div> 


         <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->
            <style type="text/css">
/*             a{
               text-decoration: none !important;
              }
              .GPS_type{
               display: none;
               }
              .postal_type{
               display: none;
              }
*/            </style>
             <script>
            // $(document).ready(function(){

            //         $('.postal_type').show();
            //          $('.GPS_type').hide();
            //          $('.pin_point_type').hide();
            //          $('#post_type').css('color','#DE4980');
            //          $('#gps_co').css('color','#5885AC');
            //          $('#pin_co').css('color','#5885AC');

            //    $('#post_type').click(function(){
            //          $('.postal_type').show();
            //          $('.GPS_type').hide();
            //          $('.pin_point_type').hide();
            //          $('#post_type').css('color','#DE4980');
            //          $('#gps_co').css('color','#5885AC');
            //          $('#pin_co').css('color','#5885AC');
            //    });
            //    $('#gps_co').click(function(){
            //          $('.GPS_type').show();
            //          $('.postal_type').hide();
            //          $('.pin_point_type').hide();
            //          $('#gps_co').css('color','#DE4980');
            //          $('#post_type').css('color','#5885AC');
            //          $('#pin_co').css('color','#5885AC');
            //    });
            //    $('#pin_co').click(function(){
            //          $('.pin_point_type').show();
            //          $('.GPS_type').hide();
            //          $('.postal_type').hide();
            //          $('#pin_co').css('color','#DE4980');
            //          $('#post_type').css('color','#5885AC');
            //          $('#gps_co').css('color','#5885AC');
            //    });
            // });
            </script>


<!--              <div class="control-group">
                <label class="control-label">
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span12">
                          <div class="active" style="width:33%; float:left;">
                              <a href="javascript:void(0)" id="post_type" style="background-color:#F6F6F6 !important">Postal Type</a>
                          </div>
                          <div style="width:33%; float:left;">
                              <a href="javascript:void(0)" id="gps_co">GPS Co-ordinate</a>
                          </div>
                          <div style="width:33%; float:left;">
                              <a href="javascript:void(0)" id="pin_co" style="color:#DE4980" onclick="get_map_size()">Pin Point On Map</a>
                          </div>
                          <div style="width:33%; float:left;">
                          </div>
                    </div>
                </div>
              </div>
            </div> 
 -->
         <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->

            <br>

            <div class="postal_type">

             <div class="control-group">
                <label class="control-label">
                  &nbsp;
                </label>
                <div class="controls controls-row span8">
                    <input required name="address" id="address" class="input-block-level" type="text" placeholder="Property Address" value="<?php echo set_value('address'); ?>">                 
                    <span class="form_error span12"><?php  echo form_error('address');  ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  &nbsp;
                </label>

                <div class="controls controls-row span8">
                  <div class="row-fluid">
                  <div class="span4">
                    <input type="text" class="input-block-level" required onkeyup="get_neighbour_city()" placeholder="City" name="city">
                    <span><?php echo form_error('city'); ?></span> 
                  </div>

<script type="text/javascript">
      function get_neighbour_city(){ 
       var city_name = $('input[name=city]').val();
        if(city_name!="")
        {
         $.ajax
         ({
              type:"post",
              url:"<?php echo base_url() ?>subadmin/get_neighbour_city",
              data:{city_name:city_name},
              success:function(res)
           {
              if(res!="")
                {
                get_neighbourhoot(res);
                }
             else
               {
               $('#neighbourhoot').html("<option>No Neighborhoods Found</option>")
               }
           }

        });
      }
        else
        {
           return false; 
        }
    }

    function get_neighbourhoot(city_id)
    {
      var city_id = city_id ;
      $.ajax
      ({
            type:'POST',
             url:'<?php echo base_url(); ?>subadmin/get_neighbourhoot_of_city',
            data:{city:city_id},
         success:function(res)
         {
            $('#neighbourhoot').html(res);
         }
      });
    }


</script>

                <div class="span4">
                    <select name="neighbourhood[]" id="neighbourhoot"  class="span12" multiple="multiuple">
                        <option>Select neighbour</option>
                    </select>
                    <span><?php echo form_error('neighbourhood'); ?></span>
                </div>

                  <?php $country = get_country_array(); ?>
                  <div class="span4">
                    <select required class="span12" id="country" name="country">
                        <option  value="">Select country</option>
                        <?php foreach ($country as $code => $name): ?>
                        <option value="<?php echo $name; ?>"> <?php echo $name; ?> </option>
                        <?php endforeach ?>
                    </select>
                    <span class="form_error "><?php echo form_error('country'); ?></span>
                  </div>
                </div>
                </div>
              </div>

              <br>
              <div class="control-group">
                <label class="control-label">
                    &nbsp;
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                  <div class="span6">
                     <input required class="input-block-level" style="width:80%" name="state" id="state" type="text" placeholder="State" value="<?php echo set_value('state'); ?>">
                      <span class="form_error"><?php echo form_error('state'); ?></span>
                  </div>
                  <div class="span6">
                    <input style="width:80%;margin-left:20%" class="input-block-level" required type="text" placeholder="zip" name="zipcode">
                    <span class="form_error span12"><?php echo form_error('zipcode'); ?></span>
                  </div>
                </div>
              </div>
            </div> 

            </div>



            <input  type="hidden" placeholder="latitude"  id="latitude"  name="latitude">
            <input  type="hidden" placeholder="longitude" id="longitude" name="longitude">

          
            
            <div class="form-actions no-margin">
              <button type="submit" id="button"  class="btn btn-info">
             Save
              </button>
            </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">


      function CheckForm()
      {
        if(Check_description()===false )
        {
          return false;
        }
      }

      function Check_description()
      {
          var desc = $('#description').val();
            var count = desc.split(' ').length;
             if(count<15)
             {
                alert("The property description must be greater then 15 Words");
                return false;
             }
      }

  </script>