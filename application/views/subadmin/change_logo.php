
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Change Logo
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Logo name
                </label>
                <div class="controls controls-row span6">
                  <input name="logo_name" class="span12" type="text" placeholder="Logo Name" value="<?php echo set_value('logo_name'); ?>">
                  <span class="form_error span12"><?php echo form_error('logo_name'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Choose Logo

                </label>
                <span>(image size 250X100 and format .png)</span> 
                <div class="controls controls-row span6">

                  <input name="logo" class="span12" type="file">
                  <span class="form_error span12"><?php if($this->session->flashdata('image_error')) echo $this->session->flashdata('image_error');  ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>