
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span>  Customers
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_customer_to_group/<?php echo $group_id ?>"> Add Customer To The Group </a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:13%">Image</th>
                  <th style="width:20%">Customer Name</th>
                  <th style="width:20%">Customer Email</th>
                  <th style="width:20%">Created</th>
                  <th style="width:20%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($members)): ?>
                    <?php $i=1; foreach ($members as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><img style="width:80%;" src="<?php echo base_url()?>assets/uploads/profile_image/<?php if(!empty($row->image)) echo $row->image;else echo '52f8ec2778663.jpg'; ?>"></td>
                        <td><?php echo $row->first_name." ".$row->last_name; ?></td>
                        <td><?php echo $row->user_email ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)) ?></td>
                        
                        <td>
                          <a href="<?php echo base_url()?>subadmin/view_customer_profile/<?php echo $row->member_id;?>/<?php echo $group_id ?>"  class="btn btn-success btn-small hidden-phone" data-original-title="">View</a>
                          <a href="<?php echo base_url()?>subadmin/delete_customer_member/<?php echo $row->id;?>/<?php echo $group_id ?>" onclick="return confirm('Do you want to delete?' );" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">Delete</a>
                        </td>
                     </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"><?php echo $pagination ?></div>
          </div>
        </div>
      </div>
    </div>