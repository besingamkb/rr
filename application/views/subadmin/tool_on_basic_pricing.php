<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Tool Tip on Basic Pricing
</div>
  <a style="float:right;margin-right:3%" href="<?php echo base_url() ?>subadmin/settings" class="btn btn-default">Back</a>
</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_row('tool_tip',array('tool_tip_page'=>'tool_on_basic_pricing')); ?>

  <?php if(!empty($row->page_data)): ?>
  <?php $row = json_decode($row->page_data); ?>
  <?php endif; ?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Currency Type
    </label>
    <div class="controls controls-row span6">
          <input name="currency_type" class="span12" type="text" placeholder="Currency Type" value="<?php if(!empty($row->currency_type)) echo $row->currency_type ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Nightly
    </label>
    <div class="controls controls-row span6">
          <input name="nightly" class="span12" type="text" placeholder="Nightly" value="<?php if(!empty($row->nightly)) echo $row->nightly ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Weekly
    </label>
    <div class="controls controls-row span6">
          <input name="weekly" class="span12" type="text" placeholder="Weekly" value="<?php if(!empty($row->weekly)) echo $row->weekly ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Monthly
    </label>
    <div class="controls controls-row span6">
          <input name="monthly" class="span12" type="text" placeholder="Monthly" value="<?php if(!empty($row->monthly)) echo $row->monthly ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Cleaning Fee
    </label>
    <div class="controls controls-row span6">
          <input name="cleaning_fee" class="span12" type="text" placeholder="Cleaning Fee" value="<?php if(!empty($row->cleaning_fee)) echo $row->cleaning_fee ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Cancellation Policy
    </label>
    <div class="controls controls-row span6">
          <input name="policy" class="span12" type="text" placeholder="Cancellation Policy" value="<?php if(!empty($row->policy)) echo $row->policy ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Minimum Stay
    </label>
    <div class="controls controls-row span6">
          <input name="minimum_stay" class="span12" type="text" placeholder="Minimum Stay" value="<?php if(!empty($row->minimum_stay)) echo $row->minimum_stay ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Maximum Stay
    </label>
    <div class="controls controls-row span6">
          <input name="maximum_stay" class="span12" type="text" placeholder="Maximum Stay" value="<?php if(!empty($row->maximum_stay)) echo $row->maximum_stay ; ?>">
    </div>
</div>





<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

