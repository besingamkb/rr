<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Home page Content
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Header left section
                </label>
                <div class="controls controls-row span6">
                  <input name="header_left" class="span12" type="text" placeholder="" value="<?php  if(!empty($site_content->header_left)) echo $site_content->header_left; else echo set_value('header_left'); ?>">
                  <span class="form_error span12"><?php echo form_error('header_left'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Header Right section
                </label>
                <div class="controls controls-row span6">
                  <input name="header_right" class="span12" type="text" placeholder="" value="<?php  if(!empty($site_content->header_right)) echo $site_content->header_right; else echo set_value('header_right'); ?>">
                  <span class="form_error span12"><?php echo form_error('header_right'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Column 1 Heading
                </label>
                <div class="controls controls-row span6">
                  <input name="col1_heading" class="span12" type="text" placeholder="" value="<?php  if(!empty($site_content->col1_heading)) echo $site_content->col1_heading; else echo set_value('col1_heading'); ?>">
                  <span class="form_error span12"><?php echo form_error('col1_heading'); ?></span>
                </div>
              </div>

               <div class="control-group">
                <label class="control-label" for="your-name">
                  Column 1 Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="col1_description" class="span12" type="text" placeholder="" ><?php  if(!empty($site_content->col1_description)) echo $site_content->col1_description; else echo set_value('col1_description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('col1_description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Column 1 Image

                </label>
                <!--<span>(image size 250X100)</span> -->
                <div class="controls controls-row span6">
                  <input name="col1_image" class="span12" type="file">
                  <span class="form_error span12"><?php if($this->session->flashdata('col1_image_error')) echo $this->session->flashdata('col1_image_error');  ?></span>
                </div>
                <?php if (!empty($site_content->col1_image)): ?>
                  <img src="<?php echo base_url() ?>assets/uploads/content/<?php echo $site_content->col1_image ?>" width="100">
                <?php endif ?>
              </div>

               <div class="control-group">
                <label class="control-label" for="your-name">
                  Column 2 Heading
                </label>
                <div class="controls controls-row span6">
                  <input name="col2_heading" class="span12" type="text" placeholder="" value="<?php  if(!empty($site_content->col2_heading)) echo $site_content->col2_heading; else echo set_value('col2_heading'); ?>">
                  <span class="form_error span12"><?php echo form_error('col2_heading'); ?></span>
                </div>
              </div>

               <div class="control-group">
                <label class="control-label" for="your-name">
                  Column 2 Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="col2_description" class="span12" type="text" placeholder=""><?php  if(!empty($site_content->col2_description)) echo $site_content->col2_description; else echo set_value('col2_description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('col2_description'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label">
                  Column 2 Image

                </label>
                <!--<span>(image size 250X100)</span> -->
                <div class="controls controls-row span6">
                  <input name="col2_image" class="span12" type="file">
                  <span class="form_error span12"><?php if($this->session->flashdata('col2_image_error')) echo $this->session->flashdata('col2_image_error');  ?></span>
                </div>
                <?php if (!empty($site_content->col2_image)): ?>
                  <img src="<?php echo base_url() ?>assets/uploads/content/<?php echo $site_content->col2_image ?>" width="100">
                <?php endif ?>
              </div>


               <div class="control-group">
                <label class="control-label" for="your-name">
                  Column 3 Heading
                </label>
                <div class="controls controls-row span6">
                  <input name="col3_heading" class="span12" type="text" placeholder="" value="<?php  if(!empty($site_content->col3_heading)) echo $site_content->col3_heading; else echo set_value('col3_heading'); ?>">
                  <span class="form_error span12"><?php echo form_error('col3_heading'); ?></span>
                </div>
              </div>

               <div class="control-group">
                <label class="control-label" for="your-name">
                  Column 3 Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="col3_description" class="span12" type="text" placeholder="" ><?php  if(!empty($site_content->col3_description)) echo $site_content->col3_description; else echo set_value('col3_description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('col3_description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Column 3 Image

                </label>
                <!--<span>(image size 250X100)</span> -->
                <div class="controls controls-row span6">
                  <input name="col3_image" class="span12" type="file">
                  <span class="form_error span12"><?php if($this->session->flashdata('col3_image_error')) echo $this->session->flashdata('col3_image_error');  ?></span>
                </div>
                <?php if (!empty($site_content->col3_image)): ?>
                  <img src="<?php echo base_url() ?>assets/uploads/content/<?php echo $site_content->col3_image ?>" width="100">
                <?php endif ?>
              </div>
              

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>