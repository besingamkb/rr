<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span> Add Booking
</div>
</div>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well','id'=>'boking_form','onsubmit'=>'return check_form_submission()')); ?>
<?php $guests = get_users();?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Guest Name
    </label>
    <div class="controls controls-row span6">
        <input  name="guest" id="guest" type="hidden"required>
            <select onchange="return get_guest_info();" id="guest_name" name="guest_name" class="span12 input-left-top-margins">
                <option value="">Select Guest Name</option>
                <?php foreach ($guests as $row): ?>
                    <option value="<?php echo $row->id; ?>" <?php if(set_value('guest_name') == $row->id ) echo "selected"; ?>    ><?php echo $row->first_name.' '.$row->last_name; ?></option>
                <?php endforeach; ?>
        </select>
        <span class="form_error span12"><?php echo form_error('guest_name'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Guest Email
    </label>
    <div class="controls controls-row span6">
        <input name="guest_email" id="email" class="span12" type="text" placeholder="Email"  value="<?php echo set_value('guest_email'); ?>"required>
       <span class="form_error span12"><?php echo form_error('guest_email'); ?></span>
    </div>
</div>

<!-- <div style="display:none" class="control-group">
    <label class="control-label" for="your-name">
        Guest Contact No.
    </label>
    <div class="controls controls-row span6">
        <input name="phone" class="span12" id="phone" type="text" placeholder="Contact Number" value="<?php echo set_value('phone');  ?>">
        <span class="form_error span12"><?php //echo form_error('phone'); ?></span>
    </div>
</div>

<div style="display:none" class="control-group">
    <label class="control-label" for="your-name">
        Guest Full Address
    </label>
    <div class="controls controls-row span6">
        <input name="address" value="<?php //echo set_value('address'); ?>" class="span12" id="address" type="text" placeholder="Address">
        <span class="form_form_error span12"><?php  //echo form_error('address'); ?></span>
    </div>
</div>
 -->
<?php $users = get_all_user();?>
<div class="control-group">
    <label class="control-label" >
        Host Email
    </label>

    <div class="controls controls-row span6">
        <select  id="host_email" name="host_email" class="span12 input-left-top-margins">
            <option value="">Select Host</option>
            <?php foreach ($users as $row): ?>
                <option value="<?php echo $row->user_email; ?>" <?php if(set_value('host_email') == $row->user_email ) echo "selected"; ?>    ><?php echo $row->first_name.' '.$row->last_name; ?></option>
            <?php endforeach; ?>
        </select>
        <!-- <input name="host_email" value="<?php  echo set_value('host_email'); ?>"  class="span12" id="host_email" type="text" placeholder="Host Email" required> -->
        <span class="form_error span12"><?php  echo form_error('host_email'); ?></span>
    </div>
</div>

<div class="control-group" style="margin-bottom:0px">
    <label class="control-label" for="your-name">
        Property Titles 
    </label>
    <div id="user_properties" class="controls controls-row span6" style="padding-top:6px">

    </div>
    <span class="form_error span12"><?php echo form_error('property_id'); ?></span>
</div>

<br>

<div class="control-group" id="clr_fee" style="display:none">
    <label class="control-label">
        Cleaning Fee
    </label>
    <div class="controls controls-row span6">
        <input  class="span12" id="cleaning_fee" type="text" placeholder="Cleaning Fee" readonly   >
        <span class="form_error span12"></span>
    </div>
</div>

<div class="control-group" id="sec_fee" style="display:none">
    <label class="control-label">
        Security Deposit
    </label>
    <div class="controls controls-row span6">
        <input  class="span12" name="security_deposit" id="security_deposit" type="text" placeholder="Security Deposit" readonly  >
        <span class="form_error span12"></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label">
        Check In 
    </label>
    <div  class="controls controls-row span7">
        <div class="input-append">
            <input id="start" onchange = "effective_fees()" class="span12" type="text" placeholder="Select Date" name="check_in" value="<?php  echo set_value('check_in'); ?>" required>
            <span class="add-on">
                <i class="icon-calendar"></i>
            </span>
        </div>
    <span class="form_error span12"><?php echo form_error('check_in'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label">
        Check Out 
    </label>
    <div  class="controls controls-row span7">
        <div class="input-append">
            <input id="end" onchange = "effective_fees()" class="span12" type="text" placeholder="Select Date" name="check_out" value="<?php  echo set_value('check_out'); ?>" required>
            <span class="add-on">
                <i class="icon-calendar"></i>
            </span>
        </div>
    <span class="form_error span12"><?php echo form_error('check_out'); ?></span>
    </div>
</div>



<div class="control-group">
    <label class="control-label">
        Tax
    </label>
    <div class="controls controls-row span6">
        <input  class="span12" id="tax" type="text" placeholder="Tax" readonly  required>
        <span class="form_error span12"></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label">
        Commision Fee
    </label>
    <div class="controls controls-row span6">
        <input  class="span12" id="commision" type="text" placeholder="Commision Fee" readonly  required>
        <span class="form_error span12"></span>
    </div>
</div>

        <input   id="property_id" type="hidden">
        <input   id="guest_email" type="hidden">
        <input   id="h_email" type="hidden">

<div class="control-group">
    <label class="control-label">
        Per Night
    </label>
    <div class="controls controls-row span6">
        <input  class="span12" id="per_night" type="text" placeholder="Per Night" readonly  required>
        <span class="form_error span12"></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label">
        Total Amount
    </label>
    <div class="controls controls-row span6">
        <input id="subtotal" name="total_amount" class="span12" type="text" placeholder="Total Amount"  readonly >
        <span class="form_error span12"><?php echo form_error('total_amount'); ?></span>
    </div>
</div>

<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
function get_property_detail(pr_id)
    {
    $.ajax({
          type:'post',
           url:'<?php echo base_url() ?>subadmin/get_property_detail/'+pr_id,
           success:function(res)
           {
              if(res)
              {
                var info  = $.parseJSON(res);
                $('#property_id').val(pr_id);
                    if(info.security_deposit!=0)
                    {
                      $('#sec_fee').show();
                      $('#security_deposit').val(info.security_deposit);
                    }
                    else
                    {
                      $('#sec_fee').hide();
                    }

                    if(info.cleaning_fee!=0)
                    {
                      $('#clr_fee').show();
                      $('#cleaning_fee').val(info.cleaning_fee);
                    }
                    else
                    {
                      $('#clr_fee').hide();
                    }
               effective_fees();
              }
           }
          });
    }
</script>


<script type="text/javascript">
function effective_fees()
  {
 
    var pr_id = $("#property_id").val();
    var check_in   = $('#start').val();
    var check_out  = $('#end').val();

   var c_in = Date.parse(check_in) / 1000;
   var c_out = Date.parse(check_out) / 1000;
   var diffrence = c_out-c_in;
   var days = diffrence/86400+1;

    // alert(pr_id);
    // alert(check_in);
    // alert(check_out);

      if(check_out!="" && check_in!="" && pr_id!="")
        {
            $.ajax({
                    type: 'POST',
                    data:{check_in:check_in, check_out:check_out, pr_id:pr_id},
                    url: '<?php echo base_url(); ?>subadmin/get_effective_booking_rate',
                    success: function(res)
                    {
                      var response = parseInt(res);
                        if(isNaN(response))
                        { 
                            alert(res);
                            $('#tax').val("");
                            $('#commision').val("");
                            $('#subtotal').val("");
                            $('#per_night').val("");
                            return false;
                        }
                        else
                        {
                            total = response;

                            <?php $taxs = get_taxs(); ?>
                            var tax_percent = <?php echo $taxs->tax ?>;
                            var tax_amount = (response/100)*tax_percent;
                            total = total + tax_amount;

                            <?php $commision_fee = get_commision_fee(); ?>
                            var commision_percent = <?php echo $commision_fee->commision_fee ?>;
                            var commision_amount = (response/100)*commision_percent;
                            total = total + commision_amount;



                            $('#tax').val(Math.round(tax_amount));
                            $('#commision').val(Math.round(commision_amount));
                            $('#subtotal').val(Math.round(total));


                            var c_in = Date.parse(check_in) / 1000;
                            var c_out = Date.parse(check_out) / 1000;
                            var diffrence = c_out-c_in;
                            var days = diffrence/86400+1;

                            var per_night = total/days;

                            $('#per_night').val(Math.round(per_night));

                        }
                    }
              });
        }
  }  



  function check_guest_host_same()
  { 
    var guest_email = $("#guest_email").val();
    var host_email = $("#h_email").val();
    if(guest_email==host_email)
    {
      alert("Guest and Host cannot be Same !");
      return false;        
    }
  }

  function check_form_submission()
  {
    if(effective_fees()===false || check_guest_host_same()===false)
    {
        return false;
    }
  }

</script>


<script type="text/javascript">
    function get_guest_info() 
    {
        var guest_id = document.getElementById('guest_name').value;
        $.post( "<?php echo base_url()?>subadmin/user_information",{'user_id':guest_id })
        .done(function( data ) {

        var user_info = $.parseJSON(data);
        document.getElementById('guest').value = user_info['first_name']+' '+user_info['last_name'];
        document.getElementById('email').value = user_info['user_email'];
        document.getElementById('guest_email').value = user_info['user_email'];
        // document.getElementById('phone').value = user_info['phone'];
        // document.getElementById('address').value = user_info['address']+' '+user_info['city']+' '+user_info['state'];

        });          
    }
</script>

<script>
    $(function(){
        var user = new Array();
        <?php foreach($users as $row){ ?>
            user.push('<?php echo $row->user_email; ?>');
        <?php } ?>

        $('#host_email').change(function(){
            var host_email = $(this).val();
            if(host_email == ""){
                $('#user_properties').html('');
                // return false;
            }else{
                $('#h_email').val(host_email);                
                $.post( "<?php echo base_url()?>subadmin/user_property",{'user_email':host_email })
                    .done(function( data ) {
                    $('#user_properties').html(data);
                    });
            }
        });

        // $( "#host_email" ).autocomplete({
        //     lookup: user,
        //     onSelect: function(suggestion)
        //         {
        //         $('#user_properties').html('');
        //         var host_email = document.getElementById('host_email').value; 
        //         $.post( "<?php echo base_url()?>subadmin/user_property",{'user_email':host_email })
        //             .done(function( data ) {
        //             $('#user_properties').append(data);
        //             });
        //         }
        // });
    });  
</script>


<script type="text/javascript">
     var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
     
    var checkin = $('#start').datepicker({
    onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1);
    checkout.setValue(newDate);
    }
    checkin.hide();
    $('#end')[0].focus();
    }).data('datepicker');
    var checkout = $('#end').datepicker({
    onRender: function(date) {
    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    checkout.hide();
    }).data('datepicker');
</script>

<script type="text/javascript">



    // function run_ajax(){
    //     chkin = $('#start').val();
    //     chkout = $('#end').val();
    //     pr_id = $('input[name=property_id]').val();
    //     // alert(chkin+chkout+pr_id);
    //     $.ajax({
    //           type:'post',
    //           data:{checkin:chkin,checkout:chkout},
    //            url:'<?php echo base_url() ?>subadmin/get_due_amount/'+pr_id,
    //                success:function(res){
    //                    $('input[name=total_amount]').val(res);
    //                     // return false;

    //                     $("#boking_form").attr('onsubmit' , 'return true;');
    //                     $("#boking_form").submit();
    //                }
    //           });
    // }

</script>