      <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id())); ?>

 <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Pages
          </div>
        <?php if($subadmin_restrictions->cms!=1):  ?>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_page"> Add Pages </a>
          </div>
         <?php endif; ?>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:20%">Page Title</th>
                  <th style="width:15%">Preview</th>
        <?php if($subadmin_restrictions->cms!=1):  ?>
                  <th style="width:15%">Page Status</th>
         <?php endif; ?>
                  <th style="width:15%">Created</th>
        <?php if($subadmin_restrictions->cms!=1):  ?>
                  <th style="width:30%">Actions</th>
          <?php endif; ?>        
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($pages)): ?>
                    <?php $i=1; foreach ($pages as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                      
                        <td><?php echo ucfirst($row->page_title); ?></td>
                        <td><a href="<?php echo base_url()."page/preview/".$row->slug; ?>" target="_blank"  class="btn btn-small hidden-phone">View </a></td>
        <?php if($subadmin_restrictions->cms!=1):  ?>
                        <td>
                          <?php if($row->status==0){?>
                              <a href="<?php echo base_url() ?>subadmin/publish_page/<?php echo $row->id;?>" class="btn btn-warning btn-small hidden-phone" data-original-title="">Unpublished</a>
                          <?php }else{?>
                              <a href="<?php echo base_url() ?>subadmin/publish_page/<?php echo $row->id;?>" class="btn btn-info btn-small hidden-phone" data-original-title="">published</a>
                          <?php }?>
                        </td>
                <?php endif; ?>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>

                    <?php if($subadmin_restrictions->cms!=1):  ?>
                        <td>
                          <?php $colors = get_buttons_color();  ?>
                          <?php if(!empty($colors)): ?>
                          <a href="<?php echo base_url()?>subadmin/delete_page/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>subadmin/edit_page/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                        </td>
                      <?php endif; ?>
                      <?php endif; ?>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>