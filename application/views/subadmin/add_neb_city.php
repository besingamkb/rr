 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Neighbourhood City
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  City
                </label>
                <div class="controls controls-row span8">
                  <input name="city" class="span12" type="text" placeholder="City" value="<?php echo set_value('city'); ?>">
                  <span class="form_error span12"><?php echo form_error('city'); ?></span>
                </div>
              </div>



              <div class="control-group">
                <label class="control-label">
                City Description
                </label>
                <div class="wysiwyg-container controls controls-row span8">
                  <textarea name="city_description" class="input-block-level no-margin" placeholder="City Description" style="height: 140px"><?php echo set_value('city_description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('city_description'); ?></span>
                </div>
              </div>

             <div class="control-group">
                <label class="control-label">
                Known For
                </label>
                <div class="wysiwyg-container controls controls-row span8">
                  <textarea name="known_for" class="input-block-level no-margin" placeholder="Known For" style="height: 140px"><?php echo set_value('known_for'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('known_for'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Get Around With
                </label>
                <div class="controls controls-row span8">
                  <input name="get_around_with"  class="span12" type="text" placeholder="Get Around With" value="<?php echo set_value('get_around_with'); ?>">
                  <span class="form_error span12"><?php echo form_error('get_around_with'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  City Image
                </label>
                <span>(Resolution: 1425x500)</span> 
                <div class="controls controls-row span6">

                  <input name="userfile1" class="span12" type="file">
                  <span class="form_error span12"><?php echo form_error('userfile1'); ?></span>
                </div>
              </div>

             <div class="control-group">
                <label class="control-label">
                  Thumbnail Image
                </label>
                <div class="controls controls-row span6">

                  <input name="userfile2" class="span12" type="file">
                  <span class="form_error span12"><?php echo form_error('userfile2'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Is Home?
                </label>
                <div class="controls controls-row span8">
                  <select name="is_home" class="span4">
                        <option value="0" <?php echo set_select('is_home', 0); ?> >No</option>
                        <option value="1" <?php echo set_select('is_home', 1); ?> >Yes</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('is_home'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>