 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Edit Room Allotment
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Room Allotment
                </label>
                <div class="controls controls-row span6">
                  <input name="room_allotment" class="span12" type="text" placeholder="Bed Type" value="<?php echo $room_allotment->room_allotment; ?>">
                  <span class="form_error span12"><?php echo form_error('room_allotment'); ?></span>
                </div>
              </div>

            

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>