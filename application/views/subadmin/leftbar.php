<style type="text/css">
  #mainnav > ul > li .current-arrow
  {
    right: -24px;
  }
</style>


<?php $subadmin_restrictions = get_row('subadmin_restrictions',array('subadmin_id'=>get_subadmin_id())); ?>


<div id="mainnav" class="hidden-phone hidden-tablet">
      <ul>
           <?php if(uri_string() == 'subadmin/dashboard'): ?>
        <li class="active">
              <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>

          <a href="<?php echo base_url(); ?>subadmin/dashboard">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
            </div>
            Dashboard
          </a>
        </li>

           <?php if($subadmin_restrictions->timeline!=2): ?>

               <?php if(uri_string() == 'subadmin/timeline'): ?>
              <li class="active">
                  <span class="current-arrow"></span>
                  <?php else: ?>
                    <li>
                  <?php endif; ?>
                <a href="<?php echo base_url(); ?>subadmin/timeline">
                  <div class="icon">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                  </div>
                  Timeline
                </a>
              </li>

         <?php endif; ?>     

           <?php if($subadmin_restrictions->city!=2): ?>

         <?php if(uri_string() == 'subadmin/cities'): ?>
        <li class="active">
            <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/cities">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0ce;"></span>
            </div>
            Cities
          </a>
        </li>

         <?php endif; ?>
           <?php if($subadmin_restrictions->neighbourhood!=2): ?>

          <?php if(uri_string() == 'subadmin/neighbourhoods'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>subadmin/neighbourhoods">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe062;"></span>
            </div>
            Neighborhoods
          </a>
        </li>

         <?php endif; ?>
           <?php if($subadmin_restrictions->booking!=2): ?>

        <?php if(uri_string() == 'subadmin/bookings'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>subadmin/bookings">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Bookings
          </a>
        </li>
           
           <?php endif; ?>
           <?php if($subadmin_restrictions->properties!=2): ?>

          <?php if(uri_string() == 'subadmin/properties'): ?>
        <li class="active">
            <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/properties">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Properties
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->payments!=2): ?>

          <?php if(uri_string() == 'subadmin/payments'): ?>
        <li class="active">
             <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/payments">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Payments
          </a>
        </li>


           <?php endif; ?>
           <?php if($subadmin_restrictions->member!=2): ?>

        

          <?php if(uri_string() == 'subadmin/members'): ?>
        

        <li style="display:none;"><!-- style="display:none;" -->
          <span class="current-arrow"></span>
          <?php else: ?>
            <li style="display:none;">
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/members">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Members
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->cms!=2): ?>

          <?php if(uri_string() == 'subadmin/pages'): ?>
          <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/pages">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            CMS
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->policies!=2): ?>


          <?php if(uri_string() == 'subadmin/cancellation_policies'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/cancellation_policies">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Cancellation Policies
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->collection!=2): ?>


          <?php if(uri_string() == 'subadmin/collection'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/collection">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Collections
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->groups!=2): ?>


          <?php if(uri_string() == 'subadmin/groups'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/groups">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Groups
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->featured_images!=2): ?>


          <?php if(uri_string() == 'subadmin/featured_images'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/featured_images">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="▓"></span>
            </div>
            Featured Images
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->notifications!=2): ?>


          <?php if(uri_string() == 'subadmin/notifications'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/notifications">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Notifications
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->help!=2): ?>


          <?php if(uri_string() == 'subadmin/help'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>subadmin/help">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="?"></span>
            </div>
           Help
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->review!=2): ?>


          <?php if(uri_string() == 'subadmin/reviews'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/reviews">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Reviews
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->message!=2): ?>

          <?php if(uri_string() == 'subadmin/messages'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/messages">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Messages
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->manage_email!=2): ?>

          <?php if(uri_string() == 'subadmin/manageEmail'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/manageEmail">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Manage Email
          </a>
        </li>

           <?php endif; ?>
           <?php if($subadmin_restrictions->setting!=2): ?>

          <?php if(uri_string() == 'subadmin/settings'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>subadmin/settings">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Settings
          </a>
        </li>

           <?php endif; ?>

      </ul>
    </div>
    
    <div class="dashboard-wrapper">
      <div class="main-container">
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/timeline">Timeline</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/cities">Cities</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/neighbourhoods">Neighborhood</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/bookings">Bookings</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/properties">Properties</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/payments">Payments</a>
                  </li>
                  <li style="display:none;">
                    <a href="<?php echo base_url(); ?>subadmin/members">Members</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/pages">CMS</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/cancellation_policies">Cancellation Policies</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/collection">Collection</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/groups">Group</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/featured_images">Fetured Images</a>
                  </li> 
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/notifications">Notifications</a>
                  </li> 
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/help">Help</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/reviews">Reviews</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/messages">Message</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/manageEmail">Manage Email</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>subadmin/settings">Settings</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>


