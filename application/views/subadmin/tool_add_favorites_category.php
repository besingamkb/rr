<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Tool Tip on Add Favorites Category
</div>
  <a style="float:right;margin-right:3%" href="<?php echo base_url() ?>subadmin/settings" class="btn btn-default">Back</a>
</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_row('tool_tip',array('tool_tip_page'=>'tool_add_favorites_category')); ?>
  
  <?php if(!empty($row->page_data)): ?>
  <?php $row = json_decode($row->page_data); ?>
  <?php endif; ?>

<div class="control-group">
    <label class="control-label" for="your-name">
         Name
    </label>
    <div class="controls controls-row span6">
          <input name="name" class="span12" type="text" placeholder="Name" value="<?php if(!empty($row->name)) echo $row->name ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
          Image
    </label>
    <div class="controls controls-row span6">
          <input name="image" class="span12" type="text" placeholder="Image" value="<?php if(!empty($row->image)) echo $row->image ; ?>">
    </div>
</div>




<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

