 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> View Payment Note
            </div>
          </div>
          <div class="widget-body">
              <div class="form-horizontal no-margin well">

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Title
                </label>
                <div class="controls controls-row span6">
                  <span ><?php echo $notes->title;?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Description
                </label>
                <div class="controls controls-row span6" style="padding-top:5px !important">
                <span ><?php echo $notes->description;?></span>
                </div> 
              </div>

              <style type="text/css">
               .controls-row.span6
               {
                  padding-top:5px !important;
                  font-size: 14px !important;
               }
              </style>

           
              
              <div class="form-actions no-margin">
                <a href="<?php echo base_url()?>subadmin/security_deposit_notes/<?php echo $notes->payment_id;?>" class="btn btn-info">
                 Back To Notes
                </a>
              </div>
        
           </div>


          </div>
        </div>
      </div>
    </div>
  </div>