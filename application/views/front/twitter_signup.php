<style>
#myform input{width:100%; font-size: 16px; height:30px; margin-bottom:0px;}
#myform div{background-color:#B3ABA9; padding:5px; border-radius:3px;}
</style>

<div class="container" style="min-height:500px; margin-left:28%;width:100%" > 
    <?php if($this->session->flashdata('msg_success')){ ?>
        <div class="span4" align="center" style="margin-top:20px;">
            <span style="padding:5px !important; " class="alert alert-success"><?php echo $this->session->flashdata('msg_success'); ?></span>
        </div>
    <?php } ?>
    
    <?php if($this->session->flashdata('error_msg')){ ?>
        <div class="span4" align="center" style="margin-top:20px;">
            <span style="padding:5px !important; " class="alert alert-danger"><?php echo $this->session->flashdata('error_msg'); ?></span>
        </div>
    <?php } ?>
    <br><br><br>
    <div class="span4" id="myform" style="width:35%">    
        <?php echo form_open(current_url()); ?>
            <div align="center" style="background-color:#ffffff; padding:10px; color:#E05284; font-size:20px;">
                Please Enter Your valid Email 
            </div>
            <div style="color:#ACABAE; background-color:#ffffff;" >
              <b> we will send you a link to your Email inbox for account activation.</b>
            </div>            
            <div>
                <input type="text" name="email" value="<?php echo set_value('email'); ?>" id="forget_password_email" placeholder="Email address" autofocus>
            </div>
            <span style="color:#E05284;" id="forget_password_email_test"><?php echo form_error('email'); ?></span><br>
          
            <div style="background-color:#ffffff;">
            <button onclick="return check_fields();" class="pull-right btn btn-lg btn-primary">Submit Here</button>
            </div>
        <?php echo form_close(); ?> 
    </div>
</div>



<script type="text/javascript">
    function check_fields(){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var email = $('#forget_password_email').val();
        if(email==''){
          $('#forget_password_email_test').html('Please enter the email first');
          setTimeout(function(){
            $('#forget_password_email_test').html('');
          },3000);
          return false;
        }else if(reg.test(email)==false){
          $('#forget_password_email_test').html('Please enter A valid email Addresss..');
          setTimeout(function(){
            $('#forget_password_email_test').html('');
          },3000);
          return false;
        }
    }
</script>
