<!-- start the content -->
<div class="container" style="min-height:450px; color:#666">

  
  <!-- <div style="text-align:center">
      <img style="width:100%" src="<?php echo base_url() ?>assets/img/contact-us-banner.png">
  </div> -->
    <div  style="width:86%; margin-left:8%"> 
      <h3>Contact Us</h3>   
      <?php if ($this->session->flashdata('success_msg')): ?>
        <p style="color:green">Successfully Submitted</p>
      <?php endif ?>
      <div class="span4" style="margin-left:0px">
       <?php echo form_open(current_url()); ?>
        <label>Name</label>
        <input  type="text" name="name" required>
        <label>Email</label>
        <input  type="email" name="email" required>
        <label>Phone</label>
        <input  type="text" name="phone" >
        <label>Subject</label>
        <input  type="text" name="subject" required>
        <label>Message</label>
        <textarea rows="5" name="message" style="width:100%" required></textarea>
        <label></label>
        <input type="submit" class="btn btn-primary">
      </form>      
      </div>
      <div class="span6">
        <!-- <img class="pull-right" src="<?php echo base_url() ?>assets/img/contact_page.jpg" style="margin-right: -4.5%;"> -->
        <iframe class="" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d387367.8965633383!2d-73.90178584800003!3d40.66710077326012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1396445767232" width="500" height="300" frameborder="0" style="border:0; margin-left:4.5%"></iframe><br>
        <div class="row" style="font-size:13px; margin-left:-1%; margin-top:3%">
        
          <div class="span1">Company </div> <div class="span4">: <?php echo @$info->name ?> </div>
          <div class="span1">Address </div> <div class="span4">: <?php echo @$info->address ?></div>
          <div class="span1">Email </div> <div class="span4">: <?php echo @$info->email ?></div>
          <div class="span1">Contact No. </div> <div class="span4">: <?php echo @$info->phone ?></div>
         <!-- <b>Address :   <br> Email : info@youremail.com <br>Contact No : 123.456.7890</b> -->
        
        </div>  
      </div>       
    </div>       
</div>
<!-- end the content -->
<style type="text/css">
  input[type="text"]{
    width:100%;
    border-radius:5px;
    border:1px solid #CCC;
    height:35px;
  }

  input[type="email"]{
    width:100%;
    border-radius:5px;
    border:1px solid #CCC;
    height:35px;
  }
</style>
