<style>
img.colimage {
    width: 170px !important;
    height: 170px;
}
#first_faraz{
    color:#E05284;
}
#first_faraz:hover{ 
    color:#474646;
    cursor:pointer;
}
a
{
    text-decoration: none !important;
}


.text4 a, .text5 a, .text8 a{
    color: #989D9F;
}

.text4 a:hover, .text5 a:hover, .text8 a:hover
{
    color:#E05284 !important;
}

a .text5:hover
{
    color:#E05284 !important;
}

.form1
{
    margin-left: 13.5% !important;
}

.alert-msg{
     margin-top: 7px !important;
}
.carousel {
    margin-top: 32px;
}
.pac-container:after
 { 
   content:none !important;
 }
</style>
<div style="color:#666666; font-size:18px; margin-top:20px; margin-left:4%">   
    <span class="span1">&nbsp;</span>
       <?php $langhead = lang('header'); ?>
    <span class="span5">
        <?php echo $langhead['socialmarket']; ?>
    </span> 
    <span class="span6 pull-right" style="">        
        <?php echo $langhead['offer']; ?>
        <a  id="first_faraz" style=" text-decoration:none !important;" <?php if($this->session->userdata('user_info')){ echo "href=".base_url()."front/listing_property_first"; }else{ ?> data-toggle="modal" data-target="#login_listing_modal"  <?php } ?> style="background-color:#8ecd9e !important; color:#fff !important;">
            <?php echo $langhead['create_listind']; ?>           
        </a>
            <?php echo $langhead['free']; ?>                   
    </span>    
</div>
<p></p>
<br>

<?php if($this->session->userdata('site_lang') !="english"){ ?>
<style type="text/css">
    .text4{
        margin-top: 17px !important;
    }
</style>
<?php } ?>

        <?php $featured_images = get_featured_images() ?>
    <div id="myCarousel" class="carousel slide">
    <ol class="carousel-indicators">
    <?php if(!empty($featured_images)): ?>
     <?php $i=0; foreach($featured_images as $row): ?>
        <?php if($i==1): ?>
              <li class="active"></li>
         <?php else: ?>
              <li></li>
         <?php endif; ?>   
     <?php $i++;endforeach; ?>
    <?php else: ?>
        <li class="active"></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    <?php endif; ?>
    </ol>

    <?php if(!empty($featured_images)): ?>
    <!-- Carousel items Dynamic Starts -->
    <div class="carousel-inner">
     <?php $i=1; foreach($featured_images as $row): ?>
        <?php if($i==1): ?>
          <div class="active item"><img style="width:1920px;height:500px" src="<?php echo base_url()?>assets/uploads/property/<?php echo $row->image_file ?>"></div>
        <?php else: ?>
          <div class="item"><img style="width:1920px;height:500px" src="<?php echo base_url()?>assets/uploads/property/<?php echo $row->image_file ?>"></div>
        <?php endif; ?>
     <?php $i++;endforeach; ?>
    </div>
    <!-- Carousel items Dynamic Ends -->
  
    <?php else: ?>
    <!-- Carousel items by Default Starts -->
    <div class="carousel-inner">
        <div class="active item"><img src="<?php echo base_url()?>assets/front_end_theme/images/1.jpg"></div>
        <div class="item"><img src="<?php echo base_url()?>assets/front_end_theme/images/slider2.jpg"></div>
        <div class="item"><img src="<?php echo base_url()?>assets/front_end_theme/images/slider3.jpg"></div>
        <div class="item"><img src="<?php echo base_url()?>assets/front_end_theme/images/slider4.jpg"></div>
        <div class="item"><img src="<?php echo base_url()?>assets/front_end_theme/images/slider5.jpg"></div>
    </div>
    <!-- Carousel items by Default Ends -->
 
    <?php endif; ?>
    <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
    <!-- Carousal Ends -->


    <div class="container">
    <div class="span12 formcontent">

        <?php $langsearch = lang('search'); ?>
    
    <?php echo form_open(''.base_url().'properties', array('class' => 'navbar-form pull-center form1 offset1') );  ?>
    <input type="text" id="front_loacation" style="border:0.1px solid !important;width:290px" name="location" class="span4 bottle1" placeholder="<?php echo $langsearch['where']; ?>">
    <input type="text" id="dpd11" name="search_check_in" class="span2 bottle1" placeholder="<?php echo $langsearch['checkin']; ?>">
    <input type="text" id="dpd22" name="search_check_out"   class="span2 bottle1" placeholder="<?php echo $langsearch['checkout']; ?>">
    <input type="text" name="guest"   class="span2 bottle1" placeholder="<?php echo $langsearch['guest']; ?>">
    <button type="search" class="btn mobile1" style="height:37px; border-color:white"><?php echo $langsearch['search'] ?></button>
    <?php echo form_close(); ?>    


    </div>
</div>
    
<style type="text/css">
    fnrtform li{
        display: inline-block;
    }
</style>

    <!-- <div class="container">
        <div class="span12 formcontent">           
            <form action="http://www.bnbclone.com/demo/properties" method="post" accept-charset="utf-8" class="navbar-form pull-center form1 offset1">    
                <div class="input-append">                          
                    <span class="add-on">.00</span>
                    <input type="text" id="appendedInput" style="border:0.1px solid !important;width:290px" name="location" class="span4 bottle1" placeholder="Where do you want to go ?" autocomplete="off">
                </div>
                <input type="text" id="dpd11" name="search_check_in" class="span2 bottle1 hasDatepicker" placeholder="Check-In">
                <input type="text" id="dpd22" name="search_check_out" class="span2 bottle1 hasDatepicker" placeholder="Check-out">
                <input type="text" name="guest" class="span2 bottle1" placeholder="Guest">
                <button type="search" class="btn mobile1" style="height:37px; border-color:white">search</button>               

            </form>    
        </div>
    </div> -->



<?php $langtravel = lang('travel'); ?>
<?php $langhost = lang('host'); ?>
<?php $langhow_it_works = lang('how_it_works'); ?>
    <!-- <hr class="hr2"> -->
    <div class="container" style="margin-top:2%; margin-bottom:2%;">
        <div class="row middle-content">
            <div class="span12">
                <div class="span3 img-content1">
                    <a href="<?php echo base_url() ?>page/cms_page/guesthost">
                    <div class="mobile10 ">
                        <?php if(!empty($site_content)): ?>
                            <img  src="<?php echo base_url()?>assets/uploads/content/<?php echo $site_content->col1_image; ?>">
                        <?php else: ?>
                            <img  src="<?php echo base_url()?>assets/front_end_theme/images/img1.png"><br>
                        <?php endif; ?>
                    </div>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p class="text3 txt01"  >
                       <?php echo $langtravel['content']; ?>
                    </p>
                    <h1 class="text5">
                       <?php echo $langtravel['travel']; ?>
                    </h1>
                    </a>
                </div>
                <div class="span3 img-content2">
                  <a href="<?php echo base_url() ?>page/cms_page/guesthost"> 
                    <div class="mobile11">
                        <?php if(!empty($site_content)): ?>
                            <img  src="<?php echo base_url()?>assets/uploads/content/<?php echo $site_content->col2_image; ?>">
                        <?php else: ?>
                            <img  src="<?php echo base_url()?>assets/front_end_theme/images/img2.png">
                        <?php endif; ?>
                    </div>
                        <p></p>
                    <p class="text3 txt02">
                       <?php echo $langhost['content']; ?>
                    </p>
                    <h1 class="text8">
                        <?php echo $langhost['host']; ?> 
                        </h1>
                  </a>
                </div>
                <div class="span3 img-content3">
                  <a href="<?php echo  base_url() ?>page/how_it_works">
                    <div class="mobile12">
                        <?php if(!empty($site_content)): ?>
                            <img  src="<?php echo base_url()?>assets/uploads/content/<?php echo $site_content->col3_image; ?>">
                        <?php else: ?>
                            <img  src="<?php echo base_url()?>assets/front_end_theme/images/img3.png">
                        <?php endif; ?>
                    </div>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p class="text6"><?php echo $langhow_it_works['content']; ?>
                    </p>
                    <h1 class="text5">
                          <?php echo $langhow_it_works['how_it_works']; ?>
                        </h1>
                  </a>
                </div>
            </div>
        </div>
    </div>
    
    <?php $langneighborhood = lang('neighborhood'); ?>

<div class="bg4">
    <div  style="background-color: rgba(0,0,0,.6); margin-bottom: -0.5%;" >
    <div class="container" >
        <div class="row">
            <div class="span12 btm-comtent">
                <p class="text7">
                    <?php echo $langneighborhood['content'] ?>
                </p><br>
                <div class="text9 span3">
                <a href="<?php echo base_url(); ?>neighbourhood" class="text10" style="cursor:pointer; text-decoration:none !important;"> <?php echo $langneighborhood['allneighborhood'] ?></a>
                </div>
            </div>
        </div>

    <!--  neighbourhoot city -->
    <style>
    .imageFrame{ 
        border:20px solid transparent;
        border-image:url(<?php echo base_url(); ?>assets/img/frame.png) 30 30 stretch;
    }
    #nbcount{
        display:none; 
        margin:-110px 0px 0px 0px; 
        position:absolute; 
        color:white; 
        font-size:14px; 
        font-weight:bold; 
        padding-left:1%;  
    }
    #nbhood{ 
        margin-left:8%;
        margin-top:20px;
        margin-bottom:20px;
    }
    #nbhood a:hover #nbcount{ 
        display:block; 
    }
    #nbhood a:hover img{ 
        opacity:0.8; 
    }
    #city_name{
        margin:-140px 0px 0px 0px; 
        position:absolute; 
        color:white; 
        font-size:24px; 
        font-weight:bold; 
        padding-left:1%;  
    }

    /*#nbhood a:hover .imageFrame{ 
        border:20px solid transparent;
        border-image:url(<?php echo base_url(); ?>assets/img/hoverframe.png) 30 30 stretch;
    }*/
    </style>
    <div> 
        <div class="span12"  align="center">
        <?php $cities = get_cities_info(); ?>
        <?php $i=1; ?>
            <div class="row">
            <?php if(!empty($cities)): ?>
            <?php foreach ($cities as $city): ?>
                <div class="span2" id="nbhood">
                    <a style="text-decoration:none;" href="<?php echo base_url(); ?>neighbourhood/city/<?php echo $city->id; ?>">
                        <img class="imageFrame" style="height:180px; width:160px;" src="<?php echo base_url()?>assets/uploads/cities/thumbnails/<?php echo $city->thumbnail; ?>">
                        <div class="span2" id="city_name"><?php echo $city->city; ?></div>
                        <div class="span2" id="nbcount" ><?php echo get_neighbourhoot_of_city($city->id, 1); ?></div>
                    </a>
                </div>
                <?php if($i%4 == 0): ?>
                    </div>
                    <div class="row">
                <?php endif; ?>
            <?php $i++; ?>    
            <?php endforeach; ?>
            <?php endif; ?>
            </div> 
        </div>
    </div>
     <!--  neighbourhoot city -->

    </div>
    </div>
</div>    
<style type="text/css">
.bg4{
    background: url('<?php echo base_url() ?>assets/img/home_map.png');
    background-size: 100%;
    padding-top: 20%;
}
    .img-content2{
        margin-left: 8%;
    }

    .img-content1{
        margin-left: 5.5%;
    }

    .img-content3{
        margin-left: 7%;
    }

    .text4 , .text8 , .text5{
        font-size: 24px;
    }

    .text8{
        margin-top: 40px !important;
    }

    .text6{
        margin-top: 10%;
    }

    .txt02{
        margin-top: 5%;
    }
    .txt01{
        margin-top: 8%;
        /*margin-left: 2%;*/

    }

    .mobile11 .text3{
        margin-top: 11%;
    }

    .middle-content{
        margin: 0 auto;
        text-align: center;
    }

    .form1{
        padding-left: 5px;
        padding-right: 5px;
        margin-left: 9%;

    }

    .formcontent{
        margin: 0 auto;
    }

    .mobile10 img{
        margin-top: 5px;
    } 

    .text7{
        font-family: 'Cabin';
        font-weight: normal;
        font-size: 26px;
    }

    .btm-comtent{
        text-align: center;
    }

    .text10{
        font-family: 'Cabin';
        vertical-align: middle;
    }

    .bg6{
        margin-top: 28px;
    }

    .text9{
        padding-top: 10px !important;
    }

    

    .list2 li{
        padding-left: 10px;
        padding-right: 10px;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .list2 li a{
        font-size: 16px;
    }

    .bg10{
        margin-top: 10px;
    }

</style>


<script>
$(function() {
$( "#dpd11" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#dpd22" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#dpd22" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#dpd11" ).datepicker( "option", "maxDate", selectedDate );
}
});
});

    $(function()
        {
            var e;           
            //$("body").addClass("show_wishlists_search");
            e = $('#front_loacation')[0];
            return new google.maps.places.Autocomplete(e, {
                // types: ["geocode"],
                types: ['(cities)'],
                // types: ['(regions)'],
            });
           
        });
</script>
