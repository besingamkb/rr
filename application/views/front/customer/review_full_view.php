<?php $this->load->view('customer/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review Full View</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review->property_title)) echo $review->property_title; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>Review Message</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review->review)) echo $review->review; ?>    
                    </div>
                </div>
                
                <br>
                
             
                <div class="row">
                    <div class="col-md-3">
                     <strong>Created Date</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review->created)) echo $review->created; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>User Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review->user_name)) echo $review->user_name; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>User Email</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review->user_email)) echo $review->user_email; ?>    
                    </div>
                </div>
                
                <br>

              <br><br>

              <div class="row">
                <div class="col-md-3">
                 <strong>&nbsp;&nbsp;&nbsp;</strong>
                </div>
                <div class="col-md-9">
                   <a class="btn btn-info" href="<?php echo base_url()?>user/review"> Back to Review</a>    
                </div>
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->