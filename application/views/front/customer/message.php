<?php $this->load->view('customer/leftbar'); ?>


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Messages</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
        <table class="table">
          <thead>
            <tr>
              <th>&nbsp;&nbsp;&nbsp;#</th>
              <th>Porperty Title</th>
              <th>Subject</th>
              <th>Messages</th>
              <th>created</th>
              <th style="width:180px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($messages)){ $i=1; foreach ($messages as $row){?>    
            <tr>
              <td><?php //echo $i; ?>
               <?php 
                    $user_info = $this->session->userdata('customer_info');
                    $current_email = $user_info['user_email'];
                    $sender_email = $row->user_email;
                      if($current_email == $sender_email): ?>
                              <img src="<?php echo base_url(); ?>assets/img/out.png" >
                <?php else: ?>
                              <img src="<?php echo base_url(); ?>assets/img/in.png" >
                <?php endif; ?>                
              </td>
              <td><?php if(!empty($row->pr_title))echo $row->pr_title;  ?></td>
              <td><?php if(!empty($row->subject)) echo $row->subject; ?></td>
              <td><?php if(!empty($row->message)) echo word_limiter($row->message,5); ?></td>
              <td><?php if(!empty($row->created)) echo $row->created; ?></td>
              <td>
                  <a style="width:90px" class="btn btn-default" href="<?php echo base_url()?>customer/message_view/<?php echo $row->id;?>">
                    View (<font color="red"><?php echo reply_count($row->id); ?></font>)
                  </a> 
                  <a class="btn btn-info" onclick="return confirm('Do you want to delete it?');"  href="<?php echo base_url()?>customer/delete_message/<?php echo $row->id;?>">Delete</a>
              </td>
            </tr>
            <?php $i++; } } else{ ?>
              <tr><td colspan="6"> No Record Found</td></tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <span style="margin-left:20px"><?php if($pagination){echo  $pagination;} ?></span>
    </div>
  </div>
</div><!-- /.container -->