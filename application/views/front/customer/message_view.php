    <?php $this->load->view('customer/leftbar'); ?>

        <div class="col-lg-9">
            <div class="row content-top">
                <div class="welcome">
                    <h3>Message</h3>
                </div>
                <?php if($this->session->flashdata('error_msg')){ ?>
                    <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
                <?php } ?>
                <?php if($this->session->flashdata('success_msg')){ ?>
                    <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
                <?php } ?>
                <br>
            <div class="col-md-12 row">
<!-- ///////////////////  Main Message /////////////////////////////// -->
            <div class="col-md-10" style="margin-left:10%; background-color:#D6F6F9; border-radius:5px;">
                <div style="width:662px; padding:5px; margin-left:-15px; color:black; font-size:24px; background-color:#33AEBD; border-radius:5px;">
                    <img src="<?php echo base_url(); ?>assets/img/default_user_old.png" height="40" width="40" style="border-radius:30px;" />
                    <?php
                        $user_info = $this->session->userdata('customer_info');
                        if($user_info['user_email'] == $message->user_email) 
                            { echo "Me"; }
                        else 
                            { echo "Customer"; }    
                    ?>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-3" align="right" style="color:#33AEBD; font-weight:bold;">
                        <strong>Property Title:</strong>
                    </div>    
                    <div class="col-md-7" style="font-weight:bold;">
                        <?php if(!empty($message->pr_title)) echo $message->pr_title; ?>    
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-3" align="right" style="color:#33AEBD; font-weight:bold;">
                        <strong>Subject:</strong>
                    </div>
                    <div class="col-md-7" style="font-weight:bold;">
                        <?php if(!empty($message->subject)) echo $message->subject; ?>    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" align="right" style="color:#33AEBD; font-weight:bold;">
                        <strong>Message:</strong>
                    </div>
                    <div class="col-md-7">
                        <?php if(!empty($message->message)) echo $message->message; ?>    
                    </div>
                </div>
                <br>
                <div align="right" class="row col-md-12" style="color:#33AEBD; padding:5px;">
                    Sent by&nbsp;<?php if(!empty($message->user_email)) echo $message->user_email; ?>
                    (<span style="color:#1F7A84;"><?php if(!empty($message->user_name)) echo $message->user_name; ?></span>)
                    &nbsp;on&nbsp;<?php echo get_time_ago($message->created); ?> 
                </div>
            </div>

<!-- ///////////////////  Reply Message /////////////////////////////// -->
<?php if($reply): ?>
    <?php foreach ($reply as  $row): ?>
            <div class="col-md-10 faraz" style="margin-left:10%; margin-top:20px; background-color:#D6F6F9; border-radius:5px;">
                <?php if($row->replier_status == 1 ): ?>
                <div align="right" style="width:662px; padding:5px; margin-left:-15px; color:black; font-size:24px; background-color:#88E0EA; border-radius:5px;">
                    Superadmin
                    <img src="<?php echo base_url(); ?>assets/img/default_user.gif" height="40" width="40" style="border-radius:30px;" />
                </div>
                <?php elseif($row->replier_status == 2 ): ?>
                <div align="right" style="width:662px; padding:5px; margin-left:-15px; color:black; font-size:24px; background-color:#88E0EA; border-radius:5px;">
                    Admin
                    <img src="<?php echo base_url(); ?>assets/img/default_user.gif" height="40" width="40" style="border-radius:30px;" />
                </div>
                <?php elseif($row->replier_status == 3 ): ?>
                <div align="right" style="width:662px; padding:5px; margin-left:-15px; color:black; font-size:24px; background-color:#88E0EA; border-radius:5px;">
                    Property owner&nbsp;
                    <img src="<?php echo base_url(); ?>assets/img/default_user_300x300.png" height="40" width="40" style="border-radius:30px;" />
                </div>
                <?php else: ?>    
                <div style="width:662px; padding:5px; margin-left:-15px; color:black; font-size:24px; background-color:#88E0EA; border-radius:5px;">
                    <img src="<?php echo base_url(); ?>assets/img/default_user_old.png" height="40" width="40" style="border-radius:30px;" />
                        <?php
                        $customer_info = $this->session->userdata('customer_info');
                        if($customer_info['user_email'] == $row->replier_email) 
                            { echo "Me"; }
                        else 
                            { echo "customer"; }    
                        ?>
                </div>
                <?php endif; ?>
                <br>
                <div class="row">
                    <div class="col-md-3" align="right" style="color:#33AEBD; font-weight:bold;">
                        <strong>Reply Message:</strong>
                    </div>
                    <div class="col-md-7">
                        <?php if(!empty($row->reply)) echo $row->reply; ?>    
                    </div>
                </div>
                <br>
                <div align="right" class="row col-md-12" style="color:#33AEBD; padding:5px;">
                    Sent by&nbsp;<?php if(!empty($row->replier_email)) echo $row->replier_email; ?>
                    (<span style="color:#1F7A84;"><?php if(!empty($row->replier_name)) echo $row->replier_name; ?></span>)
                    &nbsp;on&nbsp;<?php echo get_time_ago($row->created); ?> 
                </div>
            </div>
    <?php endforeach; ?>
<?php endif; ?>  


<div class="col-lg-9" id="dynamic_reply"></div>
<!--////////////////   LOAD MORE START  ////////////////////  -->
            <div class="col-md-10" style="margin:2% 10% 0% 10%; color:#33AEBD; ">
                <?php if ($total_reply >4): ?>
                    <h3><a id="loadmore" style="text-decoration:none; cursor:pointer;">Load more.........</a></h3> 
                <?php endif; ?>
                <input type="hidden" value="<?php echo $total_reply; ?>" id="total_reply">
            </div>
<!--////////////////   LOAD MORE END  ////////////////////  -->

<!--/////////////////////   Reply option ///////////////////////////////////////////////  -->
            <div class="col-md-10" style="margin:10%; padding:10px; background-color:#D6F6F9; border-radius:5px; ">
            <?php echo form_open(base_url().'customer/reply/'.$message->id); ?>
            <table align="left" style="width:702px; margin-top:20px;">    
                <tr>
                    <td>
                        <textarea rows="2" style="color:#33AEBD; padding:5px; width:600px; border:5px #88E0EA solid; font-weight:bold; border-radius:15px"  name="reply" required="required"></textarea>
                        <br><br>
                        <button style="margin-left:540px" class="btn btn-info btn-large hidden-phone">
                            Reply
                        </button>
                    </td>
                </tr>
            </table>
            <?php echo form_close(); ?>
            </div>
<!--////////////////////// End of reply option ////////////////////////////////  -->
            </div>
        </div>
    </div>
</div>






<!-- //////////////////////  ajax load more reply /////////////////////////// -->
<script>
$(document).ready(function(){  
   $("#loadmore").click(function(){ 
      
      var offset = $(".faraz").length;
      $.ajax({
               url: "<?php echo base_url(); ?>customer/ajax_load_more_reply/<?php echo $message->id; ?>/"+offset,
               success: function(data)
               {
                 $("#dynamic_reply").append(data);
               }
      });
      
         var total_rows = $("#total_reply").val();
         if(total_rows <= offset+4)
         {
         $("#loadmore").hide();
         }
   });
});
</script>