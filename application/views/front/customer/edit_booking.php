<style type="text/css">
    .unique_feature {
        color: #777;
        float: left;
        height: 50px;
        padding: 0 5px 10px;
        width: 126px;
    }
</style>

<?php $this->load->view('customer/leftbar'); ?>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Edit Booking</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
          <!--  <form class="form-horizontal" role="form"> -->
          <?php echo form_open(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
             <?php $guests = get_users(); /*print_r($guests); die();*/?>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Guest Name</label>
              <div class="col-sm-10">
                <input  type="text"  name="guest_name" readonly="readonly" id="guest_name" style="width:600px !important;" class="form-control" value="<?php if(!empty($booking->guest_name)) echo  $booking->guest_name; ?>" placeholder="Guest Name">
                 <!--  <select onchange="return get_guest_info();" id="guest_name" style="width:600px !important;"class="form-control" name="guest_name" class="span10 input-left-top-margins">
                      <option value="">Select Guest Name</option>
                      <?php //foreach ($guests as $row): ?>
                        <option <?php //if($booking->guest_name == $row->first_name.' '.$row->last_name) echo "selected='selected'";?>  value="<?php echo $row->id; ?>"><?php echo $row->first_name.' '.$row->last_name; ?></option>
                      <?php //endforeach; ?>
                  </select> -->

              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Guest Email</label>
              <div class="col-sm-10">
                <input type="text" name="email" readonly="readonly" value="<?php echo $booking->email;?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Last Name">
                 <span class="form_error"><?php echo form_error('email'); ?></span>
              </div>
            </div> 
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Guest Contact No.</label>
              <div class="col-sm-10">
                <input type="text" readonly="readonly" name="phone" value="<?php echo $booking->contact; ?>" style="width:600px !important;" class="form-control" id="phone" placeholder="User Contact">
                 <span class="form_error"><?php echo form_error('phone'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Guest Full Address</label>
              <div class="col-sm-10">
                <input type="text" name="address" readonly="readonly" value="<?php echo $booking->address; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Address">
                <span class="form_error"><?php echo form_error('address'); ?></span>
              </div>
            </div>
             <?php $users = get_all_user();?>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label"> Host Email</label>
              <div class="col-sm-10">
                <!-- <input type="text" name="city"  value="" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="City"> -->
                <input name="host_email"   id="host_email" style="width:600px !important;" class="form-control" type="text" placeholder="Host Email" value="<?php $Email =get_host_email($booking->pr_id);  echo $Email;?>"  >
                <span class="form_error"><?php echo form_error('host_email'); ?></span>
              </div>
            </div> 
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">  Propety Title <span style="font-size:10px;">(Selected)</span></label>
              <div class="col-sm-10">
               <input name="property_title" type="text" style="width:600px !important;" class="form-control"  placeholder="State" value="<?php $title =get_property_title($booking->pr_id);  echo $title;?>">
                  <span class="form_error span12"><?php // echo form_error('state'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Property Titles </label>
             
              <div class="col-sm-10">
                <div id="user_properties" class="controls controls-row span10">
                  
               
                </div>
                <span class="form_error span12"><?php echo form_error('property_id'); ?></span>
              </div>
            </div>
            <br>
            <br>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Check In & Out </label>
              <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label span1" for="normal-field"> Check in - </label>
                        <div class="controls span2">
                            <input  style="width:300px !important;" name="check_in" value="<?php echo date('d-m-Y', strtotime($booking->check_in)); ?>" class="form-control" type="text" id="datepicker">
                        </div>
                      <span class="form_error span12"><?php echo form_error('check_in'); ?></span>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label span1" for="normal-field"> Check Out - </label>
                        <div class="controls span2">
                           <input  style="width:300px !important;" name="check_out"  value="<?php echo date('d-m-Y', strtotime($booking->check_out));  ?>" class="form-control" type="text" id="datepicker2">
                        </div>
                        <span class="form_error span12"><?php echo form_error('check_out'); ?></span>
                    </div>
                </div>
              </div>
            </div>
            <br>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label"> Due Amount <span style="font-size:10px;">(Selected)</span></label>
              <div class="col-sm-10">
                <!-- <input name="property title" type="text"   placeholder="State" value="<?php $title =get_property_title($booking->pr_id);  echo $title;?>"> -->
                <input name="due_amount" id="due_amount" value="<?php if(!empty($booking->due_amount)) echo $booking->due_amount ;  ?>"  style="width:600px !important;" class="form-control" type="text" placeholder="Due Amount" value="<?php echo set_value('due_amount'); ?>">
                <span class="form_error span12" id="due-error"><?php echo form_error('due_amount'); ?></span>              
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn">Save Changes</button>
              </div>
            </div>
          <!-- </form> -->
          <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div><!-- /.container -->
 <script type="text/javascript">
   $(document).ready(function() {
      // var currentDate = $('#currentDate').val();
    
        $('#datepicker').datepicker({ dateFormat:"dd-mm-yy", minDate:"D" });
        // $( "#format" ).change(function() {
        //     $( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
        // });
    
      // var listDate = $('#start_c_date').val();

      $('#datepicker2').datepicker({ dateFormat:"dd-mm-yy", minDate:"+1D" });
   });
  </script>
    <script>
  $(function(){
      var user = new Array();
      <?php foreach($users as $row){ ?>
              user.push('<?php echo $row->user_email; ?>');
      <?php } ?>
      //alert(user);
    $( "#host_email" ).autocomplete({
       source: user,
       select: function(suggestion){
           var host_email = document.getElementById('host_email').value; 
          $('#user_properties').html('');
           $.post( "<?php echo base_url()?>user/user_property",{'user_email':host_email })
            .done(function( data ) {
            // alert( "Data Loaded:" + data );
            $('#user_properties').append(data);
          });
        }
     });
  });

  $( "#host_email" ).on('change', function(){

      $('#user_properties').html('');

      var host_email = document.getElementById('host_email').value; 

      $.post( "<?php echo base_url()?>user/user_property",{'user_email':host_email })
      .done(function( data ) {
      // alert( "Data Loaded:" + data );
        $('#user_properties').append(data);
      });
  });

  </script>
   <script type="text/javascript">
  function get_amount (id){
        // alert(id);
        // return false;
        // var id = $(this).val();
          $.ajax({
              type: "POST",
              data: { action: 'get_content',},
              url: "<?php echo base_url() ?>user/ajax_propety_amount/"+id,
              success: function(res){
                var obj = $.parseJSON(res);
                if(obj.status === 1){
                   $('#due_amount').val(obj.amount);
                   $('#due-error').html('');
                }
                else
                   $('#due-error').html('No amount found for the selected property.');
              }
          });
      };
   </script>