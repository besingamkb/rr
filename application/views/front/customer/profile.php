<?php $this->load->view('customer/leftbar'); ?>


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Profile</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
          <!--  <form class="form-horizontal" role="form"> -->
          <?php echo form_open(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
              <div class="col-sm-10">
                <input type="text" name="first_name" value="<?php echo $member->first_name; ?>" style="width:600px !important;" class="form-control" id="inputEmail3" placeholder="First Name">
                 <span class="form_error"><?php echo form_error('first_name'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Last Name</label>
              <div class="col-sm-10">
                <input type="text" name="last_name" value="<?php echo $member->last_name; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Last Name">
                 <span class="form_error"><?php echo form_error('last_name'); ?></span>
              </div>
            </div> 
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">User Email</label>
              <div class="col-sm-10">
                <input type="text" name="user_email" value="<?php echo $member->user_email; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="User Email">
                 <span class="form_error"><?php echo form_error('user_email'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Address</label>
              <div class="col-sm-10">
                <input type="text" name="address" value="<?php echo $member->address; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Address">
                <span class="form_error"><?php echo form_error('address'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">City</label>
              <div class="col-sm-10">
                <input type="text" name="city"  value="<?php echo $member->city; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="City">
               <span class="form_error"><?php echo form_error('city'); ?></span>
              </div>
            </div> 
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">State</label>
              <div class="col-sm-10">
                <input type="text"  name="state" value="<?php echo $member->state; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="State">
                 <span class="form_error"><?php echo form_error('state'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Country</label>
              <?php $country = get_country_array(); ?>
              <div class="col-sm-10">
                <select name="country" style="width:600px !important;"class="form-control">
                    <option value="">Select country</option>
                    <?php foreach ($country as $code => $name): ?>
                    <option <?php if($code == $member->country){echo "selected='selected'";} ?> value="<?php echo $code; ?>"> <?php echo $name; ?> </option>
                    <?php endforeach ?>
                  </select>
                <span class="form_error"><?php echo form_error('country'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Contact</label>
              <div class="col-sm-10">
                <input type="text" name="phone" value="<?php echo $member->phone; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Contact">
               <span class="form_error"><?php echo form_error('phone'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Zip</label>
              <div class="col-sm-10">
                <input type="text" name="zip" value="<?php echo $member->zip; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Contact">
                 <span class="form_error"><?php echo form_error('zip'); ?></span>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn">Save Changes</button>
              </div>
            </div>
          <!-- </form> -->
          <?php echo form_close(); ?>
        </div>
        </div>


      <!--   <div class="row content-mid">
          <div class="q_link">
            <h3>Quick links</h3>
          </div>
          <div class="links">          
            <div class="content-div">
              <div class="green-circle">
                 <img src="<?php //echo base_url()?>assets/bnb_html/img/search.png">                  
              </div>              
              <p>View/Manage listing</p>
            </div>
            <div class="content-div">
              <div class="green-circle">
                <img src="<?php //echo base_url()?>assets/bnb_html/img/bell.png" style="width:90%">                  
              </div>              
              <p>Reservation</p>
            </div>
            <div class="content-div ">
              <div class="green-circle">
                <img src="<?php //echo base_url()?>assets/bnb_html/img/eye.png" style="width:30px; margin-left:-5px" >                  
              </div>              
              <p>Review & Reference</p>
            </div>
            <div class="content-div">
              <div class="green-circle">
                <img src="<?php echo base_url()?>assets/bnb_html/img/star.png">                  
              </div>              
              <p>Favorites</p>
            </div>
            
          </div>
        </div>



        <div class="row content-top">
          <div class="q_link">
            <h3>Alert</h3>
            <hr>
          </div>
           
           <span class="btm-span"> Please Tell us how to pay you</span>
            <hr>
            <span class="btm-span">
            Connect to facebook complete your profile and make easy to login
            </span>
          <div class="links">                      
            
          </div>
        </div> -->

      </div>
     <!--  <div class="starter-template">
        <h1>Bootstrap starter template</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div> -->

    </div><!-- /.container -->