<?php $this->load->view('customer/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Bookings</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Booking Name</th>
              <th>Property Title</th>
              <th>Check in </th>
              <th>Check Out </th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($bookings)){ $i=1; foreach ($bookings as $row){?>              
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php if(!empty($row->guest_name)) echo $row->guest_name; ?></td>
                    <td><?php if(!empty($row->pr_id)){ $property= get_property_detail($row->pr_id); echo $property->title;  } ?></td>
                    <td><?php if(!empty($row->check_in)) echo $row->check_in; ?></td>
                    <td><?php if(!empty($row->check_out)) echo $row->check_out;?></td>
                    <td>
                      <?php if($row->status == 0):?>
                         <button type="button" class="btn btn-warning">Pending</button>
                      <?php else: ?>
                        <button type="button" class="btn btn-primary">Complete</button>
                      <?php endif; ?>
                    </td>
                    <td>
                      <?php if($row->status == 0):?>
                       <a  class="btn btn-info" href="<?php echo base_url() ?>user/edit_booking/<?php echo $row->id;?>">Edit</a> 
                      <?php endif; ?>
                      <a class="btn btn-warning" onclick="return confirm('Do you want to Delete it?');" href="<?php echo base_url() ?>user/delete_message/<?php echo $row->id; ?>">Delete</a>
                    </td>
                  </tr>             
            <?php $i++; } } else{ ?>
              <tr><td colspan="6"> No Record Found</td></tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div><!-- /.container -->