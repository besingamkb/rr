<!-- start the content -->
<div class="container" style="min-height:450px;">
    <div  style="width:86%; margin:2%; margin-left:7%"> 
      <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a href="#terms">Terms & Privacy</a></li>
        <li><a href="#legal" id="">Legal</a></li>
        <li><a href="#trust" id="">Trust & verifications</a></li>
        <li><a href="#policy">Policies</a></li>
      </ul>                     
      <div class="tab-content">
        <div class="tab-pane active" id="terms">
          <h2>Term & Privacy</h2>         
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod neque sem, ut feugiat elit. Vestibulum dapibus sapien vel erat pulvinar sollicitudin. Aenean faucibus sapien at nibh malesuada tincidunt. Phasellus ut volutpat lectus. Suspendisse at felis ipsum. Vestibulum porta, sapien et varius convallis, leo nisi interdum ligula, sed gravida felis risus dictum dui. Suspendisse sed metus eget sem tincidunt dapibus interdum in dolor. Donec euismod posuere est, eget ultrices sapien posuere id. Sed imperdiet hendrerit odio ut euismod. Curabitur blandit feugiat mi, ac pharetra dui rhoncus non. Sed sagittis blandit dolor, sed posuere ligula placerat id. Vivamus quis odio justo, quis elementum dolor. <br><br>
              Integer mollis tellus ac ipsum aliquet scelerisque. Praesent nisi leo, imperdiet nec suscipit at, faucibus at arcu. Mauris aliquet imperdiet condimentum. Morbi faucibus, neque sed feugiat mattis, ligula magna interdum libero, egestas scelerisque arcu velit nec magna. Nulla ultricies eros vitae massa commodo sagittis. Vivamus a turpis mauris. Nam consectetur, felis ac rutrum pretium, enim lacus elementum justo, ut congue sapien ligula a leo. Aliquam molestie metus eget ante congue viverra tristique elit pulvinar. Aliquam egestas eros ut dolor dictum vel consequat est gravida. Sed elit turpis, imperdiet vel tempus at, sollicitudin vitae metus. Praesent hendrerit rhoncus sem placerat iaculis.              
            </p>
            <p>
              Nullam vehicula vehicula nibh tincidunt facilisis. Etiam aliquam, eros a tristique aliquam, tellus libero scelerisque erat, molestie condimentum nunc magna vitae nisi. Integer sollicitudin euismod hendrerit. Curabitur nec nisi eleifend velit malesuada pharetra. Sed felis est, vehicula quis sagittis dapibus, interdum non nulla. Ut placerat nunc nec ipsum elementum elementum posuere ipsum luctus. Etiam eu nisl turpis. In hac habitasse platea dictumst.
              Nunc ut purus id ligula eleifend varius. Phasellus pretium, purus id molestie rhoncus, justo libero posuere odio, eget consectetur magna urna semper sem. Morbi ultricies, odio et hendrerit sagittis, dolor erat sagittis eros, et lobortis metus purus in elit. Proin ligula libero, ullamcorper id vehicula sed, sollicitudin non sapien. Sed nec dolor neque, id blandit lectus. Vivamus turpis massa, aliquet sit amet dignissim sollicitudin, hendrerit vitae augue. Ut at libero in nulla rhoncus facilisis a sit amet nisl. Pellentesque facilisis quam non est sodales auctor. Curabitur in neque justo.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="tab-pane" id="legal">
          <h2>Legal</h2>         
             <p>
              Nullam vehicula vehicula nibh tincidunt facilisis. Etiam aliquam, eros a tristique aliquam, tellus libero scelerisque erat, molestie condimentum nunc magna vitae nisi. Integer sollicitudin euismod hendrerit. Curabitur nec nisi eleifend velit malesuada pharetra. Sed felis est, vehicula quis sagittis dapibus, interdum non nulla. Ut placerat nunc nec ipsum elementum elementum posuere ipsum luctus. Etiam eu nisl turpis. In hac habitasse platea dictumst.
              Integer mollis tellus ac ipsum aliquet scelerisque. Praesent nisi leo, imperdiet nec suscipit at, faucibus at arcu. Mauris aliquet imperdiet condimentum. Morbi faucibus, neque sed feugiat mattis, ligula magna interdum libero, egestas scelerisque arcu velit nec magna. Nulla ultricies eros vitae massa commodo sagittis. Vivamus a turpis mauris. Nam consectetur, felis ac rutrum pretium, enim lacus elementum justo, ut congue sapien ligula a leo. Aliquam molestie metus eget ante congue viverra tristique elit pulvinar. Aliquam egestas eros ut dolor dictum vel consequat est gravida. Sed elit turpis, imperdiet vel tempus at, sollicitudin vitae metus. Praesent hendrerit rhoncus sem placerat iaculis.              
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod neque sem, ut feugiat elit. Vestibulum dapibus sapien vel erat pulvinar sollicitudin. Aenean faucibus sapien at nibh malesuada tincidunt. Phasellus ut volutpat lectus. Suspendisse at felis ipsum. Vestibulum porta, sapien et varius convallis, leo nisi interdum ligula, sed gravida felis risus dictum dui. Suspendisse sed metus eget sem tincidunt dapibus interdum in dolor. Donec euismod posuere est, eget ultrices sapien posuere id. Sed imperdiet hendrerit odio ut euismod. Curabitur blandit feugiat mi, ac pharetra dui rhoncus non. Sed sagittis blandit dolor, sed posuere ligula placerat id. Vivamus quis odio justo, quis elementum dolor. <br><br>
              Nunc ut purus id ligula eleifend varius. Phasellus pretium, purus id molestie rhoncus, justo libero posuere odio, eget consectetur magna urna semper sem. Morbi ultricies, odio et hendrerit sagittis, dolor erat sagittis eros, et lobortis metus purus in elit. Proin ligula libero, ullamcorper id vehicula sed, sollicitudin non sapien. Sed nec dolor neque, id blandit lectus. Vivamus turpis massa, aliquet sit amet dignissim sollicitudin, hendrerit vitae augue. Ut at libero in nulla rhoncus facilisis a sit amet nisl. Pellentesque facilisis quam non est sodales auctor. Curabitur in neque justo.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>  
        </div>
        <div class="tab-pane" id="trust">
          <h2>Trust & verifications</h2>         
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod neque sem, ut feugiat elit.  Vestibulum dapibus sapien vel erat pulvinar sollicitudin. Aenean faucibus sapien at nibh malesuada tincidunt. <br><br> Phasellus ut volutpat lectus. Suspendisse at felis ipsum. Vestibulum porta, sapien et varius convallis, leo nisi interdum ligula, sed gravida felis risus dictum dui. Suspendisse sed metus eget sem tincidunt dapibus interdum in dolor. Donec euismod posuere est, eget ultrices sapien posuere id. Sed imperdiet hendrerit odio ut euismod. Curabitur blandit feugiat mi, ac pharetra dui rhoncus non. Sed sagittis blandit dolor, sed posuere ligula placerat id. Vivamus quis odio justo, quis elementum dolor.
              Integer mollis tellus ac ipsum aliquet scelerisque. Praesent nisi leo, imperdiet nec suscipit at, faucibus at arcu. Mauris aliquet imperdiet condimentum. Morbi faucibus, neque sed feugiat mattis, ligula magna interdum libero, egestas scelerisque arcu velit nec magna. Nulla ultricies eros vitae massa commodo sagittis. Vivamus a turpis mauris. <br><br> Nam consectetur, felis ac rutrum pretium, enim lacus elementum justo, ut congue sapien ligula a leo. Aliquam molestie metus eget ante congue viverra tristique elit pulvinar. Aliquam egestas eros ut dolor dictum vel consequat est gravida. Sed elit turpis, imperdiet vel tempus at, sollicitudin vitae metus. Praesent hendrerit rhoncus sem placerat iaculis.              
            </p>
            <p>
              Nullam vehicula vehicula nibh tincidunt facilisis. Etiam aliquam, eros a tristique aliquam, tellus libero scelerisque erat, molestie condimentum nunc magna vitae nisi. <br><br> <b> Integer sollicitudin euismod hendrerit. Curabitur nec nisi eleifend velit malesuada pharetra. Sed felis est, vehicula quis sagittis dapibus, interdum non nulla. Ut placerat nunc nec ipsum elementum elementum posuere ipsum luctus. Etiam eu nisl turpis. In hac habitasse platea dictumst.
              Nunc ut purus id ligula eleifend varius. </b><br><br> Phasellus pretium, purus id molestie rhoncus, justo libero posuere odio, eget consectetur magna urna semper sem. Morbi ultricies, odio et hendrerit sagittis, dolor erat sagittis eros, et lobortis metus purus in elit. Proin ligula libero, ullamcorper id vehicula sed, sollicitudin non sapien. Sed nec dolor neque, id blandit lectus. Vivamus turpis massa, aliquet sit amet dignissim sollicitudin, hendrerit vitae augue. Ut at libero in nulla rhoncus facilisis a sit amet nisl. Pellentesque facilisis quam non est sodales auctor. Curabitur in neque justo.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="tab-pane" id="policy">     
          <h2>Policies</h2>         
            <p>
              Nullam vehicula vehicula nibh tincidunt facilisis. Etiam aliquam, eros a tristique aliquam, tellus libero scelerisque erat, molestie condimentum nunc magna vitae nisi. Integer sollicitudin euismod hendrerit. Curabitur nec nisi eleifend velit malesuada pharetra. Sed felis est, vehicula quis sagittis dapibus, interdum non nulla. Ut placerat nunc nec ipsum elementum elementum posuere ipsum luctus. Etiam eu nisl turpis. In hac habitasse platea dictumst.
              Integer mollis tellus ac ipsum aliquet scelerisque. Praesent nisi leo, imperdiet nec suscipit at, faucibus at arcu. Mauris aliquet imperdiet condimentum. Morbi faucibus, neque sed feugiat mattis, ligula magna interdum libero, egestas scelerisque arcu velit nec magna. Nulla ultricies eros vitae massa commodo sagittis. Vivamus a turpis mauris. Nam consectetur, felis ac rutrum pretium, enim lacus elementum justo, ut congue sapien ligula a leo. Aliquam molestie metus eget ante congue viverra tristique elit pulvinar. Aliquam egestas eros ut dolor dictum vel consequat est gravida. Sed elit turpis, imperdiet vel tempus at, sollicitudin vitae metus. Praesent hendrerit rhoncus sem placerat iaculis.              
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod neque sem, ut feugiat elit. Vestibulum dapibus sapien vel erat pulvinar sollicitudin. Aenean faucibus sapien at nibh malesuada tincidunt. Phasellus ut volutpat lectus. Suspendisse at felis ipsum. Vestibulum porta, sapien et varius convallis, leo nisi interdum ligula, sed gravida felis risus dictum dui. Suspendisse sed metus eget sem tincidunt dapibus interdum in dolor. Donec euismod posuere est, eget ultrices sapien posuere id. Sed imperdiet hendrerit odio ut euismod. Curabitur blandit feugiat mi, ac pharetra dui rhoncus non. Sed sagittis blandit dolor, sed posuere ligula placerat id. Vivamus quis odio justo, quis elementum dolor. <br><br>
              Nunc ut purus id ligula eleifend varius. Phasellus pretium, purus id molestie rhoncus, justo libero posuere odio, eget consectetur magna urna semper sem. Morbi ultricies, odio et hendrerit sagittis, dolor erat sagittis eros, et lobortis metus purus in elit. Proin ligula libero, ullamcorper id vehicula sed, sollicitudin non sapien. Sed nec dolor neque, id blandit lectus. Vivamus turpis massa, aliquet sit amet dignissim sollicitudin, hendrerit vitae augue. Ut at libero in nulla rhoncus facilisis a sit amet nisl. Pellentesque facilisis quam non est sodales auctor. Curabitur in neque justo.
            </p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>                
        </div>
      </div>   
    </div>    
</div>
<!-- end the content -->

<style type="text/css">
  .nav-tabs{
    /*background-color: #f5f5f5;*/
    padding-top: 20px;
    /*border: 1px solid #DDDDDD*/
   }

   .nav-tabs li{
    margin-left: 1%;    
   }

   .nav-tabs li a{
    border: 1px solid #DDD;
    border-radius: 4px 0;
    color:#333;
   }

   li.active a{
    background-color: #fff !important;
    /*border: none !important;*/
   }

   .content{
      border: 1px solid #DDD;
   }

   .tab-content{
      padding: 10px;
   }
 
   .media > .pull-left {
    margin-right: 13px;
  }
  .text-center {
      text-align: center;
  }
  .pull-left {
      float: left;
  }
</style>   

<script>
    $('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    })

     $('#tab2 a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    })
     $('#tab3 a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    })
</script>             
           