<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/uniiverse_dropdown.css" type="text/css" >

<style type="text/css">
.input-block-level 
{
  min-height: 40px !important;
}
th{color:#5F5F57;}

</style>

<div style="background-color:#ffffff;">
    <div class="row-fluid" >
        <div class="span8" style="padding-left:2.5%; border-radius:10px; margin-left:18%; margin-top:30px; box-shadow: 0 2px 10px #AAAAAA;">
           

            <div style="width:95%;" >
                <h1><b>List Your Space</b></h1>
                <span style="color:#E05284; font-size:20px;">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
                        <br>Lorem ipsum dolor sit amet, consectetur adipiscing
                        <br>adipiscing elit. Quisque semper,                
                </span>
            </div>
            <br>


            <?php echo form_open_multipart(current_url()); ?>
            <div style="width:95%; margin-bottom:20px; background-color:#F6F6F6; border-radius:10px; border:2px solid #EBEBEB;"  >
                <div style="padding:15px; background-color:#EBEBEB; border-radius:10px 10px 0px 0px;">
                    <font size="+1" class="pull-left"><b>About Your Place</b></font>
                    <br><p></p>
                    <font color="#777777">
                        Nunc semper, eros ac interdum scelerisque, magna ligula<br>
                        varius risus, non sollicitudin turpis ipsum id purus. Proin eget<br>
                        quam dui. Nam condimentum nisl nec est eleifend rutrum. Nulla<br>
                        facilisi.
                    </font>    
                </div>
                <div style="padding:15px; ">
                    <table>
                        <tr><td>&nbsp;</td></tr>    
                        <tr>
                            <th>Allotment<br><br></th>
                            <td>&nbsp;</td>
                            <td>
                                <div style="width:100%;">
                                    
                                    <div style="width:40%; float:left;color:#777777">   
                                        <input class="input-block-level" value="<?php echo set_value('appliction_fee'); ?>"  type="text" placeholder="How much" name="appliction_fee"  required="required">
                                        <span><?php echo form_error('appliction_fee'); ?></span>          
                                    </div>
                                    
                                    <div style="width:5%; float:left;">&nbsp;
                                    </div>
                                    
                                    <div class="chzn-container" style="width:40%; float:left;">    
                                        <select class="chzn-single" style="height:40px;padding:10px;color:#777777" name="room_allotment"  required="required">                            
                                              <option value="">Room Allot</option>
                                              <option value="1"  <?php if(set_value('room_allotment')=="1") echo "selected"; ?>>per hr.</option>
                                              <option value="2" <?php if(set_value('room_allotment')=="2") echo "selected"; ?> >per half day</option>
                                              <option value="3" <?php if(set_value('room_allotment')=="3") echo "selected"; ?> >per day</option>
                                              <option value="4"  <?php if(set_value('room_allotment')=="4") echo "selected"; ?> >per night</option>
                                              <option value="5"  <?php if(set_value('room_allotment')=="5") echo "selected"; ?>>per week</option>
                                              <option value="6"  <?php if(set_value('room_allotment')=="6") echo "selected"; ?> >per month</option>
                                              <option value="7"  <?php if(set_value('room_allotment')=="7") echo "selected"; ?>>per ticket</option>
                                              <option value="8"  <?php if(set_value('room_allotment')=="8") echo "selected"; ?>>per time</option>
                                              <option value="9"  <?php if(set_value('room_allotment')=="9") echo "selected"; ?>>per trip</option>
                                              <option value="10"  <?php if(set_value('room_allotment')=="10") echo "selected"; ?> >per unit</option>
                                        </select>
                                        <span><?php echo form_error('room_allotment'); ?></span>
                                     </div>
                                </div>   
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>    
                        <tr>
                            <th></th>
                            <td>&nbsp;</td>
                            <td>
                                <div style="width:100%">
                                   
                                    <div style="width:40%; float:left;;color:#777777">
                                        <input class="input-block-level" value="<?php echo set_value('rent'); ?>"  type="text" placeholder="How many" name="rent" required="required">
                                        <span><?php echo form_error('rent'); ?></span>
                                    </div>
                                   
                                    <div style="width:5%; float:left;">&nbsp;
                                    </div>    
                                   
                                    <div style="width:40%; float:left;;color:#777777">
                                        <input class="input-block-level"  type="text" value="<?php echo set_value('deposit'); ?>" placeholder="security Deposit" name="deposit"  required="required">
                                        <span><?php echo form_error('deposit'); ?></span>
                                    </div>

                                </div>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><a>
                            <th>Collection category</th>
                            <td>&nbsp;</td>
                            <td>
                               <div class="chzn-container">
                                <?php $category=get_collection_category();  ?>
                                <select class="chzn-single" style="height:40px;padding:10px; width:262px;color:#777777" name="collection_category"  required="required">
                                    <option value="">Select Category</option>
                                    <?php foreach ($category as $row): ?>
                                    <option value="<?php echo $row->id; ?>" <?php if(set_value('collection_category')==$row->id) echo "selected"; ?> > <?php echo $row->category_name; ?> </option>
                                    <?php endforeach ?>
                                </select>
                             </div>
                                <span><?php echo form_error('collection_category'); ?></span>
                            </td>
                        </tr></a>
                        <tr><td>&nbsp;</td></tr>
                        <tr><a>
                            <th>Amenity</th>
                            <td>&nbsp;</td>
                            <td>
                                <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                                <div style="float:left; width:200px">
                                    <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>"> <?php echo  $row->name; ?>
                                </div>
                                <?php } ?>
                            </td>    
                        </tr></a>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <th>Property Image</th>
                            <td>&nbsp;</td>
                            <td>
                                <input id="image_button" type="file" name="userfile" required="required">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>


            <!-- <div style="width:95%; margin-bottom:20px; background-color:#F6F6F6; border-radius:10px; border:2px solid #EBEBEB;"  >
                <div style="padding:15px; background-color:#EBEBEB; border-radius:10px 10px 0px 0px;">
                    <font size="+1" class="pull-left"><b>Shown only to Confirmed Guests </b></font>
                    <br><p></p>
                    <font color="#5F5F57">
                        Nunc semper, eros ac interdum scelerisque, magna ligula<br>
                        varius risus, non sollicitudin turpis ipsum id purus. Proin eget<br>
                        quam dui. Nam condimentum nisl nec est eleifend rutrum. Nulla<br>
                        facilisi.
                    </font>    
                </div>
                <div style="padding:15px; ">
                    <table>
                        <tr><td>&nbsp;</td></tr>     -->
<!--                         <tr><a>
                            <th>User Email</th>
                            <td>&nbsp;</td>
                            <td>
                                <input style="width:400px" value="<?php //echo $user['user_email']; ?>" class="input-block-level" id="user" name="user_email" type="text"  readonly>
                                <span><?php  //echo form_error('user_email'); ?></span>
                            </td>
                        </a></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><a>
                            <th>User Name</th>
                            <td>&nbsp;</td>
                            <td>
                                <input  style="width:400px" value="<?php //echo $user['first_name']." ".$user['last_name']; ?>" id="contact_name"  class="input-block-level" type="text"  name="contact_name" readonly>
                                <span><?php //echo form_error('contact_name'); ?></span>
                            </td>
                        </a></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><a>
                            <th>User Contact</th>
                            <td>&nbsp;</td>
                            <td>
                                <input style="width:400px" class="input-block-level" type="text" value="<?php //echo set_value('phone'); ?>" id="phone" placeholder="Phone" name="phone"  required="required">
                                <span><?php //echo form_error('phone'); ?></span>
                            </td>
                        </a></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><a>
                            <th>Website URL</th>
                            <td>&nbsp;</td>
                            <td>
                                <input type="text" style="width:400px" class="input-block-level"  placeholder="Website" name="website" value="<?php //echo set_value('website'); ?>"  required="required">
                                <span><?php //echo form_error('website'); ?></span>
                            </td>
                        </a></tr>
 -->       <!--              </table>
                </div>
            </div>
         -->

            <button class="btn btn-info btn-large" style="margin-bottom:20px;" type="submit" onclick="return checkbox_valid();" >
                Save
            </button> 
            <?php echo form_close(); ?>

        </div>
    </div>
</div>