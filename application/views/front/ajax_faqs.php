       <?php if(!empty($category)): ?>
        <h1 id="category_heading"><?php echo get_faqs_category($category_id)->title ?></h1>
         <ul class="unstyled">
            <?php $i=1; ?>
           <?php foreach($category as $row): ?>
           <li >
           <input type="hidden" id="faq_id" value="<?php echo $row->id ?>">
             <a  href="javascript:void(0)" class="accordion-link zakir_faraz_zakir">
               <?php echo $i ?>. &nbsp;<?php echo $row->question ?>
             </a>
             <hr>
           </li>
               <div data-swiftype-index="true" style="display:none !important" id="faq_<?php echo $row->id ?>">
               <p style="text-align: justify;">
                  <strong><?php echo $i ?>. &nbsp;<?php echo $row->title ?></strong>
                   <?php echo $row->description; ?>
                </p>
                    <p style="text-align: center;">
                    <?php if(!empty($row->image)){?>
                      <img style="width: 641px; height: 140px; border-radius:5px;box-shadow:1px 1px 5px " src="<?php echo base_url() ?>assets/uploads/faq/<?php echo $row->image ?>" alt="">
                    <?php } else if(!empty($row->video)){ ?>
                        <iframe width="420" height="345"
                        src="http://www.youtube.com/embed/<?php echo $row->video ?>">
                        </iframe>
                    <?php } ?>
                    </p>
                    <p style="text-align: justify;">
                       &nbsp;
                    </p>
               </div>
         <?php  $i++ ?>
       <?php endforeach; ?>
         </ul>
       <?php endif; ?>
