<link href="<?php echo base_url() ?>assets/front_end_theme/css/faq.css" media="screen" rel="stylesheet" type="text/css" />

<style>
/*.breadcrumb_bar_view 
{
   background:#E05284 !important;
}*/

.breadcrumb_bar_view {
background: #F3E8EF !important;
}
.breadcrumb_bar_view a
{
   color: #666 !important;
}
.sidebar_view.span3
{
  margin-left: 117px !important;
  position: relative !important;
}
.span2.bottle1.hasDatepicker
{
   margin-left: 34px !important;
   margin-top: 33px !important;
   width: 88% !important;
   height: 57px !important;
}
.search_bar_view
{
   background-image: url('<?php echo base_url() ?>assets/img/Banner-help.jpg');
   background-size: 100%;
   height: 200px !important;
}
.offset4
{
   margin-left: 68px !important;
}
.breadcrumb_bar_view ul
{
   width: 984px !important;
}


ul.sidebar-link-list a 
{
 color:#1D95CB ;
}

.container
{

  width:1170px !important;
}

.bottle1
{
   width: 500px !important;
}  



</style>

<div class="content-wrapper">

  <!-- search div starts -->
  <div class="search_bar_view" data-view="search_bar_view">
   <div class="container">
    <div class="search-tags-container" style="visibility:hidden">
    <strong>Popular Searches:</strong>&nbsp;
    <ul class="unstyled search-tags">
    
      <li><a href="/help/search?q=references">references</a></li>
    
      <li><a href="/help/search?q=tax">tax</a></li>
    
      <li><a href="/help/search?q=payment">payment</a></li>
    
      <li><a href="/help/search?q=security deposit">security deposit</a></li>
    
      <li><a href="/help/search?q=refund">refund</a></li>
    
      <li><a href="/help/search?q=cancellation">cancellation</a></li>
    
    </ul>
  </div>
  <form class="search-form" action="<?php echo current_url() ?>" method="POST">
    <input type="text" placeholder="Search the help center" class="span2 bottle1 hasDatepicker" style="padding-left:32px;font-size:125%" name="search_faq">
  </form>
</div>
</div>

  <!-- search div ends -->

  <!-- Help Center div Starts -->

      <div class="breadcrumb_bar_view" data-view="breadcrumb_bar_view">
        <ul class="unstyled container">
          <li>
            <a data-id="0" href="/help">Help Center</a>
          </li>
       </ul>
       </div>
  <!-- Help Center div Ends -->

      <div class="container">
        <div class="row">
  
<!-- side bar div starts -->

            <div class="sidebar_view span3" data-view="sidebar_view" style="float:left !imporatant">
             <ul class="unstyled sidebar-link-list" id="sb-0-1-list" >
                 <li id="sb-0-1-1">
                      <a title="Search By Categories" href="javascript:void(0)" class="accordian-link" style="color: #939393 !important;">
                       <strong style="font-size:20px">
                          Search By Categories
                       </strong>
                      </a>
                 </li>
                 <?php $categories = get_all_faqs_categories();?>
                 <?php if(!empty($categories)): ?>
                  <?php foreach($categories as $row): ?>
                 <li id="sb-0-1-1">
                      <a title="<?php echo $row->description ?>" href="javascript:void(0)" id="<?php echo $row->id ?>" class="accordian-link zak_faraz_zak">
                   <?php echo $row->title ?></a>
                 </li>
              <?php endforeach; ?>
                <?php endif; ?>
             </ul>
          </div>

<!-- side bar div Ends -->



 <!-- Faqs Questions Starts -->
<div id="content" class="span8 offset4" style="float:left !imporatant">
     <div data-fetch_summary="{&quot;model&quot;:{&quot;model&quot;:&quot;Faq&quot;,&quot;id&quot;:&quot;252&quot;}}" data-model_id="252" data-model_name="faq" data-view="faqs/question" class="faqs_question_view" id="all_faq_list">
       <?php if(!empty($search_result)): ?>
        <h1 id="category_heading">All Search Result</h1>
         <ul class="unstyled">
            <?php $i=1; ?>
           <?php foreach($search_result as $row): ?>
           <li >
           <input type="hidden" id="faq_id" value="<?php echo $row->id ?>">
             <a  href="javascript:void(0)" class="accordion-link zakir_faraz_zakir">
               <?php echo $i." "; ?>.&nbsp; <?php echo $row->question ?>
             </a>
             <hr>
           </li>
               <div data-swiftype-index="true" style="display:none !important" id="faq_<?php echo $row->id ?>">
               <p style="text-align: justify;">
                  <strong><?php echo $i." " ?>. &nbsp;<?php echo $row->title ?></strong>
                   <?php echo $row->description; ?>
                </p>
                    <p style="text-align: center;">
                    <?php if(!empty($row->image)){?>
                      <img style="width: 641px; height: 140px; border-radius:5px;box-shadow:1px 1px 5px " src="<?php echo base_url() ?>assets/uploads/faq/<?php echo $row->image ?>" alt="">
                    <?php } else if(!empty($row->video)){ ?>
                        <iframe width="420" height="345"
                        src="http://www.youtube.com/embed/<?php echo $row->video ?>">
                        </iframe>
                    <?php } ?>
                    </p>
                    <p style="text-align: justify;">
                       &nbsp;
                    </p>
               </div>
         <?php  $i++ ?>
       <?php endforeach; ?>
         </ul>
       <?php else: ?>
        <h1 id="category_heading">All Search Result</h1>
         Nothing Found..Your Search was too specific.
       <?php endif; ?>
  </div>
</div>

 <!-- Faqs Questions Ends -->
        </div>
   </div>
<!-- main div ends -->

<script>

$(document).ready(function()
{
$('a.zak_faraz_zak').click(function()
{
        var category_id = $(this).attr('id');

        $.ajax({
              url:'<?php echo base_url() ?>front/get_faqs_categories',
              data:{category_id:category_id},
              type:'post',
              success:function(res)
                  {
                      if(res!="")
                      {
                        $('#all_faq_list').html(res);
                      }
                      else
                      {
                        $('#all_faq_list').html("Nothing Found");
                      }
                  }

             });
  $('a.zak_faraz_zak').removeClass('for_category')
  $(this).addClass('for_category');      
});

});
</script>

 <script>
    
     $(document).ready(function(){

      <?php if(!empty($trigger_id) && empty($flag)): ?>   
      $('#<?php echo $trigger_id ?>').trigger('click');
      $('#<?php echo $trigger_id ?>').addClass('for_category');
     <?php endif; ?>

     });
 </script>

<script>    

    $(document).on('click','.zakir_faraz_zakir',function(){
        var id = $(this).siblings('#faq_id').val();
     $('#faq_'+id).slideToggle();

    });


</script>

<style>
    a.zakir_faraz_zakir
    {
        text-decoration: none;
    }
    a.zak_faraz_zak
    {
        text-decoration: none;
    }
    .for_category
    {
      color: #939393 !important;
    }

</style>