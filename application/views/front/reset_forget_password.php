
<style>
#myform input{width:100%; font-size: 16px; height:30px; margin-bottom:0px;}
#myform div{background-color:#B3ABA9; padding:5px; border-radius:3px;}
</style>

<div class="container" style="margin-top:30px; min-height:450px; margin-left:30%;">    
    <div class="span12">    
        <div class="span4" id="myform">
            <div  align="center" style="background-color:#ffffff; padding:10px; color:#E05284; font-size:30px;">
                Reset Password
            </div>
            <div align="center" style="background-color:#ffffff; color:#6F7378;">
            <?php if($user->image != "") : ?>
                <img  src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="height:100px; width:100px; border-radius:5px;"><br>
            <?php else: ?>
                <img  src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:100px; width:100px; border-radius:5px;"><br> 
            <?php endif; ?><b>
            <?php echo ucwords($user->first_name)." ".ucwords($user->last_name); ?></b>
            </div>
            <br>              
        <?php echo form_open(current_url()); ?>
            <span style="display:none;"><input type="text"  ></span>
            <div>
                <input type="password" name="password" id="reset_forget_password_password"  placeholder="Password" >
            </div>
            <span style="color:#E05284;" id="reset_forget_password_pass"></span><br>
            <div>
                <input type="password" name="con_password" id="reset_forget_password_con_password"  placeholder="Confirm Password">
            </div>
            <span  style="color:#E05284;"id="reset_forget_password_con_pass"></span><br>
            
            <br>
            <div style="background-color:#ffffff;">
                <button onclick="return check_fields();" class="pull-right btn  btn-primary">Reset Password</button>
            </div>
          </div>  
        <?php echo form_close(); ?>  
    </div>
</div>



<script type="text/javascript">
    function check_fields()
    {
        var password = $("#reset_forget_password_password").val();
        var confirm_pass = $("#reset_forget_password_con_password").val();
         
        if(password==''){
          $('#reset_forget_password_pass').html('Please enter Password');
          setTimeout(function(){
            $('#reset_forget_password_pass').html('');
          },3000);
          return false;
        }
        else if(confirm_pass==''){
          $('#reset_forget_password_con_pass').html('Please enter Confirm Password');
          setTimeout(function(){
            $('#reset_forget_password_con_pass').html('');
          },3000);
          return false;
        }
        else if(confirm_pass != password){
          $('#reset_forget_password_con_pass').html('Password and Confirm Password field must be same.');
          return false;
        }
    }

  </script>

