<?php $tool_tip_json = get_row('tool_tip',array('tool_tip_page'=>'tool_on_add_property')); ?>
<?php $tool_tip = json_decode($tool_tip_json->page_data); ?>


<!-- Google Pinpoint Api Starts-->
<!-- Google Pinpoint Api Starts-->
  <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.ui.addresspicker.js"></script>
  <style type="text/css">
#map {
  border: 1px solid #DDD; 
  height: 300px;
  width: 544px;
  margin: 10px 0 10px 0;
  -webkit-box-shadow: #AAA 0px 0px 15px;
}  
th
{
  text-align: left !important;
}
</style>
  
  <div class='map-wrapper'>
    <input type="hidden" id="reverseGeocode" value="true">
  </div> 

  <script>


  function get_map_size()
  {
     setTimeout(function(){
      reinailize();
      },500);

  }

  function reinailize()
  {

  $(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
      componentsFilter: 'country:IN'
    });
    var addresspickerMap = $( "#address" ).addresspicker({
      regionBias: "fr",
      updateCallback: showCallback,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(52,11),
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements: {
        map: "#map",
        lat: "#lat",
        lng: "#lng",
        street_number: '',
        route: '#route',
        locality: '#city',
        administrative_area_level_2: '',
        administrative_area_level_1: '#state',
        country: '#country',
        postal_code: '#zip',
        type: '#type'
      }
    });

    var gmarker = addresspickerMap.addresspicker( "marker");
    gmarker.setVisible(true);
    addresspickerMap.addresspicker( "updatePosition");

      $("#address").addresspicker("option", "reverseGeocode", ($('#reverseGeocode').val() === 'true'));

    function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
      get_neighbour_city();      
    }


    var map = $("#address").addresspicker("map");
    google.maps.event.addListener(map, 'idle', function(){
      $('#zoom').val(map.getZoom());
    });

  });

 }

       function get_neighbour_city(){ 
       var city_name = $('input[name=city]').val();
        if(city_name!="")
        {
         $.ajax
         ({
              type:"post",
              url:"<?php echo base_url() ?>user/get_neighbour_city",
              data:{city_name:city_name},
              success:function(res)
           {
              if(res!="")
                {
                get_neighbourhoot(res);
                }
             else
               {
               $('#neighbourhoot').html("<option>No Neighborhoods Found</option>")
               }
           }

        });
      }
        else
        {
           return false; 
        }
    }

    function get_neighbourhoot(city_id)
    {
      var city_id = city_id ;
      $.ajax
      ({
            type:'POST',
             url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city',
            data:{city:city_id},
         success:function(res)
         {
            $('#neighbourhoot').html(res);
         }
      });
    }


  </script>

<!-- Google Pinpoint  Api Ends-->
<!-- Google Pinpoint  Api Ends-->

<!-- get address by latitude  and  longtitude Starts-->
<!-- get address by latitude  and  longtitude Starts-->
<script src="<?php echo base_url() ?>assets/pin_point_map/address_by_lat_long.js"></script>

  <script>
 
    // ResponseAddress is a callback function which will be executed after converting coordinates to an address
    
    function find_address_by_lat_long(){
      var latitude  = $('#lat').val();
      var longitude  = $('#lng').val();
     Convert_LatLng_To_Address(latitude,longitude, ResponseAddress);       
    }
 
    /*
    * Response Address
    */
    function ResponseAddress() {
        
        $('#country').val(address['country']);
        $('#city').val(address['city']);
        $('#zip').val(address['postal_code']);
        $('#address').val(address['formatted_address']);
        $('#state').val(address['province']);
        setTimeout(function(){
           get_neighbour_city();
        },2000);
    }
 
  </script>

<!-- get address by latitude  and  longtitude Endss-->
<!-- get address by latitude  and  longtitude Endss-->

<style type="text/css">
.input-block-level 
{
  min-height: 40px !important;
}
th{color:#5F5F57;}
#testr th{width:20%;}
</style>

<div style="background-color:#ffffff;">
    <div class="row-fluid" >
        <div class="span8" style="padding-left:2.5%; border-radius:10px; margin-left:18%; margin-top:30px; box-shadow: 0 2px 10px #AAAAAA;">
            <div style="width:95%;" >
                <h1><b>List Your Space</b></h1>
                <span style="color:#E05284; font-size:20px;">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
                        <br>Lorem ipsum dolor sit amet, consectetur adipiscing
                        <br>adipiscing elit. Quisque semper,                
                </span>
            </div>
            <br>
            <div style="width:95%; margin-bottom:20px; background-color:#F6F6F6; border-radius:10px; border:2px solid #EBEBEB;"  >
                <div style="padding:15px; background-color:#EBEBEB; border-radius:10px 10px 0px 0px;">
                    <font size="+1" class="pull-left"><b>About Your Place</b></font>
                    <br><p></p>
                    <font color="#5F5F57">
                        Nunc semper, eros ac interdum scelerisque, magna ligula<br>
                        varius risus, non sollicitudin turpis ipsum id purus. Proin eget<br>
                        quam dui. Nam condimentum nisl nec est eleifend rutrum. Nulla<br>
                        facilisi.
                    </font>    
                </div>
                            <div style="padding:15px; ">
                            <?php echo form_open_multipart(current_url(), array('id' => 'testr')); ?>
                                <table>
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr>
                                        <th>Property Type:
                                        <?php if(!empty($tool_tip->property_type)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->property_type ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                        <?php $property_types = get_property_types(); ?>
                                            
                                          <select required="required" name="property_type" style="width:178px;height:30px;color:#777777" class="chzn-single">
                                                <option value="">Select Property Type</option>
                                                <?php foreach ($property_types as $row):?> 
                                                <option value="<?php echo $row->id; ?>" <?php if(set_value('property_type')==$row->id) echo "selected"; ?> ><?php echo $row->property_type ?></option>
                                                <?php endforeach; ?>
                                         </select>                                        
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Accomodates:
                                        <?php if(!empty($tool_tip->accomodates)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->accomodates ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <select required="required" name="accomodates" style="width:100px;height:30px;color:#777777" class="chzn-single">
                                                <option >1</option>
                                                <option >2</option>
                                                <option >3</option>
                                                <option >4</option>
                                                <option >5</option>
                                                <option >6</option>
                                                <option >7</option>
                                                <option >8</option>
                                                <option >9</option>
                                                <option >10</option>
                                                <option >11</option>
                                                <option >12</option>
                                                <option >13</option>
                                                <option >14</option>
                                                <option >15</option>
                                                <option >16+</option>
                                         </select>                              
                                        </td>       
                                       </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Room Type:
                                        <?php if(!empty($tool_tip->room_type)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->room_type ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                        <?php $room_types = get_room_type(); ?>
                                            
                                          <select required="required" name="room_type" style="width:178px;height:30px;color:#777777" class="chzn-single">
                                                <option value="">Select Room Type</option>
                                                <?php foreach ($room_types as $row):?> 
                                                <option value="<?php echo $row->id; ?>" <?php if(set_value('room_type')==$row->id) echo "selected"; ?> ><?php echo $row->room_type ?></option>
                                                <?php endforeach; ?>
                                         </select>                                        
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Bedrooms:
                                        <?php if(!empty($tool_tip->bedrooms)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->bedrooms ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <select required="required" name="bedrooms" style="width:100px;height:30px;color:#777777" class="chzn-single">
                                                <option >1</option>
                                                <option >2</option>
                                                <option >3</option>
                                                <option >4</option>
                                                <option >5</option>
                                                <option >6</option>
                                                <option >7</option>
                                                <option >8</option>
                                                <option >9</option>
                                                <option >10</option>
                                         </select>                                        
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Title:
                                        <?php if(!empty($tool_tip->title)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->title ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input type="text" class="span10" id="hosting_name" maxlength="20" name="title" size="35" style="margin-right:2px;" required="required">                                            
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Description:
                                        <?php if(!empty($tool_tip->description)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->description ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                             <textarea class="span10" cols="40" id="hosting_description" name="description" style="height: 5em;" required="required"></textarea>                                            
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>


                                    <tr style="display:none">
                                        <th>One Time Sublet:<br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input type="checkbox" onclick="is_sublet_checked()" class="span2" name="is_sublet" value="12" id="is_sublet" style="margin-right:2px;margin-bottom:22px;">                                            
                                        </td>
                                    </tr>
                                    <tr style="display:none" id="sublet_date_div">
                                        <th><br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input type="text"  class="span4" id="sublet_from_date" name="sublet_from_date"  size="35" style="margin-right:2px;margin-bottom:22px;" placeholder="mm/dd/YY" readonly >                                            
                                            <input type="text"  class="span4" id="sublet_to_date" name="sublet_to_date"  size="35" style="margin-right:2px;margin-bottom:22px;" placeholder="mm/dd/YY" readonly >                                            
                                             <span class="form_error span10"><?php echo form_error('sublet_from_date'); ?></span>
                                             <span class="form_error span10"><?php echo form_error('sublet_to_date'); ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Price:
                                        <?php if(!empty($tool_tip->price)): ?>
                                          <span class="icon icon-question-sign" title="<?php echo $tool_tip->price ?>" ></span>
                                        <?php endif; ?>
                                        <br><span>&nbsp;</span></th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <select required="required" name="price_type" style="width:100px;height:30px;color:#777777" class="chzn-single">
                                            <option value="AUD">AUD</option>
                                            <option value="BRL">BRL</option>
                                            <option value="CAD">CAD</option>
                                            <option value="CHF">CAD</option>
                                            <option value="CZK">CZK</option>
                                            <option value="DKK">DKK</option>
                                            <option value="EUR">EUR</option>
                                            <option value="HKD">HKD</option>
                                            <option value="HUF">HUF</option>
                                            <option value="JPY">JPY</option>
                                            <option value="MYR">MYR</option>
                                            <option value="MXN">MXN</option>
                                            <option value="NOK">NOK</option>
                                            <option value="NZD">NZD</option>
                                            <option value="PLN">PLN</option>
                                            <option value="RUB">RUB</option>
                                            <option value="SGD">SGD</option>
                                            <option value="SEK">SEK</option>
                                            <option value="USD">USD</option>
                                         </select>  
                                            <input type="text" class="span2" id="price_amount" maxlength="20" name="price_amount" size="35" style="margin-right:2px;" required="required" placeholder="Amount">                                            
                                             <span style="font-size:14px;font-weight:lighter">Per Night</span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>

                               <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->
                                  <style type="text/css">
                                   a{
                                     text-decoration: none !important;
                                    }
                                    .GPS_type{
                                     display: none;
                                     }
                                    .postal_type{
                                     display: none;
                                    }
                                  </style>
                                  <script>
                                  $(document).ready(function(){

                                          $('.postal_type').show();
                                           $('.GPS_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#post_type').css('color','#DE4980');
                                           $('#gps_co').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');

                                     $('#post_type').click(function(){
                                           $('.postal_type').show();
                                           $('.GPS_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#post_type').css('color','#DE4980');
                                           $('#gps_co').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');
                                     });
                                     $('#gps_co').click(function(){
                                           $('.GPS_type').show();
                                           $('.postal_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#gps_co').css('color','#DE4980');
                                           $('#post_type').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');
                                     });
                                     $('#pin_co').click(function(){
                                           $('.pin_point_type').show();
                                           $('.GPS_type').hide();
                                           $('.postal_type').hide();
                                           $('#pin_co').css('color','#DE4980');
                                           $('#post_type').css('color','#5885AC');
                                           $('#gps_co').css('color','#5885AC');
                                     });
                                  });
                                  </script>
                                    <tr>
                                     <a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div class="active" style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="post_type" style="background-color:#F6F6F6 !important">Postal Type</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="gps_co">GPS Co-ordinate</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="pin_co" style="color:#DE4980" onclick="get_map_size()">Pin Point On Map</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                </div>
                                            </div>
                                        </td>
                                     </a>
                                    </tr>
                               <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->
                               
                               <!-- Postal Type Division Starts -->
                               <!-- Postal Type Division Starts -->
                               <!-- Postal Type Division Starts -->
                                    <td class="postal_type">&nbsp;</td>
                                    <tr class="postal_type"><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:85%;color:#777777" name="address" id="address" class="input-block-level" type="text" placeholder="Property Address" value="<?php echo set_value('address'); ?>" required="required">                 
                                            <span><?php  echo form_error('address');  ?></span> 
                                        </td>
                                    </tr></a>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type">
                                        <th class="postal_type"></th>
                                        <td class="postal_type">&nbsp;</td>
                                        <td class="postal_type">
                                            <div style="width:100%" class="postal_type">
                                                <div class="" style="width:50%; float:left;">
                                                  <input onkeyup="get_neighbour_city()" required="required" style="width:76%;float:left;margin-right:12.5%" name="city" id="city" class="input-block-level form-control" type="text" placeholder="City" value="<?php echo set_value('city') ?>">                 
                                                <span><?php echo form_error('city'); ?></span> 
                                                </div>

                                                <div class="" style="width:50%; float:left;">
                                                    <?php $country = get_country_array(); ?>
                                                    <select class="" style="width:80%; height:40px; padding:10px;color:#777777; margin-left:-10%" id="country" name="country" required="required">
                                                        <option value="">Select Country</option>
                                                        <?php foreach ($country as $code => $name): ?>
                                                        <option value="<?php echo $name; ?>" > <?php echo $name; ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span><?php echo form_error('country'); ?></span>
                                                </div>


                                                <div class="" style=" margin-left: -1%;   width: 84.5%; float:left; margin-top:2%;">
                                                    <select class="" name="neighbourhood[]" id="neighbourhoot"  style="height:75px;width:100% ;padding:8px !important;margin-left:8px !important;color:#777777" multiple="multiuple" >
                                                        <option>Select Neighborhoods</option>
                                                    </select>
                                                    <span><?php echo form_error('neighbourhood'); ?></span>
                                                </div>                                                
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:85%">
                                                <!--  -->
                                                <div style="width:30%;;color:#777777; float:left;">
                                                    <input name="state" class="input-block-level" id="state" type="text" placeholder="State" value="<?php echo set_value('state'); ?>" required="required">
                                                    <span><?php echo form_error('state'); ?></span>
                                                </div>
                                                <!--  -->    
                                                <div style="width:5%; float:left;">&nbsp;
                                                </div>    
                                                <div style="width:30%; ;color:#777777;float:left;">
                                                    <input class="input-block-level" type="text" placeholder="Postal Code" id="zip" name="zipcode" value="<?php echo set_value('zipcode'); ?>" required="required">
                                                    <span><?php echo form_error('zipcode'); ?></span>
                                                </div>

                                                <div style="width:5%; float:left;;color:#777777">&nbsp;
                                                </div>
                                                <div style="width:5%; float:left;">&nbsp;
                                                </div>
                                                <div style="width:30%; float:left;">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                             <!-- Postal Type Division Ends -->                                  
                             <!-- Postal Type Division Ends --> 

                         <!-- GPS Type Division Starts -->
                         <!-- GPS Type Division Starts -->
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px"  id="lat" class="input-block-level form-control" type="text" placeholder="Latitude" onkeyup="find_address_by_lat_long()" >                 
                                        </td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                     <td>Examples: <code>37N 46' 29.74" or +37.7749295</code></td>
                                    </tr>
                                    <tr class="GPS_type"></tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px"  id="lng" class="input-block-level form-control" type="text" placeholder="Longitude" onkeyup="find_address_by_lat_long()" >                 
                                        </td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                     <td>Example: <code>122W 25' 9.89" or - 123.41494155</code></td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                         <!-- GPS Type Division Ends -->                                  
                         <!-- GPS Type Division Ends --> 

                         <!-- Pin Point Maps Division Ends -->                                  
                         <!-- Pin Point Maps Division Ends -->                                  
                                    <tr class="pin_point_type"><td>&nbsp;</td></tr>
                                    <tr class="pin_point_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td ><div id="map"></div></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="pin_point_type"><td>&nbsp;</td></tr>
                         <!-- Pin Point Maps Division Ends -->                                  
                         <!-- Pin Point Maps Division Ends -->                                  

                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input  type="hidden" placeholder="latitude"  id="latitude"  name="latitude">
                                            <input  type="hidden" placeholder="longitude" id="longitude" name="longitude">
                                            <button type="submit" id="button"  class="btn btn-info btn-large" >
                                                Save
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                     <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<p style="margin-top:2%" style="display:none"></p>

    <script type="text/javascript">
    function is_sublet_checked ()
    {
    var res = $('#is_sublet').prop('checked');
      if(res==true)
      {
        $('#sublet_date_div').show();
      }
      else
      {
        $('#sublet_date_div').hide();
      }

    }


    </script>

<script>
$(function() {
$( "#sublet_from_date" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#sublet_to_date" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#sublet_to_date" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#sublet_from_date" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>





