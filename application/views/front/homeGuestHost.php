<script type="text/javascript">
    $(document).ready(function() {
    $('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = $(this).attr('href');
 
        // Show/Hide Tabs
        $('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        $(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
});



</script>

<style>
@charset "utf-8";
/* CSS Document */

body{
    font-family: 'Open Sans', sans-serif;
    padding: 0;
    margin: 0;
    font-size: 14px;
}

#main{
    
}

#page{
    width: 1000px;
    margin: 0 auto;
}

#content{
    float: left;
    margin-top:-81px;
    margin-bottom: 20px;
    position: relative;

}



#banner{
    float: left;
    width: 100%;
    height: 645px;
    margin-top: 15px;
    position: relative;
    background: #e05284;
}

#banner_heading {
    position: absolute;
    top: 45px;
    width: 100%;
    text-align: center;
    color: #fff;
}

#banner_heading h2{
    font-size: 32px;
    margin-bottom: 0;
}

#banner_heading p{
    font-size: 16px;
    font-weight: 400;
    text-shadow: 0px 0px 0px #FFF;
    letter-spacing: 0.5px;
    margin-top: 5px;
    line-height: 24px;
}

#banner_img{
    width: 100%;
}

#patch{
    position: absolute;
    background: rgba(0, 0, 0, 0.56);
    color: #fff;
    width: 70%;
    bottom: 22%;
    left: 15%;
}

#video1{
    
}

div#video {
    padding: 20px;
    text-align: center;
}

#text{
    padding: 48px 35px;
}

.half {
    width: 50%;
    float: left;
    box-sizing: border-box;
}

div.right_content {
position: absolute;
top: 10px;
right: 20px;
width: 48%;
color: #fff;
height: 95%;
}

div.left_content {
position: absolute;
top: 10px;
left: 20px;
width: 48%;
color: #fff;
height: 95%;
}

div.tab_divs {
position: relative;
margin-bottom: 10px;
}

.right_content span.number {
font-size: 38px;
border: 4px solid #fff;
padding: 10 20px;
border-radius: 10px;
right: 0px;
position: absolute;
bottom: 10px;
color: #fff;
}

.left_content span.number {
font-size: 38px;
border: 4px solid #fff;
padding: 10 20px;
border-radius: 10px;
left: 0px;
position: absolute;
bottom: 10px;
color: #fff;
}

#testimonials .half img {
width: 20%;
float: left;
margin-right: 20px;
border: 2px solid #fff;
box-shadow: 0px 0px 10px 2px #ccc;
}

div.vertical {
padding: 30px;
border: 1px solid #e7e7e7;
border-right: none;
float: left;
height: 351px;

}

div.half_vertical {
padding: 30px;
border: 1px solid #E7E7E7;
border-bottom: none;
float: left;
height: 145px;
}

#full_testi{
height:100%;
}

span.client_name {
float: right;
color: #6BABE7;
font-weight: 600;
font-size: 16px;
}

.vertical p {
width: 74%;
float: left;
line-height: 25px;
color: #888;
margin-top: 0;
}

.half_vertical p {
width: 74%;
float: left;
line-height: 25px;
color: #888;
margin-top: 0;
}

div.half_vertical.last {
border-bottom: 1px solid #e7e7e7;
}



/*----- Tabs -----*/
.tabs {
    width:100%;
    display:inline-block;
}

/*----- Tab Links -----*/
/* Clearfix */
.tab-links:after {
display:block;
clear:both;
content:'';
}

div#tab_heading .fa{
float: left;
font-size: 30px;
padding-top: 25px;
margin-right: 10px;
}

ul.tab-links {
margin: 0;
padding-left: 0;
height: 81px;
float: left;
} 

.tab-links li {
margin:0px;
float:left;
list-style:none;
}

.tab-title p {
margin-top: 5px;
font-weight: 300;
font-size: 14px;
}

.tab-title h2 {
margin: 0;
}

div.tab-title{
margin-top: 10px;
float: left;
width: 79%;
}

div#tab_heading {
width: 50%;
margin: 0 auto;
}

.tab-links a {
/*padding: 0px 156px;*/
display:inline-block;
border-right: 2px solid #ccc;
background:#F0F0F0;
font-size:16px;
font-weight:600;
color:#888888;
transition:all linear 0.15s;
text-decoration:none;
width: 498px;
height: 81px;
}

.tab-links a:hover {
background:#a7cce5;
text-decoration:none;
}

li.active a, li.active a:hover {
background:#fff;
color:#e05284;
}

/*----- Content of Tabs -----*/
.tab-content {
padding:0;
border-radius:3px;
box-shadow:-1px 1px 1px rgba(0,0,0,0.15);
background:#fff;
float: left;
}

.tab {
display:none;
}

.tab.active {
display:block;
}
</style>



<div id="main">
    
    <div id="banner">
            <img id="banner_img" src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/slider 01.png" />
            <div id="banner_heading">
                <h2>Find a great place to stay. Or rent out your own.</h2>
                <p>With Wimbu, you can book accomodation all over the world.<br />Or earn money by renting out our own place.</p>
            </div>
            <div id="patch">
                <div id="text" class="half">
                    <h2>How It Works ?</h2>
                    <p>over 80,000 hosts are already renting our their places on BNB Clone - why not give it a try? List your empty apartment, your spare room or your couch and earn money while you sleep!</p>
                </div>
                <div id="video" class="half" >
                    <a class="youtube"  href="https://www.youtube.com/embed/5UcYO1HdU1s?rel=0&autoplay=1" title="" target='_blank'>
                        <span></span>
                        <div class="layer"></div>
                        <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/youtube.png" id="video1"  class=''/>
                     </a>
                </div>
            </div>
        </div>
    
    <div id="page">
        
        <div id="content">
            <div class="tabs">
    <ul class="tab-links">
        <li class="active">
            <a href="#tab1">
                <div id="tab_heading">
                    <i class="fa fa-search"></i>
                    <div class="tab-title">
                        <h2>For Guest</h2>
                        <p>With BNBClone, you can book</p>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="#tab2">
                <div id="tab_heading">
                    <i class="fa fa-key"></i>
                    <div class="tab-title">
                        <h2>For Host</h2>
                        <p>With BNBClone, you can book</p>
                    </div>
                </div>
            </a>
        </li>
    </ul>
 
    <div class="tab-content">
        <div id="tab1" class="tab active">
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 02.png" />
                <div class="right_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">1</span>
                </div>
            </div>
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 05.png" />
                <div class="left_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">2</span>
                </div>
            </div>
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 03.png" />
                <div class="right_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">3</span>
                </div>
            </div>
            <div id="testimonials">
                <div id="full_testi" class="half">
                    <div class="vertical">
                        <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/client(1).jpg" />
                        <p>" It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. "</p>
                        <span class="client_name"> - Lorem Ipsum</span>
                    </div>
                </div>
                <div class="half">
                    <div class="half_vertical">
                        <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/client(2).jpg" />
                    <p>" It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "</p>
                    <span class="client_name"> - Lorem Ipsum</span>
                    </div>
                    <div class="half_vertical last">
                        <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/client(3).jpg" />
                    <p>" It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "</p>
                    <span class="client_name"> - Lorem Ipsum</span>
                    </div>
                </div>
            </div>
        </div><!-- tab1 -->
 
        <div id="tab2" class="tab">
             <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 01.png" />
                <div class="right_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">1</span>
                </div>
            </div>
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 02.png" />
                <div class="left_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">2</span>
                </div>
            </div>
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 03.png" />
                <div class="right_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">3</span>
                </div>
            </div>
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 04.png" />
                <div class="left_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">4</span>
                </div>
            </div>
            <div id="" class="tab_divs">
                <img src="http://www.bnbclone.com/demo/assets/img/homeGuestHostImages/image 05.png" />
                <div class="right_content">
                    <h2>List your Place</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <span class="number">5</span>
                </div>
            </div>
        </div><!-- tab2 -->
 
    </div><!-- tab-content -->
</div>
        </div>
        
    </div>
    
   
    
</div>
<div style="clear:both"></div>
