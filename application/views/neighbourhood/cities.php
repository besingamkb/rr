<style>
div{color: white;}
#city_font{ font-size:72px; text-shadow:3px 3px 30px black; }
#cities_font{ font-size:24px; }

@media (max-width: 400px){
#city_font{ font-size:32px; }
#cities_font{ font-size:8px; }
}
a{ text-decoration:none !important;}
#cities_font:hover{ color:#E05284; text-shadow:1px 1px 5px black; }
</style>
<div style="width:100%; margin-top:8%; margin-left:0px; position:fixed; z-index:+9999;" align="center">
	<b id="city_font">Neighbourhoods</b>
	<p>&nbsp;</p>
	<?php foreach($cities as $city): ?>
		<a href="<?php echo base_url() ?>neighbourhood/city/<?php echo $city->id; ?>">
		<div style="padding:1%; width:30%; float:left;  " >
			<b id="cities_font"><?php echo ucfirst($city->city); ?></b>
		</div>
		</a>	
	<?php endforeach; ?>	
</div>
<div style="margin-bottom:0px;" id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
 <!--  <ol class="carousel-indicators">
	  <?php //$i=0; ?>
      <?php //foreach($cities as $city): ?>   
    	<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" <?php if($i==0){ echo 'class="active"'; } ?> ></li>
      <?php //$i++; ?> 				
      <?php //endforeach; ?>	
  </ol> -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  	  <?php $i=1; ?>
      <?php foreach($neighbours as $nb): ?>
    	<div class="item <?php if($i == 1){ echo "active"; } ?>">
	    	<img style="width:100%;" src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $nb->banner; ?>"  />
	      	<div class="carousel-caption" style="color:white">
	        	<?php echo ucfirst($nb->title); ?>
	      	</div>
    	</div>
      <?php $i++; ?>		
      <?php endforeach; ?>	
  </div>

  <!-- Controls -->
  <!-- <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    &laquo;
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    &raquo;
  </a> -->
</div>