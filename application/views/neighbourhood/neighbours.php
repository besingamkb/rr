<style>
	a{
		text-decoration: none !important;
	}

	span#f_nb{
		border:1px solid #C8CBCE;
		color:#32373D;
		margin:20px;
	}

	span#f_nb:hover{
	 border:1px solid #E05284; 
	 box-shadow:1px 1px 5px #E05284 
	}

	.city{ 
		font-size:36px; 
		text-shadow:2px 2px 20px black;
	}
 
@media (max-width:750px) {
	.city{ 
		font-size:24px; 
	}
}
@media (max-width:600px) {
	.city{ 
		font-size:20px; 
	}
}
@media (max-width:400px) {
	.city{ 
		font-size:16px; 
	}
}

#f_div{ 
	background-image: url(<?php echo base_url(); ?>assets/uploads/cities/<?php echo $city->city_image; ?>); 
	margin:0px;  
	color:white; 
	width:100%;  
	height:20%;
	background-repeat: no-repeat;
	background-size: 100%;  
	background-position: bottom;
}

.subtitle_shadow{
   box-shadow:1px 1px 1px #CFD3D6;
}

#filter ul{
	list-style: none;
	margin-top:20px;
}
#filter li{
	display:inline;
}
</style>

<?php if($neighbours): ?>

<!-- header  -->
<div id="f_div" class="span12"  >
	<div style="padding-left:12%; margin-top:6%;">
		<b class="city">Find A Neighborhood In <?php echo ucwords($city->city); ?></b>
	</div>
</div>
<!-- header  -->


<!-- option bar for filter -->
<div class="subtitle_shadow" id="filter" style="padding:20px 0px 20px 12%; margin:2px 0px; background-color:#EFEFEF;">
    <p>&nbsp;</p>
    <p style="font-size:18px;">
    	What kind of neighborhood are you looking for?
    </p>
	<ul>
	<?php if(!empty($filter)): ?>
	<?php foreach($filter as $catf): ?>
		<li>
			<a href="javascript:void(0);" class="btn" > 
				<label> 
					<input class="filterlabel" name="filter[]" type="checkbox" value="<?php echo $catf->id; ?>" >
					<?php echo $catf->category_name;  ?>
				</label>
			</a>    
		</li>
	<?php endforeach; ?>
	<?php endif; ?>	
	</ul>
</div>
<script>
$(document).ready(function(){ 
	$('.filterlabel').click(function(){
		var filter = [];
		$('input[type=checkbox]:checked').each(function(){
		   filter.push( $(this).val() );
		});
		$.ajax({ 
				type:'POST',
				data:{filter:filter},
				url:'<?php echo base_url(); ?>neighbourhood/nb_ajax_filter/'+filter.length+'/'+<?php echo $city->id; ?>,
				success:function(res)
				{
					if(res == "" || res == null || res == false)
					{
						$('#dynamic_ajax_nbs').html('<br>No neighbourhood found.<br><br><br><br><br><br><br>');
					}
					else
					{
						$('#dynamic_ajax_nbs').html(res);
					}	
				}
		});
	});
});
</script>	
<!-- option bar for filter -->


<!-- filter result -->
<div style="padding-left:12%;" id="dynamic_ajax_nbs">
	<div >
	<?php $i=1; ?>
	<?php foreach($neighbours as $row): ?>
		<a href="<?php echo base_url(); ?>neighbourhood/neighbour/<?php echo $row->id; ?>">		
			<span id="f_nb" class="span3" >
				<img style="height:200px; width:100%;" src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $row->homeImage; ?>"   >
				<p style="margin-top:-25px; font-size:24px; color:white; padding-left:15px;">
					<?php echo ucwords($row->title); ?>
				</p>	
				<p></p>
				<p style="padding-left:15px;">
					<?php echo ucfirst($row->excerpt); ?>
				</p>
				<div style="padding-left:10px;">
				<?php $cats = get_nb_cat($row->id);  ?>
				<?php if(!empty($cats)): ?>
				<?php foreach($cats as $cat): ?>
					<div style="border:1px solid #BABBBC; box-shadow:1px 1px 5px #BABBBC;  border-radius:1px; margin:5px; padding:3px; background-color:#EBE9E9; float:left;">
						<?php echo $cat->category_name; ?>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>	
				</div>
				<p>&nbsp;</p>
			</span>
		</a>
		<?php if($i%3 == 0 ): ?>
			</div>
			<div>
		<?php endif; ?>
	<?php $i++; ?>	
	<?php endforeach; ?>
	</div>
</div>
<!-- filter result -->


<?php else: ?>
	<div id="neighbours" align="center" class="span12" style="background-image: url(<?php echo base_url(); ?>assets/uploads/cities/<?php echo $city->city_image; ?>);  " >
		<div style="margin-top:10%;">
			<div id="neighbours_text" align="center">
				<b>
					There is no neighborhood in <?php echo ucfirst($city->city); ?> city.
				</b>
			</div>
			<p>&nbsp;</p>
			<a href="<?php echo base_url(); ?>neighbourhood/city/<?php echo $city->id; ?>" class="btn btn-info"><b>Back</b></a>
		</div>	
	</div>	
<?php endif; ?>		


<!-- footer -->
<div class="span12 subtitle_shadow" style="margin:20px 0px 0px 0px;  width:100%; background-color:#EFEFEF; ">
	<span id="neighbours_footerline" class="span4">
		<b style="coloor:#393C3D">All Neighborhoods</b><br>
		<?php if(!empty($neighbours)): ?>
		<?php foreach($neighbours as $nb):  ?>
			<a href="<?php echo base_url(); ?>neighbourhood/neighbour/<?php echo $nb->id; ?>">
				<?php echo ucfirst($nb->title); ?>
			</a>
			<br>
		<?php endforeach; ?>
		<?php else: ?>
			There is no neighborhoods.
		<?php endif; ?>
	</span>
</div>	
<div class="container"></div>