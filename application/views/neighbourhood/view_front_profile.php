<div class="container" style="width:100% !important;height:10%;box-shadow:0 4px 3px 0 #CCCCCC;" >
</div>

<style type="text/css">
.side-bar{      
        padding-top: 5%;        
        border-right:1px solid #ccc; 
        box-shadow:inset -3px 0px 5px 0px #ccc;
        
    }

    .side-bar ul{
        padding-left: 0px;
        box-shadow: 2px 10px 6px -4px #ccc;
    }

    .side-bar ul li{
        /*font-weight: bold;*/
        list-style: none;
        text-align:center;
        /*font-size: 16px;*/
        padding: 15px;
        border-bottom: 1px solid #ccc;
    }

    .f-right{
        float:right;
    }

    .no-border{
        border-bottom: 0px !important; 
    }
    .verify_sections
    {
    box-shadow: 1px 1px 10px #D6D6D6  !important;
    }
    .verify_sections li img
    {
    margin-right: 10px !important;
    }
    .verify_sections li div div
    {
    width: 92% !important;
    float: left
    }
    .verify_sections li div span
    {
      float: left;
    }

    .verify_sections li
    {
    min-height:22px !important;
    text-align:left !important;
    font-weight: bold !important;
    color:#6E6E70;
    font-family: 'Glyphicons Halflings';
    }
    .li_head
    {
      margin-left: 15%;
      margin-right: 7%
    }
    .innner_li_content
    {
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-weight: lighter;
    }
  /*sibe bar*/

  </style>

<style>
a{ text-decoration:none !important; }
</style>

<div class="container">

<!-- Side Bar Starts  -->
    <div class="span3 side-bar" style="float:left">
      <div style="margin-right:15px">
        
        <ul>
            <li style="color:#8A8A8B !important">
                <?php $user = get_user_info($user_id); ?>
                <?php if($user->image != "") : ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="height:200px;width:200px"><br>
                <?php else: ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:150px"><br> 
                <?php endif; ?>
                <p></p>
                <p >
                    <?php echo ucfirst($user->first_name)." ".ucfirst($user->last_name); ?>
                </p>
                <?php if(!empty($user->country)): ?>
                  <p>From <?php echo $user->country ?></p>
                <?php endif; ?>
            </li>
            <li>

            </li>
        </ul>
        <br>
        <ul class="verify_sections">
            <li >
            <div>
             <span class="icon-ok li_head" ></span>
              <div style="width:50% !important">
                   Verifications
              </div>
            </div>
            </li>
            <li>
                <div  style='width:100%;'>
                 <div>
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/phone-micro.png">
                   Phone number
                 </div>
                    <span class="icon-ok" ></span>
                </div>
            </li>
            <li>
                <div  style='width:100%;'>
                 <div >
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/facebook-micro.png">
                  Facebook
                 </div>
                 <?php if(!empty($user->facebook_id)): ?>
                    <span class="icon-ok" ></span>
                 <?php else: ?>
                    <span class="icon-remove"></span>
                 <?php endif; ?>
                </div>
            </li>
            <li>
                <div  style='width:100%;'>
                 <div >
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/twitter-micro.png">
                  Twitter
                 </div>
                 <?php if(!empty($user->twitter_id)): ?>
                    <span class="icon-ok" ></span>
                 <?php else: ?>
                    <span class="icon-remove"></span>
                 <?php endif; ?>
                </div>
            </li>
            <li>
                <div  style='width:100%;'>
                 <div >
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/google-micro.png">
                  Google Plus
                 </div>
                 <?php if(!empty($user->google_id)): ?>
                    <span class="icon-ok" ></span>
                 <?php else: ?>
                    <span class="icon-remove"></span>
                 <?php endif; ?>
                </div>
            </li>
        </ul>
        <br>  
        <ul class="verify_sections">
             <?php if(!empty($groups)): ?>
            <li >
             <div>
              <img style="width:20px;height:15px;float:left;" class="li_head" src="<?php echo  base_url() ?>assets/img/group-micro.png">
               <div style="width:50% !important">
                     Groups
               </div>
             </div>
            </li>
            <li>
             <div class="innner_li_content">
               <?php foreach($groups as $row): ?>
                 <a href="<?php echo base_url() ?>properties/group_properties/<?php echo $row->id ?>"><?php echo $row->group_name ?></a> ,
              <?php endforeach; ?>
             </div>
            </li>
              <?php endif; ?>
              <?php if(!empty($user->school)): ?>
            <li>
             <div>
              <img style="width:20px;height:15px;float:left;" class="li_head" src="<?php echo  base_url() ?>assets/img/school-micro.png">
               <div style="width:50% !important">
                     School
               </div>
             </div>
            </li>
            <li>
                <div  style='width:100%;' class="innner_li_content">
                  <?php echo $user->school ?>
                </div>
            </li>
            <?php endif; ?>
            <?php if(!empty($language)): ?>
            <li >
             <div>
              <img style="width:20px;height:15px;float:left;" class="li_head" src="<?php echo  base_url() ?>assets/img/lang-micro.png">
               <div style="width:50% !important">
                     Language
               </div>
             </div>
            </li>
            <li >
              <div class="innner_li_content">
                <?php foreach($language as $row): ?>
                  <?php echo get_lang_name($row->lang_id) ?> ,
                <?php endforeach; ?>
              </div>
            </li>
            <?php endif; ?>
        </ul>
      </div>
      </div>
<!-- Side Bar Ends  -->

<style type="text/css">
    .upper_div
    {
        border-radius:3px !important;
        margin-bottom:2% !important; 
        box-shadow: 1px 1px 6px 2px #CCCCCC;
    }
    .header
    {
        height: 26px;
        border-radius: 3px !important;
    }
    .lower_div
    {
        box-shadow: 1px 1px 6px 2px #CCCCCC;
        margin-top:0.5% !important; 
        margin-bottom: 5%;
        min-height: 500px;
    }
    .under_head_div
    {
        background-color: #F5F5F5;
        border-color: #DDDDDD;
        color: #333333;
        padding: 10px 16px;
        height: 39px;
    }
   .desc_desc_desc
    {
     padding:15px 15px 15px 15px; 
     color: #868686;
    }

</style>

<!-- Main Div Ends  -->
      <div class="span8" style="float:left;margin-left:0;">
        <div class="span8 upper_div" style="margin-top:2%">
          <div class="head_div">
            <div class="header">
              <div class="span4 pull-left">
                Hey i am <?php echo $user->first_name; ?>
              </div>
              <div class="pull-left" style="margin-right: 4px;">
                Member Since <?php echo date('F j, Y', strtotime($user->created)); ?>
              </div>
            </div>
          </div>
          <?php if(!empty($user->description)): ?>
          <div class="desc_desc_desc" >
            <?php echo word_limiter($user->description,50) ?>
          </div>
          <?php endif; ?>
        </div>

        <div class="span8 lower_div" >
            <div class="">
                <div class="under_head_div"> <!-- heading -->
                  <ul class="nav nav-pills nav-stacked">
                    <li class="default">
                      <a href="javascript:void(0);">
                        <span class="badge pull-right"><?php echo $total_rows; ?></span>
                        <i class="icon-list"></i>
                        Listings
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="-body row" id="prop_list"> <!-- body -->
                    <?php if($properties): ?>
                        <?php foreach($properties as $row): ?>
                            <div class="property span9">
                                <div class="span2 pull-left" style="width:26%">
                                  <img src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $row->featured_image; ?>" alt="Not Available" class="image-thumbnail">
                                </div>
                                <div class="span5  pull-left">
                                  <div class="prop-data span4"><?php echo $row->title; ?></div>
                                  <div style="word-wrap:break-word"  class="prop-data span4"><?php echo word_limiter($row->unit_street_address,10); ?></div>
                                  <div class="span6" style="margin-left:0">
                                     <div class="prop-data span2"><?php echo get_property_type($row->property_type_id); ?></div><div class="prop-data span2"><?php if($row->accommodates > 1){ echo $row->accommodates.' Guests'; }else{ echo $row->accommodates.' Guest'; }?></div>
                                  </div>
                                  <div class="span6" style="margin-left:0">
                                     <div class="prop-data span2"><?php echo $row->square_feet; ?> sqft.</div><div class="prop-data span2"><?php echo $row->currency_type; ?> <?php echo number_format($row->application_fee, 2); ?></div>
                                  </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <input id="total_rows" type="hidden" value="<?php echo $total_rows; ?>">
                <?php if($total_rows > 2): ?>
                    <div class="row load-footer">
                            <a href="javascript:void(0);" id="load_more_prop" class="btn btn-primary pull-right">View More</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
      </div>
<!-- Main Div Ends  -->

    </div><!-- /.container -->

    <script type="text/javascript">
        $('#load_more_prop').click(function(){
          var offset = $('.property').length;
           var user_id  = <?php echo $user_id ?>;   
              $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>properties/ajax_load_property/'+offset+'/'+user_id,            
                success: function (data) 
                {

                    if(data)
                    {
                        $('#prop_list').append(data);
                    }
                    else
                    {
                        $('#load_more_prop').hide();
                    }
                    if($('.property').length >= $("#total_rows").val()){
                      $('#load_more_prop').hide();
                    }
                 }
              });
          });
    </script>

    <style type="text/css">
      .header{
          background-color: #33AEBD !important;
          border-radius: 0;
          color: #FFFFFF;
          font-size: 20px;
          margin: 0;
          padding: 20px;
          text-decoration: none;
      }

      .nav-stacked > li {
         background-color: #D6D6D6 !important;
         color: #999 !important;
         font-size: 15px !important;
         border: 2px solid #B8B8B8;
         border-radius: 9px;
      }

      .nav-stacked > li >a {
         background-color: #D6D6D6 !important;
         color: #999 !important;
         height: 17px;
         font-size: 15px !important;
      }

      .nav-stacked > li > a:hover, .nav > li > a:focus {
          text-decoration: none;
          background-color: #D6D6D6 !important;
          color: #999 !important;
          font-size: 15px !important;
      }

      .property{
        margin-left: 2%;
        padding: 2%;
      }

      .prop-data{
        padding: 2%;
      }
      .load-more{
         float: right;
      }
      .load-footer{
          margin: 2% !important;
          padding: 2% !important;
      }
      .image-thumbnail
      {
      background-color: #FFFFFF;
      border: 1px solid #DDDDDD;
      border-radius: 4px;
      display: inline-block;
      line-height: 1.42857;
      padding: 4px;
      width:225px;
      height: 175px;
      transition: all 0.2s ease-in-out 0s;
      }
      .image-thumbnail:hover
      {
       cursor: pointer;

      }

      .glyphicon-list:before
      {
       content: "\e056";
      }

 
    </style>



