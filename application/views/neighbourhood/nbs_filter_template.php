<div>
	<?php $i=1; ?>
	<?php foreach($neighbours as $row): ?>
		<a href="<?php echo base_url(); ?>neighbourhood/neighbour/<?php echo $row->id; ?>">		
			<span id="f_nb" class="span3" >
				<img style="height:200px; width:100%;" src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $row->homeImage; ?>"   >
				<p style="margin-top:-25px; font-size:24px; color:white; padding-left:15px;">
					<?php echo ucwords($row->title); ?>
				</p>	
				<p></p>
				<p style="padding-left:15px;">
					<?php echo ucfirst($row->excerpt); ?>
				</p>
				<div style="padding-left:10px;">
				<?php $cats = get_nb_cat($row->id);  ?>
				<?php if(!empty($cats)): ?>
				<?php foreach($cats as $cat): ?>
					<div style="border:1px solid #BABBBC; box-shadow:1px 1px 5px #BABBBC;  border-radius:1px; margin:5px; padding:3px; background-color:#EBE9E9; float:left;">
						<?php echo $cat->category_name; ?>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>	
				</div>
				<p>&nbsp;</p>
			</span>
		</a>
		<?php if($i%3 == 0 ): ?>
			</div>
			<div>
		<?php endif; ?>
	<?php $i++; ?>	
	<?php endforeach; ?>
</div>	