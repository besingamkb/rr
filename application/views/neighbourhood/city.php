<style>
	a{ 
		text-decoration:none !important;
	}
	.city{ 
		font-size:64px; 
	}
	.known_for{ 
		font-size:24px;
		line-height: 26px; 
	}

@media (max-width:500px) {
	.city{ 
		font-size:32px; 
	}
	.known_for{ 
		font-size:12px; 
	}
}
@media (max-width:300px) {
	.city{
	 font-size:24px; 
	}
	.known_for{
	 font-size:8px; 
	}
}
span#f_nb{
	border:1px solid #C8CBCE;
	color:#32373D;
}
span#f_nb:hover{
 border:1px solid #E05284; 
 box-shadow:1px 1px 5px #E05284 
}
</style>
<!-- <div align="center" class="span12" style="margin:0px; width:100%;  height:80%; background-size: 100%; background-repeat: inherit; background-image: url(<?php echo base_url(); ?>assets/uploads/cities/<?php echo $city->city_image; ?>);  " >
	<div style="margin-top:10%;">
		<div align="center" class="city" style="color:white;  text-shadow:2px 2px 20px black; "><b><?php echo ucfirst($city->city); ?></b></div>
		<p>&nbsp;</p>
		<div align="center" class="known_for" style="color:white; width:40%;  text-shadow:2px 2px 20px black;"><b><?php echo ucfirst($city->known_for); ?></b></div>
		<p>&nbsp;</p>
		<div align="center"><a href="<?php echo base_url(); ?>neighbourhood/city/<?php echo $city->id; ?>/neighbours" class="btn btn-info"><b>Find a neighbourhood</b></a></div>
	</div>
</div>
 -->
<div align="center" class="span12" style="margin:0px; width:100%;  height:80%;   " >
<img style="height:100%; width:100%; z-index:-999;" src="<?php echo base_url(); ?>assets/uploads/cities/<?php echo $city->city_image; ?>" >
	<div id="neighbour_city">
		<div align="center" class="city" style="color:white;  text-shadow:2px 2px 20px black; "><b><?php echo ucfirst($city->city); ?></b></div>
		<p>&nbsp;</p>
		<div align="center" class="known_for" style="color:white; width:40%; "><?php echo ucfirst($city->city_description); ?></div>
		<p>&nbsp;  </p>
		<div align="center"><a href="<?php echo base_url(); ?>neighbourhood/city/<?php echo $city->id; ?>/neighbours" class="btn btn-info"><b>Find a neighborhood</b></a></div>
	</div>
</div>

 <div id="city_content" class="span12 row" style=" ">
	<div class="span2">&nbsp;</div>
	<div class="span2">
		<b style="color:#606468;">Get around with</b>
		<p style="font-size:20px;">
			<b><?php echo ucfirst($city->get_around_with); ?></b>
		</p>
	</div>
	<div class="span2">
		<b style="color:#606468;">Places to stay</b>
		<div style="font-size:20px;">
			<?php 
				$count = get_pr_count_of_city($city->city);
				echo $count;
			?>
		</div>
		<div>
			<?php if($count == 0): ?>
				No listing found.
			<?php else: ?>
				<?php echo form_open(base_url().'properties/index', array('id'=>'see_listing', 'target'=>'_blank') ); ?>	 
					<input type="hidden" value="<?php echo $city->city; ?>" name="location">
					<a onclick="see_listing()" href="javascript:void(0);">See <?php echo $count; ?> listings »</a>
					<script>
						function see_listing()
						{
							$('#see_listing').submit();
						}
					</script>
				<?php echo form_close(); ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="span4" style="color:#606468;">
		<b>Known For</b>
		<p><?php echo ucfirst($city->known_for); ?></p>
	</div>
	<div class="span2">&nbsp;</div>
</div>

<span class="span12" >
	<span class="span4">&nbsp;</span>	
	<span id="featured_neigh" class="span4">
		<b>Featured Neighborhoods</b>
	</span>
</span>

<div class="container">
	<?php if(!empty($featured_nb)): ?>
		<div class="span12 row">
		<!--<div class="span1">&nbsp;</div>-->
		<?php $i=1; ?>
		<?php foreach($featured_nb as $row): ?>
		<a href="<?php echo base_url(); ?>neighbourhood/neighbour/<?php echo $row->id; ?>">		
			<span id="f_nb" class="span3" >
				<img style="height:200px; width:100%;" src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $row->homeImage; ?>"   >
				<p style="margin-top:-25px; font-size:24px; color:white; padding-left:15px;">
					<?php echo ucwords($row->title); ?>
				</p>	
				<p></p>
				<span class="span3" style="margin:0px !important;" >
					<p style="padding:5px"><?php echo ucfirst($row->excerpt); ?></p>
				</span><p></p>
			</span>
		</a>
			<?php if($i%3 == 0 ): ?>
				</div>
				<p>&nbsp;</p>
				<div class="span12 row">
				<div class="span1">&nbsp;</div>
			<?php endif; ?>
		<?php $i++; ?>	
		<?php endforeach; ?>
		<div class="span1">&nbsp;</div>
		</div>
	<?php else: ?>
		<span id="feature_content" class="span12" >
			<span class="span4">&nbsp;</span>	
			<span class="span4">
				There is no featured neighborhoods in current city.
			</span>
		</span>		
	<?php endif; ?>
</div>

<div align="center" style="padding:50px 0px; margin:0px; width:100%;  background-color:white; ">
	<?php if(!empty($neighbours)): ?>
	<a href="<?php echo base_url(); ?>neighbourhood/city/<?php echo $city->id; ?>/neighbours" class="btn btn-info">
		<b>More neighborhoods »</b>
	</a>
	<?php endif; ?>
</div>

<div class="span12" style="margin:0px; width:100%; background-color:#EFEFEF; ">
	<span class="span2" style="margin:0px;">&nbsp;</span>
	<span id="all_neigh" class="span4">
		<b style="coloor:#393C3D">All Neighborhoods</b><br>
		<?php if(!empty($neighbours)): ?>
		<?php foreach($neighbours as $nb):  ?>
			<a href="<?php echo base_url(); ?>neighbourhood/neighbour/<?php echo $nb->id; ?>">
				<?php echo ucfirst($nb->title); ?>
			</a>
			<br>
		<?php endforeach; ?>
		<?php else: ?>
			There is no neighborhoods in current city.
		<?php endif; ?>
	</span>
</div>	