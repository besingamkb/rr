<style>
#map{
	clear:both; 
	border:1px solid #E05284;
	box-shadow: 1px 1px 10px #E05284; 
	width:73%; 
	height:380px; 
	margin-left:14%;
}

	a{ 
		text-decoration:none !important;
	}
	.city{ 
		font-size:56px; 
		text-shadow:2px 2px 20px black;
	}
	.known_for{ 
		font-size:20px; 
	}

@media (max-width:500px) {
	.city{ 
		font-size:28px; 
	}
	.known_for{ 
		font-size:10px; 
	}
}
@media (max-width:300px) {
	.city{
	 font-size:24px; 
	}
	.known_for{
	 font-size:8px; 
	}
}
span#f_nb{
	border:1px solid #C8CBCE;
	color:#32373D;
}
span#f_nb:hover{
 border:1px solid #E05284; 
 box-shadow:1px 1px 5px #E05284 
}
</style>
<div class="span12" style="margin:0px;  color:white; width:100%;  height:75%;  " >
	<img style="width:100%; height:100%;" src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $neighbour->banner; ?>" >
	<div id="neighbour_title">
		<b class="city"><?php echo ucwords($neighbour->title); ?></b>
		<br><br>
		<b class="known_for"><?php echo ucfirst($neighbour->excerpt); ?></b>
	</div>
</div>


<div id="neighbour_desc">
	<span class="span7">
		<?php echo ucwords($neighbour->description); ?>
	</span>

	<style>
	a{ text-decoration: none !important;}
	#share_image_css_id img:hover{  box-shadow:1px 1px 5px #333333; border-radius:5px;  }
	</style>
	<span class="span4" id="share_image_css_id" style="margin:20px 20px 20px 30px;">

         <?php $facebook = get_oauth_keys('facebook') ?>
          <?php if(!empty($facebook->button_images)): ?>
          <a href="javascript:void(0)" id="fshare">
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $facebook->button_images ?>" style="height:25px;width:75px">
          <a/>
          <?php endif; ?>

         <?php $twitter = get_oauth_keys('twitter') ?>
          <?php if(!empty($twitter->button_images)): ?>
          <a href="http://twitter.com/home?status=<?php echo current_url(); ?>" title="Share on Twitter" target='_blank'>
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $twitter->button_images ?>" style="width:63px; height:25px; ">
          </a>
        <?php endif; ?>

        
         <?php $google = get_oauth_keys('google') ?>
          <?php if(!empty($google->button_images)): ?>
          <a href="https://plus.google.com/share?url=<?php echo current_url(); ?>" title="share on Google+" target='_blank'>
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $google->button_images ?>" style="width:63px; height:25px; ">
          </a>
         <?php endif; ?>

         <?php $pinterest = get_oauth_keys('pinterest') ?>
          <?php if(!empty($pinterest->button_images)): ?>
          <a href="http://pinterest.com/pin/create/button/?url=<?php echo current_url() ?>" count-layout="horizontal" title="Share on Pinterest" target='_blank'>
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $pinterest->button_images ?>" style="height:25px;width:63px; border-radius:3px; ">
          </a>
        <?php endif; ?>

		<script>
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '518108701641293',
		      status     : true,
		      xfbml      : true
		    });
		  };

		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/all.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));

		$('#fshare').click(function(){
		  FB.ui(
		  {
		   method: 'feed',
		   name: 'Vacalio',
		   caption: '<?php echo $neighbour->title; ?>',
		   description: ('<?php echo word_limiter($neighbour->excerpt,5); ?>'),
		   link: '<?php echo current_url(); ?>',
		   picture: '<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $neighbour->banner; ?>'
		  },
		  function(response) {
		    if (response && response.post_id) {
		      alert('Post was published.');
		    } else {
		      alert('Post was not published.');
		    }
		  }
		);
		});
		</script>
	</span>
	<span class='span3'>
		<b>The community says:</b><br>
		<?php 
			$tag = get_nb_tag($neighbour->id); 
			if($tag)
			{
				foreach($tag as $tag)
				{
					?>
					<div style='color:#005580; float:left; width:100px; margin:5px 5px 5px 0px;'>
						<i><?php echo $tag->category_name; ?></i>,
					</div>
					<?php
				}	
			}
		?>
		<div <div style='color:#005580; float:left; width:200px; margin:5px 5px 5px 0px;'>
			<a data-toggle="modal" data-target="#myModal" href='javascript:void(0);'>+ Please suggest the a tag</a>
		</div>
	</span>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Please suggest a tag</h4>
      </div>
      <div class="modal-body">
      	<p id='tagSuccess' style='color:green;'></p>
        What word or phrase comes to mind when you think of this neighborhood?
      	<input type='text' name='tag' style='width:465px; height:35px;'>
      	<p id='tagError' style='color:#E05284;'></p>
      </div>
      <div class="modal-footer">
        <button onclick='submitTag()'  type="button" class="btn btn-primary">Submit tag</button>
      </div>
    </div>
  </div>
</div>
<script>
function submitTag()
{
	var tag = $('input[name=tag]').val();
	var nb_id = '<?php echo $neighbour->id; ?>';
	var nb = '<?php echo $neighbour->title; ?>';
	if(tag == '')
	{
		$('#tagError').html('Please enter suggested tag.');
		setTimeout(function(){
			$('#tagError').html('');
		},5000);
		return false;
	}
	else
	{
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url(); ?>neighbourhood/submitTag',
			data : {tag:tag, nb_id:nb_id, nb:nb},
			success : function(res)
			{
				$('input[name=tag]').val(null);
				$('#tagSuccess').html(res);
				setTimeout(function(){
					$('.close').trigger('click');
					$('#tagSuccess').html('');
				},3000);
			}
		});
	}	
}
</script>






	
<div class="span8" style="padding:0px 14%; margin:0px;">
<?php $places = get_property_of_neighbourhood($neighbour->id); ?>
<div id='location_div' style='display:none;'>
<?php 
	if(!empty($places))
	{
		foreach($places as $place)
		{
			$detail = property_details($place->pr_nb_id);
			if($detail)
			{
				$address = $detail->unit_street_address.'+'.$detail->city.'+'.$detail->state.'+'.$detail->country;
				echo "<p id='".$detail->id."'>".$address.'</p>';
			}
		} 
	}
	else
	{
		$address = get_city_info($neighbour->city);
		$city = urlencode($address->city);
		echo "<p id='city'>".$city.'</p>';
	} 
?>
</div>
<div class='infoWindow' style='display:none;'>
<?php
	if(!empty($places))
	{
		foreach($places as $place)
		{
			$detail = property_details($place->pr_nb_id);
			if($detail)
			{	
				?>

				<p id='pr_<?php echo $detail->id;  ?>' >
					<a href='<?php echo base_url(); ?>properties/details/<?php echo $detail->pro_id; ?>' target='_blank' >
						<?php echo $detail->title; ?>
					</a>
				</p>
				
				<?php
			}
		} 
	}
	else
	{
		$address = get_city_info($neighbour->city);
		$city = $address->city;
		echo "<p id='pr_city'>".$city.'</p>';
	} 
?>
</div>





<?php
	$city_info = get_city_info($neighbour->city);
	$city = urlencode($city_info->city);
	$url = 'http://maps.googleapis.com/maps/api/geocode/xml?address='.$city.'&sensor=true';
	$xml = simplexml_load_file($url);
	$status = $xml->status;
  	if($status=="OK")
  	{
		$latitude = $xml->result[0]->geometry->location->lat;
		$longitude = $xml->result[0]->geometry->location->lng;
	}
  	else
  	{
		$latitude = 50;
		$longitude = -10;
  	}
?>



<?php if(!empty($places)): ?>	
<?php echo form_open(base_url().'properties/index', array('target'=>'_blank', 'id'=>'nb2pr')); ?>
	<input type="hidden" value="<?php echo $neighbour->id; ?>" name="neghborhood_id">
	<a onclick="show_pr_of_nb()" href="javascript:;" class="btn btn-info">
		SEE <?php echo get_property_of_neighbourhood($neighbour->id, 1); ?> TO STAY
	</a>
<?php echo form_close(); ?>
<?php else: ?>
	<a href="javascript:;" class="btn btn-info">
		SEE <?php echo get_property_of_neighbourhood($neighbour->id, 1); ?> TO STAY
	</a>
<?php endif; ?>	
<script>
function show_pr_of_nb()
{
	$('#nb2pr').submit();
}
</script>
</div>



<!-- google map  -->
<div class="span8" id="map_heading">
<b>On the Map</b>
</div>
<br>
<div id="map">
</div>


<script>
///////////////////////////////
///// search on map  /////////
/////////////////////////////
    var map;
    var geocoder;
    var infowindow = new google.maps.InfoWindow();


    onload = initialize();
	function initialize() 
	{
        geocoder = new google.maps.Geocoder();
        var mapOptions ={
            center: new google.maps.LatLng(<?php echo $latitude; ?>,<?php echo $longitude; ?>),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);         
        $( "#location_div p" ).each(function( index ) {
            var address = $(this).html(); 
            var loc_id = $(this).attr("id");
            var latLang = findlatlang(address, loc_id);
          });     
    }
       
    function findlatlang(address, loc_id) 
    {
        var geocoder = new google.maps.Geocoder();
        var lat ='';
        var lang ='';
        geocoder.geocode( { 'address': address}, function(results, status) {
          	if (status == google.maps.GeocoderStatus.OK)
          	{
            	lat  = results[0].geometry.location.lat();
            	lang = results[0].geometry.location.lng();            
            	placeMarker(new google.maps.LatLng(lat,  lang), map, loc_id);
          	}
           	else
           	{
            	result = "Unable to find address: " + status;
          	}
        });
    }

	function placeMarker(position, map, loc_id)
	{ 

	  	var contentString = $("#pr_"+loc_id).html();
	  	var infowindow = new google.maps.InfoWindow({
	    	content: contentString
	  	});

        var marker = new google.maps.Marker({
        	position: position,
        	map: map,
        	title: 'Space'
        });

		google.maps.event.addListener(marker, 'click', function() {
		    infowindow.open(map,marker);
		});

        var lat = position.lb;
        var lng = position.mb;
        var latlng = new google.maps.LatLng(lat,lng);
        geocoder.geocode({'latLng': latlng},function(results, status) {
	          if(status == google.maps.GeocoderStatus.OK)
	          {
		            if(results[1]) 
		            {
		            } 
	          } 
	          else 
	          {
		            if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT)
		            {
		              setTimeout(function() { placeMarker(position,map); }, (200));
		            }
	          }
        });
    }
///////////////////////////////
///// search on map  /////////
/////////////////////////////
</script>


<script src="faraz//http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
</script>






<!-- footer -->
<div id="footer_links" class="span8" >
	<span class="span4" style="padding:30px 0px; margin-left:14%;">
		<b style="coloor:#393C3D">All Neighborhoods</b><br>
		<?php $neighbours  = get_neighbourhoot_of_city($neighbour->city); ?>
		<?php if(!empty($neighbours)): ?>
		<?php foreach($neighbours as $nb):  ?>
			<a href="<?php echo base_url(); ?>neighbourhood/neighbour/<?php echo $nb->id; ?>">
				<?php echo ucfirst($nb->title); ?>
			</a>
			<br>
		<?php endforeach; ?>
		<?php else: ?>
			There is no neighborhoods.
		<?php endif; ?>
	</span>
</div>	
<div class="container"></div>