<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Shirtsscore</title>	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/theme/css/export_pdf_style.css">
</head>
<body>

<div id="container">
	<h3> Members -  <?php  if(!empty($from_date)) echo  " From ".date('m/d/Y',strtotime( $from_date));?><?php if(!empty($to_date)) echo  " To ".date('m/d/Y',strtotime( $to_date));if($export_sort!="") echo " Of ".str_replace("_"," ",$export_sort);else echo " Of All "  ?></h3>

	<div id="body">
			<table width="100%" cellpadding="2" cellspacing="0" border="0">			
			<thead>
				<tr>
					<th width="10%" align="left">Name</th>
					<th width="15%" align="left">Address</th>				
					<th width="10%" align="left">Email</th>
					<th width="10%" align="left">City</th>
					<th width="10%" align="left">State</th>
					<th width="10%" align="left">Country</th>
					<th width="10%" align="">Contact</th>
					<th width="5%" align="">status</th>
					<th width="15%" align="">Created</th>

				</tr>
			</thead>
			<tbody>
				<?php if (!empty($members)): ?>
                        <?php 
         				foreach ($members as $row): ?>                         
                    <tr>
                                              
                        <td align="left"><?php echo $row->name; ?></td>                        
                        <td align="left"><?php echo $row->address; ?></td>                        
                        <td align="left"><?php echo $row->email; ?></td>                        
                        <td align="left"><?php echo $row->city; ?></td>                        
                        <td align="left"><?php echo $row->state; ?></td>                        
                        <td align="left"><?php echo $row->country; ?></td>                        
                        <td align="left"><?php echo $row->contact; ?></td>                        
                        <td align="left"><?php echo $row->status; ?></td>                        
                        <td align="left"><?php echo  date('m/d/Y', strtotime($row->created)); ?></td>                        
                    </tr>                    
                    <?php endforeach; ?>                    
                    <?php else: ?>
                    <tr>
                        <td colspan="9"> <center><strong>NO orders Found.</strong></center></td>   
                    </tr>

                    <?php endif; ?>  
			</tbody>
		</table>		
	</div>

	<div class="footer">
		
			<a href="<?php echo base_url(); ?>">vacalio.com</a>
		
	</div>
</div>

</body>
</html>