  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">

          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span>Security Deposit                   
          </div>
          <br>
        </div>
        <div class="widget-body">
          <div class="row" style="margin-left:0%;">
          </div>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

            
                  <th style="width:10%;">Booking id</th>
                  <th style="width:8%;">Host</th>
                  <th style="width:8%;">Guest</th>
                  <th style="width:8%;">Listing</th>
                  <th style="width:9%;">Deposit</th>
                  <th style="width:9%;">Booking Date</th>
                </tr>
              </thead>
              <tbody>

               <?php if(!empty($security_deposit)):?> 
               <?php  foreach ($security_deposit as $row): ?>
               <?php $pr_detail = property_details($row->property_id); //print_r($pr_detail);  ?>                      
                <tr class="gradeA">
                  <?php $owner = get_user_info($row->owner_id); ?>
                  <td><?php echo $row->booking_id ?></td>
                  <td><?php echo $row->host_name; ?></td>
                  <td><?php echo $row->guest_name; ?></td>                
                  <td><?php echo word_limiter($pr_detail->title,3); ?></td>
                  <td><?php echo $row->security_deposit; ?></td>
                  <td><?php echo date('m/d/Y', strtotime($row->created)); ?></td>
                </tr>
             <?php endforeach ?>
           <?php else: ?>
            <tr><td colspan="8">No Records Found</td></tr>
             <?php  endif ?>
            </tbody>
            </table>
           
          </div>
        </div>
      </div>
    </div>

