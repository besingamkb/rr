<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Shirtsscore</title>	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/theme/css/export_pdf_style.css">
</head>
<body>

<div id="container">
	<h3> Properties - <?php  if(!empty($from_date)) echo  " From ".date('m/d/Y',strtotime( $from_date));?><?php if(!empty($to_date)) echo  " To ".date('m/d/Y',strtotime( $to_date));if($export_pdf!="")echo " Of ".str_replace("_"," ",$export_pdf);else echo " Of All "  ?></h3>

	<div id="body">
			<table width="100%" cellpadding="2" cellspacing="0" border="0">			
			<thead>
				<tr>
					<th width="10%" align="left">Image</th>
					<th width="10%" align="left">Title</th>				
					<th width="20%" align="left">Date</th>
					<th width="30%" align="">Description</th>					
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($properties)): ?>
                        <?php 
         				foreach ($properties as $row): ?>                         
                    <tr>
                        <td align="left"><img style="width:100px;height:100px;" src="<?php echo base_url()?>assets/uploads/property/<?php echo $row->image; ?>"></td>                        
                        <td align="left"><?php echo $row->title; ?></td>                        
                        <td align="left"><?php echo  date('m/d/Y', strtotime($row->created)); ?></td>                        
                        <td align="left"><?php echo $row->description; ?></td>                        
                    </tr>                    
                    <?php endforeach; ?>                    
                    <?php else: ?>
                    <tr>
                        <td colspan="9"> <center><strong>NO orders Found.</strong></center></td>   
                    </tr>

                    <?php endif; ?>  
			</tbody>
		</table>		
	</div>

	<div class="footer">
		
			<a href="<?php echo base_url() ?>">vacalio.com</a>
		
	</div>
</div>

</body>
</html>