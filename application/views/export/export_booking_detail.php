<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php
            $currency = $this->session->userdata('currency');
             $pro_info = get_property_detail($booking->pr_id); 
              $user = get_user_info($pro_info->user_id);  ?>

              <?php $customer = get_user_info($booking->customer_id); ?>
              
            <div class="form-horizontal no-margin well" style="color:#737373; " >
              <div class="row">
                <div class="container">
                  <div class="navbar itinenarybtn">
                    <div class="navbar-inner">
                      <!-- <a class="brand" href="#">Title</a> -->
                      <ul class="nav" style="text-align: center !important;">
                          <li class="" align="Center">
                            &nbsp; &nbsp;
                               <b>Booking Detail</b>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="container " style="margin-left:1.5%">
                    <div class="span12 iti-content">
                      <div class="" style="padding-left:1%; border-bottom:1px solid #CCC">
                        <h2>Dear, <?php echo ucfirst($customer->first_name." ".$customer->last_name); ?></h2>
                        <p><b>Thank you for choosing Bnbclone.</b></p>
                      </div>

                      <div class="" style="padding-left:5%;">
                        <h2>Itinerary</h2>
                        <div class="span3">
                          <i style="font-size:22px" class="icon-calendar icon-white"></i>
                          <h5>  <?php echo date('D, F d,Y', strtotime($booking->check_in)) ?> <br> Flexible checkin time </h5>
                        </div>
                         <div class="span3">
                          <i style="font-size:22px" class="icon-calendar icon-white"></i>
                          <h5>  <?php echo date('D, F d,Y', strtotime($booking->check_out)) ?> <br> Flexible checkout time </h5>
                        </div>
                        <?php 

                          $startTimeStamp = strtotime($booking->check_in);
                          $endTimeStamp = strtotime($booking->check_out);
                          $timeDiff = abs($endTimeStamp - $startTimeStamp);
                          $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                          // and you might want to convert to integer
                          $numberDays = intval($numberDays+1);
                          // $interval = date_diff($startTimeStamp, $endTimeStamp);

                         ?>

                        <div class="span3">
                          <i style="font-size:22px" class="icon-file"></i><br>
                          <?php if(!empty($booking->security_deposit)): ?>
                             <h6> <?php echo convertCurrency($booking->security_deposit, $currency , $booking->booking_currency_type); ?> <?php echo $currency ?>  Deposit
                             </h6>
                          <?php endif; ?>
                           <h6><?php echo $numberDays; ?> Nights</h6>
                        </div>
                        </div>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 0% 4%;">
                          <div class="span5">
                              <div class="span1">
                                <i style="font-size:40px" class=" icon-home icon-white "></i>
                              </div>
                              <div class="span11">
                              <h3 style="margin-left:6%; margin-top:-1%">
                                Address 
                              </h3>
                                <p style="font-size:16px; padding-left:6%"><?php echo $pro_info->unit_street_address." ".$pro_info->city ?></p>                           
                              </div>
                              
                            </div>

                            <div class="span5">
                              <div class="span1">
                                <i style="font-size:40px" class=" icon-home icon-white "></i>
                              </div>
                              <div class="span11">
                              <h3 style="margin-left:6%; margin-top:-1%">
                                Contact
                              </h3>
                                <?php $pr_contact = get_property_contact_info($booking->pr_id); ?>
                              <?php if(!empty($pr_contact->phone)): ?>
                                <p style="font-size:16px; padding-left:6%"><i class="icon-phone"></i> &nbsp;&nbsp;<?php echo $pr_contact->phone ?></p>                           
                              <?php endif; ?>
                               <?php if(!empty($pr_contact->contact_email)): ?>
                                <p style="font-size:16px; padding-left:6%"><i class="icon-envelope"></i> &nbsp;&nbsp;<?php echo $pr_contact->contact_email ?></p>                           
                               <?php endif; ?>
                              </div>                          
                            </div>
                          </div>     


                   <?php if(!empty($pro_info->house_rules)):?>
                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-file icon-white "></i>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            House Rules
                          </h3>
                            <p style="font-size:16px">
                            <?php echo $pro_info->house_rules?>
                             </p>                                                       
                          </div> 
                      </div>
                    <?php endif; ?>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">

                        <div class="span1">
                            <i style="font-size:40px" class=" icon-file icon-white "></i>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            House Manual
                          </h3>
                            <p style="font-size:16px">
                              
                            </p>                                                       
                          </div> 
                      </div>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 0% 4%;">
                       <div class="span6" style="border-right:1px solid #CCC">
                         <div class="span1 adminicon" >
                           <!--  <i style="font-size:40px" class=" icon-star icon-white "></i> -->
                           <div class="icon" >
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%; padding-left:6%">
                            Payments
                          </h3>
                            <p style="font-size:16px; padding-left:6%">
                              The billing amount you have supplied has been billed for the full amount.The host will recieve the full payments from Bnbclone 24 hour after you check in.
                            </p>                                                       
                          </div> 
                       </div>

                       <div class="span6 ">
                         <div class="span1 adminicon">
                            <!-- <i style="font-size:40px" class=" icon-file icon-white "></i> -->
                            <div class="icon">
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%; padding-left:6%">
                           Cancellation Policies
                          </h3>
                            <p style="font-size:16px; padding-left:6%">
                              Moderate : Full refund 5 days prior to arrival, except fees.
                            </p>                                                       
                          </div> 
                       </div>
                      </div>


                        <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1 adminicon">
                            <div class="icon" >
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            Security Deposit
                          </h3>
                            <p style="font-size:16px">
                             The security deposit is held by Bnbclone. Bnbclone will only charge the amount held if an issue is reported by the host.
                            </p>                                                       
                          </div> 
                      </div>
                      
                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-phone icon-white "></i>
                          </div>
                           <div class="span5">
                          <h3 style="margin-top:-1%">
                            Customer Supports
                          </h3>
                            <p style="font-size:16px">
                              We're here to help. If you need assistance  with your reservation 
                              Please visit our help   center for urgent situation such as check-in  trouble or arriving to
                              something unexpected  please call 123-46-7890.
                            </p>                                                       
                          </div> 
                      </div>

<style type="text/css">
  #view_recip tr td+td
  {
     ;
  }
</style>

                      <div class="col-sm-10" style="margin-left:5%;">
                        <h3>Billing 
                        </h3>
                      
                        <table class="table table-bordered">
                          <tr>
                            <td>Check-in</td>
                            <td style="padding-left:200px"><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?></td>
                          </tr>
                          <tr>
                            <td>Check-out</td>
                            <td style="padding-left:200px"><?php echo date('D, F d,Y', strtotime($booking->check_out)) ?></td>
                          </tr>
                          <tr>
                            <td>Number of Nights</td>
                            <td style="padding-left:200px"><?php echo $numberDays ?></td>
                          </tr>

                           <?php 
                            $currency = $this->session->userdata('currency');
                            $total_amount = $booking->total_amount;

                               if(!empty($pro_info->cleaning_fee))
                               {
                                  $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                  $total_amount = $total_amount-$cleaning_fee;
                               }
                               if(!empty($booking->security_deposit))
                               {
                                  $total_amount = $total_amount-$booking->security_deposit;
                               }

                               $after_security_deposit_and_cleaning_fee = $total_amount;

                               $subtotal_after_coupon_if_coupon_exist = $after_security_deposit_and_cleaning_fee-$booking->commision-$booking->tax;

                           ?>



                          <?php if(!empty($booking->coupon_type)): ?>
                            
                              <?php if($booking->coupon_type=="percent"):?>
                                 
                                 <?php 
                                     $subtotal_before_coupon = ($subtotal_after_coupon_if_coupon_exist*100)/(100-$booking->coupon_amount);
                                   ?>

                                <tr>
                                  <td>Per night (approximate...) </td>
                                  <td style="padding-left:200px">
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>

                                 
                                <tr>
                                  <td>Subtotal (before coupon price apply)</td>
                                  <td style="padding-left:200px"><?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type)." ".$currency; ?> </td>
                                </tr>

                                <tr>
                                  <td>Coupon type</td>
                                  <td style="padding-left:200px"> Percent </td>
                                </tr>
                                
                                <tr>
                                  <td>Coupon Amount</td>
                                  <td style="padding-left:200px"><?php echo $booking->coupon_amount ?> % </td>
                                </tr>

                              <?php endif; ?>
                              
                              <?php if($booking->coupon_type=="amount"): ?>

                                <tr>
                                  <td>Per night (approximate...) </td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td style="padding-left:200px">
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>


                                <tr>
                                  <td>Subtotal (before coupon price apply)</td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td style="padding-left:200px">
                                  <?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type) ?>
                                  </td>
                                </tr>

                                <tr>
                                  <td>Coupon type</td>
                                  <td style="padding-left:200px"> Amount </td>
                                </tr>
                                
                                <tr>
                                  <td>Coupon Amount</td>
                                  <td style="padding-left:200px"><?php echo $booking->coupon_amount ?></td>
                                </tr>

                              <?php endif; ?>

                          <?php endif ?>

                          <tr>
                            <td>Subtotal</td>
                            <?php $subtotal = convertCurrency($subtotal_after_coupon_if_coupon_exist,$currency,$booking->booking_currency_type) ?>
                            <td style="padding-left:200px"><?php echo number_format($subtotal,2)." ".$currency ?></td>
                          </tr>

                          <tr>
                            <td>Commision fee</td>
                           <td style="padding-left:200px">
                            <?php 
                                 echo number_format($booking->commision,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <tr>
                            <td>Tax fee</td>
                               <td style="padding-left:200px">
                             <?php 
                                 echo number_format($booking->tax,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <?php if(!empty($pro_info->cleaning_fee)): ?>
                             <tr>
                               <td>Cleaning Fee</td>
                               <td style="padding-left:200px">
                               
                                  <?php $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                 
                                 /*convert into current website currency */
                                   $cleaning_fee = convertCurrency($cleaning_fee,$currency,$booking->booking_currency_type);
            
                                 /*convert into current website currency */
                                  echo number_format($cleaning_fee,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <?php if(!empty($booking->security_deposit)): ?>
                             <tr>
                               <td>Security Deposit</td>
                               <td style="padding-left:200px">
                                   <?php $security_deposit = convertCurrency($booking->security_deposit,$currency,$booking->booking_currency_type) ?>
                                  <?php echo number_format($security_deposit,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <tr>
                            <td>Total</td>
                           <?php $total_amount = convertCurrency($booking->total_amount,$currency,$booking->booking_currency_type) ?>
                            <td style="padding-left:200px"><?php echo number_format($total_amount,2).' '.$currency  ?></td>
                          </tr>
                        </table>

                      <p style="text-align:center">THANKS FOR TRAVELING WITH BNBCLONE</p>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
    border-radius: 5px;
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    margin-left: 1%;
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#737373;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>
