
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-body">
         
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:10%;">Host Name</th>
                  <th style="width:10%;">Guest Name</th>
                  <th style="width:10%;">Listing</th>
                  <th style="width:10%;">Booking Date</th>
                  <th style="width:10%;">Payment Type</th>
                  <th style="width:10%;">Payment Status</th>
                  <th style="width:10%;">Created</th>
                </tr>
              </thead>
              <tbody>
               <?php if(!empty($payments)):?> 
               <?php  foreach ($payments as $row): ?>
                <tr class="gradeA">
                  <td><?php echo $row->guest_name; ?></td>
                  <td><?php echo $row->host_name; ?></td>                
                  <td><?php echo word_limiter($row->property_title,3); ?></td>
                  <td><?php echo date('d-m-Y', strtotime($row->booking_date)); ?></td>
                  <td><?php echo $row->payment_type; ?></td>
                  <td><?php echo $row->payments_status; ?></td>
                  <td><?php echo $row->created; ?></td>
                  </td>
                </tr>
             <?php endforeach ?>
             <?php else: ?>
                  <tr><td>No Records Found.</td></tr>
             <?php  endif ?>
            </tbody>
            </table>
              <div id="data-table_info" class="dataTables_info"></div>
                </div>
              </div>
            <div class="clearfix"></div>
           
          </div>
        </div>
      </div>
    </div>

  