

<style type="text/css">
  .left_text
  {
    text-align: right !important;
    padding:10px 20px !important;
    text-transform: capitalize;
  }
  .right_text
  {
    padding-left: 200px !important;
    padding:10px 20px ;
  }
</style>
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Booking Details
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php $pro_info = get_property_detail($booking->pr_id); 
              $user = get_user_info($pro_info->user_id);
              $currency = $this->session->userdata('currency');
                ?>
            <div class="form-horizontal no-margin well" style="color:#737373; " >
              <div class="row">
                <div class="container">
                  <div class="navbar itinenarybtn">
                    <div class="navbar-inner">
                      <!-- <a class="brand" href="#">Title</a> -->
                      <ul class="nav" style="text-align: center !important;">
                        <center>
                          <li ><b>View Reciept</b></li>                        
                        </center> 
                      </ul>
                    </div>
                  </div>
                  <!-- <a href="" class="btn btn-large">Email Itinenary</a> -->
                </div>
              </div>
                <div class="row">
                  <div class="container " style="margin-left:1.5%">
              <?php $customer = get_user_info($booking->customer_id); ?>
              <?php $owner = get_user_info($booking->owner_id); ?>
              <?php 
                $startTimeStamp = strtotime($booking->check_in);
                $endTimeStamp = strtotime($booking->check_out);
                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                // and you might want to convert to integer
                $numberDays = intval($numberDays+1);
                // $interval = date_diff($startTimeStamp, $endTimeStamp);
               ?>

                  <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">                       
                      <div class="span12" style=" margin-left:0; padding:4%">
                          <h3 >
                            Customer Reciept                            
                          </h3>
                          <h4>Booking Number - #<?php echo $booking->id; ?></h4>

                        <table class="table table-bordered">
                          <tr>
                            <td class="left_text">Check-in</td>
                            <td><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?></td>
                          </tr>
                          <tr>
                            <td class="left_text">Check-out</td>
                            <td><?php echo date('D, F d,Y', strtotime($booking->check_out)) ?></td>
                          </tr>
                          <tr>
                            <td class="left_text">Number of Nights</td>
                            <td><?php echo $numberDays ?></td>
                          </tr>

                           <?php 
                            $currency = $this->session->userdata('currency');
                            $total_amount = $booking->total_amount;

                               if(!empty($pro_info->cleaning_fee))
                               {
                                  $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                  $total_amount = $total_amount-$cleaning_fee;
                               }
                               if(!empty($booking->security_deposit))
                               {
                                  $total_amount = $total_amount-$booking->security_deposit;
                               }

                               $after_security_deposit_and_cleaning_fee = $total_amount;

                               $subtotal_after_coupon_if_coupon_exist = $after_security_deposit_and_cleaning_fee-$booking->commision-$booking->tax;

                           ?>



                          <?php if(!empty($booking->coupon_type)): ?>
                            
                              <?php if($booking->coupon_type=="percent"):?>
                                 
                                 <?php 
                                     $subtotal_before_coupon = ($subtotal_after_coupon_if_coupon_exist*100)/(100-$booking->coupon_amount);
                                   ?>

                                <tr>
                                  <td class="left_text">Per night (approximate...) </td>
                                  <td class="right_text">
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>

                                 
                                <tr>
                                  <td class="left_text">Subtotal (before coupon price apply)</td>
                                  <td class="right_text"><?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type)." ".$currency; ?> </td>
                                </tr>

                                <tr>
                                  <td class="left_text">Coupon type</td>
                                  <td class="right_text"> Percent </td>
                                </tr>
                                
                                <tr>
                                  <td class="left_text">Coupon Amount </td>
                                  <td class="right_text"><?php echo $booking->coupon_amount ?> % </td>
                                </tr>

                              <?php endif; ?>
                              
                              <?php if($booking->coupon_type=="amount"): ?>

                                <tr>
                                  <td class="left_text">Per night (approximate...) </td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td class="right_text">
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>


                                <tr>
                                  <td class="left_text">Subtotal (before coupon price apply)</td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td class="right_text">
                                  <?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type) ?>
                                  </td>
                                </tr>

                                <tr>
                                  <td class="left_text">Coupon type</td>
                                  <td class="right_text"> Amount </td>
                                </tr>
                                
                                <tr>
                                  <td class="left_text">Coupon Amount</td>
                                  <td class="right_text"><?php echo $booking->coupon_amount ?></td>
                                </tr>

                              <?php endif; ?>

                          <?php endif ?>

                          <tr>
                            <td class="left_text">Subtotal</td>
                            <?php $subtotal = convertCurrency($subtotal_after_coupon_if_coupon_exist,$currency,$booking->booking_currency_type) ?>
                            <td class="right_text"><?php echo number_format($subtotal,2)." ".$currency ?></td>
                          </tr>

                          <tr>
                            <td class="left_text">Commision fee</td>
                            <td class="right_text"><?php 
                                 echo number_format($booking->commision,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <tr>
                            <td class="left_text">Tax fee</td>
                             <td class="right_text"><?php 
                                 echo number_format($booking->tax,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <?php if(!empty($pro_info->cleaning_fee)): ?>
                             <tr>
                               <td class="left_text">Cleaning Fee</td>
                               <td class="right_text">
                               
                                  <?php $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                 
                                 /*convert into current website currency */
                                   $cleaning_fee = convertCurrency($cleaning_fee,$currency,$booking->booking_currency_type);
            
                                 /*convert into current website currency */
                                  echo number_format($cleaning_fee,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <?php if(!empty($booking->security_deposit)): ?>
                             <tr>
                               <td class="left_text">Security Deposit</td>
                               <td class="right_text">
                                   <?php $security_deposit = convertCurrency($booking->security_deposit,$currency,$booking->booking_currency_type) ?>
                                  <?php echo number_format($security_deposit,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <tr>
                            <td class="left_text">Total</td>
                           <?php $total_amount = convertCurrency($booking->total_amount,$currency,$booking->booking_currency_type) ?>
                            <td class="right_text"><?php echo number_format($total_amount,2).' '.$currency  ?></td>
                          </tr>
                        </table>

                      <p style="">Bnbclone is authorized to accept Accomodation Fee on behalf of the host as its limited agent. this means that your payment obligation to the host is satisfied by your payment to Bnbclone. Any disagreements by the host regarding that payment must be settled between the host and Bnbclone</p>
                      </div>  
                         </div>

                    </div>
                  </div>
                </div>

              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    margin-left: 1%;
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#737373;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>


