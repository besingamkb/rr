            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

                  <th style="width:10%;">Payment Type</th>
                  <th style="width:10%;">Paid By</th>
                  <th style="width:10%;">Payment Reciever</th>                  
                  <th style="width:10%;">Payment Amount</th>                                                      
                  <th style="width:10%;">Payment source</th>                                                
                  <!-- <th style="width:5%;" class="hidden-phone">Due Amount</th> -->
                </tr>
              </thead>
              <tbody>

               <?php if(!empty($payments)):?> 
               <?php  foreach ($payments as $row): ?>               
                <tr class="gradeA">
                  
                  <td><?php if($row->payment_type == 1){ echo "Booking payment"; }elseif($row->payment_type == 2){ echo "Referral payment"; } ?></td>  
                  <td><?php $userinfo = get_user_info($row->paybyuser); echo $userinfo->first_name.' '.$userinfo->last_name; ?></td>                                    
                  <td><?php echo $row->first_name; ?></td>                                    
                  <td><?php echo $row->amount; ?></td>                    
                  <td><?php echo $row->paywith; ?></td>                 
                   <!-- <td> <a href="<?php //echo base_url();?>superadmin/add_booking_note/<?php echo $row->id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a></td> -->
                </tr>
             <?php endforeach ?>
           <?php else: ?>
            <tr><td>No Records Found</td></tr>
             <?php  endif ?>
            </tbody>
            </table>

