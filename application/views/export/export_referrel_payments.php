          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

            
                  <th style="width:10%;">Referred By</th>
                  <th style="width:10%;">Referred Friend</th>                  
                  <th style="width:10%;">Travel Credit Earned</th>
                  <th style="width:10%;">Travel Credit Paid</th>
                  <th style="width:10%;">Travel Credit to Pay</th>                  
                  <th style="width:10%;">Status</th>                                                
                </tr>
              </thead>
              <tbody>

               <?php if(!empty($payments)):?> 
               <?php  foreach ($payments as $row): ?>               
                <tr class="gradeA">
                  
                  <td><?php echo $row->first_name; ?></td>                                    
                  <td><?php echo $row->email; ?></td>                                                                      
                  <td>
                    <?php 
                    $credit = 0;
                      if($row->property_credit_given == 1 || $row->property_credit_given == 2){
                        $credit = $credit+10;                         
                      }

                      if($row->booking_credit_given == 1 || $row->booking_credit_given == 2){
                        $credit = $credit+10;                         
                      } 
                        echo '$'.$credit;

                    ?>
                  </td>                                   
                  <td>
                    <?php 
                    $paidcredit = 0;
                      if($row->property_credit_given == 2 && $row->booking_credit_given == 2){
                        $paidcredit = 20;
                      }else{
                        if($row->property_credit_given == 2){
                          $paidcredit = $paidcredit+10;                         
                        } 

                        if($row->booking_credit_given == 2){
                          $paidcredit = $paidcredit+10;                         
                        } 
                      }

                        echo '$'.$paidcredit;

                    ?>
                  </td>   
                  <td>
                    <?php 
                    $paycredit = 0;
                      if($row->property_credit_given == 1 && $row->booking_credit_given == 1){
                        $paycredit = 20;
                      }else{
                        if($row->property_credit_given == 1){
                          $paycredit = $paycredit+10;                         
                        } 

                        if($row->booking_credit_given == 1){
                          $paycredit = $paycredit+10;                         
                        } 
                      }

                        echo '$'.$paycredit;

                    ?>
                  </td>
                  <td><?php if ($row->is_joined == 1){ echo "Joined"; }else{ echo "Not registered"; } ?></td>                  
                   <!-- <td> <a href="<?php //echo base_url();?>superadmin/add_booking_note/<?php echo $row->id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a></td> -->
                                  </tr>
             <?php endforeach ?>
            <?php else: ?>
              <tr><td>
             No Records Found.
             </td></tr>
            <?php  endif ?>
            </tbody>
            </table>
