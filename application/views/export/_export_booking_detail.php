<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php 
             $pro_info = get_property_detail($booking->pr_id); 
              $user = get_user_info($pro_info->user_id);  ?>

              <?php $customer = get_user_info($booking->customer_id); ?>
              
            <div class="form-horizontal no-margin well" style="color:#737373; " >
              <div class="row">
                <div class="container">
                  <div class="navbar itinenarybtn">
                    <div class="navbar-inner">
                      <!-- <a class="brand" href="#">Title</a> -->
                      <ul class="nav" style="text-align: center !important;">
                          <li class="" align="Center">
                            &nbsp; &nbsp;
                               <b>Booking Detail</b>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="container " style="margin-left:1.5%">
                    <div class="span12 iti-content">
                      <div class="" style="padding-left:1%; border-bottom:1px solid #CCC">
                        <h2>Dear, <?php echo ucfirst($customer->first_name." ".$customer->last_name); ?></h2>
                        <p><b>Thank you for choosing Bnbclone.</b></p>
                      </div>

                      <div class="" style="padding-left:5%;">
                        <h2>Itinenary</h2>
                        <div class="span3">
                          <i style="font-size:22px" class="icon-calendar icon-white"></i>
                          <h5>  <?php echo date('D, F d,Y', strtotime($booking->check_in)) ?> <br> Flexible checkin time </h5>
                        </div>
                         <div class="span3">
                          <i style="font-size:22px" class="icon-calendar icon-white"></i>
                          <h5>  <?php echo date('D, F d,Y', strtotime($booking->check_out)) ?> <br> Flexible checkout time </h5>
                        </div>
                        <?php 

                          $startTimeStamp = strtotime($booking->check_in);
                          $endTimeStamp = strtotime($booking->check_out);
                          $timeDiff = abs($endTimeStamp - $startTimeStamp);
                          $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                          // and you might want to convert to integer
                          $numberDays = intval($numberDays);
                          // $interval = date_diff($startTimeStamp, $endTimeStamp);

                         ?>

                        <div class="span3">
                          <i style="font-size:22px" class="icon-file"></i><br>
                          <h5> <?php echo $pro_info->deposit_amount ?> $  Deposit <br> <?php echo $numberDays; ?> Nights</h5>
                        </div>
                        </div>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 0% 4%;">
                          <div class="span5">
                              <div class="span1">
                                <i style="font-size:40px" class=" icon-home icon-white "></i>
                              </div>
                              <div class="span11">
                              <h3 style="margin-left:6%; margin-top:-1%">
                                Lorem Ipsum Dolor Sit  
                              </h3>
                                <p style="font-size:16px; padding-left:6%"><?php echo $pro_info->unit_street_address." ".$pro_info->city ?></p>                           
                              </div>
                              
                            </div>

                            <div class="span5">
                              <div class="span1">
                                <i style="font-size:40px" class=" icon-home icon-white "></i>
                              </div>
                              <div class="span11">
                              <h3 style="margin-left:6%; margin-top:-1%">
                                Lorem Ipsum Dolor Sit  
                              </h3>
                                <?php $pr_contact = get_property_contact_info($booking->pr_id); ?>
                              <?php if(!empty($pr_contact->phone)): ?>
                                <p style="font-size:16px; padding-left:6%"><i class="icon-phone"></i> &nbsp;&nbsp;<?php echo $pr_contact->phone ?></p>                           
                              <?php endif; ?>
                               <?php if(!empty($pr_contact->contact_email)): ?>
                                <p style="font-size:16px; padding-left:6%"><i class="icon-envelope"></i> &nbsp;&nbsp;<?php echo $pr_contact->contact_email ?></p>                           
                               <?php endif; ?>
                              </div>                          
                            </div>
                          </div>     


                   <?php if(!empty($pro_info->house_rules)):?>
                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-file icon-white "></i>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            House Rules
                          </h3>
                            <p style="font-size:16px">
                            <?php echo $pro_info->house_rules?>
                             </p>                                                       
                          </div> 
                      </div>
                    <?php endif; ?>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">

                        <div class="span1">
                            <i style="font-size:40px" class=" icon-file icon-white "></i>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            House Manual
                          </h3>
                            <p style="font-size:16px">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod

                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                              <br><br>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br>
                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>                                                       
                          </div> 
                      </div>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 0% 4%;">
                       <div class="span6" style="border-right:1px solid #CCC">
                         <div class="span1 adminicon" >
                           <!--  <i style="font-size:40px" class=" icon-star icon-white "></i> -->
                           <div class="icon" >
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%; padding-left:6%">
                            Payments
                          </h3>
                            <p style="font-size:16px; padding-left:6%">
                              The billing amount you have supplied has been billed for the full amount.The host will recieve the full payments from Bnbclone 24 hour after you check in.
                            </p>                                                       
                          </div> 
                       </div>

                       <div class="span6 ">
                         <div class="span1 adminicon">
                            <!-- <i style="font-size:40px" class=" icon-file icon-white "></i> -->
                            <div class="icon">
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%; padding-left:6%">
                           Cancellation Policies
                          </h3>
                            <p style="font-size:16px; padding-left:6%">
                              Moderate : Full refund 5 days prior to arrival, except fees.
                            </p>                                                       
                          </div> 
                       </div>
                      </div>


                        <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1 adminicon">
                            <div class="icon" >
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            Security Deposit
                          </h3>
                            <p style="font-size:16px">
                             The security deposit is held by Bnbclone. Bnbclone will only charge the amount held if an issue is reported by the host.
                            </p>                                                       
                          </div> 
                      </div>
                      
                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-phone icon-white "></i>
                          </div>
                           <div class="span5">
                          <h3 style="margin-top:-1%">
                            Customer Supports
                          </h3>
                            <p style="font-size:16px">
                              We're here to help. If you need assistance  with your reservation 
                              Please visit our help   center for urgent situation such as check-in  trouble or arriving to
                              something unexpected  please call 123-46-7890.
                            </p>                                                       
                          </div> 
                      </div>

                      <div class="span12" id="view_recip" style="margin-left:54px">
                        <?php 
                            $currency = $this->session->userdata('currency');
                            if (!empty($booking->total_amount)):
                              $accomodation_fee = convertCurrency($booking->total_amount, $currency);
                              $service_fee      = convertCurrency(5, $currency);
                              if(!empty($booking->due_amount)){
                              $due_fee          = convertCurrency($booking->due_amount, $currency);
                              }
                              else{
                              $due_fee="";                         
                              }
 
                           ?>
                        <h3>Billing <span class="pull-right" style="font-size:14px"></span></h3>
                        <table class="table table-bordered">
                          <tr>
                            <td>ACCOMODATIONS</td>
                            <td style="padding-left:300px"><?php echo $accomodation_fee." ".$currency ?></td>
                          </tr>
                          <tr>
                            <td>SERVICE FEE</td>
                            <td style="padding-left:300px"><?php echo $service_fee." ".$currency ?></td>
                          </tr>

                          <tr>
                            <td>Total</td>
                            <td style="padding-left:300px"><?php echo $total = $accomodation_fee + $service_fee ?> <?php echo $currency  ?></td>
                          </tr>

                          <tr>
                            <td>PAID AMOUNT</td>
                            <td style="padding-left:300px"><?php echo $total-$due_fee ?> <?php echo $currency  ?></td>
                          </tr>

                        </table>
                         <?php endif; ?>

                      <p style="text-align:center">THANKS FOR TRAVELING WITH Bnbclone</p>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
    border-radius: 5px;
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    margin-left: 1%;
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#737373;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>
