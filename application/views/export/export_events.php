            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:15%">Event</th>
                  <th style="width:15%">Start date</th>
                  <th style="width:15%">End date</th>
                  <th style="width:15%">Start time</th>
                  <th style="width:15%">End time</th>
                  <th style="width:15%">Created</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($events)): ?>
                    <?php $i=1; foreach ($events as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->title; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->startDate)); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->endDate)); ?></td>
                        <td><?php echo $row->startTime; ?></td>
                        <td><?php echo $row->endTime; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="4"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
