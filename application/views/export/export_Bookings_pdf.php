<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title></title>	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/theme/css/export_pdf_style.css">
</head>
<body>

<div id="container">
	<h3> Bookings -  <?php  if(!empty($from_date)) echo  " From ".date('m/d/Y',strtotime( $from_date));?><?php if(!empty($to_date)) echo  " To ".date('m/d/Y',strtotime( $to_date));if($export_sort!="") echo " Of ".str_replace("_"," ",$export_sort);else echo " Of All "  ?></h3>

	<div id="body">
			<table width="100%" cellpadding="2" cellspacing="0" border="0">			
			<thead>
				<tr>
					<th width="10%" align="left">Name</th>
					<th width="10%" align="left">Address</th>				
					<th width="20%" align="left">Email</th>
					<th width="15%" align="">Contact</th>
					<th width="15%" align="">Created</th>

				</tr>
			</thead>
			<tbody>
				<?php if (!empty($bookings)): ?>
                        <?php 
         				foreach ($bookings as $row): ?>  

                    <tr>
                                              
                        <td align="left"><?php echo $row->booking_number; ?></td>                        
                        <td align="left"><?php echo $row->address; ?></td>                        
                        <td align="left"><?php echo $row->email; ?></td>                        
                        <td align="left"><?php echo $row->contact; ?></td>                        
                        <td align="left"><?php echo  date('m/d/Y', strtotime($row->created)); ?></td>                        
                    </tr>                    
                    <?php endforeach; ?>                    
                    <?php else: ?>
                    <tr>
                        <td colspan="9"> <center><strong>NO orders Found.</strong></center></td>   
                    </tr>

                    <?php endif; ?>  
			</tbody>
		</table>		
	</div>

	<div class="footer">
		
			<a href="<?php echo base_url() ?>">bnbclone.com</a>
		
	</div>
</div>

</body>
</html>