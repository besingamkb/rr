 
 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Add Blog Category</h4>
        </div><!-- page-header -->
        <?php alert() ?>
         <?php echo form_open_multipart(current_url()); ?>
          <div class="row-fluid">
          <div class="span8">               
             
                <div class="control-group">
                  <label class="control-label" for="">Category Name</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="category" value="<?php echo set_value('category') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('category') ?></span>
                </div>
                
                <div class="control-group">
                  <label class="control-label" for="">Excerpt</label>
                  <div class="controls">
                    <textarea  class="span12" rows="5" name="excerpt"><?php echo set_value('excerpt'); ?></textarea>
                  </div>  
                  <span style="color:red"><?php echo form_error('excerpt') ?></span>               
                </div>               
               
                <div class="control-group">
                  <label class="control-label" for=""></label>
                  <div class="controls">
                    <input  class="btn btn-primary" type="submit" value="submit">
                  </div>
                  
                </div>
            </div> <!-- span8 -->

          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->
