 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Property Note
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Title
                </label>
                <div class="controls controls-row span6">
                  <input name="title" class="span12" type="text" placeholder="Title" value="<?php echo set_value('title'); ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="description" class="span12" rows="5" placeholder="Description" ><?php echo set_value('description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>
                </div>
              </div>

           
              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>