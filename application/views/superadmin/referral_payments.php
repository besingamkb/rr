
<?php if($this->session->flashdata('success_msg')): ?>
    <div class="alert alert-success">
      <?php echo $this->session->flashdata('success_msg');?>
    </div>
  <?php endif ?>
  <?php if($this->session->flashdata('error_msg')): ?>
    <div class="alert alert-error">
      <?php echo $this->session->flashdata('error_msg');?>
    </div>
  <?php endif ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">

          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> <a href="<?php echo base_url() ?>superadmin/referral_payments">Referral Payments </a>                   

          </div>
         <div class="pull-right">
            <a href="<?php echo base_url();?>superadmin/payments" class="btn">Booking payments</a>                      
            <a href="<?php echo base_url();?>superadmin/referral_payments" class="btn">Referral payments</a>                      
            <a href="<?php echo base_url();?>superadmin/transaction_history" class="btn">Transaction History</a>                      
          </div>
        </div>
        <div class="widget-body">
          <div class="row" style="margin-left:0%;">
          <div class="span2">           
              <?php //$attributes = array('name'=>'myForm'); echo form_open(base_url().'superadmin/bookings',$attributes); ?>
                <select onchange="return submit_sort();"  class="span10" id="sort" name="sort"> 
                  <option value="newest" <?php if($this->uri->segment(3) == 'newest') echo "selected='selected'";  ?> >Newest</option>                  
                  <option value="oldest" <?php if($this->uri->segment(3) == 'oldest') echo "selected='selected'";  ?> >Oldest</option>                  
                  <option value="refund" <?php if($this->uri->segment(3) == 'refund') echo "selected='selected'";  ?> >Refund Payments</option>                  
                  <option value="paid_to_owner" <?php if($this->uri->segment(3) == 'paid_to_owner') echo "selected='selected'";  ?> >Paid to owner</option>                  
                </select>
              <?php //echo form_close(); ?>    
          </div>
          </div>
          <?php echo form_open(base_url().'superadmin/referral_payments'); ?>
                <div class="row" style="margin-left:0%;">
                  <?php  $attributes = array('name'=>'search_form'); echo form_open(current_url(),$attributes);?>
                    <div class="input-append">
      <!--            <input id="appendedInput" name='email' class="span2" placeholder="By Email" type="text">
      -->             <input id="srch" name='name' class="span2" placeholder="search" type="text">
                      <button class="btn" id="search" onclick="return form_submit_name();" type="submit">Search</button>
                    </div>
                </div>
            <?php echo form_close(); ?>

 <!--  <div style="" class="row-fluid">
      <h4>Export Payments</h4><br>
      <?php echo form_open(base_url().'export/export_bookings'); ?>

        <div class="row-fluid">
          <input type="hidden" name="type" value="superadmin">
            <div class="controls">
                <div class="input-append">
                    <input id="date_range1" class="span2 date_picker" type="text" placeholder="Select Date" name="report_range2">
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select  name="export_sort"> 
                    <option value="">All</option>
                    <option value="Pending_Bookings">Pending Bookings</option>
                    <option value="Pending_Check-in">Pending Check-in</option>
                    <option value="Pending_Check-out">Pending Check-out</option>
                    <option value="Cancelled_Bookings">Cancelled Bookings</option>
                  </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
          <span class="inline radio"><input type="radio" name="export_file_format" value="csv">CSV</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" value="excel">Excel</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</span>
          <br><br>
          <span ><input type="submit" value="Export" class="btn-info btn">
        </div>  
                  <?php echo form_close(); ?>        
             </div> -->

          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

            
                  <th style="width:10%;">Referred By</th>
                  <th style="width:10%;">Referred Friend</th>                  
                  <th style="width:10%;">Travel Credit Earned</th>
                  <th style="width:10%;">Travel Credit Paid</th>
                  <th style="width:10%;">Travel Credit to Pay</th>                  
                  <th style="width:10%;">Status</th>                                                
                  <!-- <th style="width:5%;" class="hidden-phone">Due Amount</th> -->
                 <th style="width:30%;" class="hidden-phone">Actions</th>
                </tr>
              </thead>
              <tbody>

               <?php if(!empty($payments)):?> 
               <?php  foreach ($payments as $row): ?>               
                <tr class="gradeA">
                  
                  <td><?php echo $row->first_name; ?></td>                                    
                  <td><?php echo $row->email; ?></td>                                                                      
                  <td>
                    <?php 
                    $credit = 0;
                      if($row->property_credit_given == 1 || $row->property_credit_given == 2){
                        $credit = $credit+10;                         
                      }

                      if($row->booking_credit_given == 1 || $row->booking_credit_given == 2){
                        $credit = $credit+10;                         
                      } 
                        echo '$'.$credit;

                    ?>
                  </td>                                   
                  <td>
                    <?php 
                    $paidcredit = 0;
                      if($row->property_credit_given == 2 && $row->booking_credit_given == 2){
                        $paidcredit = 20;
                      }else{
                        if($row->property_credit_given == 2){
                          $paidcredit = $paidcredit+10;                         
                        } 

                        if($row->booking_credit_given == 2){
                          $paidcredit = $paidcredit+10;                         
                        } 
                      }

                        echo '$'.$paidcredit;

                    ?>
                  </td>   
                  <td>
                    <?php 
                    $paycredit = 0;
                      if($row->property_credit_given == 1 && $row->booking_credit_given == 1){
                        $paycredit = 20;
                      }else{
                        if($row->property_credit_given == 1){
                          $paycredit = $paycredit+10;                         
                        } 

                        if($row->booking_credit_given == 1){
                          $paycredit = $paycredit+10;                         
                        } 
                      }

                        echo '$'.$paycredit;

                    ?>
                  </td>
                  <td><?php if ($row->is_joined == 1){ echo "Joined"; }else{ echo "Not registered"; } ?></td>                  
                   <!-- <td> <a href="<?php //echo base_url();?>superadmin/add_booking_note/<?php echo $row->id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a></td> -->
                  
                  <td>
                    <!-- <a href="#" class="btn btn-small">Notes</a> -->
                    <?php  if ($row->property_credit_given == 1 || $row->booking_credit_given == 1): ?>                       
                      <a href="<?php echo base_url() ?>superadmin/pay_referral_amount/<?php echo $row->id; ?>/<?php echo $paycredit; ?>" onclick="return confirm('Are you sure?')"  class="btn btn-success btn-small hidden-phone" data-original-title="">Pay to User</a>
                    <?php endif  ?>                   
                    <a href="<?php echo base_url() ?>superadmin/referral_payment_detail/<?php echo $row->id; ?>" class="btn btn-small btn-primary hidden-tablet hidden-phone">
                     Details
                   </a>
                    <a href="<?php echo base_url() ?>superadmin/referral_notes/<?php echo $row->id; ?>" class="btn btn-small">
                     Notes
                   </a>
                   
                  </td>
                </tr>
             <?php endforeach ?>
             <?php  endif ?>
            </tbody>
            </table>
              <div id="data-table_info" class="dataTables_info"></div>
                <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">

                   <?php if($pagination) echo $pagination; ?>
                   <!--  <a id="data-table_first" class="first paginate_button paginate_button_disabled" tabindex="0">First</a>
                      <a id="data-table_previous" class="previous paginate_button paginate_button_disabled" tabindex="0">Previous</a>
                       <span>
                          <a class="paginate_active" tabindex="0">1</a>
                          <a class="paginate_button" tabindex="0">2</a>
                      </span>
                      <a id="data-table_next" class="next paginate_button" tabindex="0">Next</a>
                    <a id="data-table_last" class="last paginate_button" tabindex="0">Last</a> -->
                </div>
              </div>
            <div class="clearfix"></div>
           
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
       function form_submit(){
          var sort = document.getElementById('sort'); 
            if(sort != ''){
              $("form[name='myForm']").submit();
            }
            else{
              return false;
            }        
        }


        function submit_sort(){
          var val = $('#sort').val();
          window.location='<?php echo base_url() ?>superadmin/referral_payments/'+val;
        }

function form_submit_name(){
var name  = $('#srch').val();
if(name == '')
{
  alert('Please Enter some value');
  return false;
}
else
{
 return true;
}        
}
    </script>

