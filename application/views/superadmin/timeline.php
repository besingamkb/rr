

        <div class="main-container">

          <div class="row-fluid">
            <div class="span8">
              <div class="widget no-margin">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe047;"></span> Timeline
                  </div>
<a href='<?php echo base_url(); ?>superadmin/events' class='pull-right btn btn-success' style=''>Events</a>
                </div>
                <div class="widget-body">
                  <div class="timeline clearfix">
                    <div class="mid-line"></div>
                    <div class="time-header info">
                      Today
                    </div>

                    <?php if(!empty($events)): ?>
                    <?php $i = 1; ?>
                    <?php foreach($events as $event ): ?>
                        <?php 
                        // get usefull parameter 
                            $time   = time();
                            $start  = strtotime($event->startDate.' '.$event->startTime);
                            $end    = strtotime($event->endDate.' '.$event->endTime);
                            $status = $event->status;
                        ?>     
                    <?php 
                        $class  = '';
                        if($status == 1 )
                        {//complete
                            $class  = 'text-success';
                        }
                        elseif($time > $start and $time < $end and $status != 1)
                        {//process
                            $class  = 'text-info';
                        }
                        elseif($time < $start and $status != 1)
                        {// new
                            $class  = 'text-warning';
                        }
                        else
                        {// due
                            $class  = 'text-error';
                        }
                     ?>

                     <?php 
                     // find out today task
                        $timeline  = strtotime(date('Y-m-d'));
                        $startdate = strtotime($event->startDate);

                      ?>
                              <?php if($timeline == $startdate): ?>
                                        <?php if($i%2 == 1 ): ?>
                                        <div class="left">
                                          <div class="l-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?>
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>                                  
                                                 @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                              <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php else: ?>
                                        <div class="right">
                                          <div class="r-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?> 
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>
                                                @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                                <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php endif; ?> 
                              <?php $i++; ?> 
                              <?php endif; ?>           
                  <?php endforeach; ?>
                  <?php else: ?>
                      There is no record for today.
                  <?php endif; ?>



      

           
                    <div class="time-header completed">
                      Yesterday
                    </div>

                    <?php if(!empty($events)): ?>
                    <?php $i = 1; ?>
                    <?php foreach($events as $event ): ?>
                        <?php 
                        // get usefull parameter 
                            $time   = time();
                            $start  = strtotime($event->startDate.' '.$event->startTime);
                            $end    = strtotime($event->endDate.' '.$event->endTime);
                            $status = $event->status;
                        ?>     
                    <?php 
                        $class  = '';
                        if($status == 1 )
                        {//complete
                            $class  = 'text-success';
                        }
                        elseif($time > $start and $time < $end and $status != 1)
                        {//process
                            $class  = 'text-info';
                        }
                        elseif($time < $start and $status != 1)
                        {// new
                            $class  = 'text-warning';
                        }
                        else
                        {// due
                            $class  = 'text-error';
                        }
                     ?>

                     <?php 
                     // find out yesterday task
                        $timeline  = strtotime(date('Y-m-d').'- 1 day');
                        $startdate = strtotime($event->startDate);

                      ?>
                              <?php if($timeline == $startdate): ?>
                                        <?php if($i%2 == 1 ): ?>
                                        <div class="left">
                                          <div class="l-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?>
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>                                  
                                                 @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                              <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php else: ?>
                                        <div class="right">
                                          <div class="r-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?> 
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>
                                                @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                                <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php endif; ?> 
                              <?php $i++; ?> 
                              <?php endif; ?>           
                  <?php endforeach; ?>
                  <?php else: ?>
                      There is no record for today.
                  <?php endif; ?>


<div class="time-header error">
  Last week
</div>

                   <?php if(!empty($events)): ?>
                    <?php $i = 1; ?>
                    <?php foreach($events as $event ): ?>
                        <?php 
                        // get usefull parameter 
                            $time   = time();
                            $start  = strtotime($event->startDate.' '.$event->startTime);
                            $end    = strtotime($event->endDate.' '.$event->endTime);
                            $status = $event->status;
                        ?>     
                    <?php 
                        $class  = '';
                        if($status == 1 )
                        {//complete
                            $class  = 'text-success';
                        }
                        elseif($time > $start and $time < $end and $status != 1)
                        {//process
                            $class  = 'text-info';
                        }
                        elseif($time < $start and $status != 1)
                        {// new
                            $class  = 'text-warning';
                        }
                        else
                        {// due
                            $class  = 'text-error';
                        }
                     ?>

                     <?php 
                     // find out last month task
                        $sweek  = strtotime(date('Y-m-d').'- 7 days');
                        $eweek  = strtotime(date('Y-m-d'));
                        $startdate = strtotime($event->startDate);

                      ?>
                              <?php if($startdate>$sweek & $startdate<$eweek): ?>
                                        <?php if($i%2 == 1 ): ?>
                                        <div class="left">
                                          <div class="l-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?>
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>                                  
                                                 @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                              <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php else: ?>
                                        <div class="right">
                                          <div class="r-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?> 
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>
                                                @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                                <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php endif; ?> 
                              <?php $i++; ?> 
                              <?php endif; ?>           
                  <?php endforeach; ?>
                  <?php else: ?>
                      There is no record for today.
                  <?php endif; ?>

          


                    <div class="time-header info">
                      Last month
                    </div>

                    <?php if(!empty($events)): ?>
                    <?php $i = 1; ?>
                    <?php foreach($events as $event ): ?>
                        <?php 
                        // get usefull parameter 
                            $time   = time();
                            $start  = strtotime($event->startDate.' '.$event->startTime);
                            $end    = strtotime($event->endDate.' '.$event->endTime);
                            $status = $event->status;
                        ?>     
                    <?php 
                        $class  = '';
                        if($status == 1 )
                        {//complete
                            $class  = 'text-success';
                        }
                        elseif($time > $start and $time < $end and $status != 1)
                        {//process
                            $class  = 'text-info';
                        }
                        elseif($time < $start and $status != 1)
                        {// new
                            $class  = 'text-warning';
                        }
                        else
                        {// due
                            $class  = 'text-error';
                        }
                     ?>

                     <?php 
                     // find out last month task
                        $sMonth  = strtotime(date('Y-m-d').'- 30 days');
                        $eMonth  = strtotime(date('Y-m-d'));
                        $startdate = strtotime($event->startDate);

                      ?>
                              <?php if($startdate>$sMonth & $startdate<$eMonth): ?>
                                        <?php if($i%2 == 1 ): ?>
                                        <div class="left">
                                          <div class="l-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?>
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>                                  
                                                 @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                              <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php else: ?>
                                        <div class="right">
                                          <div class="r-arrow"></div>
                                          <div class="about-task">
                                            <div class="header">
                                              <?php echo word_limiter($event->title,5); ?> 
                                              <small class="<?php echo $class; ?>">
                                                <?php echo date('d F',strtotime($event->startDate));  ?>
                                                @<?php echo $event->startTime; ?>
                                              </small>
                                            </div>
                                            <p class="no-margin">
                                                <?php echo $event->description; ?>
                                            </p>
                                          </div>
                                        </div>
                                        <?php endif; ?> 
                              <?php $i++; ?> 
                              <?php endif; ?>           
                  <?php endforeach; ?>
                  <?php else: ?>
                      There is no record for today.
                  <?php endif; ?>

          
                    





                  </div>
                </div>
              </div>
            </div>

            <div class="span4">
              <div class="widget no-margin">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe022;"></span> Tasks
                  </div>
                </div>
                <div class="widget-body">
                  <ul class="tasks" style="overflow:auto;max-height:1000px">
                    <!-- <span class='completed/new/process'> -->
                    <?php if(!empty($events)): ?>
                    <?php foreach($events as $event ): ?>
                        <?php 
                        // get usefull parameter 
                            $time   = time();
                            $start  = strtotime($event->startDate.' '.$event->startTime);
                            $end    = strtotime($event->endDate.' '.$event->endTime);
                            $status = $event->status;
                        ?>     
                    <?php 
                    // findout class name out of above 3 classes.
                        $class  = '';
                        $remark = '';
                        $delete = '';
                        if($status == 1 )
                        {//complete
                            $class  = 'text-success';
                        }
                        elseif($time > $start and $time < $end and $status != 1)
                        {//process
                            $class  = 'text-info';
                        }
                        elseif($time < $start and $status != 1)
                        {// new
                            $class  = 'text-warning';
                        }
                        else
                        {// due
                            $class  = 'text-error';
                        }
                     ?>
                    <li>
                      <!-- <img src="img/avatar-1.png" class="avatar" alt="Avatar"> -->              
                      <div class="message-date">
                        <h3 class="date <?php echo $class; ?>"><?php echo date('d',strtotime($event->startDate));  ?></h3>
                        <p class="month"><?php echo date('F',strtotime($event->startDate));  ?></p>
                      </div>
                      <div class="message-wrapper"  style='margin-left:0px !important;'>
                        <h4 class="message-heading"><?php echo $event->title; ?></h4>
                        <p class="message">
                           <?php echo $event->description; ?>
                        </p>
                      </div>
                    </li>
                  <?php endforeach; ?>
                  <?php else: ?>
                      There is no task in Todo list.
                  <?php endif; ?>
                </ul>
              </div>
            </div>
          </div>
        </div> 
      </div>
   