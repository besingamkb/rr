<div id="mainnav" class="hidden-phone hidden-tablet">
      <ul>
           <?php if(uri_string() == 'superadmin/dashboard'): ?>
        <li class="active">
              <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>

          <a href="<?php echo base_url(); ?>superadmin/dashboard">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
            </div>
            Dashboard
          </a>
        </li>
         <?php if(uri_string() == 'superadmin/timeline'): ?>
        <li class="active">
            <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/timeline">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Timeline
          </a>
        </li>
         <?php if(uri_string() == 'superadmin/cities'): ?>
        <li class="active">
            <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/cities">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0ce;"></span>
            </div>
            Cities
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/neighbourhoods'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>superadmin/neighbourhoods">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe062;"></span>
            </div>
            Neighborhoods
          </a>
        </li>
        <?php if(uri_string() == 'superadmin/bookings'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>superadmin/bookings">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Bookings
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/properties'): ?>
        <li class="active">
            <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/properties">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Properties
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/payments'): ?>
        <li class="active">
             <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/payments">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Payments
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/members'): ?>
        <li style="display:none;"><!-- style="display:none;" -->
          <span class="current-arrow"></span>
          <?php else: ?>
            <li style="display:none;">
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/members">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Members
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/pages'): ?>
          <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/pages">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            CMS
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/cancellation_policies'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/cancellation_policies">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Cancellation Policies
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/collection'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/collection">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Collections
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/groups'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/groups">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Groups
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/featured_images'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/featured_images">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="▓"></span>
            </div>
            Featured Images
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/notifications'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/notifications">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Notifications
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/help'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>superadmin/help">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="?"></span>
            </div>
           Help
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/reviews'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/reviews">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Reviews
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/messages'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/messages">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Messages
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/manageEmail'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/manageEmail">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Manage Email
          </a>
        </li>
          <?php if(uri_string() == 'superadmin/settings'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>superadmin/settings">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Settings
          </a>
        </li>
      </ul>
    </div>
    
    <div class="dashboard-wrapper">
      <div class="main-container">
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Bookings</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Properties</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/payments">Payments</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Payments</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Member Management</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">CMS</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Cancellation Policies</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Collection</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Group</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Notifications</a>
                  </li> 
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Review</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Message</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>superadmin/dashboard">Setting</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        