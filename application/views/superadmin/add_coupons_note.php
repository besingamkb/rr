   
    <div class="row-fluid">
     <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Coupons Note
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Coupons Name
                </label>
                <div class="controls controls-row span6">
                  <input name="coupons_name" readonly="readonly" class="span12" type="text" placeholder="Coupons Name" value="<?php echo $coupons->name ?>">
                  <span class="form_error span12"><?php echo form_error('coupons_name'); ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Note Title
                </label>
                <div class="controls controls-row span6">
                  <input name="title" class="span12" type="text" placeholder="Note Title" value="<?php echo set_value('title'); ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Coupons Note
                </label>
                <div class="controls controls-row span6">
                  <textarea name="note" class="span12" rows="5" placeholder="Coupons Note" ><?php echo set_value('note'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('note'); ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>