
<!DOCTYPE html>
  <!--[if lt IE 7]>
    <html class="lt-ie9 lt-ie8 lt-ie7" lang="en">
  <![endif]-->

  <!--[if IE 7]>
    <html class="lt-ie9 lt-ie8" lang="en">
  <![endif]-->

  <!--[if IE 8]>
    <html class="lt-ie9" lang="en">
  <![endif]-->

  <!--[if gt IE 8]>
    <!-->
    <html lang="en">
    <!--
  <![endif]-->

  <head>
    <meta charset="utf-8">
    <title>Bnbclone Superadmin Login</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon_image.ico">
    <meta name="author" content="Srinu Basava">
    <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
    <meta name="description" content="White Label Admin Admin UI">
    <meta name="keywords" content="White Label Admin, Admin UI, Admin Dashboard, Srinu Basava">
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/html5-trunk.js"></script>
    <link href="<?php echo base_url(); ?>assets/icomoon/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    <style type="text/css">
        .signin h1{
          color:#C2C2C2 !important;
        }
        .signin .signin-wrapper .actions a:hover {
            color: #FF0000 !important;
        }
    </style>
    <!--[if lte IE 7]>
      <script src="css/icomoon-font/lte-ie7.js"></script>
    <![endif]-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-40301843-2', 'iamsrinu.com');
      ga('send', 'pageview');

    </script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span4 offset4">
          <div class="signin">
            <h1 class="center-align-text">Login</h1>
            <?php echo form_open(current_url(),array('class' => 'signin-wrapper')); ?>
              <div class="content">
                <span class="help-block" style="color:white"><?php alert()?></span>
                <input class="input input-block-level" placeholder="Email" name="email" type="email" value="superadmin@bnb.com">
                <span class="help-block" style="color:white"><?php echo form_error('email')?></span>
                <input class="input input-block-level" placeholder="Password" name="password" type="password" value="superadmin">
                <span class="help-block" style="color:white"><?php echo form_error('password')?></span>
              </div>
              <div class="actions">
                <input class="btn btn-info pull-right" type="submit" value="Login">
                <span class="checkbox-wrapper" style="display:none">
                  <a href="#" class="pull-left">Forgot Password</a>
                </span>
                <div class="clearfix"></div>
              </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>