 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Review
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Select Property 
                <?php $property = get_property_table(); ?>
                </label>
                <div class="controls controls-row span8">
                  <select id="demo"  name="pr_id" class="span4" required="required">
                    <option value="">Select property title</option>
                    <?php foreach($property as $row): ?>
                        <option value="<?php echo $row->id; ?>" ><?php echo $row->title; ?></option>
                    <?php endforeach; ?>    
                  </select>
                  <span class="form_error span12"><?php echo form_error('pr_id'); ?></span>
                </div>
              </div>

            <div class="control-group">
                <label id="title" class="control-label">&nbsp;
                </label> 
                <div id="image" class="wysiwyg-container controls controls-row span8">
                </div>
            </div>
<script>
$(document).ready(function(){ 
    $('#demo option').click(function(){ 
        var title = $(this).text();
        var id    = $(this).val();
        $.ajax({  
                type : "POST",
                data : {id:id},
                url  : '<?php echo base_url() ?>superadmin/ajax_property_detail/'+id,
                success : function(res)
                {
                    $('#title').html("<a  href='<?php echo base_url(); ?>properties/details/"+id+"'  target='_blank'>"+title+"</a>");
                    $('#image').html("<img alt=' ' src='<?php echo base_url(); ?>assets/uploads/property/"+res+"' style='width:250; height:80px; border-radius:15px; border:5px solid #ffffff;'>");
                }
        });    
    });
});
</script>

 
             <div class="control-group">
                <label class="control-label">
                Review
                </label>
                <div class="wysiwyg-container controls controls-row span8">
                    <!-- review system -->
                    <!-- review system -->
                    <!-- review system -->
                    <link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
                    <div style="padding:10px 0px 20px 0px;" id="rating_div">
                    <input class="star required" type="radio" name="rating" value="1"   >
                    <input class="star" type="radio" name="rating" value="2"   >
                    <input class="star" type="radio" name="rating" value="3"   >
                    <input class="star" type="radio" name="rating" value="4"   >
                    <input class="star" type="radio" name="rating" value="5"   >
                    </div>
                    <script> 
                    $(function(){ $('#rating_div :radio.star').rating(); });
                    </script>
                    <script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
                    <!-- review system -->
                    <!-- review system -->
                    <!-- review system -->
                </div>
              </div>


             <div class="control-group">
                <label class="control-label">
                Review
                </label>
                <div class="wysiwyg-container controls controls-row span8">
                  <textarea name="review" class="input-block-level no-margin" placeholder="Enter review text ..." style="height: 140px"><?php echo set_value('review'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('review'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Status
                </label>
                <div class="controls controls-row span8">
                  <select name="status" class="span4" required="required">
                        <option value="1"  >Pubslish</option>
                        <option value="0"  >Unpublish</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('status'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
