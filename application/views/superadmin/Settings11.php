        <div class="row-fluid">
            <div class="span6">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Password Information
                  </div>
                </div>
                <div class="widget-body">
                  <?php pass_alert(); ?>
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <h5 class="text-info">Change Password </h5>
                    <hr>
                   
                    <div class="control-group">
                      <label class="control-label">
                       Old Password
                      </label>
                      <div class="controls">
                        <input type="password" name="old_password" placeholder="Old Password">
                        <span class="form_error span12"><?php echo form_error('old_password'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                      New  Password
                      </label>
                       <div class="controls">
                       <input type="password" name="new_password" placeholder="New Password">
                       <span class="form_error span12"><?php echo form_error('new_password'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                        Confirm Password
                      </label>
                      <div class="controls">
                       <input type="password" name="con_password" placeholder="Confirm Password" >
                       <span class="form_error span12"><?php echo form_error('con_password'); ?></span>
                      </div>
                    </div>
                    <div class="form-actions no-margin">
                      <input type="submit" name="save_password" value="save" class="btn btn-info">
                    
                      
                    </div>
                  <?php echo form_close() ?>
                </div>
              </div>
            </div>

            <div class="span6">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"> Profile </span> 
                  </div>
                </div>
                <div class="widget-body">
                  <?php profile_alert(); ?>
                <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <h5 class="text-info">Personal Information</h5>
                    <hr>
                    <div class="control-group">
                      <label class="control-label">
                        First Name
                      </label>
                      <div class="controls">
                       <input type="text" name="first_name" value="<?php echo $user->first_name; ?>" placeholder="Old Password" >
                        <span class="form_error span12"><?php echo form_error('first_name'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                        Last Name
                      </label>
                      <div class="controls">
                       <input type="text" name="last_name" value="<?php echo $user->last_name; ?>" placeholder="Old Password" >
                      <span class="form_error span12"><?php echo form_error('last_name'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                       Address
                      </label>
                      <div class="controls">
                       <input type="text" name="address" value="<?php echo $user->address;  ?>" placeholder="Address" >
                      <span class="form_error span12"><?php echo form_error('address'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                       User Email
                      </label>
                      <div class="controls">
                        <input type="text" name="user_email" value="<?php echo $user->user_email;?>" placeholder="User Email" >
                     <span class="form_error span12"><?php echo form_error('user_email'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                        Phone
                      </label>
                      <div class="controls">
                       <input type="text" name="phone" value="<?php echo $user->phone;?>" placeholder="Phone" >
                      <span class="form_error span12"><?php echo form_error('phone'); ?></span>
                      </div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">
                        City
                      </label>
                      <div class="controls">
                        <input type="text" name="city" value="<?php echo $user->city;?>" placeholder="City" >
                      <span class="form_error span12"><?php echo form_error('city'); ?></span>
                      </div>
                    </div>
                    <div class="form-actions no-margin">
                      <input type="submit" name="save_profile_info" value=" Save" class="btn btn-info">
                      
                      
                    </div>
                 <?php echo form_close() ?>
                </div>
              </div>
            </div>
          </div>