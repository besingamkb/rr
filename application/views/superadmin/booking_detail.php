<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Booking Details
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php 
             $pro_info = get_property_detail($booking->pr_id); 
              $user = get_user_info($pro_info->user_id);  ?>

              <?php $customer = get_user_info($booking->customer_id); ?>
              
            <div class="form-horizontal no-margin well" style="color:#737373; " >
              <div class="row">
                <div class="container0">
                  <div class="navbar itinenarybtn">
                    <div class="navbar-inner">
                      <!-- <a class="brand" href="#">Title</a> -->
                      <ul class="nav" style="text-align: center !important;">
                        <center>
                          <li style="padding-left:0%" class=""><i class=" icon-white icon-envelope"></i> &nbsp;&nbsp;<a href="<?php echo base_url() ?>superadmin/booking_detail_email/<?php echo $booking->id ?>/main_page"><b>Email Itinerary</b></a></li>
                          <li class=""><i class=" icon-white icon-print"></i>&nbsp; &nbsp;<a href="<?php echo base_url() ?>export/export_booking_detail/<?php echo $booking->id ?>"><b>Print Itinerary</b></a></li>
                          <li class=""><i class="  icon-file"></i>&nbsp;&nbsp;<a href="<?php echo base_url() ?>superadmin/reciept/<?php echo $booking->id ?>"><b>View Reciept</b></a></li>                        
                        </center> 
                      </ul>
                    </div>
                  </div>
                  <!-- <a href="" class="btn btn-large">Email Itinenary</a> -->
                </div>
              </div>
                <div class="row">
                  <div class="container0 " style="margin-left:1.5%">
                    <div class="span12 iti-content">
                      <div class="" style="padding-left:1%; border-bottom:1px solid #CCC">
                        <h2>Dear, <?php echo ucfirst($customer->first_name." ".$customer->last_name); ?></h2>
                        <p><b>Thank you for choosing Bnbclone. Please verify the information below and print this document for your record</b></p>
                      </div>

                      <div class="" style="padding-left:1%;">
                        <h2>Itinenary</h2>
                        <div class="span3">
                          <i style="font-size:22px" class="icon-calendar icon-white"></i>
                          <h5>  <?php echo date('D, F d,Y', strtotime($booking->check_in)) ?> <br> Flexible checkin time </h5>
                        </div>
                         <div class="span3">
                          <i style="font-size:22px" class="icon-calendar icon-white"></i>
                          <h5>  <?php echo date('D, F d,Y', strtotime($booking->check_out)) ?> <br> Flexible checkout time </h5>
                        </div>
                         <!-- <div class="span2">
                          <i style="font-size:22px" class="icon-user icon-white"></i>
                          <h5>  2 <br> Guests</h5>
                        </div> -->

                        <?php 

                          $startTimeStamp = strtotime($booking->check_in);
                          $endTimeStamp = strtotime($booking->check_out);
                          $timeDiff = abs($endTimeStamp - $startTimeStamp);
                          $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                          // and you might want to convert to integer
                          $numberDays = intval($numberDays);
                          // $interval = date_diff($startTimeStamp, $endTimeStamp);

                         ?>

                            <?php $currency =  $this->session->userdata('currency'); ?>
                             
                            <?php $deposit_amount = $pro_info->deposit_amount ?>
                        <div class="span3">
                          <i style="font-size:22px" class="icon-file"></i><br>
                          <?php if(!empty($deposit_amount)): ?>
                             <h5> <?php echo convertCurrency($deposit_amount, $currency); ?> <?php echo $currency ?>  Deposit <br> <?php echo $numberDays; ?> Nights</h5>
                          <?php else: ?>
                             <h5> 0  <?php echo $currency ?>  Deposit <br> <?php echo $numberDays; ?> Nights</h5>
                          <?php endif;  ?>
                        </div>
                        </div>
                          <div id="map" style="width:100%;height:380px;margin-top:10%"></div>

                        <div class="section0">                            
                          <div class="span5">
                              <div class="span1">
                                <i style="font-size:40px" class=" icon-home icon-white "></i>
                              </div>
                              <div class="span11">
                              <h3 style="margin-left:6%; margin-top:-1%">
                                Address  
                              </h3>
                              <?php if($pro_info->unit_street_address && $pro_info->city): ?>
                                <p style="font-size:16px; padding-left:6%"><?php echo $pro_info->unit_street_address." ".$pro_info->city ?></p>                           
                              <?php endif; ?>
                              </div>
                              
                            </div>

                            <div class="span5">
                              <div class="span1">
                                <i style="font-size:40px" class=" icon-home icon-white "></i>
                              </div>
                              <div class="span11">
                              <h3 style="margin-left:6%; margin-top:-1%">
                                Contact  
                              </h3>

                              <?php @$pr_contact = get_property_contact_info($booking->pr_id); ?>
                              <?php if(!empty($pr_contact->phone) && !empty($pr_contact->contact_email)): ?>
                                <p style="font-size:16px; padding-left:6%"><i class="icon-phone"></i> &nbsp;&nbsp;<?php echo @$pr_contact->phone ?></p>                           
                                <p style="font-size:16px; padding-left:6%"><i class="icon-envelope"></i> &nbsp;&nbsp;<?php echo @$pr_contact->contact_email ?></p>                           
                             <?php endif; ?>
                              </div>                          
                            </div>

                          </div>                      

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-file icon-white "></i>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            House Rules
                          </h3>
                     <?php if(!empty($pro_info->house_rules)):?>
                            <p style="font-size:16px">
                            <?php echo $pro_info->house_rules?>
                             </p>                                                       
                    <?php endif; ?>
                          </div> 
                      </div>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-file icon-white "></i>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            House Manual
                          </h3>
                    <?php if(!empty($pro_info->house_manual)):?>
                            <p style="font-size:16px">
                              <?php echo $pro_info->house_manual  ?>
                            </p>    
                    <?php endif; ?>                                                           
                          </div> 
                      </div>

                      <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                       <div class="span6" style="border-right:1px solid #CCC">
                         <div class="span1 adminicon" >
                           <!--  <i style="font-size:40px" class=" icon-star icon-white "></i> -->
                           <div class="icon" >
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%; padding-left:6%">
                            Payments
                          </h3>
                            <p style="font-size:16px; padding-left:6%">
                              The billing amount you have supplied has been billed for the full amount.The host will recieve the full payments from Bnbclone 24 hour after you check in.
                            </p>                                                       
                          </div> 
                       </div>
                       <div class="span6 ">
                         <div class="span1 adminicon">
                            <!-- <i style="font-size:40px" class=" icon-file icon-white "></i> -->
                            <div class="icon">
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%; padding-left:6%">
                           Cancellation Policies
                          </h3>
                            <p style="font-size:16px; padding-left:6%">
                              Moderate : Full refund 5 days prior to arrival, except fees.
                            </p>                                                       
                          </div> 
                       </div>
                      </div>


                        <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1 adminicon">
                            <div class="icon" >
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                            </div>
                          </div>
                           <div class="span10">
                          <h3 style="margin-top:-1%">
                            Security Deposit
                          </h3>
                            <p style="font-size:16px">
                             The security deposit is held by Bnbclone. Bnbclone will only charge the amount held if an issue is reported by the host.
                            </p>                                                       
                          </div> 
                      </div>


                        <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">
                        <div class="span1">
                            <i style="font-size:40px" class=" icon-phone icon-white "></i>
                          </div>
                           <div class="span5">
                          <h3 style="margin-top:-1%">
                            Customer Supports
                          </h3>
                            <p style="font-size:16px">
                              We're here to help. If you need assistance  with your reservation 
                              Please visit our help   center for urgent situation such as check-in  trouble or arriving to
                              something unexpected  please call 123-46-7890.
                            </p>                                                       
                          </div> 
                      </div>

                      <div class="span10" id="view_recip">
                        <?php 
                            $currency = $this->session->userdata('currency');
                            if (!empty($booking->total_amount)):
                              $accomodation_fee = convertCurrency($booking->total_amount, $currency);
                              $service_fee      = convertCurrency(5, $currency);
                                if(!empty($booking->due_amount))
                                {
                                   $due_fee          = convertCurrency($booking->due_amount, $currency);
                                }
                                else
                                {
                                   $due_fee="";                         
                                }
                           ?>
                        <h3>Billing <span class="pull-right" style="font-size:14px"><a href=""> View Reciept</a></span></h3>
                        <table class="table table-bordered">
                          <tr>
                            <td>ACCOMODATIONS</td>
                            <td><?php echo $accomodation_fee." ".$currency ?></td>
                          </tr>
                          <tr>
                            <td>SERVICE FEE</td>
                            <td><?php echo $service_fee." ".$currency ?></td>
                          </tr>


                          <tr>
                            <td>Total</td>
                            <td><?php echo $total = $accomodation_fee + $service_fee ?> <?php echo $currency  ?></td>
                          </tr>

                          <tr>
                            <td>PAID AMOUNT</td>
                            <td><?php echo $accomodation_fee-$due_fee ?> <?php echo $currency  ?></td>
                          </tr>


                        </table>
                            <?php endif; ?>
                      <p style="text-align:center">THANKS FOR TRAVELING WITH Bnbclone</p>
                      </div>  
                    </div>
                  </div>
                </div>

             
             <!--  <div class="control-group">
                <label class="control-label">
                 Guset Name
                </label>
                <div class="controls controls-row span6">
                  <span class="form_error span12">
                   <strong>
                    <?php $customer = get_user_info($booking->customer_id); ?>
                    <?php echo $customer->first_name." ".$customer->last_name; ?>
                 </strong>
                  </span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Guest Email
                </label>
                <div class="controls controls-row span6">
                  <span class="form_error span12">
                    <?php echo $customer->user_email; ?>
                  </span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                   Guest Contact
                </label>
                <div class="controls controls-row span6">
                 
                  <span class="form_error span12">
                    <?php echo $booking->contact; ?>
                  </span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Guest Address
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12">
                    <?php echo $booking->city; ?>
                  </span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                Property Title
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $pro_info->title; ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Propety Description
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $pro_info->description; ?></span>
                </div>
              </div>


               <div class="control-group">
                <label class="control-label" for="your-name">
                 Host name
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $user->first_name.' '.$user->last_name; ?></span>
                </div>
              </div> 

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Check In
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $booking->check_in ?></span>
                </div>
              </div> 

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Check Out
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $booking->check_out; ?></span>
                </div>
              </div>                

              <div class="form-actions no-margin">
                <a href="<?php echo base_url();?>superadmin/bookings" class="btn btn-info">
                  Back To Booking
                </a>
                -->
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
    border-radius: 5px;
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    margin-left: 1%;
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#737373;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>

<?php $latitude = $pro_info->latitude ?>
<?php $longitude = $pro_info->longitude ?>
<script>
  
// var address = "<?php print_r($pro_info->unit_street_address.', '.$pro_info->city.', '.$pro_info->state); ?>";
// var address = '12 Park Street, Brooklyn, New York';
  // alert(address);
  // $(document).ready(function(){
  // });

// alert(findlatlang(address));

// var myCenter=new google.maps.LatLng(findlatlang(address));
var myCenter=new google.maps.LatLng(<?php echo $latitude ?>,<?php echo $longitude ?>);

var map;
var geocoder;
function initialize()
{
var mapProp = {
  center: myCenter,
  zoom:5,
  mapTypeId: google.maps.MapTypeId.ROADMAP
  };

var map = new google.maps.Map(document.getElementById("map"),mapProp);

var marker = new google.maps.Marker({
  position: myCenter,
  title:'Click to zoom'
  });

marker.setMap(map);

// Zoom to 9 when clicking on marker
google.maps.event.addListener(marker,'click',function() {
  map.setZoom(9);
  map.setCenter(marker.getPosition());
  });
}


// function findlatlang(address) {        
//   var geocoder = new google.maps.Geocoder();
//   var lat ='';
//   var lang ='';
//   geocoder.geocode( { 'address': address}, function(results, status) {
//     if (status == google.maps.GeocoderStatus.OK) {
//       lat  = results[0].geometry.location.A;
//       lang = results[0].geometry.location.K;      
//       // console.log(lat);
//       return lat+','+lang;
//       // map.setCenter(new google.maps.LatLng(lat,lang));
//       // placeMarker(new google.maps.LatLng(lat,  lang), map);
//       // streetviewmap();
//     } else {
//       result = "Unable to find address: " + status;
//     }
//   });
// }


google.maps.event.addDomListener(window, 'load', initialize);
</script>