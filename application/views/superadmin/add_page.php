<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
  tinyMCE.init({
    height : 400,
    convert_urls: false,
    mode : "textareas",
    editor_selector : "mceEditor",
    editor_deselector : "mceNoEditor",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'

     }); 
</script>
<!-- /TinyMCE --> 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Page
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Page Title
                </label>
                <div class="controls controls-row span8">
                  <input name="page_title" id="page_title" onkeyup="title_slug_mapping()" class="span12" type="text" placeholder="Page Title" value="<?php echo set_value('page_title'); ?>">
                  <span class="form_error span12"><?php echo form_error('page_title'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Slug
                </label>
                <div class="controls controls-row span8">
                  <input name="slug" id="page_slug" class="span12" type="text" placeholder="Slug" value="<?php echo set_value('slug'); ?>">
                  <span class="form_error span12"><?php echo form_error('slug'); ?></span>
                </div>
              </div>


             <div class="control-group">
                <label class="control-label">
                Page Content
                </label>
                <div class="controls controls-row span8">
                  <textarea id="" name="content" class="input-block-level no-margin mceEditor" placeholder="Enter Content text ..." style="height: 140px">
                        <?php echo set_value('content'); ?>
                  </textarea>
                  <span class="form_error span12"><?php echo form_error('content'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Seo Title
                </label>
                <div class="controls controls-row span8">
                  <input name="seo_author" class="span12" type="text" placeholder="Seo Title" value="<?php echo set_value('seo_author'); ?>">
                  <span class="form_error span12"><?php echo form_error('seo_author'); ?></span>
                </div>
              </div>

              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Seo Keyword
                </label>
                <div class="controls controls-row span8">
                  <input name="seo_keyword" class="span12" type="text" placeholder="Keyword" value="<?php echo set_value('seo_keyword'); ?>">
                  <span class="form_error span12"><?php echo form_error('seo_keyword'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Seo Description
                </label>
                <div class="controls controls-row span8">
                  <input name="seo_description" class="span12" type="text" placeholder="Description" value="<?php echo set_value('seo_description'); ?>">
                  <span class="form_error span12"><?php echo form_error('seo_description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Page Link Position
                </label>
                <div class="controls controls-row span8">
                  <select name="page_link_position" class="span4" required="required">
                        <option value="">Select link position of page</option>
                        <option value="1" <?php if(set_value('page_link_position')==1) echo "selected"; ?> >Header</option>
                        <option value="2" <?php if(set_value('page_link_position')==2) echo "selected"; ?> >Footer</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('page_link_position'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>


<script>
function title_slug_mapping()
    {
        var page_title  = $('#page_title').val();
        $('#page_slug').val(page_title); 
    }

    $('#wysiwyg').wysihtml5();

</script>