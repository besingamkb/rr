<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Button Colors
</div>
</div>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_buttons_color(); ?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Set Currency
    </label>
    <div class="controls controls-row span6">
        <select name="currency" class="span4">
        <option value="AUD" <?php if($row->currency=="AUD") echo "selected" ?> >AUD</option>
        <option value="BRL" <?php if($row->currency=="BRL") echo "selected" ?> >BRL</option>
        <option value="CAD" <?php if($row->currency=="CAD") echo "selected" ?> >CAD</option>
        <option value="CHF" <?php if($row->currency=="CAD") echo "selected" ?> >CAD</option>
        <option value="CZK" <?php if($row->currency=="CZK") echo "selected" ?> >CZK</option>
        <option value="DKK" <?php if($row->currency=="DKK") echo "selected" ?> >DKK</option>
        <option value="EUR" <?php if($row->currency=="EUR") echo "selected" ?> >EUR</option>
        <option value="HKD" <?php if($row->currency=="HKD") echo "selected" ?> >HKD</option>
        <option value="HUF" <?php if($row->currency=="HUF") echo "selected" ?> >HUF</option>
        <option value="JPY" <?php if($row->currency=="JPY") echo "selected" ?> >JPY</option>
        <option value="MYR" <?php if($row->currency=="MYR") echo "selected" ?> >MYR</option>
        <option value="MXN" <?php if($row->currency=="MXN") echo "selected" ?> >MXN</option>
        <option value="NOK" <?php if($row->currency=="NOK") echo "selected" ?> >NOK</option>
        <option value="NZD" <?php if($row->currency=="NZD") echo "selected" ?> >NZD</option>
        <option value="PLN" <?php if($row->currency=="PLN") echo "selected" ?> >PLN</option>
        <option value="RUB" <?php if($row->currency=="RUB") echo "selected" ?> >RUB</option>
        <option value="SGD" <?php if($row->currency=="SGD") echo "selected" ?> >SGD</option>
        <option value="SEK" <?php if($row->currency=="SEK") echo "selected" ?> >SEK</option>
        <option value="USD" <?php if($row->currency=="USD") echo "selected" ?> >USD</option>
        </select>
       <span class="form_error span6"><?php echo form_error('delete'); ?></span>
    </div>
</div>




<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

