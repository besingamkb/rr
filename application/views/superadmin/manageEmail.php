<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/tiny_mce_2/tinymce.min.js"></script>
<script type="text/javascript">
  tinyMCE.init({
    statusbar : false,
    menubar : false,
    toolbar: '| bold italic | link image | forecolor | alignleft aligncenter alignright', 
    selector : ".editor",
     }); 
</script>
<!-- /TinyMCE --> 

<script type="text/javascript">
onload = function()
{
$('#after_adding_property iframe').attr('title','Replace User Name = <%username%> , Property Image = <%propertyimage%> , Property Link = <%propertylink%> , Property Title = <%propertytitle%> , website_url = <%websiteurl%>');
$('#after_host_receives_book_request iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#reservation_notification_to_guest_after_accepting_booking iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#reservation_notification_to_host_after_accepting_booking iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#on_the_day_of_guest_checkin iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Guest Contact Info => <%guestcontactinfo%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#after_registiung_with_us iframe').attr('title','Replace User Name => <%username%> , link To The website => <%link%> , website_url => <%websiteurl%>');
$('#_3_days_after_property_profile_incomplete iframe').attr('title','Replace User Name => <%username%> , Property Title => <%propertytitle%> ,  MissingInfo => <%missinginfolist%>');
$('#your_profile_is_incomplete iframe').attr('title','Replace User Name => <%username%> , MissingInfo => <%missinginfolist%>');
$('#after_booking_hasbeen_cancelled_send_to_host iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#after_booking_hasbeen_cancelled_send_to_guest iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#after_we_send_a_refund iframe').attr('title','Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> , Amount => <%amount%> , Website Url => <%websiteurl%>');
$('#after_receiving_a_message_from_host_or_guest iframe').attr('title','Replace User Name => <%username%>');
$('#reset_password_1 iframe').attr('title','Replace User Name => <%username%> , link => <%link%> , website_url => <%websiteurl%>');
$('#after_we_send_security_deposit iframe').attr('title','Replace Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> , Amount => <%amount%> , Website Url => <%websiteurl%>');
$('#accept_featured_image_request iframe').attr('title','Replace User Name => <%username%> , Property Name => <%propertytitle%> , Property Image => <%propertyimage%> , Image link => <%imagelink%> , Website Url => <%websiteurl%>');
$('#reject_featured_image_request iframe').attr('title','Replace User Name => <%username%> , Property Name => <%propertytitle%> , Property Image => <%propertyimage%> , Image link => <%imagelink%> , Website Url => <%websiteurl%>');
$('#user_request_for_featured_image iframe').attr('title','Replace Property Name => <%propertytitle%> , Property Image => <%propertyimage%> , Image link => <%imagelink%> , Website Url => <%websiteurl%>');
$('#review_3_days_after_checkout iframe').attr('title','Replace Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>');
$('#after_contacting_us iframe').attr('title','Replace User Name => <%username%> ,User Subject => <%usersubject%> ,User Message => <%usermessage%> , website_url => <%websiteurl%>');
};
</script>



<style type="text/css">
.mce-tinymce
{
  margin-left: 177px;
  width:477px;
}
.control-group .control-label
{
  width:30%;
}

.control-group .controls input[title],textarea
{
  width:50%;
  margin-left:2%;
  cursor: help;
  border-radius: 4px;
}
.ui-tooltip {
background:#DE4980 ;
opacity: .9;
border-radius: 5px;
color:white ;
border:none;
}
.control-group .controls input.btn-info
{
   width: 20%;
   margin-left: 2%;
}


</style>

<script>




  $(function() {
  $('.controls').tooltip(
     {
      show:
      {
      effect: "slideDown",
      delay: 250,
      },
      track:true,
   });
  });


$(function() {
$('.control-group .controls input,textarea').tooltip(
   {
    show:
    {
    effect: "slideDown",
    delay: 250,
    },
    track:true,
 });
});

  $(document).on('click', '#faraz iframe',function (argument) {
     $('.ui-tooltip').hide();

  });

  $(document).on('click', '.control-group .controls input,textarea',function (argument) {
     $('.ui-tooltip').hide();

  });
</script>

<div class="row-fluid">
  <div class="span12">
    <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Manage All Email 
          </div>
        </div>
        <div class="widget-body">
                  <?php alert(); ?>

          <div class="row-fluid">
            <div class="span12" >

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Website Information 
                  </div>
                </div>


                <div class="widget-body" id="website_url">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_website_url')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_website_url') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $website_url_info = get_manage_email_info('website_url_and_email'); ?>
                 

                    <div class="control-group">
                      <label class="control-label">
                       Website Url
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Website Url" name="website_url" value="<?php if(!empty($website_url_info->website_url)) echo $website_url_info->website_url ?>" placeholder="Website Url"  >
                        <span class="form_error span12"><?php echo form_error('website_url'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                       Message From Which Email
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter the Email from where the message will be sent" name="message_email" value="<?php if(!empty($website_url_info->message_from_which_email)) echo $website_url_info->message_from_which_email ?>" placeholder="Message From which Email"  >
                        <span class="form_error span12"><?php echo form_error('message_email'); ?></span>
                      </div>                      
                    </div>  


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%;margin-left:2%" name="" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>


              
                <!-- After Adding a New Property starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> After Adding a New Property
                  </div>
                </div>

              

                <div class="widget-body" id="after_adding_property">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_adding_property')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_adding_property') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_adding_property_info = get_manage_email_info('after_adding_property'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_1" value="<?php if(!empty($after_adding_property_info->subject)) echo $after_adding_property_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_1'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls" id="faraz">
                        <textarea class="editor" rows="5" title="Replace User Name => <%username%> ,Property Image => <%propertyimage%> , Property Title => <%propertytitle%> , Link To the website => <%link%> ,  website_url => <%websiteurl%>" name="content_1" placeholder="" ><?php if(!empty($after_adding_property_info->content)) echo $after_adding_property_info->content ?></textarea>
                        <div></div>
                        <span class="form_error span12"><?php echo form_error('content_1'); ?></span>
                      </div>                                
                    </div>  




                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_adding_property" value="save" class="btn btn-info">
                      </div>                      
                    </div>  


                  <?php echo form_close() ?>
                </div>

              </div>


                <!-- After Adding a New Property Ends  -->


                <!-- . After a host receives a booking request Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> After a Host Receives a Booking request
                  </div>
                </div>


                <div class="widget-body" id="after_host_receives_book_request">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_host_receives_book_request')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_host_receives_book_request') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_host_receives_book_request_info = get_manage_email_info('after_host_receives_book_request'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_2" value="<?php if(!empty($after_host_receives_book_request_info->subject)) echo $after_host_receives_book_request_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_2'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" title="Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>" name="content_2" ><?php if(!empty($after_host_receives_book_request_info->content)) echo $after_host_receives_book_request_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_2'); ?></span>
                      </div>                                
                    </div>     




                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_host_receives_book_request" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>


                <!-- . After a host receives a booking request Ends  -->


                <!-- . Reservation notification for host after accepting the booking  Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Reservation notification to host after accepting the booking 
                  </div>
                </div>


                <div class="widget-body" id="reservation_notification_to_host_after_accepting_booking">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_reservation_notification_to_host_after_accepting_booking')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_reservation_notification_to_host_after_accepting_booking') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $reservation_notification_to_host_after_accepting_booking_info = get_manage_email_info('reservation_notification_to_host_after_accepting_booking'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_3" value="<?php if(!empty($reservation_notification_to_host_after_accepting_booking_info->subject)) echo $reservation_notification_to_host_after_accepting_booking_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_3'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_3" title="Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>" ><?php if(!empty($reservation_notification_to_host_after_accepting_booking_info->content)) echo $reservation_notification_to_host_after_accepting_booking_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_3'); ?></span>
                      </div>                                
                    </div>     


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="reservation_notification_to_host_after_accepting_booking" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- . Reservation notification for host after accepting the booking  Ends  -->

                <!-- . Reservation notification for guest after accepting the booking  Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>  Reservation notification to guest after a host has accepted the booking. 
                  </div>
                </div>


                <div class="widget-body" id="reservation_notification_to_guest_after_accepting_booking">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_reservation_notification_to_guest_after_accepting_booking')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_reservation_notification_to_guest_after_accepting_booking') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $reservation_notification_to_guest_after_accepting_booking_info = get_manage_email_info('reservation_notification_to_guest_after_accepting_booking'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_4" value="<?php if(!empty($reservation_notification_to_guest_after_accepting_booking_info->subject)) echo $reservation_notification_to_guest_after_accepting_booking_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_4'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_4" title="Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>" ><?php if(!empty($reservation_notification_to_guest_after_accepting_booking_info->content)) echo $reservation_notification_to_guest_after_accepting_booking_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_4'); ?></span>
                      </div>                                
                    </div>     

                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="reservation_notification_to_guest_after_accepting_booking" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>



                <!-- . Reservation notification for guest after accepting the booking  Ends  -->
             

                <!-- . On the day of guest checking-in   Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> On the day of guest checking-in  
                  </div>
                </div>


                <div class="widget-body" id="on_the_day_of_guest_checkin">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_on_the_day_of_guest_checkin')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_on_the_day_of_guest_checkin') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $on_the_day_of_guest_checkin_info = get_manage_email_info('on_the_day_of_guest_checkin'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_5" value="<?php if(!empty($on_the_day_of_guest_checkin_info->subject)) echo $on_the_day_of_guest_checkin_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_5'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_5" title="Replace  Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> ,  website_url => <%websiteurl%>" ><?php if(!empty($on_the_day_of_guest_checkin_info->content)) echo $on_the_day_of_guest_checkin_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_5'); ?></span>
                      </div>                                
                    </div>     


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="on_the_day_of_guest_checkin" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- . On the day of guest checking-in   Ends  -->




                <!-- . After registering with us   Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> After registering with us  
                  </div>
                </div>


                <div class="widget-body" id="after_registiung_with_us">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_registiung_with_us')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_registiung_with_us') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_registiung_with_us_info = get_manage_email_info('after_registiung_with_us'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_6" value="<?php if(!empty($after_registiung_with_us_info->subject)) echo $after_registiung_with_us_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_6'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_6" title="Replace User Name => <%username%> , link => <%link%> , website_url => <%websiteurl%>" ><?php if(!empty($after_registiung_with_us_info->content)) echo $after_registiung_with_us_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_6'); ?></span>
                      </div>                                
                    </div>     


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_registiung_with_us" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- . After registering with us   Ends  -->



                <!-- .3 days after your property profile is still not completed  Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>3 days after your property profile is still not completed 
                  </div>
                </div>


                <div class="widget-body" id="_3_days_after_property_profile_incomplete">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success__3_days_after_property_profile_incomplete')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success__3_days_after_property_profile_incomplete') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $_3_days_after_property_profile_incomplete_info = get_manage_email_info('_3_days_after_property_profile_incomplete'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_7" value="<?php if(!empty($_3_days_after_property_profile_incomplete_info->subject)) echo $_3_days_after_property_profile_incomplete_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_7'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_7" title="Replace Property Title => <%propertytitle%>" ><?php if(!empty($_3_days_after_property_profile_incomplete_info->content)) echo $_3_days_after_property_profile_incomplete_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_7'); ?></span>
                      </div>                                
                    </div>     

                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="_3_days_after_property_profile_incomplete" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .3 days after your property profile is still not completed  Ends  -->




                <!-- .Your profile is incomplete  Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>Your profile is incomplete - 3 days after sign-up 
                  </div>
                </div>


                <div class="widget-body" id="your_profile_is_incomplete">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_your_profile_is_incomplete')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_your_profile_is_incomplete') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $your_profile_is_incomplete_info = get_manage_email_info('your_profile_is_incomplete'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_8" value="<?php if(!empty($your_profile_is_incomplete_info->subject)) echo $your_profile_is_incomplete_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_8'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_8" title="Replace User Name => <%hostname%>" ><?php if(!empty($your_profile_is_incomplete_info->content)) echo $your_profile_is_incomplete_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_8'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="your_profile_is_incomplete" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .Your profile is incomplete  Ends  -->





                <!-- .After booking has been cancelled Email To host  Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>After booking has been cancelled Email To host 
                  </div>
                </div>


                <div class="widget-body" id="after_booking_hasbeen_cancelled_send_to_host">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_booking_hasbeen_cancelled_send_to_host')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_booking_hasbeen_cancelled_send_to_host') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_booking_hasbeen_cancelled_send_to_host_info = get_manage_email_info('after_booking_hasbeen_cancelled_send_to_host'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text"  title="Enter Your Subject Here" name="subject_9" value="<?php if(!empty($after_booking_hasbeen_cancelled_send_to_host_info->subject)) echo $after_booking_hasbeen_cancelled_send_to_host_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_9'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_9" title="Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> , Website Url => <%websiteurl%>" ><?php if(!empty($after_booking_hasbeen_cancelled_send_to_host_info->content)) echo $after_booking_hasbeen_cancelled_send_to_host_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_9'); ?></span>
                      </div>                                
                    </div>     


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_booking_hasbeen_cancelled_send_to_host" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .After booking has been cancelled Email To host  Ends  -->




                <!-- .After booking has been cancelled Email To Guest  Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>After booking has been cancelled Email To Guest 
                  </div>
                </div>


                <div class="widget-body" id="after_booking_hasbeen_cancelled_send_to_guest">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_booking_hasbeen_cancelled_send_to_guest')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_booking_hasbeen_cancelled_send_to_guest') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_booking_hasbeen_cancelled_send_to_guest_info = get_manage_email_info('after_booking_hasbeen_cancelled_send_to_guest'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_10" value="<?php if(!empty($after_booking_hasbeen_cancelled_send_to_guest_info->subject)) echo $after_booking_hasbeen_cancelled_send_to_guest_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_10'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_10" title="Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> , Website Url => <%websiteurl%>"><?php if(!empty($after_booking_hasbeen_cancelled_send_to_guest_info->content)) echo $after_booking_hasbeen_cancelled_send_to_guest_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_10'); ?></span>
                      </div>                                
                    </div>     


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_booking_hasbeen_cancelled_send_to_guest" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .After booking has been cancelled Email To Guest  Ends  -->




                <!-- .After we send a refund Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>After we send a refund                   </div>
                </div>


                <div class="widget-body" id="after_we_send_a_refund">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_we_send_a_refund')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_we_send_a_refund') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_we_send_a_refund_info = get_manage_email_info('after_we_send_a_refund'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_11" value="<?php if(!empty($after_we_send_a_refund_info->subject)) echo $after_we_send_a_refund_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_11'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_11" title="Replace Host Name => <%hostname%> , Guest Name => <%guestname%> , Check in => <%checkin%> ,Check Out => <%checkout%> , Property Name => <%propertytitle%> , Amount => <%amount%> , Website Url => <%websiteurl%>" ><?php if(!empty($after_we_send_a_refund_info->content)) echo $after_we_send_a_refund_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_11'); ?></span>
                      </div>                                
                    </div>     


                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_we_send_a_refund" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .After we send a refund Ends  -->


  <!-- .Reset password Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>Reset password                   </div>
                </div>


                <div class="widget-body" id="reset_password_1">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_reset_password_1')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_reset_password_1') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $reset_password_1_info = get_manage_email_info('reset_password_1'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_13" value="<?php if(!empty($reset_password_1_info->subject)) echo $reset_password_1_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_13'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_13" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($reset_password_1_info->content)) echo $reset_password_1_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_13'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="reset_password_1" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .Reset password Ends  -->


  <!-- .After we send security deposit Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>After we send security deposit                   </div>
                </div>


                <div class="widget-body" id="after_we_send_security_deposit">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_we_send_security_deposit')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_we_send_security_deposit') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_we_send_security_deposit_info = get_manage_email_info('after_we_send_security_deposit'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_14" value="<?php if(!empty($after_we_send_security_deposit_info->subject)) echo $after_we_send_security_deposit_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_14'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_14" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($after_we_send_security_deposit_info->content)) echo $after_we_send_security_deposit_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_14'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_we_send_security_deposit" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .After we send security deposit Ends  -->


  <!-- .When superadmin accept featured Image request Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>When superadmin accept featured Image request                   </div>
                </div>


                <div class="widget-body" id="accept_featured_image_request">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_accept_featured_image_request')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_accept_featured_image_request') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $accept_featured_image_request_info = get_manage_email_info('accept_featured_image_request'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_15" value="<?php if(!empty($accept_featured_image_request_info->subject)) echo $accept_featured_image_request_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_15'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_15" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($accept_featured_image_request_info->content)) echo $accept_featured_image_request_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_15'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="accept_featured_image_request" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .When superadmin accept featured Image request Ends  -->



  <!-- .When superadmin reject featured Image request Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>When superadmin reject featured Image request                   </div>
                </div>


                <div class="widget-body" id="reject_featured_image_request">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_reject_featured_image_request')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_reject_featured_image_request') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $reject_featured_image_request_info = get_manage_email_info('reject_featured_image_request'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_16" value="<?php if(!empty($reject_featured_image_request_info->subject)) echo $reject_featured_image_request_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_16'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_16" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($reject_featured_image_request_info->content)) echo $reject_featured_image_request_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_16'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="reject_featured_image_request" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .When superadmin reject featured Image request Ends  -->


  <!-- .When user request for featured Image Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>When user request for featured Image                   </div>
                </div>


                <div class="widget-body" id="user_request_for_featured_image">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_user_request_for_featured_image')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_user_request_for_featured_image') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $user_request_for_featured_image_info = get_manage_email_info('user_request_for_featured_image'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_17" value="<?php if(!empty($user_request_for_featured_image_info->subject)) echo $user_request_for_featured_image_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_17'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_17" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($user_request_for_featured_image_info->content)) echo $user_request_for_featured_image_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_17'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="user_request_for_featured_image" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .When user request for featured Image Ends  -->


  <!-- .Remainder Email For Review After Checkout Starts  -->

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>Remainder Email For Review After Checkout                   </div>
                </div>


                <div class="widget-body" id="review_3_days_after_checkout">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_review_3_days_after_checkout')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_review_3_days_after_checkout') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $review_3_days_after_checkout_info = get_manage_email_info('review_3_days_after_checkout'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_18" value="<?php if(!empty($review_3_days_after_checkout_info->subject)) echo $review_3_days_after_checkout_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_18'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_18" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($review_3_days_after_checkout_info->content)) echo $review_3_days_after_checkout_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_18'); ?></span>
                      </div>                                
                    </div>     



                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="review_3_days_after_checkout" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .Remainder Email For Review After Checkout Ends  -->


  <!-- .After Contacting us Starts  -->
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span>After Contacting us </div>
                </div>


                <div class="widget-body" id="after_contacting_us">                  
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>

                    <?php if($this->session->flashdata('success_after_contacting_us')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_after_contacting_us') ?>
                        </div>
                    <?php }?>
                    
                    <hr>
                   <?php $after_contacting_us_info = get_manage_email_info('after_contacting_us'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Subject
                      </label>
                      <div class="controls">
                        <input type="text" title="Enter Your Subject Here" name="subject_19" value="<?php if(!empty($after_contacting_us_info->subject)) echo $after_contacting_us_info->subject ?>" placeholder="Your Subject Wright Here"  >
                        <span class="form_error span12"><?php echo form_error('subject_19'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Content
                      </label>
                      <div class="controls">
                        <textarea rows="5" class="editor" name="content_19" title="Replace User Name => <%username%> , Link => <%link%>"><?php if(!empty($after_contacting_us_info->content)) echo $after_contacting_us_info->content ?></textarea>
                        <span class="form_error span12"><?php echo form_error('content_19'); ?></span>
                      </div>                                
                    </div>     

                    <div class="control-group">
                      <label class="control-label">
                      </label>
                      <div class="controls">
                      <input type="submit" style="width:20%" name="after_contacting_us" value="save" class="btn btn-info">
                      </div>                      
                    </div>  
                  <?php echo form_close() ?>
                </div>

              </div>

                <!-- .After Contacting us Ends  -->
           
            </div>

        


        

            </div>         
          </div>
          
            
           
        </div>
    </div>    
  </div>
</div>



