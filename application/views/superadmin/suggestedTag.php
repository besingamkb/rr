 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Suggested Tags
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>superadmin/neighbourhoods"> Neighborhoods </a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>

          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:20%">Neighbourhood Name</th>
                  <th style="width:20%">Suggested Tag</th>
                  <th style="width:10%">Status</th>
                  <th style="width:15%">Created</th>
                  <th style="width:30%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($tags)): ?>
                    <?php $i=1; foreach ($tags as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->nb; ?></td>
                        <td><?php echo $row->tag; ?></td>
                        <td>
                            <?php if($row->status == 0): ?>
                                <a href="<?php echo base_url()?>superadmin/approveTag/<?php echo $row->id;?>" class="btn btn-default" >Pending</a>
                            <?php else: ?>
                                Approved
                            <?php endif; ?>    
                        </td>
                        <td><?php echo date('d-m-Y', $row->created); ?></td>

                        <?php $colors = get_buttons_color();  ?>
                        <?php if(!empty($colors)): ?>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/deleteSuggestedTag/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                        </td>
                        <?php else: ?>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/deleteSuggestedTag/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-success btn-small hidden-phone" data-original-title="">Delete</a>
                        </td>
                        <?php endif; ?>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="4"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
            <?php echo $pagination; ?>
          </div>
        </div>
      </div>
    </div>