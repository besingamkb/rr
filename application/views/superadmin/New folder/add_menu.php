 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Add Menu</h4>
        </div><!-- page-header -->
        <?php alert() ?>
         <?php echo form_open_multipart(current_url()); ?>
          <div class="row-fluid">
          <div class="span8">             
                <div class="control-group">
                  <label class="control-label" for="">Name</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="name" value="<?php echo set_value('name') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('name') ?></span>
                </div>
                <div class="control-group">
                  <label class="control-label" for="">Link</label>
                  <div class="controls input-prepend">
                    <span class="add-on"><?php echo base_url() ?>pages/</span>
                    <input  class="span12" type="text" name="link" value="<?php echo set_value('link') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('link') ?></span>
                </div>              
                <div class="control-group">
                  <label class="control-label" for="">Order</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="order" value="<?php echo set_value('order') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('order') ?></span>
                </div>         
                <div class="control-group">
                  <label class="control-label" for=""></label>
                  <div class="controls">
                    <input  class="btn btn-primary" type="submit" value="submit">
                  </div>
                  
                </div>
            </div> <!-- span8 -->

          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->