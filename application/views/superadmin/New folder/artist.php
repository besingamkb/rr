 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
       <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Artists</h4>
            <a href="<?php echo base_url() ?>admin/add_artist" title="">Add new</a>
        </div><!-- page-header -->
      <div class="row-fluid">          
          <div class="span12">
            <!-- alert -->
             <?php alert() ?>
            <!-- alert -->

            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="20%">Name</th>                  
                  <th width="50%">Description</th>                  
                  <th width="10%">Date</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($artist): ?>
                  <?php foreach ($artist as $row): ?>
                    <tr>
                      <td> <a href="<?php echo base_url() ?>admin/edit_artist/<?php echo $row->id ?>"><?php echo $row->name ?></a></td>                      
                      <td><?php echo word_limiter($row->description,10); ?></td>
                      <td><?php echo date('m/d/y',strtotime($row->created)); ?></td>                      
                      <td> 
                         <a href="<?php echo base_url() ?>admin/edit_artist/<?php echo $row->id ?>"><i class="icon-pencil"></i></a>
                         &nbsp;&nbsp;&nbsp;  
                         <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() ?>admin/delete_artist/<?php echo $row->id ?>"><i class="icon-remove"></i></a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                <?php endif ?>
              </tbody>
            </table>
            <?php if($pagination) echo $pagination; ?>
          </div>    
      </div><!-- row-fluid -->
      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->