 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
       <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Comments</h4>
        </div><!-- page-header -->
      <div class="row-fluid">          
          <div class="span12">
            <!-- alert -->
             <?php alert() ?>
            <!-- alert -->
            <table class="table">
              <thead>
                <tr>
                  <th width="10%">Author</th>
                  <th width="50%">Comment</th>
                  <th width="20%">In Response To</th>                  
                  <th width="10%">Status</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if (!empty($comments)): ?>
                  <?php foreach ($comments as $row): ?>
                    <tr>
                      <td>
                        <?php echo $row->comment_author ?><br>                      
                       <a href="mailto:<?php echo $row->comment_author_email ?>" "email me"><?php echo $row->comment_author_email ?></a>
                      </td>
                      <td><?php echo $row->comment_content ?></td>                     
                      <td>
                        <?php echo $row->post_title ?><br>
                        <!-- <a href="<?php //echo base_url() ?>admin/post/<?php //echo $row->post_id ?>" title="">View post</a> -->
                      </td>
                      <td> 
                        <?php if($row->comment_approved=='1'): ?>
                          <span class="text-success">Approve</span>
                           <?php elseif($row->comment_approved=='0'): ?>
                          <span class="text-warning">Pending</span>
                           <?php elseif($row->comment_approved=='2'): ?>
                          <span class="text-error">Spam</span>
                           <?php endif ?>
                         </td>                      
                      <td>                         
                         <a href="<?php echo base_url() ?>admin/edit_comment/<?php echo $row->id ?>"><i class="icon-pencil"></i></a>
                         &nbsp;&nbsp;&nbsp;
                                                 
                         <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() ?>admin/delete_comment/<?php echo $row->id ?>"><i class="icon-remove"></i></a>
                        
                      </td>
                    </tr>
                  <?php endforeach ?>
                <?php endif ?>
              </tbody>
            </table>
            <?php if($pagination) echo $pagination; ?>
          </div>    
      </div><!-- row-fluid -->
      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->