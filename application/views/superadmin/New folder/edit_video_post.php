<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">

  tinyMCE.init({
    mode : "textareas",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",   
    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'
     }); 
</script>
<!-- /TinyMCE -->

<div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
          <div class="page-header">
            <h4>Edit Post</h4>
          </div><!-- page-header -->
          <?php alert() ?>

          <div class="row-fluid">
              <div class="span8">
                <?php echo form_open_multipart(current_url()); ?>
                  <div class="control-group">
                    <label class="control-label" for="">Post Title</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="post_title" value="<?php echo $video_post->post_title; ?>" placeholder="Enter title Here">
                    </div>
                    <?php echo form_error('post_title') ?>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="">Post Excerpt</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="post_excerpt" value="<?php echo $video_post->post_excerpt ?>" placeholder="Enter Excerpt Here">
                    </div>
                    <?php echo form_error('post_excerpt') ?>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="">Video Type</label>
                    <div class="controls">
                      <select id="video_type" name="video_type">
                        <option value="">Please select</option>
                        <option value="1" <?php if($video_post->video_type == 1) echo "selected='selected'"; ?>>Youtube</option>
                        <option value="2" <?php if($video_post->video_type == 2) echo "selected='selected'"; ?>>Vimeo</option>
                        <!-- <option value="3" <?php //if($video_post->video_type == 3) echo "selected='selected'"; ?>>Limelight</option> -->
                      </select>
                    </div>
                    <?php echo form_error('video_type') ?>
                  </div>
                  <div id="video_url" class="control-group">
                    <label class="control-label" for="">Video URL</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="video_url" value="<?php if(!empty($video_post->video_url)) echo $video_post->video_url; ?>" placeholder="Enter Video Url Here">
                    </div>
                    <?php echo form_error('video_url') ?>
                  </div>
                  <div id="channel_id" class="control-group">
                    <label class="control-label" for="">Channel ID</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="channel_id" value="<?php if(!empty($video_post->channel_id)) echo $video_post->channel_id; ?>"  placeholder="Enter Channel Id Here">
                    </div>                    
                  </div>                 

                   <div class="control-group">
                    <label class="control-label" for="">Category</label>
                    <div class="controls">
                      <select name="post_category">
                        <option value="">Please select</option>
                        <?php foreach ($categories as $row): ?>                          
                        <option value="<?php echo $row->id ?>" <?php if($video_post->category_id == $row->id) echo "selected='selected'"; ?> ><?php echo $row->category_name ?></option>                        
                        <?php endforeach ?>
                      </select>
                    </div>
                    <?php echo form_error('post_category') ?>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="">Post Content</label>                    
                    <div class="controls">
                       <textarea id="textarea" class="span12 " rows="10"  name="post_content" placeholder="Enter Content Here"><?php echo $video_post->content; ?></textarea>
                    </div>
                    <?php echo form_error('post_content') ?>
                  </div>

                    <div class="control-group">
                  <label class="control-label" for="">Post Image <span style="font-size:10px;"> <!-- (640 x 360) in px --> </span> </label>
                  <div class="controls">
                    <input type="file" name="userfile">
                    <?php if (!empty($video_post->post_image)): ?>
                      <img src="<?php echo  base_url() ?>assets/uploads/custom_uploads/<?php echo $video_post->post_image; ?>" width="100px" >
                    <?php endif ?>
                  </div>                 
                </div>

                  <div class="control-group">
                    <label class="control-label" for="">Post Status</label>                    
                    <div class="controls">
                       <select name="post_status">                        
                        <option value="1" <?php if($video_post->post_status == 1) echo "selected='selected'"; ?>>Publish</option>
                        <option value="2" <?php if($video_post->post_status == 2) echo "selected='selected'"; ?>>UnPublish</option>
                       </select>
                    </div>
                    <?php echo form_error('post_status') ?>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for=""></label>                   
                    <div class="controls">
                       <input type="submit" value="Add Post" class="btn btn-primary">
                    </div>                 
                  </div>                
               </div> <!--span8 -->
              <?php echo form_close(); ?>
          

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->
<script type="text/javascript">
  jQuery(document).ready(function(){
    video_type = '<?php echo $video_post->video_type; ?>' ;    

    if(video_type == 1){
     jQuery('#video_url').show();     
     jQuery('#channel_id').hide();
    }else if(video_type == 2){     
     jQuery('#video_url').show();
     jQuery('#channel_id').hide();
    }else if(video_type == 3){
     jQuery('#channel_id').show();      
     jQuery('#video_url').hide();
    }


   jQuery('#video_type').change(function(){
      var val = jQuery(this).val()
      // alert(val);

      if(val == ""){
      jQuery('#video_url').hide();
      jQuery('#channel_id').hide();
       return false;
      }else if(val == 1){
        jQuery('#channel_id').hide();
        jQuery('#video_url').show();
        jQuery('input[name=video_url]').val('');        
      }else if(val == 2){
        jQuery('#channel_id').hide();
         jQuery('input[name=video_url]').val('');
        jQuery('#video_url').show();
      }else if(val == 3){
        jQuery('#video_url').hide();
        jQuery('#channel_id').show();
         jQuery('input[name=channel_id]').val('');
      }

      });
    // alert(video_type);
  });
</script>
