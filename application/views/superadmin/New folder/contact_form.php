 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
       <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Contact forms</h4>
        </div><!-- page-header -->
      <div class="row-fluid">          
          <div class="span12">
            <!-- alert -->
             <?php alert() ?>
            <!-- alert -->
            <table class="table">
              <thead>
                <tr>
                  <th width="10%">Name</th>
                  <th width="10%">Email</th>                  
                  <th width="50%">Message</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if (!empty($contact_form)): ?>
                  <?php foreach ($contact_form as $row): ?>
                    <tr>
                      <td>
                        <?php echo $row->name ?><br>                                             
                      </td>
                      <td><a href="mailto:<?php echo $row->email ?>" "email me"><?php echo $row->email ?></a></td>                     
                      <td><?php echo $row->message ?></td>
                      <td>
                          <a href="<?php echo base_url() ?>admin/contact_view/<?php echo $row->id ?>"><i class="icon-eye-open"></i></a> &nbsp;&nbsp;                       
                         <a onclick="return confirm('Are you sure?')" href="<?php echo base_url() ?>admin/delete_contact/<?php echo $row->id ?>"><i class="icon-remove"></i></a>                        
                      </td>
                    </tr>
                  <?php endforeach ?>
                <?php else: ?>
                  <td colspan="4">Nothing Found</td>
                <?php endif; ?>
              </tbody>
            </table>
            <?php if($pagination) echo $pagination; ?>
          </div>    
      </div><!-- row-fluid -->
      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->