 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Contact</h4>
        </div><!-- page-header -->
          <div class="row-fluid">
          <div class="span8">
            <?php if ($info): ?>

                <div class="row-fluid">
                  <div class="span2">
                    Name :
                  </div>
                  <div class="span8">
                    <?php echo $info->name; ?>
                  </div>
                </div>

                <div class="row-fluid">
                  <div class="span2">
                    E-mail :
                  </div>
                  <div class="span8">
                    <?php echo $info->email; ?>
                  </div>
                </div>

                <div class="row-fluid">
                  <div class="span2">
                    Message :
                  </div>
                  <div class="span8">
                    <?php echo $info->message; ?>
                  </div>
                </div>

                <div class="row-fluid">
                  <div class="span2">
                    Submitted On :
                  </div>
                  <div class="span8">
                    <?php echo $info->created; ?>
                  </div>
                </div>

            <?php endif ?>
        
            </div> <!-- span8 -->

          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->