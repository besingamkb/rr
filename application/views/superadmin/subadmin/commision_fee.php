 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Commision
          </div>
         
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:20%">Commision</th>
                  <th style="width:30%">Updated</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($commision_fee)): ?>
                      <tr>
                        <td>1</td>
                        <td><?php echo $commision_fee->commision_fee ?></td>
                        <td><?php echo date('d-m-Y',strtotime($commision_fee->created)); ?></td>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/edit_commision_fee/<?php echo $commision_fee->id;?>" id="" role="button" class="btn btn-info" data-toggle="modal" data-original-title="">Edit</a>
                        </td>
                      </tr>
                    <?php else: ?>
                      <tr>
                          <th colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>


                    </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>