
 <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>

<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Neighborhoods
          </div>
        
          <?php if($subadmin_restrictions->neighbourhood!=1):  ?>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/suggestedTags"> Suggested Tags </a>
            <a class="btn" href="<?php echo base_url(); ?>subadmin/neb_category"> Neighborhood Categories </a>
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_neighbour"> Add Neighborhood </a>
          </div>
        <?php endif; ?>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:10%">City</th>
                  <th style="width:10%">Place</th>
                  <th style="width:30%">Created</th>
          <?php if($subadmin_restrictions->neighbourhood!=1):  ?>
                  <th style="width:15%">Actions</th>
          <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($neighbours)): ?>
                    <?php $i=1; foreach ($neighbours as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td>
                          <?php $city_info = get_city_info($row->city); ?>
                          <?php echo $city_info->city; ?>
                        </td>
                        <td><?php echo $row->title; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        
                        <?php $colors = get_buttons_color();  ?>
                        <?php if(!empty($colors)): ?>

                  <?php if($subadmin_restrictions->neighbourhood!=1):  ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/edit_neighbour/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                          <a href="<?php echo base_url()?>subadmin/delete_neighbour/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                        </td>
                  <?php endif; ?>      
                      <?php else: ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/edit_neighbour/<?php echo $row->id;?>" id="" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">Edit</a>
                          <a href="<?php echo base_url()?>subadmin/delete_neighbour/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-success btn-small hidden-phone" data-original-title="">Delete</a>
                        </td>
                      <?php endif; ?>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="4"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>