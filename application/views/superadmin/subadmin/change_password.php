<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Change Password
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Password <p style="font-size:10px;">(must be 8 character)</p>
                </label>
                <div class="controls controls-row span6">
                  <input name="password" class="span12" type="password" placeholder="Password" value="<?php echo set_value('password'); ?>">
                  <span class="form_error span12"><?php echo form_error('password'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                Confirm Password
                </label>
                <div class="controls controls-row span6">
                  
                  <input name="confirm_password" class="span12" type="password" placeholder="Confirm Password" value="<?php echo set_value('confirm_password'); ?>">
                   <span class="form_error span12"><?php echo form_error('confirm_password'); ?></span>
                </div>
              </div>
              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>