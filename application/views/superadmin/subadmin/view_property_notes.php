
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Property Note
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Note Title
                </label>
                <div class="controls controls-row span6">
                  <span><?php echo $notes->title;?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Property Note
                </label>
                <div class="controls controls-row span6">
                <p><?php echo $notes->note;?></p>
                </div> 
              </div>

           
              
              <div class="form-actions no-margin">
                <a href="<?php echo base_url()?>subadmin/property_notes/<?php echo $notes->pr_id;?>" class="btn btn-info">
                 Back To Note
                </a>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>