
   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>Edit Category
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Category Name
                </label>
                <div class="controls controls-row span6">
                  <input name="category_name" class="span12" type="text" placeholder="Category Name" value="<?php echo $category->category_name; ?>">
                  <span class="form_error span12"><?php echo form_error('category_name'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Category Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="description" rows="5" class="span12" placeholder="Category Description"><?php echo $category->description; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Category Banner
                </label>
                <div class="controls controls-row span6">
                  <input name="collection" class="span12" type="file">
                  <span  style="color:red;" class="form_error span12"><?php if($this->session->flashdata('image_error')) echo $this->session->flashdata('image_error');  ?></span>
                  <div class="pull-right">
                    <img width="100px" hieght="100px" src="<?php echo base_url()?>assets/uploads/collection_banner/<?php echo $category->banner;?>">
                  </div>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>