      <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>

<style>
td{ overflow: hidden;}
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin">
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>Messages
                </div>
            </div>
            <div class="widget-body" style="height:800px;">
                <?php alert(); ?> 
                <div id="dt_example" class="example_alt_pagination">
                    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
                        <thead>
                            <tr>
                                <th style="width:3%">#</th>
                              <th style="width:30%">Message</th>
                              <th style="width:20%">Property Title</th>
                              <th style="width:10%">Sender Name</th>
                              <th style="width:10%">Created</th>
        <?php if($subadmin_restrictions->message!=1):  ?>
                              <th style="width:17%">Actions</th>
              <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                        <tbody>
                <?php if(!empty($messages)): ?>
                    <?php $i=1; foreach ($messages as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo word_limiter($row->message,20); ?></td>
                        <td><?php echo word_limiter($row->pr_title,10); ?></td>
                        <td><?php echo $row->user_name; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                       
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>
        <?php if($subadmin_restrictions->message!=1):  ?>
                        <td>
                          <a href="<?php echo base_url();?>subadmin/message_view/<?php echo $row->id;?>"   role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" >View</a>
                          <a href="<?php echo base_url();?>subadmin/delete_message/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" >Delete</a>
                        </td>
        
            <?php endif;  ?>
            <?php endif;  ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                        </tbody>
                        </tbody>
                    </table>
                    <div id="data-table_paginate" class="pull-left">
                        <?php if(!empty($pagination)) echo $pagination; ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>


