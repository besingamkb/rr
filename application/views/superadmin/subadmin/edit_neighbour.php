<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
  tinyMCE.init({
    height : 400,
    convert_urls: false,
    mode : "textareas",
    editor_selector : "mceEditor",
    editor_deselector : "mceNoEditor",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'

     }); 
</script>
<!-- /TinyMCE -->  

 
 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Edit Neighborhood
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  City
                </label>
                <div class="controls controls-row span8">
                  <select name="city" class="span4">
                    <option value="" > Select </option>
                    <?php if($cities): ?>
                      <?php foreach ($cities as $row):?>
                        <option value="<?php echo $row->id; ?>" <?php if($neighbour->city == $row->id){ echo "selected"; } ?> > <?php echo $row->city; ?> </option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('city'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Place
                </label>
                <div class="controls controls-row span8">
                  <input name="title" class="span12" type="text" placeholder="Title" value="<?php echo $neighbour->title; ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Excerpt
                </label>
                <div class="controls controls-row span8">
                  <textarea name="excerpt" class="input-block-level no-margin" placeholder="Excerpt" style="height: 140px"><?php echo $neighbour->excerpt; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('excerpt'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Description
                </label>
                <div class="controls controls-row span8">
                  <textarea name="description" class="mceEditor input-block-level no-margin" placeholder="Description" style="height: 140px"><?php echo $neighbour->description; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Thumbnail Image
                </label>
                <span>(Resolution: 300x200)</span> 
                <div class="controls controls-row span6">
                  <input name="homeImage" class="span12" type="file"><p>&nbsp;</p>
                  <img src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $neighbour->homeImage; ?>" width="200" height="200">
                </div>
              </div>


              <div class="control-group">
                <label class="control-label">
                  Banner
                </label>
                <span>(Resolution: 1920x700)</span> 
                <div class="controls controls-row span6">
                  <input name="userfile" class="span12" type="file"><p>&nbsp;</p>
                  <img src="<?php echo base_url(); ?>assets/uploads/neighbourhoods/<?php echo $neighbour->banner; ?>" width="200" height="200">
                  <span class="form_error span12"><?php echo form_error('userfile'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Categories
                </label>
                <div class="controls controls-row span8">
                  <select name="category[]" class="span4" multiple="multiple">
                    <option value="" > Select </option>
                    <?php if($categories): ?>
                      <?php foreach ($categories as $row):?>
                        <option value="<?php echo $row->id; ?>" <?php foreach ($current_cat as $curr){ if($curr->category == $row->id ){echo "selected";}  } ?> > <?php echo $row->category_name; ?> </option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('category'); ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Is Featured?
                </label>
                <div class="controls controls-row span8">
                  <select name="is_featured" class="span4">
                        <option value="0" <?php if($neighbour->is_featured == 0) echo "selected"; ?> >No</option>
                        <option value="1" <?php if($neighbour->is_featured == 1) echo "selected"; ?> >Yes</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('is_featured'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>