
<?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>

<div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Property Chart
          </div>
        </div>
        <div class="widget-body">
          <div id="pcolumn_chart"></div>
        </div>
      </div>
    </div>
  </div> 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> <a href="<?php echo base_url() ?>subadmin/properties">Properties</a>
          </div>
     <?php if($subadmin_restrictions->properties!=1):  ?>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/approve_all_property"> Approve all </a>  
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_property"> Add Property </a>  
            <a class="btn" href="<?php echo base_url(); ?>subadmin/property_email_records">Property Email History </a>  
          </div>
       <?php endif; ?>

        </div>
        <div class="widget-body">
          <?php alert(); ?>
           <?php $attributes = array('name'=>'myForm'); echo form_open(current_url(),$attributes); ?>
          <select onchange="return form_submit();" id="sort" name="sort">
            <option value="">Select To Sort </option>
            <option value="new_requests">New Requests</option>
            <option value="Active_Listings">Active Listings</option>
            <option value="In-Active_Listings">In-Active Listings</option>
            <option value="Featured_Listings">Featured Listings</option>
          </select>
          <?php echo form_close(); ?> 

          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <div class="row" style="margin-left:0%;">
           <h4>Search Criteria</h4><br>
            <?php  $attributes = array('name'=>'search_form'); echo form_open(current_url(),$attributes);?>
            <div class="criteria div span12 row">
                <div class="span3" style="float:left">
                     <input class="input-medium" name='property_title'  placeholder="Property Title" type="text">
                </div>
                <div class="input-append span3" style="float:left;">
                     <input class="input-medium" name='from_date'  placeholder="From Date" type="text" id="start_check">
                      <span class="add-on">
                         <i class="icon-calendar"></i>
                      </span>
                </div>
                <div class="input-append span3" style="float:left;">
                     <input class="input-medium" name='to_date'  placeholder="To Date" type="text" id="end_check">
                      <span class="add-on">
                         <i class="icon-calendar"></i>
                      </span>
                </div>
                <div class="span3" style="float:left;">
                     <select class="input-medium" name="status_typo">
                       <option selected="selected" value="">Type</option>
                       <option value="pending">Pending</option>
                       <option value="approved">Approved</option>
                     </select>
                </div>

                <div class="span6 row" style="float:left; margin-left: 0;">
                  <div class="span3" style="float:left">
                    <button class="btn" id="search" onclick="return form_submit_name();" type="submit">Search</button>
                  </div>
                  <div class="span3" style="float:left">
                    <button class="btn" href="<?php echo base_url() ?>subadmin/bookings" >Show All</button>
                  </div>
                </div>
            </div>
            <?php echo form_close(); ?>
          </div>

          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->

          <div style="" class="row-fluid">
              <h4>Export Properties</h4><br>
              <?php echo form_open(base_url().'export/export_properties'); ?>

                <div class="row-fluid">
                  <input type="hidden" name="type" value="subadmin">
                    <div class="controls">
                        <div class="input-append">
                            <input id="date_range1" class="span2 date_picker" type="text" placeholder="Select Date" name="date_range1">
                            <span class="add-on">
                                <i class="icon-calendar"></i>
                            </span>
                          <select   name="export_sort">
                            <option value="">All</option>
                            <option value="new_requests">New Requests</option>
                            <option value="Active_Listings">Active Listings</option>
                            <option value="In-Active_Listings">In-Active Listings</option>
                            <option value="Featured_Listings">Featured Listings</option>
                          </select>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                  <span class="inline radio"><input type="radio" name="export_file_format" value="csv">CSV</span>
                  <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" value="excel">Excel</span>
                  <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</span>
                  <br><br>
                  <span ><input type="submit" value="Export" class="btn-info btn">
                </div>  
                <?php echo form_close();?>        
            </div>

          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:3%">#</th>
                  <th style="width:10%">Image</th>
                  <th style="width:10%">Property</th>
                  <th style="width:10%">Description</th>
                  <th style="width:5%">Created</th>
        <?php if($subadmin_restrictions->properties!=1):  ?>
                  <th style="width:25%">Actions</th>
          <?php endif; ?>        
                </tr>
              </thead>
              <tbody>
                <tbody>
                <?php if(!empty($properties)): ?>
                    <?php $i=1; foreach ($properties as $row):?>
                      <tr>
                        <td><?php echo $i; ?></td>
                       
                        <td><img style="width:100%" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                        <td><?php echo word_limiter($row->title,30); ?></td>
                        <td><?php echo word_limiter($row->description,6); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        <?php $colors = get_buttons_color();  ?>
                        <?php if(!empty($colors)): ?>
                          <!-- ////colors Work starts//// -->
                          <!-- ////colors Work starts//// -->
                          <!-- ////colors Work starts//// -->
                   <?php if($subadmin_restrictions->properties!=1):  ?>
                        <td>
                          <a href="<?php echo base_url();?>subadmin/delete_property/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="" id="delete" >Delete</a>
                          <a href="<?php echo base_url();?>subadmin/property_email/<?php echo $row->id;?>"  class="<?php echo $colors->email_btn ?>" data-original-title="">Email</a>
                          <a href="<?php echo base_url();?>subadmin/address_description/<?php echo $row->id;?>"   role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone edit" data-toggle="modal" data-original-title="">Manage Property</a>
                          <?php if($row->status==0){?>
                              <a href="javascript:void(0)" onclick="check_approve_property(<?php echo $row->id ?>)" class="btn btn-warning btn-small hidden-phone" data-original-title="">Pending </a>
                          <?php }else{ ?>
                              <a href="<?php echo base_url() ?>subadmin/approve_property/<?php echo $row->id;?>" class="btn btn-info btn-small hidden-phone" data-original-title="">Approved</a>
                          <?php }?>
                          <a href="<?php echo base_url();?>subadmin/property_notes/<?php echo $row->id;?>"  class="<?php echo $colors->notes_btn ?>">Notes</a>
                        </td>
                    <?php endif; ?>    
                      <?php else: ?>
                        <td>
                          <a href="<?php echo base_url();?>subadmin/delete_property/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-success btn-small hidden-phone delete" data-original-title="" id="delete" >Delete</a>
                          <a href="<?php echo base_url();?>subadmin/property_email/<?php echo $row->id;?>"  class="btn btn-small btn-primary hidden-tablet hidden-phone email" data-original-title="">Email</a>
                          <a href="<?php echo base_url();?>subadmin/manage_property/<?php echo $row->id;?>"   role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone edit" data-toggle="modal" data-original-title="">Manage Property</a>
                          <?php if($row->status==0){?>
                              <a href="<?php echo base_url() ?>subadmin/approve_property/<?php echo $row->id;?>" class="btn btn-warning btn-small hidden-phone" data-original-title="">Pending </a>
                          <?php }else{ ?>
                              <a href="<?php echo base_url() ?>subadmin/approve_property/<?php echo $row->id;?>" class="btn btn-info btn-small hidden-phone" data-original-title="">Approved</a>
                          <?php }?>
                          <a href="<?php echo base_url();?>subadmin/property_notes/<?php echo $row->id;?>"  class="btn btn-small notes">Notes</a>
                        </td>
                      <?php endif; ?>
                          <!-- ////colors Work Ends//// -->
                          <!-- ////colors Work Ends//// -->
                          <!-- ////colors Work Ends//// -->

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            <?php if($properties): ?>
              <!-- <div id="data-table_info" class="dataTables_info">Showing 1 to 10 of 16 entries</div> -->
              

                <div id="data-table_paginate" class="pull-right">
                  <?php if($pagination) echo $pagination; ?>
                    <!-- <a id="data-table_first" class="first paginate_button paginate_button_disabled" tabindex="0">First</a>
                      <a id="data-table_previous" class="previous paginate_button paginate_button_disabled" tabindex="0">Previous</a>
                       <span>
                          <a class="paginate_active" tabindex="0">1</a>
                          <a class="paginate_button" tabindex="0">2</a>
                      </span>
                      <a id="data-table_next" class="next paginate_button" tabindex="0">Next</a>
                    <a id="data-table_last" class="last paginate_button" tabindex="0">Last</a> -->
                </div>
              </div>
             <?php endif; ?>
            <div class="clearfix"></div>
           
 
<script type="text/javascript">
  function check_approve_property(property_id)
  {
    $.ajax({
           type:"post",
           url:"<?php echo base_url() ?>subadmin/check_approve_property/"+property_id,
           success:function(res)
           {
              if(res!="")
              {
                alert(res);
              }
              else
              {
                window.location = '<?php echo base_url() ?>subadmin/approve_property/'+property_id;
              }
           }
       });
  }
</script>


         
             
          
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
       function form_submit(){
        var sort = document.getElementById('sort'); 
        if(sort != ''){
          $("form[name='myForm']").submit();
        }
        else{
          return false;
        }        
      }

     function form_submit_name(){
        var name  = $('input:text[name=name]').val();
        if(name == '')
        {
          alert('Please Enter some value');
          return false;
        }
        else
        {
         return true;
        }        
      }
    </script>


     <script type="text/javascript">
      google.load("visualization", '1', {packages:['corechart']});
          google.setOnLoadCallback(drawChart3);
        function drawChart3() {
          var janactive = "<?php echo get_permonthactive('01') ?>";
          var febactive = "<?php echo get_permonthactive('02') ?>";
          var maractive = "<?php echo get_permonthactive('03') ?>";
          var apractive = "<?php echo get_permonthactive('04') ?>";
          var mayactive = "<?php echo get_permonthactive('05') ?>";
          var junactive = "<?php echo get_permonthactive('06') ?>";
          var julactive = "<?php echo get_permonthactive('07') ?>";
          var augactive = "<?php echo get_permonthactive('08') ?>";
          var sepactive = "<?php echo get_permonthactive('09') ?>";
          var octactive = "<?php echo get_permonthactive('10') ?>";
          var novactive = "<?php echo get_permonthactive('11') ?>";
          var decactive = "<?php echo get_permonthactive('12') ?>";

          var janpending = "<?php echo get_pendingprop('01') ?>";
          var febpending = "<?php echo get_pendingprop('02') ?>";
          var marpending = "<?php echo get_pendingprop('03') ?>";
          var aprpending = "<?php echo get_pendingprop('04') ?>";
          var maypending = "<?php echo get_pendingprop('05') ?>";
          var junpending = "<?php echo get_pendingprop('06') ?>";
          var julpending = "<?php echo get_pendingprop('07') ?>";
          var augpending = "<?php echo get_pendingprop('08') ?>";
          var seppending = "<?php echo get_pendingprop('09') ?>";
          var octpending = "<?php echo get_pendingprop('10') ?>";
          var novpending = "<?php echo get_pendingprop('11') ?>";
          var decpending = "<?php echo get_pendingprop('12') ?>";

          var janfeatured = "<?php echo get_featured('01') ?>";
          var febfeatured = "<?php echo get_featured('02') ?>";
          var marfeatured = "<?php echo get_featured('03') ?>";
          var aprfeatured = "<?php echo get_featured('04') ?>";
          var mayfeatured = "<?php echo get_featured('05') ?>";
          var junfeatured = "<?php echo get_featured('06') ?>";
          var julfeatured = "<?php echo get_featured('07') ?>";
          var augfeatured = "<?php echo get_featured('08') ?>";
          var sepfeatured = "<?php echo get_featured('09') ?>";
          var octfeatured = "<?php echo get_featured('10') ?>";
          var novfeatured = "<?php echo get_featured('11') ?>";
          var decfeatured = "<?php echo get_featured('12') ?>";     


          // alert(parseInt(marbooking));

          var data = google.visualization.arrayToDataTable([
         ['Month', 'Active', 'Inactive', 'Featured'],
    ['Jan', Number(janactive), Number(janpending), Number(janfeatured)],
    ['Feb', Number(febactive), Number(febpending), Number(febfeatured)],
    ['Mar', Number(maractive), Number(marpending), Number(marfeatured)],
    ['Apr', Number(apractive), Number(aprpending), Number(aprfeatured)],
    ['May', Number(mayactive), Number(maypending), Number(mayfeatured)],
    ['jun', Number(junactive), Number(junpending), Number(junfeatured)],
    ['Jul', Number(julactive), Number(julpending), Number(julfeatured)],
    ['Aug', Number(augactive), Number(augpending), Number(augfeatured)],
    ['Sep', Number(sepactive), Number(seppending), Number(sepfeatured)],
    ['Oct', Number(octactive), Number(octpending), Number(octfeatured)],
    ['Nov', Number(novactive), Number(novpending), Number(novfeatured)],
    ['Dec', Number(decactive), Number(decpending), Number(decfeatured)] ]);

    //  ['Month', 'Bookings', 'Pendings', 'Cancel', 'Expenses'],
    // ['Jan', 300, 800, 900, 300],
    // ['Feb', 1170, 860, 1220, 564],
    // ['Mar', 260, 1120, 2870, 2340],
    // ['Apr', 1030, 540, 3430, 1200],
    // ['May', 200, 700, 1700, 770],
    // ['Jul', 1170, 2160, 3920, 800],
    // ['Aug', 1170, 2160, 3920, 800],
    // ['Sep', 1170, 2160, 3920, 800],
    // ['Oct', 1170, 2160, 3920, 800],
    // ['Nov', 1170, 2160, 3920, 800],
    // ['Dec', 2170, 1160, 2820, 500] ]);

          var options = {
            width: 'auto',
            height: '160',
            backgroundColor: 'transparent',
            colors: ['#3eb157', '#3660aa', '#d14836', '#dba26b', '#666666', '#f26645'],
            tooltip: {
              textStyle: {
                color: '#666666',
                fontSize: 11
              },
              showColorCode: true
            },
            legend: {
              textStyle: {
                color: 'black',
                fontSize: 12
              }
            },
            chartArea: {
              left: 60,
              top: 10,
              height: '80%'
            },
          };

          var chart = new google.visualization.ColumnChart(document.getElementById('pcolumn_chart'));
          chart.draw(data, options);
        }


    </script>


<script type="text/javascript">
     var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
     
    var checkin = $('#start_check').datepicker({
    onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1);
    checkout.setValue(newDate);
    }
    checkin.hide();
    $('#end_check')[0].focus();
    }).data('datepicker');
    var checkout = $('#end_check').datepicker({
    onRender: function(date) {
    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    checkout.hide();
    }).data('datepicker');
</script>