 
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Room Allotment
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_allotment"> Add Room Allotment </a>
             <div class="btn-group">
                <a class="btn" href="#">Property Attributes</a>
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url(); ?>subadmin/amenities">Amenities</a></li>
                  <li><a href="<?php echo base_url(); ?>subadmin/room_types">Room Types</a></li>
                  <li><a href="<?php echo base_url(); ?>subadmin/bed_types">Bed Type</a></li>
                  <li><a href="<?php echo base_url(); ?>subadmin/property_types">property Type</a></li>
                  <li><a href="<?php echo base_url(); ?>subadmin/room_allotment">Room Allotment </a></li>
                </ul>
              </div>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  
                  <th style="width:30%">Room Allotment</th>
                  <th style="width:30%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($allotments)): ?>
                    <?php $i=1; foreach ($allotments as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->room_allotment; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/delete_allotment/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" style="display:none" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>subadmin/edit_allotment/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">edit</a>
                        </td>
          <?php endif;  ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix">
              <?php echo $pagination ?>
            </div>
          </div>
        </div>
      </div>
    </div>