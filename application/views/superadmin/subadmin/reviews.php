<?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>

<style>
td{ overflow: hidden;}
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin">
            <div class="widget-header">
                <div class="title" style="width:100%">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>Reviews
        <?php if($subadmin_restrictions->review!=1):  ?>
                    <a class="btn pull-right" href="<?php echo base_url(); ?>subadmin/add_review">Add Review</a>
            <?php endif; ?>
                </div>
            </div>
            <div class="widget-body" style="height:800px;">
                <?php alert(); ?> 
                <div id="dt_example" class="example_alt_pagination">
                    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
                        <thead>
                            <tr>
                              <th style="width:3%">#</th>
                              <th style="width:30%">Review</th>
                              <th style="width:15%">Property Title</th>
                              <th style="width:15%">Reviewer</th>
                              <th style="width:10%">Status</th>
                              <th style="width:10%">Created</th>
        <?php if($subadmin_restrictions->review!=1):  ?>
                              <th style="width:22%">Actions</th>
          <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                        <tbody>
                <?php if(!empty($reviews)): ?>
                    <?php $i=1; foreach ($reviews as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo word_limiter($row->review,20); ?></td>
                        <td>
                          <a href="<?php echo base_url(); ?>properties/details/<?php echo $row->property_id; ?>" target="_blank">
                          <?php 
                            $property = get_property_detail($row->property_id); 
                            echo $property->title;
                          ?>
                          </a>
                        </td>
                        <td>
                          <?php $reviewer = get_user_info($row->customer_id); ?>
                          <?php echo $reviewer->first_name." ".$reviewer->last_name; ?>  
                        </td>
                        <td>
                           <?php
                              $status = $row->status; 
                              if($status == 1) { echo "Published";}
                              if($status == 0) { echo "Unpublished";}
                           ?>
                        </td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>
        <?php if($subadmin_restrictions->review!=1):  ?>
                        <td>
                          <a href="<?php echo base_url();?>subadmin/review_view/<?php echo $row->id;?>"   role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" >View</a>
                          <a href="<?php echo base_url();?>subadmin/edit_review/<?php echo $row->id;?>"   role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" >Edit</a>
                          <a href="<?php echo base_url();?>subadmin/delete_review/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" >Delete</a>
                        </td>
             <?php endif; ?>
             <?php endif; ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                        </tbody>
                        </tbody>
                    </table>
                    <div id="data-table_paginate" class="pull-left">
                        <?php if(!empty($pagination)) echo $pagination; ?>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>


