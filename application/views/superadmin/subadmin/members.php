      <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>


<div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Members Chart
          </div>
        </div>
        <div class="widget-body">
          <div id="mcolumn_chart"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
          <a href="<?php echo base_url(); ?>subadmin/members">  
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Members
          </a>
          </div>
        </div>
        <div class="widget-body" >
           <?php alert(); ?>
            <div class="row span12" style="margin-left:2%;">
                <div class="span10"    >
                <?php $attributes = array('name'=>'myForm'); echo form_open(base_url().'subadmin/members',$attributes); ?>
                  <select onchange="return form_submit();"  class="span3" id="sort" name="sort"> 
                    <option value="">Select To Sort</option>
                    <option value="old">Oldest</option>
                    <option value="new">Newest</option>
                    <option value="ban">Banned User</option>
                    <option value="unban">Active User</option>
                  </select>
                <?php echo form_close(); ?>    
                </div>
        <?php if($subadmin_restrictions->member!=1):  ?>
                <div class="span2" >
                  <a class="btn" href="<?php echo base_url(); ?>subadmin/add_member"> Add Member </a>
                </div>
        <?php endif; ?>
            </div>
             

          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <div class="row" style="margin-left:0%;">
           <h4>Search Criteria</h4><br>
            <?php  $attributes = array('name'=>'search_form'); echo form_open(current_url(),$attributes);?>
            <div class="criteria div">
              <div style="float:left">
                   <input  name='name'  placeholder="User Name" type="text">
              </div>
              <div style="float:left;margin-left:0.5%">
                   <input  name='email'  placeholder="User Email" type="text">
              </div>
              <div style="float:left;margin-left:0.5%">
                   <select name="status_typo">
                     <option selected="selected" value="">Type</option>
                     <option value="active">Active User</option>
                     <option value="banned">Banned User</option>
                   </select>
              </div>
          </div>
          <br><br><br>
          <div style="float:left">
             <button class="btn" id="search" onclick="return form_submit_name();" type="submit">Search</button>
             <button class="btn" href="<?php echo base_url() ?>subadmin/bookings" >Show All</button>
          </div>

            <?php echo form_close(); ?>
          </div>

          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->

                    <div  class="row-fluid">
                        <h4>Export Members</h4><br>
                        <?php echo form_open(base_url().'export/export_members'); ?>

                          <div class="row" style="margin-left:2%">
                            <input type="hidden" name="type" value="subadmin">
                              <div class="controls">
                                  <div class="input-append">
                                     <input id="report_range2" class="span2 report_range" type="text" placeholder="Select Date" name="report_range2">
                                      <span class="add-on">
                                          <i class="icon-calendar"></i>
                                      </span>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <select  name="export_sort"> 
                                      <option value="">All</option>
                                      <option value="Active_users">Active users</option>
                                      <option value="Ban_users">Ban users</option>
                                    </select>
                                  </div>
                              </div>
                          </div>
                          <div class="row" style="margin-left:2%">
                            <span style="" class="inline radio"><input type="radio" name="export_file_format" value="csv">CSV</span>
                            <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" value="excel">Excel</span>
                            <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</span>
                            <br><br>
                            <span style=""><input type="submit" value="Export" class="btn-info btn">
                          </div>  
                          <?php echo form_close();?>        
                      </div>
          
         
<style>tr{ word-wrap:break-word; }</style>        
          <div id="dt_example" class="example_alt_pagination">
<table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
  <thead>
    <tr>
      <th style="width:1%">#</th>
      <th style="width:10%">User Name</th>
      <th style="width:17%">Email</th>
      <th style="width:8%">Last login</th>
      <th style="width:8%">Last ip</th>
      <th style="width:8%">Created</th>
    <?php if($subadmin_restrictions->member!=1):  ?>
      <th style="width:5%">Status</th>
      <th style="width:30%">Actions</th>
    <?php endif; ?>  
    </tr>
  </thead>
  <tbody>
  <?php if ($members): ?>
    <?php $i=1; foreach ($members as $row): ?>                 
    <tr class="gradeA">
      <td><?php echo $i;?></td>
      <td><?php echo $row->first_name." ".$row->last_name;?></td>
      <td><a href="mailto:<?php echo $row->user_email;?>"><?php echo $row->user_email;?></a></td>
      <td><?php echo date('Y-m-d', strtotime($row->last_login));?></td>
      <td><?php echo $row->last_ip;?></td>
      <td><?php echo $row->created;?></td>
<?php if($subadmin_restrictions->member!=1):  ?>
      <td>
        <?php if($row->user_status==0){?>
            <a href="<?php echo base_url() ?>subadmin/ban_user/<?php echo $row->id;?>" class="btn btn-info btn-small hidden-phone" data-original-title="">Banned</a>
          <?php }else{?>
            <a href="<?php echo base_url() ?>subadmin/ban_user/<?php echo $row->id;?>" class="btn btn-success btn-small hidden-phone" data-original-title="">Active</a>
        <?php }?>
      </td>
<?php endif; ?>
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>

<?php if($subadmin_restrictions->member!=1):  ?>
      <td>
        <a href="<?php echo base_url() ?>subadmin/member_notes/<?php echo $row->id; ?>" class="<?php echo $colors->notes_btn ?>" data-original-title="">Notes</a>
        <a href="<?php echo base_url() ?>subadmin/change_password/<?php echo $row->id; ?>" class="<?php echo $colors->change_password_btn ?>" data-original-title="">Change Password</a>
        <a href="<?php echo base_url() ?>subadmin/member_email/<?php echo $row->id; ?>" class="<?php echo $colors->email_btn ?>" data-original-title="">Email</a>
        <a href="<?php echo base_url() ?>subadmin/edit_member/<?php echo $row->id; ?>" id="update<?php echo $row->id; ?>" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">
          Edit
        </a>
        <a href="<?php echo base_url() ?>subadmin/delete_member/<?php echo $row->id; ?>"  onclick="return confirm('Are you want to delete?');" class="<?php echo $colors->delete_btn ?>">
         delete
        </a>
      </td>

    <?php endif; ?>
    <?php endif; ?>
    </tr>
  <?php $i++; endforeach ?>
<?php else: ?>
  <tr>
      <td style="width:20%" > No Records Found</td>
  </tr> 
<?php  endif ?>
</tbody>
</table>
         <?php if($members): ?>
              <!-- <div id="data-table_info" class="dataTables_info">Showing 1 to 10 of 16 entries</div> -->
              

                <div id="data-table_paginate" class="pull-right">
                  <?php if($pagination) echo $pagination; ?>
                    
                </div>
              </div>
             <?php endif; ?>
             <?php if (validation_errors()): ?>
                <input type="hidden" id="update_error" value="<?php echo 'update'.$mem_id;?>">
             <?php else: ?>
                <input type="hidden" id="update_error" value="">
             <?php endif; ?>
            <div class="clearfix"></div>
            
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
       

      });

      function form_submit(){
      var sort = document.getElementById('sort'); 
        if(sort != ''){
          $("form[name='myForm']").submit();
        }
        else{
          return false;
        }        
      }
      
    </script>   


    <?php $arr = getmember_montharr(); ?>
    <?php $act = getactivemember_montharr(); ?>
    <?php $inact = getinactivemember_montharr();  ?>



     <script type="text/javascript">
      google.load("visualization", '1', {packages:['corechart']});
          google.setOnLoadCallback(drawChart3);



        function drawChart3() {
          // var janactive = "<?php echo get_permonthactive('01') ?>";
          var janactive = "<?php echo count($act[1]); ?>";
          var febactive = "<?php echo count($act[2]); ?>";
          var maractive = "<?php echo count($act[3]); ?>";
          var apractive = "<?php echo count($act[4]); ?>";
          var mayactive = "<?php echo count($act[5]); ?>";
          var junactive = "<?php echo count($act[6]); ?>";
          var julactive = "<?php echo count($act[7]); ?>";
          var augactive = "<?php echo count($act[8]); ?>";
          var sepactive = "<?php echo count($act[9]); ?>";
          var octactive = "<?php echo count($act[10]); ?>";
          var novactive = "<?php echo count($act[11]); ?>";
          var decactive = "<?php echo count($act[12]); ?>";

          var janpending = "<?php echo count($inact[1]); ?>";
          var febpending = "<?php echo count($inact[2]); ?>";
          var marpending = "<?php echo count($inact[3]); ?>";
          var aprpending = "<?php echo count($inact[4]); ?>";
          var maypending = "<?php echo count($inact[5]); ?>";
          var junpending = "<?php echo count($inact[6]); ?>";
          var julpending = "<?php echo count($inact[7]); ?>";
          var augpending = "<?php echo count($inact[8]); ?>";
          var seppending = "<?php echo count($inact[9]); ?>";
          var octpending = "<?php echo count($inact[10]); ?>";
          var novpending = "<?php echo count($inact[11]); ?>";
          var decpending = "<?php echo count($inact[12]); ?>";

          var janfeatured = "<?php echo count($arr[1]); ?>";
          var febfeatured = "<?php echo count($arr[2]); ?>";
          var marfeatured = "<?php echo count($arr[3]); ?>";
          var aprfeatured = "<?php echo count($arr[4]); ?>";
          var mayfeatured = "<?php echo count($arr[5]); ?>";
          var junfeatured = "<?php echo count($arr[6]); ?>";
          var julfeatured = "<?php echo count($arr[7]); ?>";
          var augfeatured = "<?php echo count($arr[8]); ?>";
          var sepfeatured = "<?php echo count($arr[9]); ?>";
          var octfeatured = "<?php echo count($arr[10]); ?>";
          var novfeatured = "<?php echo count($arr[11]);?>";
          var decfeatured = "<?php echo count($arr[12]);?>";     




          var data = google.visualization.arrayToDataTable([
         ['Month','All Member', 'Active Member', 'Inactive Member'],
    ['Jan', Number(janfeatured), Number(janactive), Number(janpending)],
    ['Feb', Number(febfeatured), Number(febactive), Number(febpending)],
    ['Mar', Number(marfeatured), Number(maractive), Number(marpending)],
    ['Apr', Number(aprfeatured), Number(apractive), Number(aprpending)],
    ['May', Number(mayfeatured), Number(mayactive), Number(maypending)],
    ['jun', Number(junfeatured), Number(junactive), Number(junpending)],
    ['Jul', Number(julfeatured), Number(julactive), Number(julpending)],
    ['Aug', Number(augfeatured), Number(augactive), Number(augpending)],
    ['Sep', Number(sepfeatured), Number(sepactive), Number(seppending)],
    ['Oct', Number(octfeatured), Number(octactive), Number(octpending)],
    ['Nov', Number(novfeatured), Number(novactive), Number(novpending)],
    ['Dec', Number(decfeatured), Number(decactive), Number(decpending)] ]);

    //  ['Month', 'Bookings', 'Pendings', 'Cancel', 'Expenses'],
    // ['Jan', 300, 800, 900, 300],
    // ['Feb', 1170, 860, 1220, 564],
    // ['Mar', 260, 1120, 2870, 2340],
    // ['Apr', 1030, 540, 3430, 1200],
    // ['May', 200, 700, 1700, 770],
    // ['Jul', 1170, 2160, 3920, 800],
    // ['Aug', 1170, 2160, 3920, 800],
    // ['Sep', 1170, 2160, 3920, 800],
    // ['Oct', 1170, 2160, 3920, 800],
    // ['Nov', 1170, 2160, 3920, 800],
    // ['Dec', 2170, 1160, 2820, 500] ]);

          var options = {
            width: 'auto',
            height: '160',
            backgroundColor: 'transparent',
            colors: ['#3eb157', '#3660aa', '#d14836', '#dba26b', '#666666', '#f26645'],
            tooltip: {
              textStyle: {
                color: '#666666',
                fontSize: 11
              },
              showColorCode: true
            },
            legend: {
              textStyle: {
                color: 'black',
                fontSize: 12
              }
            },
            chartArea: {
              left: 60,
              top: 10,
              height: '80%'
            },
          };

          var chart = new google.visualization.ColumnChart(document.getElementById('mcolumn_chart'));
          chart.draw(data, options);
        }


    </script>

 