<div class="row-fluid">
  <div class="span9">
    <div class="widget">
      <div class="widget-header">
        <div class="title">
          <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Column Chart
        </div>
      </div>
      <div class="widget-body">
        <!-- <div id="column_chart"></div> -->
        <div id="selectionCharts" style="height:200px;"></div>
      </div>
    </div>
  </div>
  <div class="span3">
            <div class="row-fluid">
              
              <div class="span12">
                <div class="stats-count">
                  <span class="fs1 arrow text-success hidden-tablet" aria-hidden="true" data-icon="&#xe1bd;"></span> 
                  <h5 class="stat-value text-success"><?php echo get_browserpercent('Chrome') ?>%</h5>
                  <span class="stat-name">Chrome Users</span>
                </div>
                <div class="stats-count">
                  <span class="fs1 arrow text-error hidden-tablet" aria-hidden="true" data-icon="&#xe1be;"></span> 
                  <h5 class="stat-value text-error"><?php echo get_browserpercent('Firefox') ?>%</h5>
                  <span class="stat-name">Firefox Users</span>
                </div>
                <div class="stats-count">
                  <span class="fs1 arrow text-info hidden-tablet" aria-hidden="true" data-icon="&#xe1c1;"></span> 
                  <h5 class="stat-value text-info"><?php echo get_browserpercent('Safari') ?>%</h5>
                  <span class="stat-name">Safari Users</span>
                </div>
                <div class="stats-count">
                  <span class="fs1 arrow text-warning hidden-tablet" aria-hidden="true" data-icon="&#xe1bf;"></span> 
                  <h5 class="stat-value text-warning"><?php echo get_browserpercent('Internet Explorer') ?>%</h5>
                  <span class="stat-name">IE Users</span>
                </div>
              </div>
            </div>
          </div>
</div> 
        <div class="row-fluid">
          
        </div>

        <div class="row-fluid">
          <div class="span6">
            <div class="plain-header">
              <h4 class="title">
                Current Sales Status
              </h4>
            </div>
            <div class="row-fluid">
              <div class="span6">
                <div class="widget less-bottom-margin widget-border widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-info"><?php echo get_recentbookings(); ?></h4>
                      <p>Current Bookings</p>
                      <div class="type">
                        <span class="fs1 arrow text-info" aria-hidden="true" data-icon="&#xe048;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="widget less-bottom-margin widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-success"><?php echo get_allbookings(); ?></h4>
                      <p>Total Bookings</p>
                      <div class="type">
                        <span class="fs1 arrow text-success" aria-hidden="true" data-icon="&#xe036;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row-fluid">
              <div class="span6">
                <div class="widget widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-warning"><?php echo get_pending_booking(); ?></h4>
                      <p>Processing</p>
                      <div class="type">
                        <span class="fs1 arrow text-warning" aria-hidden="true" data-icon="&#xe077;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="widget widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4><?php echo get_cancelled_booking(); ?></h4>
                      <p>Cancelled</p>
                      <div class="type">
                        <span class="fs1 arrow" aria-hidden="true" data-icon="&#xe0fa;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="span6">
            <div class="plain-header">
              <h4 class="title">
                Site Status
              </h4>
            </div>
            <div class="row-fluid">
              <div class="span6">
                <div class="widget less-bottom-margin widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-info"><?php echo get_totalvisit(); ?></h4>
                      <p>Total Visits</p>
                      <div class="type">
                        <span class="fs1 arrow text-info" aria-hidden="true" data-icon="&#xe071;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="widget less-bottom-margin widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-error"><?php echo get_totalprop(); ?></h4>
                      <p>Total Properties</p>
                      <div class="type">
                        <span class="fs1 arrow text-error" aria-hidden="true" data-icon="" ></span> <!-- data-icon="&#xe0c6;" -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row-fluid">
              <div class="span6">
                <div class="widget widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-warning"><?php echo get_totalusers(); ?></h4>
                      <p>Total Users</p>
                      <div class="type">
                        <span class="fs1 arrow text-warning" aria-hidden="true" data-icon="&#xe070;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="widget widget-border">
                  <div class="widget-body">
                    <div class="current-stats">
                      <h4 class="text-success"><?php echo get_totalneighbour() ?></h4>
                      <p>Total Neighbourhood</p>
                      <div class="type">
                        <span class="fs1 arrow text-success" aria-hidden="true" data-icon="&#xe04d;"></span> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row-fluid">
          <!-- <div class="span9">
            <div class="widget">
              <div class="widget-header">
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon="&#xe1cd;"></span> Auto Updating Chart
                </div>
                <div class="tools pull-right">
                </div>
              </div>
              <div class="widget-body">
                <div id="realtimechart" style="height:160px;"></div>
              </div>
            </div>
          </div> -->

          <div class="span9">
            <div class="widget">
              <div class="widget-header">
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon="&#xe07d;"></span>Twitter feeds
                </div>
              <!--   <div class="tools">
                  <ul class="sliding-tags">
                    <li>
                      <a href="#">Recent<span>95</span></a>
                    </li>
                    <li>
                      <a href="#">Important<span>75</span></a>
                    </li>
                    <li>
                      <a href="#">View All<span>275</span></a>
                    </li>
                  </ul>
                </div> -->
              </div>
              <div class="widget-body">
                <div id="scrollbar-three">
                  <div class="scrollbar">
                    <div class="track">
                      <div class="thumb">
                        <div class="end">                            
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="viewport">
                    <div class="overview">
                      <ul class="imp-messages">
                        <?php $tweets = @get_twitter_feed(); //print_r($tweets); die(); ?>
                        <?php if (@$tweets): foreach (@$tweets as $key): ?>
                          <li>
                            <img alt="Avatar" class="avatar" src="<?php echo @$key->user->profile_image_url ?>">
                             <div class="message-date">
                              <h3 class="date text-info"><?php echo date('d', strtotime(@$key->created_at)); ?></h3>
                              <p class="month"><?php echo date('F', strtotime(@$key->created_at)); ?></p>
                            </div>
                            <div class="message-wrapper">
                              <h4 class="message-heading"><?php echo @$key->text ?></h4>
                              <p class="message">                               
                              </p>
                              <p></p><br>
                            </div>
                          </li>
                        <?php endforeach; endif; ?>
                      
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="span3">
            <div class="widget">
              <div class="widget-header">
                <div class="title">
                  Social Graph
                </div>
              </div>
              <div class="widget-body">
                <div id="socialGraph" style="height: 160px;"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row-fluid">
          
        </div>

        <div class="row-fluid">
          <div class="span6">
            <div class="widget no-margin">
              <div class="widget-header">
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon="&#xe053;"></span> Upcoming Events
                </div>
                <div class="tools pull-right">
                </div>
              </div>
              <div class="widget-body">
                <div id='calendar'></div>
              </div>
            </div>
          </div>

          <div class="span6">
            <div class="widget no-margin">
              <div class="widget-header">
                <div class="icontype">

                </div>
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon="&#xe0fe;"></span> Todo list
                </div>
              </div>
              <div class="widget-body">
                <div class="todo-container">

<!-- Todo list system -->
<ul class="todo-list" style="overflow:auto;max-height:400px">
    <!-- <span class='completed/new/process'> -->
    <?php if(!empty($events)): ?>
    <?php foreach($events as $event ): ?>
    <li id="delet_<?php echo $event->id ?>">
        <?php 
        // get usefull parameter 
            $time   = time();
            $start  = strtotime($event->startDate.' '.$event->startTime);
            $end    = strtotime($event->endDate.' '.$event->endTime);
            $status = $event->status;
        ?>     
        <?php 
        // findout class name out of above 3 classes.
            $class  = '';
            $remark = '';
            $delete = '';
            if($status == 1 )
            {
                $class  = 'completed';
                $delete = 'checked="checked"';
                $remark = 'completed';
            }
            elseif($time > $start and $time < $end and $status != 1)
            {
                $class  = 'process';
                $remark = 'process';
            }
            elseif($time < $start and $status != 1)
            {
                $class  = 'new';
                $remark = 'new';
            }
            else
            {
                $class  = 'new';
                $remark = 'due';
            }
         ?>
        <span class="<?php echo $class; ?>"  name='<?php echo $event->id; ?>'></span>
        <input onclick='do_complete(<?php echo $event->id; ?>)'  type="checkbox"  id="<?php echo $event->id; ?>"  <?php echo $delete; ?>  >
        <label>
      <a href="<?php echo base_url() ?>subadmin/timeline">  <?php echo $event->title; ?></a>
        </label>
        <span class="date">
            <span remark='<?php echo $event->id; ?>' ><?php echo $remark; ?></span>&nbsp;    
        <?php echo date('M d',strtotime($event->startDate));  ?>
        </span>
    </li>
    <?php endforeach; ?>
    <?php else: ?>
        There is no task in Todo list.
    <?php endif; ?>
</ul>
<script>
function do_complete(event_id)
{
    var x = $('#'+event_id).prop('checked');
    if(x == true)
        { 
            var status = 1;  
            var addClass = 'completed';
            var removeClass = 'process';
            $('#delet_'+event_id).hide();
        }  
    else
        { 
            var status = 0;  
            var addClass = 'process';
            var removeClass = 'completed';  
        }
    $.ajax({
            type:'POST',
            url:'<?php echo base_url(); ?>subadmin/do_complete/'+event_id+'/'+status,
            success:function(res)
            {
                $('span[name='+event_id+']').removeClass(removeClass);
                $('span[name='+event_id+']').addClass(addClass);
                $('span[remark='+event_id+']').html(addClass);
            }
    });
}
</script>                  
<!-- Todo list system -->






                  <!-- <form action="#" class="no-margin">
                    <div class="control-group">
                      <div class="controls">
                        <textarea class="input-block-level" placeholder="Add new task"></textarea>
                      </div>
                    </div>
                    <div class="control-group no-margin">
                      <div class="controls">
                        <button type="submit" class="btn btn-info pull-right">Create</button>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </form> -->
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

        <?php 
        $twitter_data = get_twitter_feed();
        $twtcount = @$twitter_data[0]->user->followers_count;
       ?>

      <input type="hidden" id="countfb" value="<?php echo countfollow(); ?>">
      <input type="hidden" id="counttwitter" value="<?php echo $twtcount; ?>">
  <script type="text/javascript">
    // Morris Charts and Graphs
function socialGraph(){
  var fb =  $('#countfb').val();
  var twt =  $('#counttwitter').val();
  Morris.Donut({
    element: 'socialGraph',
    data: [
      {value: parseInt(fb), label: 'Facebook'},
      {value: parseInt(twt), label: 'Twitter'}
      // {value: 700, label: 'Google+'},
      // {value: 800, label: 'Linkedin'}
    ],
    labelColor: '#666666',
    colors: [
      '#333333',
      '#555555',
      '#777777',
      '#999999',
      '#aaaaaa'
    ],
    // formatter: function (x) { return x + "%"}
  });
}
$(document).ready(function () {
  socialGraph();
});

//Resize charts and graphs on window resize
$(document).ready(function () {
  $(window).resize(function(){
    socialGraph();
  });
});


// Calendar
$(document).ready(function() {

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

var arrval = '<?php echo getcalendar_array(); ?>';
var arr = JSON.parse(arrval);

var buildingEvents = $.map(arr, function(value) {  
  var sth = value.st.split(':');
  var eth = value.et.split(':');  
  // alert(value.important);
    if(value.important == 1){
      return {
          title: value.title,
          start: new Date(value.sd_y, value.ed_m, value.sd_d, sth[0], sth[1]),
          end: new Date(value.ed_y, value.sd_m, value.sd_d, eth[0], eth[1]),
          allDay: false,
          color:'#d13736'
         
      };
    }else{
      return {
          title: value.title,
          start: new Date(value.sd_y, value.ed_m, value.sd_d, sth[0], sth[1]),
          end: new Date(value.ed_y, value.sd_m, value.sd_d, eth[0], eth[1]),
          allDay: false,          
          // draggable:false,
          // backgroundColor: 'red'
      };

    }

})

  // $.each(arr,function(index,value){
  //   console.log(value);
  // });  
  
  var calendar = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    selectable: true,
    selectHelper: true,    
    // dayClick: function(date, allDay, jsEvent, view) {
    //   return false;
    // },
    select: function(start, end, allDay) {
      // var title = prompt('Event Title:');
      // if (title) {
      //   calendar.fullCalendar('renderEvent',
      //     {
      //       title: title,
      //       start: start,
      //       end: end,
      //       allDay: allDay
      //     },
      //     true // make the event "stick"
      //   );
      // }
      // calendar.fullCalendar('unselect');
    },
    editable: false,    
    events: buildingEvents,    
    // [

        
      // {
      //   title: 'All Day Event',
      //   start: new Date(y, m, 1)
      // },
      // {
      //   id: 999,
      //   title: 'Repeating Event',
      //   start: new Date(y, m, d-3, 16, 0),
      //   allDay: false
      // },
      // {
      //   title: 'Meeting',
      //   start: new Date(y, m, d, 10, 30),
      //   allDay: false
      // },
      // {
      //   title: 'Lunch',
      //   start: new Date(y, m, d, 12, 0),
      //   end: new Date(y, m, d, 14, 0),
      //   allDay: false
      // },
      // {
      //   title: 'Birthday Party',
      //   start: new Date(y, m, d+1, 19, 0),
      //   end: new Date(y, m, d+1, 22, 30),
      //   allDay: false
      // }
    // ]
  });
});
</script>

<?php
 $bookings   = thisyear_booking();    
  // print_r($bookings);
 $barr = array();
 foreach ($bookings as $key => $value) {
        $barr[] = "[ ".$key." , ".count($value)." ]";   
 }

 $properties = thisyear_listings();

 $parr = array();
 foreach ($properties as $key => $value) {
        $parr[] = "[ ".$key." , ".count($value)." ]";   
 }
?>

<?php   
  // $r = '[1990, 2], [1991, 37], [1992, 12], [1993, 72], [1994, 24], [1995, 113], [1996, 27], [1997, 245], [1998, 26], [1999, 189], [2000, 72], [2001, 158], [2002, 38], [2003, 126], [2004, 43], [2005, 126], [2006, 39], [2007, 112], [2008, 25], [2009, 18], [2010, 110], [2011, 34], [2012, 158], [2013, 23]';
  $r = implode(',', $barr);
  $p = implode(',', $parr);
 ?>

<script type="text/javascript">
    $(function() {

  var data = [{
    label: "Bookings",
    data: [<?php echo $r; ?>]
  },
  {
    label: "Properties",
    data: [<?php echo $p; ?>]
  }];

  var options = {
    series: {
      lines: { show: true,
        lineWidth: 2,
        fill: false,
        },
      points: { show: true, 
        lineWidth: 2 
        },
      shadowSize: 0
    },
    grid: { hoverable: true, 
      clickable: true, 
      tickColor: "#eeeeee",
      borderWidth: 0
    },
    legend: {
      noColumns: 3
    },
    colors: ["#3b5a9b", "#d3503e"],
     xaxis: {ticks:12, tickDecimals: 0},
     yaxis: {ticks:3, tickDecimals: 0},
    selection: {
      mode: "x"
    }
  };

  var placeholder = $("#selectionCharts");

  placeholder.bind("plotselected", function (event, ranges) {

    $("#selection").text(ranges.xaxis.from.toFixed(1) + " to " + ranges.xaxis.to.toFixed(1));

    var zoom = $("#zoom").attr("checked");

    if (zoom) {
      plot = $.plot(placeholder, data, $.extend(true, {}, options, {
        xaxis: {
          min: ranges.xaxis.from,
          max: ranges.xaxis.to
        }
      }));
    }
  });

  placeholder.bind("plotunselected", function (event) {
    $("#selection").text("");
  });

  var plot = $.plot(placeholder, data, options);

  $("#clearSelection").click(function () {
    plot.clearSelection();
  });

  $("#setSelection").click(function () {
    plot.setSelection({
      xaxis: {
        from: 1994,
        to: 1995
      }
    });
  });
  // Add the Flot version string to the footer
  $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
});
</script>
  