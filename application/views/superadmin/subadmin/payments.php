<?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>


<div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Payment Chart
          </div>
        </div>
        <div class="widget-body">
          <div id="pay_chart"></div>
        </div>
      </div>
    </div>
  </div>


<?php if($this->session->flashdata('success_msg')): ?>
    <div class="alert alert-success">
      <?php echo $this->session->flashdata('success_msg');?>
    </div>
  <?php endif ?>
  <?php if($this->session->flashdata('error_msg')): ?>
    <div class="alert alert-error">
      <?php echo $this->session->flashdata('error_msg');?>
    </div>
  <?php endif ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">

          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> <a href="<?php echo base_url() ?>subadmin/payments"> Payments </a>                   

          </div>
        <?php if($subadmin_restrictions->payments!=1):  ?>
          <div class="pull-right">
            <a href="<?php echo base_url();?>subadmin/security_deposit" class="btn">Security Deposit</a>                      
            <a href="<?php echo base_url();?>subadmin/coupons" class="btn">Coupons</a>                      
            <a href="<?php echo base_url();?>subadmin/referral_payments" class="btn">Referral payments</a>                      
            <a href="<?php echo base_url();?>subadmin/transaction_history" class="btn">Transaction History</a>                      
          </div>
        <?php endif; ?>
        </div>
        <div class="widget-body">
          <div class="row" style="margin-left:0%;">
          <div class="span2">           
              <?php //$attributes = array('name'=>'myForm'); echo form_open(base_url().'subadmin/bookings',$attributes); ?>
                <select onchange="return submit_sort();"  class="span10" id="sort" name="sort"> 
                  <option value="newest" <?php if($this->uri->segment(3) == 'newest') echo "selected='selected'";  ?> >Newest</option>                  
                  <option value="oldest" <?php if($this->uri->segment(3) == 'oldest') echo "selected='selected'";  ?> >Oldest</option>                  
                  <option value="refund" <?php if($this->uri->segment(3) == 'refund') echo "selected='selected'";  ?> >Refund Payments</option>                  
                  <option value="paid_to_owner" <?php if($this->uri->segment(3) == 'paid_to_owner') echo "selected='selected'";  ?> >Paid to owner</option>                  
                </select>
              <?php //echo form_close(); ?>    
          </div>
          </div>
          
          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <!-- Advance search box Starts -->
          <div class="row" style="margin-left:0%;">
           <h4>Search Criteria</h4><br>
            <?php  $attributes = array('name'=>'search_form'); echo form_open(current_url(),$attributes);?>
            <div class="criteria div">
              <div style="float:left">
                   <input  name='book_number'  placeholder="Booking Number" type="text">
              </div>
              <div style="float:left;margin-left:0.5%">
                   <input  name='guest_name'  placeholder="Guest name" type="text">
              </div>
              <div style="float:left;margin-left:0.5%">
                   <input  name='host_name'  placeholder="Host name" type="text">
              </div>
              <div style="float:left;margin-left:0.5%">
                   <input  name='property_name'  placeholder="Property name" type="text">
              </div>
          </div>
          <br><br><br>
          <div style="float:left">
             <button class="btn" id="search" onclick="return form_submit_name();" type="submit">Search</button>
             <button class="btn" href="<?php echo base_url() ?>subadmin/bookings" >Show All</button>
          </div>

            <?php echo form_close(); ?>
          </div>

          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->
          <!-- Advance search box Ends -->

  <div style="" class="row-fluid">
      <h4>Export Payments</h4><br>
      <?php echo form_open(base_url().'export/export_payments'); ?>
        <div class="row-fluid">
          <input type="hidden" name="type" value="subadmin">
            <div class="controls">
                <div class="input-append">
                    <input id="date_range1" class="span2 date_picker" type="text" placeholder="Select Date" name="report_range2">
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select  name="export_sort"> 
                    <option value="">All</option>
                    <option value="newest" >Newest</option>                  
                    <option value="oldest" >Oldest</option>                  
                    <option value="refund" >Refund Payments</option>                  
                    <option value="paid_to_owner" >Paid to owner</option>                  
                  </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
          <span class="inline radio"><input type="radio" name="export_file_format" value="csv">CSV</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" value="excel">Excel</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</span>
          <br><br>
          <span ><input type="submit" value="Export" class="btn-info btn">
        </div>  
          <?php echo form_close(); ?>        
             </div>

          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

            
                  <th style="width:10%;">Booking id</th>
                  <th style="width:8%;">Host Name</th>
                  <th style="width:8%;">Guest Name</th>
                  <th style="width:8%;">Listing</th>
                  <th style="width:9%;">Amount</th>
                  <th style="width:9%;">Booking Date</th>
                  <th style="width:9%;">Payment Status</th>
                  <th style="width:13%">Host payment status</th>   
        <?php if($subadmin_restrictions->payments!=1):  ?>
                 <th style="width:35%;" class="hidden-phone">Actions</th>
          <?php endif; ?>       
                </tr>
              </thead>
              <tbody>

               <?php if(!empty($payments)):?> 
               <?php  foreach ($payments as $row): ?>
               <?php $pr_detail = property_details($row->property_id); //print_r($pr_detail);  ?>                      
                <tr class="gradeA">
                  <?php $owner = get_user_info($row->owner_id); ?>
                  <td><a href="<?php echo base_url() ?>subadmin/booking_detail/<?php echo $row->booking_id ?>"><?php echo $row->booking_id ?></a></td>
                  <td><?php echo $owner->first_name; ?></td>
                  <td><?php echo $row->first_name; ?></td>                
                  <td><?php echo word_limiter(@$pr_detail->title,3); ?></td>
                  <td><?php echo $row->amount; ?></td>
                  <td><?php echo date('m/d/Y', strtotime($row->created)); ?></td>
                  <td><?php if ($row->is_refund == 1){ echo "Refud"; }else{ echo "Completed"; } ?></td>
                  <td><?php if ($row->paid_to_owner == 1){ echo "Paid"; }else{ echo "Pending"; } ?></td>
                   <!-- <td> <a href="<?php //echo base_url();?>subadmin/add_booking_note/<?php echo $row->id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a></td> -->
                  
                    <?php $colors = get_buttons_color();  ?>
                    <?php if(!empty($colors)): ?>

            <?php if($subadmin_restrictions->payments!=1):  ?>
                    <?php if ($row->paid_to_owner != 1 && $row->is_refund != 1): ?>                       
                  <td>                     
                      <a href="<?php echo base_url() ?>subadmin/pay_to_users/<?php echo $row->id; ?>" onclick="return confirm('Are you sure?')"  class="<?php echo $colors->pay_to_host_btn ?>" data-original-title="">Pay to host</a>
                    <?php endif ?>   

                     <?php if ($row->is_refund != 1 && $row->paid_to_owner != 1): ?>                       
                      <a href="<?php echo base_url() ?>subadmin/refund_money/<?php echo $row->id; ?>" onclick="return confirm('Are you sure?')"  class="<?php echo $colors->delete_btn ?>" data-original-title="">Refund</a>
                    <?php endif ?>                
                    <a href="<?php echo base_url() ?>subadmin/reciept/<?php echo $row->booking_id ?>" class="<?php echo $colors->details_btn ?>">
                     Details
                   </a>
                    <a href="<?php echo base_url() ?>subadmin/payment_notes/<?php echo $row->id; ?>" class="<?php echo $colors->notes_btn ?>">
                     Notes
                    </a>
                  <?php endif; ?>

                  </td>
        <?php endif; ?>          
                </tr>
             <?php endforeach ?>
           <?php else: ?>
            <tr><td colspan="8">No Records Found</td></tr>
             <?php  endif ?>
            </tbody>
            </table>
              <div id="data-table_info" class="dataTables_info"></div>
                <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">

                   <?php if($pagination) echo $pagination; ?>
                   <!--  <a id="data-table_first" class="first paginate_button paginate_button_disabled" tabindex="0">First</a>
                      <a id="data-table_previous" class="previous paginate_button paginate_button_disabled" tabindex="0">Previous</a>
                       <span>
                          <a class="paginate_active" tabindex="0">1</a>
                          <a class="paginate_button" tabindex="0">2</a>
                      </span>
                      <a id="data-table_next" class="next paginate_button" tabindex="0">Next</a>
                    <a id="data-table_last" class="last paginate_button" tabindex="0">Last</a> -->
                </div>
              </div>
            <div class="clearfix"></div>
           
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
       function form_submit(){
          var sort = document.getElementById('sort'); 
            if(sort != ''){
              $("form[name='myForm']").submit();
            }
            else{
              return false;
            }        
        }


        function submit_sort(){
          var val = $('#sort').val();
          window.location='<?php echo base_url() ?>subadmin/payments/'+val;
        }

function form_submit_name(){
var name  = $('#srch').val();
if(name == '')
{
  alert('Please Enter some value');
  return false;
}
else
{
 return true;
}        
}
    </script>

     <?php $arr = allpayment_montharr(); ?>
    <?php $act = userpaid_montharr(); ?>
    <?php $inact = refundpay_montharr();  ?>



     <script type="text/javascript">
      google.load("visualization", '1', {packages:['corechart']});
          google.setOnLoadCallback(drawChart3);



        function drawChart3() {
          // var janactive = "<?php echo get_permonthactive('01') ?>";
          var janactive = "<?php echo count($act[1]); ?>";
          var febactive = "<?php echo count($act[2]); ?>";
          var maractive = "<?php echo count($act[3]); ?>";
          var apractive = "<?php echo count($act[4]); ?>";
          var mayactive = "<?php echo count($act[5]); ?>";
          var junactive = "<?php echo count($act[6]); ?>";
          var julactive = "<?php echo count($act[7]); ?>";
          var augactive = "<?php echo count($act[8]); ?>";
          var sepactive = "<?php echo count($act[9]); ?>";
          var octactive = "<?php echo count($act[10]); ?>";
          var novactive = "<?php echo count($act[11]); ?>";
          var decactive = "<?php echo count($act[12]); ?>";

          var janpending = "<?php echo count($inact[1]); ?>";
          var febpending = "<?php echo count($inact[2]); ?>";
          var marpending = "<?php echo count($inact[3]); ?>";
          var aprpending = "<?php echo count($inact[4]); ?>";
          var maypending = "<?php echo count($inact[5]); ?>";
          var junpending = "<?php echo count($inact[6]); ?>";
          var julpending = "<?php echo count($inact[7]); ?>";
          var augpending = "<?php echo count($inact[8]); ?>";
          var seppending = "<?php echo count($inact[9]); ?>";
          var octpending = "<?php echo count($inact[10]); ?>";
          var novpending = "<?php echo count($inact[11]); ?>";
          var decpending = "<?php echo count($inact[12]); ?>";

          var janfeatured = "<?php echo count($arr[1]); ?>";
          var febfeatured = "<?php echo count($arr[2]); ?>";
          var marfeatured = "<?php echo count($arr[3]); ?>";
          var aprfeatured = "<?php echo count($arr[4]); ?>";
          var mayfeatured = "<?php echo count($arr[5]); ?>";
          var junfeatured = "<?php echo count($arr[6]); ?>";
          var julfeatured = "<?php echo count($arr[7]); ?>";
          var augfeatured = "<?php echo count($arr[8]); ?>";
          var sepfeatured = "<?php echo count($arr[9]); ?>";
          var octfeatured = "<?php echo count($arr[10]); ?>";
          var novfeatured = "<?php echo count($arr[11]);?>";
          var decfeatured = "<?php echo count($arr[12]);?>";     




          var data = google.visualization.arrayToDataTable([
         ['Month','All Payment', 'Paid to User', 'Refund'],
    ['Jan', Number(janfeatured), Number(janactive), Number(janpending)],
    ['Feb', Number(febfeatured), Number(febactive), Number(febpending)],
    ['Mar', Number(marfeatured), Number(maractive), Number(marpending)],
    ['Apr', Number(aprfeatured), Number(apractive), Number(aprpending)],
    ['May', Number(mayfeatured), Number(mayactive), Number(maypending)],
    ['jun', Number(junfeatured), Number(junactive), Number(junpending)],
    ['Jul', Number(julfeatured), Number(julactive), Number(julpending)],
    ['Aug', Number(augfeatured), Number(augactive), Number(augpending)],
    ['Sep', Number(sepfeatured), Number(sepactive), Number(seppending)],
    ['Oct', Number(octfeatured), Number(octactive), Number(octpending)],
    ['Nov', Number(novfeatured), Number(novactive), Number(novpending)],
    ['Dec', Number(decfeatured), Number(decactive), Number(decpending)] ]);

    //  ['Month', 'Bookings', 'Pendings', 'Cancel', 'Expenses'],
    // ['Jan', 300, 800, 900, 300],
    // ['Feb', 1170, 860, 1220, 564],
    // ['Mar', 260, 1120, 2870, 2340],
    // ['Apr', 1030, 540, 3430, 1200],
    // ['May', 200, 700, 1700, 770],
    // ['Jul', 1170, 2160, 3920, 800],
    // ['Aug', 1170, 2160, 3920, 800],
    // ['Sep', 1170, 2160, 3920, 800],
    // ['Oct', 1170, 2160, 3920, 800],
    // ['Nov', 1170, 2160, 3920, 800],
    // ['Dec', 2170, 1160, 2820, 500] ]);

          var options = {
            width: 'auto',
            height: '160',
            backgroundColor: 'transparent',
            colors: ['#3eb157', '#3660aa', '#d14836', '#dba26b', '#666666', '#f26645'],
            tooltip: {
              textStyle: {
                color: '#666666',
                fontSize: 11
              },
              showColorCode: true
            },
            legend: {
              textStyle: {
                color: 'black',
                fontSize: 12
              }
            },
            chartArea: {
              left: 60,
              top: 10,
              height: '80%'
            },
          };

          var chart = new google.visualization.ColumnChart(document.getElementById('pay_chart'));
          chart.draw(data, options);
        }


    </script>

