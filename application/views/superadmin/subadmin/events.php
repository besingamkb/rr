      <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>
 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Events
          </div>
        <?php if($subadmin_restrictions->timeline!=1):  ?>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/addEvent"> Add Event </a>
          </div>
        <?php endif; ?>
        </div>
        <div class="widget-body">
          <?php alert(); ?>

  <div style="" class="row-fluid">
      <h4>Export Events</h4><br>
      <?php echo form_open(base_url().'export/export_events'); ?>
        <div class="row-fluid">
          <input type="hidden" name="type" value="subadmin">
            <div class="controls">
                <div class="input-append">
                    <input id="date_range1" class="span2 date_picker" type="text" placeholder="Select Date" name="report_range2">
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select  name="export_sort"> 
                    <option value="">All</option>
                    <option value="newest" >Newest</option>                  
                    <option value="oldest" >Oldest</option>                  
                  </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
          <span class="inline radio">
          <label><input type="radio" name="export_file_format" value="csv">CSV</label></span>
          <span class="inline radio" style="margin-left:1%">
          <label><input type="radio" name="export_file_format" value="excel">Excel</label></span>
          <span class="inline radio" style="margin-left:1%">
          <label><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</label></span>
          <br><br>
          <span ><input type="submit" value="Export" class="btn-info btn">
        </div>  
          <?php echo form_close(); ?>        
      </div>


          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:15%">Event</th>
                  <th style="width:15%">Start date</th>
                  <th style="width:15%">End date</th>
                  <th style="width:15%">Start time</th>
                  <th style="width:15%">End time</th>

                  <?php if($subadmin_restrictions->timeline!=1):  ?>
                    <th style="width:20%">Actions</th>
                  <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($events)): ?>
                    <?php $i=1; foreach ($events as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->title; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->startDate)); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->endDate)); ?></td>
                        <td><?php echo $row->startTime; ?></td>
                        <td><?php echo $row->endTime; ?></td>

                        <?php $colors = get_buttons_color();  ?>

                        <?php if(!empty($colors)): ?>
                        <?php if($subadmin_restrictions->timeline!=1):  ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/editEvent/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                          <a href="<?php echo base_url()?>subadmin/deleteEvent/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                        </td>
                      <?php endif; ?>
                      <?php else: ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/editEvent/<?php echo $row->id;?>" id="" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">Edit</a>
                          <a href="<?php echo base_url()?>subadmin/deleteEvent/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-success btn-small hidden-phone" data-original-title="">Delete</a>
                        </td>
                      <?php endif; ?>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="4"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
            <?php echo $pagination; ?>
          </div>
        </div>
      </div>
    </div>