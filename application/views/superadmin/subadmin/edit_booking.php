<style type="text/css">
.unique_feature {
color: #777;
float: left;
height: 50px;
padding: 0 5px 10px;
width: 126px;
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span> Edit Booking
</div>
</div>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
<?php $guests = get_users();?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Guest Name
    </label>
    <div class="controls controls-row span6">
        <input  name="guest" id="guest" type="hidden">
            <select onchange="return get_guest_info();" id="guest_name" name="guest_name" class="span10 input-left-top-margins">
                <option value="">Select Guest Name</option>
                <?php foreach ($guests as $row): ?>
                    <option value="<?php echo $row->id; ?>" <?php if($booking->guest_name == $row->id ) echo "selected"; ?>    ><?php echo $row->first_name.' '.$row->last_name; ?></option>
                <?php endforeach; ?>
        </select>
        <span class="form_error span12"><?php echo form_error('guest_name'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Guest Email
    </label>
    <div class="controls controls-row span6">
        <input name="email" id="email" class="span12" type="text" placeholder="Email"  value="<?php echo $booking->email; ?>">
       <span class="form_error span12"><?php echo form_error('email'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Guest Contact No.
    </label>
    <div class="controls controls-row span6">
        <input name="phone" class="span12" id="phone" type="text" placeholder="Contact Number" value="<?php echo $booking->contact;  ?>">
        <span class="form_error span12"><?php echo form_error('phone'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Guest Full Address
    </label>
    <div class="controls controls-row span6">
        <input name="address" value="<?php echo $booking->address; ?>" class="span12" id="address" type="text" placeholder="Address">
        <span class="form_form_error span12"><?php  //echo form_error('address'); ?></span>
    </div>
</div>

<?php $users = get_all_user();?>
<div class="control-group">
    <label class="control-label" >
        Host Email
    </label>
    <div class="controls controls-row span6">
        <input name="host_email" value="<?php $Email =get_host_email($booking->pr_id);  echo $Email;?>"  class="span12" id="host_email" type="text" placeholder="Host Email" >
        <span class="form_error span12"><?php  echo form_error('host_email'); ?></span>
    </div>
</div>


               <div class="control-group">
                <label class="control-label">
                 Propety Title <span style="font-size:10px;">(Selected)</span>
                </label>
                <div class="controls controls-row span6">
                  <input name="property title" class="span12" type="text" placeholder="State" value="<?php $title =get_property_title($booking->pr_id);  echo $title;?>">
                  <span class="form_error span12"><?php // echo form_error('state'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                Property Titles 
                </label>
               
                <div id="user_properties" class="controls controls-row span10">
                  
                <input type="hidden" name="property_id" value="">
                </div>
                 <span class="form_error span12"><?php echo form_error('property_id'); ?></span>
              </div>
              <br>
             

<div class="control-group">
    <label class="control-label">
        Check In 
    </label>
    <div  class="controls controls-row span7">
        <div class="input-append">
            <input id="start" class="span12" type="text" placeholder="Select Date" name="check_in" value="<?php  //echo $booking->check_in; ?>" >
            <span class="add-on">
                <i class="icon-calendar"></i>
            </span>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php  echo  $booking->check_in; ?>
    <span class="form_error span12"><?php echo form_error('check_in'); ?></span>
    </div>
</div>
<div class="control-group">
    <label class="control-label">
        Check Out 
    </label>
    <div  class="controls controls-row span7">
        <div class="input-append">
            <input id="end" class="span12" type="text" placeholder="Select Date" name="check_out"  >
            <span class="add-on">
                <i class="icon-calendar"></i>
            </span>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php  echo  $booking->check_out; ?>
    <span class="form_error span12"><?php echo form_error('check_out'); ?></span>
    </div>
</div>


<div class="control-group">
    <label class="control-label">
        Due Amount
    </label>
    <div class="controls controls-row span6">
        <input name="due_amount" value="<?php  echo  $booking->due_amount; ?>"  class="span12" type="text" placeholder="Due Amount" value="<?php echo set_value('due_amount'); ?>">
        <span class="form_error span12"><?php echo form_error('due_amount'); ?></span>
    </div>
</div>

<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
    function get_guest_info() 
    {
        var guest_id = document.getElementById('guest_name').value;
        $.post( "<?php echo base_url()?>subadmin/user_information",{'user_id':guest_id })
        .done(function( data ) {

        var user_info = $.parseJSON(data);
        document.getElementById('guest').value = user_info['first_name']+' '+user_info['last_name'];
        document.getElementById('email').value = user_info['user_email'];
        document.getElementById('phone').value = user_info['phone'];
        document.getElementById('address').value = user_info['address']+' '+user_info['city']+' '+user_info['state'];
        });          
    }
</script>

<script>
    $(function(){
        var user = new Array();
        <?php foreach($users as $row){ ?>
            user.push('<?php echo $row->user_email; ?>');
        <?php } ?>
        $( "#host_email" ).autocomplete({
            lookup: user,
            onSelect: function(suggestion)
                {
                $('#user_properties').html('');
                var host_email = document.getElementById('host_email').value; 
                $.post( "<?php echo base_url()?>subadmin/user_property",{'user_email':host_email })
                    .done(function( data ) {
                    $('#user_properties').append(data);
                    });
                }
        });
    });  
</script>


<script type="text/javascript">
     var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
     
    var checkin = $('#start').datepicker({
    onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1);
    checkout.setValue(newDate);
    }
    checkin.hide();
    $('#end')[0].focus();
    }).data('datepicker');
    var checkout = $('#end').datepicker({
    onRender: function(date) {
    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    checkout.hide();
    }).data('datepicker');
</script>