
<link rel="stylesheet" href="<?php echo base_url() ?>assets/drag_drop_user_pic/css/drag_drop.css" />
<script src="<?php echo base_url() ?>assets/drag_drop_user_pic/js/jquery.filedrop.js"></script>

<style type="text/css">
#dropbox{
   margin-top:5% !important;
   margin-left:7% !important;
   border-top:0px !important;
   background-color:#EEEEEE;
   width: 85% !important;

   min-height: 300px !important;
} 
.message{
   padding-top: 40px !important;
}
.progress
{
  background-color:#E05284 !important;
  background-image: none !important;
  border-radius: 0px !important;
  box-shadow: none !important; 
}
</style>

   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title" style="width:19%">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Property Images
            </div>
            <div class="pull-left">
              <a  class="btn" href="<?php echo base_url(); ?>subadmin/see_all_property_images/<?php echo $property_id ?>">See All Uploaded Images</a>  
            </div>
          </div>

<div class="widget-body">
  <div class="form-horizontal no-margin well" style="height:auto !imporatnt">

                 <div style="width:100%">
                 <!-- Darg Drop Box Starts  -->
                        <div id="dropbox" style="min-height:400px;"  >
                           <span class="message">
                               <h4  style="color:#8F8F8F !important">
                                  Drop images/video  here to upload. 
                               </h4>
                               <br>
                               <br>
                               <img  src="<?php echo base_url()?>assets/drag_drop_user_pic/images/smallUploadArea.png" style="border-radius:5px; width:160px; height:100px;">
                            </span>
                        </div> 
                 <!-- Darg Drop Box Ends  -->
                 </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Script.js file drag and drop Starts -->   
  <script>
$(function(){
  
  var dropbox = $('#dropbox'),
    message = $('.message', dropbox);
  var property_id = <?php echo $property_id ?>;
  
  dropbox.filedrop({
    // The name of the $_FILES entry:
    paramname:'pic',
    maxfiles: 5,
    maxfilesize: 10,
    data:{property_id:property_id},
    url: '<?php echo base_url() ?>subadmin/ajax_property_drag_drop/',
    allowedfiletypes: ['image/jpeg','image/png','image/gif', 'video/mp4'],   // filetypes allowed by Content-Type.  Empty array means no restrictions
    allowedfileextensions: ['.jpg','.jpeg','.png','.gif','.mp4'], // file extensions allowed. Empty array means no restrictions
    
    uploadFinished:function(i,file,response){
      $.data(file).addClass('done');

      // response is the JSON object That user controller returns
      // alert(response.status);
      // response is the JSON object That user controller returns
    },
    
      error: function(err, file) {
      switch(err) {
        case 'BrowserNotSupported':
          showMessage('Your browser does not support HTML5 file uploads!');
          break;
        case 'TooManyFiles':
          alert('Too many files! Please select 5 at most! (configurable)');
          break;
        case 'FileTooLarge':
          alert(file.name+' is too large! Please upload files up to 2mb (configurable).');
          break;
        default:
          break;
      }
    },
    
    // Called before each upload is started
    // beforeEach: function(file){
    //   if(!file.type.match(/^image\//)){
    //     alert('Only images are allowed!');
        
    //     // Returning false will cause the
    //     // file to be rejected
    //     return false;
    //   }
    // },
    
    uploadStarted:function(i, file, len){
      createImage(file);
    },
    
    progressUpdated: function(i, file, progress) {
      $.data(file).find('.progress').width(progress);
    }
       
  });
  
   var template = '<div class="preview">'+
                  '<span class="imageHolder">'+
                     '<img />'+
                     '<span class="uploaded"></span>'+
                  '</span>'+
                  '<div class="progressHolder">'+
                     '<div class="progress"></div>'+
                  '</div>'+
               '</div>'; 
  
  
  function createImage(file){

    var preview = $(template), 
      image = $('img', preview);
      
    var reader = new FileReader();
    
    image.width = 100;
    image.height = 100;
    
    reader.onload = function(e){
      
      // e.target.result holds the DataURL which
      // can be used as a source of the image:
      
      image.attr('src',e.target.result);
    };
    
    // Reading the file as a DataURL. When finished,
    // this will trigger the onload function above:
    reader.readAsDataURL(file);
    
    message.hide();
    preview.appendTo(dropbox);
    
    // Associating a preview container
    // with the file, using jQuery's $.data():
    
    $.data(file,preview);
  }

  function showMessage(msg){
    message.html(msg);
  }

});
</script>
<!-- Script.js file drag and drop Ends -->   
