 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>Send Email of Properties
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
             

              <div class="control-group">
                <label class="control-label">
                 Property Title
                </label>
                <div class="controls controls-row span6">
                  <input name="property_name" class="span12"  type="text" placeholder="Property Name" value="<?php echo ucfirst($property->title); ?>" readonly>
                  <span class="form_error span12"><?php echo form_error('booking_name'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Property Owner Email
                </label>
                <div class="controls controls-row span6">
                  <input name="email" class="span12" type="text" placeholder="Email" value="<?php echo $user->user_email; ?>" readonly>
                  <span class="form_error span12"><?php echo form_error('email'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Subject
                </label>
                <div class="controls controls-row span6">
                  <input name="subject" class="span12" type="text" placeholder="Subject" value="<?php echo set_value('subject'); ?>">
                  <span class="form_error span12"><?php echo form_error('subject'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Message
                </label>
                <div class="controls controls-row span6">
                  <textarea name="message" rows="5" style="width:548px;" ><?php echo set_value('message'); ?></textarea>
                  <span class="form_form_error span12"><?php echo form_error('message'); ?></span>
                </div>
              </div>
               <div class="control-group">
                <label class="control-label" for="your-name">
                 Attached file with email
                </label>
                <div class="controls controls-row span6">
                  <input type="file" name="userfile" >
                  <span class="form_error span12"></span>
                </div>
              </div>

              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
               
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>