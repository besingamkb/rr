 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Add Page</h4>
        </div><!-- page-header -->
        <?php alert() ?>
         <?php echo form_open_multipart(current_url()); ?>
          <div class="row-fluid">
          <div class="span8">             
                <div class="control-group">
                  <label class="control-label" for="">Page Title</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="page_title" value="<?php echo set_value('page_title') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('page_title') ?></span>
                </div>
                <div class="control-group">
                  <label class="control-label" for="">Slug</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="slug" value="<?php echo set_value('slug') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('slug') ?></span>
                </div>              
                <div class="control-group">
                  <label class="control-label" for="">Page Content</label>
                  <div class="controls">
                   <textarea class="span12 mceEditor" rows="10" name="content" ><?php echo set_value('content'); ?></textarea>
                  </div>
                 <span style="color:red"><?php echo form_error('content') ?></span>
                </div>         
                <div class="control-group">
                  <label class="control-label" for=""></label>
                  <div class="controls">
                    <input  class="btn btn-primary" type="submit" value="submit">
                  </div>
                  
                </div>
            </div> <!-- span8 -->

          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->