 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
        <div class="page-header">
            <h4>Add Blog / Video Category</h4>
        </div><!-- page-header -->
        <?php alert() ?>
         <?php echo form_open_multipart(current_url()); ?>
          <div class="row-fluid">
          <div class="span8">

                <div class="control-group">
                  <label class="control-label" for="">Type</label>
                  <div class="controls">
                   <select name="type">
                    <option value="2">Video Category</option>
                    <option value="1">Blog Category</option>
                   </select>
                  </div>
                 <span style="color:red"><?php echo form_error('type') ?></span>
                </div>
             
                <div class="control-group">
                  <label class="control-label" for="">Category Name</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="category" value="<?php echo set_value('category') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('category') ?></span>
                </div>
                
                <div class="control-group" id="thumb_div">
                  <label class="control-label" for="">Thumbnail <span style="font-size:10px;"> (220 x 160) in px </span> </label>
                  <div class="controls">
                    <input  class="span12" type="file" name="thumbnail">
                  </div>                 
                </div>
                
                <div class="control-group" id="image_div">
                  <label class="control-label" for="">Featured Image <span style="font-size:10px;"> (640 x 360) in px </span> </label>
                  <div class="controls">
                    <input  class="span12" type="file" name="featured_image">
                  </div>                 
                </div>
                
                <div class="control-group">
                  <label class="control-label" for="">Excerpt</label>
                  <div class="controls">
                    <textarea  class="span12" rows="5" name="excerpt"><?php echo set_value('excerpt'); ?></textarea>
                  </div>  
                  <span style="color:red"><?php echo form_error('excerpt') ?></span>               
                </div>
                <?php //print_r($video); ?>
                <div class="control-group" id="vid_link_div">
                  <label class="control-label" for="">Related Video Link</label>
                  <div class="controls">
                   <select name="rel_video">
                    <option value="">Please select</option>
                    <?php foreach ($video as $row): ?>                      
                    <option value="<?php echo $row->id ?>"><?php echo $row->post_title; ?></option>                   
                    <?php endforeach ?>
                   </select>
                  </div>
                 <span style="color:red"><?php echo form_error('rel_video') ?></span>
                </div>

                <div class="control-group">
                  <label class="control-label" for=""></label>
                  <div class="controls">
                    <input  class="btn btn-primary" type="submit" value="submit">
                  </div>
                  
                </div>
            </div> <!-- span8 -->

          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->
<script type="text/javascript">
  $(document).ready(function(){
    $("select[name=type]").change(function(){
      if( $("select[name=type]").val() == '1' ){
        $("#vid_link_div, #thumb_div, #image_div ").hide();
      }else{
        $("#vid_link_div, #thumb_div, #image_div ").show();
      }
    });
  });
</script>