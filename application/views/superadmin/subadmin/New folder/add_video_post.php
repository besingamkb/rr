
<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/admin/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">

  tinyMCE.init({
    mode : "textareas",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",   
    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'
     }); 
</script>
<!-- /TinyMCE -->
 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span2">
         <?php $this->load->view('admin/leftbar'); ?> 

      </div><!--/span2-->
      <div class="span10">
          <div class="page-header">
            <h4>Add New Post</h4>
          </div><!-- page-header -->
          <?php alert() ?>
          <div class="row-fluid">
              <div class="span8">
                <?php echo form_open_multipart(current_url()); ?>
                  <div class="control-group">
                    <label class="control-label" for="">Post Title</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="post_title" value="<?php echo set_value('post_title') ?>" placeholder="Enter title Here">
                    </div>
                    <?php echo form_error('post_title') ?>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="">Post Excerpt</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="post_excerpt" value="<?php echo set_value('post_excerpt') ?>" placeholder="Enter Excerpt Here">
                    </div>
                    <?php echo form_error('post_excerpt') ?>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="">Video Type</label>
                    <div class="controls">
                      <select id="video_type" name="video_type">
                        <option value="">Please select</option>
                        <option value="1">Youtube</option>
                        <option value="2">Vimeo</option>
                        <!-- <option value="3">Limelight</option> -->
                      </select>
                    </div>
                    <?php echo form_error('video_type') ?>
                  </div>
                  <div id="video_url" class="control-group">
                    <label class="control-label" for="">Video URL</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="video_url" placeholder="Enter Video Url Here eg-http://www.youtube.com/embed/X_P26tfRxfI?feature=player_detailpage">
                    </div>
                    <?php echo form_error('video_url') ?>
                  </div>
                  <div id="channel_id" class="control-group">
                    <label class="control-label" for="">Channel ID</label>
                    <div class="controls">
                      <input  class="span12" type="text" name="channel_id"  placeholder="Enter Channel ID Here">
                    </div>                    
                  </div>                 

                   <div class="control-group">
                    <label class="control-label" for="">Category</label>
                    <div class="controls">
                      <select name="post_category">
                        <option value="">Please select</option>
                        <?php foreach ($categories as $row): ?>                          
                        <option value="<?php echo $row->id ?>"><?php echo $row->category_name ?></option>                        
                        <?php endforeach ?>
                      </select>
                    </div>
                    <?php echo form_error('post_category') ?>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="">Post Content</label>                    
                    <div class="controls">
                       <textarea id="textarea" class="span12" rows="10"  name="post_content" placeholder="Enter Content Here"><?php echo set_value('post_content') ?></textarea>
                    </div>
                    <?php echo form_error('post_content') ?>
                  </div>
                  
                   <div class="control-group">
                    <label class="control-label" for="">Post Image <span style="font-size:10px;"> <!-- (800 x 530) in px --> </span> </label>
                    <div class="controls">
                      <input type="file" name="userfile">
                    </div>                 
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="">Post Status</label>                    
                    <div class="controls">
                       <select name="post_status">                        
                        <option value="1">Publish</option>
                        <option value="2">UnPublish</option>
                       </select>
                    </div>
                    <?php echo form_error('post_content') ?>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for=""></label>                   
                    <div class="controls">
                       <input type="submit" value="Add Post" class="btn btn-primary">
                    </div>                 
                  </div>                
               </div> <!--span8 -->
              <?php echo form_close(); ?>
          

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#post_date').datepicker({dateFormat:'yy-mm-dd',minDate: +1});
    
  });
  
 jQuery(document).on('click', '#image_preview .remove_img', function (e) {
    jQuery('#feature_image').val('');
    jQuery('#image_preview').html('');
    
});

 jQuery(document).ready(function() {
  jQuery('#video_url').hide();
  jQuery('#channel_id').hide();

   jQuery('#video_type').change(function(){
      var val = jQuery(this).val()
      // alert(val);

      if(val == ""){
        jQuery('#video_url').hide();
      jQuery('#channel_id').hide();
       return false;
      }else if(val == 1){
        jQuery('#video_url').show();
        jQuery('#channel_id').hide();
      }else if(val == 2){
        jQuery('#channel_id').hide();
        jQuery('#video_url').show();
      }else if(val == 3){
        jQuery('#video_url').hide();
        jQuery('#channel_id').show();
      }

      });
  });



/* var editor = new wysihtml5.Editor("textarea", {
     
      toolbar:  "wysihtml5-editor-toolbar",
    stylesheets:  "<?php echo base_url() ?>assets/xing/examples/css/stylesheet.css",
    parserRules:  wysihtml5ParserRules
  });*/
</script>