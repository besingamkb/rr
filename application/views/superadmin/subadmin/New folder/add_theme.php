 <div class="container-fluid">
  <div class="row-fluid">      
      <div class="span12">
        <div class="page-header">
            <h4>Add Theme</h4>
        </div><!-- page-header -->
        <?php alert() ?>
         <?php echo form_open_multipart(current_url()); ?>
          <div class="row-fluid">
          <div class="span12">               
             
                <div class="control-group">
                  <label class="control-label" for="">Theme name</label>
                  <div class="controls">
                    <input  class="span12" type="text" name="theme_name" value="<?php echo set_value('theme_name') ?>">
                  </div>
                 <span style="color:red"><?php echo form_error('theme_name') ?></span>
                </div>

                <div class="control-group">
                  <label class="control-label" for="">Thumbnail image</label>
                  <div class="controls">
                    <input class="span12" type="file" name="theme_image">
                  </div>
                  <span style="color:red"><?php echo form_error('theme_image') ?></span>
                </div>
                <?php  
                  if ($payment == 1)
                    $display = 'display:block';
                  else
                    $display = 'display:none';
                ?>
                <div class="control-group">
                  <label class="control-label" for="">Theme type</label>
                  <div class="controls">
                    <select name="payment">
                      <option <?php if($payment == 0){echo 'selected="selected"';} ?> value="0">Free</option>
                      <option <?php if($payment == 1){echo 'selected="selected"';} ?> value="1">Paid</option>
                    </select>
                  </div>
                </div>

                <div class="control-group" id="price" style="<?php echo $display; ?>"> 
                  <label class="control-label" for="">Price</label>
                  <div class="controls">
                    <input class="span12" type="text" name="price">
                  </div>
                  <span style="color:red"><?php echo form_error('price') ?></span>
                </div>

                <div class="control-group">
                  <label class="control-label" for="">Number of images</label>
                  <div class="controls">
                    <input  class="span12" type="text" id="no_of_images" name="no_of_images" value="<?php if(set_value('no_of_images')){ echo set_value('no_of_images'); }else{ echo '4';} ; ?>">
                  </div>
                 <span style="color:red" id="no_img_error"><?php echo form_error('no_of_images') ?></span>
                </div>

                <h4>Image Pack</h4>

                <div class="control-group">
                  <div class="controls">
                  Charging image -  <input class="span4" accept="image/*" type="file" name="charge_image[]">
                  Charging image sound file -  <input class="span4" accept="audio/*" type="file" name="charge_mp3[]">
                  </div>
                </div>

                <h4>Percent charging image</h4>
                <div id="image-mp3">
                    <div class="control-group">
                      <div class="controls">
                      Charging image -  <input class="span4" accept="image/*" type="file" name="charge_image[]">
                      Charging image sound file -  <input class="span4" accept="audio/*" type="file" name="charge_mp3[]">
                      </div>
                    </div>

                    <div class="control-group">
                      <div class="controls">
                      Charging image -  <input class="span4" accept="image/*" type="file" name="charge_image[]">
                      Charging image sound file -  <input class="span4" accept="audio/*" type="file" name="charge_mp3[]">
                      </div>
                    </div>

                    <div class="control-group">
                      <div class="controls">
                      Charging image -  <input class="span4" accept="image/*" type="file" name="charge_image[]">
                      Charging image sound file -  <input class="span4" accept="audio/*" type="file" name="charge_mp3[]">
                      </div>
                    </div>

                    <div class="control-group">
                      <div class="controls">
                      Charging image -  <input class="span4" accept="image/*" type="file" name="charge_image[]">
                      Charging image sound file -  <input class="span4" accept="audio/*" type="file" name="charge_mp3[]">
                      </div>
                    </div>
                </div>
               <div class="control-group">
                    <div class="controls row-fluid">
                      <span style="color:red" class="span5" ><?php echo form_error('charge_image') ?></span>
                      <span style="color:red" class="span5" ><?php echo form_error('charge_mp3') ?></span>
                    </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for=""></label>
                  <div class="controls">
                    <input  class="btn btn-primary" id="save" type="submit" value="Save">
                  </div>
                  
                </div>
            </div> <!-- span8 -->
          <input type="hidden" name="fake" value="1">
          <?php echo form_close(); ?>

      </div><!--/span10-->
  </div><!--/row-->    
</div><!--/.fluid-container-->

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery("#no_of_images").on('change', function() {

        var count = jQuery(this).val();
        var id = jQuery(this).attr('id');
        var DivToAdd = '';
        /* add options */
        if (count > 4)
        {
          for (var i = 0; i < count; i++) {
                  DivToAdd +=  '<div class="control-group">';
                  DivToAdd +=     '<div class="controls">';
                  DivToAdd +=       'Charging image -  <input class="span4" type="file" name="charge_image">';
                  DivToAdd +=       'Charging image sound file -  <input class="span4" type="file" name="charge_mp3">';
                  DivToAdd +=      '</div>';
                  DivToAdd +=  '</div>';
          };
          jQuery("#image-mp3").html(DivToAdd);
          jQuery("#no_img_error").html('');
        }else{
            jQuery(this).val('4');
            jQuery("#no_img_error").html('Minimum 4 images required.');
        }
    });

    $("#no_of_images").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });

    jQuery("#save").click(function() {
        var img = jQuery("#no_of_images").val();
        if(img < 4){
          jQuery("#no_img_error").html('Minimum 4 images required.');
          return false;
        }else{
          jQuery("#no_img_error").html('');
          return true;
        }
    });
});
</script>