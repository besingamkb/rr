 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Tax Setting
          </div>
         
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:20%">Tax</th>
                  <th style="width:30%">Tax Type</th>
                  <th style="width:30%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($taxs)): ?>
                    <?php $i=1; foreach ($taxs as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->tax; ?></td>
                        <td><?php echo $row->tax_type; ?> </td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/edit_tax/<?php echo $row->id;?>" id="" role="button" class="btn btn-small btn-primary hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">Edit</a>
                        </td>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <th colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>


                    </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>