<?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1));  ?>

<div class="row-fluid">
  <div class="span12">
    <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Settings
          </div>
        <?php if($subadmin_restrictions->setting!=1): ?>

          <div class="pull-right">
            <div class="btn-group">
              <a class="btn" href="javascript:void(0)">Social Media Buttons</a>
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>subadmin/change_media_buttons/facebook">Facebook</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_media_buttons/twitter">Twitter</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_media_buttons/google">Google plus</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_media_buttons/pinterest">Pinterest</a></li>
              </ul>
            </div>
            <div class="btn-group">
              <a class="btn" href="javascript:void(0)">Social Media Settings</a>
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>subadmin/change_secret_keys/facebook">Facebook</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_secret_keys/twitter">Twitter</a></li>
<!--                 <li><a href="<?php //echo base_url(); ?>subadmin/change_secret_keys/linkedin">Linkedin</a></li>
 -->                <li><a href="<?php echo base_url(); ?>subadmin/change_secret_keys/hotmail">Hotmail</a></li>
              </ul>
            </div>
            <div class="btn-group">
              <a class="btn" href="javascript:void(0)">Property Attributes</a>
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>subadmin/amenities">Amenities</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/room_types">Room Types</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/bed_types">Bed Types</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/property_types">Property Types</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/room_allotment">Room Allotments </a></li>
              </ul>
            </div>
            <div class="btn-group">
              <a class="btn" href="javascript:void(0)">Site Settings</a>
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>subadmin/update_content">Change Home page Content</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/company_details">Company Details</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_logo">Change Logo</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_buttons_colors">Change Button Color</a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/change_site_currency">Change Site Currency</a></li>
              </ul>
            </div>
            <div class="btn-group">
              <a class="btn" href="#">Commission Settings</a>
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>subadmin/commision_fee">Commision Settings </a></li>
                <li><a href="<?php echo base_url(); ?>subadmin/taxs">Tax Settings </a></li>

                <li><a href="<?php echo base_url(); ?>subadmin/set_invite_commission">Set Invites Commission</a></li>

<!--                 <li><a href="<?php //echo base_url(); ?>subadmin/traveler_service_fee">Traveler Service Fee</a></li>
                <li><a href="<?php //echo base_url(); ?>subadmin/property_listing_fee">Property Listing Fee </a></li>
 -->              
              </ul>
            </div>
            <?php endif; ?>

          </div>
        </div>
        <div class="widget-body">
                  <?php alert(); ?>

          <div class="row-fluid">
            <div class="span6" >
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Password Information
                  </div>
                </div>
                <div class="widget-body" >
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well', 'onsubmit'=>'return false;'));?>
                    <?php pass_alert(); ?>
                    <h5 class="text-info">Change Password </h5>
                    <hr>
                    <div class="control-group">
                      <label class="control-label">
                       Old Password
                      </label>
                      <div class="controls">
                        <input type="password" name="old_password" placeholder="Old Password">
                        <span class="form_error span12"><?php echo form_error('old_password'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                      New  Password
                      </label>
                       <div class="controls">
                       <input type="password" nme="new_password" placeholder="New Password">
                       <span class="form_error span12"><?php echo form_error('new_password'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                        Confirm Password
                      </label>
                      <div class="controls">
                       <input type="password" nae="con_password" placeholder="Confirm Password" >
                       <span class="form_error span12"><?php echo form_error('con_password'); ?></span>
                      </div>
                    </div>
                    <div class="form-actions no-margin">
                      <input type="submit" disabled="disabled"  nme="save_password" value="save" class="btn btn-info">
                    
                      
                    </div>
                  <?php echo form_close() ?>
                </div>
              </div>
              
        <?php if($subadmin_restrictions->setting!=1): ?>

              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Payment Gateway
                  </div>
                </div>


                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                <div class="widget-body" id="paypal_location">                  
                <!-- Paypal Starts  -->

                    <?php if($this->session->flashdata('success_paypal')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_paypal') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_paypal')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_paypal') ?>
                        </div>
                    <?php } ?>

                    <h5 class="text-info">Paypal Settings </h5>
                    <hr>
                   <?php $paypal_info = get_payment_gateways_info('paypal'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="paypal_email" value="<?php echo $paypal_info->email ?>" placeholder="Email"  >
                        <span class="form_error span12"><?php echo form_error('paypal_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="paypal_api_key" value="<?php echo $paypal_info->api_key ?>" placeholder="Api Key" >
                        <span class="form_error span12"><?php echo form_error('paypal_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="paypal_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- Paypal Ends  -->

                <!-- Braintree Starts  -->

                <div class="widget-body" id="braintree_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_braintree')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_braintree') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_braintree')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_braintree') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">BrainTree Settings </h5>
                    <hr>
                   <?php $braintree_info = get_payment_gateways_info('braintree'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="braintree_email" value="<?php if(!empty($braintree_info->email )) echo $braintree_info->email ?>" placeholder="Email">
                        <span class="form_error span12"><?php echo form_error('braintree_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="braintree_api_key" value="<?php if(!empty($braintree_info->api_key)) echo $braintree_info->api_key ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('braintree_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="braintree_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- Braintree Ends  -->


                <!-- stripe Starts  -->

                <div class="widget-body" id="stripe_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_stripe')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_stripe') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_stripe')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_stripe') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">Stripe Settings </h5>
                    <hr>
                   <?php $stripe_info = get_payment_gateways_info('stripe'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="stripe_email" value="<?php if(!empty($stripe_info->email )) echo $stripe_info->email ?>" placeholder="Email">
                        <span class="form_error span12"><?php echo form_error('stripe_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="stripe_api_key" value="<?php if(!empty($stripe_info->api_key)) echo $stripe_info->api_key ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('stripe_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="stripe_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- Stripe Ends  -->


                <!-- 2checkout Starts  -->

                <div class="widget-body" id="2checkout_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_2checkout')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_2checkout') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_2checkout')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_2checkout') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">2Checkout Settings </h5>
                    <hr>
                   <?php $_2checkout_info = get_payment_gateways_info('2checkout'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="2checkout_email" value="<?php if(!empty($_2checkout_info->email )) echo $_2checkout_info->email ?>" placeholder="Email">
                        <span class="form_error span12"><?php echo form_error('2checkout_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="2checkout_api_key" value="<?php if(!empty($_2checkout_info->api_key)) echo $_2checkout_info->api_key ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('2checkout_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="2checkout_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- 2checkout Ends  -->


                <!-- payza Starts  -->

                <div class="widget-body" id="payza_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_payza')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_payza') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_payza')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_payza') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">Payza Settings </h5>
                    <hr>
                   <?php $payza_info = get_payment_gateways_info('payza'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="payza_email" value="<?php if(!empty($payza_info->email )) echo $payza_info->email ?>" placeholder="Email">
                        <span class="form_error span12"><?php echo form_error('payza_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="payza_api_key" value="<?php if(!empty($payza_info->api_key)) echo $payza_info->api_key ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('payza_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="payza_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- payza Ends  -->

                <!-- skrill Starts  -->

                <div class="widget-body" id="skrill_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_skrill')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_skrill') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_skrill')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_skrill') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">Skrill Settings </h5>
                    <hr>
                   <?php $skrill_info = get_payment_gateways_info('skrill'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="skrill_email" value="<?php if(!empty($skrill_info->email )) echo $skrill_info->email ?>" placeholder="Email">
                        <span class="form_error span12"><?php echo form_error('skrill_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="skrill_api_key" value="<?php if(!empty($skrill_info->api_key)) echo $skrill_info->api_key ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('skrill_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="skrill_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- skrill Ends  -->


                <!-- dwolla Starts  -->

                <div class="widget-body" id="dwolla_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_dwolla')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_dwolla') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_dwolla')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_dwolla') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">Dwolla Settings </h5>
                    <hr>
                   <?php $dwolla_info = get_payment_gateways_info('dwolla'); ?>
                    <div class="control-group">
                      <label class="control-label">
                       Email
                      </label>
                      <div class="controls">
                        <input type="text" name="dwolla_email" value="<?php if(!empty($dwolla_info->email )) echo $dwolla_info->email ?>" placeholder="Email">
                        <span class="form_error span12"><?php echo form_error('dwolla_email'); ?></span>
                      </div>                      
                    </div>  

                    <div class="control-group">
                      <label class="control-label">
                        Api key
                      </label>
                      <div class="controls">
                        <input type="text" name="dwolla_api_key" value="<?php if(!empty($dwolla_info->api_key)) echo $dwolla_info->api_key ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('dwolla_api_key'); ?></span>
                      </div>                                
                    </div>      
                    <div class="form-actions no-margin">
                      <input type="submit" name="dwolla_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>

                <!-- dwolla Ends  -->

              </div>
            </div>

          <?php endif; ?>

            <div class="span6" style="float:left">

               <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"> Profile </span> 
                  </div>
                </div>
                <div class="widget-body">
                <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                  <?php profile_alert(); ?>
                    <h5 class="text-info">Personal Information</h5>
                    <hr>
                    <div class="control-group">
                      <label class="control-label">
                        First Name
                      </label>
                      <div class="controls">
                       <input type="text" name="first_name" value="<?php echo $user->first_name; ?>" placeholder="First Name" >
                        <span class="form_error span12"><?php echo form_error('first_name'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                        Last Name
                      </label>
                      <div class="controls">
                       <input type="text" name="last_name" value="<?php echo $user->last_name; ?>" placeholder="Last Name" >
                      <span class="form_error span12"><?php echo form_error('last_name'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                       Address
                      </label>
                      <div class="controls">
                       <input type="text" name="address" value="<?php echo $user->address;  ?>" placeholder="Address" >
                      <span class="form_error span12"><?php echo form_error('address'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                       User Email
                      </label>
                      <div class="controls">
                        <input type="text" readonly name="user_email" value="<?php echo $user->user_email;?>" placeholder="User Email" >
                     <span class="form_error span12"><?php echo form_error('user_email'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                        Phone
                      </label>
                      <div class="controls">
                       <input type="text" name="phone" value="<?php echo $user->phone;?>" placeholder="Phone" >
                      <span class="form_error span12"><?php echo form_error('phone'); ?></span>
                      </div>
                    </div>
                    
                    <div class="control-group">
                      <label class="control-label">
                        City
                      </label>
                      <div class="controls">
                        <input type="text" name="city" value="<?php echo $user->city;?>" placeholder="City" >
                      <span class="form_error span12"><?php echo form_error('city'); ?></span>
                      </div>
                    </div>
                    <div class="form-actions no-margin">
                      <input type="submit" name="save_profile_info" value=" Save" class="btn btn-info">
                    </div>
                 <?php echo form_close() ?>
                </div>
              </div>

        <?php if($subadmin_restrictions->setting!=1): ?>


              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Mail Chimp 
                  </div>
                </div>
                <div class="widget-body">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <h5 class="text-info">Api Settings </h5>
                    <hr>
                   
                    <div class="control-group">
                      <label class="control-label">
                       Api Key
                      </label>
                      <div class="controls">
                        <input type="text" name="apikey" value="<?php if(!empty($mailchimp->apikey)) echo $mailchimp->apikey ; ?>" placeholder="Api Key">
                        <span class="form_error span12"><?php echo form_error('apikey'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                      List Id
                      </label>
                       <div class="controls">
                       <input type="text" name="listid" value="<?php if(!empty($mailchimp->listid)) echo $mailchimp->listid ; ?>" placeholder="List Id">
                       <span class="form_error span12"><?php echo form_error('listid'); ?></span>
                      </div>
                    </div>                    
                    <div class="form-actions no-margin">
                      <input type="submit" name="mailchimp" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>
              </div> 


              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Twillio 
                  </div>
                </div>
                <div class="widget-body" id="twillio_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php if($this->session->flashdata('success_twillio')){ ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_twillio') ?>
                        </div>
                    <?php }elseif($this->session->flashdata('error_twillio')){ ?>
                        <div class="alert alert-error">
                          <strong>Error!</strong> <?php echo $this->session->flashdata('error_twillio') ?>
                        </div>
                    <?php } ?>
                    <h5 class="text-info">Api Settings </h5>
                    <hr>

                    <?php $twillio = get_twillio_info() ?>
                   
                    <div class="control-group">
                      <label class="control-label">
                       Auth token
                      </label>
                      <div class="controls">
                        <input type="text" name="twillio_authtoken" value="<?php if(!empty($twillio->auth_token)) echo $twillio->auth_token ?>" placeholder="Twillio Auth Token">
                        <span class="form_error span12"><?php echo form_error('twillio_authtoken'); ?></span>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">
                      Account Id
                      </label>
                       <div class="controls">
                       <input type="text" name="twillio_account_id" value="<?php if(!empty($twillio->account_id)) echo $twillio->account_id ?>" placeholder="Account Id">
                       <span class="form_error span12"><?php echo form_error('twillio_account_id'); ?></span>
                      </div>
                    </div>


                    <div class="control-group">
                      <label class="control-label">
                      Your Number
                      </label>
                       <div class="controls">
                       <input type="text" name="twillio_number" value="<?php if(!empty($twillio->your_number)) echo $twillio->your_number ?>" placeholder="Number">
                       <span class="form_error span12"><?php echo form_error('twillio_number'); ?></span>
                      </div>
                    </div>                    
                    <div class="form-actions no-margin">
                      <input type="submit" name="twillio_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>
              </div>


              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Jumio 
                  </div>
                </div>
                <div class="widget-body" id="jumio_location">
                  <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                    <?php echo form_open(current_url(),array('class' => 'form-horizontal no-margin well'));?>
                      <?php if($this->session->flashdata('success_jumio')){ ?>
                       <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success_jumio') ?>
                      </div>
                        <?php }elseif($this->session->flashdata('error_jumio')){ ?>
                      <div class="alert alert-error">
                           <strong>Error!</strong> <?php echo $this->session->flashdata('error_jumio') ?>
                      </div>
                    <?php } ?>
                    <h5 class="text-info">Api Settings </h5>
                    <hr>

                  <?php $jumio = get_jumio_info() ?>
                   
                    <div class="control-group">
                      <label class="control-label">
                       Auth token
                      </label>
                      <div class="controls">
                        <input type="text" name="jumio_authtoken" value="<?php if(!empty($jumio->auth_token)) echo $jumio->auth_token ?>" placeholder="Jumio Authtoken">
                        <span class="form_error span12"><?php echo form_error('jumio_authtoken'); ?></span>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">
                      Api key
                      </label>
                       <div class="controls">
                       <input type="text" name="jumio_key" value="<?php if(!empty($jumio->api_key)) echo $jumio->api_key ?>" placeholder="Jumio Key">
                       <span class="form_error span12"><?php echo form_error('jumio_key'); ?></span>
                      </div>
                    </div>


                    <div class="control-group">
                      <label class="control-label">
                      Api secret
                      </label>
                       <div class="controls">
                       <input type="text" name="jumio_secret" value="<?php if(!empty($jumio->api_secret)) echo $jumio->api_secret ?>" placeholder="Jumio Secret">
                       <span class="form_error span12"><?php echo form_error('jumio_secret'); ?></span>
                      </div>
                    </div>                    
                    <div class="form-actions no-margin">
                      <input type="submit" name="jumio_btn_unique" value="save" class="btn btn-info">
                    </div>
                  <?php echo form_close() ?>
                </div>
              </div>           

            <?php endif; ?>      

            </div>   

          </div>
            <div class="row-fluid">
            <div class="span6" style="float:left">



            </div>

             <div class="span6" style="float:left">
            </div>

          </div>
          
            
           
        </div>
    </div>    
  </div>
</div>