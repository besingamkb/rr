  <?php $subadmin_restrictions = get_row('subadmin_restrictions',array('id'=>1)); ?>

<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Help
          </div>
        <?php if($subadmin_restrictions->help!=1):  ?>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>subadmin/help_category">Help Category </a>
            <a class="btn" href="<?php echo base_url(); ?>subadmin/add_help"> Add Help </a>
          </div>
         <?php endif; ?>
         </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:20%">Question</th>
                  <th style="width:20%">Title</th>
                  <th style="width:20%">Description</th>
                  <th style="width:15%">Category</th>
        <?php if($subadmin_restrictions->help!=1):  ?>
                  <th style="width:15%">Actions</th>
               <?php endif; ?>
                 </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($faqs)): ?>
                    <?php $i=1; foreach ($faqs as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo word_limiter($row->question,5); ?></td>
                        <td><?php echo word_limiter($row->title,5); ?></td>
                        <td><?php echo word_limiter($row->description,5); ?></td>
                       <?php $category = get_faqs_category($row->faq_cat_id) ?>
                        <td><?php echo $category->title ?></td>

        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>
        <?php if($subadmin_restrictions->help!=1):  ?>
                        <td>
                          <a href="<?php echo base_url()?>subadmin/delete_faq/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>subadmin/edit_help/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                        </td>
          <?php endif; ?>
          <?php endif; ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <th colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                    </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>