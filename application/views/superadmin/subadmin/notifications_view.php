<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin" >
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                    Notification
                </div>
            </div>
        </div>
        <?php if(!empty($notifications)): ?>
        <table align="left" style="border:5px #F2F2F2 solid; border-top:none; width:700px;  margin-top:20px;" class="table table-condensed table-striped">    
            <thead>
                <tr>
                    <th>
                        <span style="font-size:20px; color:#D33F3E;" class="fs1" data-icon="" aria-hidden="true">
                            Notification Detaials
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div  style="color:black; font-size:14px; font-weight:bold">
                            Message:
                        </div>
                        <div style="margin-left:75px">
                            <?php echo $notifications->message; ?>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="color:black; font-size:14px; font-weight:bold ">
                            Subject:
                        </div>
                        <div style="margin-left:75px">
                            <?php echo $notifications->subject; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="pull-right"> 
                            Sent To&nbsp;<?php echo ucwords($notifications->email); ?>
                            (<span style="color:#D33F3E"><?php echo ucwords($notifications->name); ?></span>)
                            &nbsp; on &nbsp;<?php echo get_time_ago($notifications->created); ?> 
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>