 
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Amenities
          </div>
          <div class="pull-right">

            <a class="btn" href="<?php echo base_url(); ?>superadmin/add_amenity"> Add Amenity </a>
             <div class="btn-group">
                  <a class="btn" href="#">Property Attributes</a>
                  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url(); ?>superadmin/amenities">Amenities</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/room_types">Room Types</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/bed_types">Bed Type</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/property_types">property Type</a></li>
                    <li><a href="<?php echo base_url(); ?>superadmin/room_allotment">Room Allotment </a></li>
                  </ul>
                </div>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  
                  <th style="width:30%">Amenity Name</th>
                  <th style="width:30%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($amenities)): ?>
                    <?php $i=1; foreach ($amenities as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/delete_amenity/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>superadmin/edit_amenity/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                        </td>
                <?php endif; ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix">
              <?php echo $pagination ?>
            </div>
          </div>
        </div>
      </div>
    </div>