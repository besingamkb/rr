
   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
<?php 
          alert();
 ?>
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Help Category
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                  <label class="control-label" for="question">
                    Title
                  </label>
                <div class="controls controls-row span6">
                  <input name="title" class="span12" type="text" placeholder="Title" value="<?php echo set_value('title'); ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="description" rows="5" style="width:97.50%;" ><?php echo set_value('description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description');  ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Status
                </label>
                <div class="controls controls-row span6">
                <select name="status">
                  <option value="0">Unpublish</option>
                  <option value="1">publish</option>
                </select>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>