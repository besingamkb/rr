
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Member Notes
          </div>
          <div class="pull-right">
            <a href="<?php echo base_url();?>superadmin/add_member_note/<?php if(!empty($id)) echo $id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a>
            <a href="<?php echo base_url()?>superadmin/members" class="btn btn-warning btn-small">Back To members</a>
          </div>          
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:30%">Title</th>
                  <th style="width:30%">Note</th>
                  <th style="width:15%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($notes)): ?>
                    <?php $i=1; foreach ($notes as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->title; ?></td>
                        <td><?php echo word_limiter($row->description,4); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/view_member_note/<?php echo $row->id;?>" id="" role="button" class="btn btn-small btn-success  hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">view</a>
                          <a href="<?php echo base_url()?>superadmin/delete_member_note/<?php echo $row->id;?>/<?php echo $row->member_id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-primary btn-small hidden-phone" data-original-title="">Delete</a>
                        </td>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>