 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Column Chart
          </div>
        </div>
        <div class="widget-body">
          <div id="column_chart"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> View Property Email Records
            </div>
          </div>
          <div class="widget-body">
           <div class="form-horizontal no-margin well">

              <div class="control-group">
                <label class="control-label">
                 Property Name
                </label>
                <div class="controls controls-row span6">
                          <?php $title = get_property_title($property_record->property_id); ?>
                  <span class=""><?php  echo $title;   ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                 User email
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php  echo $property_record->owner_email;  ?></span>
                </div>
              </div>

             

              <div class="control-group">
                <label class="control-label" >
                   Subject
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php  echo $property_record->subject;   ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                   Message
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php echo $property_record->message;    ?></span>
                </div>
              </div>

              <?php if(!empty($property_record->attachment)): ?>
              <div class="control-group">
                <label class="control-label">
                 Property attachment
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php echo $property_record->attachment;    ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="<?php echo base_url()?>superadmin/check_down/<?php echo $property_record->attachment;?>" class="btn btn-info btn-small">click to download</a>
                </div>
              </div>
             <?php endif; ?>
              
            <div class="form-actions no-margin">
                <!-- <button type="submit" class="btn btn-info">
                  Save
                </button> -->
                <a href="<?php echo base_url()?>superadmin/property_email_records"  class="btn">
                 Go Back
                </a>
              </div>

            
              </div>
            </div>
            

          </div>
        </div>
      </div>
    </div>
  </div>