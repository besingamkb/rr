 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span> Column Chart
          </div>
        </div>
        <div class="widget-body">
          <div id="column_chart"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> View Booking Email Records
            </div>
          </div>
          <div class="widget-body">
           <div class="form-horizontal no-margin well">
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Booking id
                </label>
                <div class="controls controls-row span6">
                 <span><?php echo $book_record->booking_id; ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                 Booking address
                </label>
                <div class="controls controls-row span6">
                  
                  <span class=""><?php  echo $book_record->address;   ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Booking Email
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php  echo $book_record->email;  ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Booking Contact No.
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php echo $book_record->contact;   ?></span>
                </div>
              </div>

             

              <div class="control-group">
                <label class="control-label" >
                  Booking Subject
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php  echo $book_record->subject;   ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                Booking Message
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php echo $book_record->message;    ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                 Booking attachment
                </label>
                <div class="controls controls-row span6">
                  <span class=""><?php echo $book_record->attachment;    ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="<?php echo base_url()?>superadmin/check_down/<?php echo $book_record->attachment;?>" class="btn btn-info btn-small">click to download</a>
                </div>
              </div>

              
            <div class="form-actions no-margin">
                <!-- <button type="submit" class="btn btn-info">
                  Save
                </button> -->
                <a href="<?php echo base_url()?>superadmin/booking_email_records"  class="btn">
                 Go Back
                </a>
              </div>

            
              </div>
            </div>
            

          </div>
        </div>
      </div>
    </div>
  </div>