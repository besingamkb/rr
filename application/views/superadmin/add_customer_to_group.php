   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Customer To The Group
            </div>
          </div>
          <div class="widget-body">

            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <?php $users = get_customers(); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Customer Name
                </label>
                <div class="controls controls-row span6">
                  <select onchange="return get_users_info();" id="user_email" name="user_email" class="span10 input-left-top-margins">
                      <option value="">Select Customer</option>
                      <?php if(!empty($users)): ?>
                      <?php foreach ($users as $row): ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->first_name.' '.$row->last_name; ?></option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select>
                  <span class="form_error span12"><?php echo form_error('user_email'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label">
                  Customer Email
                </label>
                <div class="controls controls-row span6">
                  <input name="user_name" class="span12" type="text" placeholder="Email" id="owner_name" readonly>
                  <span class="form_error span12"><?php echo form_error('user_name'); ?></span>
                </div>
              </div>

              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
               <!--  <button onclick="goBack()" type="button" class="btn">
                  Cancel
                </button> -->
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>


  <script>
   function get_users_info(){

    var owner_id = $('#user_email').val();
        $.ajax({
          url: '<?php echo base_url() ?>/superadmin/owner_information',
          type: 'post',
          data: {user_id:owner_id},
          success:function(res){
            if(res!=""){
              $('#owner_name').val(res);
            }
          }
        })
  }

  </script>