   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Member
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  First Name
                </label>
                <div class="controls controls-row span6">
                  <input name="firstname" class="span12" type="text" placeholder="First Name" value="<?php echo set_value('firstname'); ?>">
                  <span class="form_error span12"><?php echo form_error('firstname'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  last Name
                </label>
                <div class="controls controls-row span6">
                  <input name="lastname" class="span12" type="text" placeholder="Last Name" value="<?php echo set_value('lastname'); ?>">
                  <span class="form_error span12"><?php echo form_error('lastname'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Email
                </label>
                <div class="controls controls-row span6">
                  <input name="email" class="span12" type="text" placeholder="Email" value="<?php echo set_value('email'); ?>">
                  <span class="form_error span12"><?php echo form_error('email'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  User Type
                </label>
                <div class="controls controls-row span6">
                  <select name="user_role" class="span12 input-left-top-margins">
                      <option value="" > Select user type </option>
                      <option value="subadmin" <?php if(set_value('user_role') == 'subadmin' ) echo "selected"; ?> > Sub-admin </option>
                      <option value="user" <?php if(set_value('user_role') == 'user' ) echo "selected"; ?> > User </option>
                  </select>
                  <span class="form_error span12"><?php echo form_error('user_role'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="your-name">
                  Contact No.
                </label>
                <div class="controls controls-row span6">
                  <input name="phone" class="span12" type="text" placeholder="Contact Number" value="<?php echo set_value('phone'); ?>">
                  <span class="form_error span12"><?php echo form_error('phone'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Address
                </label>
                <div class="controls controls-row span6">
                  <input name="address" class="span12" type="text" placeholder="Address" value="<?php echo set_value('address'); ?>">
                  <span class="form_form_error span12"><?php echo form_error('address'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" >
                  Zipcode
                </label>
                <div class="controls controls-row span6">
                  <input name="zip" class="span12" type="text" placeholder="Zipcode" value="<?php echo set_value('zip'); ?>">
                  <span class="form_error span12"><?php echo form_error('zip'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  City 
                </label>
                <div class="controls controls-row span6">
                  <input name="city" class="span12" type="text" placeholder="City" value="<?php echo set_value('city'); ?>">
                  <span class="form_error span12"><?php echo form_error('city'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  State
                </label>
                <div class="controls controls-row span6">
                  <input name="state" class="span12" type="text" placeholder="State" value="<?php echo set_value('state'); ?>">
                  <span class="form_error span12"><?php echo form_error('state'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Country
                </label>
                <?php $country = get_country_array(); ?>
                <div class="controls controls-row span6">
                  <select name="country" class="span12 input-left-top-margins">
                      <option value="">Select country</option>
                      <?php foreach ($country as $code => $name): ?>
                      <option value="<?php echo $code; ?>"> <?php echo $name; ?> </option>
                      <?php endforeach ?>
                  </select>
                  <span class="form_error span12"><?php echo form_error('country'); ?></span>
                </div>
              </div>


            <div class="control-group">
                <label class="control-label">
                 Profile Image
                </label>
                <div class="controls controls-row span4">
                  <input  type="file" name="userfile" class="span12" required="required">
                  <span class="form_error span12"><?php echo form_error('userfile'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
               <!--  <button onclick="goBack()" type="button" class="btn">
                  Cancel
                </button> -->
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>