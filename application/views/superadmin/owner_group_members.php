
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Property Owners
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>superadmin/add_owner_to_group/<?php echo $group_id ?>"> Add Property Owner in The Group </a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:13%">Image</th>
                  <th style="width:20%">Owner Name</th>
                  <th style="width:20%">Owner Email</th>
                  <th style="width:20%">Created</th>
                  <th style="width:20%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($members)): ?>
                    <?php $i=1; foreach ($members as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><img style="width:80%;" src="<?php echo base_url()?>assets/uploads/profile_image/<?php if(!empty($row->image)) echo $row->image; else echo "52c6dc7f420ab.jpg"; ?>"></td>
                        <td><?php echo $row->first_name." ".$row->last_name; ?></td>
                        <td><?php echo $row->user_email ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)) ?></td>
                  <?php $colors = get_buttons_color();  ?>
                  <?php if(!empty($colors)): ?>

                        <td>
                          <a href="<?php echo base_url()?>superadmin/view_owner_profile/<?php echo $row->member_id;?>/<?php echo $group_id ?>"  class="<?php echo $colors->edit_btn ?>" data-original-title="">View</a>
                          <a href="<?php echo base_url()?>superadmin/delete_owner_member/<?php echo $row->id;?>/<?php echo $row->group_id ?>" onclick="return confirm('Do you want to delete?' );" role="button" class="<?php echo $colors->delete_btn ?>" data-toggle="modal" data-original-title="">Delete</a>
                        </td>

                      <?php endif; ?>
                     </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"><?php echo $pagination ?></div>
          </div>
        </div>
      </div>
    </div>