<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span> Change Secret Keys
</div>
</div>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>


<div class="control-group">
    <label class="control-label" for="your-name">
        Author
    </label>
    <div class="controls controls-row span6">
        <input name="phone" class="span12" id="phone" type="text" placeholder="Contact Number" value="<?php echo ucfirst($social_media_keys->name)  ?>" readonly>
    </div>
        <span class="form_error span12"><?php //echo form_error('app_id'); ?></span>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
       App ID
    </label>
    <div class="controls controls-row span6">
        <input name="app_id" class="span12" type="text" placeholder="App ID" value="<?php echo $social_media_keys->app_id ?>" >
        <span class="form_error span12"><?php echo form_error('app_id'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        App Secret Key
    </label>
    <div class="controls controls-row span6">
        <input name="app_secret_key" value="<?php echo $social_media_keys->app_secret_key ?>" class="span12" type="text" placeholder="App Secret Key">
        <span class="form_form_error span12"><?php  echo form_error('app_secret_key'); ?></span>
    </div>
</div>

<?php if($social_media_keys->name=="facebook"): ?>
<div class="control-group">
    <label class="control-label" for="your-name">
        Facebook page id
    </label>
    <div class="controls controls-row span6">
        <input name="page_id_or_user_name" value="<?php if(!empty($social_media_keys->page_id_or_user_name)) echo $social_media_keys->page_id_or_user_name ?>" class="span12" type="text" placeholder="Page Id">
        <span class="form_form_error span12"><?php  echo form_error('page_id_or_user_name'); ?></span>
    </div>
</div>
<?php endif; ?>

<?php if($social_media_keys->name=="twitter"): ?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Access token
    </label>
    <div class="controls controls-row span6">
        <input name="twitter_access_token" value="<?php echo $social_media_keys->twitter_access_token ?>" class="span12" type="text" placeholder="Access token">
        <span class="form_form_error span12"><?php  echo form_error('twitter_access_token'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Access token secret
    </label>
    <div class="controls controls-row span6">
        <input name="twitter_access_token_secret" value="<?php echo $social_media_keys->twitter_access_token_secret ?>" class="span12" type="text" placeholder="Access token Secret">
        <span class="form_form_error span12"><?php  echo form_error('twitter_access_token_secret'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Twitter user name
    </label>
    <div class="controls controls-row span6">
        <input name="page_id_or_user_name" value="<?php if(!empty($social_media_keys->page_id_or_user_name)) echo $social_media_keys->page_id_or_user_name ?>" class="span12" type="text" placeholder="Twitter Username">
        <span class="form_form_error span12"><?php  echo form_error('page_id_or_user_name'); ?></span>
    </div>
</div>
<?php endif; ?>






<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>


