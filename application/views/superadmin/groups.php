
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Groups
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>superadmin/add_group"> Add Groups </a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:14%">Image</th>
                  <th style="width:30%">Group Name</th>
                  <th style="width:30%">View</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($groups)): ?>
                    <?php $i=1; foreach ($groups as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><img style="width:100%;height:110px;border-radius:5px;border:1px solid #F7F7F7" src="<?php echo base_url()?>assets/uploads/banner/<?php echo $row->banner; ?>"></td>
                        <td><?php echo $row->group_name; ?></td>
                        <td>
                          <a href="<?php echo base_url()?>superadmin/owner_group_members/<?php echo $row->id;?>"  class="btn btn-default btn-small hidden-phone" data-original-title="">View Members (<span style="color:red"><?php echo get_no_of_members($row->id) ?></span>)</a>
                          <a href="<?php echo base_url()?>superadmin/group_members_properties/<?php echo $row->id;?>"  class="btn btn-default btn-small hidden-phone" data-original-title="">View Properties(<span style="color:red"><?php echo get_no_of_properties_from_group($row->id) ?></span>)</a>
                        </td>

                  <?php $colors = get_buttons_color();  ?>
                  <?php if(!empty($colors)): ?>

                        <td>
                          <a href="<?php echo base_url()?>superadmin/delete_group/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>superadmin/edit_group/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                        </td>

                      <?php endif; ?>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>