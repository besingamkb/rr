<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Transaction Details
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php //$pro_info = get_property_detail($payment->property_id); 
              //$user = get_user_info($pro_info->user_id);  ?>
            <div class="form-horizontal no-margin well" >
              <div class="control-group">
                <label class="control-label">
                 Payment type
                </label>
                <div class="controls controls-row span6">
                  <span class="form_error span12">
                   <strong>
                    
                    <?php if($payment->payment_type == 1){ echo 'booking'; }elseif($payment->payment_type == 2){ echo "Referral"; } ?>
                 </strong>
                  </span>
                </div>
              </div>



              <div class="control-group">
                <label class="control-label" for="your-name">
                Paid by
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php $userinfo = get_user_info($payment->paybyuser); echo $userinfo->first_name.' '.$userinfo->last_name; ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Payment Reciever
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php $userinfo = get_user_info($payment->recievebyuser); echo $userinfo->first_name.' '.$userinfo->last_name; ?></span>
                </div>
              </div>


               <div class="control-group">
                <label class="control-label" for="your-name">
                Amount
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $payment->amount ?></span>
                </div>
              </div>           

              <div class="control-group">
                <label class="control-label" for="your-name">
                 Booking Date
                </label>
                <div class="controls controls-row span6">                
                  <span class="form_form_error span12"><?php echo date('m/d/Y', strtotime($payment->created)); ?></span>
                </div>
              </div> 


              <div class="control-group">
                <label class="control-label" for="your-name">
                Payment Source
                </label>
                <div class="controls controls-row span6">
                
                  <span class="form_form_error span12"><?php echo $payment->paywith ?></span>
                </div>
              </div>           

                    

              <div class="form-actions no-margin">
                <a href="<?php echo base_url();?>superadmin/transaction_history" class="btn btn-info">
                  Back To Transaction History
                </a>
               
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
    .controls span.span12{
      padding-top: 6px;
    }

  </style>