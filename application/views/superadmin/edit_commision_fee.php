  
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>Commision Setting  <span style="font-size:10px;">(In percentage)</span>
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Commision 
                </label>
                <div class="controls controls-row span5">
                  <input name="commision_fee" class="span12" type="text" placeholder="Commision" value="<?php echo $commision_fee->commision_fee; ?>">
                  <span class="form_error span12"><?php echo form_error('commision_fee'); ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>