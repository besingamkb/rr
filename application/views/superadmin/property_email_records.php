
          <?php if($this->session->flashdata('success_msg')): ?>
            <div class="alert alert-success">
              <?php echo $this->session->flashdata('success_msg');?>
            </div>
          <?php endif ?>
        
          <div class="row-fluid">
            <div class="span12">
              <div class="widget no-margin">
                <div class="widget-header">

                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Property Email Record
                  </div>
                  <div class="pull-right">
                      <a class="btn" href="<?php echo base_url(); ?>superadmin/properties"> Back to properties</a>  
                  </div>
                </div>
                <div class="widget-body">

                  <div id="dt_example" class="example_alt_pagination">
                    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
                      <thead>
                        <tr>
                          <th style="width:6%;">#</th>
                          <th style="width:10%;">Property Name</th>
                          <th style="width:10%;">user Email</th>
                          <th style="width:15%;">Subject</th>
                          <th style="width:25%">Message</th>
                          <th style="width:5%;">Created</th>
                          <th style="width:10%;" class="hidden-phone">Actions</th>
                        </tr>
                      </thead>
                      <tbody>

                       <?php if(!empty($history)):?> 
                       <?php foreach ($history as $row):?>                      
                        <tr class="gradeA">
                          <td><?php echo ++$offset ;?></td>
                          <?php $title = get_property_title($row->property_id); ?>
                          <td><?php echo $title;?></td>
                          <td><?php echo $row->owner_email;?></td>
                          <td><?php echo $row->subject;?></td>
                          <td><?php echo $row->message;?></td>
                          <td><?php echo date('d-m-Y',strtotime($row->created));?></td>
                          <td class="hidden-phone">
                            <a href="<?php echo base_url()?>superadmin/view_property_email/<?php echo $row->id; ?>" role="button" class="btn btn-success btn-small hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">
                              view
                            </a>
                            <a href="<?php echo base_url()?>superadmin/delete_property_email/<?php echo $row->id; ?>" onclick="return confirm('Are you want to delete?');" class="btn btn-small btn-primary hidden-tablet hidden-phone">
                              delete
                            </a>
                          </td>
                        </tr>
                     <?php endforeach; ?>
                      <?php else: ?>
                      <tr><td colspan="8"> No Record Found</td></tr>
                     <?php  endif; ?>
                    </tbody>
                    </table>
                      <div id="data-table_info" class="dataTables_info">Showing 1 to 10 of 10 entries</div>
                        <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">
                           <?php if($pagination) echo $pagination; ?>
                        </div>
                      </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>

     