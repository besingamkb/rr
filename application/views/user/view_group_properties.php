<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Group Properties</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <input type="hidden" id="group_id" value="<?php echo $group_id ?>">
                                   <div class="pull-right">
                                        <a class="btn" href="<?php echo base_url() ?>user/groups" >Back to Groups</a>  
                                   </div>
                                   <div class="pull-right">
                                        <a class="btn" href="javascript:void(0)" id="check_membership"> Add Property </a>  
                                   </div>
                                   <div class="pull-left">
                                        <a class="btn" href="<?php echo base_url(); ?>user/owner_added_property/<?php echo $group_id ?>"> See Your Added Properties</a>  
                                   </div>
                              </div><br><br>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:4%">#</th>
                                                       <th style="width:10%">Image</th>
                                                       <th style="width:10%">Property Title</th>
                                                       <th style="width:16%">Property Description</th>
                                                       <th style="width:10%">Owner name</th>
                                                       <th style="width:15%; text-align:center;" >Added On</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($members_properties)): ?>
                                             <?php $i=1; foreach ($members_properties as $row):?>
                                                  <tr>
                                                       <td><?php echo $i++ ?></td>
                                                       <td><img style="width:80px; height:80px; border-radius:4px" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                                                       <td><a href="<?php echo base_url() ?>properties/details/<?php echo $row->id ?>"><?php echo word_limiter($row->title,4); ?></a></td>
                                                       <td><a href="<?php echo base_url() ?>properties/details/<?php echo $row->id ?>"><?php echo word_limiter($row->description,8); ?></a></td>
                                                       <td><?php echo $row->first_name." ".$row->last_name ?></td>
                                                       <td style="text-align:center;">
                                                        <?php echo date('d-m-Y',strtotime($row->created)) ?>
                                                       </td>
                                                  </tr>
                                             <?php  endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($members_properties): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
$(document).ready(function() {
     $('#check_membership').click(function(){
          var group_id = $('#group_id').val();
          $.ajax({
                   type:"post",
                   url:"<?php echo base_url() ?>user/check_membership",
                   data:{group_id:group_id},
                   success:function(res){
                       if(res=="valid"){
                         window.location = "<?php echo base_url() ?>user/add_property_to_group/"+group_id;
                        }
                       else if(res=="invalid"){
                         alert("Please Join The Group First.");
                       }
                   }
               }); 
     });
});

</script>
