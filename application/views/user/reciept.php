<!-- google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <!-- google map api -->
<?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Receipt</h3>
          </div>


        <?php $pro_info = get_property_detail($booking->pr_id); 
              $currency = $this->session->userdata('currency');
              $user = get_user_info($pro_info->user_id);  ?>
            <div class="ro0w">
                  <div class="container0 " style="">
                        <div class="col-sm-12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">                       
                        
<!--                          <div class="span3 pull-right" style="text-align:right">
                            <h3>Add Your Logo</h3>
                            <p>21 Park Street, Newyork NY USA<br/>
                            Wed, March 19, 2013</p>
                         </div>
 -->                        <?php $pro_info = get_property_detail($booking->pr_id); 
                            $user = get_user_info($pro_info->user_id);  ?>

                            <?php $customer = get_user_info($booking->customer_id); ?>
                            <?php $owner = get_user_info($booking->owner_id); ?>
                            <?php 
                              $startTimeStamp = strtotime($booking->check_in);
                              $endTimeStamp = strtotime($booking->check_out);
                              $timeDiff = abs($endTimeStamp - $startTimeStamp);
                              $numberDays = $timeDiff/86400;  // 86400 seconds in one day                              
                              $numberDays = intval($numberDays+1);                              
                             ?>
                                          
                          <h3>
                            Customer Receipt                            
                          </h3>
                          <h4>Booking Number - #<?php echo $booking->id; ?></h4>
                           <table class="table table-bordered">
                            <tbody><tr><th style="width:25%">NAME</th>
                            <th style="width:25%">DESTINATION</th>
                            <th style="width:25%">DURATION</th>
                            <th style="width:25%">ACCOMMODATION TYPE</th>
                            </tr><tr>
                              <td>
                                Dear, <?php echo ucfirst($customer->first_name." ".$customer->last_name); ?>
                              </td>
                              <td><?php echo $pro_info->city ?></td>
                              <td><?php echo  $numberDays ?> Nights</td>
                              <td><?php echo get_property_type($pro_info->property_type_id) ?></td>
                            </tr>
                           </tbody></table>

                           <table class="table table-bordered">
                            <tbody><tr><th style="width:25%">ACCOMMODATION ADDRESS</th>
                            <th style="width:25%">ACCOMMODATION HOST</th>                            
                            <th style="width:25%">CHECK IN</th>
                            <th style="width:25%">CHECK OUT</th>                            
                            </tr><tr>
                              <td><?php echo $pro_info->unit_street_address." ".$pro_info->city ?></td>
                              <td><?php echo $owner->first_name.' '.$owner->last_name ?><br>+<?php echo $owner->phone ?></td>
                              <td><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?> <br> Flexible checkin time</td>
                              <td>  <?php echo date('D, F d,Y', strtotime($booking->check_out)) ?> <br> Flexible checkout time</td>                              
                            </tr>
                           </tbody></table>
                      </div>


                      <div class="col-sm-10" style="margin-left:5%;">
                        <h3>Billing 
                        </h3>
                      
                        <table class="table table-bordered">
                          <tr>
                            <td>Check-in</td>
                            <td><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?></td>
                          </tr>
                          <tr>
                            <td>Check-out</td>
                            <td><?php echo date('D, F d,Y', strtotime($booking->check_out)) ?></td>
                          </tr>
                          <tr>
                            <td>Number of Nights</td>
                            <td><?php echo $numberDays ?></td>
                          </tr>

                           <?php 
                            $currency = $this->session->userdata('currency');
                            $total_amount = $booking->total_amount;

                               if(!empty($pro_info->cleaning_fee))
                               {
                                  $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                  $total_amount = $total_amount-$cleaning_fee;
                               }
                               if(!empty($booking->security_deposit))
                               {
                                  $total_amount = $total_amount-$booking->security_deposit;
                               }

                               $after_security_deposit_and_cleaning_fee = $total_amount;

                               $subtotal_after_coupon_if_coupon_exist = $after_security_deposit_and_cleaning_fee-$booking->commision-$booking->tax;

                           ?>



                          <?php if(!empty($booking->coupon_type)): ?>
                            
                              <?php if($booking->coupon_type=="percent"):?>
                                 
                                 <?php 
                                     $subtotal_before_coupon = ($subtotal_after_coupon_if_coupon_exist*100)/(100-$booking->coupon_amount);
                                   ?>

                                <tr>
                                  <td>Per night (approximate...) </td>
                                  <td>
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>

                                 
                                <tr>
                                  <td>Subtotal (before coupon price apply)</td>
                                  <td><?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type)." ".$currency; ?> </td>
                                </tr>

                                <tr>
                                  <td>Coupon type</td>
                                  <td> Percent </td>
                                </tr>
                                
                                <tr>
                                  <td>Coupon Amount</td>
                                  <td><?php echo $booking->coupon_amount ?> % </td>
                                </tr>

                              <?php endif; ?>
                              
                              <?php if($booking->coupon_type=="amount"): ?>

                                <tr>
                                  <td>Per night (approximate...) </td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td>
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>


                                <tr>
                                  <td>Subtotal (before coupon price apply)</td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td>
                                  <?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type) ?>
                                  </td>
                                </tr>

                                <tr>
                                  <td>Coupon type</td>
                                  <td> Amount </td>
                                </tr>
                                
                                <tr>
                                  <td>Coupon Amount</td>
                                  <td><?php echo $booking->coupon_amount ?></td>
                                </tr>

                              <?php endif; ?>

                          <?php endif ?>

                          <tr>
                            <td>Subtotal</td>
                            <?php $subtotal = convertCurrency($subtotal_after_coupon_if_coupon_exist,$currency,$booking->booking_currency_type) ?>
                            <td><?php echo number_format($subtotal,2)." ".$currency ?></td>
                          </tr>

                          <tr>
                            <td>Commision fee</td>
                            <td><?php 
                                 echo number_format($booking->commision,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <tr>
                            <td>Tax fee</td>
                             <td><?php 
                                 echo number_format($booking->tax,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <?php if(!empty($pro_info->cleaning_fee)): ?>
                             <tr>
                               <td>Cleaning Fee</td>
                               <td>
                               
                                  <?php $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                 
                                 /*convert into current website currency */
                                   $cleaning_fee = convertCurrency($cleaning_fee,$currency,$booking->booking_currency_type);
            
                                 /*convert into current website currency */
                                  echo number_format($cleaning_fee,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <?php if(!empty($booking->security_deposit)): ?>
                             <tr>
                               <td>Security Deposit</td>
                               <td>
                                   <?php $security_deposit = convertCurrency($booking->security_deposit,$currency,$booking->booking_currency_type) ?>
                                  <?php echo number_format($security_deposit,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <tr>
                            <td>Total</td>
                           <?php $total_amount = convertCurrency($booking->total_amount,$currency,$booking->booking_currency_type) ?>
                            <td><?php echo number_format($total_amount,2).' '.$currency  ?></td>
                          </tr>
                        </table>

                      <p style="text-align:center">THANKS FOR TRAVELING WITH BNBCLONE</p>
                      </div>  
                    </div>
                  </div> 
      </div>
    </div>
  </div>
</div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
    border-radius: 5px;
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    /*margin-left: 1%;*/
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#FFF;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>


<script>
  
// var address = "<?php print_r($pro_info->unit_street_address.', '.$pro_info->city.', '.$pro_info->state); ?>";
// var address = '12 Park Street, Brooklyn, New York';
  // alert(address);
  // $(document).ready(function(){
  // });

// alert(findlatlang(address));

// var myCenter=new google.maps.LatLng(findlatlang(address));
var myCenter=new google.maps.LatLng(51.508742,-0.120850);

var map;
var geocoder;
function initialize()
{
var mapProp = {
  center: myCenter,
  zoom:5,
  mapTypeId: google.maps.MapTypeId.ROADMAP
  };

var map = new google.maps.Map(document.getElementById("map"),mapProp);

var marker = new google.maps.Marker({
  position: myCenter,
  title:'Click to zoom'
  });

marker.setMap(map);

// Zoom to 9 when clicking on marker
google.maps.event.addListener(marker,'click',function() {
  map.setZoom(9);
  map.setCenter(marker.getPosition());
  });
}


// function findlatlang(address) {        
//   var geocoder = new google.maps.Geocoder();
//   var lat ='';
//   var lang ='';
//   geocoder.geocode( { 'address': address}, function(results, status) {
//     if (status == google.maps.GeocoderStatus.OK) {
//       lat  = results[0].geometry.location.A;
//       lang = results[0].geometry.location.K;      
//       // console.log(lat);
//       return lat+','+lang;
//       // map.setCenter(new google.maps.LatLng(lat,lang));
//       // placeMarker(new google.maps.LatLng(lat,  lang), map);
//       // streetviewmap();
//     } else {
//       result = "Unable to find address: " + status;
//     }
//   });
// }


google.maps.event.addDomListener(window, 'load', initialize);
</script>