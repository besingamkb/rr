<?php $this->load->view('user/leftbar'); ?>

<style>
a{ text-decoration:none !important; }
</style>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Welcome</h3>
          </div>
          <?php if(isset($msg)): ?>
            <?php alert($msg, $type); ?>
          <?php endif; ?>
          <div class="links">          
            <div class="content-div">
              <a href="<?php echo base_url() ?>page/how_it_works">  
                  <div class="pink-circle">
                    <img src="<?php echo base_url()?>assets/bnb_html/img/threegear.png">                  
                  </div>              
                  <p>How It Works</p>
              </a>
            </div>
            <div class="content-div">
              <a href="<?php echo base_url(); ?>front/listing_property_first">
                  <div class="pink-circle">
                    <img src="<?php echo base_url()?>assets/bnb_html/img/marker.png">                  
                  </div>              
                  <p>List Your Space</p>
              </a>
            </div>
            <div class="content-div ">
             <a href="<?php echo base_url() ?>user/groups">
              <div class="pink-circle">
                <img src="<?php echo base_url()?>assets/bnb_html/img/group.png">                  
              </div> 
              <p>Join Our Group</p>
             </a>              
            </div>
            <div class="content-div">
             <a href="#">
              <div class="pink-circle">
                <img src="<?php echo base_url()?>assets/bnb_html/img/facebook-white.png">                  
              </div>              
              <p>Facebook</p>
             </a>
            </div>
            <div class="content-div">
            <a href="#">
              <div class="pink-circle">
                <img src="<?php echo base_url()?>assets/bnb_html/img/twitterwhite.png">                  
              </div>              
              <p>Twitter</p>
            </a>
            </div>
          </div>
        </div>


        <div class="row content-mid">
          <div class="q_link">
            <h3>Quick links</h3>
          </div>
          <div class="links">          
            <div class="content-div">
                <a href="<?php echo base_url(); ?>user/properties">      
                  <div class="green-circle">
                     <img src="<?php echo base_url()?>assets/bnb_html/img/search.png">                  
                  </div>              
                  <p>Manage Listing</p>
                </a>
            </div>
            <div class="content-div">
              <a href="<?php echo base_url(); ?>user/bookings">      
              <div class="green-circle">
                <img src="<?php echo base_url()?>assets/bnb_html/img/bell.png" style="width:90%">                  
              </div>              
              <p>Reservation</p>
            </a>
            </div>
            <div class="content-div ">
                <a href="<?php echo base_url(); ?>user/review_about_you">      
                  <div class="green-circle">
                    <img src="<?php echo base_url()?>assets/bnb_html/img/eye.png" style="width:38px; margin-top:3px" >                  
                  </div>              
                  <p>Reviews</p>
                </a>
            </div>
            <div class="content-div">
              <a href="<?php echo base_url(); ?>user/favorites_property">      
                <div class="green-circle">
                  <img src="<?php echo base_url()?>assets/bnb_html/img/star.png">                  
                </div>              
                <p>Favorites</p>
              </a>
            </div>
            
          </div>
        </div>



        <div class="row content-top">
          <div class="q_link">
            <h3>Alert</h3>
          </div>
            <hr>
          
          <?php if(get_no_of_booking_alert() > 0): ?> 
          <span class="btm-span"> 
            <a href="<?php echo base_url()?>user/bookings" style='color:#F2A14F;'>! You have  <?php echo get_no_of_booking_alert(); ?> new booking notifications. <span class="glyphicon glyphicon-flash"></span></a>
          </span>  
          <hr>
          <?php endif; ?>
          

          <?php if(usermsg_notification() > 0): ?>
          <span class="btm-span"> 
            <a href="<?php echo base_url()?>user/messages_received" style='color:#F2A14F;'>! You have <?php echo usermsg_notification(); ?> new messages in your inbox. <span class="glyphicon glyphicon-comment"></span ></a>
          </span>
          <hr>
          <?php endif; ?>
    

            <span class="btm-span">
            Connect to facebook complete your profile and make easy to login
            </span>
          <div class="links">                      
            
          </div>
        </div>

        <style type="text/css">
         .invite
         {
          margin-bottom: 2.5% !important
         }
        </style>

         <div class="row content-top">
          <div class="q_link invite" >
            <h3>Invite your Friends </h3>
              <a href="<?php echo base_url() ?>user/invite" class="btn btn-default ">Invite Now</a> &nbsp;          
              <a href="javascript:void(0)" id="fshare" class="btn btn-info" style="background-color:#405D9A;border-color:#405D9A">Invite via Facebook</a> &nbsp;          
              <a class="btn btn-info" href="https://twitter.com/intent/tweet?text=I+have+been+renting+out+my+place+on+Bnbclone&url=<?php echo base_url() ?>properties/invite/<?php echo get_my_id() ?>" title="Share on Twitter" target='_blank'>
                Invite via Twitter
              </a>
           </div>
        </div>

      </div>
     <!--  <div class="starter-template">
        <h1>Bootstrap starter template</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div> -->

    </div><!-- /.container -->

<?php $facebook = get_oauth_keys('facebook'); ?>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : <?php echo $facebook->app_id ?>,
      status     : true,
      xfbml      : true
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

$('#fshare').click(function(){
      FB.ui({
           method:'feed',
           name: 'Bnbclone',
           caption: 'Bnbclone',
           description: ('I have been renting out my place on Bnbclone to meet interesting people and earn extra cash. I highly recommend you join me by listing your space. Cheers!'),
           link: '<?php echo base_url() ?>properties/invite/<?php echo get_my_id() ?>',
           picture: '<?php echo base_url(); ?>assets/img/BnbCloneLogo3.png',
          },
          function(response) {
            if (response && response.post_id) {
              alert('Post was published.');
            } else {
              alert('Post was not published.');
            }
          }
    );
});
</script>
