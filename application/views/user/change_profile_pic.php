<?php $this->load->view('user/leftbar'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/drag_drop_user_pic/css/drag_drop.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/drag_drop_user_pic/css/web_cam_and_edit_profile.css" />
<script src="<?php echo base_url() ?>assets/drag_drop_user_pic/js/jquery.filedrop.js"></script>
<script src="<?php echo base_url() ?>assets/webcamsnapper/photo.js"></script>
<script src="<?php echo base_url() ?>assets/webcamsnapper/video.js"></script>
 
 <style>
 a
 {
   text-decoration: none !important;
 }
#button123
{
  border-color: black;
  border-radius: 3px;
  padding: 5px 8px;
  cursor: pointer;
  font-weight:lighter;
  font-size: 10pt;
  color: black;
  max-width: 251px;
}
#web_img img{
  float: left;margin-left: 10%;width: 200px;
}

#my_camera{
    margin-top:-60px !important;
    margin-left:6% !important;
    border-top:0px !important;
    /*background-color:#EEEEEE;*/
    float: left;
    /*border-radius: 3px;*/
    /*margin: 80px auto 90px;*/
    /*overflow: hidden;*/
    /*padding-bottom: 40px;*/
    width: 50%;
    /*box-shadow: 0 0 4px rgba(0,0,0,0.3) inset,0 -3px 2px rgba(0,0,0,0.1);*/
    /*padding: 30px;*/
    min-height: 300px;
    /*padding-left: 5px;*/
}
#dropbox{
   margin-top:0px !important;
   margin-left:6% !important;
   border-top:0px !important;
   background-color:#EEEEEE;
   float: left;
}
.message{
   padding-top: 40px !important;
}

.browse_box{
   margin-top:0px !important;
   margin-left:6% !important;
   margin-bottom: 10% !important;
   width: 50%;
   border-radius: 4px;
   background-color: #EEEEEE;
   padding-top: 20px;
   padding-bottom: 20px;
   box-shadow: 0 0 4px rgba(0, 0, 0, 0.3) inset, 0 -3px 2px rgba(0, 0, 0, 0.1);
}
#optionsContainer{
   margin-left: 5% !important;
   padding:4px !important;
   padding-top: 30px !important;
   float: left !important;
}
</style>
 
      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Change Profile Image</h3>
          </div>
          <?php $this->load->view('user/account/profile_sub_header'); ?>
             <?php if($this->session->flashdata('error_msg')){ ?>
               <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
             <?php } ?>
             <?php if($this->session->flashdata('success_msg')){ ?>
                 <span align="center" style="padding:5px !important; width:400px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
             <?php } ?>
             <br>
              
             <div style="margin-left:6%">
                <a href="<?php echo base_url() ?>user/see_all_images">See All Images</a>
             </div>
             <br>


   <!-- Darg Drop Box Starts  -->
          <div id="dropbox"  >
             <span class="message">
                 <h4  style="color:#8F8F8F !important">
                    Drop images/video  here to upload. 
                 </h4>
                 <br>
                 <br>
                 <img  src="<?php echo base_url()?>assets/drag_drop_user_pic/images/smallUploadArea.png" style="border-radius:5px; width:160px; height:100px;">
              </span>
          </div> 
   <!-- Darg Drop Box Ends  -->

   <!-- Browse Div Starts-->
        <div align="center" id="bro" class="browse_box" style="display:none;float:left !important"> 
         <?php if(!empty($user->image)) : ?>
            <img  src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="border-radius:5px; width:150px; height:150px;"><br>
        <?php else: ?>
            <img  src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:200px"><br>
        <?php endif; ?>
           <br>
          <?php echo form_open_multipart(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
                <input type="file" name="userfile" id="button123" >
                <br>
                <button type="submit" class="btn btn-info" >Change Image</button>
          <?php echo form_close(); ?>
        </div>
   <!-- Browse Div Ends-->



   <!-- toggle box for drag drop  and browse images and Web camera starts -->
     <div  id="optionsContainer">
     <!-- webcam camera starts -->
         <div type="webcam"  class="otherOption take_photo" id="snap_button" style="cursor:pointer;">
             <input type="hidden" id="my_hidden_field" value="">
             <div class="optionIcon">
                <div class="optionImg" id="webcamOption"></div>
             </div>
            <span style="font-size:14px">Take a photo with your webcam</span>
         </div>
      <!-- webcam camera Ends -->
      <!-- Upload a picture starts -->
         <div class="otherOption swap" id="upl" style="display: block;">
           <div class="optionIcon"><div class="optionImg" id="uploadOption"></div></div>
           <span style="font-size:14px">Upload a file from your computer</span>
         </div>
         <div class="otherOption swap" id="dd" style="display: none;">
           <div class="optionIcon"><div class="optionImg" id="ddOption"></div></div>
           <span style="font-size:14px">Drag &amp; drop a new profile photo</span>
         </div>
      <!-- Upload a picture Ends -->
    </div>
<!-- toggle box for drag drop  and browse and Web camera images Ends -->

     <div  id="optionsContainer">
         <div type="webcam"  class="otherOption take_photo" id="record_button" style="cursor:pointer;">
             <input type="hidden" id="my_hidden_field" value="">
             <div class="optionIcon">
                <div class="optionImg" id="webcamOption"></div>
             </div>
            <span style="font-size:14px">Upload Video From here </span>
         </div>
    </div>          

        </div>
      </div>
   <!-- /.container -->
<script>
   $(document).ready(function(){
      $('#upl').click(function(){
        $('#bro').show(); 
        $('#dropbox').hide(); 
        $('#upl').hide();
        $('#dd').show();
      });
      $('#dd').click(function(){
        $('#bro').hide(); 
        $('#dropbox').show(); 
        $('#dd').hide();
        $('#upl').show();
      });
   });
</script>

<!-- Script.js file drag and drop starts -->
<script>

$(function(){
  
  var dropbox = $('#dropbox'),
    message = $('.message', dropbox);
  
  dropbox.filedrop({
    // The name of the $_FILES entry:
    paramname:'pic',
    maxfiles: 5,
    maxfilesize: 10,
    url: '<?php echo base_url() ?>user/ajax_drag_drop',
    allowedfiletypes: ['image/jpeg','image/png','image/gif', 'video/mp4'],   // filetypes allowed by Content-Type.  Empty array means no restrictions
    allowedfileextensions: ['.jpg','.jpeg','.png','.gif','.mp4'], // file extensions allowed. Empty array means no restrictions
    
    uploadFinished:function(i,file,response){
      $.data(file).addClass('done');

      // response is the JSON object That user controller returns
      // alert(response.status);
      // console.log(response.status);
      // response is the JSON object That user controller returns
    },
    
      error: function(err, file) {
      switch(err) {
        case 'BrowserNotSupported':
          showMessage('Your browser does not support HTML5 file uploads!');
          break;
        case 'TooManyFiles':
          alert('Too many files! Please select 5 at most! (configurable)');
          break;
        case 'FileTooLarge':
          alert(file.name+' is too large! Please upload files up to 2mb (configurable).');
          break;
        default:
          break;
      }
    },
    
    // Called before each upload is started
    // beforeEach: function(file){
    //   if(!file.type.match(/^image\//)){
    //     alert('Only images are allowed!');
        
    //     // Returning false will cause the
    //     // file to be rejected
    //     return false;
    //   }
    // },
    
    uploadStarted:function(i, file, len){
      createImage(file);
    },
    
    progressUpdated: function(i, file, progress) {
      $.data(file).find('.progress').width(progress);
    }
       
  });
  
   var template = '<div class="preview">'+
                  '<span class="imageHolder">'+
                     '<img />'+
                     '<span class="uploaded"></span>'+
                  '</span>'+
                  '<div class="progressHolder">'+
                     '<div class="progress"></div>'+
                  '</div>'+
               '</div>'; 
  
  
  function createImage(file){

    var preview = $(template), 
      image = $('img', preview);
      
    var reader = new FileReader();
    
    image.width = 100;
    image.height = 100;
    
    reader.onload = function(e){
      
      // e.target.result holds the DataURL which
      // can be used as a source of the image:
      
      image.attr('src',e.target.result);
    };
    
    // Reading the file as a DataURL. When finished,
    // this will trigger the onload function above:
    reader.readAsDataURL(file);
    
    message.hide();
    preview.appendTo(dropbox);
    
    // Associating a preview container
    // with the file, using jQuery's $.data():
    
    $.data(file,preview);
  }

  function showMessage(msg){
    message.html(msg);
  }

});
</script>
<!-- Script.js file drag and drop Ends -->   

<script type="text/javascript">
  $(function(){
  
  // Set global config
  Snapper.configure({
    snapper_size: 'tiny', // set the default snapper size to tiny
    snapper_border: 'round shadow', // render snapper with a round corners and a shadow
    output_format: 'png' // switch from 'jpg' to use 'png' when saving.
    // see configuration section of docs for full list.
  });
  
  // Load snapper when a button is clicked
  $('#snap_button').click(function(){
    Snapper.show({ // open snapper in an overlay window
      complete: function(image){ // called when the photo has been saved
        var image_temp_url = image;
          $.ajax
          ({
             type:"post",
             url:"<?php echo base_url() ?>user/upload_webcam_image",
             data:{image_temp_url:image_temp_url},
             success:function(res)
             {
               if(res=="ok")
               {
                 window.location = "";
               }
             }
          });      
      }, 
      // instance config 
      snapper_size: 'medium' // override the default size, set it to medium 
    });

  });
  
  
}); 

  
$(function(){
  
  // Set global config (this is optional)
  Recorder.configure({
    size: 'small', // set the default size to small
    bandwidth: 180, // set the max upload bandwidth to 180 kbps
    fps: 15, // set the video capture to 15 frames per second
    // see configuration section of docs for full list.
  });
  
  
  // Load the recorder when a button is clicked
  $('#record_button').click(function(){
    Recorder.show({ // open snapper in an overlay window
      complete: function(outputs){ // called when the video has been saved
        // set the output values into form fields 

      // var flv = outputs['flv']; // used for playback in flash video player
      var mp_4_ = outputs['mp4']; // used for iphone / android phones
      // var _3_gp = outputs['3gp']; // used for older mobile phones
        $.ajax
        ({
          type:"post",
          url:"<?php echo base_url() ?>user/upload_webcam_video",
          data:{mp_4_:mp_4_},
          success:function(res)
          {
            if(res=="ok")
            {
               alert(" Video Has been Uploaded Successfully. ");
               window.location = "";
            }
          }
        }); 
      }, 
      // instance config 
      resolution: '640x480' // override the default size (320x240), set it to 640x480 
    });
  });
  
});  
  
  
</script>




