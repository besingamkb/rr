<?php $this->load->view('user/leftbar'); ?>

 <style>
.members{
   font-size:150%;
   font-weight: Lighter;
   margin-bottom:40%;
}
a{
  text-decoration: none !important;
}
 </style>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Group Members</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
          <!-- Group members Starts -->
          <div class="span2 pull-left" style="margin-left:25%;">
            <img src="<?php echo base_url() ?>assets/img/third_party/member_customer.png" width="150" height="141" >
             <br>
            <div align="center" class="members">
              <a href="<?php echo  base_url() ?>user/customer_group_members/<?php echo $group_id ?>">
                Customer : 
                 (<span style="color:Red"><?php echo get_no_of_customer_from_group($group_id) ?><span>)
              <a>
            </div>
          </div>
          <div class="span2 pull-left" style="margin-top:2%;margin-left:5%">
             <img src="<?php echo base_url() ?>assets/img/third_party/member_owner.png" width="170" height="126">
              <br>
            <div align="center" class="members">
              <a href="<?php echo base_url() ?>user/owner_group_members/<?php echo $group_id ?>">
               Property Owner : 
               (<span style="color:Red"><?php echo get_no_of_owner_from_group($group_id) ?><span>)
              <a>
            </div>
         </div>
         <!-- Group members  Ends-->
       </div>
     </div>
   </div>
 </div>
 <!-- /.container -->