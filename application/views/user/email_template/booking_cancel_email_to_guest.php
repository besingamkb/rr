Hello <b>
        <?php echo $guest_info->first_name ?>
      </b> !<br>
       Your booking has been cancelled successfully on www.Bnbclone.com by superadmin and your amount is fully refunded to paypal account.
<br>
<br>
************ Here are the detail of this booking *************

<br>
<br>
<br>
<table>
<tr><td style="text-align: right;">Property Image:</td><td><img style="box-shadow: 1px 1px 5px #CCCCCC;border:2px solid white;width:100px;height:100px;border-radius: 4px;" class="image_email" src="<?php echo base_url() ?>assets/uploads/property/<?php echo $property_info->featured_image ?>"></td></tr>
<tr><td ></td><td>&nbsp;</td></tr>
  
<tr><td style="text-align: right;">Property Name :</td><td><?php echo $property_info->title ?></td></tr>
<tr><td style="text-align: right;"> Host Name :</td><td><?php echo $host_info->first_name." ".$host_info->last_name ?></td></tr>
<tr><td style="text-align: right;">Check In :</td><td><?php echo date('d F Y',strtotime($booking_info->check_in)) ?></td></tr>
<tr><td style="text-align: right;"> Check Out :</td><td><?php echo date('d F Y',strtotime($booking_info->check_out)) ?></td></tr>
</table>

<style type="text/css">
	.image_email
	{
     width:100px;
     height:100px;
     border-radius: 4px;
     border:2px solid white;
     box-shadow: 1px 1px 5px #CCCCCC;
	}
	tr td:first-child
	{
		text-align: right;
	}
	a
	{
		text-decoration: none;
	}
</style>

