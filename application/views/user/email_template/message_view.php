    <?php $this->load->view('user/leftbar'); ?>

        <div class="col-lg-9">
            <div class="row content-top">
                <div class="welcome">
                    <h3>Message</h3>
                </div>
                <?php if($this->session->flashdata('error_msg')){ ?>
                    <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
                <?php } ?>
                <?php if($this->session->flashdata('success_msg')){ ?>
                    <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
                <?php } ?>
                <br>
            <div class="col-md-12 row">

<!-- ///////////////////  Main Message /////////////////////////////// -->
<div class="col-md-12 row faraz" style="margin-left:1%; margin-top:20px;">
    <div class="col-md-2" align="center" >
        <?php $user = get_user_info($message->sender_id); ?>
        <?php $image = $user->image; ?>
        <?php if(!empty($image)): ?>
        <img src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $image;  ?>" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php else: ?>
        <img src="<?php echo base_url(); ?>assets/img/msg.png" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php endif; ?>
        <br>
        <?php echo $message->user_name; ?>
    </div>
    <div class="col-md-10" style="padding:10px; box-shadow:5px 10px 7px #8F8F91; border:1px solid #8F8F91; border-radius:5px; background-color:white; color:black;">
        <strong>Property Title:</strong>
        <?php if(!empty($message->pr_title)) echo $message->pr_title; ?>    
        <br> 
        <strong>Message:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
        <?php if(!empty($message->message)) echo $message->message; ?> 

        <div style="color:#E585A8; padding:5px;">
            Sent 
            &nbsp;on&nbsp;<?php echo get_time_ago($message->created); ?> 
        </div>
    </div>
</div>
 

<!-- ///////////////////  Reply Message /////////////////////////////// -->
<?php $user_info = $this->session->userdata('user_info');  ?>
<?php if($reply): ?>
<?php foreach ($reply as  $row): ?>
    <div class="col-md-12 row faraz" style="margin-left:1%; margin-top:50px;">
        <div class="col-md-2" align="center" >
        <?php $user  = get_user_information($row->replier_email); ?>
        <?php $image = $user->image; ?>
        <?php if(!empty($image)): ?>
        <img src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $image;  ?>" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php else: ?>
        <img src="<?php echo base_url(); ?>assets/img/msg.png" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php endif; ?>
                <br>
            <?php if($row->replier_status == 1 ) echo "Superadmin"; ?>
            <?php if($row->replier_status == 2 ) echo "Admin"; ?>
            <?php if($row->replier_status == 3 ) echo "Me";    ?>
            <?php if($row->replier_status == 4 ) echo "Customer"; ?>
        </div>
        <div class="col-md-10" style="padding:10px; box-shadow:5px 10px 7px #8F8F91; border:1px solid #8F8F91; border-radius:5px; background-color:white; color:black;">
            <?php if(!empty($row->reply)) echo $row->reply; ?>    
            <div style="color:#E585A8; padding:5px;">
                Sent 
                &nbsp;on&nbsp;<?php echo get_time_ago($row->created); ?> 
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>  


<div id="dynamic_reply"></div>
<!--////////////////   LOAD MORE START  ////////////////////  -->
<div class="col-md-10" style="margin:2% 10% 0% 17%; color:#33AEBD; ">
    <?php if ($total_reply >4): ?>
        <h3><a id="loadmore" style="color:#DE4980; text-decoration:none; cursor:pointer;">Load more.........</a></h3> 
    <?php endif; ?>
    <input type="hidden" value="<?php echo $total_reply; ?>" id="total_reply">
</div>
<!--////////////////   LOAD MORE END  ////////////////////  -->



<!--/////////////////////   Reply option ///////////////////////////////////////////////  -->
<div class="col-md-10" style="margin:10%; padding:10px;  border-radius:5px; ">
<?php echo form_open(base_url().'user/reply/'.$message->id); ?>
<table align="left" style="width:702px; margin-top:20px;">    
    <tr>
        <td>
            <textarea  rows="2" style="color:#33AEBD; padding:5px; width:600px; border:5px #88E0EA solid; font-weight:bold; border-radius:15px"  name="reply" required="required"></textarea>
            <br><br>
            <input type='submit' onclick='return validation()' style="margin-left:540px" class="btn btn-info btn-large" value='Reply'>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>
</div>
<!--////////////////////// End of reply option ////////////////////////////////  -->
            </div>
        </div>
    </div>
</div>






<!-- //////////////////////  ajax load more reply /////////////////////////// -->
<script>

    function validEmail(email) 
    {
        var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        return (email.match(r) == null) ? false : true;
    }
    
    function validation()
    {
        var incomeSTR = $('[name=reply]').val().toLowerCase();
        var incomeARRAYraw = incomeSTR.split(' ');
        incomeARRAY = Array();
        var i = 0;
        $.each(incomeARRAYraw,function(index,value){
            if(incomeARRAYraw[index] != ""){
                incomeARRAY[i] = value;
                i++;
            }
        });
        var outSTR = '';
        var pre = '';
        $.each(incomeARRAY,function(index,value){
            var len = value.length;
            var status = true;
            var next = parseInt(index)+1;

            if($.isNumeric(pre) && $.isNumeric(value)){ 
                status = false;
            }
            if($.isNumeric(incomeARRAY[next]) && $.isNumeric(value)){ 
                status = false;
            }
            if($.isNumeric(value) && len>3){ 
                status = false;
            }
            if(validEmail(value)){
                status = false;
            }
            if(value.indexOf('.') != -1){
                if(value.indexOf('.') != len-1){
                        status = false;
                    }
            }
            if(value.indexOf('/') != -1){
                status = false;
            }
            if(value == 'zero'){
                status = false;
            }
            if(value == 'one'){
                status = false;
            }
            if(value == 'two'){
                status = false;
            }
            if(value == 'three'){
                status = false;
            }
            if(value == 'four'){
                status = false;
            }
            if(value == 'five'){
                status = false;
            }
            if(value == 'six'){
                status = false;
            }
            if(value == 'seven'){
                status = false;
            }
            if(value == 'eight'){
                status = false;
            }
            if(value == 'nine'){
                status = false;
            }

            if(status == true)
            {
                outSTR += incomeARRAY[index]+' ';
            }
            else
            {
                incomeARRAY[index] = '######';
                outSTR += incomeARRAY[index]+' ';
            }    
            pre = value;
        });
        $('[name=reply]').val(outSTR);
        return true;
    }


$(document).ready(function(){ 
    $.ajax({
        type:'post',
        url:'<?php echo base_url() ?>user/update_msg/<?php echo $message->id; ?>',
        success:function(data)
        {
            return true;
        }

    });

   $("#loadmore").click(function(){ 
      
      var offset = $(".faraz").length-1;
      $.ajax({
               url: "<?php echo base_url(); ?>user/ajax_load_more_reply/<?php echo $message->id; ?>/"+offset,
               success: function(data)
               {
                 $("#dynamic_reply").append(data);
               }
      });
      
         var total_rows = $("#total_reply").val();
         if(total_rows <= offset+4)
         {
         $("#loadmore").hide();
         }
   });
});


$(document).ready(function(){
    $.ajax({
        type:"POST",
        url:'<?php echo base_url() ?>user/changemsgstatus/<?php echo $message->id ?>',        
    });
});
</script>


