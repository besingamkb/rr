

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
               <?php if($trip=="Current"){ ?>
               <h3>Current Trips View</h3>
               <?php }else if($trip=='Previous'){ ?>
               <h3>Previous Trips View</h3>
               <?php }else if($trip=="Upcoming"){ ?>
               <h3>Upcoming Trips View</h3>
               <?php } ?>
               </div>

               </div>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:20%">Transaction_id</th>
                                                       <th style="width:20%">Property</th>
                                                       <th style="width:15%">Check in</th>
                                                       <th style="width:15%">Check Out</th>
                                                       <th style="width:15%">payment status</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($current_trips)): ?>
                                             <?php $i=1; foreach ($current_trips as $row):?>
                                                  <tr>
                                                       <td><?php echo $row->transaction_id ?></td>
                                                       <td><?php echo word_limiter($row->pr_title,10); ?></td>
                                                       <td><?php echo date('d-m-Y',strtotime($row->check_in)); ?></td>
                                                       <td><?php echo date('d-m-Y',strtotime($row->check_out)); ?></td>
                                                       <td><?php echo $row->payment_status ?></td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
