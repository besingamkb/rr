


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
          <?php if($trips->trip=="Current"){ ?>
            <h3>Current Trips View</h3>
            <?php }else if($trips->trip=='Previuos'){ ?>
            <h3>Previous Trips View</h3>
            <?php }else if($trips->trip=="Upcoming"){ ?>
            <h3>Upcoming Trips View</h3>
            <?php } ?>
          </div>
          <br><br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->pr_title)) echo $trips->pr_title; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Detail</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->pr_description)) echo $trips->pr_description; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Transaction Id</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->transaction_id)) echo $trips->transaction_id; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Total Amount</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->total_amount)) echo $trips->total_amount; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Due Amount</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->due_amount)) echo $trips->due_amount; else echo "0" ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Check In</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->check_in)) echo $trips->check_in; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Check Out</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->check_out)) echo $trips->check_out; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Payment Status</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($trips->payment_status)) echo $trips->payment_status; ?>    
                    </div>
                </div>
                
                <br>

                
                <br>
                
                
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->