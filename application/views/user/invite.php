
 <style>

#emailbox{
  width: 40%;
  margin-left: 30%;
  margin-bottom: 3%;
}

.emailform{
  /*text-align: center*/
}
</style>
 
<?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">          
          <div class="welcome">
            <h3>Invite Your Friends</h3>
          </div>                    
             <?php if($this->session->flashdata('error_msg')){ ?>
               <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
             <?php } ?>
             <?php if($this->session->flashdata('success_msg')){ ?>
                 <span align="center" style="padding:5px !important; width:400px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
             <?php } ?>
             <div style="text-align:center">
            <p>You'll get $10 when they take a trip & $10 when they rent out their place.</p>
          </div>
          <div class="success" style="text-align:center; color:green">            
          </div>
          <hr>
          <div class="row" style="text-align:center; font-size:25px; margin-bottom:2%">
            <div>
              Get Started <a href="javascript:void(0)" id="showform" class="btn btn-lg btn-primary" style="background-color:#de4980; border-color:#de4980">Email Your Friends</a>
            </div>
          </div>
          <div class="row" class="emailform"  >
            <div id="emailbox" style="display:none">
            <hr>
            <form role="form" id="invite_form" onsubmit='return false;'>
              <div class="form-group">
                <label for="">Email addresss</label>
                <input type="text" name="email" id="email" class="form-control"  placeholder="Enter email seperated by comma (,)">
                <p>friend@example.com,friend@example.com</p>
                <p class="erremail" style="color:red;"></p>
              </div>
              <div class="form-group">
                <label for="">Message</label>
                <textarea class="form-control" id="message" name="message" rows="8">I've been renting out my place on Bnbclone to meet interesting people and earn extra cash. I highly recommend you join me by listing your space. Cheers! <?php $ses = $this->session->userdata('user_info'); echo $ses['first_name']; ?></textarea>
                <p class="errmsg" style="color:red; display:none">please enter Message</p>
              </div>              
              <a href="javascript:void(0)" id="submitform" class="btn btn-default">Submit</a>
            </form>
            </div>
          </div>
        </div>
      </div>
   <!-- /.container -->

   <script type="text/javascript">
    $('#showform').click(function(){
        $('#emailbox').slideToggle(300);
    });

    $('#submitform').click(function(){
      $(this).attr('disabled','disabled');
          var emails = $('#email').val();       
          var message = $('#message').val();           
          if(message == ""){            
            $('.errmsg').show();
            $('#submitform').removeAttr('disabled');
            return false;
          }
          var emailarr = emails.split(",");          
          var flag = 0;
          $.each(emailarr , function( index, value ) {              
              if(!validateEmail(value)){                
                flag = 1;                 
              }
          });

          // alert(flag);

          if(flag){            
            $('.erremail').html('Please enter valid email');
            $('#submitform').removeAttr('disabled');
            return false;
          }else{
            var formdata = $('#invite_form').serialize();
            $.ajax({
              type:'POST',
              data: formdata,
              url:'<?php echo base_url() ?>user/sendinvite',
              success:function(res){
                obj = JSON.parse(res);
                console.log(obj);
                if(obj.status == 'error'){
                  $('.erremail').html(obj.error);
                }else if(obj.status == 'success'){
                  $('.erremail').html('');
                    <?php $this->session->set_flashdata('success_msg','submitted') ?>
                    window.location.replace("<?php echo base_url() ?>user/manage_invitations");
               
                }
                $('#submitform').removeAttr('disabled');
              }
            });

          }
    });

    function validateEmail(email) {       
          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          // alert(re.test(email));
          return re.test(email);
      }
   </script>
