<!-- 4 july 2014 Work -->
<?php $this->load->view('user/leftbar'); ?>


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Messages Sent</h3>
          </div>
         <?php alert(); ?>
         <div class="pull-left">
              <a href="<?php echo base_url() ?>user/messages_received"  class="btn">Messages Received</a>  
         </div>
         <div class="pull-left">
              <a style="color:#808080 !important" href="<?php echo base_url() ?>user/messages_sent"  class="btn">Messages Sent</a>  
         </div>
        <div class="links"> 
        <table class="table">
          <thead>
            <tr>
              <th style="width:5%">&nbsp;&nbsp;#</th>
              <th style="width:12%">Property Title</th>
              <th style="width:15%">Messages</th>
              <th style="width:10%">Created</th>
              <th style="width:20%">Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($messages)){ $i=1; foreach ($messages as $row){?>    
            <tr <?php if($row->sender_alert == '0') echo 'style="background-color:#efefef"'; ?> >
              <td>
               <?php 
                    $user_info = $this->session->userdata('user_info');
                    $current_email = $user_info['user_email'];
                    $sender_email = $row->user_email;
              ?>
<img src="<?php echo base_url(); ?>assets/img/out.png" >
              </td>
              <td>
                <?php if(!empty($row->pr_title))echo substr($row->pr_title,0,6);  ?></td>
              <td><?php if(!empty($row->message)) echo substr($row->message,0,15); ?></td>
              <td><?php if(!empty($row->created)) echo date('m/d/Y',strtotime($row->created)); ?></td>
              <td>
                  <a onclick="update_alert(<?php echo $row->id ?>)" style="width:90px" class="btn btn-default" href="javascript:void(0)">
                    View <?php if(new_reply_for_sender($row->id) !=0): ?>(<font color="red"><?php  echo new_reply_for_sender($row->id); ?></font>/<?php echo total_reply_count($row->id); ?>)<?php endif; ?>
                  </a> 
                  <a class="btn btn-info" onclick="return confirm('Do you want to delete it?');"  href="<?php echo base_url()?>user/delete_by_sender/<?php echo $row->id;?>">Delete</a>
              </td>
            </tr>
            <?php $i++; } } else{ ?>
              <tr><td colspan="6"> No Record Found</td></tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <span style="margin-left:20px"><?php if($pagination){echo  $pagination;} ?></span>
    </div>
  </div>
</div><!-- /.container -->

<script type="text/javascript">
  function update_alert(message_id)
  {
    $.ajax({
            type:"post",
            url:"<?php echo base_url() ?>user/update_message_status/"+message_id+'/'+'sender_alert',
            success:function(res)
            {
                window.location = "<?php echo base_url() ?>user/message_view/sent/"+message_id;
            }
         });
  }
</script>