<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Favorites</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <?php if(!empty($favorites_property)): ?>
                                             <thead>
                                                  <tr>
                                                       <th style="width:10%">Image</th>
                                                       <th style="width:15%">Property</th>
                                                       <th style="width:15%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php $i=1; foreach ($favorites_property as $row):?>
                                                  <tr>
                                                       <td><img class="iamg_imag" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                                                       <td><a href="<?php echo base_url() ?>properties/details/<?php echo $row->property_id ?>" ><?php echo word_limiter($row->title,10); ?></a></td>
                                                       <td style="text-align:center;">
                                                            <a href="<?php echo base_url() ?>properties/details/<?php echo $row->property_id ?>"   class="btn btn-info" >View</a>
                                                            <a href="<?php echo base_url();?>user/delete_favorites_property/<?php echo $row->id;?>" class="btn btn-warning btn-small" onclick="return confirm('Do you want to delete?' );" >Delete</a>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($favorites_property): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- <td><a href="<?php echo base_url();?>superadmin/add_property_note/<?php echo $row->id;?>" class="btn btn-warning btn-small" >Add Note</a></td>
                                                   -->
<style type="text/css">
.iamg_imag
{
width:100px;
height:100px;
border-radius: 4px;
border:3px solid white;
box-shadow: 2px 2px 4px;

}
a
{
text-decoration: none !important;
}
</style>