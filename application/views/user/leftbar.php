<div class="col-lg-12 btm-nav">
    <div class="container">
        <div class="btn-group btn-pdng col-lg-7" style="padding-bottom:10px">
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/dashboard"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-home"></span >&nbsp;Dashboard</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/profile">  <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-user"></span >&nbsp;Profile</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/bookings"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-flash"></span>&nbsp;Booking <sup><label style="color:red; margin-top:1px"><?php echo get_no_of_booking_alert(); ?></label></sup></button></a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/messages_received"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-comment"></span >&nbsp;Inbox <sup><label style="color:red; margin-top:1px"><?php echo usermsg_notification(); ?></label></sup></button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/change_password"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit"></span >&nbsp;Account</button> </a>
        </div>
       <!--  <div class="pull-right">
            <form class="navbar-form" role="search">
                <div class="input-group">
                    <input type="text" class="srch-box form-control" placeholder="Search" name="srch-term" id="srch-term">
                    <div class="input-group-btn">
                        <button class="btn btn-default src-btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div> -->
    </div>
</div>
<div class="container">
    <div class="col-lg-3 side-bar">
        <ul>
            <li style="text-align:center">
             <a href="<?php echo base_url() ?>user/change_profile_image" style="text-decoration:none;">
                <?php $user = get_user_row(); ?>
                <?php if($user->image != "") : ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="height:200px;width:200px"><br>
                <?php else: ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:150px"><br> 
                <?php endif; ?>
                <p></p>
                <p style="color:#8A8A8B !important">
                    <?php echo ucfirst($user->first_name) ?>
                </p>
             </a>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/view_profile">
                    <span>
                       View Profile
                    </span>
                </a> 
                <span class="f-right"></span>
            </li>
            <li>
                <a style="text-decoration:none;"  href="<?php echo base_url(); ?>user/messages_received">
                    Messages
                </a>
                <span class="f-right"><?php echo get_user_msg_count(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/review_about_you"> 
                    Reviews
                </a>    
            <span class="f-right"><?php echo get_user_review_count(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/properties">
                    <span>Properties</span>
                </a>
                <span class="f-right"><?php echo get_user_property_count(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/FavoritesCategory"> 
                    <span>Favorites Category</span>
                </a>    
                <span class="f-right"><?php echo get_no_of_favoritesCategory(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/groups"> 
                    <span>Groups</span>
                </a>    
                <span class="f-right"><?php echo get_number_of_groups(); ?></span>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/current_trips">
                    <span>Trips</span>
                </a>
                <span class="f-right"></span>
            </li>

<!--             <li>
                <a style="text-decoration:none;" href="<?php //echo base_url()?>user/payments">
                    <span>Payments</span>
                </a>
                <span class="f-right"></span>
            </li>
 -->            
             <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/manage_invitations">
                    <span>Manage Invitation</span>
                </a>
                <span class="f-right"></span>    
            </li>
            <li style="display:none">
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/change_profile_image">
                    <span>Change Profile Image</span>
                </a>    
            </li>
<!--             <li class="no-border">
                <a style="text-decoration:none;" href="<?php //echo base_url()?>user/change_password">
                    <span>Change Password</span>
                </a>    
            </li>
 -->        </ul>
        <!-- <ul>
            <li>
                <p>Upcoming Booking</p>
                <p></p>
                <p>You have no upcoming booking</p>
                <p>Discover or create one</p>
            </li>
        </ul> -->
    </div>

    <script type="text/javascript">
        $('.side-bar ul li a').click(function(event){
            event.preventDefault();
          $(this).siblings('span').html("<img src='<?php echo base_url() ?>assets/img/loading-blue.gif'>");
            var link = $(this).attr('href');
          setTimeout(function(){
              window.location = link;
           },1000);
        });
    </script>
