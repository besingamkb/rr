<?php $this->load->view('user/leftbar'); ?>

<style type="text/css">
  .zak_zak
  {
    margin-top: 6% !important;
    min-height: 300px;
    border-radius: 5px !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
</style>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Featured Images</h3>
               </div>

               <div class="sub-header">
                  <a class="btn" href="<?php echo base_url() ?>user/manage_listing/<?php echo $property_id ?>">Manage listing</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/address_description/<?php echo $property_id ?>" > Address and Description </a>  
                  <a class="btn" href="<?php echo base_url(); ?>user/change_property_image/<?php echo $property_id ?>">Photos</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/calendar/<?php echo $property_id ?>">Calender</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/price_terms/<?php echo $property_id ?>">Price and terms</a>  
                  <a  style="color:#777777" class="btn" href="<?php echo base_url() ?>user/featured_images/<?php echo $property_id ?>">Featured Images</a>  
               </div>

               <?php alert(); ?>
               <div class="row-fluid zak_zak">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:5%;padding-left:15px">#</th>  
                                                       <th style="width:20%">Images</th>                                                         
                                                       <th style="width:15%">Created</th>
                                                       <th style="width:25%">Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($featured_images)): ?>
                                             <?php $i=1; foreach ($featured_images as $row):?>
                                                  <tr> 
                                                       <td style="padding-left:15px" ><?php ++$offset; echo $offset ?></td>                                                       
                                                        <td ><img class="featured_image" src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->image_file ?>" ></td>                                                                         
                                                        <td ><?php echo date('d F Y',strtotime($row->created)); ?></td>                                                                         
                                                        <td>
                                                          <?php if($row->is_featured_image==1): ?>
                                                              It is a Featured Image
                                                          <?php else: ?>
                                                              <?php if($row->featured_request==0 ){ ?>
                                                                 <a href="<?php echo base_url() ?>user/request_for_make_featured_image/<?php echo $row->id ?>/<?php echo $row->pr_id ?>" class="btn btn-info address_title_description" title="Request For Featured Image">Make Featured Image</a>
                                                              <?php }elseif($row->featured_request==1 ){ ?>
                                                                         Request Sent
                                                              <?php }else if($row->featured_request==2 ){ ?>
                                                                 <a href="javascript:void(0)" onclick="model_for_subscription(<?php echo $row->id ?>)" class="btn btn-info address_title_description" title="Your request has been approved now Pay For Featured Listing">Pay</a>
                                                              <?php } ?>
                                                          <?php endif; ?>                                                             
                                                         </td>                                                                         
                                                    
                                                  </tr>
                                             <?php  endforeach; ?>
                                             <?php else: ?>
                                                  <tr style="margin-left:15px">
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($featured_images): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>


<!-- Model  starts-->

<script type="text/javascript">
   function model_for_subscription(image_id)
   {
      $('input[name=image_id]').val(image_id);
      $("#paypal_model").modal();    
   }

   function subs_amount(amount)
   {
      $('input[name=subscriptions_amount]').val(amount);
   }
</script>


<div class="modal fade" id="paypal_model">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Subscriptions</h4>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title" id="note-subject">Featured Image Subscriptions Pack</h3>
        </div>
        <div class="panel-body" id="note-msg">

<?php $featued_image_subscription_info = get_row('featued_image_subscription',array('name'=>'featued_image_subscription')); ?>

        <?php echo form_open(base_url().'user/pay_for_featured_subscriptions/'.$property_id); ?>

         <table>
             <tr>
               <th>Subscriptions</th>
               <th>Type</th>
               <th>Cost</th>
             </tr>
             <tr>
           <?php if (!empty($featued_image_subscription_info->daily)): ?>
               <td><input type="radio" onclick="subs_amount(<?php echo $featued_image_subscription_info->daily ?>)" name="subscriptions_type" value="daily" checked=""></td>
               <td>Daily</td>
               <td><?php echo $featued_image_subscription_info->daily  ?> USD </td>
           <?php else: ?>
               <td><input type="radio" onclick="subs_amount(10)" name="subscriptions_type" value="daily" checked=""></td>
               <td>Daily</td>
               <td>10 $ </td>
           <?php endif; ?>    
             </tr>
             <tr>
           <?php if (!empty($featued_image_subscription_info->weekly)): ?>
               <td><input type="radio" onclick="subs_amount(<?php echo $featued_image_subscription_info->weekly ?>)" name="subscriptions_type" value="weekly" ></td>
               <td>Weekly</td>
               <td><?php echo $featued_image_subscription_info->weekly ?> USD </td>
           <?php else: ?>
               <td><input type="radio" onclick="subs_amount(50)" name="subscriptions_type" value="weekly" ></td>
               <td>Weekly</td>
               <td>50 $ </td>
           <?php endif; ?>    
             </tr>
             <tr>
           <?php if (!empty($featued_image_subscription_info->monthly)): ?>
               <td><input type="radio" onclick="subs_amount(<?php echo $featued_image_subscription_info->monthly ?>)" name="subscriptions_type" value="monthly" ></td>
               <td>Monthly</td>
               <td><?php echo $featued_image_subscription_info->monthly ?> USD </td>
           <?php else: ?>
               <td><input type="radio" onclick="subs_amount(250)" name="subscriptions_type" value="monthly" ></td>
               <td>Monthly</td>
               <td>250 $ </td>
           <?php endif; ?>    

             </tr>
         </table>


           <input type="hidden" name="image_id" value="">

           <?php if (!empty($featued_image_subscription_info->daily)): ?>
           <input type="hidden" name="subscriptions_amount" value="<?php echo $featued_image_subscription_info->daily ?>">
           <?php else: ?>
           <input type="hidden" name="subscriptions_amount" value="10">
           <?php endif; ?>    

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-info" value="Pay" >
        </div>
        <?php echo form_close(); ?>
      </div>
      </div>
    </div>
  </div>

    <!-- Model  Ends-->

<style type="text/css">
  .featured_image
  {
    width:150px;
    height: 150px;
    border-radius:4px;
    border:2px;
    box-shadow: 1px 1px 5px #CCCCCC;

  }
  tbody tr td:first-child
  {
    padding-top: 8%;
  }
  th,td
  {
    padding: 15px;
  }
</style>