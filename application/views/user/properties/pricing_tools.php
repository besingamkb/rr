<style type="text/css">
  .tools_head
  {
    background-color: #33AEBD;
    color: white;
    border-radius: 2.5px !important;
    padding:10px;
  }
  .tools_forms
  {
    margin-top: 10%;
    width:100%;

  }
  .contain_all_tools_page
  {
    min-height:504px;
    margin-top: 3%;
  }

</style>


<div  class="contain_all_tools_page">

        <div class="col-md-12 tools_head" style="width:100%">
          Use this tool to test whether the price for this listing is being calculated properly.
          The full pricing breakdown will appear below once you click the 'Test Pricing' button.
        </div>

        <br>

      <!-- Daily pricing Form starts -->
      <div  class="tools_forms">
        <!-- <form class='form-horizontal' id='formdaily'> -->
            <div class="form-group row">
                <label for="inputtext3" class="advance-labels col-md-1 control-label"></label>
                <div class="col-md-11 row">
                    <div class="dailyPriceblock col-md-12 row">
                          <label for="inputtext3" class="advance-labels col-md-2 control-label">Check In</label>
                          <div class="col-md-5">
                              <input type="text" class="form-control" id="tools_start_date" name="start" placeholder="From Date" readonly="">
                          </div>
                    </div>

                    <div class="dailyPriceblock col-md-12 row">
                          <label for="inputtext3" class="advance-labels col-md-2 control-label">Check Out</label>
                          <div class="col-md-5">
                              <input type="text" class="form-control" id="tools_end_date" name="end" placeholder="To Date" readonly="">
                          </div>
                    </div>

                    <div class="dailyPriceblock col-md-12 row">
                          <label for="inputtext3" class="advance-labels col-md-2 control-label">Guest</label>
                          <div class="col-md-5">
                           <select class="form-control" name="no_of_guest" id="no_of_guest">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                           </select>
                          </div>
                    </div>

                    <div class="dailyPriceblock col-md-12 row">
                          <label for="inputtext3" class="advance-labels col-md-2 control-label"></label>
                          <div class="col-md-5">
                          <a href="javascript:void(0)" class="btn btn-info" onclick="get_effective_pricing()">Test Pricing</a>
                          </div>
                    </div>

                </div>
            </div>

      </div>

      <ul id="show_effective_pricing">
        
      </ul>
</div>


<style type="text/css">
#show_effective_pricing {
    margin-bottom: 10px;
    }
  #show_effective_pricing li {
    border-bottom: 1px solid #CCCCCC;
    list-style: none outside none;
    padding: 15px;
}
</style>

<script type="text/javascript">
  function get_effective_pricing()
  {

    var pr_id = <?php echo $properties->id ?>;
    var check_in   = $('#tools_start_date').val();
    var check_out  = $('#tools_end_date').val();
    var no_of_guest  = $('#no_of_guest').val();
   
      if(check_out!="" && check_in!="")
        {
           $('#show_effective_pricing').html("<img src='<?php echo base_url() ?>assets/img/loading-blue.gif'>");

            $.ajax({
                    type: 'POST',
                    data:{check_in:check_in, check_out:check_out, pr_id:pr_id,no_of_guest:no_of_guest},
                    url: '<?php echo base_url(); ?>user/check_pricing_tools',
                    success: function(res)
                    {
                        $('#show_effective_pricing').html("");
                        $('#show_effective_pricing').append(res);

                    }
              });
        }

  }
</script>

<script type="text/javascript">
$(function() {

  $( "#tools_start_date" ).datepicker({
      // defaultDate: "+1w",
      minDate: 1,
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#tools_end_date" ).datepicker( "option", "minDate", selectedDate );
      }
  });

  $( "#tools_end_date" ).datepicker({
      // defaultDate: "+1w",
      minDate: 1,
      changeMonth: true,
      numberOfMonths: 1,      
      onClose: function( selectedDate ) {
        $( "#tools_start_date" ).datepicker( "option", "maxDate", selectedDate );
      }
  });

});
</script>
