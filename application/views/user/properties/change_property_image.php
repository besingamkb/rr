<?php $this->load->view('user/leftbar'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/drag_drop_user_pic/css/drag_drop.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/drag_drop_user_pic/css/web_cam_and_edit_profile.css" />
<script src="<?php echo base_url() ?>assets/drag_drop_user_pic/js/jquery.filedrop.js"></script>
 <style>
 a
 {
   text-decoration: none !important;
 }
#button123
{
  border-color: black;
  border-radius: 3px;
  padding: 5px 8px;
  cursor: pointer;
  font-weight:lighter;
  font-size: 10pt;
  color: black;
  max-width: 251px;
}
#dropbox{
   margin-top:0px !important;
   margin-left:6% !important;
   border-top:0px !important;
   background-color:#EEEEEE;
   float: left;
}
.message{
   padding-top: 40px !important;
}

.browse_box{
   margin-top:0px !important;
   margin-left:6% !important;
   margin-bottom: 10% !important;
   width: 50%;
   border-radius: 4px;
   background-color: #EEEEEE;
   padding-top: 20px;
   padding-bottom: 20px;
   box-shadow: 0 0 4px rgba(0, 0, 0, 0.3) inset, 0 -3px 2px rgba(0, 0, 0, 0.1);
}
#optionsContainer{
   margin-left: 5% !important;
   padding:4px !important;
   padding-top: 30px !important;
   float: left !important;
}
</style>
 
      <div class="col-lg-9" style="position:inherit">
        <div class="row content-top">
          <div class="welcome">
            <h3>Change Property Image</h3>
          </div>
             <?php if($this->session->flashdata('error_msg')){ ?>
               <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
             <br>
             <?php } ?>
             <?php if($this->session->flashdata('success_msg')){ ?>
                 <span align="center" style="padding:5px !important; width:400px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
             <br>
             <?php } ?>
             <div class="sub-header">
                <a class="btn" href="<?php echo base_url(); ?>user/manage_listing/<?php echo $properties->id ?>">Manage listing</a>  
                <a class="btn" href="<?php echo base_url() ?>user/address_description/<?php echo $properties->id ?>"> Address and Description </a>  
                <a class="btn" style="color:#777777" href="">Photos</a>  
                <a class="btn" href="<?php echo base_url() ?>user/calendar/<?php echo $properties->id ?>">Calender</a>  
                <a class="btn" href="<?php echo base_url() ?>user/price_terms/<?php echo $properties->id ?>">Price and terms</a>  
                <a class="btn" href="<?php echo base_url() ?>user/featured_images/<?php echo $properties->id ?>">Featured Images</a>  
             </div>
          <div class = 'form-horizontal no-margin well' style="height:400px;background-color:white">
              <div style="margin-left:6%">
                <a href="<?php echo base_url() ?>user/see_all_property_images/<?php echo $properties->id ?>">See All Images</a>
             </div>
             <br>


   <!-- Darg Drop Box Starts  -->
          <div id="dropbox"  >
             <span class="message">
                 <h4  style="color:#8F8F8F !important">
                    Drop images/video  here to upload. 
                 </h4>
                 <br>
                 <br>
                 <img  src="<?php echo base_url()?>assets/drag_drop_user_pic/images/smallUploadArea.png" style="border-radius:5px; width:160px; height:100px;">
              </span>
              <input type="hidden" id="property_id" value="<?php echo $properties->id ?>">
          </div> 
   <!-- Darg Drop Box Ends  -->

   <!-- Browse Div Starts-->
        <div align="center" id="bro" class="browse_box" style="display:none;float:left !important"> 
           <br>
          <?php echo form_open_multipart(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
                <input type="file" name="property_file" id="button123" >
                <br>
                <button type="submit" class="btn btn-info" >Add Image</button>
          <?php echo form_close(); ?>
        </div>
   <!-- Browse Div Ends-->



   <!-- toggle box for drag drop  and browse images  starts -->
     <div  id="optionsContainer">
      <!-- Upload a picture starts -->
         <div class="otherOption swap" id="upl" style="display: block;">
           <div class="optionIcon"><div class="optionImg" id="uploadOption"></div></div>
           <span style="font-size:14px">Upload a file from your computer</span>
         </div>
         <div class="otherOption swap" id="dd" style="display: none;">
           <div class="optionIcon"><div class="optionImg" id="ddOption"></div></div>
           <span style="font-size:14px">Drag &amp; drop a new profile photo</span>
         </div>
      <!-- Upload a picture Ends -->
    </div>
<!-- toggle box for drag drop  and browse  images Ends -->

</div>

        </div>
      </div>
   <!-- /.container -->
<script>
   $(document).ready(function(){
      $('#upl').click(function(){
        $('#bro').show(); 
        $('#dropbox').hide(); 
        $('#upl').hide();
        $('#dd').show();
      });
      $('#dd').click(function(){
        $('#bro').hide(); 
        $('#dropbox').show(); 
        $('#dd').hide();
        $('#upl').show();
      });
   });
</script>

<!-- Script.js file drag and drop starts -->
<script>

$(function(){
  
  var dropbox = $('#dropbox'),
    message = $('.message', dropbox);
  var property_id = $('#property_id').val();
  
  dropbox.filedrop({
    // The name of the $_FILES entry:
    paramname:'pic',
    maxfiles: 50,
    maxfilesize: 10,
    data:{property_id:property_id},
    url: '<?php echo base_url() ?>user/ajax_property_drag_drop/',
    allowedfiletypes: ['image/jpeg','image/png','image/gif', 'video/mp4'],   // filetypes allowed by Content-Type.  Empty array means no restrictions
    allowedfileextensions: ['.jpg','.jpeg','.png','.gif','.mp4'], // file extensions allowed. Empty array means no restrictions
    
    uploadFinished:function(i,file,response){
      $.data(file).addClass('done');

      // response is the JSON object That user controller returns
      // alert(response.status);
      // response is the JSON object That user controller returns
    },
    
      error: function(err, file) {
      switch(err) {
        case 'BrowserNotSupported':
          showMessage('Your browser does not support HTML5 file uploads!');
          break;
        case 'TooManyFiles':
          alert('Too many files! Please select 5 at most! (configurable)');
          break;
        case 'FileTooLarge':
          alert(file.name+' is too large! Please upload files up to 2mb (configurable).');
          break;
        default:
          break;
      }
    },
    
    // Called before each upload is started
    // beforeEach: function(file){
    //   if(!file.type.match(/^image\//)){
    //     alert('Only images are allowed!');
        
    //     // Returning false will cause the
    //     // file to be rejected
    //     return false;
    //   }
    // },
    
    uploadStarted:function(i, file, len){
      createImage(file);
    },
    
    progressUpdated: function(i, file, progress) {
      $.data(file).find('.progress').width(progress);
    }
       
  });
  
   var template = '<div class="preview">'+
                  '<span class="imageHolder">'+
                     '<img />'+
                     '<span class="uploaded"></span>'+
                  '</span>'+
                  '<div class="progressHolder">'+
                     '<div class="progress"></div>'+
                  '</div>'+
               '</div>'; 
  
  
  function createImage(file){

    var preview = $(template), 
      image = $('img', preview);
      
    var reader = new FileReader();
    
    image.width = 100;
    image.height = 100;
    
    reader.onload = function(e){
      
      // e.target.result holds the DataURL which
      // can be used as a source of the image:
      
      image.attr('src',e.target.result);
    };
    
    // Reading the file as a DataURL. When finished,
    // this will trigger the onload function above:
    reader.readAsDataURL(file);
    
    message.hide();
    preview.appendTo(dropbox);
    
    // Associating a preview container
    // with the file, using jQuery's $.data():
    
    $.data(file,preview);
  }

  function showMessage(msg){
    message.html(msg);
  }

});
</script>
<!-- Script.js file drag and drop Ends -->   



