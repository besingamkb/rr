<?php $this->load->view('user/leftbar'); ?>
<?php
/*
  @! Calendar implementation using PHP and jQuery
----------------------------------------------------------------------------- 
  # author: @akshitsethi
  # web: http://www.akshitsethi.me
  # email: ping@akshitsethi.me
  # mobile: (91)9871084893
-----------------------------------------------------------------------------
  @@ The biggest failure is failing to try.
  var d = $('#calendar').fullCalendar('getDate');
    alert("The current date of the calendar is " + d);
*/
  $calendar = base_url().'assets/event_calendar/';
?>

<link href="<?php echo $calendar; ?>css/calendar.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $calendar; ?>js/jquery.calendar.js"></script>
<script type="text/javascript" src="<?php echo $calendar; ?>js/docs.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
  $('#error_alert').hide();
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  var h = {
              left:   'title',
              center: '',
              right:  ''//'today prev,next'
          };
  $("#calendar").fullCalendar({
    header: h,
    selectable: true,
    select: function( startDate, endDate, allDay, jsEvent, view ) {
      $('#error_alert').hide();
      $('#book_error').html('');
      var newstart = $.fullCalendar.formatDate(startDate, "MMMM dd yyyy h:mm tt");
      var newend = $.fullCalendar.formatDate(endDate, "MMMM dd yyyy h:mm tt");
      var st_date = new Date(newstart);
      var en_date = new Date(newend);
      var cal_date = $("#calendar").fullCalendar('getDate');
      var cal_m = cal_date.getMonth();
      var st_m = st_date.getMonth();
      var en_m = en_date.getMonth();
      var pr_id = <?php echo $properties->id ?>;
      // console.log(cal_m);
      // return false;
      if ((cal_m == st_m) && (cal_m == en_m)) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url() ?>user/check_booking_dates",
          data: { start: newstart, end: newend,property_id:pr_id},
          success: function(response) {

            var obj = jQuery.parseJSON(response);
            // console.log(obj);
            if (obj.status) 
            {
              // console.log( obj.msg );
              $('#error_alert').show();
              $('#book_error').html(obj.msg);
            } 
            else
            {
              $("#start").html('<strong>Start </strong><br/><span>'+newstart+'</span>');
              $("#end").html('<strong>End </strong><br/><span>'+newend+'</span>');
              // $("#confirm").fadeIn();
              $('#add_event').modal();
            };
            
          }
        });
      }else{
        $('#error_alert').show();
        $('#book_error').html("Invalid date selection, can't process.");
      }
    },
    events: {
          url: "<?php echo base_url().'user/get_events/' ?>",
          type: 'POST',
          data: function() { // a function that returns an object
          var date = $("#calendar").fullCalendar('getDate');
          var month_int = date.getMonth();
          var year = date.getFullYear();
          var property_id = <?php echo $properties->id ?>;
              return {
                  month: month_int,
                  year: year,
                  property_id : property_id 
              };
          },
            // isLoading: function (isLoading, view) {
            //     if (isLoading) {
            //      $('#loading').show();
            //     } else{
            //      $('#loading').hide();
            //     };
            // },
          error: function () {
                alert('There is an error in loading calendar data...!');
            }
      },
    // events: "<?php echo base_url().'user/get_events/' ?>"+ m,
    editable: false,
    draggable: false,
    droppable: false
  });

  $('#selectMonth').on('change', function (e) {
      var gotoDate = new Date($(this).val());
      $('#calendar').fullCalendar('gotoDate', gotoDate.getFullYear(), gotoDate.getMonth(), gotoDate.getDate());
  });
  
  $('#add_event').on('hidden.bs.modal', function (e) {
    $("#confirm").fadeIn();
  });

  $('#add_event').on('show.bs.modal', function (e) {
    $("#error_alert").hide();
  });

  // $('#calendar').fullCalendarfunction( isLoading, view );
  $(document).on("click", "#confirm", function() {
    // $('#loading').hide();
    var start = $("#start span").html();
    var end = $("#end span").html();
    var eve_type = $("#eve_type").val();
    var eve_price = $("#eve_price").val();
    var listing_id = <?php echo $properties->id ?>;
    var currency_type = '<?php echo $properties->currency_type ?>';
    if (eve_price == '') {
      eve_price = 0;
    };
    $.ajax({
      type: "POST",
      // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
      // url: "<?php echo base_url() ?>user/add_event",
      url: "<?php echo base_url() ?>user/ajaxSetDailyPrice",
      data: { 
                start: start,
                end: end,
                eve_type: eve_type,
                eve_price: eve_price,
                listing_id: listing_id,
                currency_type: currency_type,
              },
      success: function(response) {

        var obj = jQuery.parseJSON(response);
        // console.log(obj);
        if (obj.status) 
        {
          $("#confirm").fadeOut();
          $("#response").html(obj.msg);
          $("#calendar").fullCalendar( 'refetchEvents' )
          setTimeout(function()
            {
             $('#add_event').modal('hide');
            },2000)
        } 
        else
        {
          $("#confirm").fadeOut();
          $("#response").html(obj.msg);
          $("#calendar").fullCalendar( 'refetchEvents' )
        };
        
      }
    });
  });

});
</script>


      <div class="col-lg-9" style="position:inherit">
        <div class="row content-top">
          <div class="welcome">
            <h3>Calendar</h3>
          </div>
             <div class="sub-header">
               <a class="btn" href="<?php echo base_url(); ?>user/manage_listing/<?php echo $properties->id ?>">Manage listing</a>  
                <a class="btn" href="<?php echo base_url() ?>user/address_description/<?php echo $properties->id ?>">Address and Description</a>  
                <a class="btn" href="<?php echo base_url() ?>user/change_property_image/<?php echo $properties->id ?>">Photos</a>  
                <a class="btn" style="color:#777777" href="javascript:void(0)">Calendar</a>  
                <a class="btn" href="<?php echo base_url() ?>user/price_terms/<?php echo $properties->id ?>">Price and terms</a>  
                <a class="btn" href="<?php echo base_url() ?>user/featured_images/<?php echo $properties->id ?>">Featured Images</a>  
             </div>

        <!-- Box shadow div Starts -->
<style type="text/css">
  .calender-upper-box
  {
   box-shadow:0.4px 0.4px 2px #B9B9B9 inset;
   padding:10px; 
   }
   .calendar
   {
    margin-top: 3%;
    margin-bottom: 3%;

   }
   .fc-event-inner
   {
    height: 50px !important;
   }
   .fc-event-title
   {
    font-size: 16px;
    font-weight: lighter;
   }
   .fc-event-container{
    z-index: 0 !important;
   }
</style>
<script type="text/javascript">
  
  function update_calender(pr_id)
  {
    $.ajax({
             type:'post',
             url:'<?php echo base_url() ?>user/update_calender/'+pr_id,
             success:function(res)
             {
              $('#update_calender_msg').html(res);
             }
          });
  }
</script>


              <div class="col-md-12 calender-upper-box">
                  <?php /* <div class="col-md-12" style="margin-top:1%;height:45px;">
                    <div class="col-md-8">
                      <?php if (!empty($properties->update_calender)): ?>
                          <?php if(get_time_ago($properties->update_calender)=="Just Now"): ?>
                             <p id="update_calender_msg">Last Updated few seconds before </p>                    
                          <?php else: ?>
                             <p id="update_calender_msg">Last Updated <?php echo get_time_ago($properties->update_calender)?></p>                    
                          <?php endif; ?>
                        <?php else: ?>
                             <p id="update_calender_msg"></p>                    
                      <?php endif ;?>
                    </div>
                    <div class="col-md-4">
                       <a class="btn btn-success" onclick="update_calender(<?php echo $properties->id ?>)" style="margin-left:39%;padding:4px 10px">Update Calender</a>
                    </div>
                   </div> */ ?>
                    <div class="col-md-12 row" style="margin-top:1%;height:45px;">
                        <div class="col-md-12 row">
                            <div class="col-md-8">
                              <?php if (!empty($properties->update_calender)): ?>
                                  <?php if(get_time_ago($properties->update_calender)=="Just Now"): ?>
                                     <p id="update_calender_msg">Last Updated few seconds before </p>                    
                                  <?php else: ?>
                                     <p id="update_calender_msg">Last Updated <?php echo get_time_ago($properties->update_calender)?></p>                    
                                  <?php endif; ?>
                                <?php else: ?>
                                     <p id="update_calender_msg"></p>                    
                              <?php endif ;?>
                            </div>
                            <div class="col-md-4">
                               <a class="btn btn-success" onclick="update_calender(<?php echo $properties->id ?>)" style="margin-left:39%;padding:4px 10px">Update Calendar</a>
                            </div>
                        </div>
                        <div class="col-md-12 row">
                            <select name="month" id="selectMonth" style="width:165px !important; margin-left:2%;"class="form-control">
                                <!-- <option value="">Month</option> -->
                                <?php 
                                    $start = time();
                                    $a = getdate($start);
                                    $end = mktime(0,0,0,$a['mon'],$a['mday'],$a['year'] + 5 );
                                ?>
                                <?php 
                                    $i=1; 
                                    while ($start <= $end) { 
                                ?>
                                    <option <?php if(date('F Y', $start) == date('F Y')){ echo 'selected="selected"';} ?> value="<?php echo date('j F Y', $start); ?>"> <?php echo date("F Y", $start); ?> </option>
                                <?php
                                      $start = mktime(0,0,0,$a['mon']+$i,$a['mday'],$a['year']);
                                      $i++;
                                      if ($i > 99) {
                                        break;
                                      }
                                    } 
                                ?>
                            </select>
                        </div>
                   </div>

                 <!-- Calender Box Starts  -->
                    <div class="col-md-9 pull-left calendar">
                      <div id="calendar"></div>
                    </div>
                 <!-- Calender Box Ends  -->
                  <div class="pull-left calendar" style="margin-left:4%">
                       <div style="width:20px;height:20px;background-color:#a10;float:left"></div><div style="float:left;margin-left:6px">Booked</div><br><br>
                       <div style="width:20px;height:20px;background-color:#819f2a;float:left"></div><div style="float:left;margin-left:6px">Available</div><br><br>
                       <div style="width:20px;height:20px;background-color:#666;float:left"></div><div style="float:left;margin-left:6px">Unavailable</div><br><br>
                  </div>


              <!-- Calender Model Starts -->
                <div class="modal fade" id="add_event">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Change Avaibility</h4>
                      </div>
                      <div class="modal-body">
                        <p id="start"><strong>Start</strong></p>
                    <p id="end"><strong>End</strong></p><br/>

                    <div class="input-group" id="event_price">
                      <span class="input-group-addon">$</span>
                      <input name="price" id="eve_price" type="text" class="form-control" placeholder="Enter amount for available dates.(Optionally)">
                      <span class="input-group-addon"><?php if(!empty($properties->currency_type)) echo $properties->currency_type ?></span>
                    </div>
                    <br>
                    <select name='eve_type' id="eve_type" class="form-control">
                       <option value="0">Unavailable</option>
                       <option value="1">Available</option>
                    </select>
                    <input type="hidden" value="Confirm" id="confirm" class="btn btn-default">

                    <br/><div id="response"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" value="Confirm" id="confirm" class="btn btn-primary">Save changes</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            <!-- Calender Model Ends -->

        </div>
        <!-- Box shadow div end -->

      </div>

      <div class="row" style="margin-left:15px;">
        <div class="col-md-12 calender-upper-box">
      <div class="calsync-export box">
        <h2><span class="edit_room_icon calsync"></span>Sync Calendars <a href="#" class="show_div" style="float:right;font-size: 12px;font-weight: normal;padding-top: 2px;">Show sync and export settings</a><a href="#" class="hide_div" style="float:right;font-size: 12px;font-weight: normal;padding-top: 2px;display:none;">Hide sync and export settings</a></h2>
        
        <div class="ical_content" style="padding:20px 20px 20px 45px;display:none;">
          <div class="box" style="margin-top:20px;">
            <h2 style="border: none;background: transparent;padding: 0;"><span class="edit_room_icon calexport"></span>Export Calendar</h2>
            <div class="padded-text" style="padding-left:36px;">
              You can also <strong>export the calendar data</strong> into calendars that support the ICAL format. Copy and paste this link into your calendar:
            <div class="ical_link">
              <a href="<?php echo base_url() ?>user/ical/<?php echo $properties->id ?>" ><?php echo base_url() ?>user/ical/<?php echo $properties->id ?></a>
            </div>

          </div>
      
      
        </div>
        </div>

      </div>
      </div>
      </div>

   </div>
   <!-- /.container -->




<script type="text/javascript">
  $(document).on('click', '.show_div', function(event){
    event.preventDefault();
    $(this).hide();
    $('.ical_content').show();
    $('.hide_div').show();
  });

  $(document).on('click', '.hide_div', function(event){
    event.preventDefault();
    $(this).hide();
    $('.show_div').show();
    $('.ical_content').hide();
  });
</script>

