      <?php $tool_tip_json = get_row('tool_tip',array('tool_tip_page'=>'tool_on_basic_pricing')); ?>
      <?php $tool_tip = json_decode($tool_tip_json->page_data); ?>


 <div class="widget no-margin">
    <?php echo form_open_multipart(current_url(), array('id' => 'testr' ,'style'=>'height:1400px', 'class' => 'form-horizontal no-margin well','onsubmit'=>'get_lat_long();')); ?>
          <h3 class="h2head">Basic Pricing</h3>
              <table class="pull-left">
                  <tr><td>&nbsp;</td></tr>    
                  <tr>
                      <th>Currency Type
                          <?php if(!empty($tool_tip->currency_type)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->currency_type ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                        <?php $currency_array = currency_array(); ?> 
                        <select class="chzn-single all_input_type"  name="currency_type" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                            <option value=""> Select Currency </option>
                            <?php if($currency_array): ?>
                                <?php foreach ($currency_array as $row): ?>
                                <option value="<?php echo $row; ?>"  <?php if(!empty($properties->currency_type)){ if($properties->currency_type == $row) echo"selected"; }  ?> > <?php echo $row; ?> </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                          <span class="form_error span12"><?php echo form_error('currency_type'); ?></span>
                      </td>
                  </tr>

                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Nightly
                          <?php if(!empty($tool_tip->nightly)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->nightly ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td class="row">
                          <input style="width:544px;border:1px solid #ACACAC !important;border-radius: 4px; " name="nightly" id="nightly" class="input-block-level form-control col-md-6  " type="text" placeholder="Per Night Price" value="<?php echo $properties->application_fee;?>">
                          <span class="form_error span12"><?php echo form_error('nightly'); ?></span>
                      </td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Weekly
                          <?php if(!empty($tool_tip->weekly)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->weekly ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <input style="width:544px;border:1px solid #ACACAC !important;border-radius: 4px; " name="weekly" class="input-block-level form-control  " type="text" placeholder="Per Week Price" value="<?php if(set_value('weekly')){echo set_value('weekly');}else{echo $properties->weekly;} ?>">
                          <span class="form_error span12"><?php echo form_error('weekly'); ?></span>
                      </td>
                      <?php $rec_weekly = get_recomended_price('weekly', $properties->application_fee) ?>
                      <?php if($rec_weekly): ?>
                          <td>
                            We recommend <strong id="pr_weekly"><?php echo $rec_weekly; ?></strong> based on your nightly price
                          </td>
                      <?php endif; ?>
                  </tr>
                  <tr><td>&nbsp;</td></tr>

                  <tr>
                      <th>Monthly
                          <?php if(!empty($tool_tip->monthly)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->monthly ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <input style="width:544px;border:1px solid #ACACAC !important;border-radius: 4px; " name="monthly" class="input-block-level form-control  " type="text" placeholder="Per Month Price" value="<?php if(set_value('monthly')){echo set_value('monthly');}else{echo $properties->monthly;} ?>" >
                          <span class="form_error span12"><?php echo form_error('monthly'); ?></span>
                      </td>
                      <?php $rec_monthly = get_recomended_price('monthly', $properties->application_fee) ?>
                      <?php if($rec_monthly): ?>
                          <td>
                            We recommend <strong id="pr_monthly"><?php echo $rec_monthly; ?></strong> based on your nightly price
                          </td>
                      <?php endif; ?>
                  </tr>
                  <tr><td>&nbsp;</td></tr>

                  <tr>
                      <th>Sublet</th>
                      <td>&nbsp;</td>
                      <td>
                        <?php if($is_sublet == 1): ?>
                           <?php $checked = 'checked="checked"'; ?>
                        <?php else: ?>
                           <?php $checked = ''; ?>
                        <?php endif; ?>
                          <input id="is_sublet" <?php echo $checked; ?> type="checkbox" name="is_sublet"  value="1"><br>If you want to lease your place while you're away, you can set up a one-time sublet for a specific time period. Outside this time period, all your settings will stay the same.
                          <br><br>
                      </td>
                  </tr>

                      <tr class="sublet">
                        <th style="vertical-align: text-top;" >Start Date</th>
                        <td>&nbsp;</td>
                        <td>
                            <!-- <input type="text" name="sublet_start"  value="1"> -->
                            <input  type="text" id="sublet_start" name="sublet_start" style="width:218px" class="span3 form-input input-block-level form-control"  placeholder="mm/dd/yyyy" readonly="readonly">
                            <span class="form_error span12"><?php echo form_error('sublet_start'); ?></span>
                            <br><br>
                        </td>
                      <tr class="sublet">
                        <th style="vertical-align: text-top;" >End Date</th>
                        <td>&nbsp;</td>
                        <td>
                            <!-- <input type="text" name="sublet_end"  value="1"> -->
                            <input  type="text"  id="sublet_end" name="sublet_end" style="width:218px" class="span3 form-input input-block-level form-control"  placeholder="mm/dd/yyyy" readonly="readonly">
                            <span class="form_error span12"><?php echo form_error('sublet_end'); ?></span>
                            <br><br>
                        </td>
                      </tr>
                      <tr class="sublet">
                        <th style="vertical-align: text-top;">Sublet Price</th>
                        <td>&nbsp;</td>
                        <td>
                            <input style="width:544px;border:1px solid #ACACAC !important;border-radius: 4px; " name="sublet_price" value="<?php if(set_value('sublet_price')){echo set_value('sublet_price');}else{echo $properties->sublet_price;} ?>" class="input-block-level form-control" type="text" placeholder="Price for sublet">
                            <span class="form_error span12"><?php echo form_error('sublet_price'); ?></span>
                            <br><br>
                        </td>
                      </tr>
                  </div>
              </table>
              <br><br>
              <h3 class="h2head">Additional Costs</h3>

              <table class="pull-left">

                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Additional Guests</th>
                      <td>&nbsp;</td>
                      <td>
                          <input style="width:290px;border:1px solid #ACACAC !important;border-radius: 4px; " name="additional_guest_price" class="input-block-level form-control  " type="text" value="<?php if(set_value('additional_guest_price')){echo set_value('additional_guest_price');}else{echo $properties->additional_guest_price;} ?>">
                          <span class="form_error span12"><?php echo form_error('additional_guest_price'); ?></span>
                      </td>
                      <td>
                        Per night for each guest after
                      </td>
                      <td>
                        <select class="chzn-single all_input_type"  name="additional_guest" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                            <?php for ($i=1; $i < 17; $i++) { ?>
                              <option value="<?php echo $i; ?>"  <?php if(!empty($properties->additional_guest)){ if($properties->additional_guest == $i) echo"selected"; }  ?> > <?php echo $i; ?> </option>
                            <?php } ?>
                        </select>
                          <span class="form_error span12"><?php echo form_error('additional_guest'); ?></span>
                      </td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Cleaning Fees
                          <?php if(!empty($tool_tip->cleaning_fee)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->cleaning_fee ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <input style="width:290px;border:1px solid #ACACAC !important;border-radius: 4px; " name="cleaning_fee" class="input-block-level form-control  " type="text" placeholder="Cleaning fees" value="<?php if(set_value('cleaning_fee')){echo set_value('cleaning_fee');}else{echo $properties->cleaning_fee;} ?>">
                          <span class="form_error span12"><?php echo form_error('cleaning_fee'); ?></span>
                      </td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
              </table>
              <br><br>
              <h3 class="h2head">Terms</h3>

              <table class="pull-left">

                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Cancellation Policy
                          <?php if(!empty($tool_tip->policy)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->policy ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <?php if($can_policies): ?>
                            <select class="chzn-single all_input_type"  name="cancellation_policies" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                <?php foreach ($can_policies as $row): ?>
                                    <option value="<?php echo $row->id; ?>"  <?php if($row->id == $properties->cancellation_policies) { echo"selected"; }  ?> > <?php echo $row->policy_title; ?> </option>
                                <?php endforeach; ?>
                            </select>
                          <?php endif; ?>
                          <span class="form_error span12"><?php echo form_error('cancellation_policies'); ?></span>
                      </td>
                      <td><a href="#">Learn More</a></td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Minimum Stay
                          <?php if(!empty($tool_tip->minimum_stay)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->minimum_stay ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <select class="chzn-single all_input_type"  name="min_stay" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                              <optgroup label="Nights">
                                  <option <?php if($properties->min_stay==1) echo "selected" ?> value="1">1 Night</option>
                                  <option <?php if($properties->min_stay==2) echo "selected" ?> value="2">2 Nights</option>
                                  <option <?php if($properties->min_stay==3) echo "selected" ?> value="3">3 Nights</option>
                                  <option <?php if($properties->min_stay==4) echo "selected" ?> value="4">4 Nights</option>
                                  <option <?php if($properties->min_stay==5) echo "selected" ?>  value="5">5 Nights</option>
                                  <option <?php if($properties->min_stay==6) echo "selected" ?>  value="6">6 Nights</option>
                              </optgroup>
                              <optgroup label="Weeks">
                                  <option <?php if($properties->min_stay==7) echo "selected" ?>  value="7">1 Week</option>
                                  <option <?php if($properties->min_stay==14) echo "selected" ?>  value="14">2 Weeks</option>
                                  <option <?php if($properties->min_stay==21) echo "selected" ?>  value="21">3 Weeks</option>
                              </optgroup>
                              <optgroup label="Months">
                                  <option <?php if($properties->min_stay==30) echo "selected" ?>  value="30">1 Month</option>
                                  <option <?php if($properties->min_stay==60) echo "selected" ?>  value="60">2 Months</option>
                                  <option <?php if($properties->min_stay==90) echo "selected" ?>  value="90">3 Months</option>
                                  <option <?php if($properties->min_stay==120) echo "selected" ?>  value="120">4 Months</option>
                                  <option <?php if($properties->min_stay==150) echo "selected" ?>  value="150">5 Months</option>
                                  <option <?php if($properties->min_stay==180) echo "selected" ?>  value="180">6 Months</option>
                                  <option <?php if($properties->min_stay==210) echo "selected" ?>  value="210">7 Months</option>
                                  <option <?php if($properties->min_stay==240) echo "selected" ?>  value="240">8 Months</option>
                                  <option <?php if($properties->min_stay==270) echo "selected" ?>  value="270">9 Months</option>
                                  <option <?php if($properties->min_stay==300) echo "selected" ?>  value="300">10 Months</option>
                                  <option <?php if($properties->min_stay==330) echo "selected" ?>  value="330">11 Months</option>
                                  <option <?php if($properties->min_stay==360) echo "selected" ?>  value="360">12 Months</option>
                              </optgroup>
                          </select>
                         <span class="form_error span12"><?php echo form_error('min_stay'); ?></span>
                      </td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                      <th>Maximum Stay
                          <?php if(!empty($tool_tip->maximum_stay)): ?>
                            <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->maximum_stay ?>" ></span>
                          <?php endif; ?>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <select class="chzn-single all_input_type"  name="max_stay" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                              <optgroup label="Nights">
                                  <option <?php if($properties->max_stay==1) echo "selected" ?> value="1">1 Night</option>
                                  <option <?php if($properties->max_stay==2) echo "selected" ?> value="2">2 Nights</option>
                                  <option <?php if($properties->max_stay==3) echo "selected" ?> value="3">3 Nights</option>
                                  <option <?php if($properties->max_stay==4) echo "selected" ?> value="4">4 Nights</option>
                                  <option <?php if($properties->max_stay==5) echo "selected" ?>  value="5">5 Nights</option>
                                  <option <?php if($properties->max_stay==6) echo "selected" ?>  value="6">6 Nights</option>
                              </optgroup>
                              <optgroup label="Weeks">
                                  <option <?php if($properties->max_stay==7) echo "selected" ?>  value="7">1 Week</option>
                                  <option <?php if($properties->max_stay==14) echo "selected" ?>  value="14">2 Weeks</option>
                                  <option <?php if($properties->max_stay==21) echo "selected" ?>  value="21">3 Weeks</option>
                              </optgroup>
                              <optgroup label="Months">
                                  <option <?php if($properties->max_stay==30) echo "selected" ?>  value="30">1 Month</option>
                                  <option <?php if($properties->max_stay==60) echo "selected" ?>  value="60">2 Months</option>
                                  <option <?php if($properties->max_stay==90) echo "selected" ?>  value="90">3 Months</option>
                                  <option <?php if($properties->max_stay==120) echo "selected" ?>  value="120">4 Months</option>
                                  <option <?php if($properties->max_stay==150) echo "selected" ?>  value="150">5 Months</option>
                                  <option <?php if($properties->max_stay==180) echo "selected" ?>  value="180">6 Months</option>
                                  <option <?php if($properties->max_stay==210) echo "selected" ?>  value="210">7 Months</option>
                                  <option <?php if($properties->max_stay==240) echo "selected" ?>  value="240">8 Months</option>
                                  <option <?php if($properties->max_stay==270) echo "selected" ?>  value="270">9 Months</option>
                                  <option <?php if($properties->max_stay==300) echo "selected" ?>  value="300">10 Months</option>
                                  <option <?php if($properties->max_stay==330) echo "selected" ?>  value="330">11 Months</option>
                                  <option <?php if($properties->max_stay==360) echo "selected" ?>  value="360">12 Months</option>
                              </optgroup>
                              <optgroup label="Years">
                                    <option <?php if($properties->max_stay==365) echo "selected" ?> value="365">1 Year</option>
                                    <option <?php if($properties->max_stay==730) echo "selected" ?> value="730">2 Years</option>
                                    <option <?php if($properties->max_stay==1095) echo "selected" ?> value="1095">3 Years</option>
                                    <option <?php if($properties->max_stay==1460) echo "selected" ?> value="1460">4 Years</option>
                                    <option <?php if($properties->max_stay==1825) echo "selected" ?> value="1825">5 Years</option>
                                    <option <?php if($properties->max_stay==2190) echo "selected" ?> value="2190">6 Years</option>
                                    <option <?php if($properties->max_stay==2555) echo "selected" ?> value="2555">7 Years</option>
                                    <option <?php if($properties->max_stay==2920) echo "selected" ?> value="2920">8 Years</option>
                                    <option <?php if($properties->max_stay==3285) echo "selected" ?> value="3285">9 Years</option>
                                    <option <?php if($properties->max_stay==3650) echo "selected" ?> value="3650">10 Years</option>
                                    <option <?php if($properties->max_stay==4015) echo "selected" ?> value="4015">11 Years</option>
                                    <option <?php if($properties->max_stay==4380) echo "selected" ?> value="4380">12 Years</option>
                              </optgroup>
                          </select>
                          <span class="form_error span12"><?php echo form_error('max_stay'); ?></span>
                      </td>
                  </tr>
                  <tr>
                      <th>
                      </th>
                      <td>&nbsp;</td>
                      <td>
                          <br>
                          <button type="submit" class="btn btn-info" style="margin-bottom:2%">
                             Save
                          </button>
                      </td>
                  </tr>
              </table>
    <?php echo form_close(); ?>
 </div>
<style type="text/css">
    .sublet{
      padding: 5px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
      <?php if($is_sublet == 0): ?>
        $(".sublet").hide();
      <?php endif; ?>
    });

    $(document).on('click', '#is_sublet',function (argument) {
      var isChecked = $('#is_sublet').prop('checked');
      if (isChecked) {
        $(".sublet").show();
      }else{
        $(".sublet").hide();
      }
    });

    $(document).on('change', '#nightly',function (argument) {
      // alert();
      var nightly = parseFloat($(this).val());

      var weekly = parseFloat(nightly * 7);
      var monthly = parseFloat(nightly * 30);

      $('#pr_weekly').html(weekly);      
      $('#pr_monthly').html(monthly);      

    });

    $(function() {
        $( "#sublet_start" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
              $( "#sublet_end" ).datepicker( "option", "minDate", selectedDate );
            }
        });

        $( "#sublet_end" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,      
            onClose: function( selectedDate ) {
              $( "#sublet_start" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

        <?php if(($properties->sublet_from_date != '0000-00-00 00:00:00') && ($properties->sublet_to_date != '0000-00-00 00:00:00')): ?>
            $( "#sublet_start" ).datepicker( "setDate", "<?php echo date('m/d/Y',strtotime($properties->sublet_from_date)); ?>" );
            $( "#sublet_end" ).datepicker( "setDate", "<?php echo date('m/d/Y',strtotime($properties->sublet_to_date)); ?>" );
        <?php endif; ?>
    });
</script>