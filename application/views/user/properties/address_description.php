<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/uniiverse_dropdown.css" type="text/css" >

<!-- Google Pinpoint Api Starts-->
<!-- Google Pinpoint Api Starts-->
  <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.js"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.ui.min.js"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.ui.addresspicker.js"></script>
  <style type="text/css">
  #map
  {
    border: 1px solid #DDD; 
    height: 300px;
    margin: 10px 0 10px 0;
    -webkit-box-shadow: #AAA 0px 0px 15px;
  } 

  </style>
  
  <div class='map-wrapper'>
    <input type="hidden" id="reverseGeocode" value="true">
  </div> 

  <script>

  function reload_address_map()
  {
    reinitialize();
  }

  function reinitialize()
  {
  $(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
      componentsFilter: 'country:IN'
    });
    var addresspickerMap = $( "#address" ).addresspicker({
      regionBias: "fr",
      updateCallback: showCallback,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(<?php echo $properties->latitude ?>,<?php echo $properties->longitude ?>),
        scrollwheel:true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements: {
        map: "#map",
        lat: "#lat",
        lng: "#lng",
        street_number: '#street_number',
        route: '#route',
        locality: '#city',
        administrative_area_level_2: '',
        administrative_area_level_1: '#state',
        country: '#country',
        postal_code: '#zip',
        type: '#type'
      }
    });

    var gmarker = addresspickerMap.addresspicker( "marker");
    gmarker.setVisible(true);
    addresspickerMap.addresspicker( "updatePosition");

      $("#address").addresspicker("option", "reverseGeocode", ($('#reverseGeocode').val() === 'true'));

    function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
       get_neighbour_city();
    }
    // Update zoom field
    var map = $("#address").addresspicker("map");
    google.maps.event.addListener(map, 'idle', function(){
      $('#zoom').val(map.getZoom());
    });

  });

} 

  </script>


<!-- Google Pinpoint  Api Ends-->
<!-- Google Pinpoint  Api Ends-->

<!-- get address by latitude  and  longtitude Starts-->
<!-- get address by latitude  and  longtitude Starts-->
<script src="<?php echo base_url() ?>assets/pin_point_map/address_by_lat_long.js"></script>
  <script>
 
    // ResponseAddress is a callback function which will be executed after converting coordinates to an address
    
    function find_address_by_lat_long(){
      var latitude  = $('#lat').val();
      var longitude  = $('#lng').val();
     Convert_LatLng_To_Address(latitude,longitude, ResponseAddress);       
    }
 
    /*
    * Response Address
    */
    function ResponseAddress() {
        
        $('#country').val(address['country']);
        $('#city').val(address['city']);
        $('#zip').val(address['postal_code']);
        $('#address').val(address['formatted_address']);
        $('#state').val(address['province']);
        setTimeout(function(){
           get_neighbour_city();
        },2000);

    }
 
  </script>

<!-- get address by latitude  and  longtitude Endss-->
<!-- get address by latitude  and  longtitude Endss-->



<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
.all_input_type
{
border:1px solid #ACACAC !important;
border-radius: 4px; 
width:180px;
}
.all_input_type:hover
{
  cursor: pointer;
} 
th
{
   text-align: right;
}

.h2head{  
 /* border-top: 1px solid #CCC;
  border-bottom: 1px solid #CCC;*/
}
</style>

<?php $this->load->view('user/leftbar'); ?>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Listing Property</h3>
               </div>
               <div class="row-fluid">
                       <div class="sub-header">
                          <a class="btn" href="<?php echo base_url(); ?>user/manage_listing/<?php echo $properties->id ?>">Manage listing</a>  
                          <a class="btn" href="" style="color:#777777"> Address and Description </a>  
                          <a class="btn" href="<?php echo base_url(); ?>user/change_property_image/<?php echo $properties->id ?>">Photos</a>  
                          <a class="btn" href="<?php echo base_url() ?>user/calendar/<?php echo $properties->id ?>">Calender</a>  
                          <a class="btn" href="<?php echo base_url() ?>user/price_terms/<?php echo $properties->id ?>">Price and terms</a>  
                          <a class="btn" href="<?php echo base_url() ?>user/featured_images/<?php echo $properties->id ?>">Featured Images</a>  
                       </div>
                    <div class="span12">
                         <div class="widget no-margin">
               
               <?php alert(); ?>
               

                <?php $tool_tip_json = get_row('tool_tip',array('tool_tip_page'=>'tool_on_update_property')); ?>
                <?php $tool_tip = json_decode($tool_tip_json->page_data); ?>

                            <?php echo form_open_multipart(current_url(), array('id' => 'testr' ,'style'=>'border-bottom:none;background-color:white', 'class' => 'form-horizontal no-margin well','onsubmit'=>'get_lat_long(); return false;')); ?>
                                <h3 class="h2head">Listing Type</h2><hr>
                                <table class="pull-left" style="width:100%">
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr>
                                        <th>Property Type
                                        <?php if(!empty($tool_tip->property_type)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->property_type ?>" ></span>
                                        <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <?php $property_type = get_property_types(); ?> 
                                          <select class="chzn-single all_input_type"  name="property_type" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                              <option value="">Select Property Type</option>
                                              <?php foreach ($property_type as $row): ?>
                                               <?php  if(set_value('property_type')): ?>
                                                  <option value="<?php echo $row->id; ?>" <?php if(set_value('property_type') == $row->id) echo 'selected="selected"'; ?>> <?php echo $row->property_type; ?> </option>
                                                <?php else: ?>
                                                  <option value="<?php echo $row->id; ?>"  <?php if(!empty($pr_info->property_type_id)){ if($pr_info->property_type_id == $row->id)echo"selected"; }  ?> > <?php echo $row->property_type; ?> </option>
                                               <?php endif; ?> 
                                              <?php endforeach ?>
                                          </select>
                                            <span class="form_error span12"><?php echo form_error('property_type'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>
                                        Room Type
                                        <?php if(!empty($tool_tip->room_type)): ?>
                                           <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->room_type ?>" ></span>
                                        <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <?php  $room_type = get_room_type(); ?> 
                                            <select class="chzn-single all_input_type"  name="room_type" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                                <option value="">Select Room Type</option>
                                                <?php foreach ($room_type as $row): ?>
                                                   <?php  if(set_value('room_type')): ?>
                                                      <option value="<?php echo $row->id; ?>" <?php if(set_value('room_type') == $row->id) echo 'selected="selected"'; ?>> <?php echo $row->room_type; ?> </option>
                                                    <?php   else: ?>
                                                       <option value="<?php echo $row->id; ?>" <?php if($pr_info->room_type == $row->id) echo 'selected="selected"'; ?>> <?php echo $row->room_type; ?> </option>
                                                    <?php   endif; ?>
                                                <?php endforeach ?>
                                            </select>
                                            <span class="form_error span12"><?php echo form_error('room_type'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th style="letter-spacing: 1.5px;">Title</th>
                                        <td>&nbsp;</td>
                                        <td>
                                           <?php  if(set_value('title')): ?>
                                              <input style="width:544px;border:1px solid #ACACAC !important;border-radius: 4px; " name="title" class="input-block-level form-control address_title_description"  <?php if(!empty($tool_tip->title)) echo "title = ".$tool_tip->title.""; ?>  type="text" placeholder="property title" value="<?php echo set_value('title');?>">
                                            <?php else: ?>
                                              <input style="width:544px;border:1px solid #ACACAC !important;border-radius: 4px; " name="title" class="input-block-level form-control address_title_description " <?php if(!empty($tool_tip->title)) echo "title = ".$tool_tip->title.""; ?> type="text" placeholder="property title" value="<?php echo $properties->title;?>">
                                          <?php endif; ?>
                                            <span class="form_error span12"><?php echo form_error('title'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Description</th>
                                        <td>&nbsp;</td>
                                        <td>
                                           <?php  if(set_value('description')): ?>
                                               <textarea rows="3" cols="87" class="input-block-level form-control address_title_description " name="description"  style="width:546px !important;padding:10px;border:1px solid #ACACAC !important;border-radius: 4px;" <?php if(!empty($tool_tip->description)) echo "title = ".$tool_tip->description.""; ?> Placeholder="Description"><?php echo set_value('description');?></textarea>
                                            <?php else: ?>
                                               <textarea rows="3" cols="87" class="input-block-level form-control address_title_description " name="description"  style="width:546px !important;padding:10px;border:1px solid #ACACAC !important;border-radius: 4px;" <?php if(!empty($tool_tip->description)) echo "title = ".$tool_tip->description.""; ?> Placeholder="Description"><?php echo $properties->description;?></textarea>
                                           <?php endif; ?>
                                            <span class="form_error span12"><?php echo form_error('description'); ?></span>  
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    </table>   
                                      <h3 class="h2head">Amenities</h2><hr>                                    
                                    <table class="pull-left" style="width:100%" style="width:100%">
                                      <tr>
                                        <th>Amenity
                                        </th>
                                        <td>&nbsp;</td>
                                        <?php if(set_value('amenities')): ?>
                                        <!-- Ameneties after validation fail -->
                                        <td style="padding-left: 4.5%">
                                             <div style="float:left;margin-left:20px">
                                                <?php $amenity = property_amenities(); $i=0; foreach ($amenity as $row){ ?>
                                             <div>
                                                <span class="glyphicon glyphicon-question-sign" title="<?php echo $row->name ?>" ></span>
                                                  <label style="font-weight:lighter">
                                                      <input type="checkbox" name="amenities[]"  <?php echo set_checkbox('amenities',$row->id) ?> value="<?php echo $row->id; ?>" >
                                                      <?php echo  $row->name; ?>
                                                 </label>
                                             </div>
                                            <?php if($i%5==4): ?>
                                             </div>&nbsp;&nbsp;&nbsp;
                                             <div style="float:left;margin-left:20px">
                                           <?php endif; ?>
                                            <?php $i++; } ?>
                                        </td> 
                                        <?php else: ?>
                                          <!-- Ameneties before validation fail -->
                                      <td style="padding-left: 4.5%">
                                        <div style="float:left;margin-left:20px">
                                         <?php $amenity = property_amenities(); $i=0; foreach ($amenity as $row){ ?>
                                      <div>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $row->name ?>" ></span>
                                           <label style="font-weight:lighter">
                                             <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>" 
                                             <?php if(!empty($pr_amenities)){
                                             foreach ($pr_amenities as $chok1){ 
                                             if($chok1->pr_amenity_id == $row->id) echo "checked"; 
                                             } } ?>
                                             > <?php echo  $row->name; ?>
                                          </label>
                                      </div>
                                      <?php if($i%5==4): ?>
                                        </div>&nbsp;&nbsp;&nbsp;
                                        <div style="float:left;margin-left:20px">
                                      <?php endif; ?>
                                      <?php $i++; } ?>
                                      </td>

                                        <?php endif; ?>

                                  <!-- Ameneties End-->

                                    </tr>
                                     <tr>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td><div style="margin-left:50px" class="form_error span12"><?php echo form_error('amenities[]'); ?></div></td>
                                     </tr>
                                    <tr><td>&nbsp;</td></tr>
                                  </table>

                                  <h3>Details</h3><hr>
                                    <table class="pull-left" style="width:100%;">
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Accomodates
                                        <?php if(!empty($tool_tip->accomodates)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->accomodates ?>" ></span>
                                        <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <select required="required" name="accomodates" style="height:35px;padding:8px !important;margin:0px !important;color:#777777" class="chzn-single all_input_type">
                                                <option <?php if($pr_info->accommodates==1) echo "selected" ?>>1</option>
                                                <option <?php if($pr_info->accommodates==2) echo "selected" ?>>2</option>
                                                <option <?php if($pr_info->accommodates==3) echo "selected" ?>>3</option>
                                                <option <?php if($pr_info->accommodates==4) echo "selected" ?>>4</option>
                                                <option <?php if($pr_info->accommodates==5) echo "selected" ?>>5</option>
                                                <option <?php if($pr_info->accommodates==6) echo "selected" ?>>6</option>
                                                <option <?php if($pr_info->accommodates==7) echo "selected" ?>>7</option>
                                                <option <?php if($pr_info->accommodates==8) echo "selected" ?>>8</option>
                                                <option <?php if($pr_info->accommodates==9) echo "selected" ?>>9</option>
                                                <option <?php if($pr_info->accommodates==10) echo "selected" ?>>10</option>
                                                <option <?php if($pr_info->accommodates==11) echo "selected" ?>>11</option>
                                                <option <?php if($pr_info->accommodates==12) echo "selected" ?>>12</option>
                                                <option <?php if($pr_info->accommodates==13) echo "selected" ?>>13</option>
                                                <option <?php if($pr_info->accommodates==14) echo "selected" ?>>14</option>
                                                <option <?php if($pr_info->accommodates==15) echo "selected" ?>>15</option>
                                                <option <?php if($pr_info->accommodates==16) echo "selected" ?>>16+</option>
                                         </select>                              
                                        </td>
                                            <span class="form_error span12"><?php echo form_error('accomodates'); ?></span>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Bed Type
                                        <?php if(!empty($tool_tip->bed_type)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->bed_type ?>" ></span>
                                        <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <?php $bed_type = get_bed_types(); ?> 
                                          <select class="chzn-single all_input_type"  name="bed_type" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                              <option value="">Select Bed Type</option>
                                              <?php foreach ($bed_type as $row): ?>
                                              <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->bed_type)){ if($pr_info->bed_type == $row->id)echo"selected"; }  ?>> <?php echo $row->bed_type; ?> </option>
                                              <?php endforeach ?>
                                          </select>
                                            <span class="form_error span12"><?php echo form_error('bed_type'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Category
                                        <?php if(!empty($tool_tip->category)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->category ?>" ></span>
                                         <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <?php $category_type = get_collection_category(); ?> 
                                          <select class="chzn-single all_input_type"  name="category_type"  style="height:40px;padding:8px !important;margin:0px !important;color:#777777">
                                              <?php foreach ($category_type as $row): ?>
                                              <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->pr_collect_cat_id)){ if($pr_info->pr_collect_cat_id == $row->id)echo"selected"; }  ?>> <?php echo $row->category_name; ?> </option>
                                              <?php endforeach ?>
                                          </select>
                                            <span class="form_error span12"><?php echo form_error('category_type'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Bedrooms
                                        <?php if(!empty($tool_tip->bedrooms)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->bedrooms ?>" ></span>
                                         <?php endif; ?>
                                         </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <select class="chzn-single all_input_type"  name="bedrooms" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                             <option <?php if($pr_info->bed==1) echo "selected" ?>>1</option>
                                             <option <?php if($pr_info->bed==2) echo "selected" ?>>2</option>
                                             <option <?php if($pr_info->bed==3) echo "selected" ?>>3</option>
                                             <option <?php if($pr_info->bed==4) echo "selected" ?>>4</option>
                                             <option <?php if($pr_info->bed==5) echo "selected" ?>>5</option>
                                             <option <?php if($pr_info->bed==6) echo "selected" ?>>6</option>
                                             <option <?php if($pr_info->bed==7) echo "selected" ?>>7</option>
                                             <option <?php if($pr_info->bed==8) echo "selected" ?>>8</option>
                                             <option <?php if($pr_info->bed==9) echo "selected" ?>>9</option>
                                             <option <?php if($pr_info->bed==10) echo "selected" ?>>10</option>
                                          </select>
                                          </select>
                                            <span class="form_error span12"><?php echo form_error('bedrooms'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <th>Bathrooms
                                        <?php if(!empty($tool_tip->bedrooms)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->bathrooms ?>" ></span>
                                        <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                          <select class="chzn-single all_input_type"  name="bathrooms" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                             <option <?php if($pr_info->bath==1) echo "selected" ?>>1</option>
                                             <option <?php if($pr_info->bath==2) echo "selected" ?>>2</option>
                                             <option <?php if($pr_info->bath==3) echo "selected" ?>>3</option>
                                             <option <?php if($pr_info->bath==4) echo "selected" ?>>4</option>
                                             <option <?php if($pr_info->bath==5) echo "selected" ?>>5</option>
                                             <option <?php if($pr_info->bath==6) echo "selected" ?>>6</option>
                                             <option <?php if($pr_info->bath==7) echo "selected" ?>>7</option>
                                             <option <?php if($pr_info->bath==8) echo "selected" ?>>8</option>
                                             <option <?php if($pr_info->bath==9) echo "selected" ?>>9</option>
                                             <option <?php if($pr_info->bath==10) echo "selected" ?>>10</option>
                                          </select>
                                            <span class="form_error span12"><?php echo form_error('bathrooms'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr id="house_rules" >
                                        <th>Size
                                        <?php if(!empty($tool_tip->size)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->size ?>" ></span>
                                         <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:10%;border:1px solid #ACACAC !important; float:left; border-radius: 4px; " name="size" class="input-block-level form-control  " type="text" placeholder="In Square Feet" value="<?php echo $pr_info->square_feet; ?>">
                                            <select style="width:17.6%;border:1px solid #ACACAC !important; border-radius: 4px; " name="sizeMode" class="input-block-level form-control  ">
                                              <option <?php if($pr_info->sizeMode == 'Sq foot'){  echo 'selected'; } ?>   >Sq foot</option>
                                              <option <?php if($pr_info->sizeMode == 'Sqm.'){  echo 'selected'; } ?>  >Sqm.</option>
                                            </select>
                                            <span class="form_error span12"><?php echo form_error('size'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr >
                                        <th>House Rules
                                        <?php if(!empty($tool_tip->house_rules)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->house_rules ?>" ></span>
                                        <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <textarea rows="3" cols="87" class="input-block-level form-control " name="house_rules"  style="width:546px !important;padding:10px;border:1px solid #ACACAC !important;border-radius: 4px;" Placeholder="House Rules"><?php echo $pr_info->house_rules ?></textarea>
                                            <span class="form_error span12"><?php echo form_error('house_rules'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>                                    
                                    <tr>
                                        <th>House Manual
                                        <?php if(!empty($tool_tip->house_manual)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->house_manual ?>" ></span>
                                         <?php endif; ?>
                                        </th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <textarea rows="3" cols="87" class="input-block-level form-control " name="house_manual"  style="width:546px !important;padding:10px;border:1px solid #ACACAC !important;border-radius: 4px;" Placeholder="House Manual"><?php echo $pr_info->house_manual ?></textarea>
                                            <span class="form_error span12"><?php echo form_error('house_manual'); ?></span>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>                                    
                                  </table>
                                    <h3>Location Information
                                        <?php if(!empty($tool_tip->location_information)): ?>
                                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->location_information ?>" ></span>
                                         <?php endif; ?>
                                    </h3>
                                    <hr>
                                  <table class="pull-left" style="width:100%">
                               <!-- Postal Type and GPS Co ordinate and Pin-point links on Map Starts -->
                                  <style type="text/css">
                                   a{
                                     text-decoration: none !important;
                                    }
                                    .GPS_type{
                                     display: none;
                                     }
                                    .pin_point_type
                                    {
                                     display: none;
                                    }
                                  </style>
                                  <script>
                                  $(document).ready(function(){
                                     $('#post_type').click(function(){
                                           $('.postal_type').show();
                                           $('.GPS_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#post_type').css('color','#DE4980');
                                           $('#gps_co').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');
                                     });
                                     $('#gps_co').click(function(){
                                           $('.GPS_type').show();
                                           $('.postal_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#gps_co').css('color','#DE4980');
                                           $('#post_type').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');
                                     });
                                     $('#pin_co').click(function(){
                                           $('.pin_point_type').show();
                                           $('.GPS_type').hide();
                                           $('.postal_type').hide();
                                           $('#pin_co').css('color','#DE4980');
                                           $('#post_type').css('color','#5885AC');
                                           $('#gps_co').css('color','#5885AC');
                                     });
                                  });
                                  </script>
                                    <tr>
                                     <a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div   style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" style="color:#DE4980" id="post_type" >Postal Type</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="gps_co">GPS Co-ordinate</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="pin_co"  onclick="reload_address_map()">Pin Point On Map</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                </div>
                                            </div>
                                        </td>
                                     </a>
                                    </tr>
                               <!-- Postal Type and GPS Co ordinate and Pin-point links on Map Ends -->
                               
                               <!-- Postal Type Division Starts -->
                               <!-- Postal Type Division Starts -->
                               <!-- Postal Type Division Starts -->
                               
                                <div >
                                    <tr class="postal_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:85%" name="address" id="address" class="input-block-level form-control" type="text" placeholder="Property Address" value="<?php echo $pr_info->unit_street_address ?>">                 
                                            <span class="form_error span12"><?php  echo form_error('address');  ?></span> 
                                        </td>
                                    </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%;">
                                            <input onkeyup="get_neighbour_city()" style="width:37%;float:left;margin-right:12.5%" name="city" id="city" class="input-block-level form-control" type="text" placeholder="City" value="<?php echo $pr_info->city ?>">                 
                                            

                                            <span><?php echo form_error('city'); ?></span> 

                                                <script>

                                                onload = get_neighbour_city(); 

                                                function get_neighbour_city()
                                                {
                                                   var city_name = $('input[name=city]').val();
                                                   if(city_name!="")
                                                   {
                                                     $.ajax({
                                                        type:"post",
                                                        url:"<?php echo base_url() ?>user/get_neighbour_city",
                                                        data:{city_name:city_name},
                                                        success:function(res)
                                                        {
                                                          if(res!="")
                                                          {
                                                            get_neighbourhoot(res);
                                                          }
                                                          else
                                                          {
                                                             $('#neighbourhoot').html("<option>No Neighborhoods Found</option>")
                                                          }
                                                        }

                                                     });
                                                   }
                                                   else
                                                   {
                                                    return false; 
                                                   }
                                                }
                                    

                                                   function get_neighbourhoot(city_id)
                                                    {
                                                        var city_id = city_id ;
                                                        $.ajax({
                                                                type:'POST',
                                                                 // url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city01/<?php echo $properties->id; ?>',
                                                                 url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city_on_edit_page',
                                                                data:{city:city_id,property_id:<?php echo $properties->id ?>},
                                                             success:function(res)
                                                             {
                                                                $('#neighbourhoot').html(res);
                                                             }
                                                        });
                                                    }


                                               </script>                                               
                                                <div  style="width:50%; float:left;">
                                                    <?php $country = get_country_array(); ?>
                                                    <select class="chzn-single all_input_type" style="width:71%; height:40px; padding:10px;color:#777777" id="country" name="country">
                                                        <option  value="">Select Country</option>
                                                        <?php foreach ($country as $code => $name): ?>
                                                        <option value="<?php echo $name; ?>" <?php if(!empty($pr_info->country)){ if($pr_info->country == $name)echo"selected"; }  ?> > <?php echo $name; ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span class="form_error "><?php echo form_error('country'); ?></span>
                                                </div>

                                                 <div  style="width:100%; float:left;">
                                                    <select class="chzn-single all_input_type" name="neighbourhood[]" id="neighbourhoot"  style="height:75px;width:85.5%;margin-top:5%;padding:8px !important;margin-left:0px !important;color:#777777" multiple="multiuple">
                                                        <option>Select Neighborhoods</option>
                                                    </select>
                                                    <span><?php echo form_error('neighbourhood'); ?></span>
                                                </div>
                                            
                                            </div>
                                        </td>
                                     </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div style="width:40%; float:left;">
                                                    <input name="state" class="input-block-level form-control" id="state" type="text" placeholder="State" value="<?php echo $pr_info->state ?>">
                                                    <span class="form_error"><?php echo form_error('state'); ?></span>
                                                </div>
                                                <div style="width:40%; float:left;margin-left:5%">
                                                    <input class="input-block-level form-control" type="text" placeholder="Postal Code" name="zipcode" id="zip" value="<?php echo $pr_info->zip ?>">
                                                    <span class="form_error span12"><?php echo form_error('zipcode'); ?></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                               </div>
                         <!-- Postal Type Division Ends -->                                  
                         <!-- Postal Type Division Ends --> 

                         <!-- GPS Type Division Starts -->
                         <!-- GPS Type Division Starts -->
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px"  id="lat" class="input-block-level form-control" type="text" placeholder="Latitude" onkeyup="find_address_by_lat_long()" >                 
                                            <span class="form_error span12"><?php  echo form_error('address');  ?></span> 
                                        </td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                     <td>Examples: <code>37N 46' 29.74" or +37.7749295</code></td>
                                    </tr>
                                    <tr class="GPS_type"></tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px"  id="lng" class="input-block-level form-control" type="text" placeholder="Longitude" onkeyup="find_address_by_lat_long()" >                 
                                        </td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                     <td>Example: <code>122W 25' 9.89" or - 123.41494155</code></td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                         <!-- GPS Type Division Ends -->                                  
                         <!-- GPS Type Division Ends --> 

                         <!-- Pin Point Maps Division Ends -->                                  
                         <!-- Pin Point Maps Division Ends -->                                  
                                    <tr class="pin_point_type"><td>&nbsp;</td></tr>
                                    <tr class="pin_point_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td ><div id="map"></div></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="pin_point_type"><td>&nbsp;</td></tr>
                         <!-- Pin Point Maps Division Ends -->                                  
                         <!-- Pin Point Maps Division Ends -->                                  
                                    <tr><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input  type="hidden" placeholder="latitude"  id="latitude"  name="latitude">
                                            <input  type="hidden" placeholder="longitude" id="longitude" name="longitude">
                                            <button type="submit" id="button"  class="btn btn-info" style="margin-bottom:2%">
                                               Save
                                            </button>

                                        </td>
                                    </tr></a>
                                </table>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
    function get_lat_long(){
      var flag = false;
      var address  = document.getElementById("address").value;
      var city     = $('#city option:selected').val();
      var state    = document.getElementById("state").value;
      var country  = document.getElementById("country").value;
      var palace =address+' '+city+' '+state+' '+country; 
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode( { 'address': palace}, function(results, status){
        var location = results[0].geometry.location;
        var lat = location.lat();
        var lng = location.lng();
        document.getElementById("latitude").value = lat;
        document.getElementById("longitude").value = lng;
        
        $("#testr").attr('onsubmit' , 'return true;');
        $("#testr").submit();

      });
    }
</script>