<?php $tool_tip_json = get_row('tool_tip',array('tool_tip_page'=>'tool_on_advance_pricing')); ?>
<?php $tool_tip = json_decode($tool_tip_json->page_data); ?>



<!-- Advanced pricing div Starts -->
  <div  id="advanced_price" class="alternate_div" >
        <h3>Advance Pricing</h3>
        <hr>
        <?php $currency = '$'; ?>
        <!-- Security Form starts -->
        <?php $arr_arr = array('class'=>'form-horizontal', 'id'=>'formSequrityDeposit'); ?> 
        <?php echo form_open(current_url(),$arr_arr) ?>
            <div class="form-group row">
                <label for="inputEmail3" class="advance-labels col-md-3 control-label">Security Deposit
               
                <?php if(!empty($tool_tip->security_deposit)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->security_deposit ?>" ></span>
                <?php endif; ?>

                </label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="sequritydeposit" placeholder="$" value="<?php if(!empty($properties->security_deposit)){ echo $properties->security_deposit; } ?>">
                    <br>
                </div>
                <div class="col-md-9 col-md-offset-3" id="responseSequrityDeposit"></div>
                <div class="col-md-9 col-md-offset-3">
                    <input type="button" onclick="updatesequritydeposit()" class="btn btn-info" value="Update">
                </div>
            </div>
            <hr>
        <?php echo form_close() ?> 
      <!-- Security Form Ends -->

      <!-- Daily pricing Form starts -->
      <?php $arr_arr = array('class'=>'form-horizontal', 'id'=>'formdaily'); ?>

      <?php echo form_open(current_url(),$arr_arr) ?>
      <!-- <form class='form-horizontal' id='formdaily'> -->
          <div class="form-group row">
              <label for="inputtext3" class="advance-labels col-md-3 control-label">Daily Pricing
                <?php if(!empty($tool_tip->daily_pricing)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->daily_pricing ?>" ></span>
                <?php endif; ?>
              </label>
              <div class="col-md-9 row">
                  <div class="dailyPriceblock col-md-12 row">
                      <label for="inputtext3" class="advance-labels col-md-2 control-label">Select</label>
                      <div class="col-md-8">
                         <select class="form-control" name="eve_type" id="avaibility">
                            <option value="1">Available</option>
                            <option value="0">Unavailable</option>
                         </select>
                      </div>
                  </div>
                  <div class="dailyPriceblock col-md-12 row">
                        <label for="inputtext3" class="advance-labels col-md-2 control-label">From</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="daily_start" name="start" placeholder="From Date" readonly="">
                        </div>
                  </div>

                  <div class="dailyPriceblock col-md-12 row">
                        <label for="inputtext3" class="advance-labels col-md-2 control-label">To</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="daily_end" name="end" placeholder="To Date" readonly="">
                        </div>
                  </div>

                  <div class="dailyPriceblock col-md-12 row" id="dailyPrice">
                      <label for="inputtext3" class="advance-labels col-md-2 control-label">Price <?php echo $currency; ?></label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="eve_price" placeholder="Enter amount for available dates.">
                          <input type="hidden" name="currency_type" value="<?php echo $properties->currency_type; ?>;">
                      </div>
                  </div>
                  <div class="dailyPriceblock col-md-12 row">
                      <label for="inputtext3" class="advance-labels col-md-3 control-label"></label>
                      <div class="col-md-4">
                          <div id="responsedaily"></div>
                          <input type="button" onclick="updatedaily()" class="btn btn-info" value="Update">
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-md-9 row">
              <?php if(!empty($rangArray)): ?>
                  <?php if($weekendprice == 0): ?>
                        <div id="responserange"></div>
                      <?php foreach($rangArray as $range): ?>
                          <?php if($range['rangeType'] == 1): ?>
                              <div id="range_<?php echo $range['rangeid']; ?>" class="col-md-12 row">
                                  <div class="col-md-2">
                                      <a onclick="removerangeprice(<?php echo $range['rangeid']; ?>)" href="javascript:void(0);"><i class="glyphicon dashcon glyphicon-remove"></i></a>
                                  </div>
                                  <div class="col-md-2">
                                      <?php echo $range['price']; ?>
                                  </div>
                                  <div class="col-md-8">
                                        <?php echo date('l F jS Y', $range['start']); ?> - <?php echo date('l F jS Y', $range['end']); ?>
                                  </div>
                              </div>
                          <?php endif; ?>
                      <?php endforeach; ?>
                          <div class="col-md-12 row">
                              <div class="col-md-2">
                                  &nbsp;
                              </div>
                              <div class="col-md-2">
                                  <?php echo $weekdaysprice; ?>
                              </div>
                              <div class="col-md-8">
                                  Price for rest of the days.
                              </div>
                          </div>
                  <?php else: ?>
                      <?php foreach($rangArray as $range): ?>
                            <?php if(isset($range['rangeid'])): ?>
                                <?php $rangeid = 'id="range_'.$range['rangeid'].'"'; ?>
                            <?php else: ?>
                                <?php $rangeid = ''; ?>
                            <?php endif; ?>
                            <div <?php echo $rangeid; ?> class="col-md-12 row">
                                <div class="col-md-2">
                                  <?php if($range['rangeType'] == 1): ?>
                                      <a onclick="removerangeprice(<?php echo $range['rangeid']; ?>)" href="javascript:void(0);"><i class="glyphicon dashcon glyphicon-remove"></i></a>
                                  <?php else: ?>
                                      &nbsp;
                                  <?php endif; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php echo $range['price']; ?>
                                </div>
                                <div class="col-md-8">
                                    <?php echo date('l F jS Y', $range['start']); ?> - <?php echo date('l F jS Y', $range['end']); ?>
                                </div>
                            </div>
                      <?php endforeach; ?>
                  <?php endif; ?>
              <?php endif; ?>
          </div>
          <hr>
     <?php echo form_close() ?> 

      <!-- Daily pricing Form Ends -->

      <!-- Weekend Form starts -->
      <?php $arr_arr = array('class'=>'form-horizontal', 'id'=>'formWeekend'); ?> 
      <?php echo form_open(current_url(),$arr_arr) ?>
          <div class="form-group row col-md-12" style="margin-top: 4%;">
              <label for="inputtext3" class="advance-labels col-md-3 control-label">Weekend Pricing
                <?php if(!empty($tool_tip->weekend_pricing)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->weekend_pricing ?>" ></span>
                <?php endif; ?>
              </label>
              <div class="col-md-4">
                  <input type="text" class="form-control" name="weekendprice" placeholder="$" value="<?php if(isset($weekendprice)){ if($weekendprice > 0){ echo $weekendprice; } } ?>">
                  <br>
              </div>
              <div class="col-md-5">
                  This changes the daily rate for every Friday and Saturday in your calendar.
              </div>

              <div class="col-md-12 row">
                  <div class="col-md-9 col-md-offset-3">
                      <div id="responseWeekend"></div>
                      <input onclick="updateweekend();" type="button" class="btn btn-info" value="Update">
                      <?php if(isset($weekendprice)): ?>
                          <?php if($weekendprice > 0): ?>
                              <a id='removeweekend' class="danger pull-right" onclick="removeweekend(<?php echo $listing_id; ?>)" href="javascript:void(0);"><i class="glyphicon dashcon glyphicon-remove"></i> Remove Weekend Price</a>
                          <?php endif; ?>
                      <?php endif; ?>
                  </div>
              </div>
          </div>
          <hr>
      <?php echo form_close() ?> 
      <!-- Weekend Form Ends -->

      <!-- Weekly Pricing Form starts -->
      <?php $arr_arr = array('class'=>'form-horizontal', 'id'=>'form-weekly'); ?> 
      <?php echo form_open(current_url(),$arr_arr) ?>
          <div class="form-group row">
              <label for="inputtext3" style="float:left" class="advance-labels col-md-3 control-label">Weekly Pricing
                <?php if(!empty($tool_tip->weekly_pricing)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->weekly_pricing ?>" ></span>
                <?php endif; ?>
              </label>
              <br>
              <div class="col-md-12 exact-select row">
                  <label for="inputtext3" class="advance-labels col-md-4 control-label">Apply weekly price to :</label>
                  <div class="col-md-6">
                     <select class="form-control" id="isExactWeekly">
                        <option value="1" <?php if($isExactWeekly == 1){ echo 'selected="selected"';} ?>>exactly 7-day periods</option>
                        <option value="0" <?php if($isExactWeekly == 0){ echo 'selected="selected"';} ?>>any stay of 7 or more days</option>
                     </select>
                  </div>
              </div>
              <br>
              <?php if($weeks): ?>
                  <?php foreach($weeks as $title => $month): ?>
                      <div class="col-xs-2">
                          <div class="col-xs-12" style="padding: 0;">
                              <font size="4"><?php echo $title; ?></font>
                          </div>
                          <?php foreach($month as $key => $value): ?>
                              <div class="monthly-input col-xs-12 row">
                                  <label style="font-size: 8px;" for="inputtext3" class="control-label"><?php echo $value['week']; ?></label>
                                  <input type="text" name="price[]" class="weeklyPrice form-control pricing-input col-xs-6" placeholder="<?php echo $currency; ?>" value="<?php if($value['price'] != 0){ echo $value['price']; }?>">
                                  <input type="hidden" name="start[]" value="<?php echo $value['start']; ?>">
                                  <input type="hidden" name="end[]" value="<?php echo $value['end']; ?>">
                                  <input type="hidden" name="id[]" value="<?php echo $value['id']; ?>">
                              </div>
                          <?php endforeach; ?>
                      </div>
                  <?php endforeach; ?>
              <?php endif; ?>

              <div class="col-md-12" style="margin-top:3% !important">
                  <div id="responseWeek"></div>
                  <input type="button" class="btn btn-info" onclick="updateweekly();" value="Update Weekly Rate">
              </div>

          </div>
          <hr>
      <?php echo form_close() ?>
    <!-- Weekly Pricing Form Ends seasonarray -->

    <!-- Monthly Pricing Form starts -->
      <?php $arr_arr = array('class'=>'form-horizontal', 'id'=>'form-monthly'); ?> 
      <?php echo form_open(current_url(),$arr_arr) ?>
          <div class="form-group row">
                <label for="inputtext3" style="float:none" class="advance-labels col-md-3 control-label">Monthly Pricing
                <?php if(!empty($tool_tip->monthly_pricing)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->monthly_pricing ?>" ></span>
                <?php endif; ?>
                </label>
              <br>
              <div class="col-md-12 exact-select row">
                  <label for="inputtext3" class="advance-labels col-md-4 control-label">Apply monthly price to :</label>
                  <div class="col-md-6">
                     <select class="form-control" id="isExactMonthly">
                        <option value="1" <?php if($isExactMonthly == 1){ echo 'selected="selected"';} ?>>exactly 1 Month periods</option>
                        <option value="0" <?php if($isExactMonthly == 0){ echo 'selected="selected"';} ?>>any stay of 1 Month or more</option>
                     </select>
                  </div>
              </div>
              <br>
              <?php if($months): ?>
                  <?php foreach($months as $title => $monthArray): ?>
                      <?php  
                        $a = explode('-', $title)
                      ?>
                      <div class="col-xs-2">
                          <div>
                              <font size="4"><?php echo $title; ?></font>
                          </div>
                          <?php foreach($monthArray as $monthName => $monthValues): ?>
                              <div class="monthly-input col-xs-12">
                                  <label style="font-size: 8px;" for="inputtext3" class="control-label"><?php echo $monthName; ?></label>
                                  <input type="text" name="price[]" class="form-control pricing-input" value="<?php if($monthValues['price'] != 0){ echo $monthValues['price']; } ?>" placeholder="<?php echo $currency; ?>">
                                  <input type="hidden" name="id[]" value="<?php echo $monthValues['id']; ?>">
                                  <input type="hidden" name="month[]" value="<?php echo $monthName; ?>">
                                  <input type="hidden" name="year[]" value="<?php echo $a[1]; ?>">
                              </div>
                          <?php endforeach; ?>
                      </div>
                  <?php endforeach; ?>
              <?php endif; ?>

              <div class="col-md-12" style="margin-top:3% !important">
                  <div id="responseMonth"></div>
                  <input type="button" class="btn btn-info" onclick="updatemonthly();" value="Update Monthly Rate">
              </div>

          </div>
          <hr>
      <?php echo form_close() ?>
    <!-- Monthly Pricing Form Ends -->



  </div>
    <!-- Advanced pricing div Ends -->

    <style type="text/css">
        
        .advance-labels ,.exact-select{
          text-align: left !important;
          margin-top: 2%;
        }

        .dailyPriceblock{
          padding: 1% !important;
        }

        .monthly-input{
          margin-top:7%;
        }

        .pricing-input{
          font-size: 9px !important;
          padding: 0 2px !important;
          height: 25px !important;
        }
    </style>

    <script type="text/javascript">
        var listing_id = "<?php echo $listing_id ?>";
        var currency_type = "<?php echo $properties->currency_type ?>";
        // $(document).ready(function () {
        //   //called when key is pressed in textbox
        //   $("#amount").keypress(function (e) {
        //      //if the letter is not digit then display error and don't type anything
        //      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //         //display error message
        //         $("#response").html('<p class="bg-danger text-danger"> Only numbers please... </p>').show().delay(2000).fadeOut('slow');
        //         return false;
        //     }
        //    });
        // });
        //$("input[value='hello']").parent("div").css("background", "red");
        // $('#updateWeekly').on('click',function(){
        $(function() {

            $( "#daily_start" ).datepicker({
                // defaultDate: "+1w",
                minDate: 1,
                changeMonth: true,
                numberOfMonths: 1,
                onClose: function( selectedDate ) {
                  $( "#daily_end" ).datepicker( "option", "minDate", selectedDate );
                }
            });

            $( "#daily_end" ).datepicker({
                // defaultDate: "+1w",
                minDate: 1,
                changeMonth: true,
                numberOfMonths: 1,      
                onClose: function( selectedDate ) {
                  $( "#daily_start" ).datepicker( "option", "maxDate", selectedDate );
                }
            });

        });

        $('#avaibility').on('change',function () {
          var val = $('#avaibility').val();
          if (val == 1) {
            $( "#dailyPrice" ).show("slow");
          }else{
            $( "#dailyPrice" ).hide("slow");
          }
        });

        function removerangeprice(id) {
          // alert(id);
          // return false;
            $.ajax({
                type: "post",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxDeleterange",
                data:{ id:id },
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    // console.log(obj.msg);
                    // return false;
                    if (obj.status)
                    {
                        $('#range_' + id).remove();
                        $('#responserange').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responserange').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responserange" ).show("slow").delay(3000).fadeOut('slow');
                    // window.setTimeout(function() {
                    //     window.location.href = '<?php echo base_url(); ?>user/price_terms/'+ listing_id +'/advance';
                    // }, 3000);
                }
            });
        }

        function removeweekend(id) {
            $.ajax({
                type: "post",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxDeleteWeekend",
                data:{ id:id },
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    // console.log(obj.msg);
                    // return false;
                    if (obj.status)
                    {
                        $('#removeweekend').remove();
                        $('#responseWeekend').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responseWeekend').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responseWeekend" ).show("slow").delay(2500).fadeOut('slow');
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url(); ?>user/price_terms/'+ listing_id +'/advance';
                    }, 2500);
                }
            });
        }

        function updateweekend (){

            post = $( "#formWeekend" ).serialize();
            // console.log('PropertyId'+listing_id);
            // console.log('CurrencyType'+currency_type);
            // return false;
            $.ajax({
                type: "post",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxSetWeekendPrice/" + listing_id,
                data:post,
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    // console.log(obj.msg);
                    // return false;
                    if (obj.status)
                    {
                        $('#responseWeekend').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responseWeekend').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responseWeekend" ).show("slow").delay(3000).fadeOut('slow');
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url(); ?>user/price_terms/'+ listing_id +'/advance';
                    }, 3000);
                }
            });
        }

        function updatesequritydeposit (){

            post = $( "#formSequrityDeposit" ).serialize();
            // console.log('PropertyId'+listing_id);
            // console.log('CurrencyType'+currency_type);
            // return false;
            $.ajax({
                type: "post",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxSetSequrityDeposit/" + listing_id,
                data:post,
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    // console.log(obj.msg);
                    // return false;
                    if (obj.status)
                    {
                        $('#responseSequrityDeposit').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responseSequrityDeposit').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responseSequrityDeposit" ).show("slow").delay(3000).fadeOut('slow');
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url(); ?>user/price_terms/'+ listing_id +'/advance';
                    }, 3000);
                }
            });
        }

        function updatedaily (){

            post = $( "#formdaily" ).serialize();
            // console.log('PropertyId'+listing_id);
            // console.log('CurrencyType'+currency_type);
            // return false;
            $.ajax({
                type: "post",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxSetDailyPrice/" + listing_id + "/" +currency_type,
                data:post,
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    // console.log(obj.msg);
                    // return false;
                    if (obj.status)
                    {
                        $('#responsedaily').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responsedaily').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responsedaily" ).show("slow").delay(3000).fadeOut('slow');
                    window.setTimeout(function() {
                        window.location.href = '<?php echo base_url(); ?>user/price_terms/'+ listing_id +'/advance';
                    }, 3000);
                }
            });
        }

        function updateweekly (){

            post = $( "#form-weekly" ).serializeArray();
            isExactWeekly = $( "#isExactWeekly" ).val();
            // console.log(post);
            // return false;
            $.ajax({
                type: "POST",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxSetWeeklyPrice/" + listing_id,
                data: { post: post, isexact: isExactWeekly},
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    if (obj.status)
                    {
                        $('#responseWeek').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responseWeek').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responseWeek" ).show("slow").delay(3000).fadeOut('slow');
                }
            });
        }

        function updatemonthly (){

            post = $( "#form-monthly" ).serializeArray();
            isExactMonthly = $( "#isExactMonthly" ).val();
            // console.log(post);
            // return false;
            $.ajax({
                type: "POST",
                // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
                url: "<?php echo base_url() ?>user/ajaxSetMonthlyPrice/" + listing_id,
                data: { post: post, isexact: isExactMonthly},
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    if (obj.status) 
                    {
                        $('#responseMonth').html('<p class="bg-success text-success">' + obj.msg + '</p>');
                    } 
                    else
                    {
                        $('#responseMonth').html('<p class="bg-danger text-danger">' + obj.msg + '</p>');
                    };

                    $( "#responseMonth" ).show("slow").delay(3000).fadeOut('slow');
                }
            });
        }
    </script>