<link href="<?php echo  base_url() ?>assets/switch_button/bootstrap-switch.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/switch_button/highlight.js"></script>
<script src="<?php echo base_url() ?>assets/switch_button/index.js"></script>
<script src="<?php echo base_url() ?>assets/switch_button/bootstrap-switch.js"></script>

<?php $map_booking = get_map_bookings($properties->id) ?>
<?php 

// $arr = $map_booking;


 ?>

       <?php 
       $arr = array();
       // $arr[] = " [ 'May' , 50 ] ";
     foreach ($map_booking as $key => $value)
     {
        $arr[] = " [ '".$key."' , ".$value." ] ";
     }
       ?>



<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      
      ['Month', 'Booking'],
      <?php echo implode(',', $arr); ?>
    
    ]);

    var options = {
      title: 'Booking Chart',
      hAxis: {title: 'Month', titleTextStyle: {color: '#DE4980'}},
      colors: ['#DE4980']
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
</script>

<style type="text/css">
 .ui-corner-left
 {
  border-radius:0px !important;
 }
  .manage
  {
    box-shadow: 0.4px 0.4px 2px #B9B9B9 inset;
    height: 800px;
    border-radius: 5px !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
  .span2
  {
   margin-left: 5% !important;
  }

  .nearly_there
  {
   padding-top: 1%;
  }
  #chart_div
  {
height: 160px;
margin-left: -35px;
  }
</style>

<?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Manage Listing</h3>
          </div>
               <div class="sub-header">
                  <a class="btn" style="color:#777777" href="">Manage listing</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/address_description/<?php echo $properties->id ?>" > Address and Description </a>  
                  <a class="btn" href="<?php echo base_url(); ?>user/change_property_image/<?php echo $properties->id ?>">Photos</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/calendar/<?php echo $properties->id ?>">Calender</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/price_terms/<?php echo $properties->id ?>">Price and terms</a>  
                  <a class="btn" href="<?php echo base_url() ?>user/featured_images/<?php echo $properties->id ?>">Featured Images</a>  
               </div>

          <?php alert() ?>

        <?php $pr_detail = get_property_detail($properties->id); ?>
    
      <?php $steps_count =  manage_listing_steps_count($properties->id) ?>
        <div class="manage"> 

            <?php if(empty($steps_count)): ?>
            <div class="span2 nearly_there">
            <?php if($pr_detail->publish_permission==1):?>
            <div  id="chart_div">
              <!-- GENERATE GRAPH BY GOOGLE -->
            </div><br>
            <hr>
          <?php endif; ?>
              <h1>
                 <font face="Comic Sans MS" style="text-transform:capitalize">
                  Well Done!
                 </font>
              </h1>
              <h4>
                  Now you are ready to go live! 
              </h4>
            </div><br>
          <?php else: ?>
            <div class="span2 nearly_there">
              <h1>
                 <font face="Comic Sans MS" style="text-transform:capitalize">
                  You're nearly there!
                 </font>
              </h1>
                <?php $steps_count =  manage_listing_steps_count($properties->id) ?>
              <h4>
                <?php if(!empty($steps_count)): ?>
                  Only <?php echo $steps_count ?> more steps to go, and you are ready to go live! 
                <?php endif; ?>
              </h4>
            </div><br>
          <?php endif; ?>

       
            <div class="col-md-12 avtive_div" style="border:1px 0 1px 0 solid ">

              <?php if(empty($steps_count)):?>
                 <div class="col-md-4 " id="hide_hide" style="margin-left:1%"><a href="<?php echo base_url() ?>user/activate_listing/<?php echo $properties->id ?>" class="btn btn-success">Activate your Listing </a></strong></div>
              <?php else: ?>
                 <div class="col-md-4" style="margin-left:1%"><strong>Activate your Listing </strong></div>
             <?php endif; ?>
             
                <div class="col-md-4" style="margin-left:25%;" align="right"> 
                    <?php if($pr_detail->publish_permission==1): ?>
                       <?php if($pr_detail->status==0): ?>
                         <div class="switch_switch_div" id="change_switch">
                            <input id="switch_switch"   type="checkbox" name="my-checkbox" data-on-text="hidden"  data-off-text="active" checked>
                         </div> 
                            <p id="switch_switch_msg">Your listing is hidden and <br> will not appear in search results</p>
                      <?php else: ?>
                         <div class="switch_switch_div" id="change_switch">
                            <input id="switch_switch"   type="checkbox" name="my-checkbox" data-on-text="hidden"  data-off-text="active">
                         </div> 
                            <p id="switch_switch_msg">Your listing is active and <br> will appear in search results</p>
                       <?php endif; ?>
                     <?php else: ?>    
                         <div class="switch_switch_div">
                            <input id="switch_switch"  disabled="disabled" type="checkbox" name="my-checkbox" data-on-text="hidden"  data-off-text="active" checked>
                         </div> 
                                <?php if(empty($steps_count)): ?>
                                     <p id="switch_switch_msg">Your listing is not activated yet.<br><a href="<?php echo base_url() ?>user/activate_listing/<?php echo $properties->id ?>">Activate Now</a></p>
                                 <?php else: ?>
                                     <p id="switch_switch_msg">Your listing is not visible yet. <br><?php echo $steps_count ?> steps remaining</p>
                                 <?php endif; ?>
                    <?php endif; ?>  
               </div>
             </div>

   
<script>
    $(document).ready(function(){
           $('input[name=my-checkbox]').click(function(){
            var property_id = <?php echo $properties->id ?>;
            $.ajax({
                 url:'<?php echo base_url() ?>user/change_property_status',
                data:{property_id:property_id},
                type:'post',
                success:function(res)
                {
                  if(res=="active")
                  {
                     $('#switch_switch_msg').html('Your listing is active and<br> will appear in search results');
                  }
                  else
                  {
                     $('#switch_switch_msg').html('Your listing is hidden and<br>will not appear in search results');
                  }
                }
            });
        });
   });
</script>

   <script type="text/javascript">
  $(document).ready(function(){
        <?php if($pr_detail->publish_permission==1): ?>
          $('#hide_hide').html("");
        <?php endif; ?>
  });
    </script>

            <?php if(!empty($steps_count)){ ?>
       <div class="col-md-12">
               <?php $i=0; ?>
                 <?php if(str_word_count($pr_detail->description)<15): ?>
                      <?php $i++; ?>
                      <div class="col-md-6">
                        <h4> Title and Description.</h4>
                         <p>Be creative and make your listing standout! It should include 15 words or more.<br>
                          </p>
                          <a href="<?php echo base_url() ?>user/address_description/<?php echo $properties->id ?>" class="btn btn-info">Add Description</a>
                      </div>
                 <?php endif; ?>
                
                <?php if(empty($pr_detail->featured_image)): ?>
                  <?php $i++; ?>
                  <div class="col-md-6">
                     <h4>Upload a Photo</h4>
                      <p>Your listing requires at least one photo. Feel free to add more later and make changes anytime.
                      </p>
                      <a href="<?php echo base_url() ?>user/change_property_image/<?php echo $properties->id ?>" class="btn btn-info">Upload a Photo</a>
                  </div>
                <?php endif; ?>

              <?php if(empty($pr_detail->update_calender)): ?>
                 <?php $i++;if($i==3): ?>
                  <div class="col-md-6" style="margin-top:4%">
                       <?php else: ?>
                  <div class="col-md-6">
                       <?php endif; ?>
                     <h4>Update Calendar</h4>
                      <p>Keep your calendar up to date at all times to maximise your booking inquiries.</p>
                      <a href="<?php echo  base_url() ?>user/calendar/<?php echo $properties->id ?>" class="btn btn-info">Update Calender</a>
                   </div>
              <?php endif; ?>
                 </div>
       </div>

    <?php }elseif(empty($steps_count)){ ?>

           <div class="col-md-12">
               <?php $i=0; ?>
                 <?php if(str_word_count($pr_detail->description)<150): ?>
                      <?php $i++; ?>
                      <div class="col-md-6">
                        <h4> Description.</h4>
                           <div id="desc_desc_desc"></div>
                         <p>
                            Your description have <?php echo str_word_count($pr_detail->description) ?> word. Top listings have over 150 words in the house rules.
                         <br>
                          </p>
                          <a href="<?php echo base_url() ?>user/address_description/<?php echo $properties->id ?>" class="btn btn-info">Add Description</a>
                      </div>
                 <?php endif; ?>

                 <?php $images = get_no_of_property_images($properties->id) ?>
                
                <?php if($images<10): ?>
                  <?php $i++; ?>
                  <div class="col-md-6">
                     <h4>Upload a Photo</h4>
                        <div id="img_img_img"></div>
                      <p>
                        Your property has <?php echo $images ?> Photos. Top properties have over 10 high-quality photos.
                      </p>
                      <a href="<?php echo base_url() ?>user/change_property_image/<?php echo $properties->id ?>" class="btn btn-info">Upload a Photo</a>
                  </div>
                <?php endif; ?>

              <?php if(str_word_count($pr_detail->house_rules)<50): ?>
                 <?php $i++;if($i==3): ?>
                  <div class="col-md-6" style="margin-top:4%">
                       <?php else: ?>
                  <div class="col-md-6">
                       <?php endif; ?>
                     <h4>House Rules</h4>
                     <div id="house_rules_house"></div>
                      <p>
                         Your house rules have <?php echo str_word_count($pr_detail->house_rules) ?> word. Top listings have over 50 words in the house rules.
                      </p>
                      <a href="<?php echo  base_url() ?>user/address_description/<?php echo $properties->id ?>/#house_rules" class="btn btn-info">Update rules</a>
                   </div>
              <?php endif; ?>

              <?php if(str_word_count($pr_detail->house_manual)<50): ?>
                 <?php $i++;if($i==4): ?>
                  <div class="col-md-6" style="margin-top:4%">
                       <?php else: ?>
                  <div class="col-md-6">
                       <?php endif; ?>
                     <h4>House Manual</h4>
                     <div id="house_manual_house"></div>
                      <p>
                         Your house manual have <?php echo str_word_count($pr_detail->house_manual) ?> word. Top listings have over 50 words in the house manual.
                      </p>
                      <a href="<?php echo  base_url() ?>user/address_description/<?php echo $properties->id ?>#house_rules" class="btn btn-info">Update manual</a>
                   </div>
              <?php endif; ?>
                 </div>


       </div>
    <?php } ?>


      </div>
    </div>
    <!-- /.container -->

    <style type="text/css">
.bootstrap-switch
{
  min-width: 60% !important;
}
.avtive_div
{
  border-top: 1px solid #E7E7E8;
  border-bottom: 1px solid #E7E7E8;
  padding-top: 3%;
 }
</style>

<?php $no_of_description = str_word_count($pr_detail->description); ?>

<script>

$(function() {
$( "#desc_desc_desc" ).progressbar({
value: <?php echo $no_of_description ?>
});
$( "#desc_desc_desc" ).progressbar({ max: 150 });
});


$(function() {
$( "#img_img_img" ).progressbar({
value: <?php echo $images ?>
});
$( "#img_img_img" ).progressbar({ max: 10});
});

<?php $house_rules = str_word_count($pr_detail->house_rules) ?>

$(function() {
$( "#house_rules_house" ).progressbar({
value: <?php echo $house_rules ?>
});
$( "#house_rules_house" ).progressbar({ max: 50});
});

<?php $house_manual = str_word_count($pr_detail->house_manual) ?>

$(function() {
$( "#house_manual_house" ).progressbar({
value: <?php echo $house_manual ?>
});
$( "#house_manual_house" ).progressbar({ max: 50});
});

</script>

<style type="text/css">
   .ui-widget-header
   {
      background: #F774AB !important;
      border: none;
   }
   .ui-widget-content{
      border: 1px solid #44BFC6 !important;
   }
   .ui-progressbar
   {
      height: 17px;
   }
   .ui-progressbar > .ui-progressbar-value
   {
      margin: 0px !important;
   }
</style>
   

