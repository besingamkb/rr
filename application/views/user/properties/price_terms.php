<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/uniiverse_dropdown.css" type="text/css" >

  <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.js"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.ui.min.js"></script>
  <script src="<?php echo base_url() ?>assets/bootstrap3/dist/js/bootstrap.js"></script>
  <!--<script src="<?php //echo base_url() ?>assets/pin_point_map/jquery.ui.addresspicker.js"></script>-->
  
  <div class='map-wrapper'>
    <input type="hidden" id="reverseGeocode" value="true">
  </div>

  <style type="text/css">
      .input-block-level {
        min-height: 40px !important;
      }
      .unique_feature {
          color: #777;
          float: left;
          height: 50px;
          padding: 0 5px 10px;
          width: 126px;
      }
      .all_input_type
      {
      border:1px solid #ACACAC !important;
      border-radius: 4px; 
      width:180px;
      }
      .all_input_type:hover
      {
        cursor: pointer;
      } 
      th
      {
         text-align: right;
      }
  </style>
  <?php if($tabid == 'basic'): ?>
    <?php  
      $basic = 'class="active"';
      $advance = '';
      $tools = '';
      $basictab = 'active in';
      $advancetab = '';
      $toolstab = '';
    ?>
  <?php elseif($tabid == 'advance'): ?>
    <?php  
      $basic = '';
      $advance = 'class="active"';
      $tools = '';
      $basictab = '';
      $advancetab = 'active in';
      $toolstab = '';
    ?>
  <?php else: ?>
    <?php  
      $basic = '';
      $advance = '';
      $tools = 'class="active"';
      $basictab = '';
      $advancetab = '';
      $toolstab = 'active in';
    ?>
  <?php endif; ?>
  <?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
            <div class="row content-top">
                 <div class="welcome">
                      <h3>Price And Terms</h3>
                 </div>
                 <?php alert(); ?>
                 <div class="row col-lg-12">
                      <div class="sub-header">
                          <a class="btn" href="<?php echo  base_url() ?>user/manage_listing/<?php echo $properties->id ?>">Manage listing</a>  
                          <a class="btn" href="<?php echo  base_url() ?>user/address_description/<?php echo $properties->id ?>" style="color:#777777"> Address and Description </a>  
                          <a class="btn" href="<?php echo base_url(); ?>user/change_property_image/<?php echo $properties->id ?>">Photos</a>  
                          <a class="btn" href="<?php echo base_url() ?>user/calendar/<?php echo $properties->id ?>">Calender</a>  
                          <a class="btn" href="<?php echo base_url() ?>user/price_terms/<?php echo $properties->id ?>">Price and terms</a>  
                          <a class="btn" href="<?php echo base_url() ?>user/featured_images/<?php echo $properties->id ?>">Featured Images</a>  
                      </div>
                      <div class="col-lg-12">
                          <ul class="nav nav-tabs" id="myTab">
                            <li <?php echo $basic; ?>><a href="#basic" data-toggle="tab">Basic Price</a></li>
                            <li <?php echo $advance; ?>><a href="#advance" data-toggle="tab">Advance Price</a></li>
                            <li <?php echo $tools; ?>><a href="#tools" data-toggle="tab">Tools</a></li>
                          </ul>

                          <div class="tab-content">
                            <div class="tab-pane fade <?php echo $basictab; ?>" id="basic">
                              <?php $this->load->view('user/properties/basic_pricing') ?>
                            </div>
                            <div class="tab-pane fade <?php echo $advancetab; ?>" id="advance">
                              <?php $this->load->view('user/properties/advance_pricing') ?>
                            </div>
                            <div class="tab-pane fade <?php echo $toolstab; ?>" id="tools">
                              <?php $this->load->view('user/properties/pricing_tools') ?>
                            </div>
                          </div>
                      </div>
                </div>
            </div>
      </div>

    </div><!-- /.container -->

    <style type="text/css">
      .nav {
          /*margin-top: 3%;*/
      }
    </style>

    <script>
      $(document).on("click", ".dropdown-toggle", function() {
          var addOrRemove = true;
          if ( $( this ).parent().hasClass( "open" ) ) {
            // alert('dsdsds');
            var addOrRemove = false;
          }
          $( this ).parent().toggleClass( 'open', addOrRemove );
      });
    </script>