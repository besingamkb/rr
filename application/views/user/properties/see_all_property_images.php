<link rel="stylesheet" href="<?php echo  base_url() ?>assets/drag_drop_user_pic/css/see_all_images.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<?php $this->load->view('user/leftbar'); ?>

<script>
$(function() {
$( "#sortable" ).sortable();
$( "#sortable" ).disableSelection();
});
</script>
<style>

.in_in_in
{
  margin-top: 4% !important;
}
a{
	text-decoration: none !important;
}
.browse_box{
   margin-top:0px !important;
   margin-left:6% !important;
   margin-bottom: 10% !important;
   width: 60%;
   border-radius: 4px;
   background-color: #EEEEEE;
   padding-top: 20px;
   padding-bottom: 20px;
   box-shadow: 0 0 4px rgba(0, 0, 0, 0.3) inset, 0 -3px 2px rgba(0, 0, 0, 0.1);
   height:432px;
   overflow-y: auto;
   overflow-x: hidden;
}

.img_img{
	border:10px solid white !important;
    border-radius: 5px !important;
    float: left !important;
    margin-bottom: 8px !important;
}
.img_img:hover{
    cursor: pointer;
}


.ui-state-default{
	border:0px solid #D5D5D5 !important;
    margin: 50px !important;
}
.made_pink{
	border:10px solid #DE4980 !important;

}
#sortable { list-style-type: none; margin: 0; padding: 0; width: 450px; }
#sortable li { margin: 5px 5px 5px 0 !important; 
	           padding: 1px !important;
	           padding-bottom: 5px !important; 
	           float: left !important; 
	           width: 100px !important;
	           height: 150px !important;
	           font-size: 4em !important;
	           text-align: center !important;
	           background-color: #EEEEEE }
#del_btn{
 margin-left: 15px !important;
}	           
</style>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>See All Propety Images</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                   <a href="<?php echo base_url() ?>user/change_property_image/<?php echo $property_id ?>" style="margin-right:20px">Back to Previous Page</a>
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
							           <br>
							           <br>
<style type="text/css">
  .save_message
  {
    font-family: Comic;
    font-weight: lighter;
    color: #47A447;
    font-size: 18px
  }


</style>

                         <div align="center" class="browse_box" style="float:left !important;height:455px !important"> 
                             <div style="width:395px">
                               <?php if(!empty($get_first_row)): ?>
                                 <img id="desc_image" src="<?php echo base_url() ?>assets/uploads/property/<?php echo $get_first_row->image_file ?>" width="395" height="295" style="border-radius:5px">
                                  <input type="hidden" id="desc_image_name" value="<?php echo $get_first_row->image_file  ?>">
                                  <input id="image_description" placeholder="Give a description: which room is this?" type="text" class="input-block-level form-control in_in_in" value="<?php echo $get_first_row->description ?>">
                                  <p id="img_message" class="save_message"></p>
                                 <a href="javascript:void(0)" style="margin-top:1% !important" class="btn btn-info " align="right" onclick="add_img_decsription()">Save</a>
                              <?php else: ?>
                                      <strong>
                                         <p style="font-size: 22px ! important;">No Property images uploads yet</p>
                                      </strong>
                                      <br>
                                      <p>Your first photo is the most important It appears in search results and gives people their first impression.
                                          You can reorder photos once they′ve finished uploading.
                                       </p>                                 
                              <?php endif; ?>
                             </div>
                        </div>


										   <?php if(!empty($gallery)):?>
                         <div align="center" id="bro" class="browse_box" style="float:left !important"> 
                      <ul id="sortable">
										   <?php foreach($gallery as $row): ?>
										     <li class="ui-state-default" id="li_<?php echo $row->id ?>">
										       <input type="hidden" id="image_unique" value="<?php echo $row->image_file ?>">
                           <input type="hidden" id="unique_id" value="<?php echo $row->id ?>">
										       <input type="hidden" id="property_id" value="<?php echo $property_id ?>">
										       <img class="img_img" src="<?php echo base_url() ?>/assets/uploads/property/<?php echo $row->image_file ?>" width="100" height="90">
										       <a href="javascript:void(0)" class="btn btn-info my_btn" style="float:left;background-color:#33AEBD;color:white" id="del_btn">Delete</a>
										       <br>
										     </li>
                         <?php endforeach; ?>
										   <?php  endif; ?>
										  </ul>
							          </div>

							          <div style="float:left !important">
<!-- 							          	 <h1>For Delete Drag Here.</h1>
 -->							          </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.img_img').click(function(){
        $('.img_img').removeClass('made_pink');
        $(this).addClass('made_pink');
        var image = $(this).siblings('#image_unique').val();
        var property_id = $(this).siblings('#property_id').val();
        $.ajax({
              type:'post',
              url:'<?php echo base_url() ?>user/ajax_change_property_profile_image',
              data:{image:image,property_id:property_id},
              success:function(res){
                   $('#desc_image').attr('src','<?php echo base_url() ?>assets/uploads/property/'+image).fadeOut(1);
                   $('#desc_image').fadeIn(2000);
                   $('#desc_image_name').val(image);
                   $('#image_description').val(res);
              }
        });
	});

  $('.my_btn').click(function(){
   var unique_id = $(this).siblings('#unique_id').val();
   var property_id = $(this).siblings('#property_id').val();
 	    $.ajax({
                 type:'post',
                 url:'<?php echo  base_url() ?>user/ajax_delete_property_image',
                 data:{unique_id:unique_id,property_id:property_id},
                 success:function(res){
                  	if(res=='deleted'){
                    $('#li_'+unique_id).fadeOut(1000);
                    // $('#li_'+unique_id).fadeIn();
                  	// $('#li_'+unique_id).remove(2000);
                     // alert('Image Has been deleted successfully');
                 	}
                 }
 	    });
  });


});

function add_img_decsription()
{
  var image_name = $('#desc_image_name').val();
  var image_description = $('#image_description').val();
  if(image_description=="")
  {
    alert("Please Give some description");
    return false;
  }
  $('#img_message').html("<img src='<?php echo base_url() ?>assets/img/loading-blue.gif'>");
  $.ajax({
          type:"post",
           url:"<?php echo  base_url() ?>user/add_pro_img_decsription",
           data:{image_name:image_name,image_description:image_description},
           success:function(res)
             {
               
                setTimeout(function(){
                 $('#img_message').html("Saved");
                },2000);
                setTimeout(function(){
                 $('#img_message').html("");
                },4000);
             }
         });
}


</script>

