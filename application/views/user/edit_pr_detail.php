<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/uniiverse_dropdown.css" type="text/css" >

<style type="text/css">
.input-block-level 
{
  min-height: 40px !important;
}
.unique_feature 
{
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>

<?php $this->load->view('user/leftbar'); ?>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Edit Property Detail</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                            <?php echo form_open_multipart(current_url(), array('style'=>'height:1000px', 'class' => 'form-horizontal no-margin well') ); ?>
                                <table class="pull-left">
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr><a>
                                        <th>Allotment</th>
                                        <td>&nbsp;</td>
                                        <td>

    <div style="float:left; width:150px;">
        <input class="input-block-level" style="color:#777777" value="<?php if(!empty($properties->application_fee)){ echo $properties->application_fee;}?>"  type="text" placeholder="How Much" name="appliction_fee">
        <span class="form_error span12"><?php echo form_error('appliction_fee'); ?></span>
    </div>


    <div style="float:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    <div class="chzn-container" style="width:150px; float:left">
        <select class="chzn-single" style="padding:11px; width:150px;color:#777777;height:40px"  name="room_allotment">                            
            <option value="" >Room Allot</option>
            <option value="1"  <?php if(!empty($properties->room_allotment)){   if($properties->room_allotment =='1') echo 'selected="selected"'; }?>   >per hr.</option>
            <option value="2"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='2') echo 'selected="selected"'; }?> >per half day</option>
            <option value="3"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='3') echo 'selected="selected"';} ?> >per day</option>
            <option value="4" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='4') echo 'selected="selected"';} ?> >per night</option>
            <option value="5" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='5') echo 'selected="selected"';} ?> >per week</option>
            <option value="6" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='6') echo 'selected="selected"'; }?> >per month</option>
            <option value="7" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='7') echo 'selected="selected"';} ?> >per ticket</option>
            <option value="8"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='8') echo 'selected="selected"'; }?>>per time</option>
            <option value="9" <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='9') echo 'selected="selected"';} ?> >per trip</option>
            <option value="10"  <?php if(!empty($properties->room_allotment)){  if($properties->room_allotment =='10') echo 'selected="selected"';} ?>>per unit</option>
        </select>
        <span class="form_error span12"><?php echo form_error('room_allotment'); ?></span>
    </div>

    <div style="float:left">&nbsp;&nbsp;&nbsp;</div>
    <div style="width:150px; float:left">
        <input class="input-block-level" style="color:#777777" type="text" value="<?php if(!empty($pr_info->rent)){  echo $pr_info->rent; } ?>"  placeholder="How Many" name="rent">
        <span class="form_error span12"><?php echo form_error('rent'); ?></span>
    </div>

    <div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    <div style="float:left; width:150px;">
        <input class="input-block-level" style="color:#777777" class="span12"  value="<?php if(!empty($properties->deposit_amount)){ echo $properties->deposit_amount;}?>" type="text" placeholder="security Deposit" name="deposit">
        <span class="form_error span12"><?php echo form_error('deposit'); ?></span>
    </div>

                                    </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th>Collection<br>category</th>
                                        <td>&nbsp;</td>
                                        <td>
                                           <div class="chzn-container">
                                            <?php $category=get_collection_category();  ?>
                                            <select class="chzn-single" style="padding:10px; width:300px;color:#777777;height:40px" name="collection_category">
                                                <option value="">Select Category</option>
                                                    <?php foreach ($category as $row): ?>
                                                        <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->pr_collect_cat_id)){ if($pr_info->pr_collect_cat_id == $row->id)echo"selected"; }  ?> > <?php echo $row->category_name; ?> </option>
                                                    <?php endforeach ?>
                                            </select>
                                            </div>
                                              <span class="form_error "><?php echo form_error('collection_category'); ?></span>
                                        </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th>Amenity</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                                            <div style="float:left; width:200px"> 
                                                <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>" <?php if(!empty($pr_amenities)){
                                            foreach ($pr_amenities as $chok1){ 
                                            if($chok1->pr_amenity_id == $row->id) echo "checked"; 
                                            } }?> > <?php echo  $row->name; ?> 
                                            </div>
                                            <?php } ?>
                                        </td>    
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                      <th>Property Image</th>
                                      <td>&nbsp;</td>
                                      <td>
                                          <input type="file" name="userfile" >
                                      </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <img width="200" height="200" src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $properties->featured_image; ?>">
                                        </td>    
                                    </tr>    
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr><td>&nbsp;</td></tr>  
<!--                                     <tr><a>
                                        <th>Contact</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div>
                                                <div style="float:left;">
                                                    <input  style="width:300px" class="input-block-level" id="contact_name" value="<?php //if(!empty($pr_contact_info->contact_name)){ echo $pr_contact_info->contact_name;}?>" type="text"  name="contact_name" readonly="readonly">
                                                    <span class="form_error span12"><?php //echo form_error('contact_name'); ?></span>
                                                </div>
                                                <div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                                <div style="float:left;">
                                                    <input style="width:300px" class="input-block-level"  id="phone" value="<?php// if(!empty($pr_contact_info->phone)){ echo $pr_contact_info->phone;}?>"  type="text" placeholder="Phone" name="phone">
                                                    <span class="form_error span12"><?php //echo form_error('phone'); ?></span>
                                                </div>             
                                            </div>
                                        </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div>
                                                <div style="float:left; width:300px;">
                                                        <input style="width:300px"  class="input-block-level" type="text" id="confirm_email" value="<?php //if(!empty($pr_contact_info->contact_email)){ echo $pr_contact_info->contact_email;}?>"  readonly="readonly" name="confirm_email">
                                                </div>
                                                <div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                                <div style="float:left;">
                                                    <input type="text" style="width:300px" class="input-block-level"  value="<?php// if(!empty($pr_contact_info->website)){ echo $pr_contact_info->website;}?>"  placeholder="Website" name="website">
                                                    <span class="form_error span12"><?php //echo form_error('website'); ?></span>
                                                </div>             
                                            </div>
                                        </td>
                                    </tr></a>
 -->                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <button type="submit" onclick="return checkbox_valid();" class="btn btn-info">
                                                Save
                                            </button>
                                        </td>
                                    </tr></a>
                                </table>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    
  <?php $users = get_all_user();?>
  <script type="text/javascript">
  $(function(){
      var user = new Array();
      <?php foreach($users as $row){ ?>
              user.push('<?php echo $row->user_email; ?>');
      <?php } ?>
    $( "#user" ).autocomplete({
        lookup: user,
        onSelect: function(suggestion){
           var user_email = document.getElementById('user').value; 
           $.post( "<?php echo base_url()?>superadmin/user_info",{'user_email':user_email })
                  .done(function( data ) {
                 if(data !='No Record Found'){  
                    var user =   $.parseJSON(data);
                    document.getElementById('confirm_email').value = user['user_email'];
                    document.getElementById('contact_name').value = user['first_name']+' '+user['last_name'];
                    document.getElementById('phone').value = user['phone'];
                    document.getElementById('user_id').value = user['id'];
                  }
                  else{
                    alert("No Record Found");
                  }
          });
        }
     });
  });  
  </script>
 