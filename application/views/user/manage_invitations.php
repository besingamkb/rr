<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Referrals</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <?php if(!empty($invitation)): ?>
                                             <thead>
                                                  <tr>
                                                       <th style="width:50%">Email</th>
                                                       <th style="width:20%">Status</th>                                                       
                                                       <th style="width:20%">Travel Credit Earned</th>                                                       
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php $i=1; foreach ($invitation as $row):?>
                                                  <tr>                                                       
                                                       <td><?php echo $row->email ?></td>
                                                       <td><?php if($row->is_joined == 1){ echo 'Joined'; }else{ echo 'Invited'; } ?></td>
                                                       <td>
                                                         <?php if(($row->property_credit_given==1) && ($row->booking_credit_given==1)){?>
                                                               20.00$
                                                         <?php } else if(($row->property_credit_given==1) && ($row->booking_credit_given==0)){ ?>
                                                               10.00$
                                                         <?php }else if(($row->property_credit_given==0) && ($row->booking_credit_given==1)){?>
                                                               10.00$
                                                         <?php }else if(($row->property_credit_given==0) && ($row->booking_credit_given==0)){ ?>
                                                               0.00$
                                                        <?php } ?>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        
                                        <div style="margin-left:05px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>                                   
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- <td><a href="<?php echo base_url();?>superadmin/add_property_note/<?php echo $row->id;?>" class="btn btn-warning btn-small" >Add Note</a></td>
                                                   -->