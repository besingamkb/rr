<?php $this->load->view('user/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Trips Full View</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>

                <?php if($last_page=='current'){ ?>
                <div class="pull-right">
                    <a href="<?php echo base_url();?>user/single_trips_export/<?php echo $bookings_id  ?>/Current" style="margin-right:15%"   class="btn btn-info btn-small hidden-phone" >Print/Export</a>
                </div>
                <?php }elseif($last_page=='previous'){ ?>
                <div class="pull-right">
                    <a href="<?php echo base_url();?>user/single_trips_export/<?php echo $bookings_id  ?>/Previous" style="margin-right:15%"   class="btn btn-info btn-small hidden-phone" >Print/Export</a>
                </div>
                <?php }elseif($last_page=='upcoming'){ ?>
                <div class="pull-right">
                    <a href="<?php echo base_url();?>user/single_trips_export/<?php echo $bookings_id  ?>/Upcoming" style="margin-right:15%"   class="btn btn-info btn-small hidden-phone" >Print/Export</a>
                </div>
                <?php } ?>

          <br><br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Detail</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->description)) echo $property_info->description; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Transaction Id</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($booking_info->transaction_id)) echo $booking_info->transaction_id; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Total Amount</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($booking_info->total_amount)) echo $booking_info->total_amount; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Due Amount</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($booking_info->due_amount)) echo $booking_info->due_amount; else echo "0" ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Check In</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($booking_info->check_in)) echo $booking_info->check_in; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Check Out</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($booking_info->check_out)) echo $booking_info->check_out; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Payment Status</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($booking_info->extra)) :?> 
                        <?php $array = json_decode($booking_info->extra) ?>
                        <?php echo $array->payment_status ?>
                        <?php endif; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong></strong>
                    </div>
                    <?php if($last_page=='current'){ ?>
                    <div class="col-md-9">
                        <a href="<?php echo base_url();?>user/current_trips"   class="btn btn-info btn-small hidden-phone" >Back To Previus Page</a>
                    </div>
                    <?php }elseif($last_page=='previous'){ ?>
                    <div class="col-md-9">
                        <a href="<?php echo base_url();?>user/previous_trips"   class="btn btn-info btn-small hidden-phone" >Back To Previus Page</a>
                    </div>
                    <?php }elseif($last_page=='upcoming'){ ?>
                    <div class="col-md-9">
                        <a href="<?php echo base_url();?>user/upcoming_trips"   class="btn btn-info btn-small hidden-phone" >Back To Previus Page</a>
                    </div>
                    <?php } ?>
                </div>
                
                <br>
                
                
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->