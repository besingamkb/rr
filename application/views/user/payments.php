<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Payments</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                             <!--  <div class="widget-header">
                                   <div class="pull-left">
                                        <a class="btn" style="color:#6E6E70" href=""> Current Trips </a>  
                                   </div>
                                   <div class="pull-left">
                                        <a class="btn" href="<?php echo base_url(); ?>user/previous_trips"> Previous Trips </a>  
                                   </div>
                                   <div class="pull-left">
                                        <a class="btn" href="<?php echo base_url(); ?>user/upcoming_trips"> Upcoming Trips </a>  
                                   </div>
                                   <div class="pull-right" style="margin-right:2%">
                                        <a class="btn" href="<?php echo base_url(); ?>user/all_current_trips/<?php echo $offset ?>"> Print/Export </a>  
                                   </div>
                              </div> -->
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:20%">Guest Name</th>  
                                                       <th style="width:20%">Propety Name</th>                                                         
                                                       <th style="width:20%">Amount</th>
                                                       <th style="width:20%">Refund</th>
                                                       <th style="width:20%">Status</th>
                                                       <th style="width:15%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($payments)): ?>
                                             <?php $i=1; foreach ($payments as $row):?>
                                                  <tr> 
                                                       <?php $pr_detail = property_details($row->property_id); ?>  
                                                       <?php $guest = get_user_info($row->owner_id); ?>
                                                       <td><?php echo $guest->first_name; ?></td>                                                                         
                                                       <td><?php echo $pr_detail->title; ?></td>
                                                       <td><?php $to = $row->amount; $paidamnt = (90*$to)/100;  echo number_format($paidamnt, 2) ; ?></td>
                                                       <td><?php if ($row->is_refund == 1): echo 'refunded'; else: echo "-"; ?>                                                            
                                                       <?php endif ?></td>
                                                       <td><?php if ($row->paid_to_owner == 1){ echo "Completed"; }else{ echo "Pending"; } ?></td>                                                       
                                                       <td style="text-align:center;">
                                                            <a href="<?php echo base_url();?>user/payment_detail/<?php echo $row->id;?>"   class="btn btn-info btn-small hidden-phone" >View</a>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($payments): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
