<link rel="stylesheet" href="<?php echo  base_url() ?>assets/drag_drop_user_pic/css/see_all_images.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<?php $this->load->view('user/leftbar'); ?>

<script>
$(function() {
$( "#sortable" ).sortable();
$( "#sortable" ).disableSelection();
});
</script>
<style>
a{
	text-decoration: none !important;
}
.browse_box{
   margin-top:0px !important;
   margin-left:6% !important;
   margin-bottom: 10% !important;
   width: 88%;
   border-radius: 4px;
   background-color: #EEEEEE;
   padding-top: 20px;
   padding-bottom: 20px;
   box-shadow: 0 0 4px rgba(0, 0, 0, 0.3) inset, 0 -3px 2px rgba(0, 0, 0, 0.1);
   height:355px;
   overflow-y: auto;
   overflow-x: hidden;
}

.img_img{
	border:10px solid white !important;
    border-radius: 5px !important;
    float: left !important;
    margin-bottom: 8px !important;
}
.img_img:hover{
    cursor: pointer;
}


.ui-state-default{
	border:0px solid #D5D5D5 !important;
    margin: 50px !important;
}
.made_pink{
	border:10px solid #DE4980 !important;

}
#sortable {
 list-style-type: none;
 margin: 0; 
 padding: 0;
/*width: 450px;*/
 }
#sortable li { margin: 5px 0px 5px 10px !important; 
	           padding: 1px !important;
	           padding-bottom: 5px !important; 
	           float: left !important; 
	           width: 20% !important;
	           height: 150px !important;
	           font-size: 4em !important;
	           text-align: center !important;
	           background-color: #EEEEEE }
#del_btn{
 margin-left: 15px !important;
 background-color: #54C0CF !important;
 color: white;
}	

@media(max-width:800px){
#dt_example #sortable li{
width: 20% !important;
}
}

@media(max-width:603px){
#dt_example #sortable li{
width: 24% !important;
}
}

@media(max-width:480px){
#dt_example #sortable li {
width: 40% !important;
}
}
@media(max-width:360px){
#dt_example #sortable li {
width: 40% !important;
}
}            
</style>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>All Images</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                   <a href="<?php echo base_url() ?>user/change_profile_image" style="margin-right:20px">Back to Previous Page</a>

                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
							           <br>
							           <br>
							           <div align="center" id="bro" class="browse_box" style="float:left !important"> 
										  <ul id="sortable">
										   <?php if(!empty($gallery)):?>
										   <?php foreach($gallery as $row): ?>
										     <li class="ui-state-default" id="li_<?php echo $row->id ?>">
										       <input type="hidden" id="image_unique" value="<?php echo $row->image ?>">
										       <input type="hidden" id="unique_id" value="<?php echo $row->id ?>">
										       <img class="img_img" src="<?php echo base_url() ?>/assets/uploads/profile_image/<?php echo $row->image ?>" width="100" height="90">
										       <a href="javascript:void(0)" class="btn btn-info my_btn" style="float:left" id="del_btn">Delete</a>
										       <br>
										     </li>
									       <?php endforeach; ?>
										   <?php else: ?>
										     <li style="font-size: 22px ! important; width: 412px ! important;">No images uploads yet</li>
										   <?php  endif; ?>
										  </ul>
							          </div>

							          <div style="float:left !important">
<!-- 							          	 <h1>For Delete Drag Here.</h1>
 -->							          </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.img_img').click(function(){
        $('.img_img').removeClass('made_pink');
        $(this).addClass('made_pink');
        var image = $(this).siblings('#image_unique').val();
        $.ajax({
              type:'post',
              url:'<?php echo base_url() ?>user/ajax_change_profile_image',
              data:{image:image},
              success:function(res){
              	alert(res);
              }
        });
	});

  $('.my_btn').click(function(){
   var unique_id = $(this).siblings('#unique_id').val();
 	    $.ajax({
                 type:'post',
                 url:'<?php echo  base_url() ?>user/ajax_delete_image',
                 data:{unique_id:unique_id},
                 success:function(res){
                  	if(res=='deleted'){
                  	$('#li_'+unique_id).html("");
                     alert('Image Has been deleted successfully');
                 	}
                 }
 	    });
  });


});


</script>
