<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Favorites Category</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right" style="margin-right:5%;margin-bottom:2%">
                                    <a href="<?php echo base_url() ?>user/addFavoritesCategory" >Add Category</a>
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <?php if(!empty($FavoritesCategory)): ?>
                                             <thead>
                                                  <tr>
                                                       <th style="width:8%">Category</th>
                                                       <th style="width:12%">Image</th>
                                                       <th style="width:9%">Favorites</th>
                                                       <th style="width:9%">Created</th>
                                                       <th style="width:15%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php $i=1; foreach ($FavoritesCategory as $row):?>
                                                  <tr>
                                                       <td><?php echo substr($row->name,0,10); ?></a></td>
                                                       <td><img  class="iamg_imag" src="<?php echo base_url()?>assets/uploads/favorites_category/<?php echo  $row->image; ?>"></td>
                                                       <td><a href="<?php echo base_url() ?>user/favorites_property/<?php echo $row->id ?>"><span class="glyphicon glyphicon-list"></span> <?php echo get_no_of_favorites_in_a_category($row->id) ?></td>
                                                       <td><?php echo date('d F Y',strtotime($row->created)) ?></td>
                                                       <td style="text-align:center;">
                                                            <a href="<?php echo base_url() ?>user/editFavoritesCategory/<?php echo $row->id ?>"   class="btn btn-info" >Edit</a>
                                                            <a href="<?php echo base_url();?>user/deleteFavoritesCategory/<?php echo $row->id;?>" class="btn btn-warning btn-small" onclick="return confirm('Do you want to delete?' );" >Delete</a>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($FavoritesCategory): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- <td><a href="<?php echo base_url();?>superadmin/add_property_note/<?php echo $row->id;?>" class="btn btn-warning btn-small" >Add Note</a></td>
                                                   -->


<style type="text/css">
     .iamg_imag
     {
       width:100px;
       height:100px;
       border-radius: 4px;
       border:3px solid white;
       box-shadow: 2px 2px 4px;

     }
     a
     {
          text-decoration: none !important;
     }
</style>