<?php $this->load->view('user/leftbar'); ?>

<style type="text/css">
	.text_text{
		border-radius: 10px ;
		border:2px solid #D5D5D6;
	    overflow: auto;
	    height: 103px;
	    padding: 7px;
	}

</style>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review Reply</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Review_info</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review_info->review)) echo $review_info->review; ?>    
                    </div>
                </div>
                
                <br>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Reviewer Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($user_info->first_name)) echo $user_info->first_name.' '.$user_info->last_name; ?>    
                    </div>
                </div>
                
                <br>


         </div>
              <div class="col-md-6" style="margin-left:8%; margin-bottom:2%;">
                 <?php echo form_open(current_url()); ?>


                    <textarea class="text_text" style="width:100%" name="review" required="required"></textarea>
                      <br><br>
                      <button style="margin-left:283px" class="btn btn-primary btn-large hidden-phone">
                       Review
                     </button>
                <?php echo form_close(); ?>
              </div>

              <br><br>
    </div>
  </div>
</div><!-- /.container -->