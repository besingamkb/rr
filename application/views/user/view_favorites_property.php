<?php $this->load->view('user/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Favorites Full View</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Image</strong>
                    </div>
                    <div class="col-md-9">
                      <img style="width:100px; height:100px;" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($property_info->featured_image)) echo  $property_info->featured_image; ?>">
                    </div>
                </div>
                
                <br>
               
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>Owner Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->first_name)) echo $customer_info->first_name." ".$customer_info->last_name; ?>    
                    </div>
                </div>
                
                <br>
                

                <?php if(!empty($customer_info->address)): ?>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Address</strong>
                    </div>
                    <div class="col-md-9">
                      <?php  echo $customer_info->address; ?>    
                    </div>
                </div>
                
                <br>
                <?php endif; ?>
                <?php if(!empty($customer_info->city)): ?>
                <div class="row">
                    <div class="col-md-3">
                     <strong>City</strong>
                    </div>
                    <div class="col-md-9">
                      <?php echo $customer_info->city; ?>    
                    </div>
                </div>
                
                <br>
                <?php endif; ?>
                <?php if(!empty($customer_info->state)): ?>

                <div class="row">
                    <div class="col-md-3">
                     <strong>State</strong>
                    </div>
                    <div class="col-md-9">
                      <?php echo $customer_info->state; ?>    
                    </div>
                </div>
                <br>
               <?php endif; ?> 
                <?php if(!empty($customer_info->country)): ?>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Country</strong>
                    </div>
                    <div class="col-md-9">
                      <?php echo $customer_info->country; ?>    
                    </div>
                </div>
                
                <br>
             <?php endif; ?>

              <br><br>

              <div class="row">
                <div class="col-md-3">
                 <strong>&nbsp;&nbsp;&nbsp;</strong>
                </div>
                <div class="col-md-9">
                   <a class="btn btn-info" href="<?php echo base_url()?>user/favorites_property"> Back to Favorites</a>    
                </div>
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->