<?php $this->load->view('user/leftbar'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
   

    <title>Change Password Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/bootstrap3/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/bootstrap3/dist/css/sign.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/bootstrap3/js/tests/vendor/jquery.js"></script>

    <style type="text/css">
    .register{
        margin-left: 280px;
        width: 600px;
    }
    </style>
  </head>
  <body>

    <div class="container">

      <?php echo form_open(current_url()); ?>
        <div class="register">
          <div class="row">
            <div class="col-md-5">
              <h3 style="padding-left:40px">Change Password</h3>
              <img style="margin:20px 63px" width="100px" height="100px" src="<?php echo base_url()?>assets/img/default_user_300x300.png" alt="" class="img-circle">
            </div>
            <div class="col-md-7">
              <labe>Old Password:</label>
              <input type="password" class="form-control" name="oldpassword" id="oldpassword" value="<?php echo set_value('oldpassword');?>" placeholder="Old Password" autofocus>
              <span><?php echo form_error('oldpassword')?></span> <span style="color:#3276B1;" id="oldpassword1"></span><br>
              
              <labe>New Password:</label>
              <input type="password" class="form-control" name="newpassword" id="newpassword" value="<?php echo set_value('newpassword');?>" placeholder="New Password" >
              <span><?php echo form_error('newpassword')?></span><span style="color:#3276B1;" id="newpassword1"></span><br>
              
              <labe>Confirm Password:</label>
              <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" value="<?php echo set_value('confirmpassword');?>" placeholder="Confirm Password">
              <span><?php echo form_error('confirmpassword')?></span><span  style="color:#3276B1;"id="confirmpassword1"></span><br>
              <br>
              
              <button onclick="return check_fields();" class="btn  btn-primary btn-block" type="submit">Change Password</button>
            </div>
          </div>


        </div>
       
      <?php echo form_close(); ?>  
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
  <script type="text/javascript">
    function check_fields(){
        var oldpassword = $('#oldpassword').val();
        var newpassword = $("#newpassword").val();
        var confirmpassword = $("#confirmpassword").val();
        
        if(oldpassword =='')
        {
          $('#oldpassword1').html('Please enter old password');
          setTimeout(function(){
            $('#oldpassword1').html('');
          },3000);
          return false;
        }
        else if(newpassword=='')
        {
          $('#newpassword1').html('Please enter new password');
          setTimeout(function(){
            $('#newpassword1').html('');
          },3000);
          return false;
        }
        else if(confirmpassword=='')
        {
          $('#confirmpassword1').html('Please enter Confirm Password');
          setTimeout(function(){
            $('#confirmpassword1').html('');
          },3000);
          return false;
        }
    }

  </script>
</html>
