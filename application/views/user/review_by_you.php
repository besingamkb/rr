<?php $this->load->view('user/leftbar'); ?>


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review</h3>
          </div>
          <?php $this->load->view('user/account/profile_sub_header'); ?>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
         <div class="pull-left">
              <a class="btn"  href="<?php echo base_url(); ?>user/review_about_you">Review about you</a>  
         </div>
         <div class="pull-left">
              <a class="btn" style="color:#808080 !important" href="<?php echo base_url(); ?>user/review_by_you">Review By you</a>  
         </div>
        <div class="links"> 
        <table class="table table-hover">
          <?php if(!empty($review)) : ?>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Property Title</th>
                    <th>Review Message</th>
                    <th>Owner Name</th>
                    <th style="text-align:center">Operation</th>
                  </tr>
                </thead>
                <tbody>
              <?php $i=1; foreach ($review as $row) : ?>
                  <tr>
                    <td><?php echo $i++;     ?></td>
                    <td><?php echo word_limiter($row->title,2);  ?></td>
                    <td><?php echo word_limiter($row->review,3);          ?></td>
                    <td><?php echo $row->first_name.' '.$row->last_name;?></td>
                    <td style="text-align:center">
                        <a class="btn btn-info" title="Full View" href="<?php echo base_url(); ?>user/review_full_view/<?php echo $row->customer_id; ?>/<?php echo $row->property_id; ?>/<?php echo $row->id ?>/review_by_you">View</a>
                        <a class="btn btn-warning" title="Delete Review" href="<?php echo base_url(); ?>user/review_delete/<?php echo $row->id; ?>/review_by_you"  onclick="return confirm('Are you sure.')" >Delete</a>
                    </td>
                  </tr>
              <?php endforeach; ?> 
            <?php else : ?>
            <tr><td>No Review Found</td></tr>    
            <?php endif; ?>        
          </tbody>
        </table>
        <?php if($pagination){echo $pagination;} ?>
      </div>
    </div>
  </div>
</div><!-- /.container -->