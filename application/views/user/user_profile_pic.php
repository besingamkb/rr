<?php $this->load->view('user/leftbar'); ?>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Messages View</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <div class="links"> 
          <!--  <form class="form-horizontal" role="form"> -->
          <?php echo form_open(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
              <div class="col-sm-10">
                <input type="text" name="first_name" value="<?php echo $member->first_name; ?>" style="width:600px !important;" class="form-control" id="inputEmail3" placeholder="First Name">
                 <span class="form_error"><?php echo form_error('first_name'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Last Name</label>
              <div class="col-sm-10">
                <input type="text" name="last_name" value="<?php echo $member->last_name; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Last Name">
                 <span class="form_error"><?php echo form_error('last_name'); ?></span>
              </div>
            </div> 
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">User Email</label>
              <div class="col-sm-10">
                <input type="text" name="user_email" value="<?php echo $member->user_email; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="User Email">
                 <span class="form_error"><?php echo form_error('user_email'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Address</label>
              <div class="col-sm-10">
                <input type="text" name="address" value="<?php echo $member->address; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="Address">
                <span class="form_error"><?php echo form_error('address'); ?></span>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">City</label>
              <div class="col-sm-10">
                <input type="text" name="city"  value="<?php echo $member->city; ?>" style="width:600px !important;" class="form-control" id="inputPassword3" placeholder="City">
               <span class="form_error"><?php echo form_error('city'); ?></span>
              </div>
            </div> 

          <!-- </form> -->
          <?php echo form_close(); ?>
        </div>
    </div>
  </div>
</div><!-- /.container -->