                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:20%">Guest Name</th>  
                                                       <th style="width:20%">Propety Name</th>                                                         
                                                       <th style="width:20%">Amount</th>
                                                       <th style="width:20%">Refund</th>
                                                       <th style="width:20%">Status</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($payments)): ?>
                                             <?php $i=1; foreach ($payments as $row):?>
                                                  <tr> 
                                                       <?php $pr_detail = property_details($row->property_id); ?>  
                                                       <?php $guest = get_user_info($row->owner_id); ?>
                                                       <td><?php echo $guest->first_name; ?></td>                                                                         
                                                       <td><?php echo $pr_detail->title; ?></td>
                                                       <td><?php $to = $row->amount; $paidamnt = (90*$to)/100;  echo number_format($paidamnt, 2) ; ?></td>
                                                       <td><?php if ($row->is_refund == 1): echo 'refunded'; else: echo "-"; ?>                                                            
                                                       <?php endif ?></td>
                                                       <td><?php if ($row->paid_to_owner == 1){ echo "Completed"; }else{ echo "Pending"; } ?></td>                                                       
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
