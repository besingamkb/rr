<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/uniiverse_dropdown.css" type="text/css" >
<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>
<?php $this->load->view('user/leftbar'); ?>
     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Add Property Detail</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                            <?php echo form_open_multipart(current_url(),array('style'=>'height:1000px', 'class' => 'form-horizontal no-margin well') ); ?>
                                <table class="pull-left">
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr><a>
                                        <th>Allotment</th>
                                        <td>&nbsp;</td>
                                        <td>

  <div style="float:left; width:150px;">
    <input style="border-radius:5px; padding-left:10px; border: 1px solid #ccc;" class="input-block-level" value="<?php echo set_value('appliction_fee'); ?>"  type="text" placeholder="How Much" name="appliction_fee">
    <span class="form_error span12"><?php echo form_error('appliction_fee'); ?></span>          
  </div>


  <div style="float:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
  <div class="chzn-container" style="width:150px; float:left">
    <select  class="chzn-single" style=" width:150px;height:40px;color:#777777" class="span12" name="room_allotment">                            
        <option value="">Room Allot</option>
        <option value="1">per hr.</option>
        <option value="2">per half day</option>
        <option value="3">per day</option>
        <option value="4">per night</option>
        <option value="5">per week</option>
        <option value="6">per month</option>
        <option value="7">per ticket</option>
        <option value="8">per time</option>
        <option value="9">per trip</option>
        <option value="10">per unit</option>
    </select>
    <span class="form_error span12"><?php echo form_error('room_allotment'); ?></span>
  </div>
 
  <div style="float:left">&nbsp;&nbsp;&nbsp;</div>
  <div style="width:150px; float:left">
    <input style="border-radius:5px; padding-left:10px; border: 1px solid #ccc;" class="input-block-level" value="<?php echo set_value('rent'); ?>"  type="text" placeholder="How Many" name="rent">
    <span class="form_error span12"><?php echo form_error('rent'); ?></span>
  </div>

  <div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
  <div style="float:left; width:150px;">
    <input style="border-radius:5px; padding-left:10px; border: 1px solid #ccc;" class="input-block-level"  type="text" value="<?php echo set_value('deposit'); ?>" placeholder="security Deposit" name="deposit">
    <span class="form_error span12"><?php echo form_error('deposit'); ?></span>
  </div>
 
              
                                        </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th>Collection<br>category</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div class="chzn-container">
                                            <?php $category=get_collection_category();  ?>
                                            <select class="chzn-single" style=" width:250px;height:40px;color:#777777" name="collection_category">
                                                <option value="">Select Category</option>
                                                <?php foreach ($category as $row): ?>
                                                <option value="<?php echo $row->id; ?>"> <?php echo $row->category_name; ?> </option>
                                                <?php endforeach ?>
                                            </select>
                                            </div>
                                            <span class="form_error "><?php echo form_error('collection_category'); ?></span>
                                        </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th>Amenity</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                                            <div style="float:left; width:200px">
                                                <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>"> <?php echo  $row->name; ?>
                                            </div>
                                            <?php } ?>
                                        </td>    
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                      <th>Property Image</th>
                                      <td>&nbsp;</td>
                                      <td>
                                          <input id="image_button" type="file" name="userfile" required="required">
                                      </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>    
<!--                                     <tr><td>&nbsp;</td></tr>    
                                    <tr><a>
                                      <th>User Email</th>
                                      <td>&nbsp;</td>
                                      <td>
                                          <input style="width:250px" value="<?php echo $user['user_email']; ?>" class="input-block-level" id="user" name="user_email" type="text"  readonly>
                                          <span><?php  echo form_error('user_email'); ?></span>
                                      </td>
                                    </a></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                      <th>User Name</th>
                                      <td>&nbsp;</td>
                                      <td>
                                          <input  style="width:250px" value="<?php echo $user['first_name']." ".$user['last_name']; ?>" id="contact_name"  class="input-block-level" type="text"  name="contact_name" readonly>
                                          <span><?php echo form_error('contact_name'); ?></span>
                                      </td>
                                  </a></tr>
                                  <tr><td>&nbsp;</td></tr>
                                  <tr><a>
                                      <th>User Contact</th>
                                      <td>&nbsp;</td>
                                      <td>
                                          <input style="width:250px" class="input-block-level" type="text" value="<?php echo set_value('phone'); ?>" id="phone" placeholder="Phone" name="phone"  required="required">
                                          <span><?php //echo form_error('phone'); ?></span>
                                      </td>
                                  </a></tr>
                                  <tr><td>&nbsp;</td></tr>
                                  <tr><a>
                                      <th>Website URL</th>
                                      <td>&nbsp;</td>
                                      <td>
                                          <input type="text" style="width:250px" class="input-block-level"  placeholder="Website" name="website" value="<?php echo set_value('website'); ?>"  required="required">
                                          <span><?php //echo form_error('website'); ?></span>
                                      </td>
                                  </a></tr>
 -->                               </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <button type="submit" onclick="return checkbox_valid();" class="btn btn-info">
                                                Save
                                            </button> 
                                        </td>
                                    </tr></a>
                                </table>
                  
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


   <script>
      jQuery(document).ready(function (){
      
        var resp='';
      var uploade ='';    
        
   uploade=jQuery('#jquery-wrapped-fine-uploader').fineUploader({
                request: {
                  endpoint: '<?php echo base_url() ?>upload_handler/fineupload', 
                }, validation: {
                  allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
                },
                autoUpload: false
               
              }).on('complete', function(event, id, fileName, responseJSON) {
                

                if(responseJSON.success){

                  jQuery(this).append('<input type="hidden" name="property_image[]" value="'+fileName+'">');
                   resp ='<li>'+
                   '<input type="radio" name="featured_image" value="'+fileName+'"> <span>Featured Iamge</span>' +
                          '<a href="#" class="thumbnail">'+
                            '<img data-src="<?php echo base_url() ?>holder.js/160x120" alt="160x120" style="width: 160px; height: 120px;" src="<?php echo base_url();?>assets/uploads/property/thumbnail/'+fileName+'">'+
                          '</a>'+
                         '</li>';

                  $('.thumbnails').append(resp);
                }
              });
          
          $('#triggerUpload').click(function() {
              uploade.fineUploader('uploadStoredFiles');
          });
      });
    </script>  