<?php $this->load->view('user/leftbar'); ?>

<style type="text/css">
	.text_text{
		border-radius: 10px ;
		border:2px solid #D5D5D6;
	    overflow: auto;
	    height: 103px;
	    padding: 7px;
	}

</style>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Edit Review</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                
                <br>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Reviewer Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($user_info->first_name)) echo $user_info->first_name.' '.$user_info->last_name; ?>    
                    </div>
                </div>
                
                <br>


         </div>
              <div class="col-md-6" style="margin-left:8%; margin-bottom:2%;">
                 <?php echo form_open(current_url()); ?>



                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->
                          <link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
                          <div style="padding:10px 0px 20px 0px;" id="rating_div">
                              <input class="star required" type="radio" name="rating" value="1" <?php if($review_info->rating == 1){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="2" <?php if($review_info->rating == 2){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="3" <?php if($review_info->rating == 3){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="4" <?php if($review_info->rating == 4){ echo 'checked'; } ?>  >
                              <input class="star" type="radio" name="rating" value="5" <?php if($review_info->rating == 5){ echo 'checked'; } ?>  >
                          </div>
                          <script> 
                             $(function(){ $('#rating_div :radio.star').rating(); });
                          </script>
                          <script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->








                    <textarea class="text_text" style="width:100%" name="review" required="required"><?php echo $review_info->review ?></textarea>
                      <br><br>
                      <button style="margin-left:283px" class="btn btn-primary btn-large hidden-phone">
                       Review
                     </button>
                <?php echo form_close(); ?>
              </div>

              <br><br>
    </div>
  </div>
</div><!-- /.container -->