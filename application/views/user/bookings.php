<style type="text/css">
  .zak_zak
  {
    box-shadow: 0.4px 0.4px 2px #B9B9B9 inset;
    margin-top: 6% !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
</style>

<?php $this->load->view('user/leftbar'); ?>


      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Bookings</h3>
          </div>

          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>

          <div class="row-fluid far_far" >
             <div class="pull-left " >
               <a href="<?php echo base_url()?>user/bookings" style="color:#909090 !important">Booked by other</a>
             </div>
             <div class="pull-left far">
               <a href="<?php echo base_url()?>user/bookings_by_me">Booked by me</a>
             </div>
          </div>




        <div class="links zak_zak"> 
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Booking Number</th>
              <th>Property Title</th>
              <th>Check in </th>
              <th>Check Out </th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($bookings)){ $i=1; foreach ($bookings as $row){?>              
                    <?php if($row->alert_status==0): ?>
                  <tr style="background-color:#EFEFEF">
                <?php else: ?>
                  <tr>
                <?php endif; ?>
                    <?php if($row->alert_status==0): ?>
                    <td><a  href="javascript:void(0)" onclick="update_alert_status(<?php echo $row->id ?>)"><?php echo $row->id; ?></a></td>
                   <?php else: ?>
                    <td><a  href="<?php echo base_url() ?>user/booking_detail/<?php echo $row->id ?>/host"><?php echo $row->id; ?></a></td>
                   <?php endif; ?>
                    <td><?php $property= get_property_detail($row->pr_id); echo $property->title;  ?></td>
                    <td><?php echo date('Y-m-d',strtotime($row->check_in)); ?></td>
                    <td><?php echo date('Y-m-d',strtotime($row->check_out));?></td>
                      <td>
                      <?php if($row->status == 0){ ?>
                         <a href="<?php echo base_url() ?>user/approve_booking/<?php echo $row->id ?>" class="btn btn-warning address_title_description" title="Click here for accept this Booking Request">Pending</a>
                      <?php }elseif($row->status == 1){ ?>
                        <button type="button" class="btn btn-primary">Complete</button>                      
                    <?php }elseif($row->status == 2){ ?>
                        <button type="button" class="btn btn-danger">Cancelled</button>
                      
                    <?php }elseif($row->status == 3){ ?>
                        <button type="button" class="btn btn-success">Approved</button>
                      <?php }  ?>
                    </td>
                    <td>
                      <a class="btn btn-info" onclick="return confirm('Do you want to Delete it?');" href="<?php echo base_url() ?>user/delete_booking_of_other/<?php echo $row->id; ?>">Delete</a>


                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->
                        <?php 
                            if($row->status == 1): 
                            $temp  = strtotime($row->check_out)+259200;
                            $today = time();
                            $period = $today-$temp; 
                            if($period > 0):
                        ?>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
                                Review
                            </button>
                        <?php 
                            endif; 
                            endif; 
                        ?>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Please review here....!!</h4>
                                  </div>
                                  <div class="modal-body">
                                    <form id='reviewRatingFormId' onsubmit='return false;'>
                                        <input type="hidden" value="<?php echo $row->id; ?>" id="pr_id" name='space'>
                                        <link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
                                        <span style='color:green; font-size:18px;' id='success_re'></span>
                                        <div style="padding:10px 0px 20px 0px;" id="rating_div">
                                            <input class="star required" type="radio" name="radio_rating" value="1">
                                            <input class="star" type="radio" name="radio_rating" value="2">
                                            <input class="star" type="radio" name="radio_rating" value="3">
                                            <input class="star" type="radio" name="radio_rating" value="4">
                                            <input class="star" type="radio" name="radio_rating" value="5">
                                        </div>
                                        <textarea class='form-control' name='review' id="customer_review" style="width:95%" placeholder="Please review here."  ></textarea>
                                        <span style='color:red;' id='error_re'></span>
                                    </form>
                                    <script> 
                                        function sendReviewRating()
                                        {
                                            var pr_rate = $('#rating_div input[name=radio_rating]:radio:checked').val();
                                            var review  = $('#customer_review').val();

                                            if(pr_rate == null)
                                            {
                                                $('#error_re').html('Please rating first to review this space.');
                                                return false;
                                            }
                                            else if(review == "")
                                            {
                                                $('#error_re').html('Please enter review comment.');
                                                return false;
                                            }
                                            else
                                            {
                                                $.ajax({
                                                        type : 'POST',
                                                         url : '<?php echo base_url(); ?>user/sendReviewRating',
                                                        data : $('#reviewRatingFormId').serialize(),
                                                     success : function(res)
                                                     {
                                                        $('#customer_review').val("");
                                                        $('#error_re').html(' ');
                                                        $('#success_re').html(res);
                                                        setTimeout(function(){
                                                            $('#customer_review').val("");
                                                            $('#error_re').html(' ');
                                                            $('#success_re').html(' ');
                                                            $('.close').trigger('click');
                                                        },3000);
                                                     }
                                                });
                                            }
                                        }                   

                                        $(function(){ $('#rating_div :radio.star').rating(); });
                                    </script>
                                    <script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" onclick="sendReviewRating()">Review</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->



                    </td>
                  </tr>             
            <?php $i++; } } else{ ?>
              <tr><td colspan="6"> No Record Found</td></tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
     <div class="col-md-6">
       <?php echo $pagination ?>
     </div>
    </div>
  </div>
</div><!-- /.container -->

<script type="text/javascript">

function update_alert_status(booking_id)
{
  $.ajax({
          type:"post",
          url:"<?php echo base_url() ?>user/update_alert_status/"+booking_id,
          success:function(res)
          {
            if(res!="")
            {
              if(res=='invalid')
              {
                alert("Receipt is not available because host has not done the payment Yet.")
              }
              else if(res=="valid")
              {
                 window.location="<?php echo base_url() ?>user/booking_detail/"+booking_id+'/host';
              }
            }
          }
        }); 
}
  
</script>