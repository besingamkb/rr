<!-- ///////////////////  Reply Message /////////////////////////////// -->
<?php $user_info = $this->session->userdata('user_info');  ?>
<?php if($reply): ?>
<?php foreach ($reply as  $row): ?>
    <div class="col-md-12 row faraz" style="margin-left:1%; margin-top:50px;">
        <div class="col-md-2" align="center" >
        <?php $user  = get_user_info($row->replier_id); ?>
        <?php $image = $user->image; ?>
        <?php if(!empty($image)): ?>
        <img src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $image;  ?>" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php else: ?>
        <img src="<?php echo base_url(); ?>assets/img/msg.png" height="80" width="80" style="border:1px solid #8F8F91; border-radius:5px;" />
        <?php endif; ?>
        <br>
        <?php echo ucfirst($user->first_name.' '.$user->last_name); ?>
        </div>
        <div class="col-md-10" style="padding:10px; box-shadow:5px 10px 7px #8F8F91; border:1px solid #8F8F91; border-radius:5px; background-color:white; color:black;">
            <?php if(!empty($row->reply)) echo $row->reply; ?>    
            <div style="color:#E585A8; padding:5px;">
                Sent 
                &nbsp;on&nbsp;<?php echo get_time_ago($row->created); ?> 

                <?php
                    $user_info = $this->session->userdata('user_info');
                    $user_id   = $user_info['id']; 
                    $sender_id   = $row->messanger_id; 
                    if($user_id == $sender_id){
                        $current_colum = 'readByMsgSender';
                    }else{
                        $current_colum = 'readByMsgReceiver';
                    }
                ?>
                <?php if($row->$current_colum == 0): ?>
                    <span style='background-color:#DE4980; color:white; padding:5px; border-radius:5px; font-family:comic sans ms; float:right;'>new</span>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>  
