<?php $this->load->view('user/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>View Property</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             
                <div class="row">
                    <div class="col-md-3">
                     <strong> Image</strong>
                    </div>
                    <div class="col-md-9">
                      <img style="width:100px; height:100px;border-radius:5px" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($properties_info->featured_image)) echo  $properties_info->featured_image; ?>">
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong> Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($properties_info->title)) echo $properties_info->title; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <b > Description</b>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($properties_info->description)) echo $properties_info->description; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <b >City</b>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($properties_info->city)) echo $properties_info->city; ?>    
                    </div>
                </div>
                
                <br>
                

                <div class="row">
                    <div class="col-md-3">
                     <b >Country</b>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($properties_info->country)) echo $properties_info->country; ?>    
                    </div>
                </div>
                
                <br>


              <br><br>

              <div class="row">
                <div class="col-md-3">
                 <strong>&nbsp;&nbsp;&nbsp;</strong>
                </div>
                <div class="col-md-9">
                   <a class="btn btn-info" href="<?php echo base_url()?>user/owner_added_property/<?php echo $group_id ?>"> Back to Review</a>    
                </div>
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->