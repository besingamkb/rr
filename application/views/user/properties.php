<?php $this->load->view('user/leftbar'); ?>


<style type="text/css">
.image_prop
{
     width:100px;
     height: 90px;
     border-radius: 5px;
     border: 5px solid white;
     box-shadow: 1px 1px 5px;
}

</style>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Properties</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                        <a class="btn" href="<?php echo base_url(); ?>front/listing_property_first"> Add Property </a>  
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:10%;margin-left:5px" >Image</th>
                                                       <th style="width:15%">Property</th>
                                                       <th style="width:10%">Preview</th>
                                                       <th style="width:8%">Status</th>
                                                       <th style="width:25%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($properties)): ?>
                                             <?php $i=1; foreach ($properties as $row):?>
                                                  <tr>
                                                       <td>
                                                         <?php if(!empty($row->featured_image)): ?>
                                                              <img class="image_prop" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>" ></td>
                                                         <?php else: ?>
                                                              <img class="image_prop" src="<?php echo base_url()?>assets/uploads/property/default_property.jpg" ></td>
                                                         <?php endif; ?>
                                                       <td><?php echo word_limiter($row->title,10); ?></td>
                                                  
                                                       <?php if($row->status==1):?>
                                                       <td><a href="<?php echo base_url() ?>properties/details/<?php echo $row->id; ?>">Preview</a></td>
                                                       <?php else: ?>
                                                       <td>Not Available yet.</td>
                                                       <?php endif; ?>

                                                       <td><?php if($row->status==1){ echo "Approved"; } else{ echo "Pending"; } ?></td>
                                                       <td style="text-align:center;">
<!--                                                             <a href="<?php //echo base_url();?>user/edit_property/<?php //echo $row->id;?>"   class="btn btn-success btn-small hidden-phone" >edit</a> -->                                                           
                                                                <a href="<?php echo base_url();?>user/manage_listing/<?php echo $row->id;?>"   class="btn btn-success btn-small hidden-phone" >Manage Listing</a>
                                                                <a href="<?php echo base_url();?>user/delete_property/<?php echo $row->id;?>" class="btn btn-warning btn-small" onclick="return confirm('Do you want to delete?' );" >Delete</a>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($properties): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- <td><a href="<?php echo base_url();?>superadmin/add_property_note/<?php echo $row->id;?>" class="btn btn-warning btn-small" >Add Note</a></td>
                                                   -->