<?php $this->load->view('user/leftbar'); ?>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Review Full View</h3>
          </div>
          <?php alert(); ?>
          <br>
          <div class="col-md-9 col-md-offset-1 row">
                
                
             

                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Title</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->title)) echo $property_info->title; ?>    
                    </div>
                </div>
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Detail</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($property_info->description)) echo $property_info->description; ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>Review Message</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review_info->review)) echo $review_info->review; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Review Rating</strong>
                    </div>
                    <div class="col-md-9">
                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->
                          <link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
                          <div style="padding:0px 0px 0disabled="disabled"px 0px;" id="rating_div">
                              <input class="star required" type="radio" name="rating" value="1" <?php if($review_info->rating == 1){ echo 'checked'; } ?> disabled="disabled" >
                              <input class="star" type="radio" name="rating" value="2" <?php if($review_info->rating == 2){ echo 'checked'; } ?>  disabled="disabled">
                              <input class="star" type="radio" name="rating" value="3" <?php if($review_info->rating == 3){ echo 'checked'; } ?> disabled="disabled" >
                              <input class="star" type="radio" name="rating" value="4" <?php if($review_info->rating == 4){ echo 'checked'; } ?>  disabled="disabled">
                              <input class="star" type="radio" name="rating" value="5" <?php if($review_info->rating == 5){ echo 'checked'; } ?>  disabled="disabled">
                          </div>
                          <script> 
                             $(function(){ $('#rating_div :radio.star').rating(); });
                          </script>
                          <script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
                        <!-- review system -->
                        <!-- review system -->
                        <!-- review system -->


                    </div>
                </div>
                
                <br>
                
             
                <div class="row">
                    <div class="col-md-3">
                     <strong>Created Date</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($review_info->created)) echo date('d-m-Y',strtotime($review_info->created)); ?>    
                    </div>
                </div>
                
                <br>
                
                <div class="row">
                    <div class="col-md-3">
                     <strong>Reviewer Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($customer_info->first_name)) echo $customer_info->first_name." ".$customer_info->last_name; ?>    
                    </div>
                </div>
                
                <br>
                

              <br><br>
             <?php $created_time = strtotime($review_info->created); ?>
             <?php $today_time   = time() ?>
             <?php $difference_time = $today_time-$created_time  ?>
             <?php $two_week_time = strtotime('+14 days')-time() ?>
              <div class="row">
                <?php if($difference_time<$two_week_time && $last_page=='review_by_you' ): ?>
                <div  align="center" class="col-md-3">
                   <a class="btn btn-primary" href="<?php echo base_url()?>user/edit_review/<?php echo $customer_info->id ?>/<?php echo $property_info->id ?>/<?php echo $review_info->id ?>/<?php echo $last_page ?>">Edit Review</a>    
                </div>
                <?php endif; ?>
                <?php if($last_page=='review_about_you' && (get_my_id() != $review_info->customer_id) ): ?>
                 <div  align="center" class="col-md-3">
                   <a class="btn btn-primary" href="<?php echo base_url()?>user/review_reply/<?php echo $customer_info->id ?>/<?php echo $property_info->id ?>/<?php echo $review_info->id ?>/<?php echo $last_page ?>">Reply</a>    
                 </div>
               <?php endif; ?>
                <?php if($last_page=='review_by_you'): ?>
                <div align="center"  class="col-md-3">
                   <a class="btn btn-primary" href="<?php echo base_url()?>user/review_by_you"> Back to Reviews</a>    
                </div>
              <?php else: ?>
                <div align="center"  class="col-md-3">
                   <a class="btn btn-primary" href="<?php echo base_url()?>user/review_about_you"> Back to Reviews</a>    
                </div>
              <?php endif; ?>

              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->