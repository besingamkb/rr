<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Current Trips</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-left">
                                        <a class="btn" style="color:#6E6E70" href=""> Current Trips </a>  
                                   </div>
                                   <div class="pull-left">
                                        <a class="btn" href="<?php echo base_url(); ?>user/previous_trips"> Previous Trips </a>  
                                   </div>
                                   <div class="pull-left">
                                        <a class="btn" href="<?php echo base_url(); ?>user/upcoming_trips"> Upcoming Trips </a>  
                                   </div>
                                   <div class="pull-right" style="margin-right:2%">
                                        <a class="btn" href="<?php echo base_url(); ?>user/all_current_trips/<?php echo $offset ?>"> Print/Export </a>  
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:20%">Image</th>
                                                       <th style="width:20%">Property</th>
                                                       <th style="width:20%">Booking id</th>
                                                       <th style="width:20%">Check in</th>
                                                       <th style="width:20%">Check Out</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($current_trips)): ?>
                                             <?php $i=1; foreach ($current_trips as $row):?>
                                                  <tr>
                                                       <td><img style="width:60px; height:60px;" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                                                       <td><a href="<?php echo base_url() ?>properties/details/<?php echo $row->pr_id ?>"><?php echo word_limiter($row->title,10); ?></a></td>
                                                       <td><a href="<?php echo base_url() ?>user/booking_detail/<?php echo $row->id ?>"><?php echo $row->id ?></td>
                                                       <td><?php echo date('d-m-Y',strtotime($row->check_in)); ?></td>
                                                       <td><?php echo date('d-m-Y',strtotime($row->check_out)); ?></td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($current_trips): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
