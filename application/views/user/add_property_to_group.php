<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3 style="font-size:300%">Add Properties To Group</h3 style="font-size:300%">
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                        <a class="btn" href="<?php echo base_url(); ?>user/view_group_properties/<?php echo $group_id ?>"> Back To Properties </a> <br> 
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:4%">#</th>
                                                       <th style="width:10%">Image</th>
                                                       <th style="width:15%">Property</th>
                                                       <th style="width:20%">Description</th>
                                                       <th style="width:15%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($properties)): ?>
                                             <?php $i=1; foreach ($properties as $row):?>
                                                 <?php if(($row->status)==1): ?>
                                                  <tr>
                                                       <td><?php echo $i++; ?></td>
                                                       <td><img style="width:80px; height:80px;border-radius:4px" src="<?php echo base_url()?>assets/uploads/property/<?php  if(!empty($row->featured_image)) echo  $row->featured_image; ?>"></td>
                                                       <td><?php echo word_limiter($row->title,10); ?></td>
                                                       <td><?php echo word_limiter($row->description,10); ?></td>
                                                       <!-- Add and remove property in the Group Starts-->
                                                       <?php $user_info = $this->session->userdata('user_info'); ?>
                                                       <?php $check = check_property_in_the_Group($group_id,$user_info['id'],$row->id) ?>
                                                        <?php if($check==1): ?>
                                                       <td style="text-align:center;" >
                                                          <input type="hidden" class="faraz_faraz"value="<?php echo $row->id ?>">
                                                            <a href="javascript:void(0)" class="btn btn-danger btn-small hidden-phone add_property" id="<?php echo $row->id ?>" >Remove From The Group</a>
                                                       </td>

                                                       <?php else: ?>
                                                       <td style="text-align:center;" >
                                                          <input type="hidden" class="faraz_faraz"value="<?php echo $row->id ?>">
                                                            <a href="javascript:void(0)" class="btn btn-success btn-small hidden-phone add_property" id="<?php echo $row->id ?>" >Add To The Group</a>
                                                       </td>
                                                     <?php endif; ?>
                                                       <!-- Add and remove property in the Group Ends -->
                                                  </tr>
                                               <?php endif; ?>   
                                             <?php endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($properties): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                                   
                            <input type="hidden" id="group_id" value="<?php echo $group_id ?>">
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script>
$(document).ready(function() {
     $('.add_property').click(function(){
          var property_id = $(this).siblings('.faraz_faraz').val();   
          var group_id = $('#group_id').val();
          $.ajax({
                   type:"post",
                   url:"<?php echo base_url() ?>user/one_click_add_property",
                   data:{property_id:property_id,group_id:group_id},
                   success:function(res){
                       if(res=="add"){
                          alert("Property Has been Added To the Group");
                           $('#'+property_id).html("Remove From The group");
                           $('#'+property_id).attr('class', 'btn btn-danger btn-small hidden-phone add_property')
                         }
                       else if(res=="delete"){
                           alert("Property Has been Removed From the group");
                           $('#'+property_id).html("Add To The group");
                           $('#'+property_id).attr('class', 'btn btn-success btn-small hidden-phone add_property')
                         }
                   }
               }); 
     });
});


</script>