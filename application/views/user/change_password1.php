<?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Account Setting</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>
        <div class="links"> 
          <!--  <form class="form-horizontal" role="form"> -->
          <?php echo form_open(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Old Password:</label>
              <div class="col-sm-10">
               
                  <input type="password" class="form-control"  style="width:600px !important;" name="oldpassword" id="oldpassword" value="<?php echo set_value('oldpassword');?>" placeholder="Old Password">
                  <span><?php echo form_error('oldpassword')?></span> <span style="color:#3276B1;" id="oldpassword1"></span><br>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">New Password:</label>
              <div class="col-sm-10">
               
                 <input type="password" class="form-control" name="newpassword" style="width:600px !important;" id="newpassword" value="<?php echo set_value('newpassword');?>" placeholder="New Password" >
              <span><?php echo form_error('newpassword')?></span><span style="color:#3276B1;" id="newpassword1"></span><br>
              </div>
            </div> 
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password:</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" style="width:600px !important;" name="confirmpassword" id="confirmpassword" value="<?php echo set_value('confirmpassword');?>" placeholder="Confirm Password">
                <span><?php echo form_error('confirmpassword')?></span><span  style="color:#3276B1;"id="confirmpassword1"></span><br>
              </div>
            </div>
           
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn">Save Changes</button>
              </div>
            </div>
          <!-- </form> -->
          <?php echo form_close(); ?>
        </div>
        </div>
      </div>
    </div><!-- /.container -->