<?php $this->load->view('user/leftbar'); ?>


     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Groups</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-header">
                                   <div class="pull-right">
                                   </div>
                              </div>
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:4%">#</th>
                                                       <th style="width:12%">Image</th>
                                                       <th style="width:12%">Group name</th>
                                                       <th style="width:30%; text-align:center;">View</th>
                                                       <th style="width:15%; text-align:center;" >Actions</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($groups)): ?>
                                             <?php $i=1; foreach ($groups as $row):?>
                                                  <tr>
                                                       <td><?php echo $i++; ?></td>
                                                       <td><img style="width:80px; height:80px;border-radius:3px" src="<?php echo base_url()?>assets/uploads/banner/<?php  if(!empty($row->banner)) echo  $row->banner; ?>"></td>
                                                       <td><?php echo word_limiter($row->group_name,10); ?></td>
                                                       <td>
                                                        <a href="<?php echo base_url()?>user/owner_group_members/<?php echo $row->id;?>"  class="btn btn-default btn-small hidden-phone" data-original-title="">View Members (<span style="color:red"><?php echo get_no_of_members($row->id) ?></span>)</a>
                                                        <a href="<?php echo base_url()?>user/view_group_properties/<?php echo $row->id;?>"  class="btn btn-default btn-small hidden-phone" data-original-title="">View Properties(<span style="color:red"><?php echo get_no_of_properties_from_group($row->id) ?></span>)</a>
                                                       </td>
                                                            <?php $owner_info = $this->session->userdata('user_info');?>
                                                            <?php $check_member = check_member_in_the_Group($row->id,$owner_info['id']) ?>
                                                            <?php if($check_member==1): ?>
                                                       <td style="text-align:center;">
                                                            <a href="<?php echo base_url();?>user/join_group/<?php echo $row->id;?>/leave"   class="btn btn-danger btn-small hidden-phone" >Leave this Group</a>
                                                       </td>
                                                         <?php else: ?>
                                                       <td style="text-align:center;">
                                                            <a href="<?php echo base_url();?>user/join_group/<?php echo $row->id;?>"   class="btn btn-success btn-small hidden-phone" >Join this Group</a>
                                                       </td>
                                                        <?php endif; ?>
                                                  </tr>
                                             <?php endforeach; ?>
                                             <?php else: ?>
                                                  <tr>
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($groups): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
