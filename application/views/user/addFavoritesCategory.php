<?php $tool_tip_json = get_row('tool_tip',array('tool_tip_page'=>'tool_add_favorites_category')); ?>
<?php $tool_tip = json_decode($tool_tip_json->page_data); ?>


<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap-filestyle.js"> </script>


<style type="text/css">
  .zak_zak
  {
    min-height: 600px;
    border-radius: 1px !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
  .alert
  {
   margin-bottom: 0 !important;
   margin-top: 20px !important;
  }
</style>

<?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
         
          <div class="welcome">
            <h3>Add Favorites Category</h3>
          </div>

         <div>
           
           <div class="col-sm-12">
             <?php alert() ?>
           </div>

         </div>
        <div class="links zak_zak col-sm-12" > 
          <!--  <form class="form-horizontal" role="form"> -->
          <?php echo form_open_multipart(current_url()); ?>
       
            <div class="form-group col-sm-12">
              <label for="inputEmail3" class="col-sm-3 control-label"> Name:
                <?php if(!empty($tool_tip->name)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->name ?>" ></span>
                <?php endif; ?>
              </label>
              <div class="col-sm-6">
                  <input type="text" class="form-control"   maxLength="25" name="name"  value="<?php echo set_value('name');?>" placeholder="Name">
                  <span><?php echo form_error('name')?></span> <span style="color:#3276B1;" id="oldpassword1"></span><br>
              </div>
            </div>
       
            <div class="form-group col-sm-12">
              <label for="inputPassword3" class="col-sm-3 control-label">Image:
                <?php if(!empty($tool_tip->image)): ?>
                  <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->image ?>" ></span>
                <?php endif; ?>
              </label>
              <div class="col-sm-6">
                 <input name="image" type="file" data-input="false" data-icon="false" data-buttonText="Select Banner" class="filestyle">
              <span><?php echo form_error('image')?></span><span style="color:#3276B1;" id="newpassword1"></span><br>
              </div>
            </div> 
       
            <div class="form-group col-sm-12">
              <label  class="col-sm-3 control-label"></label>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-info btn-block">Add</button>
              </div>
            </div>
          <!-- </form> -->
          <?php echo form_close(); ?>
        </div>
        </div>
      </div>
    </div>
    </div>
    <!-- /.container -->

    <style type="text/css">
   .zak_zak .form-group .control-label
   {
    text-align: right;
   }
    </style>