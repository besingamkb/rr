<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/uniiverse_dropdown.css" type="text/css" >

<!-- Google Pinpoint Api Starts-->
<!-- Google Pinpoint Api Starts-->
  <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.js"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.ui.min.js"></script>
  <script src="<?php echo base_url() ?>assets/pin_point_map/jquery.ui.addresspicker.js"></script>
  <style type="text/css">
#map {
  border: 1px solid #DDD; 
  height: 300px;
  margin: 10px 0 10px 0;
  -webkit-box-shadow: #AAA 0px 0px 15px;
}  
</style>
  
  <div class='map-wrapper'>
    <input type="hidden" id="reverseGeocode" value="true">
  </div> 

  <script>
  $(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
      componentsFilter: 'country:IN'
    });
    var addresspickerMap = $( "#address" ).addresspicker({
      regionBias: "fr",
      updateCallback: showCallback,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(52,11),
        scrollwheel:true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements: {
        map: "#map",
        lat: "#lat",
        lng: "#lng",
        street_number: '#street_number',
        route: '#route',
        locality: '#city',
        administrative_area_level_2: '#city',
        administrative_area_level_1: '#state',
        country: '#country',
        postal_code: '#zip',
        type: '#type'
      }
    });

    var gmarker = addresspickerMap.addresspicker( "marker");
    gmarker.setVisible(true);
    addresspickerMap.addresspicker( "updatePosition");

      $("#address").addresspicker("option", "reverseGeocode", ($('#reverseGeocode').val() === 'true'));

    function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
    }
    // Update zoom field
    var map = $("#address").addresspicker("map");
    google.maps.event.addListener(map, 'idle', function(){
      $('#zoom').val(map.getZoom());
    });

  });

  </script>


<!-- Google Pinpoint  Api Ends-->
<!-- Google Pinpoint  Api Ends-->

<!-- get address by latitude  and  longtitude Starts-->
<!-- get address by latitude  and  longtitude Starts-->
<script src="<?php echo base_url() ?>assets/pin_point_map/address_by_lat_long.js"></script>

  <script>
 
    // ResponseAddress is a callback function which will be executed after converting coordinates to an address
    
    function find_address_by_lat_long(){
      var latitude  = $('#lat').val();
      var longitude  = $('#lng').val();
     Convert_LatLng_To_Address(latitude,longitude, ResponseAddress);       
    }
 
    /*
    * Response Address
    */
    function ResponseAddress() {
        
        $('#country').val(address['country']);
        $('#city').val(address['city']);
        $('#zip').val(address['postal_code']);
        $('#address').val(address['formatted_address']);
        $('#state').val(address['province']);
    }
 
  </script>

<!-- get address by latitude  and  longtitude Endss-->
<!-- get address by latitude  and  longtitude Endss-->



<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>

<?php $this->load->view('user/leftbar'); ?>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Listing Property</h3>
               </div>
               <?php alert(); ?>
               <div class="row-fluid">
                    <div class="span12">
                         <div class="widget no-margin">
                            <?php echo form_open_multipart(current_url(), array('id' => 'testr' ,'style'=>'height:950px', 'class' => 'form-horizontal no-margin well','onsubmit'=>'get_lat_long(); return false;')); ?>
                                <table class="pull-left">
                                    <tr><td>&nbsp;</td></tr>    
                                    <tr><a>
                                        <th>Property Title</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px" name="property_title" class="input-block-level form-control" type="text" placeholder="property title" value="<?php echo set_value('property_title'); ?>">
                                            <span class="form_error span12"><?php echo form_error('property_title'); ?></span>
                                        </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th>Property Description</th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <textarea rows="5" cols="87" class="span12" name="property_descrip" ></textarea>
                                            <span class="form_error span12"><?php echo form_error('property_descrip'); ?></span>  
                                        </td>
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th>Details about this property</th>
                                        <td>&nbsp;</td>
                                        <td>

                                            <div style="width:100%">
                                                <div class="chzn-container" style="width:33%; float:left;">
                                                    <?php $property_type = get_property_types(); ?> 
                                                    <select class="chzn-single"  name="property_type" id="my-checkbox" style="height:35px;padding:8px !important;margin:0px !important;color:#777777">
                                                        <option value="">Select Property Type</option>
                                                        <?php foreach ($property_type as $row): ?>
                                                        <option value="<?php echo $row->id; ?>"> <?php echo $row->property_type; ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span class="form_error span12"><?php echo form_error('property_type'); ?></span>                                            
                                                </div>
                                                <div class="chzn-container" style="width:33%; float:left;">
                                                    <?php $bed_type = get_bed_types(); ?>
                                                    <select class="chzn-single" style="width:158px; height:35px; padding:8px;color:#777777;margin-left:9px" name="bed_type">
                                                        <option value="">Select Bed Type</option>
                                                        <?php foreach ($bed_type as $row): ?>
                                                        <option value="<?php echo $row->id; ?>"> <?php echo $row->bed_type; ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span class="form_error span12"><?php echo form_error('bed_type'); ?></span>
                                                </div>
                                                <div class="chzn-container" style="width:33%; float:left;">
                                                    <?php $room_type = get_room_type(); ?>
                                                    <select class="chzn-single" style="width:183px; height:35px; padding:8px;color:#777777"  name="room_type">
                                                        <option value="">Select Room Type</option>
                                                        <?php foreach ($room_type as $row): ?>
                                                        <option value="<?php echo $row->id; ?>"> <?php echo $row->room_type; ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span><?php echo form_error('room_type'); ?></span>
                                                </div>
                                            </div>
                                        </td>    
                                    </tr></a>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div style="width:33%; float:left;">
                                                    <input type="text" class="input-block-level form-control" placeholder="Accommodates" name="accommodates">
                                                    <span class="form_error span12"><?php echo form_error('accommodates'); ?></span> 
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <input name="bedroom" class="input-block-level form-control" type="text" placeholder="Bedroom" value="<?php echo set_value('bedroom'); ?>">                 
                                                    <span class="form_error span12"><?php  echo form_error('bedroom');  ?></span>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <input name="bathroom" class="input-block-level form-control" type="text" placeholder="Bathroom" value="<?php echo set_value('bathroom'); ?>">                 
                                                    <span class="form_error span12"><?php  echo form_error('bathroom');  ?></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr></a>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    
                               <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->
                                  <style type="text/css">
                                   a{
                                     text-decoration: none !important;
                                    }
                                    .GPS_type{
                                     display: none;
                                     }
                                    .postal_type{
                                     display: none;
                                    }
                                  </style>
                                  <script>
                                  $(document).ready(function(){
                                     $('#post_type').click(function(){
                                           $('.postal_type').show();
                                           $('.GPS_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#post_type').css('color','#DE4980');
                                           $('#gps_co').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');
                                     });
                                     $('#gps_co').click(function(){
                                           $('.GPS_type').show();
                                           $('.postal_type').hide();
                                           $('.pin_point_type').hide();
                                           $('#gps_co').css('color','#DE4980');
                                           $('#post_type').css('color','#5885AC');
                                           $('#pin_co').css('color','#5885AC');
                                     });
                                     $('#pin_co').click(function(){
                                           $('.pin_point_type').show();
                                           $('.GPS_type').hide();
                                           $('.postal_type').hide();
                                           $('#pin_co').css('color','#DE4980');
                                           $('#post_type').css('color','#5885AC');
                                           $('#gps_co').css('color','#5885AC');
                                     });
                                  });
                                  </script>
                                    <tr>
                                     <a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div class="active" style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="post_type" >Postal Type</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="gps_co">GPS Co-ordinate</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <a href="javascript:void(0)" id="pin_co" style="color:#DE4980">Pin Point On Map</a>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                </div>
                                            </div>
                                        </td>
                                     </a>
                                    </tr>
                               <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->
                               
                               <!-- Postal Type Division Starts -->
                               <!-- Postal Type Division Starts -->
                               <!-- Postal Type Division Starts -->
                               
                                <div >
                                    <tr class="postal_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px" name="address" id="address" class="input-block-level form-control" type="text" placeholder="Property Address" value="<?php echo set_value('address'); ?>">                 
                                            <span class="form_error span12"><?php  echo form_error('address');  ?></span> 
                                        </td>
                                    </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div class="chzn-container" style="width:33%; float:left;">
                                                    <?php $cities = get_cities_info(); ?>
                                                    <select class="chzn-single" onchange="get_neighbourhoot()" style="height:40px;width:177px;padding:8px !important;margin:0px !important;color:#777777" id="city" name="city"  required="required">
                                                        <option value="" >Select city</option>
                                                        <?php if(!empty($cities)): ?>
                                                        <?php foreach($cities as $city): ?>
                                                        <option value="<?php echo $city->city; ?>" class="<?php echo $city->id; ?>"><?php echo $city->city; ?></option> 
                                                        <?php endforeach ?>
                                                        <?php endif; ?>
                                                    </select>
                                                    <span><?php echo form_error('city'); ?></span> 
                                                </div>
                                                <script>
                                                    function get_neighbourhoot()
                                                    {
                                                        var city = $('#city option:selected').attr('class');
                                                        $.ajax({
                                                                type:'POST',
                                                                 url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city',
                                                                data:{city:city},
                                                             success:function(res)
                                                             {
                                                                $('#neighbourhoot').html(res);
                                                             }
                                                        });
                                                    }
                                                </script>
                                                <div class="chzn-container" style="width:33%; float:left;">
                                                    <select class="chzn-single" name="neighbourhood[]" id="neighbourhoot"  style="height:40px;width:163px;padding:8px !important;margin-left:8px !important;color:#777777" multiple="multiuple">
                                                        <option>Select neighbour</option>
                                                    </select>
                                                    <span><?php echo form_error('neighbourhood'); ?></span>
                                                </div>
                                                <div class="chzn-container" style="width:33%; float:left;">
                                                    <?php $country = get_country_array(); ?>
                                                    <select class="chzn-single" style="width:171px; height:40px; padding:10px;color:#777777" id="country" name="country">
                                                        <option value="">Select country</option>
                                                        <?php foreach ($country as $code => $name): ?>
                                                        <option value="<?php echo $name; ?>"> <?php echo $name; ?> </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <span class="form_error "><?php echo form_error('country'); ?></span>
                                                </div>
                                            </div>
                                        </td>
                                     </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="width:100%">
                                                <div style="width:33%; float:left;">
                                                    <input name="state" class="input-block-level form-control" id="state" type="text" placeholder="State" value="<?php echo set_value('state'); ?>">
                                                    <span class="form_error"><?php echo form_error('state'); ?></span>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <input class="input-block-level form-control" type="text" placeholder="zip" name="zipcode" id="zip">
                                                    <span class="form_error span12"><?php echo form_error('zipcode'); ?></span>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                    <input  class="input-block-level form-control" type="text" placeholder="Property Size" name="size">
                                                    <span class="form_error span12"><?php echo form_error('size'); ?></span>
                                                </div>
                                                <div style="width:33%; float:left;">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                                    <tr class="postal_type"><td>&nbsp;</td></tr>
                               </div>
                         <!-- Postal Type Division Ends -->                                  
                         <!-- Postal Type Division Ends --> 

                         <!-- GPS Type Division Starts -->
                         <!-- GPS Type Division Starts -->
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px"  id="lat" class="input-block-level form-control" type="text" placeholder="Latitude" onkeyup="find_address_by_lat_long()" >                 
                                            <span class="form_error span12"><?php  echo form_error('address');  ?></span> 
                                        </td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                     <td>Examples: <code>37N 46' 29.74" or +37.7749295</code></td>
                                    </tr>
                                    <tr class="GPS_type"></tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input style="width:544px"  id="lng" class="input-block-level form-control" type="text" placeholder="Longitude" onkeyup="find_address_by_lat_long()" >                 
                                        </td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                                    <tr class="GPS_type">
                                        <th></th>
                                        <td>&nbsp;</td>
                                     <td>Example: <code>122W 25' 9.89" or - 123.41494155</code></td>
                                    </tr>
                                    <tr class="GPS_type"><td>&nbsp;</td></tr>
                         <!-- GPS Type Division Ends -->                                  
                         <!-- GPS Type Division Ends --> 

                         <!-- Pin Point Maps Division Ends -->                                  
                         <!-- Pin Point Maps Division Ends -->                                  
                                    <tr class="pin_point_type"><td>&nbsp;</td></tr>
                                    <tr class="pin_point_type" >
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td ><div id="map"></div></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="pin_point_type"><td>&nbsp;</td></tr>
                         <!-- Pin Point Maps Division Ends -->                                  
                         <!-- Pin Point Maps Division Ends -->                                  
                                    <tr><a>
                                        <th></th>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input  type="hidden" placeholder="latitude"  id="latitude"  name="latitude">
                                            <input  type="hidden" placeholder="longitude" id="longitude" name="longitude">
                                            <button type="submit" id="button"  class="btn btn-info">
                                               Next >>
                                            </button>
                                        </td>
                                    </tr></a>
                                </table>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
    function get_lat_long(){
      var flag = false;
      var address  = document.getElementById("address").value;
      var city     = $('#city option:selected').val();
      var state    = document.getElementById("state").value;
      var country  = document.getElementById("country").value;
      var palace =address+' '+city+' '+state+' '+country; 
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode( { 'address': palace}, function(results, status){
        var location = results[0].geometry.location;
        var lat = location.lat();
        var lng = location.lng();
        document.getElementById("latitude").value = lat;
        document.getElementById("longitude").value = lng;
        
        $("#testr").attr('onsubmit' , 'return true;');
        $("#testr").submit();

      });
    }
</script>