<style type="text/css">
  .zak_zak
  {
    box-shadow: 0.4px 0.4px 2px #B9B9B9 inset;
    margin-top: 2% !important;
    min-height: 560px;
    border-radius: 1px !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
  .alert
  {
   margin-bottom: 0 !important;
   margin-top: 20px !important;
  }
</style>

<?php $this->load->view('user/leftbar'); ?>
      <div class="col-lg-9">
        <div class="row content-top">
         
          <div class="welcome">
            <h3>Account Setting</h3>
          </div>

         <div>
           
           <?php $this->load->view('user/account/account_header_bar');  ?>

           <div class="col-sm-12">
             <?php alert() ?>
           </div>

         </div>
        <div class="links zak_zak col-sm-12" > 
          <!--  <form class="form-horizontal" role="form"> -->
          <?php echo form_open(current_url()); ?>
       
            <div class="form-group col-sm-12">
              <label for="inputEmail3" class="col-sm-3 control-label">Old Password:</label>
              <div class="col-sm-6">
                  <input type="password" class="form-control"   name="oldpassword" id="oldpassword" value="<?php echo set_value('oldpassword');?>" placeholder="Old Password">
                  <span><?php echo form_error('oldpassword')?></span> <span style="color:#3276B1;" id="oldpassword1"></span><br>
              </div>
            </div>
       
            <div class="form-group col-sm-12">
              <label for="inputPassword3" class="col-sm-3 control-label">New Password:</label>
              <div class="col-sm-6">
                 <input type="password" class="form-control" name="newpassword"  id="newpassword" value="<?php echo set_value('newpassword');?>" placeholder="New Password" >
              <span><?php echo form_error('newpassword')?></span><span style="color:#3276B1;" id="newpassword1"></span><br>
              </div>
            </div> 
       
            <div class="form-group col-sm-12">
              <label for="inputPassword3" class="col-sm-3 control-label">Confirm Password:</label>
              <div class="col-sm-6">
                <input type="password" class="form-control"  name="confirmpassword" id="confirmpassword" value="<?php echo set_value('confirmpassword');?>" placeholder="Confirm Password">
                <span><?php echo form_error('confirmpassword')?></span><span  style="color:#3276B1;"id="confirmpassword1"></span><br>
              </div>
            </div>
       
            <div class="form-group col-sm-12">
              <label  class="col-sm-3 control-label"></label>
              <div class="col-sm-6">
                <button type="submit" class="btn btn-info btn-block">Save Changes</button>
              </div>
            </div>
          <!-- </form> -->
          <?php echo form_close(); ?>
        </div>
        </div>
      </div>
    </div>
    </div>
    <!-- /.container -->

    <style type="text/css">
   .zak_zak .form-group .control-label
   {
    text-align: right;
   }
    </style>