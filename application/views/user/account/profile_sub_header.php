<?php 
          $uri_segment = $this->uri->segment(2);
          $trust = '';
          $profile = '';
          $review = '';
          $change_profile_image = '';
          $help = '';
          switch ($uri_segment) {

            case 'profile':
              $profile = 'pro-active';
              break;
              
            case 'trust_and_verification':
              $trust = 'pro-active';
              break;

            case 'change_profile_image':
              $change_profile_image = 'pro-active';
              break;

            case 'review_by_you':
            case 'review_about_you':
              $review = 'pro-active';
              break;

            default:
              // $home = 'class="active"';
              break;
          }
      ?>

<div class="sub-header">
  	<a class="btn<?php echo ' '.$profile; ?>" href="<?php echo  base_url() ?>user/profile">Improve Your Profile</a>
  	<a class="btn<?php echo ' '.$change_profile_image; ?>" href="<?php echo base_url(); ?>user/change_profile_image">Photo</a>
    <a class="btn<?php echo ' '.$review; ?>" href="<?php echo base_url() ?>user/review_about_you">Reviews</a>
  	<a class="btn<?php echo ' '.$trust; ?>" href="<?php echo base_url() ?>user/trust_and_verification">Trust and Verification</a>
</div>

<style type="text/css">
	.pro-active{
		color:#808080 !important;
	}
</style>