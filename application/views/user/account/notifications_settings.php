<style type="text/css">
    .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
.unstyled
{
  list-style: none outside none;
}
</style>


<?php $this->load->view('user/leftbar'); ?>

      <div class="col-lg-9">
          <div class="row content-top">
              <div class="welcome">
                  <h3>Profile</h3>
              </div>
              <?php alert(); ?>
              <div>
                 <?php $this->load->view('user/account/account_header_bar'); ?>
              </div>
              <br>
              <br>
          <div id="success_jmsg">
            
          </div>
        
              <div class="panel panel-default">
                 
                  <div class="panel-heading"> <!-- heading -->
                    <ul class="nav">
                      <li class="default">
                        <a href="javascript:void(0);">
                          <i class="glyphicon dashcon glyphicon-ok-sign ok"></i> Email Settings
                        </a>
                      </li>
                    </ul>
                  </div>

                  <?php $user_info = get_user_info(get_my_id()); ?>
              
              <form class='form-horizontal' role='form' action="" id="noti_form">

                  <div class="panel-body row"> <!--panel body start -->

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Remind me when
                        </label>
                        <div class="col-sm-10">
                          <ul class="unstyled">
                            <li><input <?php if($user_info->email_upcoming_reservation==1) echo "checked" ?> type="checkbox" value="1" name="email_upcoming_reservation" > <label for="user_preference_email_upcoming_reservation">I have an upcoming reservation.</label></li>
                            <li><input <?php if($user_info->email_review_received==1) echo "checked" ?> type="checkbox" value="1" name="email_review_received" > <label for="user_preference_email_review_received">I have received a new review.</label></li>
                            <li><input <?php if($user_info->email_reference_received==1) echo "checked" ?> type="checkbox" value="1" name="email_reference_received" > <label for="user_preference_email_reference_received">I have received a new reference.</label></li>
                            <li><input <?php if($user_info->email_reference_request_received==1) echo "checked" ?> type="checkbox" value="1" name="email_reference_request_received"> <label for="user_preference_email_reference_request_received">I have received a new reference request.</label></li>
                            <li><input <?php if($user_info->email_review_reminder==1) echo "checked" ?> type="checkbox" value="1" name="email_review_reminder" > <label for="user_preference_email_review_reminder">I need to leave a review for one of my guests.</label></li>
                            <li><input <?php if($user_info->email_calendar_reminder==1) echo "checked" ?> type="checkbox" value="1" name="email_calendar_reminder" > <label for="user_preference_email_calendar_reminder">I can improve my ranking in search results by updating my calendar.</label></li>
                          </ul>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">
                        </label>
                        <div class="col-sm-10 btn_btn">
                          <input id="sav_btn" type="button" onclick="submit_notification_setting()" value="Save Setting" class="btn btn-primary">
                        </div>
                      </div>

                  </div>

                  </div> 
              <!-- Panel body end -->
        </form>
      </div>

    </div><!-- /.container -->

<script type="text/javascript">
function submit_notification_setting()
{
  $('#sav_btn').attr('disabled','disabled');
  $.ajax({
             type:'post',
             url:'<?php echo base_url() ?>user/submit_notification_setting',
             data:$('#noti_form').serialize(),
             success:function(res)
             {
              if(res)
              {
                $('#success_jmsg').html(res);
                $('#sav_btn').removeAttr('disabled','disabled');
              }
             }
          });
}
</script>

    <style type="text/css">

.form-horizontal .control-label
{
  font-size: 14px;
 padding-top: 0px;
}
.btn_btn
{
  padding-left: 7%;
}
input[type="checkbox"]:hover
{
  cursor: pointer;
}
        .form-group {
            padding: 3%;
        }

        .lang-box{
          padding: 2%;
        }
        
        .lang-btn {
            color: #848484 !important;
        }

        .input-group-addon, .input-group-btn {
            width: 0 !important;
        }

        .input-group-addon {
            padding: 0 2px !important;
        }

        .btn-div{
            margin-top: 5px !important;
            padding: 0 !important;
        }

        .add-more {
            color: #5CB85C !important;
            font-size: 13px;
        }
        .model-content {
          height: 347px !important;
          overflow: scroll;
          /*width: 100%;*/
        }

        .prefix-input {
          border: none !important;
        }

        .save-btn {
          margin-top: 4% !important;
        }

    </style>

