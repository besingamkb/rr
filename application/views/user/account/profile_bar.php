<div class="col-lg-12 btm-nav">
    <div class="container">
        <div class="btn-group btn-pdng col-lg-7" style="padding-bottom:10px">
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/dashboard"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-home"></span >&nbsp;Dashboard</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/profile">  <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-user"></span >&nbsp;Profile</button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/bookings"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-flash"></span>&nbsp;Booking<sup>&nbsp;<label style="color:red; margin-top:1px"><?php echo get_no_of_booking_alert(); ?></label></sup></button></a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/messages_received"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-comment"></span >&nbsp;Inbox <sup><label style="color:red; margin-top:1px"><?php echo usermsg_notification(); ?></label></sup></button> </a>
            <a style="text-decoration:none;" href="<?php echo base_url()?>user/change_password"> <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit"></span >&nbsp;Account</button> </a>
        </div>
       <!--  <div class="pull-right">
            <form class="navbar-form" role="search">
                <div class="input-group">
                    <input type="text" class="srch-box form-control" placeholder="Search" name="srch-term" id="srch-term">
                    <div class="input-group-btn">
                        <button class="btn btn-default src-btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div> -->
    </div>
</div>
<div class="container">
    <div class="col-lg-3 side-bar">
        <ul >
            <li>
                <?php $user = get_user_row(); ?>
                <?php if($user->image != "") : ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>" style="height:200px;width:200px"><br>
                <?php else: ?>
                    <img class="img-circle" src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:150px"><br> 
                <?php endif; ?>
                <p></p>
                <p style="color:#8A8A8B !important">
                    <?php echo ucfirst($user->first_name); ?>
                    <?php if(!empty($user->country)): ?>
                      <br>From
                     <?php echo $user->country ?>
                    <?php endif; ?>
                </p>
            </li>
            <li>
                <a style="text-decoration:none;" href="<?php echo base_url()?>user/profile">
                    <span>Improve Your Profile</span>
                </a> 
                <span class="f-right"></span>
            </li>
        </ul>
        <br>
        <ul class="verify_sections input-append" style="border:1.5px solid #cccccc;border-radius:3px"> 
            <li style="background-color:#33AEBD;color:white">
                 <div  class="glyphicon glyphicon-ok">
                 Verifications
                 </div>
                  
            </li>

            <li>
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon col-md-11">
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/phone-micro.png">
                   Phone number
                 </div>
                 <span class="glyphicon glyphicon-ok"></span>
                </div>
            </li>
            <li>
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon col-md-11">
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/facebook-micro.png">
                  Facebook
                 </div>
                 <?php if(!empty($user->facebook_id)): ?>
                 <span class="glyphicon glyphicon-ok"></span>
                 <?php else: ?>
                 <span class="glyphicon glyphicon-remove"></span>
                 <?php endif; ?>
                </div>
            </li>
            <li>
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon col-md-11">
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/twitter-micro.png">
                  Twitter
                 </div>
                 <?php if(!empty($user->twitter_id)): ?>
                 <span class="glyphicon glyphicon-ok"></span>
                 <?php else: ?>
                 <span class="glyphicon glyphicon-remove"></span>
                 <?php endif; ?>
                </div>
            </li>
            <li>
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon col-md-11">
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/google-micro.png">
                  Google Plus
                 </div>
                 <?php if(!empty($user->google_id)): ?>
                 <span class="glyphicon glyphicon-ok"></span>
                 <?php else: ?>
                 <span class="glyphicon glyphicon-remove"></span>
                 <?php endif; ?>
                </div>
            </li>
        </ul>
        <br>    
        <ul class="verify_sections" style="border:1.5px solid #cccccc;border-radius:3px">
            <li style="background-color:#33AEBD;color:white">
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon">
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/group-micro.png">
                  Groups
                 </div>
                </div>
            </li>
            <li>
                <?php if(!empty($groups)): ?>
                    <?php foreach($groups as $row): ?>
                      <a href="<?php echo base_url() ?>properties/group_properties/<?php echo $row->id ?>"><?php echo $row->group_name ?></a> ,
                    <?php endforeach; ?>
                <?php endif; ?>
            </li>
            <?php if(!empty($user->school)): ?>
            <li style="background-color:#33AEBD;color:white">
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon">
                  <img style="width:15;height:15px" src="<?php echo  base_url() ?>assets/img/school-micro.png">
                  School
                 </div>
                </div>
            </li>
            <li>
                <div  style='width:100%'>
                  <?php echo $user->school ?>
                </div>
            </li>
            <?php endif; ?>
            <li style="background-color:#33AEBD;color:white">
                <div  style='width:100%'>
                 <div class="glyphicon glyphicon-list" style="color:black">
                  <span style="color:white">
                    Language
                  </span>
                 </div>
                </div>
            </li>
            <li>
                <?php if(!empty($language)): ?>
                    <?php foreach($language as $row): ?>
                      <?php echo get_lang_name($row->lang_id) ?> ,
                    <?php endforeach; ?>
                <?php endif; ?>
            </li>

        </ul>
    </div>

    <style type="text/css">
        .side-bar ul li{
            text-align:center;
        }
      .verify_sections
     {
        box-shadow: 1px 1px 10px #D6D6D6  !important;
     }

      .verify_sections li .glyphicon
     {
        text-align:left !important;
        font-weight: bold !important;
     }

    </style>
