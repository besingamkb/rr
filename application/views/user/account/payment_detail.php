<?php $this->load->view('user/leftbar'); ?>



      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Payment Detail</h3>
          </div>
          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>

                <?php /* if($last_page=='current'){ ?>
                <div class="pull-right">
                    <a href="<?php echo base_url();?>user/single_trips_export/<?php echo $bookings_id  ?>/Current" style="margin-right:15%"   class="btn btn-info btn-small hidden-phone" >Print/Export</a>
                </div>
                <?php }elseif($last_page=='previous'){ ?>
                <div class="pull-right">
                    <a href="<?php echo base_url();?>user/single_trips_export/<?php echo $bookings_id  ?>/Previous" style="margin-right:15%"   class="btn btn-info btn-small hidden-phone" >Print/Export</a>
                </div>
                <?php }elseif($last_page=='upcoming'){ ?>
                <div class="pull-right">
                    <a href="<?php echo base_url();?>user/single_trips_export/<?php echo $bookings_id  ?>/Upcoming" style="margin-right:15%"   class="btn btn-info btn-small hidden-phone" >Print/Export</a>
                </div>
                <?php } */ ?>

          <br><br>
          <div class="col-md-9 col-md-offset-1 row">                
                <?php $pr_detail = property_details($payment->property_id); ?>  
                <?php $guest = get_user_info($payment->owner_id); ?>
              <div class="row">
                    <div class="col-md-3">
                     <strong>Guest Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($guest->first_name)) echo $guest->first_name; ?>    
                    </div>
                </div> 
                <br>
                <div class="row">
                    <div class="col-md-3">
                     <strong>Property Name</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if(!empty($pr_detail->title)) echo $pr_detail->title; ?>    
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Amount</strong>
                    </div>
                    <div class="col-md-9">
                      <?php $to = $payment->amount; $paidamnt = (90*$to)/100;  echo number_format($paidamnt, 2) ; ?>    
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong>Payment Source</strong>
                    </div>
                    <div class="col-md-9">
                     PAYPAL
                    </div>
                </div>
                <br>

                 <div class="row">
                    <div class="col-md-3">
                     <strong>Refund</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if ($payment->is_refund == 1): echo 'refunded'; else: echo "-"; ?>                                                            
                      <?php endif ?>
                    </div>
                </div>
                <br>
               
               
                <div class="row">
                    <div class="col-md-3">
                     <strong>Payment Status</strong>
                    </div>
                    <div class="col-md-9">
                      <?php if ($payment->paid_to_owner == 1){ echo "Completed"; }else{ echo "Pending"; } ?>
                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="col-md-3">
                     <strong></strong>
                    </div>
                    <?php /* if($last_page=='current'){ ?>
                    <div class="col-md-9">
                        <a href="<?php echo base_url();?>user/current_trips"   class="btn btn-info btn-small hidden-phone" >Back To Previus Page</a>
                    </div>
                    <?php }elseif($last_page=='previous'){ ?>
                    <div class="col-md-9">
                        <a href="<?php echo base_url();?>user/previous_trips"   class="btn btn-info btn-small hidden-phone" >Back To Previus Page</a>
                    </div>
                    <?php }elseif($last_page=='upcoming'){ ?>
                    <div class="col-md-9">
                        <a href="<?php echo base_url();?>user/upcoming_trips"   class="btn btn-info btn-small hidden-phone" >Back To Previus Page</a>
                    </div>
                    <?php } */ ?>
                </div>
                
                <br>
                
                
              </div>
              <br><br>
         </div>
    </div>
  </div>
</div><!-- /.container -->