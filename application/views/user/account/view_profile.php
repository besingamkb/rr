<?php $this->load->view('user/account/profile_bar'); ?>

<style>
a{ text-decoration:none !important; }
</style>

      <div class="col-lg-9">
        <div class="row content-top" style="border-radius:5px">
          <div class="panel panel-primary">
            <div class="header panel-body row">
              <div class="col-lg-6">
                Hey i am <?php echo $user->first_name; ?>
              </div>
              <div class="pull-right" style="margin-right: 4px;">
                Member Since <?php echo date('F j, Y', strtotime($user->created)); ?>
              </div>
            </div>
          </div>
              <?php if(!empty($user->description)): ?>
          <div class="col-md-12 desc_desc_desc" >
            <?php echo word_limiter($user->description,50) ?>
          </div>
             <?php endif; ?>
        </div>

        <style type="text/css">
        .desc_desc_desc
        {
          padding:0 15px 15px 15px; 
        }
         </style>

        <div class="row content-top">
            <div class="panel panel-default">
                <div class="panel-heading"> <!-- heading -->
                  <ul class="nav nav-pills nav-stacked">
                    <li class="default">
                      <a href="javascript:void(0);">
                        <span class="badge pull-right"><?php echo $total_rows; ?></span>
                        <i class="glyphicon dashcon glyphicon-list"></i>
                        Listings
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="panel-body row" id="prop_list"> <!-- body -->
                    <?php if($properties): ?>
                        <?php foreach($properties as $row): ?>
                            <div class="row property col-md-11">
                                <div class="col-md-4">
                                  <a href="<?php echo  base_url() ?>properties/details/<?php echo $row->prop_id ?>">
                                   <img src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $row->featured_image; ?>" alt="Not Available" class="img-thumbnail">
                                  </a>
                                </div>
                                <div class="col-md-7 row">
                                  <div class="prop-data col-md-11"><a href="<?php echo  base_url() ?>properties/details/<?php echo $row->prop_id ?>"><?php echo $row->title; ?></a></div>
                                  <div class="prop-data col-md-11"><?php echo substr($row->unit_street_address,0,30); ?></div>
                                  <div class="prop-data col-md-6"><?php echo get_property_type($row->property_type_id); ?></div><div class="prop-data col-md-6"><?php if($row->accommodates > 1){ echo $row->accommodates.' Guests'; }else{ echo $row->accommodates.' Guest'; }?></div>
                                  <div class="prop-data col-md-6"><a href="<?php echo  base_url() ?>properties/details/<?php echo $row->prop_id ?>/review#revRat_tab"><?php echo get_pr_review_count($row->prop_id); ?></a></div><div class="prop-data col-md-6"><?php echo $row->currency_type; ?> <?php echo number_format($row->application_fee, 2); ?></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <input id="total_rows" type="hidden" value="<?php echo $total_rows; ?>">
                <?php if($count > 2): ?>
                    <div class="panel-footer row load-footer">
                        <div class="col-md-12">
                            <a href="javascript:void(0);" id="load_more_prop" class="btn btn-primary pull-right">View More</a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
      </div>
     <!--  <div class="starter-template">
        <h1>Bootstrap starter template</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div> -->

    </div><!-- /.container -->

    <script type="text/javascript">
        $('#load_more_prop').click(function(){
          var offset = $('.property').length;
          // var category = $('#category').val();
          // var month = $('#mont').val();
          // var search = $('#search').val();
              // alert(offset); 
              // return false;
              $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>user/ajax_load_property/'+offset,            
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if(obj.status){
                      $('#prop_list').append(obj.resp);
                    }else{
                      $('#load_more_prop').hide();
                    }
                    
                    if($('.property').length >= $("#total_rows").val()){
                      $('#load_more_prop').hide();
                    }
                 }
              });
          });
    </script>

    <style type="text/css">

    .img-thumbnail
    {
      width:210px;
      height:180px;
    }
      .header{
          /*background-color:  #33AEBD !important;*/
          background-color: #33AEBD !important;
          border-radius: 0;
          color: #FFFFFF;
          font-size: 20px;
          margin: 0;
          padding: 20px;
          text-decoration: none;
      }

      .nav-stacked > li {
         background-color: #D6D6D6 !important;
         color: #999 !important;
         font-size: 15px !important;
         border: 2px solid #B8B8B8;
         border-radius: 9px;
      }

      .nav-stacked > li >a {
         background-color: #D6D6D6 !important;
         color: #999 !important;
         font-size: 15px !important;
      }

      .nav-stacked > li > a:hover, .nav > li > a:focus {
          text-decoration: none;
          background-color: #D6D6D6 !important;
          color: #999 !important;
          font-size: 15px !important;
      }

      .property{
        margin-left: 2%;
        padding: 2%;
      }

      .prop-data{
        padding: 2%;
      }
      .load-more{
         float: right;
      }
      .load-footer{
          margin: 2% !important;
          padding: 2% !important;
      }
    </style>