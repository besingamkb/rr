<?php $this->load->view('user/leftbar'); ?>
<?php $varfctn = array('email' => false,'phone' => false,'fb' => false,'g+' => false, 'twt' => false); ?>
<style>
  a{ text-decoration:none !important; }
</style>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="panel panel-primary">
            <div class="header panel-body row">
              <div class="col-lg-6">
                Trust And Verification
              </div>
              <div class="pull-right" style="margin-right: 4px;">
                Verify your account.
              </div>
            </div>
          </div>
        <?php $this->load->view('user/account/profile_sub_header'); ?>
        </div>

        <?php alert(); ?>
        <div class="row content-top">
            <div class="panel panel-default">
                <div class="panel-heading"> <!-- heading -->
                  <ul class="nav">
                    <li class="default">
                      <a href="javascript:void(0);">
                        <i class="glyphicon dashcon glyphicon-ok-sign ok"></i> Your Current Verifications
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="panel-body row"> <!-- body -->
                    <?php if($varifications): ?>
                          <div class="row var-data col-md-11">
                              <?php if(($varifications->user_status == 1) && (!empty($varifications->user_email))): ?>
                                  <div class="col-md-2">
                                      Email:
                                  </div>
                                  <div class="col-md-10">
                                      <div class="col-md-11"> Your verified email is <strong><?php echo $varifications->user_email; ?></strong></div>
                                  </div>
                                  <?php $varfctn['email'] = TRUE; ?>
                              <?php endif; ?>

                              <?php if(($varifications->user_status == 1) && ($varifications->user_email != 0)): ?>
                                  <div class="col-md-2">
                                      Phone:
                                  </div>
                                  <div class="col-md-10">
                                      <div class="col-md-11"> Your verified Phone is <strong><?php echo $varifications->phone; ?></strong></div>
                                  </div>
                                  <?php $varfctn['phone'] = TRUE; ?>
                              <?php endif; ?>

                              <?php if(($varifications->user_status == 1) && (!empty($varifications->facebook_id))): ?>
                                  <div class="col-md-2">
                                      Facebook:
                                  </div>
                                  <div class="col-md-10">
                                      <div class="col-md-11"> You are verified with <strong>Facebook</strong></div>
                                  </div>
                                  <?php $varfctn['fb'] = TRUE; ?>
                              <?php endif; ?>
                              <?php if(($varifications->user_status == 1) && (!empty($varifications->google_id))): ?>
                                  <div class="col-md-2">
                                      Google+:
                                  </div>
                                  <div class="col-md-10">
                                      <div class="col-md-11"> You are verified with <strong>Google+</strong></div>
                                  </div>
                                  <?php $varfctn['g+'] = TRUE; ?>
                              <?php endif; ?>

                              <?php if(($varifications->user_status == 1) && (!empty($varifications->twitter_id))): ?>
                                  <div class="col-md-2">
                                      Tweeter:
                                  </div>
                                  <div class="col-md-10">
                                      <div class="col-md-11"> You are verified with <strong>Tweeter</strong></div>
                                  </div>
                                  <?php $varfctn['twt'] = TRUE; ?>
                              <?php endif; ?>
                          </div>
                    <?php endif; ?>
                </div>
                <?php /* if($count > 0): ?>
                    <div class="panel-footer row load-footer">
                        <div class="col-md-12">
                            <a href="javascript:void(0);" id="load_more_prop" class="btn btn-primary pull-right">View More</a>
                        </div>
                    </div>
                <?php endif; */ ?>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"> <!-- heading -->
                  <ul class="nav">
                    <li class="default">
                      <a href="javascript:void(0);">
                        <i class="glyphicon dashcon glyphicon-plus-sign more-varfctn"></i> Add More Verifications
                      </a>
                    </li>
                  </ul>
                </div><!-- phone -->
                <div class="panel-body row"> <!-- body -->
                    <?php if($varfctn): ?>
                          <div class="row var-data col-md-12">

                              <?php if(!$varfctn['phone']): ?>
                                  <div class="col-md-12 row">
                                      <div class="col-md-10 phone-content">
                                        <h2>Phone:</h2>
                                        <p>Make it easier to communicate with a verified phone number. We’ll send you a code by SMS . Enter the code below to confirm that you’re the person on the other end.</p>
                                        <p>your number is only shared with another vacalio member once you have a confirmed booking.</p>
                                        <p>No phone number entered</p>
                                        <p>
                                          <a href="javascript:void(0);">
                                            <i class="glyphicon dashcon glyphicon-plus-sign ok"></i> Add Phone Number
                                          </a>
                                        </p>
                                      </div>
                                  </div>
                              <?php endif; ?>

                                  <div class="col-md-12 row">
                                      <div class="col-md-9 phone-content">
                                        <h2>Facebook:</h2>
                                        <p>Sign in with Facebook and discover your trusted connections to hosts and guests all over the world.</p>
                                      </div>
                                      <div class="col-md-3 link-btns" id="fb_btn">
                                        <?php if(!$varfctn['fb']): ?>
                                          <a href="javascript:void(0);" id="fb_connect" class="btn btn-primary pull-right connect">Connect</a>
                                        <?php else: ?>
                                          <a href="javascript:void(0);" id="fb_dconnect" class="btn btn-default pull-right disconnect">Disconnect</a>
                                        <?php endif; ?>
                                      </div>
                                  </div>

                                  <div class="col-md-12 row">
                                      <div class="col-md-9 phone-content">
                                        <h2>Google+:</h2>
                                        <p>Connect your Vacalio account to your Google account for simplicity and ease.</p>
                                      </div>
                                      <div class="col-md-3 link-btns" id="google_btn">
                                        <?php if(!$varfctn['g+']): ?>
                                          <a href="javascript:void(0);" id="google_connect" class="btn btn-primary pull-right connect">Connect</a>
                                        <?php else: ?>
                                          <a href="javascript:void(0);" id="google_dconnect" class="btn btn-default pull-right disconnect">Disconnect</a>
                                        <?php endif; ?>
                                      </div>
                                  </div>

                                  <div class="col-md-12 row">
                                      <div class="col-md-9 phone-content">
                                        <h2>Twitter:</h2>
                                        <p>Connect your Vacalio account to your Twitter account for simplicity and ease.</p>
                                      </div>
                                      <div class="col-md-3 link-btns" id="twt_btn">
                                        <?php if(!$varfctn['twt']): ?>
                                          <a href="javascript:void(0);" id="twt_connect" class="btn btn-primary pull-right connect">Connect</a>
                                        <?php else: ?>
                                          <a href="javascript:void(0);" id="twt_dconnect" class="btn btn-default pull-right disconnect">Disconnect</a>
                                        <?php endif; ?>
                                      </div>
                                  </div>

                          </div>
                    <?php endif; ?>
                </div>
                <?php /* if($count > 0): ?>
                    <div class="panel-footer row load-footer">
                        <div class="col-md-12">
                            <a href="javascript:void(0);" id="load_more_prop" class="btn btn-primary pull-right">View More</a>
                        </div>
                    </div>
                <?php endif; */ ?>
            </div>
        </div>
      </div>
     <!--  <div class="starter-template">
        <h1>Bootstrap starter template</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div> -->

    </div><!-- /.container -->

    <script type="text/javascript">
        $('#load_more_prop').click(function(){
          var offset = $('.property').length;
          // var category = $('#category').val();
          // var month = $('#mont').val();
          // var search = $('#search').val();
              // alert(offset); 
              // return false;
              $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>user/ajax_load_property/'+offset,            
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if(obj.status){
                      $('#prop_list').append(obj.resp);
                    }else{
                      $('#load_more_prop').hide();
                    }
                    
                    if($('.property').length >= $("#total_rows").val()){
                      $('#load_more_prop').hide();
                    }
                 }
              });
          });
    </script>

    <style type="text/css">
      .header{
          /*background-color:  #33AEBD !important;*/
          background-color: #33AEBD !important;
          border-radius: 0;
          color: #FFFFFF;
          font-size: 20px;
          margin: 0;
          padding: 20px;
          text-decoration: none;
      }

      .var-data{
        margin-left: 2%;
        padding: 2%;
        font-size: 20px;
      }

      .ok {
          color: #5CB85C !important;
          font-size: 20px;
      }

      .more-varfctn {
          color: #939393 !important;
          font-size: 20px;
      }

      .phone-content{
          margin: 0 0 9px;
          font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
          font-size: 13px;
          /*line-height: 18px;*/
      }

      .link-btns{
          margin: 0 !important;
          padding: 3% !important;
      }
    </style>
    <?php $auth = get_oauth_keys('facebook'); ?>
    <div id="fb-root"></div>

    <script type="text/javascript">
        ////////////////////////////////////////////////
        /////// Open id login page facebook starts////
        ///////////////////////////////////////////////
        window.fbAsyncInit = function() {
             //Initiallize the facebook using the facebook javascript sdk
             FB.init({
               appId:<?php echo $auth->app_id; ?>, // App ID //518108701641293 Test app id :563762117019040
               cookie:true, // enable cookies to allow the server to access the session
               status:true, // check login status
               xfbml:true, // parse XFBML
               oauth : true //enable Oauth 
             });
        };
       //Read the baseurl from the config.php file
       (function(d){
             var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement('script'); js.id = id; js.async = true;
             js.src = "//connect.facebook.net/en_US/all.js";
             ref.parentNode.insertBefore(js, ref);
        }(document));

        //Onclick for fb login
         jQuery('#fb_connect').click(function(e) {
              FB.login(function(response) {
                if(response.authResponse) {
                    window.location.href = '<?php echo base_url() ?>user/facebookConnect'; //redirect uri after closing the facebook popup
                }
           },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
        });

        $('#fb_dconnect').click(function (e) {
            window.location.href = '<?php echo base_url() ?>user/facebookDisconnect'; //redirect uri after closing the facebook popup
        });

        $('#google_connect').click(function (e) {
            window.location.href = '<?php echo base_url() ?>user/googleConnect'; //redirect uri after closing the facebook popup
        });

        $('#google_dconnect').click(function (e) {
            window.location.href = '<?php echo base_url() ?>user/googleDisconnect'; //redirect uri after closing the facebook popup
        });

        $('#twt_connect').click(function (e) {
            window.location.href = '<?php echo base_url() ?>twitter'; //redirect uri after closing the facebook popup
        });

        $('#twt_dconnect').click(function (e) {
            window.location.href = '<?php echo base_url() ?>twitter/twitterDisconnect'; //redirect uri after closing the facebook popup
        });
    </script>     