<?php $this->load->view('user/leftbar'); ?>
<style type="text/css">
  .zak_zak
  {
    box-shadow: 0.4px 0.4px 2px #B9B9B9 inset;
    margin-top: 6% !important;
    min-height: 600px;
    border-radius: 5px !important;
    padding-top: 0 !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
</style>

      <div class="col-lg-9">
        <div class="row content-top">
          <div class="welcome">
            <h3>Notifications</h3>
          </div>
          
           <?php $this->load->view('user/account/account_header_bar');  ?>

          <?php if($this->session->flashdata('error_msg')){ ?>
            <span style="padding:5px !important;  width:600px !important;" class="alert alert-danger "><?php echo $this->session->flashdata('error_msg'); ?></span>
          <?php } ?>
          <?php if($this->session->flashdata('success_msg')){ ?>
              <span style="padding:5px !important; width:600px !important; " class="alert alert-success "><?php echo $this->session->flashdata('success_msg'); ?></span>
          <?php } ?>


        <div class="links zak_zak" > 
        <table class="table">
          <thead>
            <tr>
              <th>&nbsp;#</th>
              <th style="width:20%">Message</th>
              <th style="width:15%">Subject</th>
              <th style="width:18%">Receiver Name</th>
              <th style="width:12%">Date Sent</th>
              <th style="width:180px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($notifications)){ $i=1; foreach ($notifications as $row){?>    
            <tr>
              <td><?php echo $counter; ?></td>
              <td><?php if(!empty($row->message)) echo word_limiter($row->message,5); ?></td>
              <td><?php if(!empty($row->subject))echo $row->subject;  ?></td>
              <td><?php if(!empty($row->name)) echo $row->name; ?></td>
              <td><?php if(!empty($row->created)) echo $row->created; ?></td>
              <td>
                  <a style="width:90px" id="<?php echo $row->id; ?>" class="btn btn-default view-msg" href="javascript:void(0);">
                    View
                  </a>
              </td>
            </tr>
            <?php $i++; $counter++; } } else{ ?>
              <tr><td colspan="6"> No Record Found</td></tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <span style="margin-left:20px"><?php if($pagination){echo  $pagination;} ?></span>
    </div>
  </div>
</div><!-- /.container -->

<div class="modal fade" id="see_msg">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title" id="note-subject"></h3>
        </div>
        <div class="panel-body" id="note-msg">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
        <!-- <input type="button" value="Confirm" id="confirm" class="btn btn-default"> -->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  $(document).ready(function() {
    $("#error_alert").hide();
  });

  // $('#add_event').on('hidden.bs.modal', function (e) {
  //   $("#confirm").fadeIn();
  // });

  // $('#add_event').on('show.bs.modal', function (e) {
  //   $("#error_alert").hide();
  // });

  $('.view-msg').on('click', function (e) {
      $("#error_alert").hide();
      id = $(this).attr('id');
      $.ajax({
        type: "POST",
        // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
        url: "<?php echo base_url() ?>user/ajax_get_msg",
        data: {id: id},
        success: function(response) {

          var obj = jQuery.parseJSON(response);
          // console.log(obj);
          if (obj.status) 
          {
            $("#note-subject").html(obj.sub);
            $("#note-msg").html(obj.msg);
            $('#see_msg').modal();
          } 
          else
          {
            $("#error_alert").show();
            $("#msg_error").html(obj.msg);
            $("#note-subject").html('');
            $("#note-msg").html('');
            // $("#calendar").fullCalendar( 'refetchEvents' )
          };
          
        }
      });
  });
</script>