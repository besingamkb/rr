<!-- 2 July Work -->

<?php $tool_tip_json = get_row('tool_tip',array('tool_tip_page'=>'tool_user_profile')); ?>
<?php $tool_tip = json_decode($tool_tip_json->page_data); ?>


<?php $this->load->view('user/leftbar'); ?>

      <div class="col-lg-9">
          <div class="row content-top">
              <div class="welcome">
                  <h3>Profile</h3>
              </div>
              <?php alert(); ?>
              <?php $this->load->view('user/account/profile_sub_header'); ?>
              <?php echo form_open(current_url(),array('class' =>'form-horizontal' ,'role'=>'form')); ?>
              <div class="panel panel-default">
                  <div class="panel-heading"> <!-- heading -->
                    <ul class="nav">
                      <li class="default">
                        <a href="javascript:void(0);">
                          <i class="glyphicon dashcon glyphicon-ok-sign ok"></i> Required
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="panel-body row"> <!--panel body start -->

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">First Name
                        <?php if(!empty($tool_tip->first_name)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->first_name ?>" ></span>
                        <?php endif; ?>
                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="first_name" value="<?php if(set_value('first_name')){ echo set_value('first_name');}else{echo $member->first_name;} ?>" style="width:80% !important;" class="form-control" id="inputEmail3" placeholder="First Name">
                           <span class="form_error"><?php echo form_error('first_name'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Last Name

                        <?php if(!empty($tool_tip->last_name)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->last_name ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="last_name" value="<?php if(set_value('last_name')){ echo set_value('last_name');}else{echo $member->last_name;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="Last Name">
                           <span class="form_error"><?php echo form_error('last_name'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">I Am
                   
                        <?php if(!empty($tool_tip->gender)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->gender ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <select name="gender" style="width:200px !important;"class="form-control">
                              <option value="">Gender</option>
                              <option <?php if($gender == 1){echo "selected='selected'";} ?> value="1">Male</option>
                              <option <?php if($gender == 2){echo "selected='selected'";} ?> value="2">Female</option>
                              <option <?php if($gender == 0){echo "selected='selected'";} ?> value="0">Other</option>
                            </select>
                          <span class="form_error"><?php echo form_error('gender'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Birth Date
                      
                        <?php if(!empty($tool_tip->dob)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->dob ?>" ></span>
                        <?php endif; ?>

                            </label>
                            <div class="col-sm-4">
                              <select name="month" id="month" style="width:200px;"class="form-control">
                                  <option value="">Month</option>
                                  <?php for ($i=1; $i<=12 ; $i++) { ?>
                                    <option <?php if($month == $i){echo "selected='selected'";} ?> value="<?php echo $i; ?>"><?php echo date("F", mktime(0, 0, 0, $i, 1, date('Y'))); ?></option>
                                  <?php } ?>
                                </select>
                              <span class="form_error"><?php echo form_error('month'); ?></span>
                            </div>
                            <div class="col-sm-3">
                              <select name="day" id="day" style="width:81px;"class="form-control">
                                  <option value="">Day</option>
                                  <?php for ($i=1; $i<=31 ; $i++) { ?>
                                    <option <?php if($day == $i){echo "selected='selected'";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                  <?php } ?>
                                </select>
                              <span class="form_error"><?php echo form_error('day'); ?></span>
                            </div>
                            <div class="col-sm-3">
                              <select name="year" id="year" style="width:120px;"class="form-control">
                                  <option value="">Year</option>
                                  <?php for ($i=1900; $i<=date('Y') ; $i++) { ?>
                                    <option <?php if($year == $i){echo "selected='selected'";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                  <?php } ?>
                                </select>
                              <span class="form_error"><?php echo form_error('year'); ?></span>
                            </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">User Email

                        <?php if(!empty($tool_tip->user_email)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->user_email ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="user_email" value="<?php if(set_value('user_email')){ echo set_value('user_email');}else{echo $member->user_email;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="User Email">
                           <span class="form_error"><?php echo form_error('user_email'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Contact

                        <?php if(!empty($tool_tip->contact)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->contact ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="phone" value="<?php if(set_value('phone')){ echo set_value('phone');}else{echo $member->phone;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="Contact">
                         <span class="form_error"><?php echo form_error('phone'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Address

                        <?php if(!empty($tool_tip->address)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->address ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="address" value="<?php if(set_value('address')){ echo set_value('address');}else{echo $member->address;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="Address">
                          <span class="form_error"><?php echo form_error('address'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">City

                        <?php if(!empty($tool_tip->city)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->city ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="city"  value="<?php if(set_value('city')){ echo set_value('city');}else{echo $member->city;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="City">
                         <span class="form_error"><?php echo form_error('city'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">State

                        <?php if(!empty($tool_tip->state)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->state ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text"  name="state" value="<?php if(set_value('state')){ echo set_value('state');}else{echo $member->state;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="State">
                           <span class="form_error"><?php echo form_error('state'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Country
                   
                        <?php if(!empty($tool_tip->country)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->country ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <?php $countries = get_country_array(); ?>
                        <div class="col-sm-10">
                          <select name="country" style="width:200px !important;"class="form-control">
                              <option value="">Select country</option>
                              <?php foreach ($countries as $code => $name): ?>
                              <option <?php if($code == $country){echo "selected='selected'";} ?> value="<?php echo $code; ?>"> <?php echo $name; ?> </option>
                              <?php endforeach ?>
                            </select>
                          <span class="form_error"><?php echo form_error('country'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Zip

                        <?php if(!empty($tool_tip->zip)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->zip ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="zip" value="<?php if(set_value('zip')){ echo set_value('zip');}else{echo $member->zip;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="Contact">
                           <span class="form_error"><?php echo form_error('zip'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Paypal Email
                       
                        <?php if(!empty($tool_tip->paypal_email)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->paypal_email ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text"  name="paypal_email" value="<?php if(set_value('paypal_email')){ echo set_value('paypal_email');}else{echo $member->paypal_email;} ?>" style="width:80% !important;" class="form-control" id="inputPassword3" placeholder="paypal email">
                           <span class="form_error"><?php echo form_error('paypal_email'); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Describe Yourself</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="82" class="col-sm-10 form-control" name="description" style="color:#777777" ><?php if(set_value('description')){ echo set_value('description');}else{echo $member->description;} ?></textarea>
                            <span class="form_error span12"><?php echo form_error('description'); ?></span>  
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-offset-2 col-sm-10">
                          <p>Bnbclone is built on relationships. Help other people get to know you.</p>
                          <p>Tell them about the things you like: What are 5 things you can’t live without? Share your favorite travel destinations, books, movies, shows, music, food.</p>
                          <p>Tell them what it’s like to have you as a guest or host: What’s your style of traveling? Of Bnbclone hosting?</p>
                          <p>Tell them about you: Do you have a life motto?</p>
                        </div>
                      </div>
                  </div> <!--panel body end -->
              </div>

              <div class="panel panel-default">
                  <div class="panel-heading"> <!-- heading -->
                    <ul class="nav">
                      <li class="default">
                        <a href="javascript:void(0);">
                          <i class="glyphicon dashcon glyphicon-plus-sign more-varfctn"></i> Optioanl
                        </a>
                      </li>
                    </ul>
                  </div><!-- phone -->
                  <div class="panel-body row"> <!-- Panel body start -->
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">School

                        <?php if(!empty($tool_tip->school)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->school ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="school" value="<?php if(set_value('school')){ echo set_value('school');}else{echo $member->school;} ?>" style="width:80% !important;" class="form-control" placeholder="School">
                         <span class="form_error"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Work
                      
                        <?php if(!empty($tool_tip->work)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->work ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <div class="col-sm-10">
                          <input type="text" name="work" value="<?php if(set_value('work')){ echo set_value('work');}else{echo $member->work;} ?>" style="width:80% !important;" class="form-control" placeholder="e.g. Bnbclone / Apple / Taco Stand">
                          <span class="form_error"></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Timezone

                        <?php if(!empty($tool_tip->timezone)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->timezone ?>" ></span>
                        <?php endif; ?>

                        </label>
                        <?php $timezones = get_timezone_array(); ?>
                        <div class="col-sm-10">
                          <select name="timezone" style=""class="form-control">
                              <option value="">Select Timezone</option>
                              <?php foreach ($timezones as $key => $value): ?>
                              <option <?php if($key == $member->timezone){echo "selected='selected'";} ?> value="<?php echo $key; ?>"> <?php echo $value; ?> </option>
                              <?php endforeach ?>
                            </select>
                          <span class="form_error"><?php echo form_error('timezone'); ?></span>
                        </div>
                      </div>

                        <div class="form-group">
                            <label for="inputLangauge3" class="col-sm-2 control-label">Language

                        <?php if(!empty($tool_tip->language)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->language ?>" ></span>
                        <?php endif; ?>

                            </label>
                            <div class="col-sm-10 row" id="append-lang">
                                <?php $languageArray = array(); ?>
                                <?php if($language): ?>
                                    <?php foreach($language as $lang): ?>
                                          <div class="col-sm-4 lang-box" id="div_<?php echo $lang->id; ?>">
                                            <div class="input-group">
                                              <span class="input-group-addon"> <?php $languageArray[] = $lang->lang_id; echo get_lang_name($lang->lang_id); ?></span>
                                              <span class="input-group-addon">
                                                <button type="button" id="lang_<?php echo $lang->id; ?>" class="btn btn-default btn-xs lang-btn"><i class="glyphicon dashcon glyphicon-remove-circle"></i></button>
                                              </span>
                                            </div><!-- /input-group -->
                                          </div><!-- /.col-lg-6 -->
                                    <?php endforeach; ?>
                                <?php else: ?>
                                  (None)
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-5 btn-div">
                                <a href="javascript:void(0);" id="show-lang-popup">
                                  <i class="glyphicon dashcon glyphicon-plus-sign add-more"></i> Add More
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputLangauge3" class="col-sm-2 control-label">Groups

                        <?php if(!empty($tool_tip->groups)): ?>
                          <span class="glyphicon glyphicon-question-sign" title="<?php echo $tool_tip->groups ?>" ></span>
                        <?php endif; ?>

                            </label>
                            <div class="col-sm-10 row" id="append-group">
                                <?php if($groups): ?>
                                    <?php foreach($groups as $group): ?>
                                        <div class="col-sm-4 lang-box" id="group_<?php echo $group->relt_id; ?>">
                                          <div class="input-group">
                                            <span class="input-group-addon"> <?php echo $group->group_name; ?></span>
                                            <span class="input-group-addon">
                                              <button type="button" id="grp_<?php echo $group->relt_id; ?>" class="btn btn-default btn-xs group-btn"><i class="glyphicon dashcon glyphicon-remove-circle"></i></button>
                                            </span>
                                          </div><!-- /input-group -->
                                        </div><!-- /.col-lg-6 -->
                                    <?php endforeach; ?>
                                <?php else: ?>
                                  (None)
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-5 btn-div">
                                <a href="<?php echo base_url(); ?>user/groups">
                                  See all groups 
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary save-btn">Save Changes</button>
                          </div>
                        </div>

                      </div>
                  </div> <!-- Panel body end -->
            </div>
            <?php echo form_close(); ?>
      </div>

    </div><!-- /.container -->

    <div class="modal fade" id="lang-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Spoken Languages</h4>
                </div>
                <div class="modal-body">
                    <p>What languages can you speak fluently? We have many international travelers who appreciate hosts who can speak their language.</p>
                    <!-- <input type="button" value="Confirm" id="confirm" class="btn btn-default"> -->
                    <div class="col-sm-12 row model-content">
                      <?php $lang = get_language_array();  ?>
                      <?php $division = intval(count($lang)/2); ?>
                      <?php if ($lang):  ?>
                          <?php echo form_open(current_url(),array('id' =>'checkbox-form' ,'role'=>'form')); ?>
                              <div class="col-sm-6">
                              <?php $i=1; foreach($lang as $id => $name):  ?>
                                  <div class="input-group">
                                    <span class="input-group-addon prefix-input">
                                      <input type="checkbox" class="lang_arr" name="chkbox[]" value="<?php echo $id; ?>" <?php if(!empty($languageArray)){ if(in_array($id , $languageArray)) { echo 'checked = "checked"'; } } ?>>
                                    </span>
                                    <p class="navbar-text"><?php echo $name; ?></p>
                                  </div><!-- /input-group -->
                                  <?php if($division == $i): ?>
                                    </div>
                                    <div class="col-sm-6">
                                  <?php endif;  ?>
                              <?php $i++; endforeach;  ?>
                              </div>
                          <?php echo form_close(); ?>
                      <?php endif;  ?>
                    </div>
                    <br/><div id="response"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" value="Confirm" id="add-lang" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <style type="text/css">
        .form-group {
            padding: 3%;
        }

        .lang-box{
          padding: 2%;
        }
        
        .lang-btn {
            color: #848484 !important;
        }

        .input-group-addon, .input-group-btn {
            width: 0 !important;
        }

        .input-group-addon {
            padding: 0 2px !important;
        }

        .btn-div{
            margin-top: 5px !important;
            padding: 0 !important;
        }

        .add-more {
            color: #5CB85C !important;
            font-size: 13px;
        }
        .model-content {
          height: 347px !important;
          overflow: scroll;
          /*width: 100%;*/
        }

        .prefix-input {
          border: none !important;
        }

        .save-btn {
          margin-top: 4% !important;
        }

    </style>

    <script type="text/javascript">
        var success = false;

        $(document).on("click", "#show-lang-popup", function() {
            $('#lang-popup').modal();
        });

        $('#lang-popup').on('hidden.bs.modal', function (e) {
          if (success) {
            // window.location.href = '<?php base_url() ?>user/profile';
            location.reload(true);
          };
        });

        $('#lang-popup').on('show.bs.modal', function (e) {
          $("#response").html('');
          $("#error_alert").hide();
        });//show-lang-popup

        $(document).on("click", "#add-lang", function() {
          var data = $("#checkbox-form").serializeArray();
          // console.log(data);
          $.ajax({
            type: "POST",
            // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
            url: "<?php echo base_url() ?>user/ajax_add_language",
            data: { data: data },
            success: function(response) {

              var obj = jQuery.parseJSON(response);
              // console.log(response);
              if (obj.status) 
              {
                $("#response").html('<p class="text-success">'+ obj.msg +'</p>');
                success = true;
              } 
              else
              {
                $("#response").html('<p class="text-danger">'+ obj.msg +'</p>');
                success = false;
              };
              
            }
          });
        });

        $(document).on("click", ".lang-btn", function() {
            var data = $(this).attr('id');
            var id = data.split('_');
            // alert(id[1]);
            console.log(id[1]);
            $.ajax({
              type: "POST",
              // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
              url: "<?php echo base_url() ?>user/ajax_remove_language",
              data: { id: id[1] },
              success: function(response) {

                var obj = jQuery.parseJSON(response);
                // console.log(response);
                if (obj.status) 
                {
                  $('#div_' + id[1]).fadeOut(2000);
                } 
                else
                {
                  //nothing
                };
                
              }
            });
        });

        $(document).on("click", ".group-btn", function() {
            var data = $(this).attr('id');
            var id = data.split('_');
            // alert(id[1]);
            console.log(id[1]);
            $.ajax({
              type: "POST",
              // url: "php/ajax.php?r=confirm_booking&start="+start+"&end="+end,
              url: "<?php echo base_url() ?>user/ajax_remove_groups",
              data: { id: id[1] },
              success: function(response) {

                var obj = jQuery.parseJSON(response);
                // console.log(response);
                if (obj.status) 
                {
                  $('#group_' + id[1]).fadeOut(2000);
                } 
                else
                {
                  //nothing
                };
                
              }
            });
        });
    </script>