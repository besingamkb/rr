<?php $this->load->view('user/leftbar'); ?>

<style type="text/css">
  .zak_zak
  {
    margin-top: 6% !important;
    min-height: 600px;
    border-radius: 5px !important;
  }
  .far_far
  {
    margin-left: 2%;
    margin-top: 2%;
  }
  .far
  {
    margin-left: 5%
  }
  .far_far a:hover
  {
    cursor: pointer;
    text-decoration: none !important;
  }
</style>

     <div class="col-lg-9">
          <div class="row content-top">
               <div class="welcome">
                    <h3>Transaction History</h3>
               </div>

           <?php $this->load->view('user/account/account_header_bar');  ?>

               <?php alert(); ?>
               <div class="row-fluid zak_zak">
                    <div class="span12">
                         <div class="widget no-margin">
                              <div class="widget-body">
                                   <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover pull-left" id="data-table">    
                                             <thead>
                                                  <tr>
                                                       <th style="width:20%;padding-left:15px">Payment Type</th>  
                                                       <th style="width:20%">Paid by</th>                                                         
                                                       <th style="width:20%">amount</th>
                                                       <th style="width:20%">Payment Source</th>
                                                       <th style="width:20%">created</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                             <?php if(!empty($transaction_history)): ?>
                                             <?php $i=1; foreach ($transaction_history as $row):?>
                                                  <tr> 
                                                       <?php $pr_detail = property_details($row->property_id); ?>  
                                                       <?php $guest = get_user_info($row->bookedbyuser); ?>
                                                      
                                                       <td style="padding-left:15px" ><?php if ($row->payment_type == 1){ echo "Booking"; }else{ echo "Referral"; } ?></td>                                                       
                                                       <td ><?php echo $guest->first_name." ".$guest->last_name; ?></td>                                                                         
                                                       <td ><?php echo $row->amountearnedbyuser ?></td>                                                                         
                                                       <td><?php echo $row->paywith ?></td>
                                                       <td>
                                                           <?php echo $row->created ?>
                                                       </td>
                                                  </tr>
                                             <?php $i++; endforeach; ?>
                                             <?php else: ?>
                                                  <tr style="margin-left:15px">
                                                       <td colspan="5"> No Records Found</td>
                                                  </tr> 
                                             <?php endif; ?>
                                             </tbody>
                                        </table>
                                        <?php if($transaction_history): ?>
                                        <div style="margin-left:15px;">
                                             <?php if($pagination) echo $pagination; ?>
                                        </div>
                                   </div>
                                   <?php endif; ?>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
