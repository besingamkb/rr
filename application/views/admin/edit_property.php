<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>
 
 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Listing Property
            </div>
          </div>
          <div class="widget-body">

             <?php echo form_open_multipart(current_url(), array('id' => 'testr' , 'class' => 'form-horizontal no-margin well','onsubmit'=>'get_lat_long();')); ?>
            <?php //echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Property Title
                </label>
                <div class="controls controls-row span8">
                  <input name="property_title" value="<?php echo $properties->title;?>" class="input-block-level" type="text" placeholder="property title" value="<?php echo set_value('property_title'); ?>">
                  <span class="form_error span12"><?php echo form_error('property_title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                Property Description
                </label>
                <div class="controls controls-row span8">
                 <textarea rows="5" class="span12" name="property_descrip" ><?php echo $properties->description;?></textarea>
                  <span class="form_error span12"><?php echo form_error('property_descrip'); ?></span>  
                </div>
              </div>

               <div class="control-group">
                <label class="control-label">
                  Details about this property
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <?php $property_type = get_property_types(); ?> 
                    <div class="span4">
                      <select name="property_type">
                      <option value="">Select Property Type</option>
                      <?php foreach ($property_type as $row): ?>
                        <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->property_type_id)){ if($pr_info->property_type_id == $row->id)echo"selected"; }  ?>> <?php echo $row->property_type; ?> </option>
                      <?php endforeach ?>
                      </select>                      
                      <span class="form_error span12"><?php echo form_error('property_type'); ?></span>

                    </div>
                  <?php $bed_type = get_bed_types(); ?>
                  <div class="span4">
                     <select name="bed_type">
                       <option value="">Select Bed Type</option>
                      <?php foreach ($bed_type as $row): ?>
                        <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->bed_type)){ if($pr_info->bed_type == $row->id)echo"selected"; }  ?> > <?php echo $row->bed_type; ?> </option>
                      <?php endforeach ?>
                     </select>
                     <span class="form_error span12"><?php echo form_error('bed_type'); ?></span>
                  </div>

                  <div class="span3">
                     <input type="text" placeholder="Accommodates" name="accommodates" value="<?php echo $pr_info->accommodates;  ?>">
                    <span class="form_error span12"><?php echo form_error('accommodates'); ?></span>    
                  </div> 
                </div>
              </div>
            </div>
            <br>
              <div class="control-group">
                <label class="control-label">
                    &nbsp;
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <?php $room_type = get_room_type(); ?>
                    <div class="span4">
                      <select class="input-block-level" name="room_type">
                        <option value="">Select Room Type</option>
                        <?php foreach ($room_type as $row): ?>
                        <option value="<?php echo $row->id; ?>" <?php if($pr_info->room_type == $row->id) echo 'selected="selected"'; ?> > <?php echo $row->room_type; ?> </op tion>
                        <?php endforeach ?>
                      </select>
                      <span><?php echo form_error('room_type'); ?></span>
                    </div>


                    <div class="span4">
                      <input name="bedroom" class="input-block-level" type="text" placeholder="Bedroom" value="<?php echo $pr_info->bed;  ?>">                 
                      <span class="form_error span12"><?php  echo form_error('bedroom');  ?></span>
                    </div>
                    <div class="span4">
                        <input name="bathroom" class="input-block-level" type="text" placeholder="Bathroom" value="<?php echo $pr_info->bath;  ?>">                 
                        <span class="form_error span12"><?php  echo form_error('bathroom');  ?></span>
                    </div>
                </div>
              </div>
            </div>  

             <div class="control-group">
                <label class="control-label">
                  &nbsp;
                </label>
                <div class="controls controls-row span8">
                    <input name="address" class="input-block-level" value="<?php if(!empty($pr_info->unit_street_address)) echo $pr_info->unit_street_address;  ?>" type="text" placeholder="Property Address" value="<?php echo set_value('address'); ?>" id="address" >                 
                    <span class="form_error span12"><?php  echo form_error('address');  ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  &nbsp;
                </label>

                <div class="controls controls-row span8">
                  <div class="row-fluid">
                  <div class="span4">
                    <?php $cities = get_cities_info(); ?>
                    <select onchange="get_neighbourhoot()" class="input-block-level" id="city" name="city"  required="required">
                        <option value="">Select city</option>
                        <?php if(!empty($cities)): ?>
                        <?php foreach($cities as $city): ?>
                        <option value="<?php echo $city->city; ?>" <?php if($pr_info->city == $city->city)echo "selected"; ?>  class="<?php echo $city->id; ?>"><?php echo $city->city; ?></option> 
                        <?php endforeach ?>
                        <?php endif; ?>
                    </select>
                    <span><?php echo form_error('city'); ?></span> 
                  </div>
                  <script>
                    function get_neighbourhoot()
                    {
                        var city = $('#city option:selected').attr('class');
                        $.ajax({
                                type:'POST',
                                 url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city',
                                data:{city:city},
                             success:function(res)
                             {
                                $('#neighbourhoot').html(res);
                             }
                        });
                    }
                  </script>
                  <div class="span4">
                    <select name="neighbourhood[]" id="neighbourhoot"  class="input-block-level" multiple="multiuple">
                        <option>Select neighbour</option>
                    </select>
                    <span><?php echo form_error('neighbourhood'); ?></span>
                  </div>

                <?php $country = get_country_array(); ?>
                <div class="span4">
                  <select id="country" name="country" class="input-block-level">
                      <option value="">Select country</option>
                      <?php foreach ($country as $code => $name): ?>
                      <option value="<?php echo $name; ?>" <?php if(!empty($pr_info->country)){ if($pr_info->country == $name)echo"selected"; }  ?>  > <?php echo $name; ?> </option>
                      <?php endforeach ?>
                  </select>
                  <span class="form_error "><?php echo form_error('country'); ?></span>
                </div>

                </div>
                </div>
              </div>

              <br>

              <div class="control-group">
                <label class="control-label">
                    &nbsp;
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                  <div class="span4">
                   <input name="state" id="state" type="text" value="<?php if(!empty($pr_info->state)) echo $pr_info->state;  ?>"  placeholder="State" value="<?php echo set_value('state'); ?>">
                      <span class="form_error"><?php echo form_error('state'); ?></span>
                  </div> 
                  <div class="span4">
                    <input type="text" value="<?php if(!empty($pr_info->zip)) echo $pr_info->zip;  ?>" placeholder="zip" name="zipcode">
                    <span class="form_error span12"><?php echo form_error('zipcode'); ?></span>
                  </div>
                  <div class="span4">
                    <input type="text" value="<?php if(!empty($pr_info->square_feet)) echo $pr_info->square_feet; ?>" placeholder="Property Size" name="size">
                    <span class="form_error span12"><?php echo form_error('size'); ?></span>
                  </div>
                </div>
              </div>
            </div> 


            <input  type="hidden" placeholder="latitude"  id="latitude"  name="latitude">
           
            <input  type="hidden" placeholder="longitude" id="longitude" name="longitude">

            <!--   <br>
            <div class="control-group">
                <label class="control-label">
                Property Description
                </label>
                <div class="controls controls-row span8">
                    <div class="row-fluid">
                      <div class="span3">
                       <input class="span12"  type="text" placeholder="Rent" name="city">
                      </div>
                      <div class="span3">
                        <select class="span12" name="Room Allotment">
                          <option>Room Allotment</option>
                          <option>3bhk</option>
                          <option>2bhk</option>
                          <option>2floor</option>
                        </select>
                      </div>
                      <div class="span3">
                        <input class="span12"  type="text" placeholder="security Deposit" name="deposit">
                      </div>
                      <div class="span3">
                        <input class="span12"  type="text" placeholder="Application Fee" name="appliction_fee">
                      </div> 
                    </div>
                </div>
           </div>


            <br>

            <div class="control-group">
                <label class="control-label">
                  Amenity
                </label>
                <div class="controls controls-row span8">
                 <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                  <div class="unique_feature"> <input type="checkbox" class="ameny[]"  value="<?php echo  $row->name; ?>"> <?php echo  $row->name; ?></div>
                 <?php } ?>
                </div>
            </div> 

            <div class="control-group">
                <label class="control-label">
                  Contact information for this listing
                </label>
                <div class="controls controls-row span8">

                  <div class="row-fluid">
                  <div class="span8">
                   <input  class="input-block-level span12" type="text" placeholder="Contact Name" name="contact_name">
                  </div>

                  <div class="span4">
                    <input class="input-block-level span12" type="text" placeholder="Phone" name="phone">
                  </div>
                
                </div>
              </div>
            </div>       -->



            <div class="form-actions no-margin">
              <button type="submit" class="btn btn-info">
              Next >>
              </button>
            </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function get_lat_long(){
      var flag = false;
      var address  = document.getElementById("address").value;
      var city     = document.getElementById("city").value;
      var state    = document.getElementById("state").value;
      var country  = document.getElementById("country").value;
      var palace =address+' '+city+' '+state+' '+country; 
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode( { 'address': palace}, function(results, status){
        var location = results[0].geometry.location;
        // alert(location.lat() +''+ location.lng());
        // return false();
        var lat = location.lat();
        var lng = location.lng();
        document.getElementById("latitude").value = lat;
        document.getElementById("longitude").value = lng;
        
        $("#testr").attr('onsubmit' , 'return true;');
        $("#testr").submit();

      });
    }
  </script>