  
    <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Group
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Group Name
                </label>
                <div class="controls controls-row span6">
                  <input name="group_name" class="span12" type="text" placeholder="Group Name" value="<?php echo set_value('group_name'); ?>">
                  <span class="form_error span12"><?php echo form_error('group_name'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-description">
                  Group Description
                </label>
                <div class="controls controls-row span6">
                  <input name="group_description" class="span12" type="text" placeholder="Group Description" value="<?php echo set_value('group_description'); ?>">
                  <span class="form_error span12"><?php echo form_error('group_description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Group Banner
                </label>
                <div class="controls controls-row span6">
                  <input name="banner" class="span12" type="file">
                  <span class="form_error span12"><?php if($this->session->flashdata('image_error')) echo $this->session->flashdata('image_error');  ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>