<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>
 
 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Property
            </div>
          </div>
          <div class="widget-body">
           
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
           <br>

           <div class="control-group">
                <label class="control-label">
                    Allotment
                </label>
                <div class="controls controls-row span8">
              
  <div class="row-fluid">
    
    <div class="span3">
      <input class="span12"  type="text" placeholder="How Much" name="appliction_fee">
      <span class="form_error span12"><?php echo form_error('appliction_fee'); ?></span>
    </div> 


    <div class="span3">
      <select  class="span12" name="room_allotment">                            
          <option value="">Room Allot</option>
          <option value="1">per hr.</option>
          <option value="2">per half day</option>
          <option value="3">per day</option>
          <option value="4">per night</option>
          <option value="5">per week</option>
          <option value="6">per month</option>
          <option value="7">per ticket</option>
          <option value="8">per time</option>
          <option value="9">per trip</option>
          <option value="10">per unit</option>
      </select>
      <span class="form_error span12"><?php echo form_error('room_allotment'); ?></span>
    </div>

    <div class="span3">
       <input class="span12"  type="text" placeholder="How Many" name="rent">
       <span class="form_error span12"><?php echo form_error('rent'); ?></span>
    </div>

    <div class="span3">
      <input class="span12"  type="text" placeholder="security Deposit" name="deposit">
    <span class="form_error span12"><?php echo form_error('deposit'); ?></span>
    </div>



                    </div>
                </div>
           </div>


           <?php $category=get_collection_category();  ?>
            <div class="control-group">
                <label class="control-label">
                  Collection category
                </label>
                <div class="controls controls-row span8">
                 <select name="collection_category">
                      <option value="">Select Category</option>
                      <?php foreach ($category as $row): ?>
                      <option value="<?php echo $row->id; ?>"> <?php echo $row->category_name; ?> </option>
                      <?php endforeach ?>
                  </select>
                  <span class="form_error "><?php echo form_error('collection_category'); ?></span>
                </div>

            </div> 
            <br>
            <br>

            <div class="control-group" style="margin-bottom:0px">
                <label class="control-label">
                  Featured Listing
                </label>
                <div class="controls controls-row span8" style="margin-top:0.55%">
                  <div class="unique_feature"> <input type="checkbox" name="featured_listing"  value="1"></div>
                </div>

            </div> 
            <!-- <br>
            <br> -->
            <div class="control-group" style="margin-bottom:0px" >
                <label class="control-label">
                  Amenity
                </label>
                <div class="controls controls-row span8" style="margin-top:0.55%">
                 <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                  <div class="unique_feature"> <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>"> <?php echo  $row->name; ?></div>
                 <?php } ?>
                </div>

            </div> 
            <!-- <br>
            <br> -->

              <div class="control-group">
                <label class="control-label" >
                  Users
                </label>
                <div class="controls controls-row span8">
                   <input class="span12" id="user" name="user_email" type="text"  placeholder="User Email">
                  <!-- <select onchange="return get_user_info();" id="user" name="user_email" class="span12 input-left-top-margins">
                      <option value="">Select User</option>
                      <?php foreach ($users as $row): ?>
                      <option value="<?php //echo $row->id; ?>"><?php echo $row->user_email; ?></option>
                      <?php endforeach; ?>
                  </select> -->
                  <span class="form_error span12"><?php  echo form_error('user_email'); ?></span>
                </div>
              </div>

            <div class="control-group">
                <label class="control-label">
                  Contact information for this listing
                </label>
                <div class="controls controls-row span8">

                  <div class="row-fluid">
                  <div class="span8">
                   <input  id="contact_name"  class="input-block-level span12" type="text" placeholder="Contact Name" name="contact_name">
                  <span class="form_error span12"><?php echo form_error('contact_name'); ?></span>
                  </div>

                  <div class="span4">
                    <input class="input-block-level span12" type="text" id="phone" placeholder="Phone" name="phone">
                  <span class="form_error span12"><?php echo form_error('phone'); ?></span>
                  </div>
                
                </div>
              </div>
            </div>
              <div class="control-group">
                 <label class="control-label">
                    &nbsp;
                </label>
              <div class="controls controls-row span8" style="margin-left:10px">

               <div class="row-fluid">
                   <input  type="hidden" placeholder="Contact Email" id="user_id" name="user_id">
                   <!-- <div class="span4">
                        
                       <span class="form_error span12"><?php //echo form_error('contact_email'); ?></span>
                    </div> -->
                    <div class="span4 ">
                        <input type="text" class="input-block-level span12" id="confirm_email" placeholder="Confirm Email" name="confirm_email">
                        <span class="form_error span12"><?php echo form_error('confirm_email'); ?></span>
                    </div>
                    <div class="span4">
                        <input type="text" class="input-block-level span12"  placeholder="Website" name="website" >
                        <span class="form_error span12"><?php echo form_error('website'); ?></span>
                    </div>
              </div>
            </div>
          </div>
          <!--  <div class="control-group">
                <label class="control-label">
                 Property Image
                </label>
                <div class="controls controls-row span8">
                 <input type="file" name="pr_image" >
                  <span class="form_error span12"><?php if($this->session->flashdata('image_error')) echo $this->session->flashdata('image_error');  ?></span>
                </div>

            </div>  -->
             <div class="control-group">
                <label class="control-label">
                 Property Image
                </label>
                <div class="controls controls-row span8">
                  <div class="span8" id="jquery-wrapped-fine-uploader" style="border:1px solid #999;min-height:150px;"> 
                      DRAG AND DROP
                  </div> 

                </div>
                  <div id="triggerUpload" class="btn btn-primary" style="margin-top: 10px; margin-left:-9.5%">
                      Upload now
                  </div>  

            </div> 

            <div class="control-group">
                <label class="control-label">
                
                </label>
                <div class="controls controls-row span8">
                  <ul class="thumbnails">
                   

                  </ul>
               </div>
                </div>

            </div> 


            <div class="form-actions no-margin">
              <button type="submit" onclick="return checkbox_valid();" class="btn btn-info">
                Save
              </button>
            </div>
          
            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $users = get_all_user();?>
  <script type="text/javascript">
   function get_user_info(){
   var user_id = document.getElementById('user').value;
    // alert(user_id);
    $.post( "<?php echo base_url()?>admin/user_info",{'user_id':user_id })
        .done(function( data ) {
       if(data !='No Record Found'){  
          var user =   $.parseJSON(data);
          document.getElementById('confirm_email').value = user['user_email'];
          document.getElementById('contact_name').value = user['first_name']+' '+user['last_name'];
          document.getElementById('phone').value = user['phone'];
          document.getElementById('user_id').value = user['id'];
        }
        else{
          alert("No Record Found");
        }

      });
   }
  </script>
  <script>
  $(function(){
      var user = new Array();
      <?php foreach($users as $row){ ?>
              user.push('<?php echo $row->user_email; ?>');
      <?php } ?>
    $( "#user" ).autocomplete({
        lookup: user,
        onSelect: function(suggestion){
           var user_email = document.getElementById('user').value; 
           $.post( "<?php echo base_url()?>admin/user_info",{'user_email':user_email })
                  .done(function( data ) {
                 if(data !='No Record Found'){  
                    var user =   $.parseJSON(data);
                    document.getElementById('confirm_email').value = user['user_email'];
                    document.getElementById('contact_name').value = user['first_name']+' '+user['last_name'];
                    document.getElementById('phone').value = user['phone'];
                    document.getElementById('user_id').value = user['id'];
                  }
                  else{
                    alert("No Record Found");
                  }
          });
        }
     });
  });  
  </script>
   <script>
      jQuery(document).ready(function (){
      
        var resp='';
      var uploade ='';    
        
   uploade=jQuery('#jquery-wrapped-fine-uploader').fineUploader({
                request: {
                  endpoint: '<?php echo base_url() ?>upload_handler/fineupload', 
                }, validation: {
                  allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
                },
                autoUpload: false
               
              }).on('complete', function(event, id, fileName, responseJSON) {
                

                if(responseJSON.success){

                  jQuery(this).append('<input type="hidden" name="property_image[]" value="'+fileName+'">');
                   resp ='<li>'+
                   '<input type="radio" name="featured_image" value="'+fileName+'"> <span>Featured Image</span>' +
                          '<a href="#" class="thumbnail" style="margin-top:10%">'+
                            '<img data-src="<?php echo base_url() ?>holder.js/160x120" alt="160x120" style="width: 160px; height: 120px;" src="<?php echo base_url();?>assets/uploads/property/'+fileName+'">'+
                          '</a>'+
                         '</li>';

                  $('.thumbnails').append(resp);
                }
              });
          
          $('#triggerUpload').click(function() {
              uploade.fineUploader('uploadStoredFiles');
          });
    


        //  jQuery('#jquery-wrapped-fine-uploader2').fineUploader({
        //   request: {
        //     endpoint: '<?php echo base_url() ?>upload_handler/fineupload', 
        //   }, validation: {
        //     allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
        //     itemLimit: 1
        //   }
        // }).on('complete', function(event, id, fileName, responseJSON) {
        //   if (responseJSON.success) {
        //     jQuery(this).append('<input type="hidden" name="footprint_image" value="'+fileName+'">');
        //   }
        // });

      });
    </script>  

    <style type="text/css">
      .unique_feature {
          color: #777777;
          float: left;
          /*height: 50px;*/
          padding: 0 5px 10px;
          width: 126px;
      }

      input[type="checkbox"] {
        margin-top: -3px;
      }


      input[type="radio"] {
        margin-top: -3px;
      }

       .control-group{
        margin-bottom: 0px !important;
       }


      .qq-upload-button{
        background-color: #e05284;
      }
    </style>

