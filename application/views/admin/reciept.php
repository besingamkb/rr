
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Booking Details
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php //echo form_open_multipart(current_url(), //array('class' => 'form-horizontal no-margin well')); ?> -->
             
        <?php $pro_info = get_property_detail($booking->pr_id); 
              $user = get_user_info($pro_info->user_id);  ?>
            <div class="form-horizontal no-margin well" style="color:#737373; " >
              <div class="row">
                <div class="">
                  <div class="navbar itinenarybtn">
                    <div class="navbar-inner">
                      <!-- <a class="brand" href="#">Title</a> -->
                      <ul class="nav" style="text-align: center !important;">
                        <center>
                          <li style="padding-left:0%" class=""><i class=" icon-white icon-envelope"></i> &nbsp;&nbsp;<a href="<?php echo base_url() ?>admin/booking_detail_email/<?php echo $booking->id ?>/receipt_page"><b>Email Itinenary</b></a></li>
                          <li class=""><i class=" icon-white icon-print"></i>&nbsp; &nbsp;<a href="<?php echo base_url() ?>export/export_booking_receipt/<?php echo $booking->id ?>"><b>Print Itinenary</b></a></li>
                          <li class=""><i class="icon-list-alt"></i>&nbsp;&nbsp;<a href="#"><b>View Reciept</b></a></li>                        
                        </center> 
                      </ul>
                    </div>
                  </div>
                  <!-- <a href="" class="btn btn-large">Email Itinenary</a> -->
                </div>
              </div>
                <div class="row">
                  <div class=" " style="margin-left:1.5%">
                        <div class="span12" style="border-bottom:1px solid #ccc; margin-left:0px; border-top:1px solid #CCC; padding:4% 4% 4%;">                       
                        
<!--                          <div class="span3 pull-right" style="text-align:right">
                            <h3>Add Your Logo</h3>
                            <p>21 Park Street, Newyork NY USA<br/>
                            Wed, March 19, 2013</p>
                         </div>
 -->                        
              <?php $customer = get_user_info($booking->customer_id); ?>
              <?php $owner = get_user_info($booking->owner_id); ?>
              <?php 
                $startTimeStamp = strtotime($booking->check_in);
                $endTimeStamp = strtotime($booking->check_out);
                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                // and you might want to convert to integer
                $numberDays = intval($numberDays+1);
                // $interval = date_diff($startTimeStamp, $endTimeStamp);
               ?>

                          <h3 >
                            Customer Reciept                            
                          </h3>
                          <h4>Booking Number - #<?php echo $booking->id; ?></h4>
                           <table class="table table-bordered">
                            <th style="width:25%">NAME</th>
                            <TH style="width:25%">DESTINATION</TH>
                            <th style="width:25%">DURATION</th>
                            <th style="width:25%">ACCOMMODATION TYPE</th>
                            <tr>
                              <td>
                                Dear, <?php echo ucfirst($customer->first_name." ".$customer->last_name); ?>
                              </td>
                              <td><?php echo $pro_info->city ?></td>
                              <td><?php echo  $numberDays ?> Nights</td>
                              <td><?php echo get_property_type($pro_info->property_type_id) ?></td>
                            </tr>
                           </table>

                           <table class="table table-bordered">
                            <th style="width:25%">ACCOMMODATION ADDRESS</th>
                            <th style="width:25%">ACCOMMODATION HOST</th>                            
                            <th style="width:25%">CHECK IN</th>
                            <th style="width:25%">CHECK OUT</th>                            
                            <tr>
                              <td><?php echo $pro_info->unit_street_address." ".$pro_info->city ?></td>
                              <td><?php echo $owner->first_name.' '.$owner->last_name ?><br>+<?php echo $owner->phone ?></td>
                              <td><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?> <br> Flexible checkin time</td>
                              <td>  <?php echo date('D, F d,Y', strtotime($booking->check_out)) ?> <br> Flexible checkout time</td>                              
                            </tr>
                           </table>
                      </div>


                      <div class="col-sm-10" style="margin-left:5%;">
                        <h3>Billing 
                        </h3>
                      
                        <table class="table table-bordered">
                          <tr>
                            <td>Check-in</td>
                            <td><?php echo date('D, F d,Y', strtotime($booking->check_in)) ?></td>
                          </tr>
                          <tr>
                            <td>Check-out</td>
                            <td><?php echo date('D, F d,Y', strtotime($booking->check_out)) ?></td>
                          </tr>
                          <tr>
                            <td>Number of Nights</td>
                            <td><?php echo $numberDays ?></td>
                          </tr>

                           <?php 
                            $currency = $this->session->userdata('currency');
                            $total_amount = $booking->total_amount;

                               if(!empty($pro_info->cleaning_fee))
                               {
                                  $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                  $total_amount = $total_amount-$cleaning_fee;
                               }
                               if(!empty($booking->security_deposit))
                               {
                                  $total_amount = $total_amount-$booking->security_deposit;
                               }

                               $after_security_deposit_and_cleaning_fee = $total_amount;

                               $subtotal_after_coupon_if_coupon_exist = $after_security_deposit_and_cleaning_fee-$booking->commision-$booking->tax;

                           ?>



                          <?php if(!empty($booking->coupon_type)): ?>
                            
                              <?php if($booking->coupon_type=="percent"):?>
                                 
                                 <?php 
                                     $subtotal_before_coupon = ($subtotal_after_coupon_if_coupon_exist*100)/(100-$booking->coupon_amount);
                                   ?>

                                <tr>
                                  <td>Per night (approximate...) </td>
                                  <td>
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>

                                 
                                <tr>
                                  <td>Subtotal (before coupon price apply)</td>
                                  <td><?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type)." ".$currency; ?> </td>
                                </tr>

                                <tr>
                                  <td>Coupon type</td>
                                  <td> Percent </td>
                                </tr>
                                
                                <tr>
                                  <td>Coupon Amount</td>
                                  <td><?php echo $booking->coupon_amount ?> % </td>
                                </tr>

                              <?php endif; ?>
                              
                              <?php if($booking->coupon_type=="amount"): ?>

                                <tr>
                                  <td>Per night (approximate...) </td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td>
                                  <?php echo convertCurrency($subtotal_before_coupon/$numberDays,$currency,$booking->booking_currency_type)." ".$currency ?>
                                  </td>
                                </tr>


                                <tr>
                                  <td>Subtotal (before coupon price apply)</td>
                                    <?php $subtotal_before_coupon =  $subtotal_after_coupon_if_coupon_exist+$booking->coupon_amount; ?>
                                  <td>
                                  <?php echo convertCurrency($subtotal_before_coupon,$currency,$booking->booking_currency_type) ?>
                                  </td>
                                </tr>

                                <tr>
                                  <td>Coupon type</td>
                                  <td> Amount </td>
                                </tr>
                                
                                <tr>
                                  <td>Coupon Amount</td>
                                  <td><?php echo $booking->coupon_amount ?></td>
                                </tr>

                              <?php endif; ?>

                          <?php endif ?>

                          <tr>
                            <td>Subtotal</td>
                            <?php $subtotal = convertCurrency($subtotal_after_coupon_if_coupon_exist,$currency,$booking->booking_currency_type) ?>
                            <td><?php echo number_format($subtotal,2)." ".$currency ?></td>
                          </tr>

                          <tr>
                            <td>Commision fee</td>
                            <td><?php 
                                 echo number_format($booking->commision,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <tr>
                            <td>Tax fee</td>
                             <td><?php 
                                 echo number_format($booking->tax,2).' '.$currency ;?>
                            </td>
                          </tr>

                          <?php if(!empty($pro_info->cleaning_fee)): ?>
                             <tr>
                               <td>Cleaning Fee</td>
                               <td>
                               
                                  <?php $cleaning_fee = convertCurrency($pro_info->cleaning_fee,$booking->booking_currency_type,$pro_info->currency_type);
                                 
                                 /*convert into current website currency */
                                   $cleaning_fee = convertCurrency($cleaning_fee,$currency,$booking->booking_currency_type);
            
                                 /*convert into current website currency */
                                  echo number_format($cleaning_fee,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <?php if(!empty($booking->security_deposit)): ?>
                             <tr>
                               <td>Security Deposit</td>
                               <td>
                                   <?php $security_deposit = convertCurrency($booking->security_deposit,$currency,$booking->booking_currency_type) ?>
                                  <?php echo number_format($security_deposit,2)." ".$currency ?>
                               </td>
                             </tr>
                           <?php endif; ?>

                          <tr>
                            <td>Total</td>
                           <?php $total_amount = convertCurrency($booking->total_amount,$currency,$booking->booking_currency_type) ?>
                            <td><?php echo number_format($total_amount,2).' '.$currency  ?></td>
                          </tr>
                        </table>

                      <p style="text-align:center">THANKS FOR TRAVELING WITH BNBCLONE</p>
                      </div>  
                    </div>
                  </div>
                </div>

              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
   <style type="text/css">

 .adminicon [data-icon]:before{
    font-size: 40px;
  }

.section0{
  margin-top: 6%;
  /*width: 100%;*/
  padding-left: 4%;
  /*border-bottom: 1px solid #CCC;*/
}

.span-text{
  margin-left: 2%;
}
  .iti-content{
    border:1px solid #CCC;
    /*margin-left: 1%;*/
  }

  .controls span.span12{
    padding-top: 6px;
  }

  .itinenarybtn{
    margin-left: 1%;
  }

  .itinenarybtn ul{
    text-align: center !important;
    width: 100%;
    font-size: 18px;
    margin-top: 1% !important;
  }
  .itinenarybtn ul li a{
    color:#737373;                  
  }

  .itinenarybtn ul li{
    padding-left: 20%;                  
    display: inline;
  }

  img {
    max-width: none;
}

</style>


