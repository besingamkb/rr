
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Property Types
          </div>
          <div class="pull-right">

            <a class="btn" href="<?php echo base_url(); ?>admin/add_pr_type"> Add Property Type </a>
             <div class="btn-group">
              <a class="btn" href="#">Property Attributes</a>
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>admin/amenities">Amenities</a></li>
                <li><a href="<?php echo base_url(); ?>admin/room_types">Room Types</a></li>
                <li><a href="<?php echo base_url(); ?>admin/bed_types">Bed Type</a></li>
                <li><a href="<?php echo base_url(); ?>admin/property_types">property Type</a></li>
                <li><a href="<?php echo base_url(); ?>admin/room_allotment">Room Allotment </a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  
                  <th style="width:30%">Property Type</th>
                  <th style="width:30%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($property_type)): ?>
                    <?php $i=1; foreach ($property_type as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->property_type; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>
                        <td>
                          <a href="<?php echo base_url()?>admin/delete_pr_type/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" style="display:none"  class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>admin/edit_pr_type/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">edit</a>
                        </td>
             <?php endif; ?>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix">
              <?php echo $pagination ?>
            </div>
          </div>
        </div>
      </div>
    </div>