<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Tool Tip on Add Property
</div>
  <a style="float:right;margin-right:3%" href="<?php echo base_url() ?>admin/settings" class="btn btn-default">Back</a>
</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_row('tool_tip',array('tool_tip_page'=>'tool_on_add_property')); ?>

  <?php if(!empty($row->page_data)): ?>
  <?php $row = json_decode($row->page_data); ?>
  <?php endif; ?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Property Type
    </label>
    <div class="controls controls-row span6">
          <input name="property_type" class="span12" type="text" placeholder="Property Type" value="<?php if(!empty($row->property_type)) echo $row->property_type ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Accomodates
    </label>
    <div class="controls controls-row span6">
          <input name="accomodates" class="span12" type="text" placeholder="Accomodates" value="<?php if(!empty($row->accomodates)) echo $row->accomodates ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Room Type
    </label>
    <div class="controls controls-row span6">
          <input name="room_type" class="span12" type="text" placeholder="Room Type" value="<?php if(!empty($row->room_type)) echo $row->room_type ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Bedrooms
    </label>
    <div class="controls controls-row span6">
          <input name="bedrooms" class="span12" type="text" placeholder="Bedrooms" value="<?php if(!empty($row->bedrooms)) echo $row->bedrooms ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Title
    </label>
    <div class="controls controls-row span6">
          <input name="title" class="span12" type="text" placeholder="Title" value="<?php if(!empty($row->title)) echo $row->title ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Description
    </label>
    <div class="controls controls-row span6">
          <input name="description" class="span12" type="text" placeholder="Description" value="<?php if(!empty($row->description)) echo $row->description ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Price
    </label>
    <div class="controls controls-row span6">
          <input name="price" class="span12" type="text" placeholder="Price" value="<?php if(!empty($row->price)) echo $row->price ; ?>">
    </div>
</div>







<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

