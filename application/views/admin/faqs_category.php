 
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Help Category
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>admin/add_help_category"> Add Help Category </a>
            <a class="btn" href="<?php echo base_url(); ?>admin/help"> Back To Help</a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:12%">Title </th>
                  <th style="width:12%">Description</th>
                  <th style="width:10%">Created</th>
                  <th style="width:10%">Status</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($faqs_category)): ?>
                    <?php  foreach ($faqs_category as $row):?>
                      <tr>
                        <td><?php echo ++$offset;?></td>
                        <td><?php echo word_limiter($row->title,2);?></td>
                        <td><?php echo word_limiter($row->description, 15); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        <td>
                          <?php if($row->status==0){?>
                            <a href="<?php echo base_url() ?>admin/faqs_category_status/<?php echo $row->id;?>" class="btn btn-warning btn-small hidden-phone" data-original-title="">Unpublish</a>
                          <?php }else if($row->status==1){?>
                            <a href="<?php echo base_url() ?>admin/faqs_category_status/<?php echo $row->id;?>" class="btn btn-info btn-small hidden-phone" data-original-title="">Publish</a>
                          <?php }?>
                        </td>

        <?php $colors = get_buttons_color();  ?>
        <?php if(!empty($colors)): ?>

                        <td>
                          <a href="<?php echo base_url()?>admin/delete_faqs_category/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="<?php echo $colors->delete_btn ?>" data-original-title="">Delete</a>
                          <a href="<?php echo base_url()?>admin/edit_help_category/<?php echo $row->id;?>" id="" role="button" class="<?php echo $colors->edit_btn ?>" data-toggle="modal" data-original-title="">Edit</a>
                        </td>
        <?php endif; ?>       

                      </tr>
                    <?php  endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>