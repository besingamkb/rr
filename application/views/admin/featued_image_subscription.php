
<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Featured Image Subscription
</div>
  <a style="float:right;margin-right:3%" href="<?php echo base_url() ?>admin/settings" class="btn btn-default">Back</a>
</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_row('featued_image_subscription',array('name'=>'featued_image_subscription')); ?>




<div class="control-group">
    <label class="control-label" for="your-name">
        Daily (in US Dollar)
    </label>
    <div class="controls controls-row span6">
          <input required name="daily" class="span12" type="text" placeholder="Daily" value="<?php if(!empty($row->daily)) echo $row->daily ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Weekly (in US Dollar)
    </label>
    <div class="controls controls-row span6">
          <input required name="weekly" class="span12" type="text" placeholder="Weekly" value="<?php if(!empty($row->weekly)) echo $row->weekly ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Monthly (in US Dollar)
    </label>
    <div class="controls controls-row span6">
          <input required name="monthly" class="span12" type="text" placeholder="Monthly" value="<?php if(!empty($row->monthly)) echo $row->monthly ; ?>">
    </div>
</div>


<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

