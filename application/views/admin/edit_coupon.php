
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Edit Coupons
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                  <label class="control-label" for="question">
                    Coupon Name
                  </label>
                <div class="controls controls-row span6">
                    <input type="text" name="name" value="<?php echo $coupon->name; ?>" placeholder="Name">
                  <span class="form_error span12"><?php echo form_error('name'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                Amount Type
                </label>
                <div class="controls controls-row span6">
                    <select class="chzn-select" name="type">
                      <option value="" >Please Select</option>
                      <option <?php if($coupon->reduction_type == 1){echo "selected='selected'";} ?> value="1">Percent</option>
                      <option <?php if($coupon->reduction_type == 2){echo "selected='selected'";} ?> value="2">Amount</option>
                    </select>
                  <span class="form_error span12"><?php echo form_error('type');  ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                 Coupon Code
                </label>
                <div class="controls controls-row span6">
                <input type="text"   name="coupon_code" value="<?php echo $coupon->code; ?>" placeholder="Coupon Code" readonly> 
                  <span class="form_error span12"><?php echo form_error('coupon_code');  ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                  Start Date
                </label>
                <div class="controls controls-row span6">
                      <input  class="date_picker" type="text" value="<?php echo $coupon->start_date; ?>" placeholder="Start Date" name="start_date" id="from_dat">
                  <span class="form_error span12"><?php echo form_error('start_date');  ?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                  End Date
                </label>
                <div class="controls controls-row span6">
                      <input  class="date_picker" type="text" value="<?php echo $coupon->end_date; ?>" placeholder="End Date" name="end_date" id="to_dat">
                    <span class="form_error span12"><?php echo form_error('end_date');  ?></span>
                </div>
              </div>
               <div class="control-group">
                <label class="control-label">
                 Amount 
                </label>
                <div class="controls controls-row span6">
                      <input  class="date_picker" type="text" value="<?php echo $coupon->reduction_amount; ?>" placeholder="Amount" name="amount">
                    <span class="form_error span12"><?php echo form_error('amount');  ?></span>
                </div>
              </div>
              

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>



<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
$(function() {
$( "#from_dat" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to_dat" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to_dat" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from_dat" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>

