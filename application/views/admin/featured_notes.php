
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Featured Notes
          </div>
          <div class="pull-right">
            <a href="<?php echo base_url();?>admin/add_featured_note/<?php if(!empty($id)) echo $id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a>
            <a href="<?php echo base_url()?>admin/featured_images" class="btn btn-warning btn-small">Back To Featured Images</a>
          </div>          
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:30%">Title</th>
                  <th style="width:30%">Note</th>
                  <th style="width:15%">Created</th>
                  <th style="width:15%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($notes)): ?>
                    <?php $i=1; foreach ($notes as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo substr($row->title,0,25); ?></td>
                        <td><?php echo substr($row->description,0,25); ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)); ?></td>
                        <td>
                          <a href="<?php echo base_url()?>admin/view_featured_note/<?php echo $row->id;?>" id="" role="button" class="btn btn-small btn-success  hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">view</a>
                          <a href="<?php echo base_url()?>admin/delete_featured_note/<?php echo $row->id;?>/<?php echo $row->featured_id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-primary btn-small hidden-phone" data-original-title="">Delete</a>
                        </td>
                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>