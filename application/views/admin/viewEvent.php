<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin" >
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                    Event full view
                </div>
            </div>
        <table align="center" style="border:5px #F2F2F2 solid; border-top:none; width:50%;  margin-top:20px;" class="table table-condensed table-striped">    
            <thead>
                <tr>
                    <th style="width:120px;">
                        <span style="font-size:20px; color:#D33F3E; "  data-icon="" >
                            Event
                        </span>
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='font-weight:bold;'>
                        Title
                    </td>
                    <td colspan="2" style='font-size:16px;'>
                        <?php echo $event->title; ?>
                    </td>
                </tr>
    
                <tr>
                    <td style='font-weight:bold;'>
                        Description
                    </td>
                    <td style='font-size:16px; text-align:justify;'>    
                        <?php echo $event->description; ?>
                    </td>
                </tr>
      
                <tr>
                    <td style='font-weight:bold;'>
                        Location
                    </td>
                    <td style='font-size:16px;'>
                        <?php echo $event->location;  ?>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight:bold;'>
                        Start Date
                    </td>
                    <td style='font-size:16px;'>
                        <?php echo date('d F Y',strtotime($event->startDate));  ?>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight:bold;'>
                        End Date
                    </td>
                    <td style='font-size:16px;'>
                        <?php echo date('d F Y',strtotime($event->endDate));  ?>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight:bold;'>
                        Start Time
                    </td>
                    <td style='font-size:16px;'>
                        <?php echo $event->startTime; ?>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight:bold;'>
                        End Time
                    </td>
                    <td style='font-size:16px;'>
                        <?php echo $event->endTime; ?>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight:bold;'>
                        Is important ?
                    </td>
                    <td style='font-size:16px;'>
                        <?php if($event->isImportant == 1): ?>
                            Yes
                        <?php else: ?>
                            No
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="btn btn-success"   href="<?php echo base_url(); ?>admin/events" >Back</a> 
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
        <div class='clear:both;'></div>
</div>
