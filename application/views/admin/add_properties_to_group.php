<style>
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
</style>


   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Properties To Group
            </div>
          </div>
          <div class="widget-body">

            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <?php $users = get_members_of_group($group_id,'owner');?>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Property Owner Name
                </label>
                <div class="controls controls-row span6">
                  <select onchange="return get_property_of_members();" id="user_email" name="user_name" class="span10 input-left-top-margins">
                      <option value="">Select Owner</option>
                      <?php foreach ($users as $row): ?>
                      <option value="<?php echo $row->id; ?>"><?php echo $row->first_name.' '.$row->last_name; ?></option>
                      <?php endforeach; ?>
                  </select>
                  <span class="form_error span12"><?php echo form_error('user_name'); ?></span>
                </div>
              </div>


              <div class="control-group">
                <label class="control-label">
                  Property
                </label>
                <div class="controls controls-row span6" id="special">

                </div>
                <div class="controls controls-row span6" style="floatLright">
                  <span class="form_error span12"><?php echo form_error('property'); ?></span>
                </div>
             </div>

              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>


  <script>
   function get_property_of_members(){

    var owner_id = $('#user_email').val();
        $.ajax({
          url: '<?php echo base_url() ?>/admin/owner_property_info',
          type: 'post',
          data: {user_id:owner_id},
          success:function(res){
            if(res!=""){
              $('#special').html(res);
            }
            else{
              $('#special').html("No property Found For This Owner.");
            }
          }
        })
  }

  </script>