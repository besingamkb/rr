 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Member
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
             

              <div class="control-group">
                <label class="control-label">
                  Member
                </label>
                <div class="controls controls-row span6">
                  <input name="member_name" class="span12"  type="text" placeholder="Member Name" value="<?php echo $member->first_name; ?>" readonly>
                  <span class="form_error span12"><?php echo form_error('member_name'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Email
                </label>
                <div class="controls controls-row span6">
                  <input name="email" class="span12" type="text" placeholder="Email" value="<?php echo $member->user_email; ?>" readonly>
                  <span class="form_error span12"><?php echo form_error('email'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Subject
                </label>
                <div class="controls controls-row span6">
                  <input name="subject" class="span12" type="text" placeholder="Subject" value="<?php echo set_value('subject'); ?>">
                  <span class="form_error span12"><?php echo form_error('subject'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Message
                </label>
                <div class="controls controls-row span6">
                  <textarea name="message" rows="5" style="width:548px;" ><?php echo set_value('message'); ?></textarea>
                  <span class="form_form_error span12"><?php echo form_error('message'); ?></span>
                </div>
              </div>

              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
               
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>