 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Event
            </div>
          </div>


          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>


              <div class="control-group">
                <label class="control-label" for="your-name">
                Event Title
                </label>
                <div class="controls controls-row span8">
                  <input name="title" class="span12" type="text" placeholder="Event title" value="<?php echo $event->title; ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>



              <div class="control-group">
                <label class="control-label">
                 Description
                </label>
                <div class="wysiwyg-container controls controls-row span8">
                  <textarea name="description" class="input-block-level no-margin" placeholder="Event description" style="height: 140px"><?php echo $event->description; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                 Location
               </label>
                <div class="wysiwyg-container controls controls-row span8">
                  <textarea name="location" class="input-block-level no-margin"  style="height: 80px"><?php echo $event->location; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('location'); ?></span>
                </div>
              </div>

  <div class="control-group">
    <label class="control-label" >
    Start date
    </label>
    <div class="controls controls-row span8">
      <input name="sDate" id='from' class="span4" type="text" value="<?php echo date('m/d/Y' ,strtotime($event->startDate)); ?>"   readonly='readonly'>
      <span class="form_error span12"><?php echo form_error('sDate'); ?></span>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">
    End date
    </label>
    <div class="controls controls-row span8">
      <input name="eDate"  id='to' class="span4" type="text" placeholder="mm/dd/yyyy" value="<?php echo date('m/d/Y' ,strtotime($event->endDate)); ?>"  readonly='readonly'>
      <span class="form_error span12"><?php echo form_error('eDate'); ?></span>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">
    Start time
    </label>
    <div class="controls controls-row span8">
          <div class="input-append bootstrap-timepicker">
            <input value='<?php echo $event->startTime; ?>' name='sTime' readonly='readonly'  type="text" class="input-small">
            <span class="add-on"><i class="icon-time"></i></span>
          </div>
      <span class="form_error span12"><?php echo form_error('sTime'); ?></span>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">
    End time
    </label>
    <div class="controls controls-row span8">
          <div class="input-append bootstrap-timepicker">
            <input value='<?php echo $event->endTime; ?>' name='eTime'  readonly='readonly'   type="text" class="input-small">
            <span class="add-on"><i class="icon-time"></i></span>
          </div>
      <span class="form_error span12"><?php echo form_error('eTime'); ?></span>
    </div>
  </div>


        
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Is Important ?
                </label>
                <div class="controls controls-row span8">
                  <select name="isImportant" class="span4">
                        <option value="0" <?php if($event->isImportant == 0) echo 'selected'; ?> >No</option>
                        <option value="1" <?php if($event->isImportant == 1) echo 'selected'; ?> >Yes</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('isImportant'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>

 <script>
$(function() {
$( "#from" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#to" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#to" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#from" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>




 
<script type="text/javascript">
$('input[name=sTime]').timepicker({
    defaultTime:'<?php echo $event->startTime; ?>'
});
$('input[name=eTime]').timepicker({
    defaultTime:'<?php echo $event->endTime; ?>'
});
</script