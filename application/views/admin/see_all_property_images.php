
<link rel="stylesheet" href="<?php echo base_url() ?>assets/drag_drop_user_pic/css/drag_drop.css" />


   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" ><i class="icon-film" ></i></span>&nbsp; See All Property Images
            </div>
            <div class="pull-right">
              <a  class="btn" href="<?php echo base_url(); ?>admin/property_images/<?php echo $property_id ?>">Back To Previus Page</a>  
            </div>
          </div>

                <div class="widget-body">
                  <div class="form-horizontal no-margin well" style="height:auto !imporatnt">

                 <div style="width:100%">
                 <!-- Upper  Box Starts  -->
                        <div class="">
                            <h4>click any one of them to Select as a property profile image.</h1>
                        </div> 
                 <!-- Upper  Box Ends  -->
                 </div>
                 <br>

                 <div style="width:100%">
                 <!-- Upper  Box Starts  -->
                        <div class="see_all" style="min-height:400px;"  >
                             <?php if(!empty($gallery)): ?>
                      <ul id="sortable">
                                <?php foreach($gallery as $row): ?>
                         <li class="ui-state-default" id="li_<?php echo $row->id ?>">
                           <input type="hidden" id="image_unique" value="<?php echo $row->image_file ?>">
                           <input type="hidden" id="unique_id" value="<?php echo $row->id ?>">
                           <input type="hidden" id="property_id" value="<?php echo $property_id ?>">
                           <img class="img_img" src="<?php echo base_url() ?>/assets/uploads/property/<?php echo $row->image_file ?>">
                           <a href="javascript:void(0)" class="btn btn-success my_btn" style="color:white">Delete</a>
                           <br>
                         </li>
                                <?php endforeach; ?>
                      </ul>
                             <?php  endif; ?>
                        </div> 
                 <!-- Upper  Box Ends  -->
                 </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">

$(function() {
$( "#sortable" ).sortable();
$( "#sortable" ).disableSelection();
});
 

  $(document).ready(function(){
  $('.img_img').click(function(){

        $('.img_img').removeClass('made_pink');
        $(this).addClass('made_pink');
        var image = $(this).siblings('#image_unique').val();
        var property_id = $(this).siblings('#property_id').val();
        $.ajax({
              type:'post',
              url:'<?php echo base_url() ?>admin/ajax_change_property_profile_image',
              data:{image:image,property_id:property_id},
              success:function(res)
              { 
                if(res=="valid")
                {
                  alert("This image has been successfully set as a property profile image.");
                }
              }
        });
  });

  $('.my_btn').click(function(){
   var unique_id = $(this).siblings('#unique_id').val();
   var property_id = $(this).siblings('#property_id').val();
      $.ajax({
                 type:'post',
                 url:'<?php echo  base_url() ?>admin/ajax_delete_property_image',
                 data:{unique_id:unique_id,property_id:property_id},
                 success:function(res){
                    if(res=='deleted'){
                    $('#li_'+unique_id).fadeOut(1000);
                    // $('#li_'+unique_id).fadeIn();
                    // $('#li_'+unique_id).remove(2000);
                     // alert('Image Has been deleted successfully');
                  }
                 }
      });
  });


});


</script>


<style>


.img_img{
    border:10px solid white !important;
    border-radius: 5px !important;
    width:100px;
    height: 100px;

    margin-bottom: 8px !important;
}
.img_img:hover{
    cursor: pointer;
}


.ui-state-default{
  border:0px solid #D5D5D5 !important;
    margin: 50px !important;
    min-width:160px !important;
}

.made_pink{
  border:10px solid #DE4980 !important;
}
#sortable { list-style-type: none; margin: 0; padding: 0; width: 850px; }

#sortable li { 
             margin: 5px 5px 5px 0 !important; 
             padding: 1px !important;
             padding-bottom: 5px !important; 
             float: left !important; 
             width: 100px !important;
             min-height: 150px !important;
             font-size: 4em !important;
             text-align: center !important;
             background-color: #EEEEEE 
           }

.see_all{
   margin-top:5% !important;
   margin-left:7% !important;
   border-top:0px !important;
   background-color:#EEEEEE;
   width: 85% !important;
   height: 300px !important;
   overflow: hidden;
   overflow-y: auto;
}
.message{
   padding-top: 40px !important;
}

          
</style>



