
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Featured Images
          </div>
          <div class="pull-right">
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>


           <?php $attributes = array('name'=>'myForm'); echo form_open(current_url(),$attributes); ?>
          <select onchange="return form_submit();" id="sort" name="sort">
            <option value="">Select To Sort </option>
            <option value="new_requests">New Requests</option>
            <option value="publish">Publish Featured</option>
            <option value="unpublish">Unpublish Featured</option>
          </select>
          <?php echo form_close(); ?>




          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:12%">Image</th>
                  <th style="width:12%">Created</th>
                  <th style="width:35%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($featured_images)): ?>
                    <?php  foreach ($featured_images as $row):?>
                      <tr>
                        <td><?php ++$offset; echo $offset;?></td>
                        <td><img class="featured_image" src="<?php echo base_url()?>assets/uploads/property/<?php echo $row->image_file; ?>"></td>
                        <td><?php echo date('d F Y',strtotime($row->created)); ?></td>

                        <td>
                          <?php if($row->featured_request==1): ?>
                             <a href="<?php echo base_url()?>admin/accept_featured_image_request/<?php echo $row->id;?>" class="btn btn-danger" data-original-title="">Accept Request</a>
                             <a href="<?php echo base_url()?>admin/reject_featured_image_request/<?php echo $row->id;?>" class="btn btn-danger" data-original-title="">Reject Request</a>
                          <?php endif; ?>

                          <?php if($row->is_featured_image==0){ ?>
                             <a class="btn btn-info" href="javascript:void(0)" onclick="model_for_subscription(<?php echo $row->id ?>)">Publish</a>
                          <?php }else if($row->is_featured_image==1){ ?>
                             <a href="<?php echo base_url()?>admin/unpublish_featured_image/<?php echo $row->id;?>" class="btn btn-warning" data-original-title=""> Unpublish </a>
                          <?php } ?>
                             <a href="<?php echo base_url()?>admin/featured_notes/<?php echo $row->id;?>" class="btn btn-danger" data-original-title="">Notes</a>
                        </td>
                      </tr>
                    <?php  endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"><?php echo $pagination; ?></div>
          </div>
        </div>
      </div>
    </div>

<style type="text/css">

  .featured_image
  {
  border: 4px solid #FFFFFF;
  border-radius: 4px;
  box-shadow: 1px 1px 5px #CCCCCC;
  height: 150px;
  width: 165px;
  }

  .featured_image:hover
  {
  cursor: pointer;
  }
  tbody tr td:first-child
  {
  padding-top: 8%;
  }
  th,td
  {
  padding: 15px;
  }

</style>


<script type="text/javascript">
   function model_for_subscription(image_id)
   {
      $('input[name=image_id]').val(image_id);
      $("#paypal_model").modal();    
   }

</script>

<div class="modal fade" id="paypal_model" style="display:none">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Subscriptions</h4>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title" id="note-subject">Featured Image Subscriptions Pack</h4>
        </div>

        <div class="panel-body" id="note-msg">
        <?php echo form_open(base_url().'admin/publish_featured_image'); ?>
         <table align="center">
             <tr>
               <th>Subscriptions</th>
               <th>Type</th>
             </tr>
             <tr>
               <td><input type="radio"  name="subscriptions_type" value="daily" checked=""></td>
               <td>Daily</td>
             </tr>
             <tr>
               <td><input type="radio"  name="subscriptions_type" value="weekly" ></td>
               <td>Weekly</td>
             </tr>
             <tr>
               <td><input type="radio"  name="subscriptions_type" value="monthly" ></td>
               <td>Monthly</td>
             </tr>
         </table>
           <input type="hidden" name="image_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-info" value="Pay" >
        </div>
        <?php echo form_close(); ?>
      </div>
      </div>
    </div>
  </div>

    <!-- Model  Ends-->

    <script type="text/javascript">
       function form_submit(){
        var sort = document.getElementById('sort'); 
        if(sort != ''){
          $("form[name='myForm']").submit();
        }
        else{
          return false;
        }        
      }

    </script>



<style type="text/css">
  
.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
  border:1px solid #cccccc; 
  color: #333333;
  background-color: #f5f5f5;
  border-color: #dddddd;

}
.panel-heading h4 
{
  font-weight: lighter !important
}



.panel-footer {
  padding: 10px 15px;
  background-color: #f5f5f5;
  border-top: 1px solid #dddddd;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
  
</style>
