
          <?php if($this->session->flashdata('success_msg')): ?>
            <div class="alert alert-success">
              <?php echo $this->session->flashdata('success_msg');?>
            </div>
          <?php endif ?>
        
          <div class="row-fluid">
            <div class="span12">
              <div class="widget no-margin">
                <div class="widget-header">

                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Bookings Email Record
                   

                  </div>
                  <!-- <div class="pull-right">
                    <a href="<?php //echo base_url();?>admin/add_booking" class="btn">Add Booking</a>                      
                    <a href="<?php //echo base_url();?>admin/booking_email_records" class="btn">Booking Email History</a>                      
                  </div> -->
                </div>
                <div class="widget-body">
               
                <!--   <div class="span2">
                      <?php $attributes = array('name'=>'myForm'); echo form_open(base_url().'admin/bookings',$attributes); ?>
                        <select onchange="return form_submit();"  class="span10" id="sort" name="sort"> 
                          <option value="">Select To Sort</option>
                          <option value="old">Old Booking</option>
                          <option value="new">New Booking</option>
                          <option value="max_am">Max Amount</option>
                          <option value="min_am">Min Amount</option>
                        </select>
                      <?php echo form_close(); ?>    
                  </div>
                  <div class="pull-right">
                    <?php  $attributes = array('name'=>'search_form'); echo form_open(current_url(),$attributes);?>
                      <div class="input-append">
                        <input id="appendedInput" name='email' class="span5" placeholder="By Email" type="text">
                        <input id="appendedInput" name='name' class="span5" placeholder="By Name" type="text">
                        <button class="btn" id="search" onclick="return form_submit_name();" type="submit">Search</button>
                      </div>
                    <?php echo form_close(); ?>
                  </div> -->
                  <div id="dt_example" class="example_alt_pagination">
                    <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
                      <thead>
                        <tr>
                          <th style="width:6%;">Booking id</th>
                          <th style="width:10%;">Address</th>
                          <th style="width:10%;">Email</th>
                          <th style="width:15%;">Suject</th>
                          <th style="width:25%">Message</th>
                          <th style="width:10%;" class="hidden-phone">Contact</th>
                          <th style="width:5%;">Created</th>
                          <th style="width:10%;" class="hidden-phone">Actions</th>
                        </tr>
                      </thead>
                      <tbody>

                       <?php if(!empty($history)):?> 
                       <?php  foreach ($history as $row):?>                      
                        <tr class="gradeA">
                          <td><?php echo $row->booking_id;?></td>
                          <td><?php echo $row->address;?></td>
                          <td><?php echo $row->email;?></td>
                          <!-- <td>
                            <?php if($row->status==0){?>
                              <a href="<?php echo base_url() ?>admin/booking_status/<?php echo $row->id;?>" class="btn btn-warning btn-small hidden-phone" data-original-title="">Pending</a>
                            <?php }else if($row->status==1){?>
                              <a href="#" class="btn btn-inverse btn-small hidden-phone" data-original-title="">Complete</a>
                            <?php }else{?>
                              <a href="<?php echo base_url() ?>admin/cancel_booking/<?php echo $row->id;?>" class="btn btn-inverse btn-small hidden-phone btn-info"  data-placement="top" data-original-title="Do you want to active it?" data-original-title="">Cancelled</a>

                            <?php }?>
                          </td> -->
                          <td><?php echo $row->subject;?></td>
                          <td><?php echo $row->message;?></td>


                          <td class="hidden-phone"><?php echo $row->contact;?></td>
                          <td><?php echo $row->created;?></td>
                         
                          <td class="hidden-phone">
                            <!-- <a href="<?php //echo base_url();?>admin/booking_email/<?php //echo $row->id; ?>"  class="btn btn-success btn-small hidden-phone" data-original-title="">email</a> -->
                            <a href="<?php echo base_url()?>admin/view_booking_record/<?php echo $row->id; ?>" role="button" class="btn btn-success btn-small hidden-tablet hidden-phone" data-toggle="modal" data-original-title="">
                              view
                            </a>
                            <a href="<?php echo base_url()?>admin/delete_booking_record/<?php echo $row->id; ?>" onclick="return confirm('Are you want to delete?');" class="btn btn-small btn-primary hidden-tablet hidden-phone">
                              delete
                            </a>
                           

                            <!-- edit modal -->
                              <div id="accSettings16_<?php echo $row->id; ?>" class="modal hide fade" tabindex="-16" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                  ×
                                </button>
                                <h4 id="myModalLabel16">
                                  Edit client details
                                </h4>
                              </div>
                            
                              <div class="modal-body">
                               <?php echo form_open('admin/bookings_edit/'.$row->id) ?>
                                <div class="row-fluid">  
                                  <div class="span6">
                                    <input type="text" value="<?php if(!empty($row->booking_name)){echo $row->booking_name;}?>" class="span12" id="booking_name" name="booking_name" placeholder="Booking name">
                                    <span style="display:none;"  id='book_name'></span>
                                  </div>
                                 
                                  <div class="span6">
                                    <input type="text"  value="<?php if(!empty($row->email)){echo $row->email;}?>" class="span12" name="email"  id="email" placeholder="email">
                                    <span style="display:none;"  id='book_email'></span>
                                  </div>
                                </div>
                                <div class="row-fluid">  
                                  <div class="span8">
                                    <input type="text" value="<?php if(!empty($row->address)){echo $row->address;}?>" class="span12"  name="address" id="address" placeholder="Address">
                                    <span style="display:none;"  id='book_address'></span>
                                  </div>
                                  <div class="span4">
                                    <input type="text" value="<?php if(!empty($row->contact)){echo $row->contact;}?>" class="span12" name="contact"  id="contact" placeholder="Contact">
                                    <span  style="display:none;"  id='book_contact'></span>
                                  </div>
                                  
                                </div>
                                <div class="pull-right">
                                  <button class="btn btn-primary" onclick="return booking_changes()">
                                   Save changes
                                  </button>
                                </div>
                               <?php echo form_close();?>
                              </div>
                            </div>
                            <!-- edit modal close -->
                            
                            <!-- email modal -->
                            <div id="email_modal_<?php echo $row->id; ?>" class="modal hide fade" tabindex="-16" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                  ×
                                </button>
                                <h4 id="myModalLabel1">
                                  Email To User
                                </h4>
                              </div>
                              
                              <div id="modal_body" class="modal-body">
                               01
                                <div class="row-fluid">  
                                  <div class="span10">
                                    <input type="text"  id="user_email" value="<?php if(!empty($row->email)){echo $row->email;}?>"  name="user_email"  class="span12" placeholder="Email" readonly>
                                  </div>
                                </div>
                                <div class="row-fluid">  
                                  <div class="span10">
                                    <input type="text" id="subject" name="subject" class="span12" placeholder="Subject">
                                  </div>
                                </div>
                                <div class="row-fluid">  
                                  <div class="span10">
                                    <textarea name="mail_body" id="description"  rows="5" style="width:97%;" placeholder="Mail Description"> Write Description Here.. </textarea>
                                  </div>
                                 </div>
                                </div>
                               <div class="modal-footer">
                                  <button id="send_mail" class="btn btn-primary">
                                    Send
                                  </button>
                             </div>
                            </div>      
                            <!-- email modal close-->
                          </td>
                        </tr>
                     <?php endforeach; ?>
                      <?php else: ?>
                      <tr><td colspan="8"> No Record Found</td></tr>
                     <?php  endif; ?>
                    </tbody>
                    </table>
                      <div id="data-table_info" class="dataTables_info">Showing 1 to 10 of 10 entries</div>
                        <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">
                           <?php if($pagination) echo $pagination; ?>
                           <!--  <a id="data-table_first" class="first paginate_button paginate_button_disabled" tabindex="0">First</a>
                              <a id="data-table_previous" class="previous paginate_button paginate_button_disabled" tabindex="0">Previous</a>
                               <span>
                                  <a class="paginate_active" tabindex="0">1</a>
                                  <a class="paginate_button" tabindex="0">2</a>
                              </span>
                              <a id="data-table_next" class="next paginate_button" tabindex="0">Next</a>
                            <a id="data-table_last" class="last paginate_button" tabindex="0">Last</a> -->
                        </div>
                      </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>

            <script type="text/javascript">
               function form_submit(){
                  var sort = document.getElementById('sort'); 
                    if(sort != ''){
                      $("form[name='myForm']").submit();
                    }
                    else{
                      return false;
                    }        
                }
     function form_submit_name(){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/; 
        var name  = $('input:text[name=name]').val();
        var email = $('input:text[name=email]').val();
        if(name == ''){
          alert('Please Enter Name');
          return false;
        }else if(email==''){
          alert('Please Enter Email');
          return false;
        }else if(reg.test(email) == false){
           alert('Invalid Email Address'); 
            return false; 
        }
        else{
         return true;
        }        
      }
            </script>
     
     