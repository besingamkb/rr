 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>Host Profile
            </div>
          </div>
          <div class="widget-body">
           <!--  <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?> -->
              <div class="form-horizontal no-margin well" >
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Host Name
                </label>
                <div class="controls controls-row span6">
                  <span><?php echo $user->first_name.''.$user->last_name;?></span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Host Email
                </label>
                <div class="controls controls-row span6">
                <p><?php echo $user->user_email;?></p>
                </div> 
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Host Contact
                </label>
                <div class="controls controls-row span6">
                <p><?php echo $user->phone;?></p>
                </div> 
              </div>
            
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Host Address
                </label>
                <div class="controls controls-row span6">
                <p><?php echo $user->address.' '.$user->city.' '.$user->state;?></p>
                </div> 
              </div>

           
              
              <div class="form-actions no-margin">
                <a href="<?php echo base_url()?>admin/bookings" class="btn btn-info">
                 Back To Bookings
                </a>
              </div>
            </div>
            <?php //echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>