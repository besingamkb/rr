<style type="text/css">
.input-block-level {
  min-height: 40px !important;
}
.unique_feature {
    color: #777;
    float: left;
    height: 50px;
    padding: 0 5px 10px;
    width: 126px;
}
.form-horizontal .control-label
{
  width:175px ;
}
</style>

 <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title" style="width:19%">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Manage Property
            </div>
          <div class="pull-left" >
            <a class="btn" href="<?php echo base_url(); ?>admin/property_images/<?php echo $properties->id ?>">Upload Images Here </a>  
          </div>
          </div>



          <div class="widget-body">
          <?php alert(); ?>
            <?php echo form_open_multipart(current_url(), array('id' => 'testr' , 'class' => 'form-horizontal no-margin well','onsubmit'=>'return CheckForm();')); ?>
              
            <?php $user_info = get_user_info($properties->user_id) ?>
             <div class="control-group">
                <label class="control-label">
                User Name
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                     <?php $users = get_all_users(); ?>
                    <div class="span10">
                      <input  readonly class="input-block-level" style="width:118%;border-radius:5px" type="text"  value="<?php echo $user_info->first_name." ".$user_info->last_name ?>">
                    </div>
                </div>
              </div>
            </div> 

<br>
              <div class="control-group">
                <label class="control-label" for="your-name">
                 Property Title
                </label>
                <div class="controls controls-row span8">
                  <input name="title" style="width:98%;border-radius:5px" class="input-block-level" type="text" placeholder="Title" required value="<?php echo $properties->title; ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                Property Description
                </label>
                <div class="controls controls-row span8">
                 <textarea rows="5" id="description" style="width:98%;border-radius:5px" required class="span12" name="description" ><?php echo $properties->description; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>  
                </div>
              </div>

               <div class="control-group">
                <label class="control-label">
                  Details
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">

                    <div class="span4">
                      <select  name="bathrooms" required>
                        <option value="">Bathrooms</option>
                           <option <?php if($pr_info->bath==1) echo "selected" ?>>1</option>
                           <option <?php if($pr_info->bath==2) echo "selected" ?>>2</option>
                           <option <?php if($pr_info->bath==3) echo "selected" ?>>3</option>
                           <option <?php if($pr_info->bath==4) echo "selected" ?>>4</option>
                           <option <?php if($pr_info->bath==5) echo "selected" ?>>5</option>
                           <option <?php if($pr_info->bath==6) echo "selected" ?>>6</option>
                           <option <?php if($pr_info->bath==7) echo "selected" ?>>7</option>
                           <option <?php if($pr_info->bath==8) echo "selected" ?>>8</option>
                           <option <?php if($pr_info->bath==9) echo "selected" ?>>9</option>
                           <option <?php if($pr_info->bath==10) echo "selected" ?>>10</option>
                      </select>
                      <span><?php echo form_error('bathrooms'); ?></span>
                    </div>


                  <div class="span4">
                     <select name="bedrooms" required>
                       <option value="">Bedrooms</option>
                           <option <?php if($pr_info->bed==1) echo "selected" ?>>1</option>
                           <option <?php if($pr_info->bed==2) echo "selected" ?>>2</option>
                           <option <?php if($pr_info->bed==3) echo "selected" ?>>3</option>
                           <option <?php if($pr_info->bed==4) echo "selected" ?>>4</option>
                           <option <?php if($pr_info->bed==5) echo "selected" ?>>5</option>
                           <option <?php if($pr_info->bed==6) echo "selected" ?>>6</option>
                           <option <?php if($pr_info->bed==7) echo "selected" ?>>7</option>
                           <option <?php if($pr_info->bed==8) echo "selected" ?>>8</option>
                           <option <?php if($pr_info->bed==9) echo "selected" ?>>9</option>
                           <option <?php if($pr_info->bed==10) echo "selected" ?>>10</option>
                     </select>
                     <span class="form_error span12"><?php echo form_error('bedrooms'); ?></span>
                  </div>

                  <div class="span3">
                    <select name="accomodates" required>
                        <option value="">Accomodates</option>
                        <option <?php if($pr_info->accommodates==1) echo "selected" ?>>1</option>
                        <option <?php if($pr_info->accommodates==2) echo "selected" ?>>2</option>
                        <option <?php if($pr_info->accommodates==3) echo "selected" ?>>3</option>
                        <option <?php if($pr_info->accommodates==4) echo "selected" ?>>4</option>
                        <option <?php if($pr_info->accommodates==5) echo "selected" ?>>5</option>
                        <option <?php if($pr_info->accommodates==6) echo "selected" ?>>6</option>
                        <option <?php if($pr_info->accommodates==7) echo "selected" ?>>7</option>
                        <option <?php if($pr_info->accommodates==8) echo "selected" ?>>8</option>
                        <option <?php if($pr_info->accommodates==9) echo "selected" ?>>9</option>
                        <option <?php if($pr_info->accommodates==10) echo "selected" ?>>10</option>
                        <option <?php if($pr_info->accommodates==11) echo "selected" ?>>11</option>
                        <option <?php if($pr_info->accommodates==12) echo "selected" ?>>12</option>
                        <option <?php if($pr_info->accommodates==13) echo "selected" ?>>13</option>
                        <option <?php if($pr_info->accommodates==14) echo "selected" ?>>14</option>
                        <option <?php if($pr_info->accommodates==15) echo "selected" ?>>15</option>
                        <option <?php if($pr_info->accommodates==16) echo "selected" ?>>16+</option>
                    </select>
                    <span class="form_error span12"><?php echo form_error('accommodates'); ?></span> 
                  </div> 
                </div>
              </div>
            </div>

             <div class="control-group">
                <label class="control-label">
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                     <?php $room_type = get_room_type(); ?>
                    <div class="span4">
                      <select  name="room_type" required>
                        <option value="">Room Type</option>
                        <?php foreach ($room_type as $row): ?>
                        <option value="<?php echo $row->id; ?>" <?php if($pr_info->room_type == $row->id) echo 'selected="selected"'; ?> > <?php echo $row->room_type; ?> </option>
                        <?php endforeach ?>
                      </select>
                      <span><?php echo form_error('room_type'); ?></span>
                    </div>
                     <?php $bed_type = get_bed_types(); ?>
                    <div class="span4">
                      <select  name="bed_type" required>
                        <option value="">Bed Type</option>
                        <?php foreach ($bed_type as $row): ?>
                          <option value="<?php echo $row->id; ?>" <?php if($pr_info->bed_type == $row->id) echo 'selected="selected"'; ?> > <?php echo $row->bed_type; ?> </option>
                        <?php endforeach ?>
                      </select>
                      <span><?php echo form_error('bed_type'); ?></span>
                    </div>

                    <?php $property_type = get_property_types(); ?> 
                    <div class="span4">
                      <select name="property_type" required>
                        <option value="">Property Type</option>
                      <?php foreach ($property_type as $row): ?>
                      <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->property_type_id)){ if($pr_info->property_type_id == $row->id)echo"selected"; }  ?>> <?php echo $row->property_type; ?> </option>
                      <?php endforeach ?>
                      </select>                      
                      <span class="form_error span12"><?php echo form_error('property_type'); ?></span>
                    </div>

                </div>
              </div>
            </div> 


             <div class="control-group">
                <label class="control-label">
                Collection Category
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span10">
                      <?php $category_type = get_collection_category(); ?> 
                      <select  style="float:left" name="price_type">
                        <?php foreach ($category_type as $row): ?>
                        <option value="<?php echo $row->id; ?>" <?php if(!empty($pr_info->pr_collect_cat_id)){ if($pr_info->pr_collect_cat_id == $row->id)echo"selected"; }  ?>> <?php echo $row->category_name; ?> </option>
                        <?php endforeach ?>
                      </select>
                    </div>
                </div>
              </div>
            </div> 
<br>


             <div class="control-group">
                <label class="control-label">
                Price
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span10">
                        <?php $currency_array = currency_array(); ?> 
                      <select  style="float:left" name="price_type">
                          <?php foreach ($currency_array as $row): ?>
                          <option value="<?php echo $row; ?>"  <?php if(!empty($properties->currency_type)){ if($properties->currency_type == $row) echo"selected"; }  ?> > <?php echo $row; ?> </option>
                          <?php endforeach; ?>
                      </select>
                          <input type="text" class="span2"  value="<?php if(!empty($properties->application_fee)) echo $properties->application_fee ?>" maxlength="20" name="price_amount" size="35" style="float:left;margin-left:10px;" required="required" placeholder="Amount">                                            
                      <span><?php echo form_error('price_amount'); ?></span>
                    </div>
                </div>
              </div>
            </div> 
<br>

             <div class="control-group">
                <label class="control-label">
                Security Deposit
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span10">
                      <input type="text" value="<?php if(!empty($properties->security_deposit)) echo $properties->security_deposit ?>" class="span2" style="width:98%;border-radius:5px" maxlength="20" name="security_deposit" size="35" required="required" placeholder="Security Deposit">                                            
                    </div>
                </div>
              </div>
            </div> 

<br>

             <div class="control-group">
                <label class="control-label">
                Cleaning Fee
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span10">
                      <input type="text" value="<?php if(!empty($properties->cleaning_fee)) echo $properties->cleaning_fee ?>" class="span2" style="width:98%;border-radius:5px" maxlength="20" name="cleaning_fee" size="35" required="required" placeholder="Cleaning Fee">                                            
                    </div>
                </div>
              </div>
            </div> 

<br>
            
             <div class="control-group">
                <label class="control-label">
                Size
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span12">
                      <input type="text" class="span2" value="<?php if(!empty($pr_info->square_feet)) echo $pr_info->square_feet ?>" maxlength="20" name="size" size="35" style="float:left;" required="required" placeholder="Size">                                            
                      <select  name="size_mode" style="float:left;margin-left:10px;" >
                          <option <?php if($pr_info->sizeMode == 'Sq foot') echo "selected" ?>>Sq foot</option>
                          <option <?php if($pr_info->sizeMode == 'Sqm.') echo "selected" ?>>Sqm.</option>
                      </select>
                      <span><?php echo form_error('room_type'); ?></span>
                    </div>
                    <div class="span4">
                    </div>
                </div>
              </div>
            </div> 
         
            <br>


            <div class="control-group">
                <label class="control-label">
                  Amenities
                </label>

          <div class="controls controls-row span8">
            <div style="float:left;margin-left:20px">
              <?php $amenity = property_amenities(); $i=0; foreach ($amenity as $row){ ?>
            <div>
             <span class="glyphicon glyphicon-question-sign" title="<?php echo $row->name ?>" ></span>
              <label style="font-weight:lighter">
                 <input type="checkbox" name="amenities[]"  value="<?php echo  $row->id; ?>" 
                 <?php if(!empty($pr_amenities)){
                  foreach ($pr_amenities as $chok1){ 
                 if($chok1->pr_amenity_id == $row->id) echo "checked"; 
                   } } ?>
                > <?php echo  $row->name; ?>
             </label>
            </div>
            <?php if($i%5==4): ?>
            </div>&nbsp;&nbsp;&nbsp;
            <div style="float:left;margin-left:20px">
            <?php endif; ?>
            <?php $i++; } ?>
                </div>
            </div> 
            </div> 
            <br>



              <div class="control-group">
                <label class="control-label">
                House Rules
                </label>
                <div class="controls controls-row span8">
                 <textarea rows="5" style="width:98%;border-radius:5px" required class="span12" name="house_rules" ><?php if(!empty($pr_info->house_rules)) echo $pr_info->house_rules; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('house_rules'); ?></span>  
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                House Manual
                </label>
                <div class="controls controls-row span8">
                 <textarea rows="5" style="width:98%;border-radius:5px" required class="span12" name="house_manual" ><?php if(!empty($pr_info->house_manual)) echo $pr_info->house_manual; ?></textarea>
                  <span class="form_error span12"><?php echo form_error('house_manual'); ?></span>  
                </div>
              </div>



         <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->
            <style type="text/css">
/*             a{
               text-decoration: none !important;
              }
              .GPS_type{
               display: none;
               }
              .postal_type{
               display: none;
              }
*/            </style>
             <script>
            // $(document).ready(function(){

            //         $('.postal_type').show();
            //          $('.GPS_type').hide();
            //          $('.pin_point_type').hide();
            //          $('#post_type').css('color','#DE4980');
            //          $('#gps_co').css('color','#5885AC');
            //          $('#pin_co').css('color','#5885AC');

            //    $('#post_type').click(function(){
            //          $('.postal_type').show();
            //          $('.GPS_type').hide();
            //          $('.pin_point_type').hide();
            //          $('#post_type').css('color','#DE4980');
            //          $('#gps_co').css('color','#5885AC');
            //          $('#pin_co').css('color','#5885AC');
            //    });
            //    $('#gps_co').click(function(){
            //          $('.GPS_type').show();
            //          $('.postal_type').hide();
            //          $('.pin_point_type').hide();
            //          $('#gps_co').css('color','#DE4980');
            //          $('#post_type').css('color','#5885AC');
            //          $('#pin_co').css('color','#5885AC');
            //    });
            //    $('#pin_co').click(function(){
            //          $('.pin_point_type').show();
            //          $('.GPS_type').hide();
            //          $('.postal_type').hide();
            //          $('#pin_co').css('color','#DE4980');
            //          $('#post_type').css('color','#5885AC');
            //          $('#gps_co').css('color','#5885AC');
            //    });
            // });
            </script>


<!--              <div class="control-group">
                <label class="control-label">
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span12">
                          <div class="active" style="width:33%; float:left;">
                              <a href="javascript:void(0)" id="post_type" style="background-color:#F6F6F6 !important">Postal Type</a>
                          </div>
                          <div style="width:33%; float:left;">
                              <a href="javascript:void(0)" id="gps_co">GPS Co-ordinate</a>
                          </div>
                          <div style="width:33%; float:left;">
                              <a href="javascript:void(0)" id="pin_co" style="color:#DE4980" onclick="get_map_size()">Pin Point On Map</a>
                          </div>
                          <div style="width:33%; float:left;">
                          </div>
                    </div>
                </div>
              </div>
            </div> 
 -->
         <!-- Postal Type and GPS Co ordinate and Pin-point on Map Ends -->

            <br>

            <div class="postal_type">

             <div class="control-group">
                <label class="control-label">
                  &nbsp;
                </label>
                <div class="controls controls-row span8">
                    <input required name="address" style="width:98%;border-radius:5px" id="address" class="input-block-level" type="text" placeholder="Property Address" value="<?php echo $pr_info->unit_street_address ?>">                 
                    <span class="form_error span12"><?php  echo form_error('address');  ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  &nbsp;
                </label>

                <div class="controls controls-row span8">
                  <div class="row-fluid">
                  <div class="span4">
                    <input type="text" class="input-block-level"  value="<?php echo $pr_info->city ?>" required onkeyup="get_neighbour_city()" placeholder="City" name="city">
                    <span><?php echo form_error('city'); ?></span> 
                  </div>



                  <script>
                    // function get_neighbourhoot()
                    // {
                    //     var city = $('#city option:selected').attr('class');
                    //     $.ajax({
                    //             type:'POST',
                    //              url:'<?php echo base_url(); ?>admin/get_neighbourhoot_of_city',
                    //             data:{city:city},
                    //          success:function(res)
                    //          {
                    //             $('#neighbourhoot').html(res);
                    //          }
                    //     });
                    // }
                </script>
                <div class="span4">
                    <select name="neighbourhood[]" <?php set_select('') ?>id="neighbourhoot"  class="span12" multiple="multiuple">
                        <option>Select neighbour</option>
                    </select>
                    <span><?php echo form_error('neighbourhood'); ?></span>
                </div>

                  <?php $country = get_country_array(); ?>
                  <div class="span4">
                    <select required class="span12" id="country" name="country">
                      <option  value="">Select Country</option>
                      <?php foreach ($country as $code => $name): ?>
                      <option value="<?php echo $name; ?>" <?php if(!empty($pr_info->country)){ if($pr_info->country == $name)echo"selected"; }  ?> > <?php echo $name; ?> </option>
                        <?php endforeach ?>
                    </select>
                    <span class="form_error "><?php echo form_error('country'); ?></span>
                  </div>
                </div>
                </div>
              </div>

              <br>
              <div class="control-group">
                <label class="control-label">
                    &nbsp;
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                  <div class="span6">
                     <input required class="input-block-level" style="width:80%" name="state" id="state" type="text" placeholder="State" value="<?php echo $pr_info->state ?>">
                      <span class="form_error"><?php echo form_error('state'); ?></span>
                  </div>
                  <div class="span6">
                    <input style="width:80%;margin-left:20%" class="input-block-level" required type="text" placeholder="zip" name="zipcode" value="<?php echo $pr_info->zip ?>">
                    <span class="form_error span12"><?php echo form_error('zipcode'); ?></span>
                  </div>
                </div>
              </div>
            </div> 

            </div>


<!--             <div class="GPS_type">

             <div class="control-group">
                <label class="control-label">
                </label>
                <div class="controls controls-row span8">
                  <div class="row-fluid">
                    <div class="span12">
                    <input name="lat"  class="input-block-level" type="text" placeholder="latitude" value="<?php echo set_value('address'); ?>">                 
                    <input name="long"  class="input-block-level" type="text" placeholder="longitude" value="<?php echo set_value('address'); ?>">                 
                    </div>
                </div>
              </div>
            </div> 

            </div>

 -->
            <input  type="hidden" placeholder="latitude"  id="latitude"  name="latitude">
            <input  type="hidden" placeholder="longitude" id="longitude" name="longitude">

            <!--   <br>
            <div class="control-group">
                <label class="control-label">
                Property Description
                </label>
                <div class="controls controls-row span8">
                    <div class="row-fluid">
                      <div class="span3">
                       <input class="span12"  type="text" placeholder="Rent" name="city">
                      </div>
                      <div class="span3">
                        <select class="span12" name="Room Allotment">
                          <option>Room Allotment</option>
                          <option>3bhk</option>
                          <option>2bhk</option>
                          <option>2floor</option>
                        </select>
                      </div>
                      <div class="span3">
                        <input class="span12"  type="text" placeholder="security Deposit" name="deposit">
                      </div>
                      <div class="span3">
                        <input class="span12"  type="text" placeholder="Application Fee" name="appliction_fee">
                      </div> 
                    </div>
                </div>
           </div>


            <br>

             <div class="control-group">
                <label class="control-label">
                  Amenity
                </label>
                <div class="controls controls-row span8">
                 <?php $amenity = property_amenities(); foreach ($amenity as $row){ ?>
                  <div class="unique_feature"> <input type="checkbox" class="ameny[]"  value="<?php echo  $row->name; ?>"> <?php echo  $row->name; ?></div>
                 <?php } ?>
                </div>
            </div> 
 
            <div class="control-group">
                <label class="control-label">
                  Contact information for this listing
                </label>
                <div class="controls controls-row span8">

                  <div class="row-fluid">
                  <div class="span8">
                   <input  class="input-block-level span12" type="text" placeholder="Contact Name" name="contact_name">
                  </div>

                  <div class="span4">
                    <input class="input-block-level span12" type="text" placeholder="Phone" name="phone">
                  </div>
                
                </div>
              </div>
            </div>       -->
            
            <div class="form-actions no-margin">
              <button type="submit" id="button"  class="btn btn-info">
             Save
              </button>
            </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">


      function CheckForm()
      {
        if(Check_ameneties()===false || Check_description()===false )
        {
          return false;
        }
      }

      function Check_ameneties()
      {
        var checked=false;
        var elements = document.getElementsByName("amenities[]");
        for(var i=0; i < elements.length; i++)
        {
          if(elements[i].checked) {
            checked = true;
          }
        }
        if (!checked)
        {
          alert('Please Select any ameneties');
        }
        return checked;
      }

      function Check_description()
      {
          var desc = $('#description').val();
            var count = desc.split(' ').length;
             if(count<15)
             {
                alert("The property description must be greater then 15 Words");
                return false;
             }
      }

  </script>

  <script>

onload = get_neighbour_city(); 

function get_neighbour_city()
{
 var city_name = $('input[name=city]').val();
 if(city_name!="")
 {
   $.ajax({
      type:"post",
      url:"<?php echo base_url() ?>user/get_neighbour_city",
      data:{city_name:city_name},
      success:function(res)
      {
        if(res!="")
        {
          get_neighbourhoot(res);
        }
        else
        {
           $('#neighbourhoot').html("<option>No Neighborhoods Found</option>")
        }
      }

   });
 }
 else
 {
  return false; 
 }
}


 function get_neighbourhoot(city_id)
  {
      var city_id = city_id ;
      $.ajax({
              type:'POST',
               // url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city01/<?php echo $properties->id; ?>',
               url:'<?php echo base_url(); ?>user/get_neighbourhoot_of_city_on_edit_page',
              data:{city:city_id,property_id:<?php echo $properties->id ?>},
           success:function(res)
           {
              $('#neighbourhoot').html(res);
           }
      });
  }
</script>                                               
