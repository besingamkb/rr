
   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
<?php 
          alert();
 ?>
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Edit Help Category
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                  <label class="control-label" for="question">
                    Title
                  </label>
                <div class="controls controls-row span6">
                  <input name="title" class="span12" type="text" placeholder="Title" value="<?php echo $faqs_category->title ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="description" rows="5" style="width:97.50%;" ><?php echo  $faqs_category->description ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description');  ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Status
                </label>
                <div class="controls controls-row span6">
                <select name="status">
                  <option value="0" <?php if($faqs_category->status==0) echo "selected"; ?>>Unpublish</option>
                  <option value="1" <?php if($faqs_category->status==1) echo "selected"; ?>>publish</option>
                </select>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>