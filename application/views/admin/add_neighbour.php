<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
  tinyMCE.init({
    height : 400,
    convert_urls: false,
    mode : "textareas",
    editor_selector : "mceEditor",
    editor_deselector : "mceNoEditor",
    theme : "advanced",  
    plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks,openmanager",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,code,|,forecolor|,removeformat|,fullscreen",    
    file_browser_callback: "openmanager",
    open_manager_upload_path: '../../../../uploads/'

     }); 
</script>
<!-- /TinyMCE -->  

 
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Add Neighborhood
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  City
                </label>
                <div class="controls controls-row span8">
                  <select name="city" class="span4">
                    <option value="" > Select </option>
                    <?php if($cities): ?>
                      <?php foreach ($cities as $row):?>
                        <option value="<?php echo $row->id; ?>" <?php echo set_select('city', $row->id); ?> > <?php echo $row->city; ?> </option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('city'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Place
                </label>
                <div class="controls controls-row span8">
                  <input name="title" class="span12" type="text" placeholder="Title" value="<?php echo set_value('title'); ?>">
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Excerpt
                </label>
                <div class="controls controls-row span8">
                  <textarea name="excerpt" class="input-block-level no-margin" placeholder="Excerpt" style="height: 140px"><?php echo set_value('excerpt'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('excerpt'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Description
                </label>
                <div class="controls controls-row span8">
                  <textarea name="description" class="input-block-level no-margin mceEditor" placeholder="Description" style="height: 140px"><?php echo set_value('description'); ?></textarea>
                  <span class="form_error span12"><?php echo form_error('description'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Thumbnail Image
                </label>
                <span>(Resolution: 300x200)</span> 
                <div class="controls controls-row span6">
                  <input name="homeImage" class="span12" type="file" required='required' >
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Banner
                </label>
                <span>(Resolution: 1920x700)</span> 
                <div class="controls controls-row span6">
                  <input name="userfile" class="span12" type="file"   required='required'>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Categories
                </label>
                <div class="controls controls-row span8">
                  <select name="category[]" class="span4" multiple="multiple">
                    <option value="" > Select </option>
                    <?php if($categories): ?>
                      <?php foreach ($categories as $row):?>
                        <option value="<?php echo $row->id; ?>" <?php echo set_select('category', $row->id); ?> > <?php echo $row->category_name; ?> </option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('category'); ?></span>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="your-name">
                  Is Featured?
                </label>
                <div class="controls controls-row span8">
                  <select name="is_featured" class="span4">
                        <option value="0" <?php echo set_select('is_featured', 0); ?> >No</option>
                        <option value="1" <?php echo set_select('is_featured', 1); ?> >Yes</option>
                  </select> 
                  <span class="form_error span12"><?php echo form_error('is_featured'); ?></span>
                </div>
              </div>


              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>