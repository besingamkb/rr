<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin" >
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                    Review full view
                </div>
            </div>
        </div>
        <?php if(!empty($review)): ?>
        <table align="center" style="border:5px #F2F2F2 solid; border-top:none; width:50%;  margin-top:20px;" class="table table-condensed table-striped">    
            <thead>
                <tr>
                    <th style="width:120px;">
                        <span style="font-size:20px; color:#D33F3E; "  data-icon="" >
                            Review
                        </span>
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <?php 
                        $property = get_property_detail($review->property_id);
                        echo "<h2>".$property->title."</h2>";
                        ?>
                    </td>
                </tr>
    
                <tr>
                    <td colspan="2">    
                        <?php
                            $property = get_property_detail($review->property_id);
                        ?>  
                        <img style="width:100%; height:200px;" src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $property->featured_image; ?>">  
                    </td>
                </tr>
    
                <tr>
                    <td>
                        Review
                    </td>
                    <td>    
                        <?php echo $review->review; ?>
                    </td>
                </tr>
      
                <tr>
                    <td>
                        Reviewer
                    </td>
                    <td>
                        <?php 
                        $reviewer = get_user_info($review->customer_id); 
                        echo $reviewer->first_name." ".$reviewer->last_name;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Status 
                    </td>
                    <td>
                        <?php 
                        $valid = $review->status; 
                        if($valid == 1) { echo "Published"; }
                        if($valid == 0) { echo "Unpublish"; }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Created 
                    </td>
                    <td>
                        <?php echo $review->created; ?>
                    </td>
                </tr>
<link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
                <tr>
                    <td>
                        Rating 
                    </td>
                    <td>
                      <?php $rate = $review->rating; ?>
                        <input name="star3" type="radio" class="star" disabled="disabled" <?php if($rate == 1 ) { echo "checked='checked'"; } ?> /> 
                        <input name="star3" type="radio" class="star" disabled="disabled" <?php if($rate == 2 ) { echo "checked='checked'"; } ?>  /> 
                        <input name="star3" type="radio" class="star" disabled="disabled" <?php if($rate == 3 ) { echo "checked='checked'"; } ?> /> 
                        <input name="star3" type="radio" class="star" disabled="disabled" <?php if($rate == 4 ) { echo "checked='checked'"; } ?> />
                        <input name="star3" type="radio" class="star" disabled="disabled" <?php if($rate == 5 ) { echo "checked='checked'"; } ?> />
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="btn btn-success"   href="<?php echo base_url(); ?>admin/reviews" >Back</a> 
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>


<script>
$(function(){ $('#rating_div :radio.star').rating(); });

function property_rating()
{
    var cus_ses     = $('#login_check').val();
    var pr_rate     = $('#rating_div input[name=radio_rating]:radio:checked').val();
    var pr_id       = $('#pr_id').val();
    if(cus_ses == "")
    {
        alert('Please login first to rating this property.');
        return;
    }
    else
    {
        $.ajax({
                type : 'POST',
                 url : '<?php echo base_url(); ?>properties/property_rating',
                data : { pr_rate:pr_rate, pr_id:pr_id},
             success : function(res)
             {
               alert('You have been rated this property successfully.');
             }
        });
    }    
}
</script>
<script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
