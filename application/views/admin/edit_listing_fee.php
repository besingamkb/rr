
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>  Property Listing Fee
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label">
                  Type
                </label>
                <div class="wysiwyg-container controls controls-row span5">
                  <select name="type">
                      <option value="">Select  Fee Type</option>
                      <option value="1" <?php  if($listing_fee->type == 1) echo"selected";?> >Amount</option>
                      <option value="2" <?php  if($listing_fee->type == 2) echo"selected";?> >Percentage</option>
                  </select>
                  <span class="form_error span12"><?php echo form_error('type'); ?></span>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Listing Fee
                </label>
                <div class="controls controls-row span5">
                  <input name="listing_fee" class="span12" type="text" placeholder="Property Listing Fee" value="<?php echo $listing_fee->listing_fee; ?>">
                  <span class="form_error span12"><?php echo form_error('listing_fee'); ?></span>
                </div>
              </div>



              <div class="control-group">
                <label class="control-label" for="your-name">
                    Fee Duration
                </label>
                <div class="controls controls-row span5">
                  <!-- <input name="fee_duration"  type="text" placeholder=" Listing Fee" value="<?php echo set_value('fee_duration'); ?>"> -->
                  <select name="fee_duration"  class="span12"> 
                    <option value="" >Select Duration</option>
                    <option value="per_day" <?php  if($listing_fee->fee_duration == 'per_day') echo"selected";?>>Per Day</option>
                    <option value="per_week" <?php  if($listing_fee->fee_duration == 'per_week') echo"selected";?>>Per Week</option>
                    <option value="per_3_months" <?php  if($listing_fee->fee_duration == 'per_3_months') echo"selected";?>>Per 3 Months</option>
                    <option value="per_year" <?php  if($listing_fee->fee_duration == 'per_year') echo"selected";?>>Per Year</option>
                    <option value="one_time_coat" <?php  if($listing_fee->fee_duration == 'one_time_coat') echo"selected";?>>One Time Coat</option>
                    <option value="per_booking" <?php  if($listing_fee->fee_duration == 'per_booking') echo"selected";?>>Per Booking</option>
                  </select>
                  <span class="form_error span12"><?php echo form_error('fee_duration'); ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>