
<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
  <span  class="fs1" aria-hidden="true" data-icon=""></span>Tool Tip on User Profile
</div>
  <a style="float:right;margin-right:3%" href="<?php echo base_url() ?>admin/settings" class="btn btn-default">Back</a>

</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_row('tool_tip',array('tool_tip_page'=>'tool_user_profile')); ?>
    <?php if(!empty($row->page_data)): ?>
   <?php $row = json_decode($row->page_data); ?>
  <?php endif; ?>
<div class="control-group">
    <label class="control-label" for="your-name">
         First Name
    </label>
    <div class="controls controls-row span6">
          <input name="first_name" class="span12" type="text" placeholder="First Name" value="<?php if(!empty($row->first_name)) echo $row->first_name ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
          Last Name
    </label>
    <div class="controls controls-row span6">
          <input name="last_name" class="span12" type="text" placeholder="Last Name" value="<?php if(!empty($row->last_name)) echo $row->last_name ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
          Gender
    </label>
    <div class="controls controls-row span6">
          <input name="gender" class="span12" type="text" placeholder="Gender" value="<?php if(!empty($row->gender)) echo $row->gender ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Date of Birth
    </label>
    <div class="controls controls-row span6">
          <input name="dob" class="span12" type="text" placeholder="Date of birth" value="<?php if(!empty($row->dob)) echo $row->dob ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
          User Email
    </label>
    <div class="controls controls-row span6">
          <input name="user_email" class="span12" type="text" placeholder="User Email" value="<?php if(!empty($row->user_email)) echo $row->user_email ; ?>">
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
          Contact
    </label>
    <div class="controls controls-row span6">
          <input name="contact" class="span12" type="text" placeholder="Contact" value="<?php if(!empty($row->contact)) echo $row->contact ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          State
    </label>
    <div class="controls controls-row span6">
          <input name="state" class="span12" type="text" placeholder="State" value="<?php if(!empty($row->state)) echo $row->state ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          City
    </label>
    <div class="controls controls-row span6">
          <input name="city" class="span12" type="text" placeholder="City" value="<?php if(!empty($row->city)) echo $row->city ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Address
    </label>
    <div class="controls controls-row span6">
          <input name="address" class="span12" type="text" placeholder="Address" value="<?php if(!empty($row->address)) echo $row->address ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Country
    </label>
    <div class="controls controls-row span6">
          <input name="country" class="span12" type="text" placeholder="Country" value="<?php if(!empty($row->country)) echo $row->country ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Zip
    </label>
    <div class="controls controls-row span6">
          <input name="zip" class="span12" type="text" placeholder="Zip" value="<?php if(!empty($row->zip)) echo $row->zip ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Paypal Email
    </label>
    <div class="controls controls-row span6">
          <input name="paypal_email" class="span12" type="text" placeholder="Paypal Email" value="<?php if(!empty($row->paypal_email)) echo $row->paypal_email ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          School
    </label>
    <div class="controls controls-row span6">
          <input name="school" class="span12" type="text" placeholder="School" value="<?php if(!empty($row->school)) echo $row->school ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Work
    </label>
    <div class="controls controls-row span6">
          <input name="work" class="span12" type="text" placeholder="Work" value="<?php if(!empty($row->work)) echo $row->work ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Timezone
    </label>
    <div class="controls controls-row span6">
          <input name="timezone" class="span12" type="text" placeholder="Timezone" value="<?php if(!empty($row->timezone)) echo $row->timezone ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          language
    </label>
    <div class="controls controls-row span6">
          <input name="language" class="span12" type="text" placeholder="language" value="<?php if(!empty($row->language)) echo $row->language ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
          Groups
    </label>
    <div class="controls controls-row span6">
          <input name="groups" class="span12" type="text" placeholder="Groups" value="<?php if(!empty($row->groups)) echo $row->groups ; ?>">
    </div>
</div>




<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

