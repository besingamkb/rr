<?php if($this->session->flashdata('success_msg')): ?>
    <div class="alert alert-success">
      <?php echo $this->session->flashdata('success_msg');?>
    </div>
  <?php endif ?>
  <?php if($this->session->flashdata('error_msg')): ?>
    <div class="alert alert-error">
      <?php echo $this->session->flashdata('error_msg');?>
    </div>
  <?php endif ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">

          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> <a href="<?php echo base_url() ?>admin/security_deposit">Security Deposit</a>                   

          </div>
          <div class="pull-right">
            <a href="<?php echo base_url();?>admin/security_deposit" class="btn">Security Deposit</a>                      
            <a href="<?php echo base_url();?>admin/security_deposit_history" class="btn">Security Deposit History</a>                      
          </div>
        </div>
        <div class="widget-body">
          <div class="row" style="margin-left:0%;">
          <div class="span2">           
                <select onchange="return submit_sort();"  class="span10" id="sort" name="sort"> 
                  <option value="newest" <?php if($this->uri->segment(3) == 'newest') echo "selected='selected'";  ?> >Newest</option>                  
                  <option value="oldest" <?php if($this->uri->segment(3) == 'oldest') echo "selected='selected'";  ?> >Oldest</option>                  
                </select>
          </div>
          </div>

  <div style="" class="row-fluid">
      <h4>Export Security Deposit</h4><br>
      <?php echo form_open(base_url().'export/export_security_deposit'); ?>
        <div class="row-fluid">
          <input type="hidden" name="type" value="deposit_history">
            <div class="controls">
                <div class="input-append">
                    <input id="date_range1" class="span2 date_picker" type="text" placeholder="Select Date" name="report_range2">
                    <span class="add-on">
                        <i class="icon-calendar"></i>
                    </span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <select  name="export_sort"> 
                    <option value="">All</option>
                    <option value="newest" >Newest</option>                  
                    <option value="oldest" >Oldest</option>                  
                  </select>
                </div>
            </div>
        </div>
        <div class="row-fluid">
          <span class="inline radio"><input type="radio" name="export_file_format" value="csv">CSV</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" value="excel">Excel</span>
          <span class="inline radio" style="margin-left:1%"><input type="radio" name="export_file_format" checked="checked" value="pdf">PDF</span>
          <br><br>
          <span ><input type="submit" value="Export" class="btn-info btn">
        </div>  
          <?php echo form_close(); ?>        
             </div>

          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>

            
                  <th style="width:10%;">Booking id</th>
                  <th style="width:8%;">Host Name</th>
                  <th style="width:8%;">Guest Name</th>
                  <th style="width:8%;">Listing</th>
                  <th style="width:9%;">Security Deposit</th>
                  <th style="width:9%;">Booking Date</th>
                 <th style="width:35%;" class="hidden-phone">Actions</th>
                </tr>
              </thead>
              <tbody>

<!--               <?php echo "<pre>";print_r($security_deposit); ?>
 -->
               <?php if(!empty($security_deposit)):?> 
               <?php  foreach ($security_deposit as $row): ?>
               <?php $pr_detail = property_details($row->property_id); //print_r($pr_detail);  ?>                      
                <tr class="gradeA">
                  <td><a href="<?php echo base_url() ?>admin/booking_detail/<?php echo $row->booking_id ?>"><?php echo $row->booking_id ?></a></td>
                  <td><?php echo $row->host_name; ?></td>
                  <td><?php echo $row->guest_name; ?></td>                
                  <td><?php echo word_limiter(@$pr_detail->title,3); ?></td>
                  <?php $deposit = convertCurrency($row->security_deposit,'USD',$row->booking_currency_type); ?>
                  <td><?php echo $deposit; ?></td>
                  <td><?php echo date('m/d/Y', strtotime($row->created)); ?></td>
                  <td>                     
                   <!-- <td> <a href="<?php //echo base_url();?>admin/add_booking_note/<?php echo $row->id; ?>"  class="btn btn-warning btn-small" data-original-title="">Add Note</a></td> -->
                  
                    <?php $colors = get_buttons_color();  ?>
                    <?php if(!empty($colors)): ?>
                    
                     <?php if ($row->paid_to_owner == 1): ?>                       
                       <a href="<?php echo base_url() ?>admin/refund_security_deposit/<?php echo $row->id; ?>" onclick="return confirm('Are you sure?')"  class="<?php echo $colors->delete_btn ?>" data-original-title="">Refund</a>
                     <?php endif ?>                
                    <a href="<?php echo base_url() ?>admin/reciept/<?php echo $row->booking_id ?>" class="<?php echo $colors->details_btn ?>">
                     Details
                   </a>
                    <a href="<?php echo base_url() ?>admin/security_deposit_notes/<?php echo $row->id; ?>" class="<?php echo $colors->notes_btn ?>">
                     Notes
                    </a>
                  <?php endif; ?>

                  </td>
                </tr>
             <?php endforeach ?>
           <?php else: ?>
            <tr><td colspan="8">No Records Found</td></tr>
             <?php  endif ?>
            </tbody>
            </table>
              <div id="data-table_info" class="dataTables_info"></div>
                <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">

                   <?php if($pagination) echo $pagination; ?>
                   <!--  <a id="data-table_first" class="first paginate_button paginate_button_disabled" tabindex="0">First</a>
                      <a id="data-table_previous" class="previous paginate_button paginate_button_disabled" tabindex="0">Previous</a>
                       <span>
                          <a class="paginate_active" tabindex="0">1</a>
                          <a class="paginate_button" tabindex="0">2</a>
                      </span>
                      <a id="data-table_next" class="next paginate_button" tabindex="0">Next</a>
                    <a id="data-table_last" class="last paginate_button" tabindex="0">Last</a> -->
                </div>
              </div>
            <div class="clearfix"></div>
           
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
       function form_submit(){
          var sort = document.getElementById('sort'); 
            if(sort != ''){
              $("form[name='myForm']").submit();
            }
            else{
              return false;
            }        
        }



function form_submit_name(){
var name  = $('#srch').val();
if(name == '')
{
  alert('Please Enter some value');
  return false;
}
else
{
 return true;
}        
}
    </script>



    