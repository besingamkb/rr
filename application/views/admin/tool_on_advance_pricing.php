<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Tool Tip on Advance Pricing
</div>
  <a style="float:right;margin-right:3%" href="<?php echo base_url() ?>admin/settings" class="btn btn-default">Back</a>
</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>

<?php $row = get_row('tool_tip',array('tool_tip_page'=>'tool_on_advance_pricing')); ?>

  <?php if(!empty($row->page_data)): ?>
  <?php $row = json_decode($row->page_data); ?>
  <?php endif; ?>

<div class="control-group">
    <label class="control-label" for="your-name">
        Security Deposit
    </label>
    <div class="controls controls-row span6">
          <input name="security_deposit" class="span12" type="text" placeholder="Security Deposit" value="<?php if(!empty($row->security_deposit)) echo $row->security_deposit ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Daily Pricing
    </label>
    <div class="controls controls-row span6">
          <input name="daily_pricing" class="span12" type="text" placeholder="Daily Pricing" value="<?php if(!empty($row->daily_pricing)) echo $row->daily_pricing ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Weekend Pricing
    </label>
    <div class="controls controls-row span6">
          <input name="weekend_pricing" class="span12" type="text" placeholder="Weekend Pricing" value="<?php if(!empty($row->weekend_pricing)) echo $row->weekend_pricing ; ?>">
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
        Weekly Pricing
    </label>
    <div class="controls controls-row span6">
          <input name="weekly_pricing" class="span12" type="text" placeholder="Weekly Pricing" value="<?php if(!empty($row->weekly_pricing)) echo $row->weekly_pricing ; ?>">
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Monthly Pricing
    </label>
    <div class="controls controls-row span6">
          <input name="monthly_pricing" class="span12" type="text" placeholder="Monthly Pricing" value="<?php if(!empty($row->monthly_pricing)) echo $row->monthly_pricing ; ?>">
    </div>
</div>





<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

