 
<div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Neighbourhood Categories
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>admin/add_neb_category"> Add Neighbourhood Category </a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th class="no_sort"> # </th>
                  <th>Name</th>
                  <th>Created</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($categories)): ?>
                    <?php $i=1; foreach ($categories as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $row->category_name; ?></td>
                        <td><?php echo date('m/d/Y', strtotime($row->created)); ?></td>
                        <td>
                          <a href="<?php echo base_url()?>admin/delete_neb_category/<?php echo $row->id;?>" onclick="return confirm('Do you want to delete?' );" class="btn btn-success btn-small hidden-phone" data-original-title="">Delete</a>
                        </td>

                      </tr>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="4"> No Category found yet </td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>