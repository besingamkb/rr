
   <div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Edit Help
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                  <label class="control-label" for="question">
                    Question
                  </label>
                <div class="controls controls-row span6">
                  <input name="question" class="span12" type="text" placeholder="Question" value="<?php echo $faqs->question; ?>">
                  <span class="form_error span12"><?php echo form_error('question'); ?></span>
                </div>
              </div>

              <div class="control-group">
                  <label class="control-label">
                  </label>
                <div class="controls controls-row span6" style="font-size:22px">
                   Answer.
                </div>
              </div>

              <div class="control-group">
                  <label class="control-label" for="question">
                    Title
                  </label>
                <div class="controls controls-row span6">
                  <input name="title" class="span12" type="text" placeholder="Title" value="<?php echo $faqs->title; ?>">
                  <?php if(form_error('title')): ?>
                  <span class="form_error span12"><?php echo form_error('title'); ?></span>
                  <?php endif; ?>
                </div>
              </div>




              <div class="control-group">
                <label class="control-label">
                  Description
                </label>
                <div class="controls controls-row span6">
                  <textarea name="description" rows="5" style="width:97.50%;"  placeholder="Description" ><?php echo $faqs->description ?></textarea>
                  <?php if(form_error('description')): ?>
                  <span class="form_error span12"><?php echo form_error('description');  ?></span>
                <?php endif; ?>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">
                  Media Type
                </label>
                <div class="controls controls-row span6">
                <select id="get_type" onchange="get_media_type()">
                  <option value="type_1" <?php if($faqs->image) echo "selected" ?>>Image</option>
                  <option value="type_2" <?php if($faqs->video) echo "selected" ?>>Video</option>
                </select>
                </div>
              </div>

<!-- Video and image div Starts -->

              <div class="control-group img" style="display:none">
                  <label class="control-label" for="question">
                    Image
                  </label>
                <div class="controls controls-row span6">
                  <input name="faq_file" class="span12" type="file" >
                  <?php if(form_error('faq_file')): ?>
                    <span class="form_error span12"><?php echo form_error('faq_file'); ?></span>
                  <?php endif; ?>
                </div>
              </div>

              <div class="control-group img"  style="display:none">
                  <label class="control-label" for="question">

                  </label>
                <div class="controls controls-row span6">
                 <img src="<?php echo base_url() ?>assets/uploads/faq/<?php echo $faqs->image; ?>" height="120" width="120"> 
                </div>
              </div>

              <div class="control-group" style="display:none" id="video">
                  <label class="control-label" for="question">
                  Video
                  </label>
                <div class="controls controls-row span6">
                  <input name="video" class="span12" type="text" placeholder="Youtube ID" value="<?php echo $faqs->video; ?>">
                </div>
              </div>

<!-- Video and image div Ends -->



              <div class="control-group">
                <label class="control-label">
                  Category
                </label>
                <div class="controls controls-row span6">
                <?php $all_category = get_all_faqs_categories(); ?>
                <select name="faq_category">
                <?php foreach($all_category as $row): ?>
                  <option value="<?php echo $row->id ?>"><?php echo $row->title ?></option>
                <?php endforeach; ?>
                </select>
                </div>
              </div>


              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  
onload = function automatic_select()
{
   $('#video').val("");
   var type = $('#get_type').val();
   if(type=='type_1')
   {
    $('.img').show();
    $('#video').hide();
   }
   else if(type=='type_2')
   {
    $('.img').hide();
    $('#video').show();
   }
}

function get_media_type()
{
   $('#video').val("");
   var type = $('#get_type').val();
   if(type=='type_1')
   {
    $('.img').show();
    $('#video').hide();
   }
   else if(type=='type_2')
   {
    $('.img').hide();
    $('#video').show();
   }
}



</script>