<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span>Subadmin Restrictions
</div>
</div>
<?php alert() ?>

<div class="widget-body">
<?php echo form_open(current_url(), array('class' => 'form-horizontal no-margin well')); ?>


<div class="control-group">
    <label class="control-label" for="your-name">
        Subadmin Email
    </label>
    <div class="controls controls-row span6">
    <?php $user = get_row('users',array('id'=>$subadmin_id))?>
      <input  class="span12" type="text" value="<?php echo $user->user_email;?>" readonly>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Timeline
    </label>
    <div class="controls controls-row span6">
        <select name="timeline" class="span4">
            <option value="1" <?php  if(!empty($row)) if($row->timeline=="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php  if(!empty($row)) if($row->timeline=="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php  if(!empty($row)) if($row->timeline=="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        City
    </label>
    <div class="controls controls-row span6">
        <select name="city" class="span4">
            <option value="1" <?php  if(!empty($row)) if($row->city=="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php  if(!empty($row)) if($row->city=="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php  if(!empty($row)) if($row->city=="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Neighborhood 
    </label>
    <div class="controls controls-row span6">
        <select name="neighbourhood" class="span4">
            <option value="1" <?php  if(!empty($row)) if($row->neighbourhood =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php  if(!empty($row)) if($row->neighbourhood =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php  if(!empty($row)) if($row->neighbourhood =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Booking 
    </label>
    <div class="controls controls-row span6">
        <select name="booking" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->booking =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->booking =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->booking =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Properties 
    </label>
    <div class="controls controls-row span6">
        <select name="properties" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->properties =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->properties =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->properties =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        payments 
    </label>
    <div class="controls controls-row span6">
        <select name="payments" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->payments =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->payments =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->payments =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        member 
    </label>
    <div class="controls controls-row span6">
        <select name="member" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->member =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->member =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->member =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Cancellation policies 
    </label>
    <div class="controls controls-row span6">
        <select name="policies" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->policies =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->policies =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->policies =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        CMS 
    </label>
    <div class="controls controls-row span6">
        <select name="cms" class="span4">
            <option value="1" <?php if(!empty($row))  if($row->cms =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->cms =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->cms =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Collections 
    </label>
    <div class="controls controls-row span6">
        <select name="collection" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->collection =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->collection =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->collection =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        groups 
    </label>
    <div class="controls controls-row span6">
        <select name="groups" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->groups =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->groups =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->groups =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Featured Images 
    </label>
    <div class="controls controls-row span6">
        <select name="featured_images" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->featured_images =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->featured_images =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->featured_images =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Notifications 
    </label>
    <div class="controls controls-row span6">
        <select name="notifications" class="span4">
            <option value="1" <?php if(!empty($row))if($row->notifications =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row))if($row->notifications =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row))if($row->notifications =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Help 
    </label>
    <div class="controls controls-row span6">
        <select name="help" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->help =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->help =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->help =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Review 
    </label>
    <div class="controls controls-row span6">
        <select name="review" class="span4">
            <option value="1" <?php if(!empty($row))  if($row->review =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php  if(!empty($row)) if($row->review =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php  if(!empty($row)) if($row->review =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Message 
    </label>
    <div class="controls controls-row span6">
        <select name="message" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->message =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->message =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->message =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>



<div class="control-group">
    <label class="control-label" for="your-name">
        Manage Email 
    </label>
    <div class="controls controls-row span6">
        <select name="manage_email" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->manage_email =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->manage_email =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->manage_email =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>


<div class="control-group">
    <label class="control-label" for="your-name">
        Settings 
    </label>
    <div class="controls controls-row span6">
        <select name="setting" class="span4">
            <option value="1" <?php if(!empty($row)) if($row->setting =="1") echo "selected"; ?> >View only</option>
            <option value="2"  <?php if(!empty($row)) if($row->setting =="2") echo "selected"; ?> >No permission</option>
            <option value="3"  <?php if(!empty($row)) if($row->setting =="3") echo "selected"; ?> >Full permission</option>
        </select>
    </div>
</div>


<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>

