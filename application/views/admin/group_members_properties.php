
  <div class="row-fluid">
    <div class="span12">
      <div class="widget no-margin">
        <div class="widget-header">
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon="&#xe0b7;"></span> Group Properties
          </div>
          <div class="pull-right">
            <a class="btn" href="<?php echo base_url(); ?>admin/add_properties_to_group/<?php echo $group_id ?>"> Add Properties To the group </a>
          </div>
        </div>
        <div class="widget-body">
          <?php alert(); ?>
          <div id="dt_example" class="example_alt_pagination">
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
              <thead>
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:13%">Property Image</th>
                  <th style="width:20%">Property Title</th>
                  <th style="width:20%">Owner Email</th>
                  <th style="width:20%">Created</th>
                  <th style="width:20%">Actions</th>
                </tr>
              </thead>
              <tbody>
                <tbody>
                  <?php if(!empty($members_properties)): ?>
                    <?php $i=1; foreach ($members_properties as $row):?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><img style="width:80%;" src="<?php echo base_url()?>assets/uploads/property/<?php echo $row->featured_image; ?>"></td>
                        <td><?php echo $row->title; ?></td>
                        <td><?php echo $row->user_email ?></td>
                        <td><?php echo date('d-m-Y',strtotime($row->created)) ?></td>
                        
                <?php $colors = get_buttons_color();  ?>
                <?php if(!empty($colors)): ?>
                        <td>
                          <a href="<?php echo base_url()?>admin/view_member_properties/<?php echo $row->property_id;?>/<?php echo $group_id ?>"  class="<?php echo $colors->edit_btn ?>" data-original-title="">View</a>
                          <a href="<?php echo base_url()?>admin/delete_group_properties/<?php echo $row->id;?>/<?php echo $group_id ?>" onclick="return confirm('Do you want to delete?' );" role="button" class="<?php echo $colors->delete_btn ?>" data-toggle="modal" data-original-title="">Delete</a>
                        </td>
                     </tr>
                   <?php endif; ?>
                    <?php $i++; endforeach; ?>
                    <?php else: ?>
                      <tr>
                          <td colspan="5"> No Records Found</td>
                      </tr> 
                    <?php endif; ?>
                </tbody>
            </tbody>
            </table>
            
            <div class="clearfix"><?php echo $pagination ?></div>
          </div>
        </div>
      </div>
    </div>