<div id="mainnav" class="hidden-phone hidden-tablet">
      <ul>
           <?php if(uri_string() == 'admin/dashboard'): ?>
        <li class="active">
              <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>

          <a href="<?php echo base_url(); ?>admin/dashboard">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
            </div>
            Dashboard
          </a>
        </li>
         <?php if(uri_string() == 'admin/timeline'): ?>
        <li class="active">
            <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/timeline">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Timeline
          </a>
        </li>
         <?php if(uri_string() == 'admin/cities'): ?>
        <li class="active">
            <span class="current-arrow"></span>
            <?php else: ?>
              <li>
            <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/cities">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0ce;"></span>
            </div>
            Cities
          </a>
        </li>
          <?php if(uri_string() == 'admin/neighbourhoods'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>admin/neighbourhoods">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe062;"></span>
            </div>
            Neighborhoods
          </a>
        </li>
        <?php if(uri_string() == 'admin/bookings'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>admin/bookings">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Bookings
          </a>
        </li>
          <?php if(uri_string() == 'admin/properties'): ?>
        <li class="active">
            <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/properties">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Properties
          </a>
        </li>
          <?php if(uri_string() == 'admin/payments'): ?>
        <li class="active">
             <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/payments">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Payments
          </a>
        </li>
          <?php if(uri_string() == 'admin/members'): ?>
        <li><!-- style="display:none;" -->
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/members">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Members
          </a>
        </li>
          <?php if(uri_string() == 'admin/pages'): ?>
          <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/pages">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            CMS
          </a>
        </li>
          <?php if(uri_string() == 'admin/cancellation_policies'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/cancellation_policies">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Cancellation Policies
          </a>
        </li>
          <?php if(uri_string() == 'admin/collection'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/collection">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Collections
          </a>
        </li>
          <?php if(uri_string() == 'admin/groups'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/groups">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Groups
          </a>
        </li>
          <?php if(uri_string() == 'admin/featured_images'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/featured_images">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="▓"></span>
            </div>
            Featured Images
          </a>
        </li>
          <?php if(uri_string() == 'admin/notifications'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/notifications">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Notifications
          </a>
        </li>
          <?php if(uri_string() == 'admin/help'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>

          <a href="<?php echo base_url(); ?>admin/help">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="?"></span>
            </div>
           Help
          </a>
        </li>
          <?php if(uri_string() == 'admin/reviews'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/reviews">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Reviews
          </a>
        </li>
          <?php if(uri_string() == 'admin/messages'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/messages">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Messages
          </a>
        </li>
          <?php if(uri_string() == 'admin/manageEmail'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/manageEmail">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Manage Email
          </a>
        </li>
          <?php if(uri_string() == 'admin/settings'): ?>
        <li class="active">
          <span class="current-arrow"></span>
          <?php else: ?>
            <li>
          <?php endif; ?>
          <a href="<?php echo base_url(); ?>admin/settings">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            Settings
          </a>
        </li>
      </ul>
    </div>
    
    <div class="dashboard-wrapper">
      <div class="main-container">
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/timeline">Timeline</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/cities">Cities</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/neighbourhoods">Neighborhood</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/bookings">Bookings</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/properties">Properties</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/payments">Payments</a>
                  </li>
                  <li style="display:none;">
                    <a href="<?php echo base_url(); ?>admin/members">Members</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/pages">CMS</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/cancellation_policies">Cancellation Policies</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/collection">Collection</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/groups">Group</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/featured_images">Fetured Images</a>
                  </li> 
                  <li>
                    <a href="<?php echo base_url(); ?>admin/notifications">Notifications</a>
                  </li> 
                  <li>
                    <a href="<?php echo base_url(); ?>admin/help">Help</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/reviews">Reviews</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/messages">Message</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/manageEmail">Manage Email</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url(); ?>admin/settings">Settings</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        