<div class="row-fluid">
    <div class="span12">
        <div class="widget no-margin" >
            <div class="widget-header">
                <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                    View Properties
                </div>
            </div>
        </div>
        <?php if(!empty($properties_info)): ?>
        <table align="center" style="border:5px #F2F2F2 solid; border-top:none; width:50%;  margin-top:20px;" class="table table-condensed table-striped">    
            <thead>
                <tr>
                    <th style="width:155px;">
                        <span style="font-size:20px; color:#D33F3E; "  data-icon="" >
                            Property
                        </span>
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <?php 
                        echo "<h2>".$properties_info->title."</h2>";
                        ?>
                    </td>
                </tr>
    
                <tr>
                    <td colspan="2">    
                        <img style="width:100%; height:200px;" src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $properties_info->featured_image; ?>">  
                    </td>
                </tr>
    
                <tr>
                    <td>
                        Description
                    </td>
                    <td>    
                        <?php echo $properties_info->description; ?>
                    </td>
                </tr>
            <?php if(!empty($properties_info->rent)):  ?>
                <tr>
                    <td>
                        Rent
                    </td>
                    <td>    
                        $<?php echo $properties_info->rent; ?> / Night
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php if(!empty($properties_info->bed)):  ?>
                <tr>
                    <td>
                        Bedroom
                    </td>
                    <td>    
                        <?php echo $properties_info->bed; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php if(!empty($properties_info->bath)):  ?>
                <tr>
                    <td>
                        Bathroom
                    </td>
                    <td>    
                        <?php echo $properties_info->bath; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php if(!empty($properties_info->accommodates)):  ?>
                <tr>
                    <td>
                        Accommodates
                    </td>
                    <td>    
                        <?php echo $properties_info->accommodates; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
              <?php if($properties_info->city!=""):  ?>
                <tr>
                    <td>
                        City
                    </td>
                    <td>    
                        <?php echo $properties_info->city; ?>
                    </td>
                </tr>
            <?php endif; ?>
             <?php if($properties_info->state!=""):  ?>
                <tr>
                    <td>
                        State
                    </td>
                    <td>    
                        <?php echo $properties_info->state; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php if($properties_info->country!=""):  ?>
                <tr>
                    <td>
                        Country
                    </td>
                    <td>    
                        <?php echo $properties_info->country; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php $user = get_user_info($properties_info->user_id) ?>
            <?php if(!empty($user->first_name)):  ?>
                <tr>
                    <td>
                        Property Owner Name
                    </td>
                    <td>    
                        <?php echo $user->first_name." ".$user->last_name; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            <?php if(!empty($user->user_email)):  ?>
                <tr>
                    <td>
                       Property Owner Email
                    </td>
                    <td>    
                        <?php echo $user->user_email; ?>
                    </td>
                </tr>
      
            <?php endif; ?>
            

                <tr>
                    <td>
                        <a class="btn btn-success"   href="<?php echo base_url(); ?>admin/group_members_properties/<?php echo $group_id ?>" >Back</a> 
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>

