<style type="text/css">
.unique_feature {
color: #777;
/*float: left;*/
/*height: 50px;*/
padding: 0 5px 10px;
/*width: 126px;*/
}
</style>
<div class="row-fluid">
<div class="span12">
<div class="widget">
<div class="widget-header">
<div class="title">
<span class="fs1" aria-hidden="true" data-icon=""></span> Change Media Images
</div>
</div>

<div class="widget-body">
<?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>


<div class="control-group">
    <label class="control-label" for="your-name">
        Author
    </label>
    <div class="controls controls-row span6">
        <input name="author" class="span12" id="phone" type="text" placeholder="Contact Number" value="<?php echo ucfirst($change_media_buttons->name)  ?>" readonly>
    </div>
        <span class="form_error span12"></span>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
       Image
    </label>
    <div class="controls controls-row span6">
        <input name="button_images" class="span12" type="file">
        <span class="form_error span12"><?php echo form_error('button_images'); ?></span>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="your-name">
    </label>
    <div class="controls controls-row span6">
            <?php if(!empty($change_media_buttons->button_images)): ?>
           <img style="max-width:150px;max-height:150px" class="img-polaroid" src="<?php echo base_url() ?>assets/img/third_party/<?php echo $change_media_buttons->button_images ?>">
            <?php endif; ?>
        <span class="form_error span12"></span>
    </div>
</div>



<div class="form-actions no-margin">
    <button type="submit" class="btn btn-info">
        Save
    </button>
</div>

<?php echo form_close(); ?>

</div>
</div>
</div>
</div>
</div>


