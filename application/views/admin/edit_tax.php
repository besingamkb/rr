
<div class="row-fluid">
    <div class="span12">
      <div class="widget">
          <div class="widget-header">
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span>Tax Setting  <span style="font-size:10px;">(In percentage)</span>
            </div>
          </div>
          <div class="widget-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal no-margin well')); ?>
              <div class="control-group">
                <label class="control-label">
                  Tax Type
                </label>
                <div class="wysiwyg-container controls controls-row span5">
                  <input name="tax_type" class="span12" type="text" placeholder="Tax Type" readonly="readonly" value="Percentage">
                  <span class="form_error span12"><?php echo form_error('tax_type'); ?></span>
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label" for="your-name">
                  Tax 
                </label>
                <div class="controls controls-row span5">
                  <input name="tax" class="span12" type="text" placeholder="Tax" value="<?php echo $taxs->tax; ?>">
                  <span class="form_error span12"><?php echo form_error('tax'); ?></span>
                </div>
              </div>

              
              <div class="form-actions no-margin">
                <button type="submit" class="btn btn-info">
                  Save
                </button>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>