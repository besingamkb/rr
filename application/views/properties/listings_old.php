    <div class="container">      
        <div class="row">
            <div class="span12" style="">
                        <form id="srch_form">
                <div class="searchsort">                   
                          <!-- <form class="form-inline" id="src_cty_from" method="POST" action="<?php echo base_url() ?>properties/index"> -->
                            <div class="input-append">
                                <input class="span2 filter-input" placeholder="City" name="location" id="appendedInputButton" type="text">
                                <button class="btn" id="src_cty" type="button">Search</button>
                                <!-- <button class="btn" id="src_cty" onclick="return check_val()" type="button">Search</button> -->
                            </div>                            
                            <a href="javascript:void(0)" id="map_view" style="margin-left:10px;" class="btn pull-right">Map View</a>
                            <a href="javascript:void(0)" id="photo_view" class="btn pull-right">Photo View</a>
                          <!-- </form> -->

                  </div>
                <div class="span3 filter">
                    <div>
                          <div class="control-group">
                            <label class="control-label" for="">Sort By</label>
                            <div class="controls">
                              <select name="sortby" id="sortby" class="filter-input">
                                <option value="">Please select</option>
                                <option value="recommended">Recommended</option>
                                <option value="l2h">Price: low to high</option>
                                <option value="h2l">Price: high to low</option>
                                <option value="new">Newest</option>
                              </select>
                            </div>                            
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="">Room Type </label>
                            <div class="controls">
                              <select name="room_type" id="room_type" class="filter-input">
                                <option value="">Please select</option>
                                <option value="3">Entire Place</option>
                                <option value="4">Sweet</option>
                                <option value="2">Private room</option>
                                <option value="1">Shared Room</option>
                              </select>
                            </div>                            
                          </div>
                          <hr>
                          <div class="control-group">
                            <label class="control-label" for="">Price <span id="amount" style="border:0; color:#e05284; font-weight:bold;" ></span><!-- <input type="text" id="amount" style="border:0; color:#e05284; font-weight:bold;"> --></label>
                            <div class="controls">
                              <div id="slider-range"></div>
                              <input type="hidden" id="pricerange" name="pricerange">
                              <!-- <select name="room_type" class="filter-input">
                                <option value="">Please select</option>
                                <option>Entire Place</option>
                                <option>Private room</option>
                                <option>Shared Room</option>
                              </select> -->
                            </div>                            
                          </div>
                          <hr>
                          <div class="control-group">
                            <label class="control-label" for="">Bedrooms <a href="javascript:void(0)" id="bedtoggle"><span class="pull-right toogle-open">(+)</span></a></label>
                            <div class="controls" id="bedtoggle2" style="display:none">
                              <select name="bedroom" id="beedroom" class="filter-input">
                                <option value="">Please select</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>5+</option>
                              </select>
                            </div>                            
                          </div>
                          <hr>
                          <div class="control-group">
                            <label class="control-label" for="">Bathroom <a href="javascript:void(0)" id="bathtoggle"><span class="pull-right toogle-open">(+)</span></a></label>
                            <div class="controls" id="bathtoggle2" style="display:none">
                              <select name="bathrooms" id="bathroom" class="filter-input">
                                <option value="">Please select</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>5+</option>
                              </select>
                            </div>                            
                          </div>
                          <hr>
                          <!-- <div class="control-group">
                            <label class="control-label" for="">Beds <a href="javascript:void(0)" id="b_toggle"><span class="pull-right toogle-open">(+)</span></a></label>
                            <div class="controls" id="b_toggle2" style="display:none">
                              <select name="beds" id="bed" class="filter-input">
                                <option value="">Please select</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5+</option>
                              </select>
                            </div>                            
                          </div> -->
                          <hr>
                          <div class="control-group">
                            <label class="control-label" for="">Amenities <a href="javascript:void(0)" id="am_toggle"><span class="pull-right toogle-open">(+)</span></a></label>
                            <div class="controls" id="am_toggle2" style="display:none">
                              <?php if ($amenities): $i=1; foreach ($amenities as $amenity) { ?>                                
                                  <input type="checkbox" class="row amenity" value="<?php echo $amenity->id ?>" name="amenities[]"> <?php echo $amenity->name; ?><br>                              
                                <?php /* if($i < 6 ){ ?>
                                <?php }else{ ?>
                                  <div style="display:block" id="rem_amenity">
                                    <input type="checkbox" class="row" value="<?php echo $amenity->id ?>" name="amenities[]"> <?php echo $amenity->name; ?><br>
                                    </div>
                                <?php }  */ ?>
                              <?php $i++; } ?>  <?php endif; ?>
                                  <!-- <br><span class="a_more"><a href="#" id="more_am"> more +</a></span> -->
                            </div>                            
                          </div>
                          <hr>
                          <div class="control-group">
                            <label class="control-label" for="">Keywords</label>
                            <div class="controls">
                                  <div class="input-append">
                                        <input class="span2 filter-input" name="keyword" id="appendedInputButton" type="text">
                                        <button class="btn" id="keyword" type="button">Go!</button>
                                    </div>
                            </div>                            
                          </div>
                    </div>
                        </form>
                </div>                
                <div class="span8 listings" id="map" style="display:none">
                </div>
                <div class="span8 listings" id="all_list" style="display:block">                  
                  <?php if ($property): foreach ($property as $row) { ?>
                      <div class="span3 img-item">
                        <img src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->featured_image ?>">
                        <p class="list-details"><span class="p-title"><a style="text-decoration:none; color:#E05284;" href="<?php echo base_url(); ?>properties/details/<?php echo $row->id; ?>"><?php echo $row->title ?></a></span> <br>
                            <?php echo $row->first_name.' '.$row->last_name ?>
                        </p>
                        <p class="pull-right">
                           <span class="p-price"> $<?php echo $row->application_fee ?></span> <br>
                            <?php //echo $row->room_allotment; ?>
                        </p>
                      </div>                    
                      <?php } ?>
                  <?php else: ?>
                    <p>Nothing found</p>
                  <?php endif ?>
                </div>
                <div class="span3"></div>
                <div class="span8 load" id="load_div">
                    <a href="javascript:void(0)"  class="btn" id="load_more" >Load more</a>
                </div>
                <input type="hidden" name="selected_location" id="selected_location" value="<?php echo $location ?>">
            </div>
        </div>
    </div>   


    <div id="location_div" style="display:none;">
      <?php if ($property): foreach ($property as $row) { ?>
        <span id="<?php echo $row->id ?>"><?php echo $row->unit_street_address.' '.$row->city.' '.$row->state.' '.$row->country; ?></span>
      <?php } endif; ?>
    </div>
<style type="text/css">
    /*new*/

    .control-label a{
        text-decoration: none;
    }

    .control-group label{
        width: 80%;
    }

    #load_more, #load_more2{
        width: 20%;
        margin-bottom: 2%;
    }

    .load{
        /*text-align: center;*/
    }
    .img-item{
        border: 1px solid #989D9F;     
        margin:auto 4% 4% 4%;   
    }

    .p-price{
        font-size: 18px;
        color:#E05284;
    }
    .p-title{        
        color:#E05284;
        font-weight: bold
    }

    .list-details{
        float: left;
    }

    .img-item p{
        padding: 10px;
        margin-bottom: 0px;
        /*float: left;*/
    }

    .filter-input{
        height: 30px !important;
    }

    .filter{
        padding-top: 30px;
        color:#989D9F;
        margin-left: 0px;
    }

    .listings{
        padding-top: 30px;
        color:#989D9F;  
        /*border-left: 1px solid;      */
        /*margin-left: 0px;*/        
    }
    /*.a_more{
        text-align: center;
        width: 100%;
    }*/

    hr{
        width: 80%;
        margin: 10px 0;
    }

    #slider-range{
      width: 80%;
    }

    .searchsort{
      padding-top:1%; 
      width: 80%;
    }

    #map{
      width: 55%;
    }

    .ui-widget-header{
      background-color: #e05284; 
    }

</style>
<script type="text/javascript">
    $('#bedtoggle').click(function(){        
         if($('#bedtoggle span').text() == "(-)")
        $('#bedtoggle span').text('(+)');
      else
        $('#bedtoggle span').text('(-)');
        $('#bedtoggle2').slideToggle();
    });

    $('#bathtoggle').click(function(){        
         if($('#bathtoggle span').text() == "(-)")
        $('#bathtoggle span').text('(+)');
      else
        $('#bathtoggle span').text('(-)');
        $('#bathtoggle2').slideToggle();
    });

    $('#b_toggle').click(function(){        
         if($('#b_toggle span').text() == "(-)")
        $('#b_toggle span').text('(+)');
      else
        $('#b_toggle span').text('(-)');
        $('#b_toggle2').slideToggle();
    });

    $('#am_toggle').click(function(){        
         if($('#am_toggle span').text() == "(-)")
        $('#am_toggle span').text('(+)');
      else
        $('#am_toggle span').text('(-)');
        $('#am_toggle2').slideToggle();
    });

    // $('#more_am').click(function(){      
    //  if($('#rem_amenity span').text() == " more -")
    //     $('#rem_amenity span').text(' more +');
    //   else
    //     $('#rem_amenity span').text(' more -');
    //     $('#rem_amenity').slideToggle();
    // })
    
    $('#room_type, #beedroom, #bathroom, #bed, #sortby').on('change', function(){
        $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/',            
            success: function (res) {              
                if(res !=""){
                  var list = JSON.parse(res);
                  // alert(list.loca);
                  $('#all_list').html(list.list);
                  $('#location_div').html(list.loctn);
                  $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');
                }else{
                  $('#all_list').html('Nothing found');
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }                
               
             }
          });  
    });


    $('.amenity, #keyword, #src_cty').on('click', function(){
        $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/',            
            success: function (res) {                     
                $('#load_more').data('offset','10');     
                if(res !=""){              
                var list = JSON.parse(res);
                  // alert(list.loca);
                  $('#all_list').html(list.list);                      
                  $('#location_div').html(list.loctn);                      
                   $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');
                }else{
                  $('#all_list').html('Nothing found');                  
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }              
               
             }
          });  
    });

    $('#load_more').click(function(){
       var location = $('#selected_location').val();
       var offset = $('#all_list .img-item').length;
       $.ajax({
        type:'POST',
        data:{ location:location },
        url:'<?php echo base_url() ?>properties/ajaxloadmore/'+offset,
        success: function (res) {                                     
                if(res !=""){
                   var list = JSON.parse(res);                  
                  $('#all_list').append(list.list);
                  $('#location_div').append(list.loctn);
                  // $('#all_list').append(res);
                }else{                  
                  $('#load_more').hide();
                }              
               
             }
       });
    });

    // $('#load_more2').click(function(){
    //    var offset = $('#all_list .img-item').length;
    //    $.ajax({
    //         type: "POST",
    //         data: $('#srch_form').serialize(),
    //         url: '<?php echo base_url() ?>properties/ajaxfilter_property/'+offset,            
    //         success: function (res) {                                     
    //             if(res !=""){                  
    //               $('#all_list').append(res);                   
    //             }else{
    //               $('#all_list').append('No more result to display');                  
    //               $('#load_more').hide();
    //               $('#load_more2').hide();
    //             }              
               
    //          }
    //       });
    // });

    $(document).on("click","#load_more2",function(){
        var offset = $('#all_list .img-item').length;
       $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/'+offset,            
            success: function (res) {                                                 
                if(res !=""){  
                  var list = JSON.parse(res);                  
                  $('#all_list').append(list.list);
                  $('#location_div').append(list.loctn);                
                  // $('#all_list').append(res);                   
                }else{
                  $('#all_list').append('<div class="span5">No more result to display</div>');                  
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }              
               
             }
          });
    });


  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: '<?php echo(getMaxprice()->application_fee); ?>',
      values: [ 0, '<?php echo(getMaxprice()->application_fee); ?>' ],
      slide: function( event, ui ) {
        $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        $('#pricerange').val(ui.values[ 0 ] + "-" + ui.values[ 1 ]);
        // alert($('#pricerange').val());

        $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/',            
            success: function (res) {                     
                $('#load_more').data('offset','10');     
                if(res !=""){                  
                  $('#all_list').html(list.list);                      
                  $('#location_div').html(list.loctn);                      
                   $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');
                  // $('#all_list').html(res);
                  //  $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');
                }else{
                  $('#all_list').html('Nothing found');                  
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }              
               
             }
          });  
        
      }
    });
    $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });

    function check_val(){
      var val = $('#appendedInputButton').val();
      
      if(val !=""){
        $('#src_cty_from').submit();
        return true;
      }else{
        return false;
        
      }

    }


    var map;
    var geocoder;

    $('#map_view').click(function(){
      initialize();

      function initialize() {
        geocoder = new google.maps.Geocoder();
        var mapOptions ={
            center: new google.maps.LatLng(41.882598,-87.638283),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);         
        $( "#location_div span" ).each(function( index ) {
            var address = $(this).html();            
            var loc_id = $(this).attr("id");
            var latLang = findlatlang(address, loc_id);
            // var latLang = findlatlang('Vijay Nagar, Indore, Madhya Pradesh', loc_id);
            // console.log(latLang);
          });     
        // google.maps.Size(width:"200", height:"300")
          $("#map").css("height", "400px");
          // google.maps.event.trigger(map, "resize");

      }


       function findlatlang(address, loc_id) {
        // var address ='rajwada indore mp india'; 
        var geocoder = new google.maps.Geocoder();
        var lat ='';
        var lang ='';
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat  = results[0].geometry.location.lat();
            lang = results[0].geometry.location.lng();            
            placeMarker(new google.maps.LatLng(lat,  lang), map, loc_id);
          } else {
            result = "Unable to find address: " + status;
          }
        });
      }

      function placeMarker(position, map, loc_id) { 
        var marker = new google.maps.Marker({
          position: position,
          map: map,
          title: 'Vacalio'
        });
        // map.panTo(position);
        var lat = position.lb;
        var lng = position.mb;
        var latlng = new google.maps.LatLng(lat,lng);
        

        // var contentString = "<img src='<?php echo base_url() ?>assets/front/img/logo.png' class='img-responsive'><h1 style=\"font-family: 'FuturaStd-Book';color: #333333;font-size: 17px;margin-top: 25px;display: block;\">Hooray Puree Store</h1>";
        // var contentString = '<div style="height:250px; padding:10px;">';
        // contentString += $("#span_no_"+loc_id).html();
        // contentString += '</div>';
        

        // var infowindow = new google.maps.InfoWindow({
        //   content: contentString
        // });
        // google.maps.event.addListener(marker, 'click', function() {
        //   infowindow.open(map,marker);
        // });
        geocoder.geocode({'latLng': latlng},function(results, status) {
          if (status == google.maps.GeocoderStatus.OK){
            if (results[1]) {
              // infowindow.setContent(results[1].formatted_address);
              // infowindow.open(map,marker);
            } 
          } else {
            if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
              setTimeout(function() { placeMarker(position,map); }, (200));
            }
          }
        });
      }

      $('#map').show();
      $('#all_list').hide();
      google.maps.event.trigger(map, 'resize');
      $('#load_more2').hide();
      $('#load_more').hide();


    });

    $('#photo_view').click(function(){
      $('#map').html("");
      $('#map').css("");
      $('#map').hide();
      $('#all_list').show();
      $('#load_more2').show();
      $('#load_more').show();
    });

</script>  