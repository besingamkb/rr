
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/booking.css" />
<?php $currency = $this->session->userdata('currency'); ?>
<?php $commision_fee = get_commision_fee(); ?>
<?php $tax_fee = get_taxs(); ?>
<!--Main div start -->
<p>&nbsp;</p>
<div>
  <!--header and contant div start -->
  <div class="mainwidth">
    
     <!--contant div start -->
     <div class="contantwidth">
          <div class="contantinner">

<div id="content_wrapper" class="clearfix">


  <div class="relative">
	<div id="how_it_works" style="background-color:#EACCD6">
	  <h1>How this works</h1>
	  <p>Provide your booking details below. <b>You will only be charged if the host accepts your request.</b> They'll have 24 hours to reply.</p>
	  <p>If the host declines or does not respond, no charge is made. Then you can try booking with someone else.</p>
    </div>

<div id="book_it">
<?php echo form_open(base_url().'properties/confirm_booking'); ?>
        <input name="pr_id" type="hidden" value="<?php echo $details->id; ?>">
        <input name="owner_id" type="hidden" value="<?php echo $details->user_id; ?>">
        <input name="check_in" type="hidden" value="<?php echo $cin; ?>">
        <input name="check_out" type="hidden" value="<?php echo $cout; ?>" >
         
         <?php if(!empty($coupon_type)): ?>
            <?php if($coupon_type=="discount"): ?>
              <?php 
                $after_discount_price = get_after_discount_price($subtotal,$coupon_amount);
                if(!empty($commision_fee->commision_fee))
                {
                  $a = ($subtotal*$commision_fee->commision_fee);
                  $commision = $a/100;
                  $commision = convertCurrency($commision, $currency,$details->currency_type); 
                  $after_discount_price = $commision+$after_discount_price;
                }
                if(!empty($tax_fee->tax))
                {
                    $a = $subtotal*$tax_fee->tax;
                    $tax = $a/100;
                    $tax = convertCurrency($tax, $currency,$details->currency_type); 
                    $after_discount_price = $tax+$after_discount_price;
                }
                if(!empty($details->security_deposit))
                {
                  $security_deposit = convertCurrency($details->security_deposit,$currency,$details->currency_type);
                  $after_discount_price = number_format($security_deposit,2)+$after_discount_price;
                }
                if(!empty($details->cleaning_fee))
                {
                  $cleaning_fee = convertCurrency($details->cleaning_fee,$currency,$details->currency_type);
                  $after_discount_price = number_format($cleaning_fee,2)+$after_discount_price;
                }

               ?>

              <input name="commision" type="hidden" value="<?php echo $commision ?>">
              <input name="tax" type="hidden" value="<?php echo $tax ?>">
              <input name="coupon_type" type="hidden" value="percent">
              <input name="coupon_type" type="hidden" value="percent">
              <input name="coupon_amount" type="hidden" value="<?php echo $coupon_amount ?>">
              <input name="total_amount" type="hidden" value="<?php echo number_format($after_discount_price,2);  ?>">
            <?php else: ?>
              <?php 
                 $after_reduction = $subtotal-convertCurrency($coupon_amount,$currency);
                  if(!empty($commision_fee->commision_fee))
                  {
                      $a = $subtotal*$commision_fee->commision_fee;
                      $commision = $a/100;
                      $commision = convertCurrency($commision, $currency,$details->currency_type); 
                      $after_reduction = $commision+$after_reduction;
                  }
                  if(!empty($tax_fee->tax))
                  {
                      $a = $subtotal*$tax_fee->tax;
                      $tax = $a/100;
                      $tax = convertCurrency($tax, $currency,$details->currency_type); 
                      $after_reduction = $tax+$after_reduction;
                  }
                  if(!empty($details->security_deposit))
                  {
                    $security_deposit = convertCurrency($details->security_deposit,$currency,$details->currency_type);
                    $after_reduction = number_format($security_deposit,2)+$after_reduction;
                  }
                  if(!empty($details->cleaning_fee))
                  {
                    $cleaning_fee = convertCurrency($details->cleaning_fee,$currency,$details->currency_type);
                    $after_reduction = number_format($cleaning_fee,2)+$after_reduction;
                  }

               ?>
              <input name="commision" type="hidden" value="<?php echo $commision ?>">
              <input name="tax" type="hidden" value="<?php echo $tax ?>">
              <input name="coupon_type" type="hidden" value="amount">
              <input name="coupon_amount" type="hidden" value="<?php echo convertCurrency($coupon_amount,$currency) ?>">
            <input name="total_amount" type="hidden" value="<?php echo number_format($after_reduction,2);  ?>">
            <?php endif; ?>
         <?php else: ?>
            <?php 
              
              
              $paypal_amount = $subtotal;
              if(!empty($commision_fee->commision_fee))
              {
                      $a = $subtotal*$commision_fee->commision_fee;
                      $commision = $a/100;
                      $commision = convertCurrency($commision, $currency,$details->currency_type); 
                      $paypal_amount = $commision+$paypal_amount;
              }
              if(!empty($tax_fee->tax))
              {
                      $a = $subtotal*$tax_fee->tax;
                      $tax = $a/100;
                      $tax = convertCurrency($tax, $currency,$details->currency_type); 
                      $paypal_amount = $tax+$paypal_amount;
              }
              if(!empty($details->security_deposit))
              {
                $security_deposit = convertCurrency($details->security_deposit,$currency,$details->currency_type);
                $paypal_amount = number_format($security_deposit,2)+$paypal_amount;
              }
              if(!empty($details->cleaning_fee))
              {
                $cleaning_fee = convertCurrency($details->cleaning_fee,$currency,$details->currency_type);
                $paypal_amount = number_format($cleaning_fee,2)+$paypal_amount;
              }
            ?>
              <input name="commision" type="hidden" value="<?php echo $commision ?>">
              <input name="tax" type="hidden" value="<?php echo $tax ?>">
             <input name="total_amount" type="hidden" value="<?php echo number_format($paypal_amount,2);  ?>">
        <?php endif; ?>


		<div id="message-host" class="book_it_section first">
		  <h1>1. Include a message to your host</h1>
		  <p>Hosts like to know the purpose of your trip and the others traveling in your party. They also appreciate knowing what you like about their place.</p>
		  <p>If you have any questions, include those too.</p>
		  <div>
			<div class="host-wrapper">
			  <div class="_pm_container small">
			    <div class="_pm_shadow r"></div>
			    <div class="_pm_shadow l"></div>
			    <div class="_pm">
			      <div class="_pm_inner clearfix">
			        <div class="_pm_shadow_inner r"></div>
			        <div class="_pm_shadow_inner l"></div>
			        <div class="_pm_shadow_inner t"></div>
			        <div class="_pm_shadow_inner b"></div>
      				<img alt="Subadmin" style="border:5px solid #EACCD6;" height="68" src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $details->image; ?>" width="68">
				  </div>
  				</div>
			  </div>
			  <div class="host-name"><?php echo $details->first_name." ".$details->last_name; ?></div>
			</div>
			<textarea id="message_to_host" name="message" rows="5"></textarea>
		  </div>
		</div>

<div id="property_details" >
  <h1>2. The Listing</h1>
  <div class="content_box" style="border:none;">
    <img src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $details->featured_image; ?>" height="114" width="74" border="0" class="main_photo">
    <div id="hosting_details" >
      <h2><a href="<?php echo base_url(); ?>properties/details/<?php echo $details->id; ?>" target="_blank"><?php echo ucfirst($details->title); ?></a></h2>
      <div class="box overflow-auto listing-infos-container">
        <div class="left address-box">
          <?php echo $details->unit_street_address; ?>
        </div>
        <div class="amentities-box right">
          <ul id="amentities-list" class="overflow-hidden">
            <li class="amentity alt">
              <span class="property">Room Type:</span>
              <span class="value right"><?php echo get_room_types($details->room_type); ?></span>
            </li>
            <li class="amentity">
              <span class="property">Cancellation:</span>
              <span class="value right">Flexible</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>

<style>
#dynamic_div div{ margin-top:15px !important;}
</style>
<div style="font-size:18px;" id="dynamic_div">
  <h1>3. Your Trip</h1>
  <ul style="list-style:none; ">
    <li>
       <div class="span2">
        Check In
       </div>       
       <div class="span4"> 
        <?php echo date('d-M-Y', strtotime($cin)); ?>(Flexible check in time)
       </div>
    </li>
    <li>
       <div class="span2">
        Check Out
       </div>       
       <div class="span4"> 
        <?php echo date('d-M-Y', strtotime($cout)); ?>(Flexible check in time)
       </div>
    </li>
    <li>
       <div class="span2">
        Nights
       </div>       
       <div class="span4"> 
       <?php $time = strtotime($cout)-strtotime($cin) ?>
       <?php $days = ($time/86400)+1 ?>
        <?php echo $days; ?>     
       </div>
    </li>
    <li>
       <div class="span2">
          Rate per night 
       </div>       
       <div class="span4"> 
        <?php echo number_format(($subtotal)/$days,2).' '.$currency; ?> (approximate..)
       </div>
    </li>

    <!-- Coupon work starts -->
         <?php if(!empty($coupon_type)): ?>

      <li>
         <div class="span2">
          Subtotal 
         </div>       
         <div class="span4"> 
          <?php echo number_format($subtotal,2).' '.$currency; ?> (before coupon discount applied)
         </div>
      </li>


           <!-- If Coupon type == Discount Starts -->
      <?php if($coupon_type=="discount"): ?>
      <li>
         <div class="span2">
          Coupon Type
         </div>       
         <div class="span4"> 
             Percent
         </div>
      </li>
      <li>
         <div class="span2">
          Coupon Discount
         </div>       
         <div class="span4"> 
            <?php echo $coupon_amount." %" ?>
         </div>
      </li>
      <li>
         <div class="span2">
          Subtotal
         </div>       
         <div class="span4">
          <?php 
            $after_discount_price = get_after_discount_price($subtotal,$coupon_amount);
           ?>
          <?php echo number_format($after_discount_price,2).' '.$currency; ?>
         </div>
      </li>

      <li>
         <div class="span2">
          Commision fee
         </div>       
         <div class="span4">
                  <?php
                      $a = $subtotal*$commision_fee->commision_fee;
                      $commision = $a/100;
                   ?>
          <?php echo convertCurrency($commision, $currency,$details->currency_type).' '.$currency; ?>
         </div>
      </li>

      <li>
         <div class="span2">
          Tax fee
         </div>       
         <div class="span4">
                  <?php
                      $a = $subtotal*$tax_fee->tax;
                      $tax = $a/100;
                   ?>
          <?php echo convertCurrency($tax, $currency,$details->currency_type).' '.$currency; ?>
         </div>
      </li>


      <li>
         <div class="span2" style="color:#E05284;">
          Total
         </div>       
         <div class="span4" style="color:#E05284;"> 
          <?php $final_amounnt = $after_discount_price+convertCurrency($commision, $currency,$details->currency_type)+convertCurrency($tax, $currency,$details->currency_type) ?>
          <?php echo number_format($final_amounnt,2).' '.$currency; ?>
         </div>
      </li>
           <!-- If Coupon type == Discount Ends -->

           <!-- If Coupon type == Amount Starts -->
                <?php else: ?>
      <li>
         <div class="span2">
          Coupon Type
         </div>       
         <div class="span4"> 
             Amount
         </div>
      </li>
      <li>
         <div class="span2">
          Coupon Amount
         </div>       
         <div class="span4"> 
            <?php echo convertCurrency($coupon_amount,$currency)." ".$currency; ?>
         </div>
      </li>
      <li>
         <div class="span2">
          Subtotal
         </div>       
         <div class="span4">
          <?php 
            $after_reduction = $subtotal-convertCurrency($coupon_amount,$currency);
           ?>
          <?php echo number_format($after_reduction,2).' '.$currency; ?>
         </div>
      </li>
      <li>
         <div class="span2">
          Commision fee
         </div>       
         <div class="span4">
                  <?php 
                      $a = $subtotal*$commision_fee->commision_fee;
                      $commision = $a/100;
                   ?>
          <?php echo convertCurrency($commision, $currency,$details->currency_type).' '.$currency; ?>
         </div>
      </li>

      <li>
         <div class="span2">
          Tax fee
         </div>       
         <div class="span4">
                  <?php
                      $a = $subtotal*$tax_fee->tax;
                      $tax = $a/100;
                   ?>
          <?php echo convertCurrency($tax, $currency,$details->currency_type).' '.$currency; ?>
         </div>
      </li>

      <li>
         <div class="span2" style="color:#E05284;">
          Total
         </div>       
         <div class="span4" style="color:#E05284;"> 
          <?php $final_amounnt = $after_reduction+convertCurrency($commision, $currency,$details->currency_type)+convertCurrency($tax, $currency,$details->currency_type) ?>
          <?php echo number_format($final_amounnt,2).' '.$currency; ?>
         </div>
      </li>
           <!-- If Coupon type == Amount Ends -->
                <?php endif; ?>
         <?php else: ?>
      <li>
        <div class="span2">
          Subtotal
        </div>       
        <div class="span4">
          <?php echo $subtotal.' '.$currency; ?>
        </div>
      </li>
      <li>
         <div class="span2">
          Commision fee
         </div>       
         <div class="span4">

                  <?php 
                      $a = $subtotal*$commision_fee->commision_fee;
                      $commision = $a/100;
                   ?>
          <?php echo convertCurrency($commision, $currency,$details->currency_type).' '.$currency; ?>
         </div>
      </li>
      <li>
         <div class="span2">
          Tax fee
         </div>       
         <div class="span4">
                  <?php
                  
                      $a = $subtotal*$tax_fee->tax;
                      $tax = $a/100;
                   ?>
          <?php echo convertCurrency($tax, $currency,$details->currency_type).' '.$currency; ?>
         </div>
      </li>
      <li>
         <div class="span2" style="color:#E05284;">
          Total
         </div>       
         <div class="span4" style="color:#E05284;"> 
          <?php $final_amounnt = $subtotal+convertCurrency($commision, $currency,$details->currency_type)+convertCurrency($tax, $currency,$details->currency_type) ?>
          <?php echo number_format($final_amounnt,2).' '.$currency; ?>
         </div>
      </li>

        <?php endif; ?>
        <!-- Coupon work ends -->


        <!-- Security Deposit Work Starts -->
    
    <?php if(!empty($details->security_deposit)): ?>
      <li>
         <div class="span2">
          Security Deposit
         </div>       
         <div class="span4"> 
          <?php $secu_deposit =  convertCurrency($details->security_deposit,$currency,$details->currency_type); ?>
          <?php echo number_format($secu_deposit,2).' '.$currency; ?>
         </div>
      </li>
    <?php endif; ?>

    <?php if(!empty($details->cleaning_fee)): ?>
      <li>
         <div class="span2">
          Cleaning Fee
         </div>       
         <div class="span4"> 
          <?php $cleaning_fee =  convertCurrency($details->cleaning_fee,$currency,$details->currency_type); ?>
          <?php echo number_format($cleaning_fee,2).' '.$currency; ?>
         </div>
      </li>
    <?php endif; ?>

    <!-- Security Deposit Work Ends -->


  </ul>
</div>

        <div id="verification_options" class="book_it_section contact_requirements">
          <h1>4. Additional Required Information</h1>
                    <div id="p4-verifications">
            <div class="verification-flow-container content-row">
    		  <div class="verification-flow-panels">
        		<div class="verification-flow-panel basic_profile" data-step="basic_profile" style="display: block; ">
  				  <div class="verification-flow-header clearfix">
    				<h3>Add a description to your public profile</h3>
  				  </div>
  				  <p class="suggestion">Where you're from, what you like to do - anything that gives a sense of who you are.</p>
  				  <textarea id="aboutme" name="comment" required="required"></textarea>
				</div>
    		  </div>
			</div>
          </div>
		            <div class="content_box additional_info">
            <div class="clearfix needs_location">
              <label for="address">What city are you currently in?</label>
              <input  id="address" name="city" placeholder="Your current city" type="text" required="required" style="height: 35px !important">
              <span class="example">e.g. New York, NY; San Francisco, CA; Paris, FR</span>
            </div>
          </div>
		  		  
          <div class="content_box can_host_call">
            <span class="addendum">
  			  <label for="guest_preferences_host_can_call_yes"><input checked="checked" id="host_callY" name="inquiry" type="radio" value="1"> Yes</label>
  			  <label for="guest_preferences_host_can_call_no"><input id="host_callN" name="inquiry" type="radio" value="0"> No</label>
			</span>
			<h3>Can this host call you about your inquiry?</h3>
			<span class="suggestion">Your number won't be revealed. They can only call from 9am to 9pm in your time zone.</span>
			<span class="timezone change">
  			  <select name="time_zone" id="guest_preferences_time_zone" required="required">
            <option value="" selected="selected">Choose a time zone</option>
            <option value="GMT - 12:00">(GMT - 12:00) International Date Line West</option>
            <option value="GMT - 11:00">(GMT - 11:00) Midway Island, Samoa</option>
            <option value="GMT - 10:00">(GMT - 10:00) Hawali</option>
            <option value="GMT - 09:00">(GMT - 09:00) Alaska</option>
            <option value="GMT - 08:00">(GMT - 08:00) Pasific Time(US & Canada); Tijuana</option>
            <option value="GMT - 07:00">(GMT - 07:00) Mauntain Time(US & Canada)</option>
            <option value="GMT - 07:00">(GMT - 07:00) Chihuahua,La Paz,Mazatlen</option>
            <option value="GMT - 07:00">(GMT - 07:00) Arizona</option>
            <option value="GMT - 06:00">(GMT - 06:00) Gaudalajara,Maxico,City,Monttrey</option>
            <option value="GMT - 06:00">(GMT - 06:00) Central America</option>
            <option value="GMT - 06:00">(GMT - 06:00) Saskatchewan</option>
            <option value="GMT - 06:00">(GMT - 06:00) Central Time(US & Canada)</option>
            <option value="GMT - 05:00">(GMT - 05:00) Indiana(East)</option>
            <option value="GMT - 05:00">(GMT - 05:00) Estern Time(US & Canada),Indiana</option>
            <option value="GMT - 05:00">(GMT - 05:00) Bogota,Lima,Quito</option>
            <option value="GMT - 04:00">(GMT - 04:00) Atalantic Time(Canada)</option>
            <option value="GMT - 04:00">(GMT - 04:00) Caracas,La Paz</option>
            <option value="GMT - 04:00">(GMT - 04:00) Santiago</option>
            <option value="GMT - 03:30">(GMT - 03:30) NewFoundLand</option>
            <option value="GMT - 03:00">(GMT - 03:00) Buenos Aires,Georgetown</option>
            <option value="GMT - 03:00">(GMT - 03:00) Greenland</option>
            <option value="GMT - 03:00">(GMT - 03:00) Brasilla</option>
            <option value="GMT - 02:00">(GMT - 02:00) Mid-Atlantic</option>
            <option value="GMT - 01:00">(GMT - 01:00) Cape Verde Is.</option>
            <option value="GMT - 01:00">(GMT - 01:00) Azores</option>
            <option value="GMT">(GMT) Greanwich Mean Time :Dublin,Edinburgh,Lisbon,London</option>
            <option value="GMT">(GMT) Casablanca,Monrovia</option>
            <option value="GMT + 01:00">(GMT + 01:00) BelGrade,Bratislava,Budapest,Ljubljana,Pregue</option>
            <option value="GMT + 01:00">(GMT + 01:00) West Cetral Africa</option>
            <option value="GMT + 01:00">(GMT + 01:00) Brussels,Copenhagen,Madrid,Paris</option>
            <option value="GMT + 01:00">(GMT + 01:00) Sarajevo,Skopje,Warsaw,Zagbreb</option>
            <option value="GMT + 01:00">(GMT + 01:00) Amsterdam,Berlin,Bern,Rome,Stolkhome,Vienna</option>
            <option value="GMT + 02:00">(GMT + 02:00) Harare,Pretroria</option>
            <option value="GMT + 02:00">(GMT + 02:00) Athens,Beirut,Istanbul,Minsk</option>
            <option value="GMT + 02:00">(GMT + 02:00) Bucharest</option>
            <option value="GMT + 02:00">(GMT + 02:00) Cairo</option>
            <option value="GMT + 02:00">(GMT + 02:00) Helsinki,Kyiv,Riga,,Sofia,Tallin,Vilnius</option>
            <option value="GMT + 02:00">(GMT + 02:00) Jerusalem</option>
            <option value="GMT + 03:00">(GMT + 03:00) Bhagdad</option>
            <option value="GMT + 03:00">(GMT + 03:00) Kuwait,Riyadh</option>
            <option value="GMT + 03:00">(GMT + 03:00) Moskow,St. Petersburg,Volgograd</option>
            <option value="GMT + 03:00">(GMT + 03:00) Nairobi</option>
            <option value="GMT + 03:30">(GMT + 03:30) Tehran</option>
            <option value="GMT + 04:00">(GMT + 04:00) Baku,Tbilisi,Yerevan</option>
            <option value="GMT + 04:00">(GMT + 04:00) Abu Dhabi, Muskat</option>
            <option value="GMT + 04:30">(GMT + 04:30) Kabul</option>
            <option value="GMT + 05:00">(GMT + 05:00) Islamabad, Karanchi, Tashkent</option>
            <option value="GMT + 05:00">(GMT + 05:00) Ekaterinburg</option>
            <option value="GMT + 05:30">(GMT + 05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
            <option value="GMT + 05:45">(GMT + 05:45) Kathmandu</option>
            <option value="GMT + 06:00">(GMT + 06:00) Astana, Dhaka</option>
            <option value="GMT + 06:00">(GMT + 06:00) Almaty, Novosibirsk</option>
            <option value="GMT + 06:00">(GMT + 06:00) Sri Jayaverdenepura</option>
            <option value="GMT + 06:30">(GMT + 06:30) Rangoon</option>
            <option value="GMT + 07:00">(GMT + 07:00) Krasnoyarsk</option>
            <option value="GMT + 07:00">(GMT + 07:00) Bangkok, Hanoi, Jakarta</option>
            <option value="GMT + 08:00">(GMT + 08:00) Irkutsk, Ulaan Battar</option>
            <option value="GMT + 08:00">(GMT + 08:00) Kuala Lumpur, Singapore</option>
            <option value="GMT + 08:00">(GMT + 08:00) Perth</option>
            <option value="GMT + 08:00">(GMT + 08:00) Taipei</option>
            <option value="GMT + 08:00">(GMT + 08:00) Beijing, Chongging, Hong Kong, Urumqi</option>
            <option value="GMT + 09:00">(GMT + 09:00) Yakutsk</option>
            <option value="GMT + 09:00">(GMT + 09:00) Seoul</option>
            <option value="GMT + 09:00">(GMT + 09:00) Osaka, Sapporo, Tokyo</option>
            <option value="GMT + 09:30">(GMT + 09:30) Darwin</option>
            <option value="GMT + 09:30">(GMT + 09:30) Adelaide</option>
            <option value="GMT + 10:00">(GMT + 10:00) vladivostok</option>
            <option value="GMT + 10:00">(GMT + 10:00) Hobart</option>
            <option value="GMT + 10:00">(GMT + 10:00) Gaum, Port Moresby</option>
            <option value="GMT + 10:00">(GMT + 10:00) Canberra, Melbourne, Sydney</option>
            <option value="GMT + 10:00">(GMT + 10:00) Brisbane</option>
            <option value="GMT + 11:00">(GMT + 11:00) Magadan, Soloman Is., New Caledonia</option>
            <option value="GMT + 12:00">(GMT + 12:00) Auckland, Welington</option>
            <option value="GMT + 12:00">(GMT + 12:00) Fiji, Kamchatka, Marshall Is.</option>
            <option value="GMT + 13:00">(GMT + 13:00) Nuku&#039;alofa</option>
          </select>			</span>

            <div class="extras" id="extras">
              <hr class="thin_line">
              <div class="clearfix">
                <label for="phone">What is your best contact phone number?</label>
                <input  id="phone" name="contact" type="text" required="required" style="height: 35px !important;width:220px !important ">
              </div>
            </div>
          </div>
        </div>

<div id="payment_options" class="book_it_section">
    <h1>5. Payment options for		    
    <select id="payment_country" name="payment_country">
        <option  value=AF>Afghanistan</option><option  value=AL>Albania</option><option  value=DZ>Algeria</option><option  value=AS>American Samoa</option><option  value=AD>Andorra</option><option  value=AO>Angola</option><option  value=AI>Anguilla</option><option  value=AQ>Antarctica</option><option  value=AG>Antigua and Barbuda</option><option  value=AR>Argentina</option><option  value=AM>Armenia</option><option  value=AW>Aruba</option><option  value=AU>Australia</option><option  value=AT>Austria</option><option  value=AZ>Azerbaijan</option><option  value=BS>Bahamas</option><option  value=BH>Bahrain</option><option  value=BD>Bangladesh</option><option  value=BB>Barbados</option><option  value=BY>Belarus</option><option  value=BE>Belgium</option><option  value=BZ>Belize</option><option  value=BM>Bermuda</option><option  value=BT>Bhutan</option><option  value=BO>Bolivia</option><option  value=BA>Bosnia</option><option  value=BW>Botswana</option><option  value=BV>Bouvet Island</option><option  value=BR>Brazil</option><option  value=IO>British Indian Ocean Territory</option><option  value=BN>Brunei Darussalam</option><option  value=BG>Bulgaria</option><option  value=BI>Burundi</option><option  value=KH>Cambodia</option><option  value=CM>Cameroon</option><option  value=CA>Canada</option><option  value=CV>Cape Verde</option><option  value=KY>Cayman Islands</option><option  value=CF>Central African Republic</option><option  value=TD>Chad</option><option  value=CL>Chile</option><option  value=CN>China</option><option  value=CX>Christmas Island</option><option  value=CC>Cocos (Keeling) Islands</option><option  value=CO>Colombia</option><option  value=KM>Comoros</option><option  value=CG>Congo</option><option  value=CK>Cook Islands</option><option  value=CR>Costa Rica</option><option  value=HR>Croatia</option><option  value=CY>Cyprus</option><option  value=CZ>Czech Republic</option><option  value=DK>Denmark</option><option  value=DJ>Djibouti</option><option  value=DM>Dominica</option><option  value=DO>Dominican Republic</option><option  value=EC>Ecuador</option><option  value=EG>Egypt</option><option  value=SV>El Salvador</option><option  value=GQ>Equatorial Guinea</option><option  value=ER>Eritrea</option><option  value=EE>Estonia</option><option  value=ET>Ethiopia</option><option  value=FK>Falkland Islands (Malvinas)</option><option  value=FO>Faroe Islands</option><option  value=FJ>Fiji</option><option  value=FI>Finland</option><option  value=FR>France</option><option  value=GF>French Guiana</option><option  value=PF>French Polynesia</option><option  value=TF>French Southern Territories</option><option  value=GA>Gabon</option><option  value=GM>Gambia</option><option  value=GE>Georgia</option><option  value=DE>Germany</option><option  value=GI>Gibraltar</option><option  value=GR>Greece</option><option  value=GL>Greenland</option><option  value=GD>Grenada</option><option  value=GP>Guadeloupe</option><option  value=GU>Guam</option><option  value=GT>Guatemala</option><option  value=GN>Guinea</option><option  value=GW>Guinea-bissau</option><option  value=GY>Guyana</option><option  value=HT>Haiti</option><option  value=HM>Heard and Mc Donald Islands</option><option  value=HN>Honduras</option><option  value=HK>Hong Kong</option><option  value=HU>Hungary</option><option  value=IS>Iceland</option><option  value=IN>India</option><option  value=ID>Indonesia</option><option  value=IQ>Iraq</option><option  value=IE>Ireland</option><option  value=IL>Israel</option><option  value=IT>Italy</option><option  value=JM>Jamaica</option><option  value=JP>Japan</option><option  value=JO>Jordan</option><option  value=KZ>Kazakhstan</option><option  value=KE>Kenya</option><option  value=KI>Kiribati</option><option  value=XK>Kosovo</option><option  value=KW>Kuwait</option><option  value=KG>Kyrgyzstan</option><option  value=LA>Laos</option><option  value=LV>Latvia</option><option  value=LB>Lebanon</option><option  value=LS>Lesotho</option><option  value=LR>Liberia</option><option  value=LY>Libya</option><option  value=LI>Liechtenstein</option><option  value=LT>Lithuania</option><option  value=LU>Luxembourg</option><option  value=MO>Macau</option><option  value=MK>Macedonia</option><option  value=MG>Madagascar</option><option  value=MW>Malawi</option><option  value=MY>Malaysia</option><option  value=MV>Maldives</option><option  value=ML>Mali</option><option  value=MT>Malta</option><option  value=MH>Marshall Islands</option><option  value=MQ>Martinique</option><option  value=MR>Mauritania</option><option  value=MU>Mauritius</option><option  value=YT>Mayotte</option><option  value=MX>Mexico</option><option  value=FM>Micronesia</option><option  value=MD>Moldova</option><option  value=MC>Monaco</option><option  value=MN>Mongolia</option><option  value=ME>Montenegro</option><option  value=MS>Montserrat</option><option  value=MA>Morocco</option><option  value=MZ>Mozambique</option><option  value=NA>Namibia</option><option  value=NR>Nauru</option><option  value=NP>Nepal</option><option  value=NL>Netherlands</option><option  value=AN>Netherlands Antilles</option><option  value=NC>New Caledonia</option><option  value=NZ>New Zealand</option><option  value=NI>Nicaragua</option><option  value=NE>Niger</option><option  value=NU>Niue</option><option  value=NF>Norfolk Island</option><option  value=MP>Northern Mariana Islands</option><option  value=NO>Norway</option><option  value=OM>Oman</option><option  value=PK>Pakistan</option><option  value=PW>Palau</option><option  value=PA>Panama</option><option  value=PG>Papua New Guinea</option><option  value=PY>Paraguay</option><option  value=PE>Peru</option><option  value=PH>Philippines</option><option  value=PN>Pitcairn</option><option  value=PL>Poland</option><option  value=PT>Portugal</option><option  value=PR>Puerto Rico</option><option  value=QA>Qatar</option><option  value=RE>Reunion</option><option  value=RO>Romania</option><option  value=RU>Russian Federation</option><option  value=RW>Rwanda</option><option  value=BL>Saint Barthélemy</option><option  value=KN>Saint Kitts and Nevis</option><option  value=LC>Saint Lucia</option><option  value=VC>Saint Vincent and The Grenadines</option><option  value=WS>Samoa</option><option  value=SM>San Marino</option><option  value=ST>Sao Tome and Principe</option><option  value=SA>Saudi Arabia</option><option  value=RS>Serbia</option><option  value=SC>Seychelles</option><option  value=SG>Singapore</option><option  value=SK>Slovakia</option><option  value=SI>Slovenia</option><option  value=SB>Solomon Islands</option><option  value=ZA>South Africa</option><option  value=KR>South Korea</option><option  value=ES>Spain</option><option  value=LK>Sri Lanka</option><option  value=SH>St. Helena</option><option  value=PM>St. Pierre and Miquelon</option><option  value=SR>Suriname</option><option  value=SJ>Svalbard and Jan Mayen Islands</option><option  value=SZ>Swaziland</option><option  value=SE>Sweden</option><option  value=CH>Switzerland</option><option  value=TW>Taiwan</option><option  value=TJ>Tajikistan</option><option  value=TZ>Tanzania</option><option  value=TH>Thailand</option><option  value=TK>Tokelau</option><option  value=TO>Tonga</option><option  value=TT>Trinidad and Tobago</option><option  value=TN>Tunisia</option><option  value=TR>Turkey</option><option  value=TM>Turkmenistan</option><option  value=TC>Turks and Caicos Islands</option><option  value=TV>Tuvalu</option><option  value=UG>Uganda</option><option  value=UA>Ukraine</option><option  value=AE>United Arab Emirates</option><option  value=GB>United Kingdom</option><option  value=US>United States</option><option  value=UY>Uruguay</option><option  value=UZ>Uzbekistan</option><option  value=VU>Vanuatu</option><option  value=VA>Vatican City State</option><option  value=VE>Venezuela</option><option  value=VN>Vietnam</option><option  value=VG>Virgin Islands (British)</option><option  value=VI>Virgin Islands (U.S.)</option><option  value=WF>Wallis and Futuna Islands</option><option  value=EH>Western Sahara</option><option  value=YE>Yemen</option><option  value=ZM>Zambia</option><option  value=ZW>Zimbabwe</option> 
    </select>
    </h1><br>
            <div id="security_seals">
                 <img alt="PayPal Verified" height="100" id="paypal_seal" src="<?php echo base_url(); ?>assets/img/paypal_seal.png" width="100" border="0">
            </div>
    <div>
        <div>
            <div class="currency_alert">
            <?php if(!empty($details->security_deposit) && !empty($details->cleaning_fee)){ ?>
                    This payment transacts in <?php echo $currency; ?>. Your total charge is <?php echo number_format($final_amounnt+$secu_deposit+$cleaning_fee,2).' '.$currency;  ?>.      
            <?php }elseif(empty($details->security_deposit) && !empty($details->cleaning_fee)){ ?>
                    This payment transacts in <?php echo $currency; ?>. Your total charge is <?php echo number_format($final_amounnt+$cleaning_fee,2).' '.$currency;  ?>.      
            <?php }elseif(!empty($details->security_deposit) && empty($details->cleaning_fee)){ ?>
                    This payment transacts in <?php echo $currency; ?>. Your total charge is <?php echo number_format($final_amounnt+$secu_deposit,2).' '.$currency;  ?>.      
            <?php }else{ ?>
                    This payment transacts in <?php echo $currency; ?>. Your total charge is <?php echo number_format($final_amounnt,2).' '.$currency;  ?>.      
            <?php } ?>
            </div>
            <p>
            <span style="font-weight:bold;">Instructions:</span>
            <br />When you click on "Pay With Paypal" . you will be redirected to PayPal to complete payment.        	  <span style="font-weight:bold;">You must complete the process or the transaction will not occur.</span>
            </p>
        </div>
    </div>
</div>

		<div class="book_it_section" style="padding-bottom:10px;">
		  <p id="book_it_fine_print" style="width:592px; overflow:hidden;">
		    <input type="checkbox" value="1" id="agreeclick" style="float:left;">
			<label style="font-size:16px;">
				 &nbsp;I agree to the cancellation policy, house rules, 
				<a href="javascript:void(0);" target="_blank">guest refund policy terms</a>, 
				and 
				<a href="javascript:void(0);" target="_blank">terms of service</a>.
			</label>
		  </p>
		  <input type="submit" id="agreebutton" class="btn btn-large" style="margin:10px 0 0 10px;" disabled="disabled" value="Pay With Paypal">
		</div>
<?php echo form_close(); ?>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function(){ 
	$('#agreeclick').click(function(){  
			if($('#agreeclick').prop('checked'))
			{
				$('#agreebutton').removeAttr('disabled');
			}
			else 
			{
				$('#agreebutton').attr('disabled','disabled');
			}
	});
});

</script>



     		</div>
		</div>
	</div>
</div>
<p>&nbsp;</p>