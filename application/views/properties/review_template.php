<?php $review = get_pr_review($pr_id);    ?>
<?php if(!empty($review)) : ?>
<?php foreach($review as $view): ?>
    <li>
        <div class="media">
            <div class="pull-left text-center">
                <a href="javascript:;">              
                    <?php if(!empty($view->image)): ?>
                        <img style="border-radius:5px;" src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $view->image; ?>" width="70" height="70" >
                    <?php else: ?>    
                        <img style="border-radius:5px;" src="<?php echo base_url(); ?>assets/img/default_user_300x300.png" width="70" height="70" >
                    <?php endif; ?>
                </a>          
                <div class="name">
                   <?php echo $view->first_name; ?>
                </div>
            </div>
            <div class="media-body">
                <p><?php echo $view->review; ?></p>
                <div class="text-muted date">
                    <?php echo get_time_ago($view->created); ?>
                </div>
            </div>
        </div>
    </li>
<hr>
<?php endforeach; ?>
<?php endif; ?>
