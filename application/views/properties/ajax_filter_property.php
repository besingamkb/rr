            <?php if ($properties): ?> 
            <?php foreach ($properties as $row) : ?>
            <li class="img-item" >
              <a href="<?php echo base_url(); ?>properties/details/<?php echo $row->id; ?>">
                <div class="img-div-img" align="center">
                  <img src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->featured_image ?>">
               </div>
                <?php if(!empty($row->image)): ?>
                  <img class="pro_img" src="<?php echo base_url() ?>assets/uploads/profile_image/<?php echo $row->image ?>">
                <?php else: ?>
                  <img class="pro_img" src="<?php echo base_url()?>assets/bnb_html/img/user.png">
                <?php endif; ?>
                <div class="pro_details">
                     <div class="descript">
                        <span id="pro_title">
                            <?php echo substr($row->title,0,10) ?>
                        </span>
                         <br> 
                        <span class="other_pro_details">
                         <?php echo get_room_types($row->room_type) ?>
                         --
                            <?php echo substr($row->unit_street_address,0,10) ?>
                        </span>
                        <br> 
                        <br> 
                        <span class="other_pro_details">
                            <?php echo substr($row->description,0,10) ?>
                        </span> 
                    </div>
                    <div id="pro_price">

                      <?php  $currency = $this->session->userdata('currency'); ?>                        
                        <span style="font-size:25px;margin-left:10px" >
                        <?php               
                            echo convertCurrency($row->application_fee, $currency, $row->currency_type); 
                        ?>
                        </span>
                        <span >&nbsp; <b> <?php echo $currency; ?> </b></span> <br>


                        <!-- <span style="float:left">USD</span> 
                        <span style="font-size:25px;float:left;margin-left:10px" >
                        <?php echo $row->application_fee ?>$
                        </span><br> -->
                        <span style="margin-left:25px">
                          per night
                        </span>
                    </div>
                    
                  </div>   
                </a>
            </li>
            <hr>
            <?php endforeach; ?>
            <?php endif; ?>
