<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/calendar/css/calendar.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/calendar/css/custom_2.css" />
<script src="<?php echo base_url() ?>assets/calendar/js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/calendar/js/jquery.calendario.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/calendar/js/data.js"></script>
 -->



<script>
$(document).ready(function(){  
   $("#loadmore").click(function(){ 
      
      var offset = $(".faraz").length;
      $.ajax({
               url: "<?php echo base_url(); ?>properties/ajax_load_more_groups/"+offset,
               success: function(data)
               {
                 $("#zakir_faraz_zakir").append(data);
               }
      });
      
         var total_rows = $("#num_of_groups").val();
         if(offset+2 >= total_rows)
         {
         $("#loadmore").hide();
         }
   });
});


</script>



<style>
a{ text-decoration: none !important;
}
.heading{
	font-weight:lighter;
	margin:0px 15px !important;
   height: 20% !important;

}
#main_content{
  box-shadow: 1px 1px 10px  #858587;
  border-radius:10px 10px 15px 15px !important;
  border: 0.1px solid #E1E1E1;
  margin-bottom:3%;
  
}
</style>

    <div class="container">
      <div class="row" >
        <div class="span12">
          <span style="float:left">


        <div class="row">
            <div class="" >
                <div class="span10 content" style="margin-top:3%;margin-left:10%;width:83%;border-radius:8px 8px 0px 0px;background-color:">                  
                  <ul class="nav nav-tabs"  style="margin-bottom:0px;border-radius:8px 8px 0px 0px;height:40px;padding-left:17px !important;padding-top:10px !important;padding-bottom:10px !important;border-bottom:0.1px solid #C0C0C0" >
                      <li style="list-style-type:none">
                        <b class="heading" style="color:#777777;font-size:120%">GROUPS</b>
                      </li>
                  </ul>

                  <!-- Div contain Side bar and Main content Starts -->
              <div style="width:100%;margin-top:3%;margin-bottom:3%;">    
                  <!-- Search div starts -->
               <div class="span6 pull left" id="main_content">
                  
                  <ul class="nav nav-tabs" style="padding-bottom:-10px !important;background-color:#E1E1E1" >
                        <?php echo form_open(current_url()); ?>
                     <div class="input-append" style="padding-left:5%">
                        <input class="span2 filter-input" style="height:30px" placeholder="Search Group" name="search_group" id="appendedInputButton" type="text">
                        <button class="btn" id="src_cty" type="button" onclick="submit()">Search</button>
                     </div>
                       <?php echo form_close(); ?>
                  </ul>
                    <ul style="list-style-type:none;min-height:200px" id="zakir_faraz_zakir">
                        <?php if(!empty($groups)): ?>
                        <?php foreach($groups as $groups):?>
                          <li class="faraz">
                            <div style="float:left;">
                               <img class="img_img_img" src="<?php echo base_url() ?>assets/uploads/banner/<?php echo $groups->banner ?>" style="border-radius:5px;width:90px;height:80px">   
                             </div>
                             <div style="float:left;margin-left:5%">
                               <a href="<?php echo base_url() ?>properties/group_properties/<?php echo $groups->id ?>"><?php echo $groups->group_name ?></a>
                                <br><?php echo word_limiter($groups->group_description,4) ?>
                             </div>
                             <div style="float:right;margin-right:12%">
                               <a href="<?php echo base_url() ?>properties/owner_group_members/<?php echo $groups->id ?>"><?php echo get_no_of_members($groups->id) ?> Members</a>&nbsp;
                               <a href="<?php echo base_url() ?>properties/group_properties/<?php echo $groups->id ?>"><?php echo get_no_of_properties_from_group($groups->id) ?> Properties</a>
                             </div>
                          </li><br><br><br><br><hr>
                         <?php endforeach; ?>
                         <?php else: ?>
                         <p align="center">No groups Found</p>
                        <?php endif; ?>
                    </ul> 
                         
                           <input type="hidden" id="num_of_groups" value="<?php echo $num_of_groups ?>">
                           <?php if(!empty($num_of_groups) && ($num_of_groups>3)):?>
                            <li style="list-style-type:none">
                               <a href="javascript:void(0)" id="loadmore"><h3 style="color:#E05586" align="center"> Load More </h3></a>
                           </li>
                           <?php endif; ?>
                        <ul class="nav nav-tabs" style="height:10%;margin-bottom:0px;border-radius: 0 0 15px 15px;background-color:#E1E1E1"></ul>
                  </div> 
                  <!-- Search div Ends -->

                  <!-- Side bar starts -->
                      <div class="span3 pull-left" style="box-shadow: 1px 1px 10px #858587;border-radius:6px; width:32%" >
                       <div style="padding:15px;">
                           <h3 style="font-weight:lighter">
                               About
                           </h3>
                            <p> Groups let you travel with your tribe. 
                                Once you join a group, you can stay with and host 
                                other members all over the world.
                            </p><hr>
                             <div style="padding-bottom:20px">
                               <div class="label pull-left">Total Groups</div>
                               <div class="label pull-right"><?php echo get_number_of_groups() ?></div>
                             </div>
                       </div>
                      </div> 
                 <!-- Side bar End's -->
                    </div>
                  <!-- Div contain Side bar and Main content Ends -->
                  </div>                  
               </div>   
           </div>      
       </div>
   </div>  
</div>  

<style type="text/css">
/*#bookings input{
  height:30px;
}*/


 
  .sidebar{
    /*float: right;*/
  }

   .content{
    /*margin: 0;*/
   }

  /* .container{
    margin-left: 0px;
   }*/

  .nav-tabs{
    background-color: #f5f5f5;
    padding-top: 20px;
    /*border: 1px solid #DDDDDD*/
   }

   .nav-tabs li{
    margin-left: 1%;    
   }

   .nav-tabs li a{
    border: 1px solid #DDD;
    border-radius: 4px 0;
    color:#333;
   }

   li.active a{
    background-color: #fff !important;
    /*border: none !important;*/
   }

   .content{
      border: 1px solid #DDD;
   }

   .tab-content{
      padding: 10px;
   }

   .R-2{
    margin-top: 2%;
   }

   #review ul li{
    list-style: none
   }


   .media > .pull-left {
    margin-right: 13px;
  }
  .text-center {
      text-align: center;
  }
  .pull-left {
      float: left;
  }

</style>



<style type="text/css">
  .img_img_img
  {
    background-color: #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.2);
    box-shadow: 0 1px 5px #D6D6D6;
    padding: 4px;
  }

.img_img_img:hover {
    box-shadow: 0 1px 5px #D6D6D6 inset;
    cursor: pointer;
}

</style>
