
<style>
body{
    line-height: 17px !important;
}
a{
    text-decoration: none !important;
    cursor:pointer !important;
}
.main{  
        border:1px solid #A8A8A8;
        margin-left:13.2%;
        margin-top: 1.5%;
        width:1000px;
        border-radius:10px 10px 0px 0px;
}
.header{
        background-color: #f2f2f2;
        height: 45px;
        border-radius:8px 8px 0px 0px;
        border-bottom:1px solid #dbdbdb;
        padding: 15px;
        padding-top: 10px !important;
        box-shadow: 2px 74px 5px -2px #CCC;

}
.searchmenu{
        height: 45px;
        /*width: 805px; */
        float:left;
}   
.location{
        width: 250px; 
        float:left;
}   
.location input{
    padding-top: 7px;
        height: 45px;
        width: 250px; 
        border-radius:8px 0px 0px 8px;
        border:1px solid #A8A8A8;
        float:left;
        font-size: 18px;
        font-weight: bold;
}   
.range{
        height: 44px;
        width: 307px; 
        border:1px solid #A8A8A8;
        border-left: none;
        font-size: 12px;
        background-color: #ECECEC;
        float:left;
}   
.range td{
        font-size: 12px;
        padding-left: 13px;
        color:#949596;
}   

.range input {
    background-color: #FFFFFF;
    border: 1px solid #D1D1C9;
    border-radius: 5px;
    box-shadow: 1px 1px 5px #ECECEC;
    cursor: pointer;
    font-size: 12px !important;
    height: 14px;
    margin-top: -12px !important;
    width: 78px;
    height: 24px !important;
}

.range select{
        height: 22px;
        width: 45px; 
        border-radius: 5px;
        cursor: pointer;
        background-color: white;
        border:1px solid #D1D1C9;
        box-shadow:1px 1px 5px #ECECEC;
        font-size: 12px !important;
        padding:3px !important;
}   
.search{
        height: 45px;
        width: 100px; 
        /*border:1px solid #A8A8A8;*/
        border-left: none;
        float:left;
        padding: 0px;
}   
.search button{
        height: 46px;
        width: 100px;
        background-color: #E05284;
        border: none;
        font-size: 20px;
        color: white;
        border-top-right-radius:5px; 
        border-bottom-right-radius:5px; 
}   
.search button:hover{
        background-color: #F16395;
}   
.listphotomap{
        float: right;
        margin-left: 5%;
        /*margin-top: 0.5%;*/
}
.listphotomap button{
        width:60px;
        height:45px;
        font-weight: lighter; 
        margin-left:-3px;
        box-shadow: 1px 1px 3px black;
        border:none;
        color: #B1B1B1;
}

.list1{
        border-radius: 10px 0px 0px 10px;
        background-color: #E05284;
        color: white;
} 
.photo{
        border-radius:0px;
} 
.map{
        border-radius:0px 10px 10px 0px;
}
.secondheader{
        /*width:1000px;*/
        height: 30px;
        background-color: #F2F2F2;
        padding:20px;
        font-weight: bold;
        color: #8C8C7F;
        font-size: 16px;
}
.leftresult{
        width:750px;
        float: left;
        margin:20px;
}

.listings hr{
    margin:0px 0px 15px 0px;
}
.rightfilter hr{
    margin: 8px 0px;
}
.rightfilter select{
    width: 186px;
    margin-top: 3px;
    margin-bottom: 0px;
}
.rightfilter{
        width:230px;
        float: right;
        background-color: #f2f2f2;
        border-bottom:1px  solid #A8A8A8;
        border-left:1px  solid #A8A8A8;
        box-shadow: -6px -3px 8px -8px;
}
    
/*li:hover{
    background-color: #F2F2F2;
}*/
.img-item
{
    width:700px;
    height: 100px;
}
.photo-img-item
{
    width:667px;
    height: 200px;
}
.photo-img-item:hover
{
  box-shadow: 0.5px 0.5px 5px #E05284; 
  border: 1px solid #E05284 !important;
}
.img-div-img
{
    margin-left: 15px !important;
} 
.descript
{
    margin-left: 15px !important;
    width: 250px;
}
.ht_deal
{
    margin-left: 10px;
    width:150px;
    max-height: 115px;
}

@media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {
    .main{
        margin-left: 10.2%;
    }
}



input[type="checkbox"]{
    margin-top: 0px;
}
</style>



<div class='main'>
    <!-- header start -->
    <div class='header'>
        <div class='searchmenu'>
        <form id="srch_form" onsubmit="return false;" >
            <div class='location'>
                <input id="place_location" name="location" value="<?php if(!empty($loca_tion)) echo $loca_tion ?>" type='text' placeholder='City' class="all_search_header_form">
            </div>
            <div class='range'>
                <table>
                    <tr>
                        <td>Check In</td>
                        <td>Check Out</td>
                        <td>Guests</td>
                    </tr>
                    <tr>
                        <td>
                            <input id="dpd11" value="<?php if(!empty($c_in_date)) echo $c_in_date ?>" name="search_check_in" placeholder='mm/dd/yyyy' readonly>
                        </td>
                        <td>
                            <input id="dpd22"  value="<?php if(!empty($c_out_date)) echo $c_out_date ?>" name="search_check_out" placeholder='mm/dd/yyyy' readonly>
                        </td>
                        <td>
                            <select id="guest_count" name='guest_count'>
                            <?php for($i=1;$i<=16;$i++): ?>
                                <option <?php if(isset($g_count)) if($g_count==$i) echo "selected" ?>><?php echo $i ?></option>
                            <?php endfor; ?>
                            </select>
                        </td>
                    </tr>
                </table>    
            </div>
            <div class='search' style="color:white !important">
                <button id="srch_btn">Search</button>
            </div>
        </div>
        <div class='listphotomap'>
            <button id='list_view' style="color:white"  class='list1' > List  </button>
            <button id='photo_view' class='photo'  > Photo </button>
            <button id='map_view'   class='map'   show=""> Map   </button>
        </div>    
    </div> 
    <!-- header end -->


    <!-- second header start -->
    <div class='secondheader' >
        <div style='float:left; padding-top:4px;'>
            Sort By: &nbsp;
        </div>
            <div style='float:left'>
            <select name="sortby" id="sortby" style='width:150px;'>
            <option value="recommended">Recommended</option>
            <option value="l2h">Price: low to high</option>
            <option value="h2l">Price: high to low</option>
            <option value="new">Newest</option>
            </select>
        </div>
    </div>

    <!-- second header end -->

    <div class='row'>
    <!-- left result start -->
    <div class='leftresult'>

        <!-- list view start here -->
        <div class="listings"  style="display:block">                  
            <ul style='list-style-type:none; margin:0px 0px 0px 10px;' id="all_list">
            <?php if ($property): ?> 
            <?php foreach ($property as $row) : ?>
            <li class="img-item" >
                <div style="float:left" class="img-div-img" align="center">
                  <img class='' style='height:80px; width:120px;float:left; border:4px solid white; box-shadow:0px 2px 5px #60605A; ' src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->featured_image ?>">
               </div>
                <?php if(!empty($row->image)): ?>
                  <img style='height:35px; width:40px;float:left;margin-left:8px; border:1px solid white; box-shadow:0px 1px 3px #60605A; ' src="<?php echo base_url() ?>assets/uploads/profile_image/<?php echo $row->image ?>">
                <?php else: ?>
                  <img style='height:35px; width:40px;float:left;margin-left:8px; border:1px solid white; box-shadow:0px 1px 3px #60605A; ' src="<?php echo base_url()?>assets/bnb_html/img/user.png">
                <?php endif; ?>
                <div id="descript_wrapper">
                     <div style="float:left" class="descript">
                        <span style="font-size:20px;font-weight:bold">
                          <a href="<?php echo base_url(); ?>properties/details/<?php echo $row->id; ?>" style="color:">
                            <?php echo substr($row->title,0,10) ?>
                          </a>
                        </span>
                         <br> 
                        <span style="color:#B0B2B4">
                         <?php echo get_room_types($row->room_type) ?>
                         --
                            <?php echo substr($row->unit_street_address,0,10) ?>
                        </span>
                        <br> 
                        <p></p>
                        <!-- <br>  -->
                        <span style="color:#B0B2B4">
                            <?php echo substr($row->description,0,10) ?>
                        </span> 
                    </div>
                    <div style="float:right;">
                        <?php  $currency = $this->session->userdata('currency'); ?>                        
                        <span style="font-size:25px;float:left;margin-left:10px" >
                        <?php 
              /////////////////   convert currency here  /////////////             
                echo convertCurrency($row->application_fee, $currency, $row->currency_type); 
              ?>
                        </span>
                        <span style="float:left">&nbsp; <b> <?php echo $currency; ?> </b></span> 
                        <br>
                        <span style="float:left;margin-left:25px">
                          per night
                        </span>
                    </div>
                    
                  </div>   
            </li>
            <hr>
            <?php endforeach; ?>
            <?php else: ?>
                <li>Nothing found</li>
            <?php endif; ?>
            </ul>
        </div>
        <!-- end of list view -->


        <!-- photo view start here -->
        <div class="listings" id="all_photo_view" style="display:none; margin-left:4%">                  
            <?php if ($property): foreach ($property as $row) { ?>
                <a href="<?php echo base_url(); ?>properties/details/<?php echo $row->id; ?>">
                <div class="span3 photo-img-item" style='margin-bottom:20px; min-height:205px;max-width:200px;border:1px solid #CACACA; margin-left:4%;'>
                    <div style="min-height:150px;" >
                        <img width="190" style='height:141px;border:5px solid white' src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->featured_image ?>">
                    </div>
                    <div style="margin-left:10px" >

                            <?php echo substr($row->title,0,10) ?>
                            <br>
                             <div style="width:128px;color:#8B8B8B;float:left">

                                <?php echo word_limiter(get_room_types($row->room_type),2) ?>
                             </div>
                             <div style="">
                               <span style="font-size:20px;font-weight:bold">
                                 <?php echo convertCurrency($row->application_fee, $currency, $row->currency_type);  ?>
                               </span> <?php echo $currency ?>
                             </div>
                    </div>  
                </div>                  
                </a>
            <?php } ?>
            <?php else: ?>
                <p>Nothing found</p>
            <?php endif; ?>
        </div>
        <?php if(!empty($property)): ?>
        <div class="span8" id="load_div" style="margin-left:4%">
            <a href="javascript:void(0)"  class="btn" id="load_more" >Load more</a>
        </div>
    <?php else: ?>
        <div class="span8" id="load_div" style="margin-left:4%">
           <span id="load_more"></span>
         </div>
    <?php endif; ?>
        <input type="hidden" name="selected_location" id="selected_location" value="<?php echo $location ?>">
        
        <div id="location_div" style="display:none;">
          <?php if ($property): foreach ($property as $row) { ?>
            <span id="<?php echo $row->id ?>"><?php echo $row->unit_street_address.' '.$row->city.' '.$row->state.' '.$row->country; ?></span>
          <?php } endif; ?>
        </div>
        <div class=" listings" id="map" style="display:none; margin-left:2%; width:752px">
        </div>
    </div>
    <!-- left result end -->



    <!-- right filter start -->
    <div class='rightfilter' style="min-height:420px">
            <div style='margin:5px 20px;'>
                <b>Room Type </b>
                <br>
                <select name="room_type" id="room_type">
                    <option value=""> Please select</option>
                    <option value="3">Entire Place </option>
                    <option value="4">Sweet        </option>
                    <option value="2">Private room </option>
                    <option value="1">Shared Room  </option>
                </select>
            </div>                            
            <hr>
            <div  style='margin:5px 20px; width:183px'>
                <b>
                    Price <span id="amount" style="border:0; color:#E05284; font-weight:bold;" ></span>
                </b><br>
                <div id="slider-range"></div>
                <input type="hidden" id="pricerange" name="pricerange">
            </div>                            
            <hr>
            <div  style='margin:5px 20px;'>
                <b>Bedrooms</b> 
                <a href="javascript:void(0)" id="bedtoggle">
                    <span class="pull-right toogle-open"  style="color:#E05284">(+)</span>
                </a><br>
                <div class="controls" id="bedtoggle2" style="display:none">
                    <select name="bedroom" id="beedroom" class="filter-input">
                        <option value="">Please select</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>5+</option>
                    </select>
                </div>                            
            </div>
            <hr>
            <div  style='margin:5px 20px;'>    
                <b>Bathroom</b> 
                <a href="javascript:void(0)" id="bathtoggle">
                    <span class="pull-right toogle-open"  style="color:#E05284">(+)</span>
                </a><br>
                <div class="controls" id="bathtoggle2" style="display:none">
                    <select name="bathrooms" id="bathroom" class="filter-input">
                        <option value="">Please select</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>5+</option>
                    </select>
                </div>                            
            </div>
            <hr>
            <div  style='margin:5px 20px;'>
                <b>Amenities</b> 
                <a href="javascript:void(0)" id="am_toggle">
                    <span class="pull-right toogle-open" style="color:#E05284">(+)</span>
                </a>
                <div class="controls" id="am_toggle2" style="display:none">
                    <?php if ($amenities): ?>
                    <?php  $i=1; ?>
                    <?php foreach ($amenities as $amenity) : ?>                                
                        <input type="checkbox" class="row amenity" value="<?php echo $amenity->id ?>" name="amenities[]"> <?php echo $amenity->name; ?><br>                              
                    <?php $i++; ?>
                    <?php endforeach; ?>  
                    <?php endif; ?>
                </div>                            
            </div>
            <hr>
            <div  style='margin:5px 20px;'>
                <b>Keywords</b><br>
                <input style='height:29px; width:180px' name="keyword" id="appendedInputButton" type="text">
                <button class="btn" id="keyword" type="button">Go!</button>
            </div>
        </form>
    </div>
    <!-- right filter end -->
    </div>
</div>



<div class='container'></div>
<p></p>
<br>

<script>
// map || photo || list paginator
$(document).ready(function(){
    $('.map').click(function(){
        $('.map').css('color','white');
        $('.list1').css('color','#B1B1B1');
        $('.photo').css('color','#B1B1B1');
        $('.map').css('backgroundColor','#E05284');
        $('.map').attr('show','true');
        $('.photo').css('backgroundColor','#F2F2F2');
        $('.list1').css('backgroundColor','#F2F2F2');
    });
     $('.photo').click(function(){
        $('.photo').css('color','white');
        $('.list1').css('color','#B1B1B1');
        $('.map').css('color','#B1B1B1');
        $('.map').attr('show',' ');
        $('.photo').css('backgroundColor','#E05284');
        $('.list1').css('backgroundColor','#F2F2F2');
        $('.map').css('backgroundColor','#F2F2F2');
    });
     $('.list1').click(function(){
        $('.list1').css('color','white');
        $('.map').css('color','#B1B1B1');
        $('.map').attr('show',' ');
        $('.photo').css('color','#B1B1B1');
        $('.list1').css('backgroundColor','#E05284');
        $('.map').css('backgroundColor','#F2F2F2');
        $('.photo').css('backgroundColor','#F2F2F2');
    });
});
</script>

<script>
// Date picker
$(function() {
$( "#dpd11" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#dpd22" ).datepicker( "option", "minDate", selectedDate );
}
});
$( "#dpd22" ).datepicker({
defaultDate: "+1w",
changeMonth: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$( "#dpd11" ).datepicker( "option", "maxDate", selectedDate );
}
});
});
</script>

<script type="text/javascript">
    $('#bedtoggle').click(function(){        
         if($('#bedtoggle span').text() == "(-)")
        $('#bedtoggle span').text('(+)');
      else
        $('#bedtoggle span').text('(-)');
        $('#bedtoggle2').slideToggle();
    });

    $('#bathtoggle').click(function(){        
         if($('#bathtoggle span').text() == "(-)")
        $('#bathtoggle span').text('(+)');
      else
        $('#bathtoggle span').text('(-)');
        $('#bathtoggle2').slideToggle();
    });

    $('#b_toggle').click(function(){        
         if($('#b_toggle span').text() == "(-)")
        $('#b_toggle span').text('(+)');
      else
        $('#b_toggle span').text('(-)');
        $('#b_toggle2').slideToggle();
    });

    $('#am_toggle').click(function(){        
         if($('#am_toggle span').text() == "(-)")
        $('#am_toggle span').text('(+)');
      else
        $('#am_toggle span').text('(-)');
        $('#am_toggle2').slideToggle();
    });

    // $('#tags_toggle').click(function(){        
    //      if($('#tags_toggle span').text() == "(-)")
    //         $('#tags_toggle span').text('(+)');
    //   else
    //     $('#tags_toggle span').text('(-)');
    //     $('#tags_toggle_2').slideToggle();
    // });

    // $('#more_am').click(function(){      
    //  if($('#rem_amenity span').text() == " more -")
    //     $('#rem_amenity span').text(' more +');
    //   else
    //     $('#rem_amenity span').text(' more -');
    //     $('#rem_amenity').slideToggle();
    // })
    
    $('#room_type, #beedroom, #bathroom, #bed, #sortby').on('change', function(){
        $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/',            
            success: function (res) {
                if(res !="")
                {
                  var list = JSON.parse(res);
                  $('#all_list').html(list.list);
                  $('#all_photo_view').html(list.photo_view);
                  $('#location_div').html(list.loctn);
                  $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');
                    $('#map').html("");
                    $('#map').css("");
                    // $('#map').hide();
                    var check_map = $('#map_view').attr('show');
                      $('#load_more').hide();
                      $('#load_more2').hide();
                    if(check_map!='true')
                    {
                        $('#load_more2').show();
                        $('#load_more').hide();
                    }

                }
                else
                {
                  $('#all_list').html('Nothing found');
                  $('#all_photo_view').html('Nothing found');
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }                
                     initialize();
               
             }
          });  
    });


    $('.amenity, #keyword, #src_cty ,#srch_btn').on('click', function()
    {
        $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/',            
            success: function (res) { 
                $('#load_more').data('offset','10');     
                if(res !=""){  
                var list = JSON.parse(res);
                  $('#all_list').html(list.list);                      
                  $('#all_photo_view').html(list.photo_view);
                  $('#location_div').html(list.loctn);                      
                   $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');

                    $('#map').html("");
                    $('#map').css("");
                    // $('#map').hide();
                    // $('#all_list').show();
                    var check_map = $('#map_view').attr('show');
                      $('#load_more').hide();
                      $('#load_more2').hide();
                    if(check_map!='true')
                    {
                        $('#load_more2').show();
                        $('#load_more').hide();
                    }

                }else{
                  $('#all_list').html('Nothing found');
                  $('#all_photo_view').html('Nothing found');
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }              
                     initialize();
               
             }
          });  
    });

    $('#load_more').click(function(){
       var location = $('#place_location').val();
       var check_in = $('input[name=search_check_in]').val();
       var check_out = $('input[name=search_check_out]').val();
       var guest = $('#guest_count').val();
       var offset = $('#all_list .img-item').length;
       $.ajax({
        type:'POST',
        data:{location:location,check_in:check_in,check_out:check_out,guest:guest},
        url:'<?php echo base_url() ?>properties/ajaxloadmore/'+offset,
        success: function (res) {
                if(res !=""){
                   var list = JSON.parse(res); 
                  $('#all_list').append(list.list);
                  $('#all_photo_view').append(list.photo_view);
                  $('#location_div').append(list.loctn);
                }else{                  
                  $('#load_more').hide();
                }  
                     initialize();
             }
       });
    });

    // $('#load_more2').click(function(){
    //    var offset = $('#all_list .img-item').length;
    //    $.ajax({
    //         type: "POST",
    //         data: $('#srch_form').serialize(),
    //         url: '<?php echo base_url() ?>properties/ajaxfilter_property/'+offset,            
    //         success: function (res) {                                     
    //             if(res !=""){                  
    //               $('#all_list').append(res);                   
    //             }else{
    //               $('#all_list').append('No more result to display');                  
    //               $('#load_more').hide();
    //               $('#load_more2').hide();
    //             }              
               
    //          }
    //       });
    // });

    $(document).on("click","#load_more2",function(){        
        var offset = $('#all_list .img-item').length;
       $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/'+offset,            
            success: function (res) {  
                if(res !=""){  
                  var list = JSON.parse(res); 
                  $('#all_list').append(list.list);
                  $('#all_photo_view').append(list.photo_view);
                  $('#location_div').append(list.loctn);
                }else{
                  $('#load_more').hide();
                  $('#load_more2').hide();
                }              
               
                     initialize();
             }
          });
    });


  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: '<?php echo(getMaxprice()->application_fee); ?>',
      values: [ 0, '<?php echo(getMaxprice()->application_fee); ?>' ],
      slide: function( event, ui ) {
        $( "#amount" ).html( "<?php echo $currency ?> " + ui.values[ 0 ] + " - <?php echo $currency ?> " + ui.values[ 1 ] );
        $('#pricerange').val(ui.values[ 0 ] + "-" + ui.values[ 1 ]);
        // alert($('#pricerange').val());     
        
      },

      change: function(){

         $.ajax({
            type: "POST",
            data: $('#srch_form').serialize(),
            url: '<?php echo base_url() ?>properties/ajaxfilter_property/',            
            success: function (res) {                     
                $('#load_more').data('offset','10');     
                if(res !=""){  
                  var list = JSON.parse(res);                
                  $('#all_list').html(list.list);                      
                  $('#all_photo_view').html(list.photo_view);                      
                  $('#location_div').html(list.loctn);                      
                   $('#load_div').html('<a href="javascript:void(0)"  class="btn" id="load_more2" >Load more</a>');

                    $('#map').html("");
                    $('#map').css("");
                    var check_map = $('#map_view').attr('show');
                      $('#load_more').hide();
                      $('#load_more2').hide();
                    if(check_map!='true')
                    {
                        $('#load_more2').show();
                        $('#load_more').hide();
                    }

                }
                else{
                  $('#all_list').html('Nothing found');
                  $('#all_photo_view').html('Nothing found');
                  $('#load_more').hide();
                  $('#load_more2').hide();

                }              
               
                     initialize();
             }
          });  
      }
    });
    $( "#amount" ).html( "<?php echo $currency ?> " + $( "#slider-range" ).slider( "values", 0 ) +
      " - <?php echo $currency ?> " + $( "#slider-range" ).slider( "values", 1 ) );
  });

    function check_val(){
      var val = $('#appendedInputButton').val();
      
      if(val !=""){
        $('#src_cty_from').submit();
        return true;
      }else{
        return false;
        
      }

    }


    var map;
    var geocoder;

    $('#map_view').click(function()
    {
      initialize();
      $('#map').show();
      $('#all_list').hide();
      $('#all_photo_view').hide();
      google.maps.event.trigger(map, 'resize');
      $('#load_more2').hide();
      $('#load_more').hide();


    });

      function initialize() {
        geocoder = new google.maps.Geocoder();
        var mapOptions ={
            center: new google.maps.LatLng(40.7056,-73.978),
            zoom: 2,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
        map = new google.maps.Map(document.getElementById("map"), mapOptions);         
        $( "#location_div span" ).each(function( index ){
            var address = $(this).html();            
            var loc_id = $(this).attr("id");
            var latLang = findlatlang(address, loc_id);
            // var latLang = findlatlang('Vijay Nagar, Indore, Madhya Pradesh', loc_id);
            // console.log(latLang);
          });     
        // google.maps.Size(width:"200", height:"300")
          $("#map").css("height", "400px");
          // google.maps.event.trigger(map, "resize");

      }


       function findlatlang(address, loc_id) {
        // var address ='rajwada indore mp india'; 
        var geocoder = new google.maps.Geocoder();
        var lat ='';
        var lang ='';
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            lat  = results[0].geometry.location.lat();
            lang = results[0].geometry.location.lng();            
            placeMarker(new google.maps.LatLng(lat,  lang), map, loc_id);
          } else {
            result = "Unable to find address: " + status;
          }
        });
      }

      function placeMarker(position, map, loc_id) { 
        var marker = new google.maps.Marker({
          position: position,
          map: map,
          title: 'Vacalio'
        });
        // map.panTo(position);
        var lat = position.lb;
        var lng = position.mb;
        var latlng = new google.maps.LatLng(lat,lng);
        

        // var contentString = "<img src='<?php echo base_url() ?>assets/front/img/logo.png' class='img-responsive'><h1 style=\"font-family: 'FuturaStd-Book';color: #333333;font-size: 17px;margin-top: 25px;display: block;\">Hooray Puree Store</h1>";
        // var contentString = '<div style="height:250px; padding:10px;">';
        // contentString += $("#span_no_"+loc_id).html();
        // contentString += '</div>';
        

        // var infowindow = new google.maps.InfoWindow({
        //   content: contentString
        // });
        // google.maps.event.addListener(marker, 'click', function() {
        //   infowindow.open(map,marker);
        // });
        geocoder.geocode({'latLng': latlng},function(results, status) {
          if (status == google.maps.GeocoderStatus.OK){
            if (results[1]) {
              // infowindow.setContent(results[1].formatted_address);
              // infowindow.open(map,marker);
            } 
          } else {
            if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
              setTimeout(function() { placeMarker(position,map); }, (200));
            }
          }
        });
      }


    $('#photo_view').click(function(){
      $('#map').html("");
      $('#map').css("");
      $('#map').hide();
      $('#all_list').hide();
      $('#all_photo_view').show();
      $('#load_more2').show();
      $('#load_more').show();
    });

    $('#list_view').click(function(){
      $('#map').html("");
      $('#map').css("");
      $('#map').hide();
      $('#all_photo_view').hide();
      $('#all_list').show();
      $('#load_more2').show();
      $('#load_more').show();
    });

    $(function()
        {
            var e;           
            e = $('#place_location')[0];
            return new google.maps.places.Autocomplete(e, {
                types: ['(cities)'],
            });
           
        });

</script>  
// <script type="text/javascript">
//     var he = $('.leftresult').height();
//     var mainh = parseInt(he+40);
//     $('.rightfilter').height(mainh);

//     function contheight(){
//         var he = $('.leftresult').height();
//         var mainh = parseInt(he+40);
//         $('.rightfilter').height(mainh);        
//     }
// </script>