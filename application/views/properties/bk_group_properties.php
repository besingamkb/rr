<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/calendar/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/calendar/css/custom_2.css" />
<script src="<?php echo base_url() ?>assets/calendar/js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/calendar/js/jquery.calendario.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/calendar/js/data.js"></script>

<style>
a{ text-decoration: none !important;
}
.heading{
  font-weight:lighter;
  margin:0px 15px !important;
   height: 20% !important;

}
#main_content{
  box-shadow: 1px 1px 10px  #858587;
  border-radius:10px 10px 15px 15px !important;
  border: 0.1px solid #E1E1E1;
  margin-bottom:3%;
}
.desc
{
   max-width: 180px !important;
}
</style>

    <div class="container">
      <div class="row" >
        <div class="span12">
          <span style="float:left">
            <div style="margin-top:0.7%;visibility:hidden" >Space</div>


        <div class="row">
            <div class="">
                <div class="span10 content" style="margin-left:13%;border-radius:8px 8px 0px 0px;background-color:">                  
                  <ul class="nav nav-tabs"  style="margin-bottom:0px;border-radius:8px 8px 0px 0px;height:40px;padding-left:27px !important;padding-top:10px !important;padding-bottom:10px !important;border-bottom:0.5px solid #C0C0C0" >
                     <li class="heading" style="color:#777777;font-size:120%">GROUPS</li> 
                  </ul>
                  <ul class="nav nav-tabs"  style="margin-bottom:0px;padding-top:10px !important;padding-left:27px !important;padding-bottom:10px !important;border-bottom:0.1px solid #C0C0C0" >
                      <li class="row pull-left" >
                         <img src="<?php echo base_url() ?>assets/uploads/banner/<?php echo $groups->banner ?>"  style="border-radius:5px;width:80px;height:80px">   
                        &nbsp;&nbsp;&nbsp;&nbsp;<strong style="font-size:20px"><?php if(!empty($groups->group_name)) echo $groups->group_name ?></strong>
                       </li>
                  </ul>

                  <!-- Div contain Side bar and Main content Starts -->
              <div style="width:100%;margin-top:3%;margin-bottom:3%;">    
                  <!-- Search div starts -->
               <div class="span6 pull left" id="main_content">
                  <ul class="nav nav-tabs" style="padding-bottom:10px !important;background-color:#E1E1E1" >
                    <?php echo form_open(current_url()); ?>
                      <div class="input-append" style="padding-left:5%">
                           <input class="span2 filter-input" style="height:30px" placeholder="Search Properties" name="search_properties" id="appendedInputButton" type="text">
                            <button class="btn" id="src_cty" type="button" onclick="submit()">Search</button>
                      </div>
                    <?php echo form_close(); ?>
                    <span class="form_error span12"><?php echo form_error('search_properties'); ?></span>
                  </ul>
                    <ul style="list-style-type:none;min-height:200px" id="zakir_faraz_zakir">
                        <?php if(!empty($group_properties)): ?>
                        <?php foreach($group_properties as $group_properties):?>
                          <li class="faraz">
                            <div style="float:left;">
                               <img src="<?php echo base_url() ?>assets/uploads/property/<?php echo $group_properties->featured_image ?>" width="60" style="border-radius:5px;">   
                             </div>
                             <div style="float:left;margin-left:5%">
                               <a href="<?php echo base_url() ?>properties/details/<?php echo $group_properties->property_id ?>"><?php echo $group_properties->title ?></a>
                                <br>
                                <div class="desc">
                                  <?php echo word_limiter($group_properties->description,3) ?>
                                </div>
                             </div>
                             <div style="float:right;margin-right:12%">
                             </div>
                          </li><br><br><br><hr>
                         <?php endforeach; ?>
                         <?php else: ?>
                         <p align="center">No properties Found</p>
                        <?php endif; ?>
                    </ul> 

                           <input type="hidden" id="num_of_group_properties" value="<?php echo $num_of_group_properties ?>">
                           <input type="hidden" id="group_id" value="<?php echo $groups->id ?>">
                           <?php if(!empty($num_of_group_properties) && ($num_of_group_properties>1)):?>
                            <li style="list-style-type:none">
                               <a href="javascript:void(0)" id="loadmore"><h3 style="color:#E05586" align="center"> Load More </h3></a>
                           </li>
                           <?php endif; ?>
                        <ul class="nav nav-tabs" style="height:10%;margin-bottom:0px;border-radius: 0 0 15px 15px;background-color:#E1E1E1"></ul>
                     </div> 

                  <!-- Search div Ends -->

                  <!-- Side bar starts -->
                      <div class="span3 pull-left" style="box-shadow: 1px 1px 10px #858587;border-radius:6px" >
                       <div style="padding:15px;">
                           <h3 style="font-weight:lighter">
                               About
                           </h3>
                            <p> groups let you travel with your tribe. 
                                Once you join a group, you can stay with and host 
                                other members all over the world.
                            </p><hr>
                             <div style="padding-bottom:20px">
                             <a href="<?php echo base_url() ?>properties/all_groups">
                               <div class="label pull-left">Total Groups</div>
                               <div class="label pull-right"><?php echo get_number_of_groups() ?></div>
                             </a>
                             </div>
                       </div>
                      </div> 
                 <!-- Side bar End's -->
                    </div>
                  <!-- Div contain Side bar and Main content Ends -->
                  </div>                  
               </div>   
           </div>      
       </div>
   </div>  
</div>  

<style type="text/css">


  .sidebar{
    /*float: right;*/
  }

   /*.content{
    margin: 0;
   }
*/
  .nav-tabs{
    background-color: #f5f5f5;
    padding-top: 20px;
    /*border: 1px solid #DDDDDD*/
   }

   .nav-tabs li{
    margin-left: 1%;    
   }

   .nav-tabs li a{
    border: 1px solid #DDD;
    border-radius: 4px 0;
    color:#333;
   }

   li.active a{
    background-color: #fff !important;
    /*border: none !important;*/
   }

   .content{
      border: 1px solid #DDD;
   }

   .tab-content{
      padding: 10px;
   }

   .R-2{
    margin-top: 2%;
   }

   #review ul li{
    list-style: none
   }


   .media > .pull-left {
    margin-right: 13px;
  }
  .text-center {
      text-align: center;
  }
  .pull-left {
      float: left;
  }

</style>

<script>
$(document).ready(function(){  
   $("#loadmore").click(function(){ 
      
      var offset = $(".faraz").length;
      var group_id = $('#group_id').val();

      $.ajax({
               url: "<?php echo base_url(); ?>properties/ajax_load_more_group_property/"+offset+'/'+group_id,
               success: function(data)
               {
                 $("#zakir_faraz_zakir").append(data);
               }
      });
         var total_rows = $("#num_of_group_properties").val();
         if(offset+2 >= total_rows)
         {
         $("#loadmore").hide();
         }
   });
});
</script>



  