                    <?php if($properties): ?>
                        <?php foreach($properties as $row): ?>
                            <div class="property span9">
                                <div class="span2 pull-left" style="width:26%">
                                <a href="<?php echo base_url() ?>properties/details/<?php echo $row->prop_id ?>">
                                  <img src="<?php echo base_url(); ?>assets/uploads/property/<?php echo $row->featured_image; ?>" alt="Not Available" class="image-thumbnail">
                                </a>
                                </div>
                                <div class="span5  pull-left">
                                  <div class="prop-data span4"><a href="<?php echo base_url() ?>properties/details/<?php echo $row->prop_id ?>"><?php echo $row->title; ?></a></div>
                                  <div class="prop-data span4"><?php echo substr($row->unit_street_address,0,25); ?></div>
                                  <div class="span6" style="margin-left:0">
                                     <div class="prop-data span2"><?php echo get_property_type($row->property_type_id); ?></div><div class="prop-data span2"><?php if($row->accommodates > 1){ echo $row->accommodates.' Guests'; }else{ echo $row->accommodates.' Guest'; }?></div>
                                  </div>
                                  <div class="span6" style="margin-left:0">
                                     <div class="prop-data span2"><a href="<?php echo base_url() ?>properties/details/<?php echo $row->prop_id ?>/review#revRat_tab"><?php echo get_pr_review_count($row->prop_id); ?></a></div><div class="prop-data span2"><?php echo $row->currency_type; ?> <?php echo number_format($row->application_fee, 2); ?></div>
                                  </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
