<?php 
/**
4 July 2014
*/
 ?>

<?php 
$address = $details->unit_street_address.', '.$details->city.', '.$details->state.', '.$details->country; 
$latlng = get_lat_long(urlencode($address)); 
$lat = $latlng['lat'];
$lng = $latlng['lng'];
?>


<!-- Calendar css/Js -->
<?php
      $calendar = base_url().'assets/event_calendar/';
?>
<link href="<?php echo $calendar; ?>css/calendar.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $calendar; ?>js/jquery.calendar.js"></script>
<script type="text/javascript" src="<?php echo $calendar; ?>js/docs.min.js"></script>


<!-- flex slider -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/flex/flexslider.css" />
<script src="<?php echo base_url() ?>assets/flex/jquery.flexslider.js"></script>


<style type="text/css">

#similar_listing
{
  height:400px;
  overflow: auto;
}
#near_by
{
  height:400px;
  overflow: auto;
}
#tab2 li a:hover
{
  cursor: pointer !important;
}
#myTab li a:hover
{
  cursor: pointer !important;
}
.btn-large
{
  width:250 !important;
  height: 50px !important;
  font-size: 18px !important;
  font-weight: normal !important;
}

#bookings input{
  height:30px;
}
.prp_name{
  text-align: center;
  margin-left: 4%;
}

.prp_name h3{
  /*margin-left: 15%; */
}
.subadmin img{
  height: 200px;
  width: 200px;  
}
.subadmin{
  /*text-align: center;*/
  margin-top: 4%;
  margin-left: 18%;
}

#street img, #map img{
  max-width: none;
}



   .content{
    margin: 0;
   }

  .nav-tabs{
    background-color: #f5f5f5;
    padding-top: 20px;
    /*border: 1px solid #DDDDDD*/
   }

   .nav-tabs li{
    margin-left: 1%;    
   }

   .nav-tabs li a{
    border: 1px solid #DDD;
    border-radius: 4px 0;
    color:#333;
   }

   li.active a{
    background-color: #fff !important;
    /*border: none !important;*/
   }

   .content{
      border: 1px solid #DDD;
   }

   .tab-content{
      padding: 10px;
   }

   .R-2{
    margin-top: 2%;
   }

   #revRat ul li{
    list-style: none
   }


   .media > .pull-left {
    margin-right: 13px;
  }
  .text-center {
      text-align: center;
  }
  .pull-left {
      float: left;
  }


a{ text-decoration: none !important;}
#share_image_css_id img:hover{  }

.description_detail{
  float:left;
  width:70%;
  height:18%;
  margin-right:5%;
  border-right:1px dotted;
}

.neigh{
  width:129px;
  margin-top: -18px !important;
}


#carousel li img{
  height: 70px;
}
#slider ul li div
{
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 2px;
  color: #FFFFFF;
  font-family: courier;
  font-size: 17px;
  margin: -47px 0;
  padding: 13px;
  position: absolute;
  width: 10%;
  display: none;           
}
#slider ul li:hover div
{
  display: block;
}
#slider ul li:hover
{
    cursor: pointer;
}

   #favorites_modal
   {
    margin-top: 2%;
   }
    #favorites_modal input,select{ font-size: 16px;border-radius:5px;}
    .inner_favorites_model
    {
      float:left;
    }
    #favorites_modal .modal-body
    {
      height:300px;
      overflow-y: auto;
      overflow-x: hidden;
    }
    .modal-body .checkbox label:hover
    {
       color:#E05284 !important;
    }

    #sidebar_contact_me{
        padding: 0px !important;
        width: 107% !important;
        padding-bottom: 25px !important;
    }
</style>



<div id="details_box" >
<div class="container">

    <div class="product_box">
        <h2>
            <?php echo $details->title; ?>
            <?php $customer = $this->session->userdata('user_info'); ?>    
            <?php  if(!empty($customer)): ?> 
                <?php $customer_id = $customer['id']; ?>
                <?php $result = is_customer_likes_this_pr($customer_id, $details->id); ?>
                <span class="like_or_unlike">    
                    <?php if($result == 1 ): ?>
                            <img onclick='unlike_property()' src="<?php echo base_url(); ?>assets/img/liked.png" height="35" width="35">
                    <?php else: ?>    
                            <img onclick='like_property()' src="<?php echo base_url(); ?>assets/img/unliked.png" height="35" width="35">
                    <?php endif; ?>    
                </span>
            <?php else: ?>
                <a onclick="alert('Please login first to likes this property.')" href="javascript:;">
                    <img  src="<?php echo base_url(); ?>assets/img/unliked.png" height="35" width="35">
                </a>    
            <?php endif; ?>
        </h2>
        <p id='address'>
            <?php echo $address; ?> 
        </p>
        <?php $rate = ceil(property_rating($details->id)); ?>
        <input name="star" type="radio" class="star" disabled="disabled" <?php if($rate == 1 ) { echo "checked='checked'"; } ?> /> 
        <input name="star" type="radio" class="star" disabled="disabled" <?php if($rate == 2 ) { echo "checked='checked'"; } ?>  /> 
        <input name="star" type="radio" class="star" disabled="disabled" <?php if($rate == 3 ) { echo "checked='checked'"; } ?> /> 
        <input name="star" type="radio" class="star" disabled="disabled" <?php if($rate == 4 ) { echo "checked='checked'"; } ?> />
        <input name="star" type="radio" class="star" disabled="disabled" <?php if($rate == 5 ) { echo "checked='checked'"; } ?> />
        &nbsp;&nbsp;
        ( <?php echo get_pr_review_count($details->id); ?> )
          <p></p>
        <div class="span7 content">                  
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a id="photo_view"><b>Photos</b></a></li>
                <li><a id="map_view"><b>Map</b></a></li>
                <li><a  id="street_view"><b>Street View</b></a></li>
                <li><a  id='event_view'><b>Calendar</b></a></li>
            </ul>                     
            <div class="tab-content">
                <div class="active" id="photo">
                      <?php /**/ ?>
                        <?php $primages = get_property_images($details->id); ?>
                      <div id="slider" class="flexslider">
                        <ul class="slides">
                          <?php if ($primages): foreach($primages as $imgs): ?>
                          <li>
                            <img src="<?php echo base_url() ?>assets/uploads/property/<?php echo $imgs->image_file; ?>" />
                             <?php if(!empty($imgs->description)): ?>
                               <div><?php echo $imgs->description; ?></div>
                             <?php endif; ?>
                          </li>
                          <?php endforeach; endif; ?>

                        </ul>
                      </div>
                      <div id="carousel" class="flexslider">
                        <ul class="slides">
                          <?php if ($primages): foreach($primages as $imgs): ?>
                          <li>
                            <img src="<?php echo base_url() ?>assets/uploads/property/<?php echo $imgs->image_file; ?>" />
                          </li>
                          <?php endforeach; endif; ?>
                        </ul>
                      </div>
                </div>
                <div id="map"></div>
                <div id="street"></div>
                <div id="calView" style="display:none;">
                    <div id="calMonth" class="col-md-12 row">
                        <select name="month" id="selectMonth" style="width:165px !important; margin-left:4%;"class="form-control">
                            <?php 
                                  $start = time();
                                  $a = getdate($start);
                                  $end = mktime(0,0,0,$a['mon'],$a['mday'],$a['year'] + 5 );
                            ?>
                            <?php 
                                $i=1; 
                                while ($start <= $end) { 
                            ?>
                                <option <?php if(date('F Y', $start) == date('F Y')){ echo 'selected="selected"';} ?> value="<?php echo date('j F Y', $start); ?>"> <?php echo date("F Y", $start); ?> </option>
                            <?php
                                  $start = mktime(0,0,0,$a['mon']+$i,$a['mday'],$a['year']);
                                  $i++;
                                  if ($i > 99) {
                                    break;
                                  }
                                } 
                            ?>
                        </select>
                    </div>
                    
                    <div id="calendar" style="height:460px; overflow:hidden;"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
         onload=function()
         {
          <?php if($flag==='review'): ?>
               $('#revRat_tab').trigger('click');
          <?php endif; ?>
         }
        </script>             
                
        
        <div class="span7 content" style="margin-top:20px;">                  
            <ul class="nav nav-tabs" id="tab2">
                <li class="active"><a id="des_tab">Description</a></li>
                <li><a id="ame_tab">Amenities</a></li>
                <li><a id="revRat_tab">Rating / Reviews</a></li>                   
                <li><a id="house_tba">House Rules</a></li>                   
            </ul>                     
            <div class="tab-content">
                <div class="tab-pane active" id="descript">
                    <div class="span3" style='float:left'>
                        <b>The Property</b><br>
                        <p><?php echo $details->description ?></p>
                    </div>
                    <div class="span3" style='float:right; margin-right:3%'>
                      <table class="table  table-striped" style='font-size:14px !important;'>
                          <tbody><tr>
                            <td>Facility title:</td>
                            <td class="value"><?php echo $details->title; ?></td>
                          </tr>
                          <tr>
                            <td>Room Type:</td>
                            <td class="value"><?php echo @get_room_types($details->room_type)?></td>
                          </tr>
                          <tr>
                            <td>Bed Type:</td>
                            <td class="value"><?php echo @get_types_of_bed($details->bed_type)?>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Accommodates:</td>
                            <td class="value"><?php echo @$details->accommodates ?></td>
                          </tr>
                          <tr>
                            <td>Bedrooms:</td>
                            <td class="value"><?php echo @$details->bed ?></td>
                          </tr>
                          <tr>
                            <td>Published:</td>
                            <td class="value"><?php echo date('d/m/Y', strtotime($details->created)); ?></td>
                          </tr>
                          <tr>
                            <td>City:</td>
                            <td class="value"><span><?php echo @$details->city; ?></span></td>
                          </tr>
                          <tr>
                            <td>Country:</td>
                            <td class="value"><span><?php echo @$details->country; ?></span></td>
                          </tr>
                      </tbody></table>
                    </div>
                </div>                      
                        

                <div class="tab-pane" id="amenity">
                    <?php $amenity = get_pr_amenities($details->id); ?> 
                    <?php  foreach ($amenity as $amenity): ?>
                        <div class='span2' >
                          <i title="Has amenity / Allowed" rel="tooltip" class="icon icon-ok icon-pink"></i>
                          <strong>
                            <?php  echo @$amenity->name; ?>
                          </strong>
                        </div>  
                    <?php endforeach; ?>                                
                </div>


<!-- review system created by me.... -->
<link href='<?php echo base_url(); ?>assets/5starRating/jquery.rating.css' type="text/css" rel="stylesheet"/>
<script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>

                <div class="tab-pane" id="revRat">
                    <ul id="display_review">
                    <?php $review = get_pr_review($details->id);    ?>
                    <?php if(!empty($review)) : ?>
                    <?php $i=1; ?>
                    <?php foreach($review as $view): ?>
                        <li>
                            <div class="media">
                                <div class="pull-left text-center">
                                    <a href="javascript:;">              
                                        <?php if(!empty($view->image)): ?>
                                            <img  src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $view->image; ?>" style='width:70px; height:70; border-radius:5px;' >
                                        <?php else: ?>    
                                            <img  src="<?php echo base_url(); ?>assets/img/default_user_300x300.png"  style='width:70px; height:70px; border-radius:5px;' >
                                        <?php endif; ?>
                                    </a>          
                                    <div class="name">
                                       <?php echo $view->first_name; ?>
                                    </div>
                                </div>
                                <div class="media-body">
                                   
                                   <div style="width:100%">
                                      <div style='float:left;width:75%'>
                                          <input name="star<?php echo $i; ?>" type="radio" class="star" disabled="disabled" <?php if($view->rating == 1 ) { echo "checked='checked'"; } ?> /> 
                                          <input name="star<?php echo $i; ?>" type="radio" class="star" disabled="disabled" <?php if($view->rating == 2 ) { echo "checked='checked'"; } ?>  /> 
                                          <input name="star<?php echo $i; ?>" type="radio" class="star" disabled="disabled" <?php if($view->rating == 3 ) { echo "checked='checked'"; } ?> /> 
                                          <input name="star<?php echo $i; ?>" type="radio" class="star" disabled="disabled" <?php if($view->rating == 4 ) { echo "checked='checked'"; } ?> />
                                          <input name="star<?php echo $i; ?>" type="radio" class="star" disabled="disabled" <?php if($view->rating == 5 ) { echo "checked='checked'"; } ?> />
                                      </div>
                                      <div style="float:left;width:25%" >
                                        <?php echo get_time_ago($view->created); ?>
                                      </div>
                                   </div>

                                    <div class="text-muted date" style='float:left;'>
                                        <?php echo $view->review; ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <hr>
                    <?php $i++;  ?>
                    <?php endforeach; ?>
                    <?php else: ?>
                        Review not found.
                    <?php endif; ?>
                    </ul>
                </div>
                <div class="tab-pane" id="house_rules">
                  <?php if(!empty($details->house_rules)): ?>
                    <?php echo $details->house_rules; ?>
                  <?php else: ?>
                    House rules not found.
                  <?php endif; ?>
                </div>
            </div>                  
        </div>   
    </div>
    <!-- left menu -->
    <!-- left menu -->
    <!-- left menu -->
    <!-- left menu -->

    <!-- right bar -->
    <!-- right bar -->
    <!-- right bar -->
    <div id="property_sidebar">
        <div id="share_image_css_id">
         
         <?php $facebook = get_oauth_keys('facebook') ?>
          <?php if(!empty($facebook->button_images)): ?>
          <a href="javascript:void(0)" id="fshare">
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $facebook->button_images ?>" style="height:25px;width:75px">
          <a/>
          <?php endif; ?>

         <?php $twitter = get_oauth_keys('twitter') ?>
          <?php if(!empty($twitter->button_images)): ?>
          <a href="http://twitter.com/home?status=<?php echo $details->title ?> <?php echo current_url(); ?>" title="Share on Twitter" target='_blank'>
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $twitter->button_images ?>" style="width:63px; height:25px; ">
          </a>
        <?php endif; ?>

        
         <?php $google = get_oauth_keys('google') ?>
          <?php if(!empty($google->button_images)): ?>
          <a href="https://plus.google.com/share?url=<?php echo current_url(); ?>" title="share on Google+" target='_blank'>
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $google->button_images ?>" style="width:63px; height:25px; ">
          </a>
         <?php endif; ?>

         <?php $pinterest = get_oauth_keys('pinterest') ?>
          <?php if(!empty($pinterest->button_images)): ?>
          <a href="http://pinterest.com/pin/create/button/?url=<?php echo current_url() ?>&media=<?php echo base_url() ?>assets/uploads/property/<?php echo $details->featured_image; ?>&description=<?php echo $details->description; ?>" count-layout="horizontal" title="Share on Pinterest" target='_blank'>
            <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $pinterest->button_images ?>" style="height:25px;width:63px; border-radius:3px; ">
          </a>
        <?php endif; ?>
    </div>
        
        <!-- booking system -->
        <div id="socialshare_booking" align='center'>
            <?php echo form_open(base_url().'properties/booking' ,array('id'=>'bookings') ); ?>
            <h4>From <span id="basicfees">
              <?php 
              /////////////////   convert currency here  /////////////
              $currency = $this->session->userdata('currency');
              echo @convertCurrency($details->application_fee, $currency,$details->currency_type); 
              ?>
              </span><?php echo ' '.$currency; ?><br></h4>
            <input type="hidden" value="<?php echo $details->id; ?>" name="pr_id">
            <input type="hidden" value="" name="days" id="days">
            <select  onchange="show_basic_charge()" class="" name="allot" id="allot" style="margin-top:10px">
                <option value="4" >Per Night</option>
                <option value="5" >Per Week</option>
                <option value="6" >Per Month</option>
            </select>                    
            <label>Check In</label>                    
            <input  type="text"  id="chkin" name="cin" style="width:218px" onchange="effective_fees()"  class="span3 form-input"  placeholder="mm/dd/yyyy" readonly="readonly">
            <label>Check Out</label>
            <input  type="text"  id="chkout" name="cout" style="width:218px" onchange="effective_fees()"  class="span3 form-input"  placeholder="mm/dd/yyyy" readonly="readonly">
            <label>Guest</label>
            <select  onchange="effective_fees()" id="no_of_guest" style="margin-top:10px">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
            </select>                    
            <label>Sub Total</label>
            <input type="hidden" class="subtotal_val" value="" name="subtotal">
            <h2><span  class="subtotal_html">
              <?php 
              /////////////////   convert currency here  /////////////
              echo @convertCurrency($details->application_fee, $currency,$details->currency_type); 
              ?>
            </span><?php echo ' '.$currency; ?></h2>
            <!-- Coupon work Starts -->
            <div class="input-append" >
                <input name='name' style="width:146px" class="span2" placeholder="Coupon Code" type="text" id="coupon_code">
                <a class="btn" onclick="check_coupon_valid()">Submit</a>
            </div>
            <input type="hidden" id="c_type" name="coupon_type">
            <input type="hidden" id="c_amount" name="coupon_amount">
            <!-- Coupon work Ends -->
            <input type="submit" id="sub_book" disabled="disabled" class="btn btn-large" value="Book It !">
            <?php echo form_close(); ?>
        </div>
<!-- booking script starts -->
<!-- booking script starts -->
<!-- booking script starts -->
<script>
function show_basic_charge()
{
    var per_type    = $('#allot :selected').val();
    var fees      = "<?php echo convertCurrency($details->application_fee, $currency,$details->currency_type); ?>";
    
    // here findout how many fees per day or week or month //
    if(per_type == 4) // per night
    {
        var newfees = fees;
    }
    if(per_type == 5) // per week
    {
        <?php if(!empty($details->weekly)): ?>
        var newfees = <?php echo convertCurrency($details->weekly, $currency,$details->currency_type); ?>
        <?php else: ?>
        var newfees = fees*7;
        <?php endif; ?>
    }
    if(per_type == 6) // per month
    {
        <?php if(!empty($details->monthly)): ?>
        var newfees = <?php echo convertCurrency($details->monthly, $currency,$details->currency_type); ?>
        <?php else: ?>
        var newfees = fees*30;
        <?php endif; ?>
    }

    $('#basicfees').html(Math.round(newfees));

  }


  function effective_fees()
  {
    var pr_id = <?php echo $details->id ?>;
    var check_in   = $('#chkin').val();
    var check_out  = $('#chkout').val();
    var no_of_guest  = $('#no_of_guest').val();


      if(check_out!="" && check_in!="")
        {
           var default_rate = $('.subtotal_html').html();
           $('.subtotal_html').html("<img src='<?php echo base_url() ?>assets/img/loading-blue.gif'>");

            $.ajax({
                    type: 'POST',
                    data:{check_in:check_in, check_out:check_out, pr_id:pr_id,no_of_guest:no_of_guest},
                    url: '<?php echo base_url(); ?>properties/get_effective_booking_rate',
                    success: function(res)
                    {
                      var response = parseInt(res);
                        if(isNaN(response))
                        { 
                            $('#sub_book').remove();
                            $('#bookings').append('<input type="submit" id="sub_book" class="btn btn-large" disabled="disabled" value="Book It !" >');
                            $('.subtotal_html').html(default_rate);
                            alert(res);
                        }
                        else
                        {
                            $('.subtotal_html').html(response);
                            $('.subtotal_val').val(response);
                            $('#sub_book').remove();
                            $('#bookings').append('<input type="submit" id="sub_book" class="btn btn-large"  value="Book It !" onclick="return check_login_first()" >');
                        }
                    }
              });
        }
  }  
</script>


        <!-- contact me section -->
        <div id="sidebar_contact_me">
          <?php if(!empty($details->image)): ?>
              <img style='width:323px; height:200px; border-radius:0px !important;' src="<?php echo base_url(); ?>assets/uploads/profile_image/<?php echo $details->image; ?>"  >
          <?php else: ?>    
              <img style='width:323px; height:200px; border-radius:0px !important;' src="<?php echo base_url(); ?>assets/img/default_user_300x300.png" >
          <?php endif; ?>
            <!-- contact me button to call jquery modal -->
            <?php //echo $details->user_id; ?>
            <?php $u = get_user_info($details->user_id); ?>
            <?php if ($u): ?>
              <h3 align='center'><a href="<?php echo base_url() ?>properties/view_front_profile/<?php echo $u->id ?>"><?php echo ucfirst($u->first_name)." ".ucfirst(substr($u->last_name,0,1)).". "; ?></a></h3>
            <?php endif ?>
              <?php if($this->session->userdata('user_info')):  ?>
              <button data-target="#contact_me_modal" data-toggle="modal" class="btn btn-large" >Contact me</button>
              <?php else: ?>
              <button  onclick="alert('Please login first to contact me.')" class="btn btn-large" >Contact me</button>
              <?php endif; ?>
            <!-- contact me button to call jquery modal -->
        </div>
        <!-- contact me section -->
        <!-- Similar listing and Near by Work Starts  -->
        <div id="sidebar_similar_listing">                  
                <ul class="nav nav-tabs" id="tab3">
                  <li class="active"><a  class='neigh' id="similar" href="#similar_listing">Similar Listing</a></li>
                  <li><a  class='neigh' href="#near_by" id="near">Near by</a></li>
                </ul>                     
                <?php $data = array(
                                   'room_type'=>@$details->room_type,
                                   'bed_type'=>@$details->bed_type,
                                   'bedrooms'=>@$details->bed,
                                   'accommodates'=>@$details->accommodates,
                                   'country'=>@$details->room_type,
                                   'bathrooms'=>@$details->bath,
                                   ); 
                 ?>
                <?php  $similar_listing = get_similar_listing($data,$details->id); ?>
                <div id="similar_listing" >
                  <ul style="list-style-type:none">
                    <?php if(!empty($similar_listing)): ?>
                    <?php foreach($similar_listing as $value): ?>
                    <?php $info = @get_similar_listing_user_info($value->pr_id); ?>
                     <li>
                        <div style="float:left">
                          <img src="<?php echo base_url()?>assets/uploads/property/<?php echo @$value->featured_image ?>"  style="border-radius:3px;width:80px;height:80px">
                        </div>
                        <div style="float:left;margin-left:5%">
                          
                           <a href="<?php echo base_url()?>properties/details/<?php echo $value->pr_id ?>" style="background-color:#ffffff !important"><?php echo @convertCurrency($value->application_fee,$currency,$value->currency_type) ?> <?php echo $currency ?> / night</a><br>
                            <span style="color:#B2B2B2"><?php echo @substr($value->title,0,15) ?></span><br>
                        </div><br><br><br><br><br>
                     </li>
                    <?php endforeach; ?>
                     <?php else: ?>
                      <p align="center"> No Similar listings Found.</p>
                     <?php endif; ?>
                 </ul> 
               </div>
               <div id="near_by" style="display:none;">
                  <ul style="list-style-type:none">
                   <?php $result = get_distance_near_by($details->id); ?>
                   <?php if(!empty($result)): ?>
                   <?php $i=0;foreach($result as $row): ?>
                       <?php $i++; $pr_info = get_property_detail($row['id']); ?>                       
                     <li >
                        <div style="float:left">
                          <img src="<?php echo base_url()?>assets/uploads/property/<?php echo @$pr_info->featured_image ?>"  style="border-radius:3px;width:80px;height:80px">
                        </div>
                        <div style="float:left;margin-left:5%;">
                           <a href="<?php echo base_url()?>properties/details/<?php echo $pr_info->pr_id ?>" style="background-color:#ffffff !important">
                             <?php if($row['distance']>30): ?>
                             <?php echo number_format($row['distance'],2) ?> km away
                             <?php else: ?>
                              In the Same City
                             <?php endif; ?>
                           </a>
                           <br>
                             <span style="color:#B2B2B2"><?php echo substr($pr_info->title,0,15) ?></span><br>
                             <span style="color:#B2B2B2"><?php echo get_room_types($pr_info->room_type) ?></span><br>
                          </div><br><br><br><br><br>
                     </li>
                    <?php endforeach; ?>
                     <?php else: ?>
                      <p align="center"> No Near by Found.</p>
                     <?php endif; ?>
                  </ul> 
              </div>
            </div>
        </div>      
    </div>
    <!-- right bar -->
    <!-- right bar -->
    <!-- right bar -->
</div>
</div>
<!-- main division -->


<br>
<p class='container'>&nbsp;</p>
    <script type="text/javascript">
    function check_coupon_valid()
    {
      var coupon_code = $('#coupon_code').val();
      $.ajax({
             type:"post",
             data:{coupon_code:coupon_code},
             url:"<?php echo base_url() ?>properties/check_coupon_valid",
             success:function(res)
             {
                var code = JSON.parse(res);
                alert(code.stat);
                if(code.r_type!="")
                {
                  $('#c_type').val(code.r_type);
                  $('#c_amount').val(code.amount);
                }
                else
                {
                  $('#c_type').val("");
                  $('#c_amount').val("");
                }
             }    
         });
    }

    </script>
<!-- booking system -->


<script>
$(document).ready(function(){

  $('#photo_view').click(function(){
    $("#photo").show();
    $("#map").hide();
    $("#street").hide();
    $("#calView").hide();
    
  });  

  $('#map_view').click(function(){
    $("#photo").hide();
    $("#map").show();
    $("#street").hide();
    $("#calView").hide();
    initialize();

    });

  $('#street_view').click(function(){
    $("#photo").hide();
    $("#map").hide();
    $("#street").show();
    $("#calView").hide();
    streetviewmap();
  });  

  $('#event_view').click(function(){
    $("#photo").hide();
    $("#map").hide();
    $("#street").hide();
    $("#calView").show();
    $(".fc-button-today").trigger('click');
    $(".fc-button-today").hide();
  });  

});
</script>

<!-- Calendar Starts -->
<script type="text/javascript">

$(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  var h = {
            left:'title',
            center:'',
            right:'today'
          };
  $("#calendar").fullCalendar({
    header: h,
    selectable: false,
    events: {
          url: "<?php echo base_url().'properties/get_events/' ?>",
          type: 'POST',
          data: function() { // a function that returns an object
          var date = $("#calendar").fullCalendar('getDate');
          var month_int = date.getMonth();
          var year = date.getFullYear();
          var property_id = "<?php echo $details->id ?>";
          // console.log("Month : " + month_int + " Year : " + year);
              return {
                  month: month_int,
                  year: year,
                  property_id : property_id 
              };
          },
          error: function () {
              alert('There is an error in loading calendar data...!');
          }
      },
      editable: false,
      draggable: false,
      droppable: false
  });

  $('#selectMonth').on('change', function (e) {
      var gotoDate = new Date($(this).val());
      $('#calendar').fullCalendar('gotoDate', gotoDate.getFullYear(), gotoDate.getMonth(), gotoDate.getDate());
  });

});
</script>
<style type="text/css">
  .calender-upper-box
  {
   box-shadow:0.4px 0.4px 2px #B9B9B9 inset;
   padding:10px; 
   }
   .calendar
   {
    margin-top: 3%;
    margin-bottom: 3%;

   }
   .fc-event-inner
   {
    height: 50px !important;
   }
   .fc-event-title
   {
    font-size: 16px;
    font-weight: lighter;
   }
</style>

<script>
$(document).ready(function(){
  $('#near').click(function(){
    $("#similar_listing").hide();
    $("#near_by").show();
  });  
});

$(document).ready(function(){
  $('#similar').click(function(){
    $("#similar_listing").show();
    $("#near_by").hide();
  });  
});


</script>


<script>
    $('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    })

     $('#tab2 a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    })
     $('#tab3 a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    })
</script>


<script type="text/javascript">
    var map;
    var position = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $lng; ?>);
    var geocoder = new google.maps.Geocoder();
    var mapOptions ={
        center: position,            
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    function initialize() 
    {
        map = new google.maps.Map(document.getElementById("map"), mapOptions);                         
        $("#map").css("height", "400px");        

        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: '<?php echo $details->title; ?>'
        });
    }

    function  streetviewmap()
    {
        $("#street").css("height", "400px");
        var panoramaOptions = {
            position: position,
            pov: {
                heading: 34,
                pitch: 10
            }
        };
        var panorama = new  google.maps.StreetViewPanorama(document.getElementById("street"), panoramaOptions);
        map.setStreetView(panorama);
        google.maps.event.trigger(panorama, 'resize');
    }
</script>


<script>
$(function() {
  $( "#contact_me_checkin" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#contact_me_checkout" ).datepicker( "option", "minDate", selectedDate );
      }
    });

    $( "#contact_me_checkout" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,      
      onClose: function( selectedDate ) {
        $( "#contact_me_checkin" ).datepicker( "option", "maxDate", selectedDate );
      }
  });

    $( "#chkin" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      minDate: 0
    });

    $( "#chkout" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      minDate: 0
    });
});
</script>
<script type="text/javascript">
function contact_me()
{
    var pr_id     = "<?php echo $details->id; ?>";
    var owner_id  = "<?php echo $details->user_id; ?>";
    var pr_title  = "<?php echo $details->title; ?>";
    var check_in  = $('#contact_me_checkin').val();
    var check_out = $('#contact_me_checkout').val();
    var guest     = $('#contact_me_guest').val();
    var message   = $('#contact_me_message').val();
    var contact   = "<?php echo $details->phone; ?>";
    if(check_in == "")
    {
       $("#contact_me_in_error").html("Select Check In.");
    }
    else if(check_out == "")
    {
       $("#contact_me_out_error").html("Select Check Out.");
    }
    else if(message == "")
    {
       $("#contact_me_message_error").html("Please enter some message.");
    }
    else
    {
        $.ajax({
                type:"POST",
                data:{check_in:check_in, check_out:check_out, guest:guest, message:message, pr_title:pr_title, pr_id:pr_id, owner_id:owner_id, contact:contact},
                url:"<?php echo base_url(); ?>properties/ajax_send_contact_me_message",
                success:function(res)
                {
                    $('#contact_me_ajax_msg').html('<div style="width:300px; box-shadow:2px 2px 20px #DCDCDC; background-color:#D0E5CC; padding:10px; border-radius:5px;"><b>'+res+'</b></div>');
                }
        });
    }
    setTimeout(function(){ 
                        $("#contact_me_in_error").html("");    
                        $("#contact_me_out_error").html("");    
                        $("#contact_me_message_error").html("");    
                        $('#contact_me_ajax_msg').html(""); 
                         },10000 
            );                    
}    
</script>


<!--  favorites modal -->

<div class="modal show" id="favorites_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none; width:522px; ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="border:1px solid #B3ABA9; padding:3px 8px; border-radius:3px;"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="color:#666666;">Add Into Favorites category</h3>
            </div>
          <form id="favorites_cat_model">
            <div class="modal-body">
            <div class="span4 checkbox">
              <?php $favo_catgeory  = get_no_of_favoritesCategory('result') ?>
                <?php if(!empty($favo_catgeory)):  ?>
                     <?php $i=0;foreach ($favo_catgeory as $row):?>
                      <?php $i++; ?>
                        <label>
                          <input type="checkbox"  name="favorites_category[]" value="<?php echo $row->id ?>">
                          <?php echo $row->name; ?>
                        </label>
                      <?php if($i%3==0): ?>
                        </div>
                        <div class="span4 checkbox">
                      <?php endif; ?>
                     <?php endforeach; ?>
                <?php else: ?>
                   You have not added any categories yet.<br>
                <?php endif; ?>
              </div>
            </div>
            <div class="modal-footer" style="padding-left:8%;">
            <div id="fav_msg"></div>
                <?php if(!empty($favo_catgeory)):  ?>
            <a class="btn btn-info" id="add_fav_btn" onclick="add_into_favorites_category()" href="javascript:void(0)">Add </a>
          </form>
          <?php else: ?>
            <a class="btn btn-info" href="<?php echo base_url() ?>user/addFavoritesCategory">Add a Category</a>
              <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!--  favorites modal -->

<script type="text/javascript">
function like_property()
{ 
      $('#add_fav_btn').show();
    $('#favorites_modal').modal();
}

function add_into_favorites_category()
{
    var pr_id       = <?php echo $details->id; ?>;
        var checked=false;
        var elements = document.getElementsByName("favorites_category[]");
        for(var i=0; i < elements.length; i++)
        {
          if(elements[i].checked)
          {
            checked = true;
          }
        }
        if (!checked)
        {
          alert('Please Select any Category');
          return false;
        }


      $.ajax
      ({
          type:'POST',
          url:'<?php echo base_url(); ?>properties/add_into_favorites_category/'+pr_id,
          data:$('#favorites_cat_model').serialize(),
          success: function(res)
          {
            if(res=="error")
            {
              alert("Please Login First To like This property");
              return false;
            }
              $('#fav_msg').html(' Favorites Has been Added Successfully');
              $('#add_fav_btn').hide();
              setTimeout(function()
              {
                  $('#favorites_modal').modal('hide');
                  $('#fav_msg').html("");
              },2000)

              $('span.like_or_unlike').html("<img onclick='unlike_property()' src='<?php echo base_url(); ?>assets/img/liked.png' width='35' height='35'>");
          }
      });
}

function unlike_property()
{
    var customer_id = <?php echo $customer_id; ?>;
    var pr_id       = <?php echo $details->id; ?>;
        $.ajax({ 
            type:'POST',
            url:'<?php echo base_url(); ?>properties/unlike_property',
            data:{customer_id:customer_id, pr_id:pr_id },
            success: function(res)
            {
                $('span.like_or_unlike').html("<img onclick='like_property()' src='<?php echo base_url(); ?>assets/img/unliked.png' width='35' height='35'>");
            }
        });
}
</script> 
<!-- favorite like and unlike -->

<!-- DISPLAY SHARE & review and star ***** rating system -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '518108701641293',
      status     : true,
      xfbml      : true
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

$('#fshare').click(function(){
  FB.ui(
  {
   method: 'feed',
   name: 'Vacalio',
   caption: '<?php echo $details->title; ?>',
   description: ('<?php echo word_limiter($details->description,5); ?>'),
   link: '<?php echo current_url(); ?>',
   picture: '<?php echo base_url(); ?>assets/uploads/property/<?php echo $details->featured_image; ?>'
  },
  function(response) {
    if (response && response.post_id) {
      alert('Post was published.');
    } else {
      alert('Post was not published.');
    }
  }
);
});
</script>
<!-- DISPLAY SHARE & review and star ***** rating system -->

<script> 
function send_review()
{
    var cus_ses = $('#login_check').val();
    var review  = $('#customer_review').val();
    var pr_id   = $('#pr_id').val();
    var user_id = $('#user_id').val();
    if(cus_ses == "")
    {
        alert('Please login first to review.');
        return;
    }
    else if(review == "")
    {
        alert('your review is empty, please enter review.');
        return;
    }
    else
    {
        $.ajax({
                type : 'POST',
                 url : '<?php echo base_url(); ?>properties/send_review',
                data : {pr_id:pr_id, user_id:user_id, review:review},
             success : function(res)
             {
                $('#display_review').empty();
                $('#display_review').html(res);
                $('#customer_review').val("");
             }
        });
    }
}                   
</script>                    
<!-- review system created by Faraz.... -->



<script>
$(document).ready(function(){
  $('#des_tab').click(function(){
    $("#descript").show();
    $("#amenity").hide();
    $("#revRat").hide();
    $("#house_rules").hide();
  });  
});

$(document).ready(function(){
  $('#ame_tab').click(function(){
    $("#descript").hide();
    $("#amenity").show();
    $("#revRat").hide();
    $("#house_rules").hide();
    
  });  
});

$(document).ready(function(){
  $('#revRat_tab').click(function(){
    $("#descript").hide();
    $("#amenity").hide();
    $("#revRat").show();
    $("#house_rules").hide();
  });  
});

$(document).ready(function(){
  $('#house_tba').click(function(){
    $("#descript").hide();
    $("#amenity").hide();
    $("#revRat").hide();
    $("#house_rules").show();
  });  
});

</script>

<!--  propertty rating  system -->   
<script>
$(function(){ $('#rating_div :radio.star').rating(); });

function property_rating()
{
    var cus_ses     = $('#login_check').val();
    var pr_rate     = $('#rating_div input[name=radio_rating]:radio:checked').val();
    var pr_id       = $('#pr_id').val();
    if(cus_ses == "")
    {
        alert('Please login first to rating this property.');
        return;
    }
    else
    {
        $.ajax({
                type : 'POST',
                 url : '<?php echo base_url(); ?>properties/property_rating',
                data : { pr_rate:pr_rate, pr_id:pr_id},
             success : function(res)
             {
               alert('You have been rated this property successfully.');
             }
        });
    }    
}
</script>
<script src='<?php echo base_url(); ?>assets/5starRating/jquery.rating.js' type="text/javascript" language="javascript"></script>
<!--  propertty rating  system -->

<!--  contact me modal -->
<style>
   #contact_me_modal{margin-top: 6%;}
    #contact_me_modal input,select{ font-size: 16px; height:35px; border-radius:5px;}
</style>
<div class="modal show" id="contact_me_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none; width:522px; ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="border:1px solid #B3ABA9; padding:3px 8px; border-radius:3px;"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="color:#666666;">Send Message to Property owner</h3>
            </div>
            <div id="contact_me_ajax_msg" style=" padding:10px !important;" align="center">
            </div>
            <div class="modal-body" style="width:464px; padding-left:8%; padding-top:0px !important;">
                <div style="float:left">
                    <label>Check In</label>
                    <input  style="width:120px;"  type="text" readonly="readonly" placeholder="mm/dd/yyyy" id="contact_me_checkin" >
                    <br>
                    <span style="color:#E05284;" id="contact_me_in_error"></span>
                </div>
                <div style="float:left; margin-left:20px;">
                    <label>Check Out</label>
                    <input  style="width:120px;"  type="text" readonly="readonly" placeholder="mm/dd/yyyy" id="contact_me_checkout" >
                    <br>
                    <span style="color:#E05284;" id="contact_me_out_error" ></span>
                </div>
                <div style="float:left; margin-left:20px;">
                    <label>Guests</label>
                    <select style="width:60px;" id="contact_me_guest">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16+</option>
                    </select>
                </div>
                <br><br>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div style="width:100%">
                    <span style="color:#A7A9AC; font-size:12px;">Tell Subadmin what you like about their place, what matters most about your accommodations, or ask them a question.</span>
                    <br>
                    <textarea  style="width:95%" height="auto" id="contact_me_message"></textarea>
                    <span style="color:#E05284;" id="contact_me_message_error" ></span>
                    <br>
                </div>
            </div>
            <div class="modal-footer" style="padding-left:8%;">
                <button onclick="contact_me()" class="pull-left btn  btn-primary" type="submit">Send now</button>
            </div>
        </div>
    </div>
</div>
<!--  contact me modal -->


<!-- flex slider -->  
<script type="text/javascript">


  $(document).ready(function(){
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemHeight: 100,
    itemMargin: 15,
    asNavFor: '#slider'
  });
   
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});
</script>

<style type="text/css">

  .flexslider{
    margin-bottom: 0px;
  }

  .flex-control-thumbs li{
    width: 80px !important;
    padding: 2px;
  }

  .flex-control-thumbs img{
    width: 100% !important;
  }

 /* .flex-next{
    display: none !important;
  }*/


  .flex-direction-nav a.flex-prev {
     background: url('<?php echo base_url() ?>assets/flex/bg_direction_nav.png') no-repeat 0 0 !important;
  }  


  .flex-direction-nav a.flex-next {
     background: url('<?php echo base_url() ?>assets/flex/bg_direction_nav.png') no-repeat -30 0 !important;
   }


  .flex-direction-nav a {
      width: 30px;
      height: 30px;
      margin: -20px 0 0;
      display: block;
      /*background: url('<?php echo base_url() ?>assets/flex/bg_direction_nav.png') no-repeat 0 0 !important;*/
      position: absolute;
      top: 50%;
      cursor: pointer;
      text-indent: -9999px;
      opacity: 0;
      -webkit-transition: all .3s ease;
  }

  .flexslider:hover .flex-next {
      opacity: 0.8;
      right: 5px;
   }

   #carousel ul li{
    margin-left: 15px;
   }

   .flex-direction-nav .flex-next {
    background-position: 100% 0;
    right: -36px;
    text-align: none;
    }

  .flex-direction-nav a.flex-next::before{
      content: "" !important;
    }


    .flexslider{
      overflow: hidden;
    }


    #slider li img{
      width: 100%;
      height: 350px;
    }


</style>

<!-- flex slider -->
