            <?php if ($properties): foreach ($properties as $row) { ?>
                <div id="pr_<?php echo $row->id ?>">
                <a href="<?php echo base_url(); ?>properties/details/<?php echo $row->id; ?>">
                <div class="span3 photo-img-item" style='margin-bottom:20px; min-height:205px;max-width:200px;border:1px solid #CACACA; margin-left:4%;'>
                    <div style="min-height:150px;" >
                        <img width="190" style='height:141px;border:5px solid white' src="<?php echo base_url() ?>assets/uploads/property/<?php echo $row->featured_image ?>">
                    </div>
                    <div style="margin-left:10px" >

                            <?php echo substr($row->title,0,10) ?>
                            <br>
                             <div style="width:128px;color:#8B8B8B;float:left">

                                <?php echo word_limiter(get_room_types($row->room_type),2) ?>
                             </div>
                      <?php  $currency = $this->session->userdata('currency'); ?>                        
                             <div style="">
                               <span style="font-size:20px;font-weight:bold">
                                    <?php echo convertCurrency($row->application_fee, $currency, $row->currency_type);  ?>
                                    </span> 
                                     <?php echo $currency; ?>
                             </div>
                    </div>  
                </div>                  
                </a>
                </div>
            <?php } ?>
            <?php endif; ?>
