
<style>


a{ text-decoration: none !important;
}
.heading{
  font-weight:lighter;
  margin:0px 15px !important;
   height: 20% !important;

}
#main_content{
  box-shadow: 1px 1px 10px  #858587;
  border-radius:10px 10px 15px 15px !important;
  border: 0.1px solid #E1E1E1;
  margin-bottom:3%;
}
.desc
{
   max-width: 180px !important;
}
</style>

    <div class="container">
      <div class="row" >
        <div class="span12">


        <div class="row">
            <div class="">
                <div class="span10 content" style="margin-left:10%;margin-top:3%;width:83%;border-radius:8px 8px 0px 0px;background-color:">                  
                  <ul class="nav nav-tabs"  style="margin-bottom:0px;border-radius:8px 8px 0px 0px;height:40px;padding-left:7px !important;padding-top:10px !important;padding-bottom:10px !important;border-bottom:1px solid #C0C0C0" >
                     <li class="heading" style="color:#777777;font-size:120%">
                          <b class="heading" style="color:#777777;font-size:120%">GROUP</b>
                     </li> 
                  </ul>
                  <ul class="nav nav-tabs"  style="margin-bottom:0px;important;padding:15px !important; border-bottom:1px solid #C0C0C0" >
                      <li class="span6" >
                         <img src="<?php echo base_url() ?>assets/uploads/banner/<?php echo $groups->banner ?>"  style="border-radius:5px;width:80px;height:80px;margin-left:2%">   
                        &nbsp;&nbsp;&nbsp;&nbsp;<strong style="font-size:20px;color:grey"><?php if(!empty($groups->group_name)) echo $groups->group_name ?></strong>
                             
                       </li>

                       <li class="row pull-right" >
                          <div id="share_image_css_id" style="margin-top:25px; margin-bottom:0px; text-align: right;">
                              <?php if(!empty($groups->group_name)){ $grp_name = $groups->group_name; }else{ $grp_name = 'Group at bnb'; } ?>
                              <?php $facebook = get_oauth_keys('facebook') ?>
                              <?php if(!empty($facebook->button_images)): ?>
                                <a href="javascript:void(0)" id="fshare">
                                  <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $facebook->button_images ?>" style="height:25px;width:80px ">
                                <a/>
                              <?php endif; ?>

                              <?php $twitter = get_oauth_keys('twitter') ?>
                              <?php if(!empty($twitter->button_images)): ?>
                                <a href="http://twitter.com/home?status=<?php echo $grp_name ?> <?php echo current_url(); ?>" title="Share on Twitter" target='_blank'>
                                 <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $twitter->button_images ?>" style="width:63px; height:25px; ">
                                </a>
                              <?php endif; ?>

                              <?php $google = get_oauth_keys('google') ?>
                              <?php if(!empty($google->button_images)): ?>
                                <a href="https://plus.google.com/share?url=<?php echo current_url(); ?>" title="share on Google+" target='_blank'>
                                  <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $google->button_images ?>" style="width:63px; height:25px; ">
                                </a>
                              <?php endif; ?>

                              <?php $pinterest = get_oauth_keys('pinterest') ?>
                              <?php if(!empty($pinterest->button_images)): ?>
                                <a href="http://pinterest.com/pin/create/button/?url=<?php echo current_url() ?>&media=<?php echo base_url() ?>assets/uploads/banner/<?php echo $groups->banner; ?>&description=<?php echo $groups->group_description; ?>" count-layout="horizontal" title="Share on Pinterest" target='_blank'>
                                  <img src="<?php echo base_url(); ?>assets/img/third_party/<?php echo $pinterest->button_images ?>" style="height:25px;width:63px; border-radius:3px; ">
                                </a>
                              <?php endif; ?>                         
                          </div>
                       </li>
                  </ul>

                  <!-- Div contain Side bar and Main content Starts -->
              <div style="width:100%;margin-top:3%;margin-bottom:3%;">    
                  <!-- Search div starts -->
               <div class="span6 pull left" id="main_content">
                   <ul class="nav nav-tabs" style="min-height:50px;background-color:#E1E1E1" >
                    
                        <h4 style="font-weight:lighter;margin-left:4%;color:grey">Group Members</h4>
                    
                    </ul>
                    <ul style="list-style-type:none;min-height:200px" id="zakir_faraz_zakir">
                        <?php if(!empty($members)): ?>
                        <?php foreach($members as $members):?>
                          <li class="faraz">
                            <div style="float:left;">
                             <?php if(!empty($members->image)): ?>
                               <img class="img_img_img" src="<?php echo base_url() ?>assets/uploads/profile_image/<?php echo $members->image ?>" style="border-radius:5px;width:90px;height:80px">   
                            <?php else: ?>
                               <img class="img_img_img" src="<?php echo base_url() ?>assets/bnb_html/img/user.png" style="border-radius:5px;width:90px;height:80px">   
                            <?php endif; ?>
                             </div>
                             <div style="float:left;margin-left:5%">
                               <a href="<?php echo base_url() ?>properties/view_front_profile/<?php echo $members->member_id ?>"><?php echo $members->first_name." ".$members->last_name ?></a>
                                <br>
                                <div class="desc">
                                </div>
                             </div>                            
                             <div style="float:right;margin-right:12%">
                             </div>
                          </li><br><br><br><br><hr>
                         <?php endforeach; ?>
                         <?php else: ?>
                         <p align="center">No Members Found</p>
                        <?php endif; ?>
                    </ul> 

                           <input type="hidden" id="num_of_members" value="<?php echo $num_of_members ?>">
                           <input type="hidden" id="group_id" value="<?php echo $groups->id ?>">
                           <?php if(!empty($num_of_members) && ($num_of_members>2)):?>
                            <li style="list-style-type:none">
                               <a href="javascript:void(0)" id="loadmore"><h3 style="color:#E05586" align="center"> Load More </h3></a>
                           </li>
                           <?php endif; ?>
                        <ul class="nav nav-tabs" style="height:10%;margin-bottom:0px;border-radius: 0 0 15px 15px;background-color:#E1E1E1"></ul>
                     </div> 

                  <!-- Search div Ends -->

                  <!-- Side bar starts -->
                      <div class="span3 pull-left" style="box-shadow: 1px 1px 10px #858587;border-radius:6px" >
                       <div style="padding:15px;">
                           <h3 style="font-weight:lighter">
                               About
                           </h3>
                            <p> groups let you travel with your tribe. 
                                Once you join a group, you can stay with and host 
                                other members all over the world.
                            </p><hr>
                         <div style="padding-bottom:20px">
                             <div class="label pull-left"><a href="<?php echo base_url() ?>properties/owner_group_members/<?php echo $groups->id ?>"  style="color:white !important">Members</a></div>
                             <div class="label pull-right"><?php echo get_no_of_members($groups->id) ?></div><br><br>
                             <div class="label pull-left"><a href="<?php echo base_url() ?>properties/group_properties/<?php echo $groups->id  ?>"  style="color:white !important">Properties</a></div>
                             <div class="label pull-right"><?php echo get_no_of_properties_from_group($groups->id) ?></div>
                         </div>
                       </div>
                      </div> 
                 <!-- Side bar End's -->
                    </div>
                  <!-- Div contain Side bar and Main content Ends -->
                  </div>                  
               </div>   
           </div>      
       </div>
   </div>  
</div>  

<style type="text/css">
#bookings input{
  height:30px;
}
.prp_name{
  text-align: center;
  margin-left: 7%;
}

.prp_name h3{
  /*margin-left: 15%; */
}
.subadmin img{
  height: 200px;
  width: 200px;  
}
.subadmin{
  /*text-align: center;*/
  margin-top: 4%;
  margin-left: 30%;
}
  .bookit{
    border: 1px solid #ddd;
    padding: 10px;
  }

  .sidebar{
    /*float: right;*/
  }

   .content{
    margin: 0;
   }

  .nav-tabs{
    background-color: #f5f5f5;
    padding-top: 20px;
    /*border: 1px solid #DDDDDD*/
   }

   .nav-tabs li{
    margin-left: 1%;    
   }

   .nav-tabs li a{
    border: 1px solid #DDD;
    border-radius: 4px 0;
    color:#333;
   }

   li.active a{
    background-color: #fff !important;
    /*border: none !important;*/
   }

   .content{
      border: 1px solid #DDD;
   }

   .tab-content{
      padding: 10px;
   }

   .R-2{
    margin-top: 2%;
   }

   #review ul li{
    list-style: none
   }


   .media > .pull-left {
    margin-right: 13px;
  }
  .text-center {
      text-align: center;
  }
  .pull-left {
      float: left;
  }

</style>


</script> 
<!-- favorite like and unlike -->

<!-- DISPLAY SHARE & review and star ***** rating system -->
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '518108701641293',
      status     : true,
      xfbml      : true
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

$('#fshare').click(function(){
    FB.ui({
           method: 'feed',
           name: 'Vacalio',
           caption: '<?php echo $groups->group_name; ?>',
           description: ('<?php echo word_limiter($groups->group_description,5); ?>'),
           link: '<?php echo current_url(); ?>',
           picture: '<?php echo base_url(); ?>assets/uploads/banner/<?php echo $groups->banner; ?>'
          },
          function(response) {
            if (response && response.post_id) {
              alert('Post was published.');
            } else {
              alert('Post was not published.');
            }
          }
    );
});
</script>
<!-- DISPLAY SHARE & review and star ***** rating system -->

<script>
$(document).ready(function(){  
   $("#loadmore").click(function(){ 
      
      var offset = $(".faraz").length;
      var group_id = $('#group_id').val();

      $.ajax({
               url: "<?php echo base_url(); ?>properties/ajax_load_more_group_members/"+offset+'/'+group_id,
               success: function(data)
               {
                 $("#zakir_faraz_zakir").append(data);
               }
      });
         var total_rows = $("#num_of_members").val();
         if(offset+2 >= total_rows)
         {
         $("#loadmore").hide();
         }
   });
});
</script>



<style type="text/css">
  .img_img_img
  {
     background-color: #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.2);
    box-shadow: 0 1px 5px #D6D6D6;
    padding: 4px;
  }



.img_img_img:hover {
    box-shadow: 0 1px 5px #D6D6D6 inset;
    cursor: pointer;
}

</style>




  