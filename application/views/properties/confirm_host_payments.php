

<div class="span12" align="center" style="min-height:480px; padding-top:10%;"> 
  
  <img src="<?php echo base_url() ?>assets/img/loading-blue.gif">
  <?php  $paypal_info = get_payment_gateways_info('paypal'); ?>
<form  style="display:none " action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
    
    <input type="hidden" name="cmd" value="_cart">
    <input type="hidden" name="upload" value="1">
    <input type="hidden" name="rm" value="2">
    <input type="hidden" name="cbt" value="Return to vacalio">
    <input type="hidden" name="invoice" value="<?php echo rand('111111111', '999999999') ?>">
    <input type="hidden" name="business" value="<?php echo $paypal_info->email ?>">
    <input type="hidden" name="return" value="<?php echo base_url(); ?>properties/success_host_payments/<?php echo $booking_id ?>" />
    <input type="hidden" name="cancel_return" value="<?php echo base_url(); ?>properties/cancel_host_payments/<?php echo $booking_id ?>" />
    <input type="hidden" name="email" value="<?php echo $user_info->user_email; ?>">
    <input type="hidden" name="first_name" value="<?php echo $user_info->first_name." ".$user_info->last_name; ?>">
    <input type="hidden" name="item_name_1" value="Book vacalio property" />
    <input type="hidden" name="amount_1" value="<?php echo round($amount); ?>"/>
    <input type="hidden" name="quantity_1" value="1"/>
    <input type="hidden" name="currency_code" value="<?php echo $currency ?>">

    <button id="triggerclick" class="btn btn-large" type="submit">Pay </button>

</form>

<script>
onload = function()
{
    $('#triggerclick').trigger('click');
}
</script>
</div>
<div class="container"></div>