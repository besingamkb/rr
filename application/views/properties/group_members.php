
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/calendar/css/calendar.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/calendar/css/custom_2.css" />
<script src="<?php echo base_url() ?>assets/calendar/js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/calendar/js/jquery.calendario.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/calendar/js/data.js"></script>



<style>
a{ text-decoration: none !important;}
#heading{
  font-weight:lighter;
  margin:0px 15px !important;
  font-size:200%;

}
</style>

    <div class="container">
      <div class="row" >
        <div class="span12">
          <span style="float:left">
            <h2 style="font-weight:lighter">Group</h2>


        <div class="row">
            <div class="">
                <div class="span8 content">                  
                  <ul class="nav nav-tabs" >
                    <?php $group_info = get_group_info($group_id) ?>
                    <img src="<?php echo base_url() ?>assets/uploads/banner/<?php echo $group_info->banner ?>" width="120" style="border-radius:5px;margin-left:3%;margin-bottom:3%"> 
                    <b id="heading"><?php echo ucfirst($group_info->group_name) ?></b> 
                  </ul>
          <!-- Group members Starts -->
                      <div class="span2 pull-left" style="margin-left:25%;margin-top:5%;margin-bottom:7%">
                        <img src="<?php echo base_url() ?>assets/img/third_party/member_customer.png" width="150" height="141" >
                         <br>
                        <div align="center" class="members">
                          <a href="<?php echo  base_url() ?>properties/customer_group_members/<?php echo $group_id ?>">
                            Customer : 
                             (<span style="color:Red"><?php echo get_no_of_customer_from_group($group_id) ?><span>)
                          </a>
                        </div>
                      </div>
                      <div class="span2 pull-left" style="margin-top:7%;margin-left:5%">
                         <img src="<?php echo base_url() ?>assets/img/third_party/member_owner.png" width="170" height="126">
                          <br>
                        <div align="center" class="members" style="margin-top:5.5%">
                          <a href="<?php echo base_url() ?>properties/owner_group_members/<?php echo $group_id ?>">
                           Property Owner : 
                           (<span style="color:Red"><?php echo get_no_of_owner_from_group($group_id) ?><span>)
                          </a>
                        </div>
                     </div>
         <!-- Group members  Ends-->
                </div>  
                <div class="span3 sidebar">
                  <div class="span3 bookit">
                    <h3 style="font-weight:lighter">
                        Share
                    </h3>
                     <hr>
                     <div class="pull-left">
                          <img src="<?php echo base_url() ?>assets/img/third_party/facebook.png"><br>
                     </div>
                     <div class="pull-right">
                          <img src="<?php echo base_url() ?>assets/img/third_party/twitter.png" width="40" height="40"><br>
                     </div><br><br><br><br>
                     <div class="pull-left">
                          <img src="<?php echo base_url() ?>assets/img/third_party/pinterest.png" width="40" height="20"><br>
                     </div>
                     <div class="pull-right">
                          <img src="<?php echo base_url() ?>assets/img/third_party/googleplus.png" width="70" height="30"><br>
                     </div>
                  </div>
          <div class="span3 subadmin">
          </div>
                </div>                  
                <div class="span3 sidebar">
                  <div class="span3 bookit">
                    <h3 style="font-weight:lighter">
                        About
                    </h3>
                    <p> Vacalio groups let you travel with your tribe. 
                        Once you join a group, you can stay with and host 
                        other members all over the world.
                    </p>
                    <hr>
                     <div class="label pull-left"><a href="#"  style="color:white !important">Members</a></div>
                     <div class="label pull-right"><?php echo get_no_of_members($group_id) ?></div><br><br>
                     <div class="label pull-left"><a href="<?php echo base_url() ?>properties/group_properties/<?php echo $group_id  ?>"  style="color:white !important">Properties</a></div>
                     <div class="label pull-right"><?php echo get_no_of_properties_from_group($group_id) ?></div>
                  </div>
          <div class="span3 subadmin">
          </div>
                </div>                  
             </div>                  
          </div>   
       </div>      
    </div>
 </div>  

<style type="text/css">
#bookings input{
  height:30px;
}
.prp_name{
  text-align: center;
  margin-left: 7%;
}

.prp_name h3{
  /*margin-left: 15%; */
}
.subadmin img{
  height: 200px;
  width: 200px;  
}
.subadmin{
  /*text-align: center;*/
  margin-top: 4%;
  margin-left: 30%;
}
  .bookit{
    border: 1px solid #ddd;
    padding: 10px;
  }

  .sidebar{
    /*float: right;*/
  }

   .content{
    margin: 0;
   }

  .nav-tabs{
    background-color: #f5f5f5;
    padding-top: 20px;
    /*border: 1px solid #DDDDDD*/
   }

   .nav-tabs li{
    margin-left: 1%;    
   }

   .nav-tabs li a{
    border: 1px solid #DDD;
    border-radius: 4px 0;
    color:#333;
   }

   li.active a{
    background-color: #fff !important;
    /*border: none !important;*/
   }

   .content{
      border: 1px solid #DDD;
   }

   .tab-content{
      padding: 10px;
   }

   .R-2{
    margin-top: 2%;
   }

   #review ul li{
    list-style: none
   }


   .media > .pull-left {
    margin-right: 13px;
  }
  .text-center {
      text-align: center;
  }
  .pull-left {
      float: left;
  }



</style>



<style type="text/css">
  .fc-content div{
    display: none;
  }

  .fc-calendar .fc-row > div.fc-content {
  background: #fff4c3;
}


.fc-calendar .fc-row > div.fc-content {
  background: transparent;
  box-shadow: inset 0 0 100px rgba(255,255,255,0.1);
}

.fc-calendar .fc-row > div.fc-content {
  background: transparent;
  box-shadow: inset 0 0 100px rgba(255,255,255,0.1);
}


.fc-calendar .fc-row > div.fc-content:after { 
  content: ''; 
  display: block;
  position: absolute;
  top: 0; 
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0.2;
  background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(255, 255, 255, 0.15)), to(rgba(0, 0, 0, 0.25))), -webkit-gradient(linear, left top, right bottom, color-stop(0, rgba(255, 255, 255, 0)), color-stop(0.5, rgba(255, 255, 255, .15)), color-stop(0.501, rgba(255, 255, 255, 0)), color-stop(1, rgba(255, 255, 255, 0)));
  background: -moz-linear-gradient(top, rgba(255, 255, 255, 0.15), rgba(0, 0, 0, 0.25)), -moz-linear-gradient(left top, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0));
  background: -o-linear-gradient(top, rgba(255, 255, 255, 0.15), rgba(0, 0, 0, 0.25)), -o-llinear-gradient(left top, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0));
  background: -ms-linear-gradient(top, rgba(255, 255, 255, 0.15), rgba(0, 0, 0, 0.25)), -ms-linear-gradient(left top, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0));
  background: linear-gradient(top, rgba(255, 255, 255, 0.15), rgba(0, 0, 0, 0.25)), linear-gradient(left top, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0) 50%, rgba(255, 255, 255, 0));
}

.fc-calendar .fc-row > div.fc-content {
  background: rgba(255, 255, 255, 0.2);
}

.fc-calendar .fc-row > div.fc-content:after { 
  display: none;
}

.fc-calendar .fc-row > div.fc-content > span.fc-date {
  color: #fff;
  text-shadow: 0 1px 1px rgba(0,0,0,0.1);
}

.fc-calendar .fc-row > div.fc-content {
  background: #ef4f69;
  box-shadow: inset 0 -1px 1px rgba(0,0,0,0.1);
}

.fc-calendar .fc-row > div{
  background: none repeat scroll 0 0 #819F2A; 
}

.fc-calendar .fc-row > div > span.fc-date{
  color: #FFF;
}
</style>

<!-- for calendar -->