     </div>
   </div><!-- dashboard-container -->
  </div><!-- container-fluid -->
<footer style="margin-top:82px">
       <p class="copyright">&copy; Bnbclone <?php echo date('Y') ?></p>
    </footer>
    <script type="text/javascript">
          $('#send_mail').on('click',function() {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/; 
            var email = document.getElementById('user_email').value;
            var subject = document.getElementById('subject').value;
            var description = document.getElementById('description').value;
            if(email==''){
              alert('Please Enter Email');
              return false;
            }
            else if(reg.test(email) == false){
              alert('Please Enter Email');
              return false;
            }
            else if(subject == ''){
              alert('Please Enter Subject');
              return false;
            }
            else if(description == ''){
              alert('Please Enter Description');
              return false;
            }
            else{
              $('#modal_body').html('');
              $('#modal_body').html('<img style="margin:0px 250px;" src="<?php echo base_url()?>assets/img/loading-black.gif">');
              $.post( "<?php echo base_url()?>subadmin/send_mail", { 'email':email ,'subject':subject,'description':description }).done(function( data ) {
                    $('#modal_body').html('<p>'+data+'</p>');
                    $('#send_mail').css('display', 'none');
                    setTimeout(function(){
                     location.reload();
                    },1000);
              });
            }
        });
      </script> 
       <script type="text/javascript">
      function booking_changes(){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/; 
        var booking_name = document.getElementById('booking_name').value;
        var email = document.getElementById('email').value;
        var address = document.getElementById('address').value;
        var contact = document.getElementById('contact').value;
        // alert(booking_name+','+email+','+address+','+contact);
        // return false;
        if(booking_name == ''){
          $("#book_name").css('display','block'); 
          $("#book_name").append('<p style="color:red;" > Please enter the Booking name</p>');
            setTimeout(function(){
              $("#book_name").css('display','none'); 
              $("#book_name").html('');
            },3000);
          return false;
        }
        else if(email==''){
         $("#book_email").css('display','block'); 
          $("#book_email").append('<p style="color:red;" > Please enter Email</p>');
            setTimeout(function(){
              $("#book_email").css('display','none'); 
              $("#book_email").html('');
            },3000);
          return false;
        }
        else if(reg.test(email) == false){
          $("#book_email").css('display','block'); 
          $("#book_email").append('<p style="color:red;" > Please enter valid Email</p>');
            setTimeout(function(){
              $("#book_email").css('display','none'); 
              $("#book_email").html('');
            },3000);
          return false;
        }
        else if(address ==''){
          $("#book_address").css('display','block'); 
          $("#book_address").append('<p style="color:red;" > Please enter proper Address</p>');
            setTimeout(function(){
              $("#book_address").css('display','none'); 
              $("#book_address").html('');
            },3000);
          return false;

        }
        else if(contact == '' ){
          $("#book_contact").css('display','block'); 
          $("#book_contact").append('<p style="color:red;" > Please enter contact</p>');
            setTimeout(function(){
              $("#book_contact").css('display','none'); 
              $("#book_contact").html('');
            },3000);
          return false;

        } 
        else if($.isNumeric(contact) == false){
          $("#book_contact").css('display','block'); 
          $("#book_contact").append('<p style="color:red;" > Please enter numeric value </p>');
            setTimeout(function(){
              $("#book_contact").css('display','none'); 
              $("#book_contact").html('');
            },3000);
          return false;
        } 
        else{
          return true;
        }

      }
      </script>
      <script type="text/javascript">
      
      </script>  
          
      
  </body>

 
</html>