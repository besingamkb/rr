<?php 
        /*Social Invite session unset*/
          if($this->session->userdata('social_invite'))
          {
            $session_data = $this->session->userdata('social_invite');
            $timeout = time()-$session_data['time'];
            if($timeout>1800)
            {
                $this->session->unset_userdata('social_invite');
            }
          }     
        /*Social Invite session unset*/

      /* Featured images updation*/
        get_featured_images();
      /* Featured images updation*/
 ?>

<html>
<head>
     <meta charset="utf-8">
<meta name="viewport" content="width=device-width" />
     <title>  </title>
    
     <?php if(!empty($meta_title)): ?>
        <meta name="<?php echo $meta_title ?>" content="width=device-width, initial-scale=1.0">
    <?php else: ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php endif; ?>

    <?php if(!empty($meta_description)): ?>
        <meta name="<?php echo word_limiter($meta_description,10) ?>" content="">
    <?php else: ?>
        <meta name="description" content="">
    <?php endif; ?>

    <?php if(!empty($meta_author)): ?>
        <meta name="<?php echo $meta_author ?>" content="">
    <?php else: ?>
        <meta name="author" content="">
    <?php endif; ?>
    


<!--     <link rel="shortcut icon" href="<?php //echo base_url(); ?>assets/img/favicon_v.ico"> -->  
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon_image.ico">
    <link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'> 
    <!-- Le styles -->
    <link href="<?php echo base_url()?>assets/front_end_theme/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/front_end_theme/css/jquery-ui.css" rel="stylesheet">
        

    <style type="text/css">
      body {
      }




      .navbar .nav > li > a{
        padding: 8px 15px !important;
      }

      .list li a{
        border-radius: 5px !important;
        background-color: #f7f7f7 !important;
        color: #333 !important;
        /*color: #989D9F !important;*/
      }

      .bg5{
        border-radius: 5px !important;
        margin-top: 13px !important;
      }

      .text9{
        border-radius: 5px !important;
        margin-left: 40% !important;
      }

     
    </style>
    <link href="<?php echo base_url()?>assets/front_end_theme/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/front_end_theme/style.css">
    
     <script src="<?php echo base_url()?>assets/front_end_theme/js/jquery.js"></script>
     <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
   
     <script src="<?php echo base_url()?>assets/front_end_theme/js/jquery-ui.js"></script>

</head>
<body>

     <?php $langhead = lang('header'); ?>

  <div id="header">
    <div class="container" style="width:100% !important">
        <div class="navbar" style="margin-left:3%">
            <div class="bg">
                <div id="logo" style="padding-top:12px; margin-left:4.5%">
                   <a href="<?php echo base_url(); ?>" style="text-decoration:none;">
                    <?php $logo = get_buttons_color() ?>
                      <?php  if(!empty($logo)):?>
                       <span style=""><img src="<?php echo base_url() ?>assets/front_end_theme/img/logo/<?php echo $logo->logo ?>"></span>
                        <?php else: ?>
                       <span style=""><img src="<?php echo base_url() ?>assets/front_end_theme/img/logo1.png"></span>
                     <?php endif; ?>
                  </a>
                </div>
                <div class="form" style="margin-left:2%">
                    <form class="navbar-form" action="<?php echo base_url() ?>properties/index" method="post">
                       <div class="input-prepend" style="margin-top:3.5%">
                          <span class="add-on" style="height:21px; padding-top:8px; background-color:#FFF; color:#CCC; border-color:#FFF"> <i class="icon icon-search"></i></span>
                          <input id="header_location" name="location" class="span3 all_search_header_form" style="height:35px; border-color: #FFF;" type="text" placeholder="Where are you going?">
                        </div>
                    </form>
                </div>
                    <button type="button" class="btn btn-navbar btn1" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>  
                    </button>
                <div class="nav-collapse collapse">
<ul class="nav list pull-right" style="margin-right:6%">

<!-- start the dynamic li for nav .bar -->
<?php $header = get_cms_page(1); ?>
<?php if(!empty($header)): ?>
    <?php foreach($header as $head): ?>    
        <li><a href="<?php  echo base_url(); ?>page/cms_page/<?php echo $head->slug;  ?>" ><?php echo $head->page_title;  ?></a></li>
    <?php endforeach; ?>
<?php endif; ?>    
<!-- End the dynamic li for nav .bar -->




<!-- change currency li -->
<style>
#curr_ul a:hover{ color:#E05284 !important; cursor: pointer;  }
</style>
<li>
<div class="dropdown">
  <button  style="height:36px;width:86px; background-color:#FFFFFF !important; color:#333333 !important;" class="btn dropdown-toggle sr-only" type="button" id="dropdownMenu1" data-toggle="dropdown">
    <?php echo $this->session->userdata('currency'); ?>
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" id='curr_ul' style="width:86px">
    <li><a onclick='changeCurrency("AUD")' tabindex='-1'>AUD</a></li>
    <li><a onclick='changeCurrency("BRL")' tabindex='-1'>BRL</a></li>
    <li><a onclick='changeCurrency("CAD")' tabindex='-1'>CAD</a></li>
    <li><a onclick='changeCurrency("CHF")' tabindex='-1'>CHF</a></li>
    <li><a onclick='changeCurrency("CZK")' tabindex='-1'>CZK</a></li>
    <li><a onclick='changeCurrency("DKK")' tabindex='-1'>DKK</a></li>
    <li><a onclick='changeCurrency("EUR")' tabindex='-1'>EUR</a></li>
    <li><a onclick='changeCurrency("GBP")' tabindex='-1'>GBP</a></li>
    <li><a onclick='changeCurrency("HKD")' tabindex='-1'>HKD</a></li>
    <li><a onclick='changeCurrency("HUF")' tabindex='-1'>HUF</a></li>
    <li><a onclick='changeCurrency("JPY")' tabindex='-1'>JPY</a></li>
    <li><a onclick='changeCurrency("MYR")' tabindex='-1'>MYR</a></li>
    <li><a onclick='changeCurrency("MXN")' tabindex='-1'>MXN</a></li>
    <li><a onclick='changeCurrency("NOK")' tabindex='-1'>NOK</a></li>
    <li><a onclick='changeCurrency("NZD")' tabindex='-1'>NZD</a></li>
    <li><a onclick='changeCurrency("PHP")' tabindex='-1'>PHP</a></li>
    <li><a onclick='changeCurrency("PLN")' tabindex='-1'>PLN</a></li>
    <li><a onclick='changeCurrency("RUB")' tabindex='-1'>RUB</a></li>
    <li><a onclick='changeCurrency("SGD")' tabindex='-1'>SGD</a></li>
    <li><a onclick='changeCurrency("SEK")' tabindex='-1'>SEK</a></li>
    <li><a onclick='changeCurrency("THB")' tabindex='-1'>THB</a></li>
    <li><a onclick='changeCurrency("USD")' tabindex='-1'>USD</a></li>
  </ul>
</div>
</li>



<li>
<div class="dropdown">
  <button  style="height:36px; background-color:#FFFFFF !important; color:#333333 !important;" class="btn dropdown-toggle sr-only" type="button" id="dropdownMenu1" data-toggle="dropdown">
    <?php if(!$this->session->userdata('site_lang')){ echo "English"; }elseif($this->session->userdata('site_lang') !='english'){ echo 'Français'; }else{ echo ucfirst($this->session->userdata('site_lang')); } ?>
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" id='cur_ul'>
    <li><a href="<?php echo base_url() ?>front/switchLanguage/english"  tabindex='-1'>English</a></li>
    <li><a href="<?php echo base_url() ?>front/switchLanguage/french"  tabindex='-1'>Français</a></li>   
  </ul>
</div>
</li>
<script>
function changeCurrency(currency)
{
    $.ajax({
            type:'post',
            data:{currency:currency},
            url:'<?php echo base_url(); ?>front/set_currency',
            success:function(res)
            {
                window.location.replace('');
            }
    });
}
</script>
<!-- change currency li -->





<?php if(!$this->session->userdata('user_info') && !$this->session->userdata('customer_info')): ?>
    <li><a data-toggle="modal" data-target="#registration_modal_1"  > <?php echo $langhead['signup']; ?> </a></li>
    <li><a data-toggle="modal" data-target="#login_modal" id="zakir"><?php echo $langhead['login']; ?> </a></li>
<?php elseif($this->session->userdata('user_info')): ?>                                
    <li><a href="<?php  echo base_url(); ?>user/dashboard" ><?php echo $langhead['dashboard']; ?></a></li>
    <li><a href="<?php  echo base_url(); ?>user/logout" ><?php echo $langhead['logout']; ?></a></li>
<?php else: ?>                                
    <li><a href="<?php  echo base_url(); ?>customer/dashboard" ><?php echo @$langhead['dashboard']; ?></a></li>
    <li><a href="<?php  echo base_url(); ?>customer/logout" ><?php echo @$langhead['logout']; ?></a></li>
<?php endif; ?>

<!-- <li><a  <?php if($this->session->userdata('user_info')){ echo "href=".base_url()."front/listing_property_first"; }else{ ?> data-toggle="modal" data-target="#login_listing_modal"  <?php } ?> style="background-color:#8ecd9e !important; color:#fff !important;">List Your Space</a></li> -->

</ul>
                   </div>
                 </div>
               </div>
            </div>
        </div>

    <!-- registration page 1 -->
<div class="modal show" id="registration_modal_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="border:1px solid #B3ABA9; padding:3px 8px; border-radius:3px;"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="color:#666666;">Create your free account</h3>
            </div>
            <div class="modal-body" align="center">
            <div  id="signup_success_msg" style="padding:10px !important;" align="center" ></div>
                <p>Discover things to do nearby.<br>Meet new people in an authentic way.</p>
               <!-- My task -->
                <div style='display:none;'>    
                    <font color="#666666"><b>Register as</b></font>
                    &nbsp;
                    <font color="#E05284"> 
                    Customer&nbsp;<input type="radio" name="user_google_role" value="15"  style="width:15px; height:15px;" >
                    &nbsp;&nbsp;
                    Property Owner&nbsp;<input type="radio" name="user_google_role" value="20"  style="width:18px; height:18px;" checked="checked">
                    </font>
                </div>
               <!-- My task -->
              
                <a href="javascript:void(0)" id="facebook-signup" value="facebook"> 
                    <img src="<?php echo base_url(); ?>assets/img/facebook.png" style="width:280px !important; height:46px !important; " >
                </a>
                <br><p></p>
                <div  >
                <a href="javascript:void(0)" class="open_id_signup" name="openid" value="google"> 
                    <img src="<?php echo base_url(); ?>assets/img/google.png" style="width:280px !important; height:46px !important; " >
                </a>
                </div>
                <p></p>
                <div style="margin-left:23.8%;">
                    <div style="float:left;">                   <a href="javascript:void(0)" id="open_id_twitter_signup" style="text-decoration:none; color:#666666;"><img src="<?php echo base_url(); ?>assets/img/twitter.png" style="width:45px !important; height:45px !important; " ><br>Twitter</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" value="yahoo" class="open_id_signup"><img src="<?php echo base_url(); ?>assets/img/yahoo.png"   style="width:45px !important; height:45px !important; " ><br>Yahoo</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" value="msn" class="open_id_signup"><img src="<?php echo base_url(); ?>assets/img/hotmail.png" style="width:45px !important; height:45px !important; " ><br>Hotmail</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_signup" value="google"><img src="<?php echo base_url(); ?>assets/img/gmail.png"   style="width:45px !important; height:45px !important; " ><br>Gmail</a></div>
                </div>
                <br>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div  style="display:none">
                <a href="javascript:void(0)" class="open_id_signup" name="openid" value="linkedin"> 
                    <img src="<?php echo base_url(); ?>assets/img/Linkedin-Connect.png" style="width:280px !important; height:60px !important; " >
                </a>
                </div>
                <div style="float:left; color:#B3ABA9;">
                    - - - - - - - - - - - - - - - - - - - - - - - - - - -
                        &nbsp;<i style="color:black;"> or </i>&nbsp; 
                    - - - - - - - - - - - - - - - - - - - - - - - - - - -
                </div>
                <p></p>
                <a data-dismiss="modal" style="text-decoration:none; cursor:pointer;"  data-toggle="modal" data-target="#registration_modal_2" >use regular email sign up</a>
                <br>
                <p></p> 
            </div>
            <div class="modal-footer">
                 <p class="pull-right">Already have an account? <a data-dismiss="modal" style="text-decoration:none; cursor:pointer;"  data-toggle="modal" data-target="#login_modal">Log in</a> </p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- registration page 2 -->
<style>
#registration_modal_2 input{width:300px; font-size: 16px; height:35px; border-radius:7px;}
</style>
<div class="modal show" id="registration_modal_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none; ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="border:1px solid #B3ABA9; padding:3px 8px; border-radius:3px;"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="color:#666666;">Create your free account</h3>
            </div>
            <div  id="register_success_msg" style="padding:10px !important;" align="center">
            </div>

            <div class="modal-body" style="padding-left:24%; padding-top:0px !important;">
                    <div style='display:none;'>    
                        <font color="#666666"><b>Register as</b></font>
                        &nbsp;
                        <font color="#E05284"> 
                        Customer&nbsp;<input type="radio" name="user_role" value="4" style="width:15px; height:15px;" >
                        &nbsp;&nbsp;
                        Property Owner&nbsp;<input type="radio" name="user_role" value="3" style="width:18px; height:18px;" checked="checked">
                        </font>
                    </div>

                    <span style="color:#E05284;" id="fname"></span>
                    <br>
                    <div>
                        <input type="text" name="first_name" id="first_name"  placeholder="First Name">
                    </div>
                    
                    </span><span  style="color:#E05284;" id="lname"></span>
                    <br>
                    <div>
                        <input type="text" name="last_name" id="last_name"  placeholder="Last Name">
                    </div>

                    </span><span style="color:#E05284;" id="email"></span>
                    <br>
                    <div>
                        <input type="text" name="user_email" id="user_email"  placeholder="Email Address" >
                    </div>

                    </span><span style="color:#E05284;" id="pass"></span>
                    <br>
                    <div>
                        <input type="password" name="password" id="password"  placeholder="Password" >
                    </div>

                    </span><span  style="color:#E05284;"id="con_pass"></span>
                    <br>
                    <div>
                        <input type="password" name="con_password" id="con_password"  placeholder="Confirm Password">
                    </div>
                    
                    <br>
                    <div style="background-color:#ffffff;" id="registration_button">
                        <button onClick="return check_registration();" class="btn  btn-primary" type="submit">Register now</button>
                    </div>
            </div>
            <div class="modal-footer">
                <p class="pull-right">Already have an account? <a data-dismiss="modal" style="text-decoration:none; cursor:pointer;"  data-toggle="modal" data-target="#login_modal">Log in</a> </p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
/////////////////////////////////////////////////
/////////  registration form  //////////////////
///////////////////////////////////////////////
function check_registration(){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var email = $('#user_email').val();
    var password = $("#password").val();
    var confirm_pass = $("#con_password").val();
    var first_name = $("#first_name").val();
    var last_name  = $("#last_name").val();
    var user_role = $('input[name=user_role]:radio:checked').val();

        if(first_name =='')
        {
          $('#fname').html('Please enter first name');
          setTimeout(function(){
            $('#fname').html('');
          },5000);
          return false;
        }
        else if(last_name=='')
        {
          $('#lname').html('Please enter last name');
          setTimeout(function(){
            $('#lname').html('');
          },5000);
          return false;
        }
        else if(email=='')
        {
          $('#email').html('Please enter the email first');
          setTimeout(function(){
            $('#email').html('');
          },5000);
          return false;
        }
        else if(reg.test(email)==false)
        {
          $('#email').html('Please enter Correct Registered email..');
          setTimeout(function(){
            $('#email').html('');
          },5000);
          return false;
        }
        else if(password=='')
        {
          $('#pass').html('Please enter Password');
          setTimeout(function(){
            $('#pass').html('');
          },5000);
          return false;
        }
        else if(password.length < 6)
        {
          $('#pass').html('Password must be at least 6 characters');
          setTimeout(function(){
            $('#pass').html('');
          },5000);
          return false;
        }
        else if(confirm_pass=='')
        {
          $('#con_pass').html('Please enter Confirm Password');
          setTimeout(function(){
            $('#con_pass').html('');
          },5000);
          return false;
        }
        else if(confirm_pass != password)
        {
          $('#con_pass').html('Password and Confirm Password field must be same.');
          setTimeout(function(){
            $('#con_pass').html('');
          },5000);
          return false;
        }
        else if(email != null)
        {
            $.ajax({
                    type:"POST",
                    data:{email:email},
                    url:"<?php echo base_url(); ?>front/ajax_check_unique_email",
                    success:function(res)
                    {
                        if(res == "false")
                        {
                            $('#email').html('Email already exist, Please enter other email..!');
                            setTimeout(function(){ $('#email').html(''); },5000);
                            return false;
                        }    
                        else
                        {  
                            $('#registration_button').html('<span style="color:#E05284; font-size:16px; "><b>Please wait.......!!</b></span>');  
                            $.ajax({
                                    type:"POST",
                                    data:{user_role:user_role, first_name:first_name, last_name:last_name, user_email:email, password:password},
                                    url:"<?php echo base_url(); ?>front/register",
                                    success:function(result)
                                    {
                                       $('#register_success_msg').html("<span class='alert alert-success'>"+result+"</span>");
                                       $('#registration_button').html('<button onclick="return check_registration();" class="btn  btn-primary" type="submit">Register now</button></b></span>');  
                                    }
                            });
                        }    
                    }
            });
        }
    }
</script>
<script>
$(document).ready
  (function(){
  <?php if(@$flag==1) : ?>
  $('#zakir').trigger('click');

           <?php if($this->session->flashdata('success_msg')!=""): ?>
              $('#login_success_msg').html("<span class='alert alert-success'><?php echo $this->session->flashdata('success_msg'); ?></span>");
              setTimeout(function(){
                $('#login_success_msg').html('');
              },5000);
           <?php endif; ?>
           <?php if($this->session->flashdata('error_msg')!=""): ?>
              $('#login_success_msg').html("<span class='alert alert-error'><?php echo $this->session->flashdata('error_msg'); ?></span>");
              setTimeout(function(){
                $('#login_success_msg').html('');
              },5000);
           <?php endif; ?>
           

  <?php endif; ?>

   <?php if(@$flag==2): ?>

  $('#faraz').trigger('click');
    
           <?php if($this->session->flashdata('success_msg')!=""): ?>
              $('#signup_success_msg').html("<span class='alert alert-success'><?php echo $this->session->flashdata('success_msg'); ?></span>");
              setTimeout(function(){
                $('#signup_success_msg').html('');
              },5000);
           <?php endif; ?>
           <?php if($this->session->flashdata('error_msg')!=""): ?>
              $('#signup_success_msg').html("<span class='alert alert-error'><?php echo $this->session->flashdata('error_msg'); ?></span>");
              setTimeout(function(){
                $('#signup_success_msg').html('');
              },5000);
           <?php endif; ?>

   <?php endif; ?>  
});
</script>


<!-- Log-in page -->
<style>
#login_modal input{width:300px; font-size: 16px; height:35px; border-radius:7px;}
#login_modal{
    position: fixed !important;
}
</style>
<div class="modal show" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display:none; ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="border:1px solid #B3ABA9; padding:3px 8px; border-radius:3px;"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="color:#666666;"> Log in </h3>
            </div>
            <div class="modal-body" align="center">
                <a href="javascript:void(0)" id="facebook-login" value="facebook"> 
                    <img src="<?php echo base_url(); ?>assets/img/facebook.png" style="width:280px !important; height:46px !important; " >
                </a>
                <p></p>
                <a href="javascript:void(0)" class="open_id_login" value="google"> 
                    <img src="<?php echo base_url(); ?>assets/img/google.png" style="width:280px !important; height:46px !important; " >
                </a>
                <p></p>
                <div id="social_btns">
                <div style="float:left;"><a href="javascript:void(0);" style="text-decoration:none; color:#666666;" id="open_id_twitter_login" ><img src="<?php echo base_url(); ?>assets/img/twitter.png" style="width:45px !important; height:45px !important; " ><br>Twitter</a></div>                  
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_login" value="yahoo"><img src="<?php echo base_url(); ?>assets/img/yahoo.png"   style="width:45px !important; height:45px !important; " ><br>Yahoo</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_login" value="msn"><img src="<?php echo base_url(); ?>assets/img/hotmail.png" style="width:45px !important; height:45px !important; " ><br>Hotmail</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_login" value="google"><img src="<?php echo base_url(); ?>assets/img/gmail.png"   style="width:45px !important; height:45px !important; " ><br>Gmail</a></div>
                </div>
                <br><br><br><br>
                <div  style="display:none">
                <a href="javascript:void(0)" class="open_id_login" name="openid" value="linkedin"> 
                    <img src="<?php echo base_url(); ?>assets/img/Linkedin-Connect.png" style="width:280px !important; height:60px !important; " >
                </a>
                </div>
                <span id="underline" align="center">
                    - - - - - - - - - - - - - - - - - - - - - - -
                        &nbsp;<i style="color:black;"> or </i>&nbsp; 
                    - - - - - - - - - - - - - - - - - - - - - - -
                </span>
            </div>
            <div  id="login_success_msg" align="center"></div>
            <div id="loginform">     
                <div style='display:none;' >    
                    <font color="#666666"><b>Log in as</b></font>
                    &nbsp;&nbsp;&nbsp;<br>
                    <font color="#E05284"> 
                    &nbsp;<input class="radio_btn" type="radio" name="login_user_role" value="4" >
                    Customer&nbsp;&nbsp;<br>
                    &nbsp;<input class="radio_btn" type="radio" name="login_user_role" value="3" checked="checked">Property Owner
                    </font>
                </div>

                <span id="login_email_test"></span>
                <br>
                <div class="form_inputs">
                    <input type="text" name="login_email" id="login_email" placeholder="Email address">
                </div>
                
                <span id="login_password_test"></span>     
                <br>
                <div class="form_inputs"> 
                   <input type="password" name="login_password" id="login_password" placeholder="Password" >
                </div>
                 
                <div class="form_inputs"> 
                <button onClick="return check_login();" class="btn btn-lg btn-primary" type="submit">Login</button>
                </div> 
                <div>&nbsp;</div>
            </div>
            <div class="modal-footer">
                <p class="pull-left">Forgot your password? <a href="<?php echo base_url(); ?>front/forget_password" style="text-decoration:none;"> Reset it here</a> </p>
                <p class="pull-right">It's free to join! <a data-dismiss="modal" style="text-decoration:none; cursor:pointer;"  data-toggle="modal" data-target="#registration_modal_1" id="faraz"> Sign up</a> </p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
/////////////////////////////////////////////////
////////////////  login form  //////////////////
///////////////////////////////////////////////
    function check_login()
    {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var email = $('#login_email').val();
        var password = $("#login_password").val();
        var user_role = $('input[name=login_user_role]:radio:checked').val();

        if(email=='')
        {
          $('#login_email_test').html('Please enter the email first');
          setTimeout(function(){
            $('#login_email_test').html('');
          },5000);
          return false;
        }
        else if(reg.test(email)==false){
          $('#login_email_test').html('Please enter Correct Registered email..');
          setTimeout(function(){
            $('#login_email_test').html('');
          },5000);
          return false;
        }
        if(password=='')
        {
          $('#login_password_test').html('Please enter Password');
          setTimeout(function(){
            $('#login_password_test').html('');
          },5000);
          return false;
        }
        else
        {
            $.ajax({
                    type:"POST",
                    data:{user_role:user_role, login_email:email, login_password:password},
                    url:"<?php echo base_url(); ?>front/user_login",
                    success:function(result)
                    {//alert(result);
                       if(result == "valid")
                       {
                            $('#login_success_msg').html("<span class='alert alert-success'>Welcome, Please wait....................!</span>");
                            if(user_role == 3)
                            {
                            window.location.replace("<?php echo base_url(); ?>user/dashboard"); 
                            }
                            if(user_role == 4)
                            {
                            window.location.replace("<?php echo base_url(); ?>customer/dashboard"); 
                            }
                       }
                       else
                       {
                            $('#login_success_msg').html("<span class='alert alert-danger'>Invalid email or password, please enter correct email and password.</span>");
                       } 
                    }
            });
        }    
    }
</script>


<!-- Log-in page for listing -->
<style>
#login_listing_modal input{width:300px; font-size: 16px; height:35px; border-radius:7px;}
</style>
<div class="modal show" id="login_listing_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none; ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="border:1px solid #B3ABA9; padding:3px 8px; border-radius:3px;"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 style="color:#666666;"> Log in first to list your property. </h3>
            </div>
            <div class="modal-body" align="center">
                <a href="javascript:void(0)" id="facebook-login_on_listing" value="facebook">
                    <img src="<?php echo base_url(); ?>assets/img/facebook.png" style="width:280px !important; height:46px !important; " >
                </a>
                <p></p>
                <a href="javascript:void(0)" class="open_id_login_for_owner" value="google"> 
                    <img src="<?php echo base_url(); ?>assets/img/google.png" style="width:280px !important; height:46px !important; " >
                </a>
                <p></p>
                <div style="margin-left:23.8%;">
                    <div style="float:left;"><a href="javascript:;" style="text-decoration:none; color:#666666;" id="open_id_twitter_owner_login" ><img src="<?php echo base_url(); ?>assets/img/twitter.png" style="width:45px !important; height:45px !important; " ><br>Twitter</a></div>                    
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_login_for_owner" value="yahoo"><img src="<?php echo base_url(); ?>assets/img/yahoo.png"   style="width:45px !important; height:45px !important; " ><br>Yahoo</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_login_for_owner" value="msn"><img src="<?php echo base_url(); ?>assets/img/hotmail.png" style="width:45px !important; height:45px !important; " ><br>Hotmail</a></div>
                    <div style="float:left; margin-left:8%; "><a href="javascript:;" style="text-decoration:none; color:#666666;" class="open_id_login_for_owner" value="google"><img src="<?php echo base_url(); ?>assets/img/gmail.png"   style="width:45px !important; height:45px !important; " ><br>Gmail</a></div>
                </div>
                <br>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div  style="display:none">
                <a href="javascript:void(0)" class="open_id_login_for_owner" name="openid" value="linkedin"> 
                    <img src="<?php echo base_url(); ?>assets/img/Linkedin-Connect.png" style="width:280px !important; height:60px !important; " >
                </a>
                </div>
                <span style="color:#B3ABA9;" align="center">
                    - - - - - - - - - - - - - - - - - - - - - - -
                        &nbsp;<i style="color:black;"> or </i>&nbsp; 
                    - - - - - - - - - - - - - - - - - - - - - - -
                </span>
            </div>
            <div  id="login_listing_success_msg" style="padding:10px !important;" align="center"></div>
            <div style="margin-left:24%;">     
                <span style="color:#E05284;" id="login_listing_email_test"></span>
                <br>
                <div>
                    <input type="text" name="login_email" id="login_listing_email" placeholder="Email address">
                </div>
                
                <span style="color:#E05284;" id="login_listing_password_test"></span>     
                <br>
                <div> 
                   <input type="password" name="login_password" id="login_listing_password" placeholder="Password" >
                </div>
                 
                <div> 
                <button onClick="return check_listing_login();" class="btn btn-lg btn-primary" type="submit">Login</button>
                </div> 
                <div>&nbsp;</div>
            </div>
            <div class="modal-footer">
                <p class="pull-left">Forgot your password? <a href="<?php echo base_url(); ?>front/forget_password" style="text-decoration:none;"> Reset it here</a> </p>
                <p class="pull-right">It's free to join! <a data-dismiss="modal" style="text-decoration:none; cursor:pointer;"  data-toggle="modal" data-target="#registration_modal_1"> Sign up</a> </p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
/////////////////////////////////////////////////
/////  login form for property listing  ////////
///////////////////////////////////////////////
    function check_listing_login()
    {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var email = $('#login_listing_email').val();
        var password = $("#login_listing_password").val();
        if(email=='')
        {
          $('#login_listing_email_test').html('Please enter the email first');
          setTimeout(function(){
            $('#login_listing_email_test').html('');
          },5000);
          return false;
        }
        else if(reg.test(email)==false){
          $('#login_listing_email_test').html('Please enter Correct Registered email..');
          setTimeout(function(){
            $('#login_listing_email_test').html('');
          },5000);
          return false;
        }
        if(password=='')
        {
          $('#login_listing_password_test').html('Please enter Password');
          setTimeout(function(){
            $('#login_listing_password_test').html('');
          },5000);
          return false;
        }
        else
        {
            $.ajax({
                    type:"POST",
                    data:{user_role:3, login_email:email, login_password:password},
                    url:"<?php echo base_url(); ?>front/user_login",
                    success:function(result)
                    {
                       if(result == "valid")
                       {
                            $('#login_listing_success_msg').html("<span class='alert alert-success'>Welcome, Please wait....................!</span>");
                            window.location.replace("<?php echo base_url(); ?>front/listing_property_first"); 
                       }
                       else
                       {
                            $('#login_listing_success_msg').html("<span class='alert alert-danger'>Invalid email or password, please enter correct email and password.</span>");
                       } 
                    }
            });
        }    
    }

    // $(function() {
    //         var e;           
    //         //$("body").addClass("show_wishlists_search");
    //         e = $('.form1 input[name="location"]')[0];
    //         return new google.maps.places.Autocomplete(e, {
    //             // types: ["geocode"]
    //             types: ['(cities)'],
    //             // types: ['(regions)']
    //         });
           
    //     });
</script>

        <script>

////////////////////////////////////////////////
/////// Open id Sign_up page javascript starts////
///////////////////////////////////////////////
        $(document).ready(function()
          {
            $('.open_id_signup').click(function(){
                var user_role = $('input[name=user_google_role]:radio:checked').val();
                var openid = $(this).attr('value');
                if(user_role==15){
                   $.ajax({
                            type:"POST",
                            data:{user_role:user_role},
                            url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                            success:function(res){
                                if(res==15){
                                    window.location = "<?php echo base_url() ?>front/open_id_login_process/"+ openid;
                                }
                            }
                
                    });

                }
                else if(user_role==20){
                    $.ajax({
                            type:"POST",
                            data:{user_role:user_role},
                            url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                            success:function(res){
                                if(res==20){
                                    window.location = "<?php echo base_url() ?>front/open_id_login_process/"+ openid;
                                }
                            }

                    });

                }
          });
       });

////////////////////////////////////////////////
/////// Open id Sign_up page javascript starts////
///////////////////////////////////////////////


////////////////////////////////////////////////
/////// Open id login page javascript starts////
///////////////////////////////////////////////
        $(document).ready(function()
          {
            $('.open_id_login').click(function(){
                var user_role = $('input[name=login_user_role]:radio:checked').val();
                var openid = $(this).attr('value');
                if(user_role==4){
                   $.ajax({
                            type:"POST",
                            data:{user_role:user_role},
                            url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                            success:function(res){
                                if(res==4){
                                    window.location = "<?php echo base_url() ?>front/open_id_login_process/"+ openid;
                                }
                            }
                
                    });

                }
                else if(user_role==3){
                    $.ajax({
                            type:"POST",
                            data:{user_role:user_role},
                            url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                            success:function(res){
                                if(res==3){
                                    window.location = "<?php echo base_url() ?>front/open_id_login_process/"+ openid;
                                }
                            }

                    });

                }
          });
       });

        $(document).ready(function()
          {
            $('.open_id_login_for_owner').click(function(){
                var openid = $(this).attr('value');
                   $.ajax({
                            type:"POST",
                            data:{user_role:3},
                            url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                            success:function(res){
                                if(res==3){
                                    window.location = "<?php echo base_url() ?>front/open_id_login_process/"+ openid;
                                }
                            }
                
                    });

          });
       });



////////////////////////////////////////////////
/////// Open id login page javascript Ends////
///////////////////////////////////////////////

        </script>
<div id="fb-root"></div>

<script type="text/javascript">

////////////////////////////////////////////////
/////// Open id login page facebook starts////
///////////////////////////////////////////////

<?php $facebook = get_oauth_keys('facebook'); ?>

  window.fbAsyncInit = function() {
      //Initiallize the facebook using the facebook javascript sdk
     FB.init({       
       appId:<?php echo $facebook->app_id ?>, // App ID 
       cookie:true, // enable cookies to allow the server to access the session
       status:true, // check login status
       xfbml:true, // parse XFBML
       oauth : true //enable Oauth 
     });
   };
   //Read the baseurl from the config.php file
   (function(d){
           var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/en_US/all.js";
           ref.parentNode.insertBefore(js, ref);
         }(document));



    //Onclick for fb login



 jQuery('#facebook-signup').click(function(e) {
    FB.login(function(response) {
      if(response.authResponse) {
                var user_role = $('input[name=user_google_role]:radio:checked').val();
                $.ajax({
                        type:"POST",
                        data:{user_role:user_role},
                        url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                        success:function(res){
                              parent.location ='<?php echo base_url() ?>open_id_login_controller/facebook'; //redirect uri after closing the facebook popup
                        }
                  });

      }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook

});

 jQuery('#facebook-login').click(function(e) {
    FB.login(function(response) {
      if(response.authResponse) {
                var user_role = $('input[name=login_user_role]:radio:checked').val();
                $.ajax({
                        type:"POST",
                        data:{user_role:user_role},
                        url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                        success:function(res){
                              parent.location ='<?php echo base_url() ?>open_id_login_controller/facebook'; //redirect uri after closing the facebook popup
                        }
                  });

      }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook

});

 jQuery('#facebook-login_on_listing').click(function(e) {
    FB.login(function(response) {
      if(response.authResponse) {
                var user_role = 3;
                $.ajax({
                        type:"POST",
                        data:{user_role:user_role},
                        url:"<?php echo base_url(); ?>open_id_login_controller/set_session_by_ajax",
                        success:function(res){
                              parent.location ='<?php echo base_url() ?>open_id_login_controller/facebook'; //redirect uri after closing the facebook popup
                        }
                  });

      }
 },{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook

}); 

////////////////////////////////////////////////
/////// Open id login page facebook starts////
///////////////////////////////////////////////

////////////////////////////////////////////////
/////// Open id sign up page Twitter starts////
///////////////////////////////////////////////
        $(document).ready(function()
          {
            $('#open_id_twitter_signup').click(function(){
                var user_role = $('input[name=user_google_role]:radio:checked').val();
                   $.ajax({
                            type:"POST",
                            data:{user_role:user_role, action:'signup'},
                            url:"<?php echo base_url(); ?>twitter/set_session_by_ajax",
                            success:function(res){
                                    window.location = "<?php echo base_url() ?>twitter";
                            }
                
                    });

          });
       });

        $(document).ready(function()
          {
            $('#open_id_twitter_login').click(function(){
                var user_role = $('input[name=login_user_role]:radio:checked').val();
                   $.ajax({
                            type:"POST",
                            data:{user_role:user_role, action:'login'},
                            url:"<?php echo base_url(); ?>twitter/set_session_by_ajax",
                            success:function(res){
                                    window.location = "<?php echo base_url() ?>twitter";
                            }
                
                    });

          });
       });

        $(document).ready(function()
          {
            $('#open_id_twitter_owner_login').click(function(){
                var user_role = 3;
                   $.ajax({
                            type:"POST",
                            data:{user_role:user_role},
                            url:"<?php echo base_url(); ?>twitter/set_session_by_ajax",
                            success:function(res){
                                    window.location = "<?php echo base_url() ?>twitter";
                            }
                
                    });

          });
       });

////////////////////////////////////////////////
/////// Open id sign up page Twitter starts////
///////////////////////////////////////////////

    $(function()
        {
            var e;           
            //$("body").addClass("show_wishlists_search");
            e = $('#header_location')[0];
            return new google.maps.places.Autocomplete(e, {
                // types: ["geocode"],
                types: ['(cities)'],
                // types: ['(regions)'],
            });
           
        });
</script>

<style type="text/css">


#logo:hover {
    position: relative !important;
-webkit-animation:wiggle .2s ease-in-out alternate;
-moz-animation:wiggle .2s ease-in-out alternate;
-ms-animation:wiggle .2s ease-in-out alternate
}

@-webkit-keyframes wiggle {
0% {
-webkit-transform:rotate3d(0, 0, 0, 0deg)
}
25% {
-webkit-transform:rotate3d(0, 0, 0, 5deg)
}
75% {
-webkit-transform:rotate3d(0, 0, 0, -5deg)
}
100% {
-webkit-transform:rotate3d(0, 0, 0, 0deg)
}
}
@-moz-keyframes wiggle {
0% {
-moz-transform:rotate(0deg)
}
25% {
-moz-transform:rotate(5deg)
}
75% {
-moz-transform:rotate(-5deg)
}
100% {
-moz-transform:rotate(0deg)
}
}
.pac-container:after
 { 
   content:none !important;
 }
</style>

