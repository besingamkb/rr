<!DOCTYPE html>
  <!--[if lt IE 7]>
    <html class="lt-ie9 lt-ie8 lt-ie7" lang="en">
  <![endif]-->

  <!--[if IE 7]>
    <html class="lt-ie9 lt-ie8" lang="en">
  <![endif]-->

  <!--[if IE 8]>
    <html class="lt-ie9" lang="en">
  <![endif]-->

  <!--[if gt IE 8]>
    <!-->
    <html lang="en">
    <!--
  <![endif]-->

  <head>
    <meta charset="utf-8">
    <title>Bnbclone | Subadmin dashboard</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon_pink.ico">
    <meta name="author" content="Srinu Basava">
    <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
    <meta name="description" content="White Label Admin Admin UI">
    <meta name="keywords" content="White Label Admin, Admin UI, Admin Dashboard, Srinu Basava">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/js/html5-trunk.js"></script>
    <script src="<?php //echo base_url(); ?>assets/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/date-picker/date.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/date-picker/daterangepicker.js"></script>
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/icomoon/style.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js"></script>
    <!--[if lte IE 7]>
    
    <!-- google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <!-- google map api -->
    <!-- file uploder -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fineuploader/fineuploader.css" type="text/css" >
    <script src="<?php echo base_url() ?>assets/fineuploader/jquery.fineuploader-3.8.0.min.js"></script>

    <!-- file uploder -->

    <!-- bootstrap css -->
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/wysiwyg/wysiwyg-color.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/timepicker.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-editable.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/select2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/js/jquery.ui.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wysiwyg/wysihtml5-0.3.0.js"></script>

  
    <script src="<?php echo base_url(); ?>assets/js/wysiwyg/bootstrap-wysihtml5.js"></script>
 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.js"></script>

    <!-- Editable Inputs -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-editable.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.js"></script>
    
    <script src="<?php //echo base_url(); ?>assets/js/custom-graphs.js"></script>


<!-- charts -->
       <!-- morris charts -->
    <script src="<?php echo base_url(); ?>assets/js/morris/morris.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/morris/raphael-min.js"></script>
   
    <!-- Flot charts -->
    <script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.selection.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.resize.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom-index.js"></script>
     
    <!-- Calendar Js -->
    <script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>

    <!-- Tiny Scrollbar JS -->
    <script src="<?php echo base_url(); ?>assets/js/tiny-scrollbar.js"></script>

    <!-- custom Js -->
   
    <script src="<?php echo base_url(); ?>assets/js/nvd/d3.v2.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/nv.d3.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/utils.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/legend.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/axis.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/scatter.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/line.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/lineWithFocusChart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nvd/stream_layers.js"></script>
    
    <!-- Google Visualization JS -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- customjs -->
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script src="<?php //echo base_url(); ?>assets/js/custom-graphs.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom-forms.js"></script>


<!-- charts -->




    <script> 


    </script>
    <style type="text/css">
     header .logo {
        max-width: 280px !important;
        max-height:35px !important;
         margin: 10px 0 0 -33px;
     }
    </style>
  </head>
  <body>
    <header>
      <a href="<?php echo base_url(); ?>subadmin" class="logo">
      <?php $logo = get_buttons_color() ?>
      <?php  /* if(!empty($logo)):?>
        <span style=""><img src="<?php echo base_url() ?>assets/front_end_theme/img/logo/<?php echo $logo->logo ?>"></span>
       <?php else: ?>
        <span style=""><img src="<?php echo base_url() ?>assets/front_end_theme/img/logo1.png"></span>
       <?php endif; */ ?>
        <span style=""><img src="<?php echo base_url() ?>assets/front_end_theme/img/logo/superadminlogo.png"></span>
      </a>
      <div id="mini-nav">
        <ul class="hidden-phone">
            <li> <a href=""> <strong> Hello, <?php echo get_subadmin_name(); ?></strong></a> </li>
            <li><a href="<?php echo base_url() ?>user/dashboard" >Visit Site</a></li>
            <li><a href="#" >Tasks</a></li>
            <li style="display:none"><a>Signup's <span><?php echo get_signup(); ?></span></a></li>
            <!-- message Notificatin => Srart  -->
            <?php $msg = get_message_info(); ?>
            <?php if($msg['row']>0): ?>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="">
                        Messages <span><?php echo $msg['row']; ?></span><b class="caret icon-white"></b>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <?php foreach ($msg['data'] as $row): ?>
                            <a href="<?php echo base_url(); ?>subadmin/message_view/<?php echo $row->id; ?>">
                                <li class="quick-messages">
                                    <img src="<?php echo base_url(); ?>assets/img/message.png" class="avatar" alt="Avatar">
                                    <div class="message-wrapper">
                                        <h4 class="message-heading"><?php echo word_limiter($row->subject,10); ?></h4>
                                        <p class="message">[<font color="#D33F3E"><?php echo reply_count($row->id); ?></font>]
                                            Sent by <font color="#D33F3E"><?php echo $row->user_name; ?></font></p>
                                        <p class="message">on <?php echo get_time_ago($row->created); ?></p>
                                    </div>
                                </li>
                            </a>
                        <?php endforeach; ?>
                        <a href="<?php echo base_url(); ?>subadmin/messages">
                            <li class="quick-messages">
                                <div class="message-wrapper">
                                    <p class="message">
                                        View All Messages
                                    </p>
                                </div>
                            </li>
                        </a> 
                    </ul>
                </li>
            <?php else: ?>
                <li class="dropdown">
                    <a href="<?php echo base_url(); ?>subadmin/messages">
                        Messages <span><?php echo $msg['row']; ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <!-- message Notificatin => End  -->
            <li><a href="<?php echo base_url(); ?>subadmin/logout">Logout</a></li>
        </ul>
      </div>
    </header>
    <div class="container-fluid">
        <?php $this->load->view('subadmin/leftbar'); ?>
        <div class="row-fluid">
          <div class="span12">
            <ul class="breadcrumb-beauty">
              <li>
                <a href="<?php echo base_url()?>subadmin/dashboard"><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</a>
              </li>
              <li>
                <a href="#">Reports</a>
              </li>
            </ul>
          </div>
        </div>

        <br>

        <style type="text/css">


.logo:hover {
    position: relative !important;
-webkit-animation:wiggle .2s ease-in-out alternate;
-moz-animation:wiggle .2s ease-in-out alternate;
-ms-animation:wiggle .2s ease-in-out alternate
}

@-webkit-keyframes wiggle {
0% {
-webkit-transform:rotate3d(0, 0, 0, 0deg)
}
25% {
-webkit-transform:rotate3d(0, 0, 0, 5deg)
}
75% {
-webkit-transform:rotate3d(0, 0, 0, -5deg)
}
100% {
-webkit-transform:rotate3d(0, 0, 0, 0deg)
}
}
@-moz-keyframes wiggle {
0% {
-moz-transform:rotate(0deg)
}
25% {
-moz-transform:rotate(5deg)
}
75% {
-moz-transform:rotate(-5deg)
}
100% {
-moz-transform:rotate(0deg)
}
}
</style>