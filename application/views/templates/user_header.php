<?php 
        /*Social Invite session unset*/
          if($this->session->userdata('social_invite'))
          {
            $session_data = $this->session->userdata('social_invite');
            $timeout = time()-$session_data['time'];
            if($timeout>1800)
            {
                $this->session->unset_userdata('social_invite');
            }
          }     
        /*Social Invite session unset*/

      /* Featured images updation*/
        get_featured_images();
      /* Featured images updation*/
        
 ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon_image.ico">
    <title>Bnbclone | User Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>assets/bnb_html/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>assets/bnb_html/css/style.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
   
    <script src="<?php echo base_url()?>assets/bnb_html/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.ui.js"></script>
    <link href="<?php echo base_url()?>assets/css/jquery-ui.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-editable.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

     <link rel="stylesheet" href="<?php echo base_url() ?>assets/fineuploader/fineuploader.css" type="text/css" >
    <script src="<?php echo base_url() ?>assets/fineuploader/jquery.fineuploader-3.8.0.min.js"></script>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <label>
          <a style="text-decoration:none;" class="logo" href="<?php echo base_url(); ?>front">
            <?php $logo = get_buttons_color() ?>
              <?php  if(!empty($logo)):?>
               <span style="" ><img class="logo" src="<?php echo base_url() ?>assets/front_end_theme/img/logo/<?php echo $logo->logo ?>"></span>
                <?php else: ?>
               <span style="" ><img class="logo" src="<?php echo base_url() ?>assets/front_end_theme/img/logo1.png"></span>
             <?php endif; ?>
          </a>
          </label>
         </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav pull-right">

            <li><a href="#" >
                <?php $user = get_user_row();  ?>
                <?php if(!empty($user->image)) : ?>

            <img class="img-circle" src="<?php echo base_url()?>assets/uploads/profile_image/<?php echo $user->image;  ?>"  style="height:50px;width:50px">
             <?php else: ?>

            <img class="img-circle" src="<?php echo base_url()?>assets/bnb_html/img/user.png"  style="height:50px">
            <!--         <img class="img-circle" src="<?php echo base_url()?>assets/bnb_html/img/user.png" style="height:56px">  -->
           <?php endif; ?>
              &nbsp;&nbsp;<?php echo ucfirst($user->first_name); ?></a>                
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" ><b class="caret"></b></a>  
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url()?>user/profile">Profile</a></li>
                <li><a href="<?php echo base_url()?>user/change_profile_image">Upload Photo</a></li>
                <li><a href="<?php echo base_url()?>user/properties">Properties</a></li>                  
                <li><a href="<?php echo base_url()?>user/logout">Logout</a></li>
              </ul>          
            </li>
          </ul>
        </div><!--/.nav-collapse -->        
      </div>
    </div>

    <style type="text/css">
.logo:hover {
    position: relative !important;
-webkit-animation:wiggle .2s ease-in-out alternate;
-moz-animation:wiggle .2s ease-in-out alternate;
-ms-animation:wiggle .2s ease-in-out alternate
}

@-webkit-keyframes wiggle {
0% {
-webkit-transform:rotate3d(0, 0, 0, 0deg)
}
25% {
-webkit-transform:rotate3d(0, 0, 0, 5deg)
}
75% {
-webkit-transform:rotate3d(0, 0, 0, -5deg)
}
100% {
-webkit-transform:rotate3d(0, 0, 0, 0deg)
}
}
@-moz-keyframes wiggle {
0% {
-moz-transform:rotate(0deg)
}
25% {
-moz-transform:rotate(5deg)
}
75% {
-moz-transform:rotate(-5deg)
}
100% {
-moz-transform:rotate(0deg)
}
}
</style>