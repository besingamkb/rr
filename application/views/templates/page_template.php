<?php $this->load->view('templates/front_header');  ?>

<!-- start the content -->
<div  style="min-height:530px;">
    <?php if(!empty($content->content)) echo $content->content; ?>
    <?php if(!empty($error)) echo $error; ?>
</div>
<!-- end the content -->






<?php if($this->uri->segment(3) == 'guesthost'): ?>
<script type="text/javascript">
$(document).ready(function(){
    var li1 = $('#LItab1').html();
    $('#LItab1').html('<a href="#tab1">'+li1+'</a>');
    var li2 = $('#LItab2').html();
    $('#LItab2').html('<a href="#tab2">'+li2+'</a>');
});


$(document).on('click','.tabs .tab-links a',function(e){
    var currentAttrValue = $(this).attr('href');
    $('.tabs ' + currentAttrValue).show().siblings().hide();
    $(this).parent('li').addClass('active').siblings().removeClass('active');
    e.preventDefault();
});
</script>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<style>
@charset "utf-8";
/* CSS Document */

body{
    font-family: 'Open Sans', sans-serif;
    padding: 0;
    margin: 0;
    font-size: 14px;
}

#main{
    
}

#page{
    width: 1000px;
    margin: 0 auto;
}

#content{
    float: left;
    margin-top:-81px;
    margin-bottom: 20px;
    position: relative;

}



#banner{
    float: left;
    width: 100%;
    height: 645px;
    margin-top: 15px;
    position: relative;
    background: #e05284;
}

#banner_heading {
    position: absolute;
    top: 45px;
    width: 100%;
    text-align: center;
    color: #fff;
}

#banner_heading h2{
    font-size: 32px;
    margin-bottom: 0;
}

#banner_heading p{
    font-size: 16px;
    font-weight: 400;
    text-shadow: 0px 0px 0px #FFF;
    letter-spacing: 0.5px;
    margin-top: 5px;
    line-height: 24px;
}

#banner_img{
    width: 100%;
}

#patch{
    position: absolute;
    background: rgba(0, 0, 0, 0.56);
    color: #fff;
    width: 70%;
    bottom: 22%;
    left: 15%;
}

#video1{
    
}

div#video {
    padding: 20px;
    text-align: center;
}

#text{
    padding: 48px 35px;
}

.half {
    width: 50%;
    float: left;
    box-sizing: border-box;
}

div.right_content {
position: absolute;
top: 10px;
right: 20px;
width: 48%;
color: #fff;
height: 95%;
}

div.left_content {
position: absolute;
top: 10px;
left: 20px;
width: 48%;
color: #fff;
height: 95%;
}

div.tab_divs {
position: relative;
margin-bottom: 10px;
}

.right_content span.number {
font-size: 38px;
border: 4px solid #fff;
padding: 10 20px;
border-radius: 10px;
right: 0px;
position: absolute;
bottom: 10px;
color: #fff;
}

.left_content span.number {
font-size: 38px;
border: 4px solid #fff;
padding: 10 20px;
border-radius: 10px;
left: 0px;
position: absolute;
bottom: 10px;
color: #fff;
}

#testimonials .half img {
width: 20%;
float: left;
margin-right: 20px;
border: 2px solid #fff;
box-shadow: 0px 0px 10px 2px #ccc;
}

div.vertical {
padding: 30px;
border: 1px solid #e7e7e7;
border-right: none;
float: left;
height: 351px;

}

div.half_vertical {
padding: 30px;
border: 1px solid #E7E7E7;
border-bottom: none;
float: left;
height: 145px;
}

#full_testi{
height:100%;
}

span.client_name {
float: right;
color: #6BABE7;
font-weight: 600;
font-size: 16px;
}

.vertical p {
width: 74%;
float: left;
line-height: 25px;
color: #888;
margin-top: 0;
}

.half_vertical p {
width: 74%;
float: left;
line-height: 25px;
color: #888;
margin-top: 0;
}

div.half_vertical.last {
border-bottom: 1px solid #e7e7e7;
}



/*----- Tabs -----*/
.tabs {
    width:100%;
    display:inline-block;
}

/*----- Tab Links -----*/
/* Clearfix */
.tab-links:after {
display:block;
clear:both;
content:'';
}

div#tab_heading .fa{
float: left;
font-size: 30px;
padding-top: 25px;
margin-right: 10px;
}

ul.tab-links {
margin: 0;
padding-left: 0;
height: 81px;
float: left;
} 

.tab-links li {
margin:0px;
float:left;
list-style:none;
}

.tab-title p {
margin-top: 5px;
font-weight: 300;
font-size: 14px;
}

.tab-title h2 {
margin: 0;
}

div.tab-title{
margin-top: 10px;
float: left;
width: 79%;
}

div#tab_heading {
width: 50%;
margin: 0 auto;
}

.tab-links a {
/*padding: 0px 156px;*/
display:inline-block;
border-right: 2px solid #ccc;
background:#F0F0F0;
font-size:16px;
font-weight:600;
color:#888888;
transition:all linear 0.15s;
text-decoration:none;
width: 498px;
height: 81px;
}

.tab-links a:hover {
background:#a7cce5;
text-decoration:none;
}

li.active a, li.active a:hover {
background:#fff;
color:#e05284;
}

/*----- Content of Tabs -----*/
.tab-content {
padding:0;
border-radius:3px;
/*box-shadow:-1px 1px 1px rgba(0,0,0,0.15);*/
background:#fff;
float: left;
}

.tab {
display:none;
}

.tab.active {
display:block;
}
</style>
<?php endif; ?>



<div style="clear:both"></div>
<?php $this->load->view('templates/front_footer');  ?>