

    <!-- Modal -->
<div id="myModal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top:30%">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Subscribe To Our Newsletter</h3>
  </div>
  <div class="modal-body">      
    <input type="text" id="news" style="height:30px" placeholder="Enter email address">
    <a href="javascript:void(0)" style="margin-top:-10px !important" class="btn btn-primary" id="subscribe_newsletter">Subscribe</a>
    <p id="err">Please enter valid email address.</p>
  </div>
</div>

<footer>
    <?php $langfooter = lang('footer'); ?>
    <?php $footer = get_cms_page(2); //print_r($footer); die(); ?>
    <div class="container">
        <ul class="foot">                 
            <li><a href=""><img src="<?php echo base_url() ?>assets/img/facebook-mic_mic.png"></a></li>
            <li><a href=""><img src="<?php echo base_url() ?>assets/img/twitter-mic_mic.png"></a></li>
            <li style="color:#ccc"><?php echo date('Y') ?>,Bnbclone</li>
            <li><a href="#myModal" role="button" class="" data-toggle="modal"><?php echo $langfooter['subscribe']; ?></a></li>      
            <li  class=""><a href="<?php echo base_url() ?>front/help" role="button" data-toggle="modal" ><?php echo $langfooter['help']; ?></a></li>      
            <li  class=""><a href="<?php echo base_url() ?>page/contactus" role="button" data-toggle="modal" ><?php echo $langfooter['contact_us']; ?></a></li>      
            <li  class=""><a href="<?php echo base_url() ?>properties/all_groups" role="button" data-toggle="modal" ><?php echo $langfooter['group']; ?></a></li>      
            <li  class=""><a href="<?php echo base_url() ?>properties/collection_category" role="button" data-toggle="modal" >Collection</a></li>      
             <?php if(!empty($footer)): ?>
                <?php $i=1; foreach($footer as $foot): ?>    
                <?php if($foot->slug != 'guesthost'): ?>                    
                    <li><a href="<?php  echo base_url(); ?>page/cms_page/<?php echo $foot->slug;  ?>"><?php echo $foot->page_title;  ?></a></li>
                <?php endif; ?>
                <?php $i++; endforeach; ?>
            <?php endif; ?> 
        <!-- nobrdr -->
        </ul>
    </div>
</footer>

</body> 
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-transition.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-alert.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-dropdown.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-scrollspy.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-tab.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-tooltip.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-popover.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-button.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-collapse.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-carousel.js"></script>
    <script src="<?php echo base_url()?>assets/front_end_theme/js/bootstrap-typeahead.js"></script>

    <script>
    $('.carousel').carousel();
    $('.carousel').carousel({
    interval:1000
    });

     $('#subscribe_newsletter').click(function(){   
        $(this).hide();
        var email = $('#news').val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;        
        if(reg.test(email)==false){ 
            $('#err').show();
            $('#subscribe_newsletter').show();
            return false;
        }else{
            $('#err').show();
            $('#err').css('color','black');
            $('#err').html('please wait ...');
        }

        $.ajax({
            type:'POST',
            data:{email:email},
            url:'<?php echo base_url() ?>front/ajax_subscribe',
            success:function(res){
                if(res =="1"){
                    $('#err').show();
                $('#err').css('color','green');
                $('#err').html('Email has been sent to You please confirm your email');
                }else{
                    $('#err').show();
                    $('#err').css('color','red');
                    $('#err').html('Something went wrong please try again.');
                    $('#subscribe_newsletter').show();
                }
            }
        });
    });
    </script>
    <style type="text/css">

     #err{
        color: red;
        font-size: 14px;
        display: none;
    }
    footer{
        background-color: black;
        /*position: fixed;*/
        width: 100%;
        color: white;
        font-size: 14px;
    }

    ul.foot li{
        display: inline;
        list-style: none;
        padding-left: 5px;
        padding-right: 5px;  
        border-right: 1px solid #ccc;
    }
    .nobrdr{
        border-right:0px !important;
    }

    ul.foot li a{
        color: white;
    }

    ul.foot{
        margin-top: 10px;
    }

    .img-content2{
        margin-left: 8%;
    }

    .img-content1{
        margin-left: 5.5%;
    }

    .img-content3{
        margin-left: 7%;
    }

    .text4 , .text8 , .text5{
        font-size: 24px;
        color:#989D9F;
    }

    .text8{
        /*margin-top: 51px !important;*/
        /*color:#989D9F;*/
    }

    .text6{
        margin-top: 10%;
    }

    .txt02{
        margin-top: 5%;
    }
    .txt01{
        margin-top: 8%;
        /*margin-left: 2%;*/

    }

    .mobile11 .text3{
        margin-top: 11%;
    }

    .middle-content{
        margin: 0 auto;
        text-align: center;
    }

    .form1{
        padding-left: 5px;
        padding-right: 5px;
        margin-left: 9.5%;
height: 50px !important;

    }

    .formcontent{
        margin: 0 auto;
    }

    .mobile10 img{
        margin-top: 5px;
    } 

    .text7{
        font-family: 'Cabin';
        font-weight: normal;
        font-size: 26px;
    }

    .btm-comtent{
        text-align: center;
    }

    .text10{
        font-family: 'Cabin';
        vertical-align: middle;
    }

    .bg6{
        margin-top: 28px;
    }

    .text9{
        padding-top: 10px !important;
    }

    

    .list2 li{
        padding-left: 10px;
        padding-right: 10px;
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .list2 li a{
        font-size: 16px;
    }

    .bg10{
        margin-top: 10px;
         margin-bottom: -5px;
    }

.bottle1{
        height: 40px !important;
    }

.btn .mobile1{
height:37px !important;
}

.scond_row{
    margin-top: 2.5%;
}

.bg6{
    margin-left: 10.5%;
    padding-bottom: 3.5%;
}

</style>




<style type="text/css">
    
/*.tooltip-inner {
background:#DE4980 !important ;
opacity: .9 !important;
border-radius: 5px !important;
color:white !important;
border:none !important;
}*/

.icon-question-sign
{
  cursor: help !important;
}


/*.arrow {
width: 40px !important;
height: 16px !important;
overflow: hidden !important;
position: absolute !important;
left: 50% !important;
margin-left: -35px !important;
bottom: -16px !important;
}
*/
</style>



<script>
$(function() {
$('.icon-question-sign').tooltip(
   {
    show:
    {
    effect: "slideDown",
    delay: 250,
    }
   }

    );
});
</script>
</html>