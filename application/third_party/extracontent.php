<?php 

/*video and blog category*/
	
	// public function categories($offset=0){
	// 	$this->check_login();
	// 	$limit=10;
	// 	$data['categories']=$this->admin_model->categories($limit,$offset);
	// 	$config= get_theme_pagination();	
	// 	$config['base_url'] = base_url().'admin/categories/';
	// 	$config['total_rows'] = $this->admin_model->categories(0, 0);
	// 	$config['per_page'] = $limit;
	// 	$config['num_links'] = 5;		
	// 	$this->pagination->initialize($config); 		
	// 	$data['pagination'] = $this->pagination->create_links();		

 //        $data['template'] = 'admin/categories';
 //        $this->load->view('templates/admin_template', $data);			
	// }	

	// public function add_category(){
	// 	$this->check_login(); 
	// 	$this->form_validation->set_rules('category', 'category', 'required');			
	// 	$this->form_validation->set_rules('type', 'Type', 'required');			
	// 	$this->form_validation->set_rules('excerpt', 'excerpt', 'required');			
		
	// 	if(isset($_POST['type']))
	// 		if($_POST['type'] == 2)
	// 			$this->form_validation->set_rules('rel_video', 'Related video link', 'required');			
		
	// 	if ($this->form_validation->run() == TRUE){			
	// 		$data=array(
	// 			'category_name'=>$this->input->post('category'),				
	// 			'type'=>$this->input->post('type'),
	// 			'excerpt'=>$this->input->post('excerpt'),
	// 			'related_video_post_id'=>$this->input->post('rel_video'),
	// 			'created' => date('Y-m-d H:i:s')		
	// 		);

	// 		if($_FILES['thumbnail']['name'] !=""){ 
	// 			$data['thumbnail'] = $this->do_core_upload('thumbnail');
	// 			if(!$data['thumbnail']){
	// 				$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
	// 				redirect('admin/add_category');
	// 			}
	// 		}

	// 		if($_FILES['featured_image']['name'] !=""){ 
	// 			$data['featured_image'] = $this->do_core_upload('featured_image');
	// 			if(!$data['featured_image']){
	// 				$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
	// 				redirect('admin/add_category');
	// 			}
	// 		}
			
	// 		$this->admin_model->insert('post_categories',$data);		
	// 		$this->session->set_flashdata('success_msg',"Category has been added successfully.");
	// 		redirect('admin/categories');
	// 	}
	// 	$data['video'] = $this->admin_model->get_result('posts', array('post_type' => 2));		
	// 	$data['template'] = 'admin/add_category';
 //        $this->load->view('templates/admin_template', $data);		
	// }

	// public function edit_category($id){
	// 	$this->check_login(); 
	// 	$this->form_validation->set_rules('category', 'category', 'required');	
	// 	$this->form_validation->set_rules('type', 'Type', 'required');	
	// 	$this->form_validation->set_rules('excerpt', 'excerpt', 'required');			
		
	// 	if(isset($_POST['type']))
	// 		if($_POST['type'] == 2)
	// 			$this->form_validation->set_rules('rel_video', 'Related video link', 'required');			
		
	// 	if ($this->form_validation->run() == TRUE){			
	// 		$data=array(
	// 			'category_name'=>$this->input->post('category'),								
	// 			'type'=>$this->input->post('type'),	
	// 			'excerpt'=>$this->input->post('excerpt'),
	// 			'related_video_post_id'=>$this->input->post('rel_video'),			
	// 		);	

	// 		if($_FILES['thumbnail']['name'] !=""){ 
	// 			$data['thumbnail'] = $this->do_core_upload('thumbnail');
	// 			if(!$data['thumbnail']){
	// 				$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
	// 				redirect('admin/edit_category/'.$id);
	// 			}
	// 		}

	// 		if($_FILES['featured_image']['name'] !=""){ 
	// 			$data['featured_image'] = $this->do_core_upload('featured_image');
	// 			if(!$data['featured_image']){
	// 				$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
	// 				redirect('admin/edit_category/'.$id);
	// 			}
	// 		}					

	// 		$this->admin_model->update('categories',$data, array('id'=>$id));		
	// 		$this->session->set_flashdata('success_msg',"Category has been updated successfully.");
	// 		redirect('admin/categories');
	// 	}
	// 	$data['video'] = $this->admin_model->get_result('posts', array('post_type' => 2));
	// 	$data['category'] = $this->admin_model->get_row('categories', array('id'=> $id));
	// 	$data['template'] = 'admin/edit_category';
 //        $this->load->view('templates/admin_template', $data);		
	// }

/*video & blog category*/

/*Video Posts*/
	/*
	public function video_posts($offset=0){				
		$this->check_login();
		$limit=10;
		$data['video_posts']=$this->admin_model->video_posts($limit,$offset);
		$config= get_theme_pagination();	
		$config['base_url'] = base_url().'admin/video_posts/';
		$config['total_rows'] = $this->admin_model->video_posts(0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();
		
		$data['template'] = 'admin/video_posts';
        $this->load->view('templates/admin_template', $data);		
	}

	public function add_video_post(){		
		$this->check_login();

		$data['categories'] = $this->admin_model->get_result('categories', array('type'=>'2'));
		// print_r($data['categories']);
		// die();
		$info = $this->session->userdata('UserInfo');
		$this->form_validation->set_rules('post_title', 'Post Title', 'required');
		$this->form_validation->set_rules('post_excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('post_content', 'Post Content', 'required');
		$this->form_validation->set_rules('post_category', 'Post Category', 'required');
		// $this->form_validation->set_rules('video_url', 'Video Embedding URL', 'xss_clean');
		$this->form_validation->set_rules('video_type', 'Type', 'required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');	
			
		if ($this->form_validation->run() == TRUE){
			$post_data=array(
				'author_id'  => $info['id'],				
				'content' => $this->input->post('post_content'),				
				'post_title'   => $this->input->post('post_title'),
				'post_status'  => $this->input->post('post_status'),		
				'category_id'  => $this->input->post('post_category'),
				'post_excerpt'  => $this->input->post('post_excerpt'),
				'video_url'  => $this->input->post('video_url'),
				'channel_id'  => $this->input->post('channel_id'),
				'video_type'  => $this->input->post('video_type'),			
				'created'  		=> date('Y-m-d h:i:s'),			
				'post_type'    => 2);
			
			if($_FILES['userfile']['name'] !=""){
				$post_data['post_image'] = $this->do_core_upload('userfile');
				if(!$post_data['post_image']){
					$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
					redirect('admin/add_video_post');
				}
				else{
					$this->create_thumb($post_data['post_image'], './assets/uploads/custom_uploads',260 , 260 );
				}
			}

			$post_id = $this->admin_model->insert('posts',$post_data);
			$this->session->set_flashdata('success_msg','Post published.');						
			redirect('admin/video_posts');
		}

		$data['template'] = 'admin/add_video_post';
        $this->load->view('templates/admin_template', $data);		
	}

	public function edit_video_post($post_id=''){		
		$this->check_login(); 
		if(empty($post_id)) redirect('admin/video_posts');

		$data['video_post']=$this->admin_model->get_row('posts',array('id'=>$post_id));
		$data['categories'] = $this->admin_model->get_result('categories', array('type'=>'2'));

		// print_r($data['categories']);
		// die();
		$info = $this->session->userdata('UserInfo');
		$this->form_validation->set_rules('post_title', 'Post Title', 'required');
		$this->form_validation->set_rules('post_excerpt', 'Excerpt', 'required');
		$this->form_validation->set_rules('post_content', 'Post Content', 'required');
		$this->form_validation->set_rules('post_category', 'Post Category', 'required');
		// $this->form_validation->set_rules('video_url', 'Video Embedding URL', 'xss_clean');
		$this->form_validation->set_rules('video_type', 'Type', 'required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');	
			
		if ($this->form_validation->run() == TRUE){
			$post_data=array(				
				'content' => $this->input->post('post_content'),				
				'post_title'   => $this->input->post('post_title'),
				'post_status'  => $this->input->post('post_status'),		
				'category_id'  => $this->input->post('post_category'),
				'post_excerpt'  => $this->input->post('post_excerpt'),				
				'video_type'  => $this->input->post('video_type'),							
				'post_type'    => 2);

			$video_type  = $this->input->post('video_type');
			$channel_id	= $this->input->post('channel_id');

			if($video_type == 1){
				$post_data['video_url'] = $this->input->post('video_url');
			}
			if($video_type == 2){
				$post_data['video_url'] = $this->input->post('video_url');
			}
			if($video_type = 3){
				$post_data['channel_id'] = $channel_id;	
			}

			if($_FILES['userfile']['name'] !=""){
				$post_image = $this->do_core_upload('userfile');
				if(!$post_image){
					$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
					redirect('admin/edit_video_post/'.$post_id);
				}else{
					if(!empty($data['video_post']->post_image)){
						$path = './assets/uploads/custom_uploads/';
						$delete_file = $data['video_post']->post_image;
						@unlink($path.$delete_file);
						@unlink($path.'thumbs/'.$delete_file);
						}
					$post_data['post_image'] = $post_image;
					$this->create_thumb($post_data['post_image']);
				}
			}
			// print_r($post_data); die();
			$post_id = $this->admin_model->update('posts',$post_data, array('id'=>$post_id));
			$this->session->set_flashdata('success_msg','Post updated successfully.');						
			redirect('admin/video_posts');
		}
		
		// print_r($data['posts']);
		// die();
		$data['template'] = 'admin/edit_video_post';
        $this->load->view('templates/admin_template', $data);		
	}

	public function delete_video_post($post_id=''){		
		$this->check_login(); 
		if(empty($post_id)) redirect('admin/posts');
		
		if($this->admin_model->delete('posts',array('id'=>$post_id)))
			$this->session->set_flashdata('success_msg','Post deleted.');

		redirect('admin/video_posts');
	}

	*/
	

/*Video Posts*/

/*Artists

	public function artist($offset=0){				
		$this->check_login();
		$limit=10;
		$data['artist']=$this->admin_model->artist($limit,$offset);
		$config= get_theme_pagination();	
		$config['base_url'] = base_url().'admin/artist/';
		$config['total_rows'] = $this->admin_model->artist(0, 0);
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;		
		$this->pagination->initialize($config); 		
		$data['pagination'] = $this->pagination->create_links();
		
		$data['template'] = 'admin/artist';
        $this->load->view('templates/admin_template', $data);		
	}

	public function add_artist(){		
		$this->check_login();
		
		$info = $this->session->userdata('UserInfo');
		$this->form_validation->set_rules('name', 'Name', 'required');		
		$this->form_validation->set_rules('description', 'descriptiom', 'required');	
		$this->form_validation->set_rules('video_type', 'Type', 'required');
		$this->form_validation->set_rules('video_url', 'Video URL', 'required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');	
			
		if ($this->form_validation->run() == TRUE){
			$post_data=array(				
				'description' => $this->input->post('description'),				
				'name'   => $this->input->post('name'),
				'video_type'  => $this->input->post('video_type'),		
				'video_url'  => $this->input->post('video_url'),
				'created'  		=> date('Y-m-d h:i:s'),							
				);
			
			if($_FILES['userfile']['name'] !=""){
				$post_data['artist_image'] = $this->do_core_upload('userfile');
				if(!$post_data['artist_image']){
					$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
					redirect('admin/add_artist');
				}
				else{
					$this->create_thumb($post_data['artist_image'], './assets/uploads/custom_uploads',260 , 260 );
				}
			}

			$post_id = $this->admin_model->insert('artist',$post_data);
			$this->session->set_flashdata('success_msg','Artist Added.');						
			redirect('admin/artist');
		}

		$data['template'] = 'admin/add_artist';
        $this->load->view('templates/admin_template', $data);		
	}

	public function edit_artist($post_id=''){		
		$this->check_login(); 
		if(empty($post_id)) redirect('admin/artist');

		$data['artist']=$this->admin_model->get_row('artist',array('id'=>$post_id));		

		$this->form_validation->set_rules('name', 'Name', 'required');		
		$this->form_validation->set_rules('description', 'descriptiom', 'required');	
		$this->form_validation->set_rules('video_type', 'Type', 'required');
		$this->form_validation->set_rules('video_url', 'Video URL', 'required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');	
			
		if ($this->form_validation->run() == TRUE){
			$post_data=array(				
				'description' => $this->input->post('description'),				
				'name'   => $this->input->post('name'),
				'video_type'  => $this->input->post('video_type'),		
				'video_url'  => $this->input->post('video_url'),
				'created'  		=> date('Y-m-d h:i:s'),							
				);

			$video_type  = $this->input->post('video_type');		

			if($video_type == 1){
				$post_data['video_url'] = $this->input->post('video_url');
			}
			if($video_type == 2){
				$post_data['video_url'] = $this->input->post('video_url');
			}
			

			if($_FILES['userfile']['name'] !=""){
				$artist_image = $this->do_core_upload('userfile');
				if(!$artist_image){
					$this->session->set_flashdata('error_msg', 'Some problem in uploading your file');
					redirect('admin/edit_artist/'.$post_id);
				}else{
					if(!empty($data['artist']->artist_image)){
						$path = './assets/uploads/custom_uploads/';
						$delete_file = $data['artist']->artist_image;
						@unlink($path.$delete_file);
						@unlink($path.'thumbs/'.$delete_file);
						}
					$post_data['artist_image'] = $artist_image;
					$this->create_thumb($post_data['artist_image']);
				}
			}
			
			$post_id = $this->admin_model->update('artist',$post_data, array('id'=>$post_id));
			$this->session->set_flashdata('success_msg','Artist Updated.');						
			redirect('admin/artist');
		}		
		
		$data['template'] = 'admin/edit_artist';
        $this->load->view('templates/admin_template', $data);		
	}

	public function delete_artist($artist_id=''){		
		$this->check_login(); 
		if(empty($artist_id)) redirect('admin/artist');
		
		if($this->admin_model->delete('artist',array('id'=>$artist_id)))
			$this->session->set_flashdata('success_msg','Artist deleted.');

		redirect('admin/artist');
	}


*/