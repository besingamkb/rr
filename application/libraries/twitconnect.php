<?php

/**
 * Eugene Poberezkin, 2012.
 *
 * License: CC BY 3.0 (http://creativecommons.org/licenses/by/3.0/)
 *
 * This license means you can do almost whatever you like with it
 * attributing it to the original source.
 *
 *---------------------------------------------------------------
 * File /application/libraries/twconnect.php
 * 
 * Twconnect library for Codeigniter v.0.1
 *---------------------------------------------------------------
 *
 * Encapsulates TwitterOAuth library by Abraham Williams - https://github.com/abraham/twitteroauth
 * TwitterOAuth library (only twitteroauth folder from github) should be located in /application/libraries/twitteroauth/ folder
 *
 * Simplifies Twitter login/signup/access
 *
 *
 * Class variables
 * $tw_user_id - Twitter user ID
 * $tw_user_name - Twitter user screen name
 * $tw_user_info - the whole bunch of info about user
 *      twaccount_verify_credentials() should be called to set it
 * $tw_config - copy of the configuration file application/config/twitter.php
 * $tw_request_token - temporary request token, array
 * $tw_access_token - permanent access token for the currently logged in user.
 *		It should be stored in the database.
 *		At least one of the previous two variables will be null.
 * $tw_status - Twitter user status
 *		'old_token' - request token expired, should be redirected to Twitter again
 *		'access_error' - could not connect to Twitter to get access token
 *		'verified' - user verified
 *
 *
 * Class functions:
 *    1. public function Twconnect() - Twconnect class constructor
 *       It handles creation of the TwitterOAuth object for all 3 stages of authentication process
 *
 *    2. public function twredirect() - redirects to Twitter for authetication
 *    3. public function twprocess_callback() - requests permanent access token after callback
 *    4. public function tw_get($url, $parameters) - GET request to Twitter API
 *    5. public function tw_post($url, $parameters) - POST request to Twitter API
 *	     Both tw_get and tw_post check if user is verified and then call get and post
 *       of TwitterOAuth class, if not verified - return false
 *
 *    6. public function twaccount_verify_credentials() - gets all Twitter user info
 *       and saves it to $this->tw_user_info
 *
 *    All GET and POST requests are here https://dev.twitter.com/docs/api/1.1
 *
 *
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'libraries/twitteroauth/config.php';
require_once APPPATH.'libraries/twitteroauth/twitteroauth.php';

class Twitconnect extends TwitterOAuth {

	public function __construct()
	{
		parent::__construct(CONSUMER_KEY, CONSUMER_SECRET);			
		clear_cache();
	}

	public function check_config()
	{
		/**
		 * @file
		 * Check if consumer token is set and if so send user to get a request token.
		 */

		/**
		 * Exit with an error message if the CONSUMER_KEY or CONSUMER_SECRET is not defined.
		 */
		if (CONSUMER_KEY === '' || CONSUMER_SECRET === '' || CONSUMER_KEY === '' || CONSUMER_SECRET === '') {
		  return array('status' => false,'msg' => 'You need a consumer key and secret to process this execution code.');
		}else
		  return array('status' => true);
		/* Build an image link to start the redirect process. */
		// $content = '<a href="'.base_url().'welcome/redirectmethod"><img src="'.base_url().'assets/img/third_party/lighter.png" alt="Sign in with Twitter"/></a>';





		/* Include HTML to display on the page */
		// include('html.inc');
		// $data['content'] = $content;
		// $this->load->view('front/twitter', $data);
	}

	public function redirectmethod()
	{
		$ci =& get_instance();
		/* Build TwitterOAuth object with client credentials. */

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
		 
		/* Get temporary credentials. */
		$request_token = $connection->getRequestToken(OAUTH_CALLBACK);

		/* Save temporary credentials to session. */
		$auth = array(
                   'oauth_token'  		=> $request_token['oauth_token'],
                   'oauth_token_secret' => $request_token['oauth_token_secret'],
               );
		$token = $request_token['oauth_token'];
		$ci->session->set_userdata('auth', $auth);
		// $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
		// $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
		 
		/* If last connection failed don't display authorization link. */
		switch ($connection->http_code) {
		  case 200:
		    /* Build authorize URL and redirect user to Twitter. */
		    $url = $connection->getAuthorizeURL($token);
		    // redirect($url);
		    return array('status' => true, 'url' => $url);
		    // header('Location: ' . $url); 
		    break;
		  default:
		    /* Show notification if something went wrong. */
		    return array('status' => false, 'msg' => 'Could not connect to Twitter. Refresh the page or try again later.');
		}
	}

	public function callback()
	{
		/* If the oauth_token is old redirect to the connect page. */
		$ci =& get_instance();
		$auth = $ci->session->userdata('auth');
		if (isset($_REQUEST['oauth_token']) && $auth['oauth_token'] !== $_REQUEST['oauth_token']) {
		  $auth['oauth_status'] = 'oldtoken';
		  $ci->session->set_userdata('auth', $auth);
		  // header('Location: ./clearsessions.php');
		  return $this->clearsessions();
		}

		/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $auth['oauth_token'], $auth['oauth_token_secret']);

		/* Request access tokens from twitter */
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$auth['access_token'] = $access_token;

		/* Remove no longer needed request tokens */
		$auth['oauth_token'] = '';
		$auth['oauth_token_secret'] = '';

		/* If HTTP response is 200 continue otherwise send to connect page to retry */
		if (200 == $connection->http_code) {
		  /* The user has been verified and the access tokens can be saved for future use */
		  $auth['status'] = 'verified';
		  $ci->session->set_userdata('auth', $auth);
		  // header('Location: ./index.php');
		  return $this->result();
		} else {
		  /* Save HTTP status for error dialog on connnect page.*/
		  // header('Location: ./clearsessions.php');
		  return $this->clearsessions();
		}
	}

	public function result()
	{
		$ci =& get_instance();
		// $this->load->view('welcome_message');
		/* If access tokens are not available redirect to connect page. */
		$auth = $ci->session->userdata('auth');
		if (empty($auth['access_token']) || empty($auth['access_token']['oauth_token']) || empty($auth['access_token']['oauth_token_secret'])) {
		    // header('Location: ./clearsessions.php');
		    return $this->clearsessions();
		}
		/* Get user access tokens out of the session. */
		$access_token = $auth['access_token'];

		/* Create a TwitterOauth object with consumer/user tokens. */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

		/* If method is set change API call made. Test is called by default. */
		$content = $connection->get('account/verify_credentials');
		$ci->session->set_userdata('auth', '');
		$ci->session->unset_userdata('auth');
		return $content;
		/* Some example calls */
		//$connection->get('users/show', array('screen_name' => 'abraham'));
		//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
		//$connection->post('statuses/destroy', array('id' => 5437877770));
		//$connection->post('friendships/create', array('id' => 9436992));
		//$connection->post('friendships/destroy', array('id' => 9436992));
		/* Include HTML to display on the page */
		// include('html.inc');
	}

	public function clearsessions($value='')
	{
		$ci =& get_instance();
		/**
		 * @file
		 * Clears PHP sessions and redirects to the connect page.
		 */
		 
		/* Load and clear sessions */
		$ci->session->set_userdata('auth', '');
		$ci->session->unset_userdata('auth');
		 
		/* Redirect to page with the connect to Twitter option. */
		// header('Location: ./connect.php');
		return false;
	}

}

?>