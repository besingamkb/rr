<?php if( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );


// Set the server api endpoint and http methods as constants
define( 'PAYPAL_ENVIRONMENT', 'sandbox' );  // or 'beta-sandbox' or 'live'


class Paypal_refund{

	private $_conf = NULL;	
	
	public function __construct() {
		$this->CI =& get_instance();
		// Set up your API credentials, PayPal end point, and API version.
		// $config['Paypal_API_UserName'] = urlencode('faheem.test-facilitator_api1.yahoo.com');		// api_username
		// $config['Paypal_API_Password'] = urlencode('1368631028');		// api_password
		// $config['Paypal_API_Signature'] = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AWYWwiwpIPCG1KF1F84KurPnE.z2');	//api_signature
		// echo "USER: ".$api_username;
		// die();

		// $config['Paypal_API_UserName'] = urlencode('chris_api1.baitup.com');		// api_username
		// $config['Paypal_API_Password'] = urlencode('1378912120');		// api_password
		// $config['Paypal_API_Signature'] = urlencode('Am0MeqHpCcNip1tiTDLwIXqMVB8lAa8D4jw0wt4KEu5JUBIvASnUAj9I');	//api_signature
		// $this->CI->_conf = $config;
	}

	public function paypal_cofig($api_username='', $api_password='', $api_signature='', $api_environment='', $api_endpoint='', $api_version='')
	{
		$config['Paypal_API_UserName'] = urlencode($api_username);		// api_username
		$config['Paypal_API_Password'] = urlencode($api_password);		// api_password
		$config['Paypal_API_Signature'] = urlencode($api_signature);	//api_signature
		$config['PAYPAL_ENVIRONMENT'] = $api_environment;	//api_ENVIRONMENT
		$config['PAYPAL_ENDPOINT'] = $api_endpoint; //api_ENDPOINT
		$config['PAYPAL_VERSION'] = urlencode($api_version); //api_VERSION
		$this->CI->_conf = $config;
	}

	public function post_to_paypal($methodName_, $nvpStr_) {	
		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === $this->CI->_conf['PAYPAL_ENVIRONMENT'] || "beta-sandbox" === $this->CI->_conf['PAYPAL_ENVIRONMENT']) {
			$API_Endpoint = "https://api-3t.".$this->CI->_conf['PAYPAL_ENVIRONMENT'].".paypal.com/nvp";
		}
		$version = $this->CI->_conf['PAYPAL_VERSION'];

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=".$this->CI->_conf['Paypal_API_Password']."&USER=".$this->CI->_conf['Paypal_API_UserName']."&SIGNATURE=".$this->CI->_conf['Paypal_API_Signature']."$nvpStr_";

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}

		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		return $httpParsedResponseAr;
	}


	public function refund_paypal_amount($transaction_id='', $refund_type='Full', $amount='', $memo='', $currency='USD'){
        
        // Set request-specific fields.
		$transactionID = urlencode($transaction_id); 
		$refundType = urlencode($refund_type);						// or 'Partial'
		//$amount;												// required if Partial.
		//$memo;													// required if Partial.
		$currencyID = urlencode($currency);							// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

		// Add request-specific fields to the request string.
		$nvpStr = "&TRANSACTIONID=$transactionID&REFUNDTYPE=$refundType&CURRENCYCODE=$currencyID";

		if(isset($memo)) {
			$nvpStr .= "&NOTE=$memo";
		}

		if(strcasecmp($refundType, 'Partial') == 0) {
			if(!isset($amount)) {
				exit('Partial Refund Amount is not specified.');
			} else {
		 		$nvpStr = $nvpStr."&AMT=$amount";
			}

			if(!isset($memo)) {
				exit('Partial Refund Memo is not specified.');
			}
		}
		//print_r($nvpStr); die;
		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = $this->post_to_paypal('RefundTransaction', $nvpStr);

		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
			//exit('Refund Completed Successfully: '.print_r($httpParsedResponseAr, true));
			return $httpParsedResponseAr;
		} else  {
			//exit('RefundTransaction failed: ' . print_r($httpParsedResponseAr, true));
			return $httpParsedResponseAr;
		}		
	}
}