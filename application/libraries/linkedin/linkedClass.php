<?php
class LinkedClass {


    public function linkedinGetUserInfo( $requestToken='', $oauthVerifier='', $accessToken='',$key="",$secret=""){
        require_once APPPATH.'libraries/linkedin/linkedinoAuth.php';
        $linkedin = new linkedinoAuth($key, $secret);
        $linkedin->request_token    =   unserialize($requestToken); //as data is passed here serialized form
        $linkedin->oauth_verifier   =   $oauthVerifier;
        $linkedin->access_token     =   unserialize($accessToken);

        try{
            $xml_response = $linkedin->getProfile("~:(id,first-name,last-name,interests,publications,patents,languages,skills,date-of-birth,email-address,phone-numbers,im-accounts,main-address,twitter-accounts,headline,picture-url,public-profile-url)");
        }
        catch (Exception $o){
            print_r($o);
        }
        return $xml_response;
    }
}
?>
