<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
* Blog Library
* Auther: Joe Parihar
*/

class Acapella_blog{
  private $CI;
  protected $post_table_name;
  protected $q;

  
  protected $theme_query;
  protected $theme_row;
  protected $theme_result;

  protected $menu_query;
  protected $menu_row;
  protected $menu_result;

  protected $post_query;
  protected $post_row;
  protected $post_result;
  protected $posts_per_page;

  protected $page_template_query;
  protected $page_template_row;
  protected $page_template_result;
   

  protected $option_query;
  protected $option_row;
  protected $option_result;
  protected $option_name_search;

  protected $segment_one = '';
  protected $segment_two = '';
  protected $segment_three = '';
  protected $destination_path = '';


  protected $data = array();
  

  public function __construct(){
    $this->CI = &get_instance();   

    $this->CI->segment_one   = $this->CI->uri->segment(1);  
    $this->CI->segment_two   = $this->CI->uri->segment(2);  
    $this->CI->segment_three = $this->CI->uri->segment(3);

   
    /* theme query */     
    $this->CI->theme_query = $this->CI->db->get_where('themes',array('is_active'=>1)); 
    if($this->CI->theme_query->num_rows()>0){
      $this->CI->theme_row    = $this->CI->theme_query->row();  
      $this->CI->theme_result = $this->CI->theme_query->result();  
    }else{
      $this->CI->theme_row    = FALSE;       
    }    
    /* end theme query */

    $this->CI->option_query = $this->CI->db->get_where('options',array('autoload'=>'yes')); 
    if($this->CI->option_query->num_rows()>0){
      $this->CI->option_row    = $this->CI->option_query->row();  
      $this->CI->option_result = $this->CI->option_query->result();  
    }else{
      $this->CI->option_row    = FALSE;  
      $this->CI->option_result = FALSE;
    }

    $this->CI->posts_per_page = $this->option_name_search('posts_per_page');    
    $show_on_front = $this->option_name_search('show_on_front');
    $page_on_front = $this->option_name_search('page_on_front');
    $page_for_posts = $this->option_name_search('page_for_posts');

    
    if($show_on_front==='posts')
      $show_on_front = substr($show_on_front, 0, -1);    

    /*if(!empty($page_for_posts) && $page_for_posts != 0):
      $q_p_f_p=$this->CI->db->get_where('posts',array('ID'=>$page_for_posts));
      if($q_p_f_p->num_rows()>0)
        $page_for_posts = $q_p_f_p->row()->post_name;
      else
        $page_for_posts=0;
    endif; */

    if(!empty($this->CI->segment_one)):
        $this->CI->page_template_query=$this->CI->db->get_where('posts',array('post_name'=>$this->CI->segment_one));
      
        if($this->CI->page_template_query->num_rows()>0)
          $this->CI->page_template_row    = $this->CI->page_template_query->row();        
        else
          $this->CI->page_template_row    = FALSE;    

      else:
        $this->CI->page_template_query=$this->CI->db->get_where('posts',array('ID'=>$page_on_front));
        if($this->CI->page_template_query->num_rows()>0)
          $this->CI->page_template_row    = $this->CI->page_template_query->row();  
        else
          $this->CI->page_template_row    = FALSE; 
    endif;  





   
    // echo $page_for_posts; // blog page

     if(empty($this->CI->segment_one) && $show_on_front!=FALSE){      
      if($this->option_name_search('page_on_front')!='0')
          $this->CI->db->where('ID',$this->option_name_search('page_on_front'));      
       // echo $show_on_front;
        $this->CI->db->where('post_type',$show_on_front); 

      } elseif(!empty($this->CI->segment_one) && strtolower($this->CI->segment_one) !='blog' && strtolower($this->CI->segment_one) !='blogs' ){
      
       $this->CI->db->where('post_name',$this->CI->segment_one);


      }elseif(!empty($this->CI->segment_one) && strtolower($this->CI->segment_one) =='blog' || strtolower($this->CI->segment_one) =='blogs'){        

        $this->CI->db->where("post_date <=",date('Y-m-d').'12:59:59');

      if($this->CI->segment_two !='' && $this->CI->segment_two != 'page'){
        $this->CI->db->where('post_name',$this->CI->segment_two);
      }else{

      if($this->CI->uri->segment(3) =='')
        $this->CI->post_offset=0;
      else
        $this->CI->post_offset = $this->CI->uri->segment(3);

        $this->CI->db->where('post_type','post');      
      

      if($this->CI->posts_per_page >0 && $this->CI->post_offset >=0):       
          $this->CI->db->limit($this->CI->posts_per_page,$this->CI->post_offset);
       endif;       
        
      }
 
    }
    $this->CI->db->order_by('ID','desc');
    $this->CI->db->where('post_status','publish');
    $this->CI->post_query = $this->CI->db->get('posts'); 
   
    if($this->CI->post_query->num_rows()>0){
      $this->CI->post_row    = $this->CI->post_query->row();  
      $this->CI->post_result = $this->CI->post_query->result();       
    }else{
      $this->CI->post_row    = FALSE;  
      $this->CI->post_result = FALSE;
    }
   

    $filename = "./assets/themes/".$this->CI->session->userdata('current_theme')."/funcations.php";

    if (file_exists($filename)) {
        include_once ($filename);
    } 

   
  }

  public function get_current_theme(){    
    if($this->CI->theme_row):
        $this->CI->session->set_userdata('current_theme',$this->CI->theme_row->dir_path);
        return trim($this->CI->theme_row->dir_path);
    else:
        return FALSE;
      endif;
  }


  public function post_pagination($pagination=array()){
   
   if($this->CI->segment_two=='' || $this->CI->segment_two=='page'): 
      $qp=$this->CI->db->get_where('posts',array('post_type'=>'post','post_status'=>'publish'));
   
    //$this->CI->load->library('pagination');
      if(!empty($pagination))
      $config = $pagination;  
      $config['base_url'] = base_url().$this->CI->segment_one.'/page/';
      $config['total_rows'] = $qp->num_rows();
      $config['per_page'] = $this->CI->posts_per_page;
      $config['num_links'] = 5;
      //$config['use_page_numbers'] = FALSE; 
      //$config['page_query_string'] = TRUE;
      //$config['prev_link'] = 'Previous';
      //$config['next_link'] = 'Next';
     // $config['display_pages'] = FALSE; 
      $this->CI->pagination->initialize($config);     
      return $this->CI->pagination->create_links();
    else:
    return FALSE;
    endif;   

  }

 public function option_name_search($key_name='') {    
    $this->CI->option_name_search=array();  
    if(!empty($this->CI->option_result)):
      foreach ($this->CI->option_result as $i => $v)
        $this->CI->option_name_search[base64_encode($v->option_value."##".$v->option_name)] = trim($v->option_name);    
    endif;
    //print_r($this->CI->option_name_search);
    if(!empty($key_name)):   
      if($key=array_search($key_name, $this->CI->option_name_search)):
        $keys_arr=explode('##',base64_decode($key));
        return $keys_arr[0];
      else:
        return FALSE;
      endif;
    endif;         
  }

  /*public function option_name_search($key_name='') {    
    $this->CI->option_name_search=array();
    if(!empty($this->CI->option_result)):
      foreach ($this->CI->option_result as $i => $v)
        $this->CI->option_name_search[base64_encode($v->option_value)] = trim($v->option_name);    
    endif;
    if(!empty($key_name)):    
      if($key=array_search($key_name, $this->CI->option_name_search))
        return base64_decode($key);
      else
        return FALSE;
    endif;         
  }*/

  public function blog_info($blogname='blogname') {
     return $this->option_name_search($blogname);     
  }

  public function get_top_menu(){
     $this->CI->qu = $this->CI->db->select('post_title as menu_name, post_name as menu_link')->get_where('posts',array('post_type'=>'page'));

    if($this->CI->qu->num_rows()>0)
      return $this->CI->qu->result();
    else
      return FALSE;  
  }

  public function page_title() {
    if($this->CI->post_row)
      return $this->CI->post_row->post_title;
  }

  public function page_content() {
    if($this->CI->post_row)
    return $this->CI->post_row->post_content;
  }

  public function posts() {  
    if($this->CI->post_result)
      return $this->CI->post_result;
    else
      return FALSE;
  }

 /* public function the_post() {
    if($this->CI->post_result)
    return $this->CI->post_result->post_content;
  }*/

  public function get_current_page_template(){
   //  print_r( $this->CI->page_template_row);
    //if($this->CI->page_template_row):
      if(!empty($this->CI->page_template_row)):
       // echo "OKKK";
        $p_t=explode('.php',$this->CI->page_template_row->page_template);
        return $p_t[0];
      else:
       // echo "ERRR";
        return FALSE;  
      endif;
    //else:
     //  return FALSE;  
     //endif;
  }

  public function menus($slug='') {  
      $this->CI->db->select('menus.*');
      $this->CI->db->from('menus');
      $this->CI->db->join('terms','terms.term_id=menus.term_id AND terms.slug='."'$slug'");
       $this->CI->db->order_by('menus.order','asc');
      $qry=$this->CI->db->get();
      if($qry->num_rows()>0)
        return $qry->result();
      else
        return FALSE;
  }


 public function get_full_content($str=''){
  //echo $aa=file_get_contents(current_url(),TRUE);
   //echo  $aa=fopen(current_url(),'r');
     //$this->load->library('shortcodes');
       // $str = $this->acapella_blog->parse($str);
 }

 public function get_form($params=array()){
   $form_id = isset($params['id']) ? $params['id'] : FALSE;
   if( $form_id){
      $frm_query=$this->CI->db->get_where('forms',array('id'=>$form_id));
      if($frm_query->num_rows()>0)            
         return $frm_query->row()->form_content;
      else
         return FALSE;
   }
 }

  public function get_slider($params=array()){
      $slider_id = isset($params['id']) ? $params['id'] : FALSE;
   if( $slider_id){
      $frm_query=$this->CI->db->get_where('slider_images',array('slider_id'=>$slider_id));
      if($frm_query->num_rows()>0)            
        return $frm_query->result();
      else
        return FALSE;
    }  
 }


 public function get_post($params=array()){
   $page_id = isset($params['id']) ? $params['id'] : FALSE;
   if( $page_id){
      $frm_query=$this->CI->db->get_where('posts',array('id'=>$page_id));
      if($frm_query->num_rows()>0)            
         return $frm_query->row();
      else
         return FALSE;
   }
 }

  /*public function parse ($str)
    {
      // echo "AA ".$str." BB";
        // Check for existing shortcodes
        if (! strstr($str, '[acapella:')) {
            return $str;
        }
        
        // Find matches against shortcodes like [acapella:user id=1|class=admin]
        preg_match_all('/\[acapella:([a-zA-Z0-9-_: |=\.]+)]/', $str, $shortcodes);
        
        if ($shortcodes == NULL) {
            return $str;
        }
        
        // Tidy up
        foreach ($shortcodes[1] as $key => $shortcode) {
            if (strstr($shortcode, ' ')) {
                $code = substr($shortcode, 0, strpos($shortcode, ' '));
                $tmp = explode('|', str_replace($code . ' ', '', $shortcode));
                $params = array();
                if (count($tmp)) {
                    foreach ($tmp as $param) {
                        $pair = explode('=', $param);
                        $params[$pair[0]] = $pair[1];
                    }
                }
                $array = array('code' => $code, 'params' => $params);
            }
            else {
                $array = array('code' => $shortcode, 'params' => array());
            }
            
            $shortcode_array[$shortcodes[0][$key]] = $array;
        }
        
        // Replace shortcode instances with HTML strings
        if (count($shortcode_array)) {
                             
            foreach ($shortcode_array as $search => $shortcode) {
                $shortcode_model = $shortcode['code']; 

                 $str = str_replace($search, $this->short_code_get($shortcode['params'],$shortcode_model), $str);
            }
        }
        
        // Return the entire parsed string
        return $str;
    }*/

    /*public function short_code_get($params=array(),$shortcode=''){    
      if(!empty($shortcode)):
        switch (strtolower($shortcode)) {
          case 'form':
            
                  $form_id = isset($params['id']) ? $params['id'] : FALSE;
                  if($form_id):            
                  $frm_query=$this->CI->db->get_where('forms',array('id'=>$form_id));
                  if($frm_query->num_rows()>0)            
                    return $frm_query->row()->form_content;
                  else
                    return FALSE;
                  else:
                  return FALSE;
                  endif; 


            break;

             case 'slider':
            
                  $slider_id = isset($params['id']) ? $params['id'] : FALSE;
                  if($slider_id):            
                  $frm_query=$this->CI->db->get_where('slider_images',array('slider_id'=>$slider_id));
                  if($frm_query->num_rows()>0)            
                    return $frm_query->result();
                  else
                    return FALSE;
                  else:
                  return FALSE;
                  endif; 
            
            break;
          
          default:
              return '';
            break;
        }       
      endif;
    }*/


    /*public function get_sliders($slider_id='') {
      if(!empty($slider_id)){
        $qry=$this->CI->db->get_where('slider_images',array('slider_id'=>$slider_id));
        if($qry->num_rows()>0)
          return $qry->result();
        else
          return FALSE;
      }else{
        return FALSE;
      }  
    }*/

}?>